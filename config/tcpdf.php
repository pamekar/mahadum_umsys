<?php
return [
    'page_format'           => 'A4',
    'page_orientation'      => 'P',
    'page_units'            => 'mm',
    'unicode'               => true,
    'encoding'              => 'UTF-8',
    'font_directory'        => '',
    'image_directory'       => public_path(''),
    'tcpdf_throw_exception' => true,
    // See more info at the tcpdf_config.php file in TCPDF (if you do not set this here, TCPDF will use it default)
    // https://raw.githubusercontent.com/tecnickcom/TCPDF/develop/config/tcpdf_config.php
    // 'path_main' => '', // K_PATH_MAIN
    // 'path_url' => '', // K_PATH_URL
    'header_logo'           => 'images/global/_icon.jpg',
    // PDF_HEADER_LOGO
    'header_logo_width'     => '25',
    // PDF_HEADER_LOGO_WIDTH
    'path_cache'            => sys_get_temp_dir() . '/',
    // K_PATH_CACHE
    // 'blank_image' => '', // K_BLANK_IMAGE
    'creator'               => 'GreenWhiteDev',
    // PDF_CREATOR
    'author'                => 'Mahadum',
    // PDF_AUTHOR
    'header_title'          => 'Mahadum University Management System',
    // PDF_HEADER_TITLE
    'header_string'         => 'Mahadum University Management System (MUMS)',
    // PDF_HEADER_STRING
    'page_units'            => 'mm',
    // PDF_UNIT
    'margin_header'         => '20',
    // PDF_MARGIN_HEADER
    'margin_footer'         => '15',
    // PDF_MARGIN_FOOTER
    'margin_top'            => '30',
    // PDF_MARGIN_TOP
    'margin_bottom'         => '16',
    // PDF_MARGIN_BOTTOM
    'margin_left'           => '',
    // PDF_MARGIN_LEFT
    'margin_right'          => '',
    // PDF_MARGIN_RIGHT
    'font_name_main'        => 'helvetica',
    // PDF_FONT_NAME_MAIN
    'font_size_main'        => '10',
    // PDF_FONT_SIZE_MAIN
    'font_name_data'        => 'helvetica',
    // PDF_FONT_NAME_DATA
    'font_size_data'        => '8',
    // PDF_FONT_SIZE_DATA
    'foto_monospaced'       => 'courier',
    // PDF_FONT_MONOSPACED
    'image_scale_ratio'     => '1.25',
    // PDF_IMAGE_SCALE_RATIO
    'head_magnification'    => '1.1',
    // HEAD_MAGNIFICATION
    'cell_height_ratio'     => '1.25',
    // K_CELL_HEIGHT_RATIO
    'title_magnification'   => '1.3',
    // K_TITLE_MAGNIFICATION
    'small_ratio'           => '2/3',
    // K_SMALL_RATIO
    'thai_topchars'         => 'true',
    // K_THAI_TOPCHARS
    'tcpdf_calls_in_html'   => 'true',
    // K_TCPDF_CALLS_IN_HTML
    'timezone'              => ''
]; // K_TIMEZONE
