function fetchMessageCount() {
    $.ajax({
        url: '/staff/lecturer/messages/action/count',
        type: 'get',
        success: function (data) {
            $('#count-draft').text(data.draft);
            $('#count-inbox').text(data.inbox);
            $('#count-sent').text(data.sent);
            $('#count-starred').text(data.starred);
            $('#count-trash').text(data.trash);
        },
        complete: function (data) {
            setTimeout(fetchMessageCount, 5000);
        }
    });
}

$(document).ready(function () {
    setTimeout(fetchMessageCount, 5000);
});

// drg >> function to star a message
function starMessage(element) {
    $.post('/staff/lecturer/messages/action/star', {'id': element.getAttribute('data-msgid')}).done(
        function (result) {
            if (result.starred) {
                $(element).find('i').attr('class', 'fa fa-star text-warning');
            } else {
                $(element).find('i').attr('class', 'si si-star text-warning');
            }
        }).fail(
        function (result) {
            Dashmix.helpers('notify', {
                align: 'center',
                type: 'danger',
                icon: 'fa fa-times mr-1',
                message: 'Oops! Something went wrong..'
            });
        });
}
function trashMessage(element) {
    $.post('/staff/lecturer/messages/action/trash', {'id': element.getAttribute('data-msgid')}).done(
        function (result) {
            if (result.trashed) {
                $(element).find('i').attr('class', 'fa fa-arrow-alt-circle-right text-info');
                $(element).attr('data-original-title','Move to Inbox');
                Dashmix.helpers('notify', {
                    align: 'center',
                    type: 'danger',
                    icon: 'fa fa-times mr-1',
                    message: 'Message has been moved to trash'
                });
            } else {
                $(element).find('i').attr('class', 'fa fa-trash-alt text-danger');
                $(element).attr('data-original-title','Delete Message');
                Dashmix.helpers('notify', {
                    align: 'center',
                    type: 'danger',
                    icon: 'fa fa-times mr-1',
                    message: 'Message has been restored to inbox'
                });

            }
        }).fail(
        function (result) {
            Dashmix.helpers('notify', {
                align: 'center',
                type: 'danger',
                icon: 'fa fa-times mr-1',
                message: 'Oops! Something went wrong..'
            });
        });
}