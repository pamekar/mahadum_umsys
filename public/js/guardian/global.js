function changePassword() {
    Dashmix.layout('header_loader_on');

    var form = document.getElementById('password-form');
    var data = new FormData(form);
    var url = '';

    $.ajax({
        url: '/change_password',
        method: 'post',
        contentType: false,
        data: data,
        processData: false,
        success: function (result) {
            Dashmix.layout('header_loader_off');
            Dashmix.helpers('notify', {
                align: 'center',
                type: result.type,
                icon: 'fa fa-check mr-1',
                message: result.message
            });
            $('.password').val('');
        },
        error: function () {
            Dashmix.layout('header_loader_off');
            Dashmix.helpers('notify', {
                align: 'center',
                type: 'danger',
                icon: 'fa fa-times mr-1',
                message: 'Oops! Something went wrong..'
            });
        }
    });

    return false;
}

function ward(ward,action,status){
    Dashmix.layout('header_loader_on');

    let data={state:status};
    $.post('/guardian/settings/ward/'+ward+'/'+action,data,function(result){
        Dashmix.layout('header_loader_off');
        Dashmix.helpers('notify', {
            align: 'center',
            type: result.type,
            icon: 'fa fa-check mr-1',
            message: result.message
        });
        let content= $('#wards');
        content.fadeOut(100);
        content.html('');
        content.hide();
        content.html(result.html);
        content.fadeIn(800);


    }).fail(function () {
        Dashmix.layout('header_loader_off');
        Dashmix.helpers('notify', {
            align: 'center',
            type: 'danger',
            icon: 'fa fa-times mr-1',
            message: 'Oops! Something went wrong..'
        });
    });
}