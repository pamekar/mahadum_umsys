function editGuardian() {
    $('#guardian-button').html('Update').attr('onclick', 'changeGuardian()');
    $('#guardian-name').removeAttr('readonly');
}

function changeGuardian() {
    Dashmix.layout('header_loader_on');
    let data = {guardian: $('#guardian-name').val()};
    $.post('/undergraduate/settings/guardian', data, function (result) {
        Dashmix.layout('header_loader_off');
        Dashmix.helpers('notify', {
            align: 'center',
            type: result.type,
            icon: 'fa fa-check mr-1',
            message: result.message
        });
        $('#guardian-button').html('<i class="fa fa-edit"></i>').attr('onclick', 'editGuardian()');
        $('#guardian-name').attr('readonly', '');
    }).fail(function () {
        Dashmix.layout('header_loader_off');
        Dashmix.helpers('notify', {
            align: 'center',
            type: 'danger',
            icon: 'fa fa-times mr-1',
            message: 'Oops! Something went wrong..'
        });
    });
}

function updateGuardian() {
    Dashmix.layout('header_loader_on');

    var form = document.getElementById('guardian-form');
    var data = new FormData(form);
    var url = '';

    $.ajax({
        url: '/undergraduate/settings/guardian/update',
        method: 'Post',
        contentType: false,
        data: data,
        processData: false,
        success: function (result) {
            Dashmix.layout('header_loader_off');
            Dashmix.helpers('notify', {
                align: 'center',
                type: result.type,
                icon: 'fa fa-check mr-1',
                message: result.message
            });
            $('#guardian-button').html('<i class="fa fa-edit"></i>').attr('onclick', 'editGuardian()');
            $('#guardian-name').attr('readonly', '');
        },
        error: function () {
            Dashmix.layout('header_loader_off');
            Dashmix.helpers('notify', {
                align: 'center',
                type: 'danger',
                icon: 'fa fa-times mr-1',
                message: 'Oops! Something went wrong..'
            });
        }
    });

    return false;
}

function changePassword() {
    Dashmix.layout('header_loader_on');

    var form = document.getElementById('password-form');
    var data = new FormData(form);
    var url = '';

    $.ajax({
        url: '/change_password',
        method: 'post',
        contentType: false,
        data: data,
        processData: false,
        success: function (result) {
            Dashmix.layout('header_loader_off');
            Dashmix.helpers('notify', {
                align: 'center',
                type: result.type,
                icon: 'fa fa-check mr-1',
                message: result.message
            });
            $('.password').val('');
        },
        error: function () {
            Dashmix.layout('header_loader_off');
            Dashmix.helpers('notify', {
                align: 'center',
                type: 'danger',
                icon: 'fa fa-times mr-1',
                message: 'Oops! Something went wrong..'
            });
        }
    });

    return false;
}