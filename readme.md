# # **Project MAHADUM** (a University Management System) #

Mahadum is a University Management System (UMS) designed by Ndu Victor O. to address the shortcomings of the current system used in managing tasks in universities. The concept of this management system is to synchronize and co-ordinate the tasks performed in universities around the country. Mahadum is an application of information technology to manage processes carried out in universities; it is a reform to:

* staff and students management,  
* student course registration, 
* exam management,
* results management, 
* Identification/verification of staffs and students (in exam halls, and other school equipment/facilities) etc. 

Mahadum proffers a solution to many of challenges encountered in the areas listed above. The use of a University Management System (UMS) will introduce efficiency, proficiency and reliability to the management of a university.


Created by: Ndu Victor O.
Designed by: Ndu Victor O.
Proposed duration: 18 months
Start date: 1-6-16 (11:11)
End date: --- 

It is built using 

- Laravel PHP Framework
- HTML (Blade Templates)
- CSS (Bootstrap V4)
- Javascript (jQuery)

The database schema construction .sql file can be located at the "database/migrations" path.

# Laravel PHP Framework

[![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Total Downloads](https://poser.pugx.org/laravel/framework/d/total.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![Latest Unstable Version](https://poser.pugx.org/laravel/framework/v/unstable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

Laravel is a web application framework with expressive, elegant syntax. We believe development must be an enjoyable, creative experience to be truly fulfilling. Laravel attempts to take the pain out of development by easing common tasks used in the majority of web projects, such as authentication, routing, sessions, queueing, and caching.

Laravel is accessible, yet powerful, providing tools needed for large, robust applications. A superb inversion of control container, expressive migration system, and tightly integrated unit testing support give you the tools you need to build any application with which you are tasked.

## Official Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

## Contributing

Thank you for considering contributing to the Laravel framework! The contribution guide can be found in the [Laravel documentation](http://laravel.com/docs/contributions).

## Security Vulnerabilities

If you discover a security vulnerability within Laravel, please send an e-mail to Taylor Otwell at taylor@laravel.com. All security vulnerabilities will be promptly addressed.

## License

The Laravel framework is open-sourced software licensed under the [MIT license](http://opensource.org/licenses/MIT).