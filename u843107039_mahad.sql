-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Nov 30, 2018 at 03:02 PM
-- Server version: 10.2.17-MariaDB
-- PHP Version: 7.1.22

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `u843107039_mahad`
--

-- --------------------------------------------------------

--
-- Table structure for table `mums_accounts_undergraduate_transactions`
--

CREATE TABLE `mums_accounts_undergraduate_transactions` (
  `transaction_id` int(11) NOT NULL,
  `undergraduate_id` int(11) NOT NULL,
  `transaction_description` longtext NOT NULL,
  `transaction_charge` int(11) NOT NULL,
  `transaction_open_date` datetime NOT NULL,
  `transaction_close_date` datetime DEFAULT NULL,
  `transaction_amount_cleared` int(11) DEFAULT NULL,
  `transaction_amount_outstanding` int(11) DEFAULT NULL,
  `transaction_payment_date` datetime DEFAULT NULL,
  `transaction_payment_location` varchar(255) DEFAULT NULL,
  `transaction_payment_by` varchar(255) DEFAULT NULL,
  `transaction_status` varchar(255) DEFAULT 'pending',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mums_admin_access`
--

CREATE TABLE `mums_admin_access` (
  `id` int(11) NOT NULL,
  `system_id` varchar(225) DEFAULT NULL,
  `admin_id` varchar(225) DEFAULT NULL,
  `admin_function` varchar(225) DEFAULT NULL,
  `admin_title` varchar(225) DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_admin_access`
--

INSERT INTO `mums_admin_access` (`id`, `system_id`, `admin_id`, `admin_function`, `admin_title`, `created_at`, `updated_at`) VALUES
(1, 'GST101_GST_SOSC_BIO', 'SP1472', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(2, 'GST103_GST_SOSC_BIO', 'SP1337', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(3, 'ENG101_MEE_SOSC_BIO', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(4, 'ENG103_MEE_SOSC_BIO', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(5, 'MTH101_MTH_SOSC_BIO', 'SP1317', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(6, 'BIO101_BIO_SOSC_BIO', 'SP1522', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(7, 'CHM101_CHM_SOSC_BIO', 'SP1332', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(8, 'PHY101_PHY_SOSC_BIO', 'SP1442', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(9, 'GST101_GST_SOSC_CHM', 'SP1472', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(10, 'GST103_GST_SOSC_CHM', 'SP1337', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(11, 'ENG101_MEE_SOSC_CHM', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(12, 'ENG103_MEE_SOSC_CHM', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(13, 'MTH101_MTH_SOSC_CHM', 'SP1317', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(14, 'BIO101_BIO_SOSC_CHM', 'SP1522', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(15, 'CHM101_CHM_SOSC_CHM', 'SP1332', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(16, 'PHY101_PHY_SOSC_CHM', 'SP1442', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(17, 'GST101_GST_SOSC_PHY', 'SP1472', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(18, 'GST103_GST_SOSC_PHY', 'SP1337', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(19, 'ENG101_MEE_SOSC_PHY', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(20, 'ENG103_MEE_SOSC_PHY', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(21, 'MTH101_MTH_SOSC_PHY', 'SP1317', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(22, 'BIO101_BIO_SOSC_PHY', 'SP1522', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(23, 'CHM101_CHM_SOSC_PHY', 'SP1332', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(24, 'PHY101_PHY_SOSC_PHY', 'SP1442', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(25, 'GST101_GST_SOSC_MTH', 'SP1472', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(26, 'GST103_GST_SOSC_MTH', 'SP1337', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(27, 'ENG101_MEE_SOSC_MTH', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(28, 'ENG103_MEE_SOSC_MTH', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(29, 'MTH101_MTH_SOSC_MTH', 'SP1317', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(30, 'BIO101_BIO_SOSC_MTH', 'SP1522', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(31, 'CHM101_CHM_SOSC_MTH', 'SP1332', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(32, 'PHY101_PHY_SOSC_MTH', 'SP1442', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(33, 'GST101_GST_SEET_CIE', 'SP1577', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(34, 'GST103_GST_SEET_CIE', 'SP1532', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(35, 'ENG101_MEE_SEET_CIE', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(36, 'ENG103_MEE_SEET_CIE', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(37, 'MTH101_MTH_SEET_CIE', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(38, 'BIO101_BIO_SEET_CIE', 'SP1557', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(39, 'CHM101_CHM_SEET_CIE', 'SP1427', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(40, 'PHY101_PHY_SEET_CIE', 'SP1162', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(41, 'GST101_GST_SEET_CHE', 'SP1577', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(42, 'GST103_GST_SEET_CHE', 'SP1532', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(43, 'ENG101_MEE_SEET_CHE', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(44, 'ENG103_MEE_SEET_CHE', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(45, 'MTH101_MTH_SEET_CHE', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(46, 'BIO101_BIO_SEET_CHE', 'SP1557', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(47, 'CHM101_CHM_SEET_CHE', 'SP1427', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(48, 'PHY101_PHY_SEET_CHE', 'SP1162', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(49, 'GST101_GST_SEET_MEE', 'SP1577', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(50, 'GST103_GST_SEET_MEE', 'SP1532', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(51, 'ENG101_MEE_SEET_MEE', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(52, 'ENG103_MEE_SEET_MEE', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(53, 'MTH101_MTH_SEET_MEE', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(54, 'BIO101_BIO_SEET_MEE', 'SP1557', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(55, 'CHM101_CHM_SEET_MEE', 'SP1427', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(56, 'PHY101_PHY_SEET_MEE', 'SP1162', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(57, 'GST101_GST_SMAT_PMT', 'SP1532', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(58, 'GST103_GST_SMAT_PMT', 'SP1472', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(59, 'ENG101_MEE_SMAT_PMT', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(60, 'ENG103_MEE_SMAT_PMT', 'SP1147', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(61, 'MTH101_MTH_SMAT_PMT', 'SP1242', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(62, 'BIO101_BIO_SMAT_PMT', 'SP1382', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(63, 'CHM101_CHM_SMAT_PMT', 'SP1447', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(64, 'PHY101_PHY_SMAT_PMT', 'SP1387', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(65, 'GST101_GST_SMAT_PMT', 'SP1532', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(66, 'GST103_GST_SMAT_PMT', 'SP1472', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(67, 'ENG101_MEE_SMAT_PMT', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(68, 'ENG103_MEE_SMAT_PMT', 'SP1147', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(69, 'MTH101_MTH_SMAT_PMT', 'SP1242', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(70, 'BIO101_BIO_SMAT_PMT', 'SP1382', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(71, 'CHM101_CHM_SMAT_PMT', 'SP1447', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(72, 'PHY101_PHY_SMAT_PMT', 'SP1387', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(73, 'GST201_GST_SOSC_BIO', 'SP1267', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(74, 'ENG201_CIE_SOSC_BIO', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(75, 'CSC201_IMT_SOSC_BIO', 'SP1572', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(76, 'MTH201_MTH_SOSC_BIO', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(77, 'GST201_GST_SOSC_CHM', 'SP1267', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(78, 'ENG201_CIE_SOSC_CHM', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(79, 'CSC201_IMT_SOSC_CHM', 'SP1572', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(80, 'MTH201_MTH_SOSC_CHM', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(81, 'BIO201_BIO_SOSC_BIO', 'SP1272', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(82, 'BIO203_BIO_SOSC_BIO', 'SP1557', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(83, 'CHM201_CHM_SOSC_BIO', 'SP1332', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(84, 'CHM207_CHM_SOSC_BIO', 'SP1222', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(85, 'BIO201_BIO_SOSC_CHM', 'SP1272', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(86, 'CHM203_CHM_SOSC_CHM', 'SP1447', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(87, 'CHM201_CHM_SOSC_CHM', 'SP1332', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(88, 'CHM207_CHM_SOSC_CHM', 'SP1222', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(89, 'GST201_GST_SOSC_PHY', 'SP1267', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(90, 'ENG201_CIE_SOSC_PHY', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(91, 'CSC201_IMT_SOSC_PHY', 'SP1572', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(92, 'MTH201_MTH_SOSC_PHY', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(93, 'GST201_GST_SOSC_MTH', 'SP1267', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(94, 'ENG201_CIE_SOSC_MTH', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(95, 'CSC201_IMT_SOSC_MTH', 'SP1572', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(96, 'MTH201_MTH_SOSC_MTH', 'SP1367', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(97, 'GST201_GST_SEET_CHE', 'SP1577', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(98, 'ENG201_CIE_SEET_CHE', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(99, 'CSC201_IMT_SEET_CHE', 'SP1462', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(100, 'MTH201_MTH_SEET_CHE', 'SP1452', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(101, 'GST201_GST_SEET_MEE', 'SP1577', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(102, 'ENG201_CIE_SEET_MEE', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(103, 'CSC201_IMT_SEET_MEE', 'SP1462', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(104, 'MTH201_MTH_SEET_MEE', 'SP1452', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(105, 'GST201_GST_SEET_CIE', 'SP1577', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(106, 'ENG201_CIE_SEET_CIE', 'SP1152', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(107, 'CSC201_IMT_SEET_CIE', 'SP1462', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(108, 'MTH201_MTH_SEET_CIE', 'SP1452', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(109, 'GST201_GST_SMAT_PMT', 'SP1337', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(110, 'ENG201_CIE_SMAT_PMT', 'SP1097', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(111, 'CSC201_IMT_SMAT_PMT', 'SP1497', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(112, 'MTH201_MTH_SMAT_PMT', 'SP1317', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(113, 'GST201_GST_SMAT_IMT', 'SP1337', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(114, 'ENG201_CIE_SMAT_IMT', 'SP1097', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(115, 'CSC201_IMT_SMAT_IMT', 'SP1497', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(116, 'MTH201_MTH_SMAT_IMT', 'SP1317', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(117, 'MTH203_MTH_SOSC_PHY', 'SP1252', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(118, 'MTH203_MTH_SOSC_MTH', 'SP1252', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(119, 'MTH203_MTH_SEET_CIE', 'SP1432', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(120, 'MTH203_MTH_SEET_CHE', 'SP1432', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(121, 'MTH203_MTH_SEET_MEE', 'SP1432', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(122, 'MTH211_MTH_SOSC_MTH', 'SP1242', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(123, 'MTH211_MTH_SOSC_PHY', '#N/A', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(124, 'MTH211_MTH_SMAT_IMT', 'SP1432', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(125, 'ENG203_CHE_SEET_CIE', 'SP1147', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(126, 'ENG203_CHE_SEET_CHE', 'SP1147', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(127, 'ENG203_CHE_SEET_MEE', 'SP1147', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(128, 'ENG203_CHE_SMAT_PMT', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(129, 'ENG203_CHE_SMAT_IMT', 'SP1312', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(130, 'ENG207_MEE_SEET_CIE', '#N/A', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(131, 'ENG207_MEE_SEET_CHE', 'SP1562', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(132, 'ENG207_MEE_SEET_MEE', 'SP1562', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(133, 'PHY201_PHY_SOSC_PHY', 'SP1197', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(134, 'PHY203_PHY_SOSC_PHY', 'SP1227', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(135, 'PHY207_PHY_SOSC_PHY', 'SP1117', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(136, 'PHY203_PHY_SOSC_MTH', 'SP1227', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(137, 'PHY207_PHY_SOSC_MTH', '#N/A', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(138, 'CIE201_CIE_SEET_CIE', 'SP1437', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(139, 'CHE201_CHE_SEET_CHE', 'SP1062', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(140, 'MEE201_MEE_SEET_MEE', 'SP1392', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(141, 'PMT201_PMT_SMAT_PMT', 'SP1477', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(142, 'PMT203_PMT_SMAT_PMT', 'SP1352', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(143, 'IMT201_IMT_SMAT_IMT', 'SP1527', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(144, 'IMT203_IMT_SMAT_IMT', 'SP1302', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(145, 'MTH211_MTH_SMAT_PMT', 'SP1432', 'course_lecturer', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(146, 'CA_2016_SOSC_BIO', 'SP1557', 'course_adviser', 'Course Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(147, 'CA_2016_SEET_CHE', 'SP1357', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(148, 'CA_2016_SOSC_CHM', 'SP1447', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(149, 'CA_2016_SEET_CIE', 'SP1512', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(150, 'CA_2016_SMAT_IMT', 'SP1527', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(151, 'CA_2016_SEET_MEE', 'SP1502', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(152, 'CA_2016_SOSC_MTH', 'SP1432', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(153, 'CA_2016_SOSC_PHY', 'SP1542', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(154, 'CA_2016_SMAT_PMT', 'SP1507', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(155, 'CA_2015_SOSC_BIO', 'SP1492', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(156, 'CA_2015_SEET_CHE', 'SP1182', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(157, 'CA_2015_SOSC_CHM', 'SP1407', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(158, 'CA_2015_SEET_CIE', 'SP1437', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(159, 'CA_2015_SMAT_IMT', 'SP1467', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(160, 'CA_2015_SEET_MEE', 'SP1392', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(161, 'CA_2015_SOSC_MTH', 'SP1367', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(162, 'CA_2015_SOSC_PHY', 'SP1422', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(163, 'CA_2015_SMAT_PMT', 'SP1477', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(164, 'CA_2014_SOSC_BIO', 'SP1382', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(165, 'CA_2014_SEET_CHE', 'SP1132', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(166, 'CA_2014_SOSC_CHM', 'SP1232', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(167, 'CA_2014_SEET_CIE', 'SP1362', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(168, 'CA_2014_SMAT_IMT', 'SP1347', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(169, 'CA_2014_SEET_MEE', 'SP1307', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(170, 'CA_2014_SOSC_MTH', 'SP1317', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(171, 'CA_2014_SOSC_PHY', 'SP1237', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(172, 'CA_2014_SMAT_PMT', 'SP1352', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(173, 'CA_2013_SOSC_BIO', 'SP1277', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(174, 'CA_2013_SEET_CHE', 'SP1062', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(175, 'CA_2013_SOSC_CHM', 'SP1217', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(176, 'CA_2013_SEET_CIE', 'SP1207', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(177, 'CA_2013_SMAT_IMT', 'SP1297', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(178, 'CA_2013_SEET_MEE', 'SP1152', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(179, 'CA_2013_SOSC_MTH', 'SP1247', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(180, 'CA_2013_SOSC_PHY', 'SP1197', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(181, 'CA_2013_SMAT_PMT', 'SP1322', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(182, 'CA_2012_SOSC_BIO', 'SP1257', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(183, 'CA_2012_SEET_CHE', 'SP1017', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(184, 'CA_2012_SOSC_CHM', 'SP1177', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(185, 'CA_2012_SEET_CIE', 'SP1082', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(186, 'CA_2012_SMAT_IMT', 'SP1137', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(187, 'CA_2012_SEET_MEE', 'SP1097', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(188, 'CA_2012_SOSC_MTH', 'SP1187', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(189, 'CA_2012_SOSC_PHY', 'SP1117', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(190, 'CA_2012_SMAT_PMT', 'SP1157', 'course_adviser', 'Course Adviser', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(191, 'HOD_SMAT_IMT', 'SP1032', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(192, 'HOD_SOSC_BIO', 'SP1052', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(193, 'HOD_SEET_CHE', 'SP1012', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(194, 'HOD_SOSC_CHM', 'SP1072', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(195, 'HOD_SEET_CIE', 'SP1057', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(196, 'HOD_SOGS_GST', 'SP1122', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(197, 'HOD_SEET_MEE', 'SP1022', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(198, 'HOD_SOSC_MTH', 'SP1027', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(199, 'HOD_SOSC_PHY', 'SP1107', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(200, 'HOD_SMAT_PMT', 'SP1037', 'head_of_department', 'HOD', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(201, 'LECT_SEET_CHE', 'SP1042', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(202, 'LECT_SEET_CHE', 'SP1127', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(203, 'LECT_SEET_CHE', 'SP1167', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(204, 'LECT_SEET_CHE', 'SP1287', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(205, 'LECT_SEET_CHE', 'SP1397', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(206, 'LECT_SEET_CHE', 'SP1402', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(207, 'LECT_SEET_CIE', 'SP1077', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(208, 'LECT_SEET_CIE', 'SP1172', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(209, 'LECT_SEET_CIE', 'SP1292', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(210, 'LECT_SEET_CIE', 'SP1377', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(211, 'LECT_SEET_CIE', 'SP1482', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(212, 'LECT_SEET_CIE', 'SP1547', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(213, 'LECT_SEET_MEE', 'SP1087', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(214, 'LECT_SEET_MEE', 'SP1147', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(215, 'LECT_SEET_MEE', 'SP1202', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(216, 'LECT_SEET_MEE', 'SP1312', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(217, 'LECT_SEET_MEE', 'SP1412', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(218, 'LECT_SEET_MEE', 'SP1562', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(219, 'LECT_SMAT_IMT', 'SP1102', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(220, 'LECT_SMAT_IMT', 'SP1262', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(221, 'LECT_SMAT_IMT', 'SP1302', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(222, 'LECT_SMAT_IMT', 'SP1462', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(223, 'LECT_SMAT_IMT', 'SP1497', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(224, 'LECT_SMAT_IMT', 'SP1572', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(225, 'LECT_SMAT_PMT', 'SP1092', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(226, 'LECT_SMAT_PMT', 'SP1192', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(227, 'LECT_SMAT_PMT', 'SP1327', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(228, 'LECT_SMAT_PMT', 'SP1372', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(229, 'LECT_SMAT_PMT', 'SP1487', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(230, 'LECT_SMAT_PMT', 'SP1517', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(231, 'LECT_SOSC_BIO', 'SP1067', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(232, 'LECT_SOSC_BIO', 'SP1272', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(233, 'LECT_SOSC_BIO', 'SP1282', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(234, 'LECT_SOSC_BIO', 'SP1457', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(235, 'LECT_SOSC_BIO', 'SP1522', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(236, 'LECT_SOSC_BIO', 'SP1567', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(237, 'LECT_SOSC_CHM', 'SP1142', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(238, 'LECT_SOSC_CHM', 'SP1212', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(239, 'LECT_SOSC_CHM', 'SP1222', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(240, 'LECT_SOSC_CHM', 'SP1332', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(241, 'LECT_SOSC_CHM', 'SP1427', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(242, 'LECT_SOSC_CHM', 'SP1537', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(243, 'LECT_SOSC_MTH', 'SP1047', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(244, 'LECT_SOSC_MTH', 'SP1242', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(245, 'LECT_SOSC_MTH', 'SP1252', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(246, 'LECT_SOSC_MTH', 'SP1342', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(247, 'LECT_SOSC_MTH', 'SP1417', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(248, 'LECT_SOSC_MTH', 'SP1452', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(249, 'LECT_SOSC_PHY', 'SP1112', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(250, 'LECT_SOSC_PHY', 'SP1162', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(251, 'LECT_SOSC_PHY', 'SP1227', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(252, 'LECT_SOSC_PHY', 'SP1387', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(253, 'LECT_SOSC_PHY', 'SP1442', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(254, 'LECT_SOSC_PHY', 'SP1552', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(255, 'LECT_SOGS_GST', 'SP1267', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(256, 'LECT_SOGS_GST', 'SP1337', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(257, 'LECT_SOGS_GST', 'SP1472', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(258, 'LECT_SOGS_GST', 'SP1532', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(259, 'LECT_SOGS_GST', 'SP1577', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(260, 'LECT_SOSC_BIO', 'SP1557', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(261, 'LECT_SEET_CHE', 'SP1357', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(262, 'LECT_SOSC_CHM', 'SP1447', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(263, 'LECT_SEET_CIE', 'SP1512', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(264, 'LECT_SMAT_IMT', 'SP1527', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(265, 'LECT_SEET_MEE', 'SP1502', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(266, 'LECT_SOSC_MTH', 'SP1432', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(267, 'LECT_SOSC_PHY', 'SP1542', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(268, 'LECT_SMAT_PMT', 'SP1507', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(269, 'LECT_SOSC_BIO', 'SP1492', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(270, 'LECT_SEET_CHE', 'SP1182', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(271, 'LECT_SOSC_CHM', 'SP1407', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(272, 'LECT_SEET_CIE', 'SP1437', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(273, 'LECT_SMAT_IMT', 'SP1467', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(274, 'LECT_SEET_MEE', 'SP1392', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(275, 'LECT_SOSC_MTH', 'SP1367', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(276, 'LECT_SOSC_PHY', 'SP1422', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(277, 'LECT_SMAT_PMT', 'SP1477', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(278, 'LECT_SOSC_BIO', 'SP1382', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(279, 'LECT_SEET_CHE', 'SP1132', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(280, 'LECT_SOSC_CHM', 'SP1232', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(281, 'LECT_SEET_CIE', 'SP1362', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(282, 'LECT_SMAT_IMT', 'SP1347', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(283, 'LECT_SEET_MEE', 'SP1307', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(284, 'LECT_SOSC_MTH', 'SP1317', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(285, 'LECT_SOSC_PHY', 'SP1237', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(286, 'LECT_SMAT_PMT', 'SP1352', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(287, 'LECT_SOSC_BIO', 'SP1277', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(288, 'LECT_SEET_CHE', 'SP1062', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(289, 'LECT_SOSC_CHM', 'SP1217', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(290, 'LECT_SEET_CIE', 'SP1207', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(291, 'LECT_SMAT_IMT', 'SP1297', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(292, 'LECT_SEET_MEE', 'SP1152', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(293, 'LECT_SOSC_MTH', 'SP1247', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(294, 'LECT_SOSC_PHY', 'SP1197', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(295, 'LECT_SMAT_PMT', 'SP1322', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(296, 'LECT_SOSC_BIO', 'SP1257', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(297, 'LECT_SEET_CHE', 'SP1017', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(298, 'LECT_SOSC_CHM', 'SP1177', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(299, 'LECT_SEET_CIE', 'SP1082', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(300, 'LECT_SMAT_IMT', 'SP1137', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(301, 'LECT_SEET_MEE', 'SP1097', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(302, 'LECT_SOSC_MTH', 'SP1187', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(303, 'LECT_SOSC_PHY', 'SP1117', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(304, 'LECT_SMAT_PMT', 'SP1157', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(305, 'LECT_SMAT_IMT', 'SP1032', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(306, 'LECT_SOSC_BIO', 'SP1052', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(307, 'LECT_SEET_CHE', 'SP1012', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(308, 'LECT_SOSC_CHM', 'SP1072', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(309, 'LECT_SEET_CIE', 'SP1057', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(310, 'LECT_SOGS_GST', 'SP1122', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(311, 'LECT_SEET_MEE', 'SP1022', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(312, 'LECT_SOSC_MTH', 'SP1027', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(313, 'LECT_SOSC_PHY', 'SP1107', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(314, 'LECT_SMAT_PMT', 'SP1037', 'lecturer', 'Lecturer', '2018-01-02 20:57:15', '2018-01-02 20:59:54'),
(315, 'mums', 'registrar', 'registrar', 'Registrar', '2018-01-02 20:57:15', '2018-01-02 20:59:54');

-- --------------------------------------------------------

--
-- Table structure for table `mums_categories`
--

CREATE TABLE `mums_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_categories`
--

INSERT INTO `mums_categories` (`id`, `parent_id`, `order`, `name`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Category 1', 'category-1', '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(2, NULL, 1, 'Category 2', 'category-2', '2018-11-09 22:49:44', '2018-11-09 22:49:44');

-- --------------------------------------------------------

--
-- Table structure for table `mums_chatter_categories`
--

CREATE TABLE `mums_chatter_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `parent_id` int(10) UNSIGNED DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_chatter_categories`
--

INSERT INTO `mums_chatter_categories` (`id`, `parent_id`, `order`, `name`, `color`, `slug`, `created_at`, `updated_at`) VALUES
(1, NULL, 1, 'Introductions', '#3498DB', 'introductions', NULL, NULL),
(2, NULL, 2, 'General', '#2ECC71', 'general', NULL, NULL),
(3, NULL, 3, 'Feedback', '#9B59B6', 'feedback', NULL, NULL),
(4, NULL, 4, 'Random', '#E67E22', 'random', NULL, NULL),
(5, 1, 1, 'Rules', '#227ab5', 'rules', NULL, NULL),
(6, 5, 1, 'Basics', '#195a86', 'basics', NULL, NULL),
(7, 5, 2, 'Contribution', '#195a86', 'contribution', NULL, NULL),
(8, 1, 2, 'About', '#227ab5', 'about', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mums_chatter_discussion`
--

CREATE TABLE `mums_chatter_discussion` (
  `id` int(10) UNSIGNED NOT NULL,
  `chatter_category_id` int(10) UNSIGNED NOT NULL DEFAULT 1,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `sticky` tinyint(1) NOT NULL DEFAULT 0,
  `views` int(10) UNSIGNED NOT NULL DEFAULT 0,
  `answered` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(20) COLLATE utf8mb4_unicode_ci DEFAULT '#232629',
  `deleted_at` timestamp NULL DEFAULT NULL,
  `last_reply_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_chatter_discussion`
--

INSERT INTO `mums_chatter_discussion` (`id`, `chatter_category_id`, `title`, `user_id`, `sticky`, `views`, `answered`, `created_at`, `updated_at`, `slug`, `color`, `deleted_at`, `last_reply_at`) VALUES
(3, 1, 'Hello Everyone, This is my Introduction', 1, 0, 6, 0, '2016-08-18 13:27:56', '2018-10-18 17:16:10', 'hello-everyone-this-is-my-introduction', '#239900', NULL, '2018-10-18 17:16:10'),
(6, 2, 'Login Information for Chatter', 1, 0, 0, 0, '2016-08-18 13:39:36', '2016-08-18 13:39:36', 'login-information-for-chatter', '#1a1067', NULL, '2018-10-18 16:23:06'),
(7, 3, 'Leaving Feedback', 1, 0, 0, 0, '2016-08-18 13:42:29', '2016-08-18 13:42:29', 'leaving-feedback', '#8e1869', NULL, '2018-10-18 16:23:06'),
(8, 4, 'Just a random post', 1, 0, 0, 0, '2016-08-18 13:46:38', '2016-08-18 13:46:38', 'just-a-random-post', '', NULL, '2018-10-18 16:23:06'),
(9, 2, 'Welcome to the Chatter Laravel Forum Package', 1, 0, 0, 0, '2016-08-18 13:59:37', '2016-08-18 13:59:37', 'welcome-to-the-chatter-laravel-forum-package', '', NULL, '2018-10-18 16:23:06'),
(10, 2, 'Web Developer and system practice', 188, 0, 5, 0, '2018-10-21 11:36:09', '2018-10-28 11:47:01', 'web-developer-and-system-practice', NULL, NULL, '2018-10-21 11:40:26');

-- --------------------------------------------------------

--
-- Table structure for table `mums_chatter_post`
--

CREATE TABLE `mums_chatter_post` (
  `id` int(10) UNSIGNED NOT NULL,
  `chatter_discussion_id` int(10) UNSIGNED NOT NULL,
  `user_id` int(10) UNSIGNED NOT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `markdown` tinyint(1) NOT NULL DEFAULT 0,
  `locked` tinyint(1) NOT NULL DEFAULT 0,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_chatter_post`
--

INSERT INTO `mums_chatter_post` (`id`, `chatter_discussion_id`, `user_id`, `body`, `created_at`, `updated_at`, `markdown`, `locked`, `deleted_at`) VALUES
(1, 3, 1, '<p>My name is Tony and I\'m a developer at <a href=\"https://devdojo.com\" target=\"_blank\">https://devdojo.com</a> and I also work with an awesome company in PB called The Control Group: <a href=\"http://www.thecontrolgroup.com\" target=\"_blank\">http://www.thecontrolgroup.com</a></p>\n        <p>You can check me out on twitter at <a href=\"http://www.twitter.com/tnylea\" target=\"_blank\">http://www.twitter.com/tnylea</a></p>\n        <p>or you can subscribe to me on YouTube at <a href=\"http://www.youtube.com/devdojo\" target=\"_blank\">http://www.youtube.com/devdojo</a></p>', '2016-08-18 13:27:56', '2016-08-18 13:27:56', 0, 0, NULL),
(5, 6, 1, '<p>Hey!</p>\n        <p>Thanks again for checking out chatter. If you want to login with the default user you can login with the following credentials:</p>\n        <p><strong>email address</strong>: tony@hello.com</p>\n        <p><strong>password</strong>: password</p>\n        <p>You\'ll probably want to delete this user, but if for some reason you want to keep it... Go ahead :)</p>', '2016-08-18 13:39:36', '2016-08-18 13:39:36', 0, 0, NULL),
(6, 7, 1, '<p>If you would like to leave some feedback or have any issues be sure to visit the github page here: <a href=\"https://github.com/thedevdojo/chatter\" target=\"_blank\">https://github.com/thedevdojo/chatter</a>&nbsp;and I\'m sure I can help out.</p>\n        <p>Let\'s make this package the go to Laravel Forum package. Feel free to contribute and share your ideas :)</p>', '2016-08-18 13:42:29', '2016-08-18 13:42:29', 0, 0, NULL),
(7, 8, 1, '<p>This is just a random post to show you some of the formatting that you can do in the WYSIWYG editor. You can make your text <strong>bold</strong>, <em>italic</em>, or <span style=\"text-decoration: underline;\">underlined</span>.</p>\n        <p style=\"text-align: center;\">Additionally, you can center align text.</p>\n        <p style=\"text-align: right;\">You can align the text to the right!</p>\n        <p>Or by default it will be aligned to the left.</p>\n        <ul>\n        <li>We can also</li>\n        <li>add a bulleted</li>\n        <li>list</li>\n        </ul>\n        <ol>\n        <li><span style=\"line-height: 1.6;\">or we can</span></li>\n        <li><span style=\"line-height: 1.6;\">add a numbered list</span></li>\n        </ol>\n        <p style=\"padding-left: 30px;\"><span style=\"line-height: 1.6;\">We can choose to indent our text</span></p>\n        <p><span style=\"line-height: 1.6;\">Post links: <a href=\"https://devdojo.com\" target=\"_blank\">https://devdojo.com</a></span></p>\n        <p><span style=\"line-height: 1.6;\">and add images:</span></p>\n        <p><span style=\"line-height: 1.6;\"><img src=\"https://media.giphy.com/media/o0vwzuFwCGAFO/giphy.gif\" alt=\"\" width=\"300\" height=\"300\" /></span></p>', '2016-08-18 13:46:38', '2016-08-18 13:46:38', 0, 0, NULL),
(8, 8, 1, '<p>Haha :) Cats!</p>\n        <p><img src=\"https://media.giphy.com/media/5Vy3WpDbXXMze/giphy.gif\" alt=\"\" width=\"250\" height=\"141\" /></p>\n        <p><img src=\"https://media.giphy.com/media/XNdoIMwndQfqE/200.gif\" alt=\"\" width=\"200\" height=\"200\" /></p>', '2016-08-18 13:55:42', '2016-08-18 14:45:13', 0, 0, NULL),
(9, 9, 1, '<p>Hey There!</p>\n        <p>My name is Tony and I\'m the creator of this package that you\'ve just installed. Thanks for checking out it out and if you have any questions or want to contribute be sure to checkout the repo here: <a href=\"https://github.com/thedevdojo/chatter\" target=\"_blank\">https://github.com/thedevdojo/chatter</a></p>\n        <p>Happy programming!</p>', '2016-08-18 13:59:37', '2016-08-18 13:59:37', 0, 0, NULL),
(10, 9, 1, '<p>Hell yeah Bro Sauce!</p>\n        <p><img src=\"https://media.giphy.com/media/j5QcmXoFWl4Q0/giphy.gif\" alt=\"\" width=\"366\" height=\"229\" /></p>', '2016-08-18 14:01:25', '2016-08-18 14:01:25', 0, 0, NULL),
(11, 3, 320, '<p>You can check me out on twitter at <a href=\"http://www.twitter.com/tnylea\" target=\"_blank\">http://www.twitter.com/tnylea</a></p>\r\n<p>or you can subscribe to me on YouTube at <a href=\"http://www.youtube.com/devdojo\" target=\"_blank\">http://www.youtube.com/devdojo</a></p>', '2018-10-18 17:16:10', '2018-10-18 17:16:10', 0, 0, NULL),
(12, 10, 188, '<p>&nbsp;I\'m just starting a conversation</p>', '2018-10-21 11:36:09', '2018-10-21 11:36:09', 0, 0, NULL),
(13, 10, 188, '<p>This is a typical reply.</p>', '2018-10-21 11:40:25', '2018-10-21 11:40:25', 0, 0, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mums_chatter_user_discussion`
--

CREATE TABLE `mums_chatter_user_discussion` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `discussion_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_chatter_user_discussion`
--

INSERT INTO `mums_chatter_user_discussion` (`user_id`, `discussion_id`) VALUES
(188, 10);

-- --------------------------------------------------------

--
-- Table structure for table `mums_data_rows`
--

CREATE TABLE `mums_data_rows` (
  `id` int(10) UNSIGNED NOT NULL,
  `data_type_id` int(10) UNSIGNED NOT NULL,
  `field` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `required` tinyint(1) NOT NULL DEFAULT 0,
  `browse` tinyint(1) NOT NULL DEFAULT 1,
  `read` tinyint(1) NOT NULL DEFAULT 1,
  `edit` tinyint(1) NOT NULL DEFAULT 1,
  `add` tinyint(1) NOT NULL DEFAULT 1,
  `delete` tinyint(1) NOT NULL DEFAULT 1,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `order` int(11) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_data_rows`
--

INSERT INTO `mums_data_rows` (`id`, `data_type_id`, `field`, `type`, `display_name`, `required`, `browse`, `read`, `edit`, `add`, `delete`, `details`, `order`) VALUES
(1, 1, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(2, 1, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(3, 1, 'email', 'text', 'Email', 1, 1, 1, 1, 1, 1, '', 3),
(4, 1, 'password', 'password', 'Password', 1, 0, 0, 1, 1, 0, '', 4),
(5, 1, 'remember_token', 'text', 'Remember Token', 0, 0, 0, 0, 0, 0, '', 5),
(6, 1, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 6),
(7, 1, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(8, 1, 'avatar', 'image', 'Avatar', 0, 1, 1, 1, 1, 1, '', 8),
(9, 1, 'user_belongsto_role_relationship', 'relationship', 'Role', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsTo\",\"column\":\"role_id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"roles\",\"pivot\":\"0\"}', 10),
(10, 1, 'user_belongstomany_role_relationship', 'relationship', 'Roles', 0, 1, 1, 1, 1, 0, '{\"model\":\"TCG\\\\Voyager\\\\Models\\\\Role\",\"table\":\"roles\",\"type\":\"belongsToMany\",\"column\":\"id\",\"key\":\"id\",\"label\":\"display_name\",\"pivot_table\":\"user_roles\",\"pivot\":\"1\",\"taggable\":\"0\"}', 11),
(11, 1, 'locale', 'text', 'Locale', 0, 1, 1, 1, 1, 0, '', 12),
(12, 1, 'settings', 'hidden', 'Settings', 0, 0, 0, 0, 0, 0, '', 12),
(13, 2, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(14, 2, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(15, 2, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(16, 2, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(17, 3, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(18, 3, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 2),
(19, 3, 'created_at', 'timestamp', 'Created At', 0, 0, 0, 0, 0, 0, '', 3),
(20, 3, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 4),
(21, 3, 'display_name', 'text', 'Display Name', 1, 1, 1, 1, 1, 1, '', 5),
(22, 1, 'role_id', 'text', 'Role', 1, 1, 1, 1, 1, 1, '', 9),
(23, 4, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(24, 4, 'parent_id', 'select_dropdown', 'Parent', 0, 0, 1, 1, 1, 1, '{\"default\":\"\",\"null\":\"\",\"options\":{\"\":\"-- None --\"},\"relationship\":{\"key\":\"id\",\"label\":\"name\"}}', 2),
(25, 4, 'order', 'text', 'Order', 1, 1, 1, 1, 1, 1, '{\"default\":1}', 3),
(26, 4, 'name', 'text', 'Name', 1, 1, 1, 1, 1, 1, '', 4),
(27, 4, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"name\"}}', 5),
(28, 4, 'created_at', 'timestamp', 'Created At', 0, 0, 1, 0, 0, 0, '', 6),
(29, 4, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 7),
(30, 5, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(31, 5, 'author_id', 'text', 'Author', 1, 0, 1, 1, 0, 1, '', 2),
(32, 5, 'category_id', 'text', 'Category', 1, 0, 1, 1, 1, 0, '', 3),
(33, 5, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 4),
(34, 5, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 5),
(35, 5, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 6),
(36, 5, 'image', 'image', 'Post Image', 0, 1, 1, 1, 1, 1, '{\"resize\":{\"width\":\"1000\",\"height\":\"null\"},\"quality\":\"70%\",\"upsize\":true,\"thumbnails\":[{\"name\":\"medium\",\"scale\":\"50%\"},{\"name\":\"small\",\"scale\":\"25%\"},{\"name\":\"cropped\",\"crop\":{\"width\":\"300\",\"height\":\"250\"}}]}', 7),
(37, 5, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\",\"forceUpdate\":true},\"validation\":{\"rule\":\"unique:posts,slug\"}}', 8),
(38, 5, 'meta_description', 'text_area', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 9),
(39, 5, 'meta_keywords', 'text_area', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 10),
(40, 5, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"DRAFT\",\"options\":{\"PUBLISHED\":\"published\",\"DRAFT\":\"draft\",\"PENDING\":\"pending\"}}', 11),
(41, 5, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, '', 12),
(42, 5, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, '', 13),
(43, 5, 'seo_title', 'text', 'SEO Title', 0, 1, 1, 1, 1, 1, '', 14),
(44, 5, 'featured', 'checkbox', 'Featured', 1, 1, 1, 1, 1, 1, '', 15),
(45, 6, 'id', 'number', 'ID', 1, 0, 0, 0, 0, 0, '', 1),
(46, 6, 'author_id', 'text', 'Author', 1, 0, 0, 0, 0, 0, '', 2),
(47, 6, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, '', 3),
(48, 6, 'excerpt', 'text_area', 'Excerpt', 1, 0, 1, 1, 1, 1, '', 4),
(49, 6, 'body', 'rich_text_box', 'Body', 1, 0, 1, 1, 1, 1, '', 5),
(50, 6, 'slug', 'text', 'Slug', 1, 0, 1, 1, 1, 1, '{\"slugify\":{\"origin\":\"title\"},\"validation\":{\"rule\":\"unique:pages,slug\"}}', 6),
(51, 6, 'meta_description', 'text', 'Meta Description', 1, 0, 1, 1, 1, 1, '', 7),
(52, 6, 'meta_keywords', 'text', 'Meta Keywords', 1, 0, 1, 1, 1, 1, '', 8),
(53, 6, 'status', 'select_dropdown', 'Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"INACTIVE\",\"options\":{\"INACTIVE\":\"INACTIVE\",\"ACTIVE\":\"ACTIVE\"}}', 9),
(54, 6, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, '', 10),
(55, 6, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, '', 11),
(56, 6, 'image', 'image', 'Page Image', 0, 1, 1, 1, 1, 1, '', 12),
(70, 11, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(71, 11, 'chatter_category_id', 'text', 'Chatter Category Id', 1, 1, 1, 1, 1, 1, NULL, 2),
(72, 11, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 4),
(73, 11, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, NULL, 3),
(74, 11, 'sticky', 'text', 'Sticky', 1, 1, 1, 1, 1, 1, NULL, 5),
(75, 11, 'views', 'text', 'Views', 1, 1, 1, 1, 1, 1, NULL, 6),
(76, 11, 'answered', 'text', 'Answered', 1, 1, 1, 1, 1, 1, NULL, 7),
(77, 11, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 8),
(78, 11, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 9),
(79, 11, 'slug', 'text', 'Slug', 1, 1, 1, 1, 1, 1, NULL, 10),
(80, 11, 'color', 'text', 'Color', 0, 1, 1, 1, 1, 1, NULL, 11),
(81, 11, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 12),
(82, 11, 'last_reply_at', 'timestamp', 'Last Reply At', 1, 1, 1, 1, 1, 1, NULL, 13),
(83, 13, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(84, 13, 'chatter_discussion_id', 'text', 'Chatter Discussion Id', 1, 0, 0, 0, 0, 0, NULL, 2),
(85, 13, 'user_id', 'text', 'User Id', 1, 1, 1, 1, 1, 1, NULL, 3),
(86, 13, 'body', 'rich_text_box', 'Body', 1, 1, 1, 1, 1, 1, NULL, 4),
(87, 13, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 1, 0, 1, NULL, 5),
(88, 13, 'updated_at', 'timestamp', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 6),
(89, 13, 'markdown', 'text', 'Markdown', 1, 1, 1, 1, 1, 1, NULL, 7),
(90, 13, 'locked', 'text', 'Locked', 1, 1, 1, 1, 1, 1, NULL, 8),
(91, 13, 'deleted_at', 'timestamp', 'Deleted At', 0, 1, 1, 1, 1, 1, NULL, 9),
(104, 16, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(105, 16, 'session', 'number', 'Session', 0, 1, 1, 1, 1, 1, NULL, 5),
(106, 16, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, NULL, 3),
(107, 16, 'type', 'select_dropdown', 'Type', 0, 1, 1, 1, 1, 1, '{\"default\":\"student\",\"options\":{\"university\":\"For University\",\"faculty\":\"For Faculty\",\"department\":\"For Department\",\"level\":\"For Level\",\"student\":\"For Student\"}}', 4),
(108, 16, 'target', 'text', 'Target', 0, 1, 1, 1, 1, 1, NULL, 6),
(109, 16, 'issuer', 'text', 'Issuer', 0, 1, 1, 1, 1, 1, '{\"default\":\"Bursary\"}', 7),
(110, 16, 'created_by', 'text', 'Created By', 0, 0, 1, 0, 0, 0, NULL, 8),
(111, 16, 'status', 'radio_btn', 'Status', 0, 1, 1, 1, 1, 1, '{\"default\":\"pending\",\"options\":{\"pending\":\"Pending\",\"publisher\":\"Published\"}}', 9),
(112, 16, 'updated_at', 'timestamp', 'Updated At', 1, 0, 1, 0, 0, 0, NULL, 10),
(113, 16, 'created_at', 'timestamp', 'Created At', 1, 0, 1, 0, 0, 0, NULL, 11),
(115, 19, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(116, 19, 'invoice_id', 'number', 'Invoice', 0, 1, 1, 1, 1, 1, NULL, 2),
(117, 19, 'item', 'text', 'Item', 0, 1, 1, 1, 1, 1, NULL, 4),
(120, 19, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 7),
(121, 19, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 8),
(122, 19, 'item_no', 'text', 'Item No', 0, 0, 0, 0, 0, 0, NULL, 9),
(123, 19, 'mums_invoice_line_belongsto_mums_invoice_relationship', 'relationship', 'Invoice', 0, 0, 0, 1, 1, 1, '{\"model\":\"App\\\\Invoice\",\"table\":\"mums_invoices\",\"type\":\"belongsTo\",\"column\":\"invoice_id\",\"key\":\"invoice_id\",\"label\":\"invoice_id\",\"pivot_table\":\"mums_accounts_undergraduate_transactions\",\"pivot\":\"0\",\"taggable\":\"0\"}', 3),
(125, 16, 'invoice_code', 'text', 'Invoice Code', 0, 1, 1, 0, 0, 0, NULL, 3),
(126, 21, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(127, 21, 'title', 'text', 'Title', 0, 1, 1, 1, 1, 1, NULL, 2),
(128, 21, 'content', 'rich_text_box', 'Content', 0, 0, 1, 1, 1, 1, NULL, 3),
(129, 21, 'system_id', 'text', 'System Id', 0, 0, 0, 0, 0, 0, NULL, 4),
(130, 21, 'announcer', 'text', 'Announcer', 0, 0, 1, 0, 0, 0, NULL, 5),
(131, 21, 'display', 'text', 'View Status', 1, 1, 1, 1, 1, 1, '{\"default\":\"hide\",\"options\":{\"show\":\"Show\",\"hide\":\"Hide\"}}', 6),
(132, 21, 'created_at', 'timestamp', 'Created At', 0, 1, 1, 0, 0, 0, NULL, 7),
(133, 21, 'updated_at', 'text', 'Updated At', 0, 0, 1, 0, 0, 0, NULL, 8),
(134, 23, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(135, 23, 'title', 'text', 'Title', 1, 1, 1, 1, 1, 1, NULL, 2),
(136, 23, 'start', 'timestamp', 'Start', 1, 1, 1, 1, 1, 1, NULL, 3),
(137, 23, 'end', 'timestamp', 'End', 1, 1, 1, 1, 1, 1, NULL, 4),
(138, 23, 'event_type', 'text', 'Type', 1, 1, 1, 1, 1, 1, NULL, 5),
(139, 23, 'event_status', 'text', 'Status', 1, 1, 1, 1, 1, 1, NULL, 6),
(140, 23, 'created_at', 'text', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 7),
(141, 24, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(142, 24, 'payment_id', 'text', 'Payment Id', 0, 0, 0, 0, 0, 0, NULL, 2),
(143, 24, 'invoice_id', 'text', 'Invoice Id', 0, 0, 1, 0, 0, 0, NULL, 3),
(144, 24, 'user_id', 'text', 'User Id', 0, 1, 1, 0, 0, 0, NULL, 5),
(145, 24, 'gateway', 'text', 'Gateway', 0, 1, 1, 0, 0, 0, NULL, 6),
(146, 24, 'amount', 'text', 'Amount', 0, 1, 1, 0, 0, 0, NULL, 7),
(147, 24, 'created_at', 'timestamp', 'Created At', 1, 1, 1, 0, 0, 0, NULL, 8),
(148, 24, 'updated_at', 'timestamp', 'Updated At', 1, 0, 0, 0, 0, 0, NULL, 9),
(149, 19, 'amount', 'text', 'Amount', 0, 1, 1, 1, 1, 1, NULL, 4),
(150, 19, 'unit', 'text', 'Unit', 1, 1, 1, 1, 1, 1, NULL, 5),
(151, 24, 'mums_payment_belongsto_mums_invoice_relationship', 'relationship', 'mums_invoices', 0, 1, 1, 1, 1, 1, '{\"model\":\"App\\\\Invoice\",\"table\":\"mums_invoices\",\"type\":\"belongsTo\",\"column\":\"invoice_id\",\"key\":\"invoice_id\",\"label\":\"title\",\"pivot_table\":\"mums_accounts_undergraduate_transactions\",\"pivot\":\"0\",\"taggable\":\"0\"}', 4),
(152, 25, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(153, 25, 'week_day', 'text', 'Week Day', 0, 1, 1, 1, 1, 1, NULL, 2),
(154, 25, 'location', 'text', 'Location', 0, 1, 1, 1, 1, 1, NULL, 3),
(155, 25, 'start_time', 'time', 'Start Time', 0, 1, 1, 1, 1, 1, NULL, 4),
(156, 25, 'end_time', 'time', 'End Time', 0, 1, 1, 1, 1, 1, NULL, 5),
(157, 25, 'course_code', 'text', 'Course Code', 0, 1, 1, 1, 1, 1, NULL, 6),
(158, 25, 'system_course_id', 'text', 'System Course Id', 0, 0, 0, 0, 0, 0, NULL, 7),
(159, 25, 'level', 'text', 'Level', 1, 0, 1, 1, 1, 1, NULL, 8),
(160, 25, 'course_source', 'text', 'Course Source', 0, 0, 1, 1, 1, 1, NULL, 9),
(161, 25, 'faculty', 'text', 'Faculty', 0, 0, 1, 1, 1, 1, NULL, 10),
(162, 25, 'dept', 'text', 'Dept', 0, 1, 1, 1, 1, 1, NULL, 11),
(163, 25, 'lecturer', 'text', 'Lecturer', 0, 1, 1, 1, 1, 1, NULL, 12),
(164, 25, 'session', 'number', 'Session', 1, 0, 1, 1, 1, 1, NULL, 13),
(165, 25, 'semester', 'text', 'Semester', 1, 0, 1, 1, 1, 1, NULL, 14),
(166, 25, 'created_at', 'hidden', 'Created At', 0, 0, 1, 0, 0, 0, NULL, 15),
(167, 26, 'id', 'text', 'Id', 1, 0, 0, 0, 0, 0, NULL, 1),
(168, 26, 'system_id', 'text', 'System Id', 0, 1, 1, 1, 1, 1, NULL, 2),
(169, 26, 'admin_id', 'text', 'Admin Id', 0, 1, 1, 1, 1, 1, NULL, 3),
(170, 26, 'admin_function', 'text', 'Admin Function', 0, 1, 1, 1, 1, 1, NULL, 4),
(171, 26, 'admin_title', 'text', 'Admin Title', 0, 1, 1, 1, 1, 1, NULL, 5),
(172, 26, 'created_at', 'text', 'Created At', 0, 0, 0, 0, 0, 0, NULL, 6),
(173, 26, 'updated_at', 'text', 'Updated At', 0, 0, 0, 0, 0, 0, NULL, 7);

-- --------------------------------------------------------

--
-- Table structure for table `mums_data_types`
--

CREATE TABLE `mums_data_types` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_singular` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name_plural` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `icon` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `model_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `policy_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `generate_permissions` tinyint(1) NOT NULL DEFAULT 0,
  `server_side` tinyint(4) NOT NULL DEFAULT 0,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_data_types`
--

INSERT INTO `mums_data_types` (`id`, `name`, `slug`, `display_name_singular`, `display_name_plural`, `icon`, `model_name`, `policy_name`, `controller`, `description`, `generate_permissions`, `server_side`, `details`, `created_at`, `updated_at`) VALUES
(1, 'users', 'users', 'User', 'Users', 'voyager-person', 'TCG\\Voyager\\Models\\User', 'TCG\\Voyager\\Policies\\UserPolicy', '', '', 1, 1, NULL, '2018-11-09 22:49:36', '2018-11-09 22:49:36'),
(2, 'menus', 'menus', 'Menu', 'Menus', 'voyager-list', 'TCG\\Voyager\\Models\\Menu', NULL, '', '', 1, 0, NULL, '2018-11-09 22:49:36', '2018-11-09 22:49:36'),
(3, 'roles', 'roles', 'Role', 'Roles', 'voyager-lock', 'TCG\\Voyager\\Models\\Role', NULL, '', '', 1, 0, NULL, '2018-11-09 22:49:36', '2018-11-09 22:49:36'),
(4, 'categories', 'categories', 'Category', 'Categories', 'voyager-categories', 'TCG\\Voyager\\Models\\Category', NULL, '', '', 1, 0, NULL, '2018-11-09 22:49:43', '2018-11-09 22:49:43'),
(5, 'posts', 'posts', 'Post', 'Posts', 'voyager-news', 'TCG\\Voyager\\Models\\Post', 'TCG\\Voyager\\Policies\\PostPolicy', '', '', 1, 0, NULL, '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(6, 'pages', 'pages', 'Page', 'Pages', 'voyager-file-text', 'TCG\\Voyager\\Models\\Page', NULL, '', '', 1, 0, NULL, '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(11, 'mums_chatter_discussion', 'mums-chatter-discussion', 'Forum Discussion', 'Forum Discussions', NULL, 'App\\Chatter_discussion', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-10 10:32:19', '2018-11-28 14:12:23'),
(13, 'mums_chatter_post', 'mums-chatter-post', 'Mums Chatter Post', 'Mums Chatter Posts', 'voyager-documentation', 'App\\Chatter_post', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-10 10:43:44', '2018-11-29 17:59:42'),
(16, 'mums_invoices', 'invoices', 'Invoice', 'Invoices', 'voyager-receipt', 'App\\Invoice', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-27 14:18:51', '2018-11-28 14:11:33'),
(19, 'mums_invoice_lines', 'invoice-lines', 'Invoice Line', 'Invoice Lines', 'voyager-window-list', 'App\\Invoice_line', NULL, NULL, 'This are the lines for each invoice created.', 1, 1, '{\"order_column\":\"item_no\",\"order_display_column\":\"item\"}', '2018-11-27 16:02:32', '2018-11-28 14:12:04'),
(21, 'mums_notices', 'notices', 'Notice', 'Notices', 'voyager-news', 'App\\Notice', NULL, NULL, 'The notices for different users', 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-29 17:58:13', '2018-11-30 00:06:16'),
(23, 'mums_school_calendar', 'calendar', 'School Calendar', 'School Calendars', 'voyager-calendar', 'App\\School_calendar', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-29 20:57:14', '2018-11-29 20:57:14'),
(24, 'mums_payments', 'payments', 'Payment', 'Payments', 'voyager-paypal', 'App\\Payment', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-30 00:33:46', '2018-11-30 00:33:46'),
(25, 'mums_time_table', 'time-table', 'Time Table', 'Time Tables', 'voyager-dashboard', 'App\\Time_table', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-30 00:50:46', '2018-11-30 14:50:34'),
(26, 'mums_admin_access', 'admin-access', 'Admin Access', 'Admin Accesses', 'voyager-anchor', 'App\\Admin_access', NULL, NULL, NULL, 1, 1, '{\"order_column\":null,\"order_display_column\":null}', '2018-11-30 01:05:55', '2018-11-30 14:52:00');

-- --------------------------------------------------------

--
-- Table structure for table `mums_guardian_student`
--

CREATE TABLE `mums_guardian_student` (
  `id` int(11) NOT NULL,
  `guardian_id` varchar(255) NOT NULL,
  `student_id` varchar(255) NOT NULL,
  `relationship` varchar(255) NOT NULL DEFAULT 'guardian',
  `updated_at` datetime DEFAULT current_timestamp(),
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mums_invoices`
--

CREATE TABLE `mums_invoices` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(191) DEFAULT NULL,
  `title` varchar(191) DEFAULT NULL,
  `type` enum('university','faculty','dept','level','student') DEFAULT NULL,
  `session` int(11) DEFAULT NULL,
  `target` varchar(191) DEFAULT NULL,
  `issuer` varchar(191) DEFAULT NULL,
  `gateway` varchar(191) DEFAULT NULL,
  `created_by` varchar(191) DEFAULT NULL,
  `status` enum('pending','published') DEFAULT 'pending',
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `created_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_invoices`
--

INSERT INTO `mums_invoices` (`id`, `invoice_id`, `title`, `type`, `session`, `target`, `issuer`, `gateway`, `created_by`, `status`, `updated_at`, `created_at`) VALUES
(1, 'university-2013--3', 'School Fees Invoice', 'university', 2013, '', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(2, 'faculty-2013-SOSC-4', 'Faculty Charges', 'faculty', 2013, 'SOSC', 'SOSC', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(3, 'faculty-2013-SMAT-5', 'Faculty Charges', 'faculty', 2013, 'SMAT', 'SMAT', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(4, 'faculty-2013-SEET-6', 'Faculty Charges', 'faculty', 2013, 'SEET', 'SEET', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(5, 'dept-2013-BIO-8', 'Departmental Charges', 'dept', 2013, 'BIO', 'BIO', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(6, 'dept-2013-IMT-9', 'Departmental Charges', 'dept', 2013, 'IMT', 'IMT', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(7, 'dept-2013-MEE-10', 'Departmental Charges', 'dept', 2013, 'MEE', 'MEE', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(8, 'dept-2013-CIE-11', 'Departmental Charges', 'dept', 2013, 'CIE', 'CIE', NULL, 'admin', 'pending', '2018-11-29 09:12:04', '2018-11-29 09:12:04'),
(9, 'dept-2013-PMT-12', 'Departmental Charges', 'dept', 2013, 'PMT', 'PMT', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(10, 'dept-2013-CHM-13', 'Departmental Charges', 'dept', 2013, 'CHM', 'CHM', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(11, 'dept-2013-CHE-14', 'Departmental Charges', 'dept', 2013, 'CHE', 'CHE', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(12, 'dept-2013-MTH-15', 'Departmental Charges', 'dept', 2013, 'MTH', 'MTH', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(13, 'dept-2013-PHY-16', 'Departmental Charges', 'dept', 2013, 'PHY', 'PHY', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(14, 'level-2013-100-18', '\'Level\' Charges', 'level', 2013, '100', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(15, 'level-2013-200-19', '\'Level\' Charges', 'level', 2013, '200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(16, 'level-2013-300-20', '\'Level\' Charges', 'level', 2013, '300', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(17, 'level-2013-400-21', '\'Level\' Charges', 'level', 2013, '400', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(18, 'level-2013-500-22', '\'Level\' Charges', 'level', 2013, '500', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(19, 'university-2014--26', 'School Fees Invoice', 'university', 2014, '', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:05', '2018-11-29 09:12:05'),
(20, 'faculty-2014-SOSC-27', 'Faculty Charges', 'faculty', 2014, 'SOSC', 'SOSC', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(21, 'faculty-2014-SMAT-28', 'Faculty Charges', 'faculty', 2014, 'SMAT', 'SMAT', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(22, 'faculty-2014-SEET-29', 'Faculty Charges', 'faculty', 2014, 'SEET', 'SEET', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(23, 'dept-2014-BIO-31', 'Departmental Charges', 'dept', 2014, 'BIO', 'BIO', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(24, 'dept-2014-IMT-32', 'Departmental Charges', 'dept', 2014, 'IMT', 'IMT', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(25, 'dept-2014-MEE-33', 'Departmental Charges', 'dept', 2014, 'MEE', 'MEE', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(26, 'dept-2014-CIE-34', 'Departmental Charges', 'dept', 2014, 'CIE', 'CIE', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(27, 'dept-2014-PMT-35', 'Departmental Charges', 'dept', 2014, 'PMT', 'PMT', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(28, 'dept-2014-CHM-36', 'Departmental Charges', 'dept', 2014, 'CHM', 'CHM', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(29, 'dept-2014-CHE-37', 'Departmental Charges', 'dept', 2014, 'CHE', 'CHE', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(30, 'dept-2014-MTH-38', 'Departmental Charges', 'dept', 2014, 'MTH', 'MTH', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(31, 'dept-2014-PHY-39', 'Departmental Charges', 'dept', 2014, 'PHY', 'PHY', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(32, 'level-2014-100-41', '\'Level\' Charges', 'level', 2014, '100', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:06', '2018-11-29 09:12:06'),
(33, 'level-2014-200-42', '\'Level\' Charges', 'level', 2014, '200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(34, 'level-2014-300-43', '\'Level\' Charges', 'level', 2014, '300', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(35, 'level-2014-400-44', '\'Level\' Charges', 'level', 2014, '400', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(36, 'level-2014-500-45', '\'Level\' Charges', 'level', 2014, '500', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(37, 'student-2014-20132035080-47', 'Student Charges for', 'student', 2014, '20132035080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(38, 'student-2014-20133055140-48', 'Student Charges for', 'student', 2014, '20133055140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(39, 'student-2014-20143055185-49', 'Student Charges for', 'student', 2014, '20143055185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(40, 'student-2014-20131030215-50', 'Student Charges for', 'student', 2014, '20131030215', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(41, 'student-2014-20133045110-51', 'Student Charges for', 'student', 2014, '20133045110', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(42, 'student-2014-20131025050-52', 'Student Charges for', 'student', 2014, '20131025050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:07', '2018-11-29 09:12:07'),
(43, 'student-2014-20133050020-53', 'Student Charges for', 'student', 2014, '20133050020', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(44, 'student-2014-20141015185-54', 'Student Charges for', 'student', 2014, '20141015185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(45, 'university-2015--57', 'School Fees Invoice', 'university', 2015, '', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(46, 'faculty-2015-SOSC-58', 'Faculty Charges', 'faculty', 2015, 'SOSC', 'SOSC', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(47, 'faculty-2015-SMAT-59', 'Faculty Charges', 'faculty', 2015, 'SMAT', 'SMAT', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(48, 'faculty-2015-SEET-60', 'Faculty Charges', 'faculty', 2015, 'SEET', 'SEET', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(49, 'dept-2015-BIO-62', 'Departmental Charges', 'dept', 2015, 'BIO', 'BIO', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(50, 'dept-2015-IMT-63', 'Departmental Charges', 'dept', 2015, 'IMT', 'IMT', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(51, 'dept-2015-MEE-64', 'Departmental Charges', 'dept', 2015, 'MEE', 'MEE', NULL, 'admin', 'pending', '2018-11-29 09:12:08', '2018-11-29 09:12:08'),
(52, 'dept-2015-CIE-65', 'Departmental Charges', 'dept', 2015, 'CIE', 'CIE', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(53, 'dept-2015-PMT-66', 'Departmental Charges', 'dept', 2015, 'PMT', 'PMT', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(54, 'dept-2015-CHM-67', 'Departmental Charges', 'dept', 2015, 'CHM', 'CHM', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(55, 'dept-2015-CHE-68', 'Departmental Charges', 'dept', 2015, 'CHE', 'CHE', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(56, 'dept-2015-MTH-69', 'Departmental Charges', 'dept', 2015, 'MTH', 'MTH', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(57, 'dept-2015-PHY-70', 'Departmental Charges', 'dept', 2015, 'PHY', 'PHY', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(58, 'level-2015-100-72', '\'Level\' Charges', 'level', 2015, '100', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(59, 'level-2015-200-73', '\'Level\' Charges', 'level', 2015, '200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(60, 'level-2015-300-74', '\'Level\' Charges', 'level', 2015, '300', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(61, 'level-2015-400-75', '\'Level\' Charges', 'level', 2015, '400', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(62, 'level-2015-500-76', '\'Level\' Charges', 'level', 2015, '500', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(63, 'student-2015-20131020125-78', 'Student Charges for', 'student', 2015, '20131020125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:09', '2018-11-29 09:12:09'),
(64, 'student-2015-20143055185-79', 'Student Charges for', 'student', 2015, '20143055185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(65, 'student-2015-20153055005-80', 'Student Charges for', 'student', 2015, '20153055005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(66, 'student-2015-20132040005-81', 'Student Charges for', 'student', 2015, '20132040005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(67, 'student-2015-20153055275-82', 'Student Charges for', 'student', 2015, '20153055275', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(68, 'student-2015-20153045245-83', 'Student Charges for', 'student', 2015, '20153045245', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(69, 'student-2015-20133055095-84', 'Student Charges for', 'student', 2015, '20133055095', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(70, 'student-2015-20152035005-85', 'Student Charges for', 'student', 2015, '20152035005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(71, 'student-2015-20151025155-86', 'Student Charges for', 'student', 2015, '20151025155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(72, 'student-2015-20153055050-87', 'Student Charges for', 'student', 2015, '20153055050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(73, 'student-2015-20141025050-88', 'Student Charges for', 'student', 2015, '20141025050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(74, 'student-2015-20143045005-89', 'Student Charges for', 'student', 2015, '20143045005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:10', '2018-11-29 09:12:10'),
(75, 'student-2015-20133050125-90', 'Student Charges for', 'student', 2015, '20133050125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(76, 'student-2015-20141015185-91', 'Student Charges for', 'student', 2015, '20141015185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(77, 'student-2015-20132040015-92', 'Student Charges for', 'student', 2015, '20132040015', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(78, 'student-2015-20132040010-93', 'Student Charges for', 'student', 2015, '20132040010', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(79, 'university-2016--96', 'School Fees Invoice', 'university', 2016, '', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(80, 'faculty-2016-SOSC-97', 'Faculty Charges', 'faculty', 2016, 'SOSC', 'SOSC', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(81, 'faculty-2016-SMAT-98', 'Faculty Charges', 'faculty', 2016, 'SMAT', 'SMAT', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(82, 'faculty-2016-SEET-99', 'Faculty Charges', 'faculty', 2016, 'SEET', 'SEET', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(83, 'dept-2016-BIO-101', 'Departmental Charges', 'dept', 2016, 'BIO', 'BIO', NULL, 'admin', 'pending', '2018-11-29 09:12:11', '2018-11-29 09:12:11'),
(84, 'dept-2016-IMT-102', 'Departmental Charges', 'dept', 2016, 'IMT', 'IMT', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(85, 'dept-2016-MEE-103', 'Departmental Charges', 'dept', 2016, 'MEE', 'MEE', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(86, 'dept-2016-CIE-104', 'Departmental Charges', 'dept', 2016, 'CIE', 'CIE', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(87, 'dept-2016-PMT-105', 'Departmental Charges', 'dept', 2016, 'PMT', 'PMT', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(88, 'dept-2016-CHM-106', 'Departmental Charges', 'dept', 2016, 'CHM', 'CHM', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(89, 'dept-2016-CHE-107', 'Departmental Charges', 'dept', 2016, 'CHE', 'CHE', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(90, 'dept-2016-MTH-108', 'Departmental Charges', 'dept', 2016, 'MTH', 'MTH', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(91, 'dept-2016-PHY-109', 'Departmental Charges', 'dept', 2016, 'PHY', 'PHY', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(92, 'level-2016-100-111', '\'Level\' Charges', 'level', 2016, '100', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(93, 'level-2016-200-112', '\'Level\' Charges', 'level', 2016, '200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(94, 'level-2016-300-113', '\'Level\' Charges', 'level', 2016, '300', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(95, 'level-2016-400-114', '\'Level\' Charges', 'level', 2016, '400', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(96, 'level-2016-500-115', '\'Level\' Charges', 'level', 2016, '500', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:12', '2018-11-29 09:12:12'),
(97, 'student-2016-20153055125-117', 'Student Charges for', 'student', 2016, '20153055125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(98, 'student-2016-20141025050-118', 'Student Charges for', 'student', 2016, '20141025050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(99, 'student-2016-20132035080-119', 'Student Charges for', 'student', 2016, '20132035080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(100, 'student-2016-20133050125-120', 'Student Charges for', 'student', 2016, '20133050125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(101, 'student-2016-20161030095-121', 'Student Charges for', 'student', 2016, '20161030095', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(102, 'student-2016-20143055125-122', 'Student Charges for', 'student', 2016, '20143055125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(103, 'student-2016-20153045110-123', 'Student Charges for', 'student', 2016, '20153045110', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(104, 'student-2016-20141030200-124', 'Student Charges for', 'student', 2016, '20141030200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(105, 'student-2016-20153050200-125', 'Student Charges for', 'student', 2016, '20153050200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(106, 'student-2016-20163045200-126', 'Student Charges for', 'student', 2016, '20163045200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:13', '2018-11-29 09:12:13'),
(107, 'student-2016-20151015050-127', 'Student Charges for', 'student', 2016, '20151015050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(108, 'student-2016-20131020125-128', 'Student Charges for', 'student', 2016, '20131020125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(109, 'student-2016-20161015140-129', 'Student Charges for', 'student', 2016, '20161015140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(110, 'student-2016-20161015080-130', 'Student Charges for', 'student', 2016, '20161015080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(111, 'student-2016-20151020170-131', 'Student Charges for', 'student', 2016, '20151020170', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(112, 'student-2016-20161015110-132', 'Student Charges for', 'student', 2016, '20161015110', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(113, 'student-2016-20142035065-133', 'Student Charges for', 'student', 2016, '20142035065', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(114, 'student-2016-20153055275-134', 'Student Charges for', 'student', 2016, '20153055275', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(115, 'student-2016-20142035020-135', 'Student Charges for', 'student', 2016, '20142035020', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(116, 'student-2016-20161025155-136', 'Student Charges for', 'student', 2016, '20161025155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(117, 'student-2016-20141025155-137', 'Student Charges for', 'student', 2016, '20141025155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:14', '2018-11-29 09:12:14'),
(118, 'student-2016-20161020170-138', 'Student Charges for', 'student', 2016, '20161020170', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(119, 'student-2016-20131025140-139', 'Student Charges for', 'student', 2016, '20131025140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(120, 'student-2016-20151020140-140', 'Student Charges for', 'student', 2016, '20151020140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(121, 'university-2017--143', 'School Fees Invoice', 'university', 2017, '', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(122, 'faculty-2017-SOSC-144', 'Faculty Charges', 'faculty', 2017, 'SOSC', 'SOSC', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(123, 'faculty-2017-SMAT-145', 'Faculty Charges', 'faculty', 2017, 'SMAT', 'SMAT', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(124, 'faculty-2017-SEET-146', 'Faculty Charges', 'faculty', 2017, 'SEET', 'SEET', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(125, 'dept-2017-BIO-148', 'Departmental Charges', 'dept', 2017, 'BIO', 'BIO', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(126, 'dept-2017-IMT-149', 'Departmental Charges', 'dept', 2017, 'IMT', 'IMT', NULL, 'admin', 'pending', '2018-11-29 09:12:15', '2018-11-29 09:12:15'),
(127, 'dept-2017-MEE-150', 'Departmental Charges', 'dept', 2017, 'MEE', 'MEE', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(128, 'dept-2017-CIE-151', 'Departmental Charges', 'dept', 2017, 'CIE', 'CIE', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(129, 'dept-2017-PMT-152', 'Departmental Charges', 'dept', 2017, 'PMT', 'PMT', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(130, 'dept-2017-CHM-153', 'Departmental Charges', 'dept', 2017, 'CHM', 'CHM', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(131, 'dept-2017-CHE-154', 'Departmental Charges', 'dept', 2017, 'CHE', 'CHE', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(132, 'dept-2017-MTH-155', 'Departmental Charges', 'dept', 2017, 'MTH', 'MTH', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(133, 'dept-2017-PHY-156', 'Departmental Charges', 'dept', 2017, 'PHY', 'PHY', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(134, 'level-2017-100-158', '\'Level\' Charges', 'level', 2017, '100', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(135, 'level-2017-200-159', '\'Level\' Charges', 'level', 2017, '200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(136, 'level-2017-300-160', '\'Level\' Charges', 'level', 2017, '300', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(137, 'level-2017-400-161', '\'Level\' Charges', 'level', 2017, '400', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(138, 'level-2017-500-162', '\'Level\' Charges', 'level', 2017, '500', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(139, 'student-2017-20171030230-164', 'Student Charges for', 'student', 2017, '20171030230', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:16', '2018-11-29 09:12:16'),
(140, 'student-2017-20161020125-165', 'Student Charges for', 'student', 2017, '20161020125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(141, 'student-2017-20162035080-166', 'Student Charges for', 'student', 2017, '20162035080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(142, 'student-2017-20141030200-167', 'Student Charges for', 'student', 2017, '20141030200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(143, 'student-2017-20173045230-168', 'Student Charges for', 'student', 2017, '20173045230', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(144, 'student-2017-20163045125-169', 'Student Charges for', 'student', 2017, '20163045125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(145, 'student-2017-20131015070-170', 'Student Charges for', 'student', 2017, '20131015070', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(146, 'student-2017-20143055155-171', 'Student Charges for', 'student', 2017, '20143055155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(147, 'student-2017-20173045125-172', 'Student Charges for', 'student', 2017, '20173045125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(148, 'student-2017-20143045005-173', 'Student Charges for', 'student', 2017, '20143045005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(149, 'student-2017-20171030095-174', 'Student Charges for', 'student', 2017, '20171030095', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(150, 'student-2017-20132035050-175', 'Student Charges for', 'student', 2017, '20132035050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:17', '2018-11-29 09:12:17'),
(151, 'student-2017-20163055185-176', 'Student Charges for', 'student', 2017, '20163055185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(152, 'student-2017-20153045320-177', 'Student Charges for', 'student', 2017, '20153045320', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(153, 'student-2017-20141025155-178', 'Student Charges for', 'student', 2017, '20141025155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(154, 'student-2017-20163050140-179', 'Student Charges for', 'student', 2017, '20163050140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(155, 'student-2017-20163050035-180', 'Student Charges for', 'student', 2017, '20163050035', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(156, 'student-2017-20133055140-181', 'Student Charges for', 'student', 2017, '20133055140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(157, 'student-2017-20141030125-182', 'Student Charges for', 'student', 2017, '20141030125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(158, 'student-2017-20163050215-183', 'Student Charges for', 'student', 2017, '20163050215', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(159, 'student-2017-20131015155-184', 'Student Charges for', 'student', 2017, '20131015155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(160, 'student-2017-20133055095-185', 'Student Charges for', 'student', 2017, '20133055095', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:18', '2018-11-29 09:12:18'),
(161, 'student-2017-20141030005-186', 'Student Charges for', 'student', 2017, '20141030005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(162, 'student-2017-20143055185-187', 'Student Charges for', 'student', 2017, '20143055185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(163, 'student-2017-20143055095-188', 'Student Charges for', 'student', 2017, '20143055095', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(164, 'student-2017-20143050035-189', 'Student Charges for', 'student', 2017, '20143050035', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(165, 'student-2017-20163055155-190', 'Student Charges for', 'student', 2017, '20163055155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(166, 'student-2017-20172035080-191', 'Student Charges for', 'student', 2017, '20172035080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(167, 'student-2017-20173050215-192', 'Student Charges for', 'student', 2017, '20173050215', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(168, 'student-2017-20163045065-193', 'Student Charges for', 'student', 2017, '20163045065', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(169, 'student-2017-20133050020-194', 'Student Charges for', 'student', 2017, '20133050020', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(170, 'student-2017-20171015140-195', 'Student Charges for', 'student', 2017, '20171015140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(171, 'university-2018--198', 'School Fees Invoice', 'university', 2018, '', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:19', '2018-11-29 09:12:19'),
(172, 'faculty-2018-SOSC-199', 'Faculty Charges', 'faculty', 2018, 'SOSC', 'SOSC', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(173, 'faculty-2018-SMAT-200', 'Faculty Charges', 'faculty', 2018, 'SMAT', 'SMAT', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(174, 'faculty-2018-SEET-201', 'Faculty Charges', 'faculty', 2018, 'SEET', 'SEET', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(175, 'dept-2018-BIO-203', 'Departmental Charges', 'dept', 2018, 'BIO', 'BIO', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(176, 'dept-2018-IMT-204', 'Departmental Charges', 'dept', 2018, 'IMT', 'IMT', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(177, 'dept-2018-MEE-205', 'Departmental Charges', 'dept', 2018, 'MEE', 'MEE', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(178, 'dept-2018-CIE-206', 'Departmental Charges', 'dept', 2018, 'CIE', 'CIE', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(179, 'dept-2018-PMT-207', 'Departmental Charges', 'dept', 2018, 'PMT', 'PMT', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(180, 'dept-2018-CHM-208', 'Departmental Charges', 'dept', 2018, 'CHM', 'CHM', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(181, 'dept-2018-CHE-209', 'Departmental Charges', 'dept', 2018, 'CHE', 'CHE', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(182, 'dept-2018-MTH-210', 'Departmental Charges', 'dept', 2018, 'MTH', 'MTH', NULL, 'admin', 'pending', '2018-11-29 09:12:20', '2018-11-29 09:12:20'),
(183, 'dept-2018-PHY-211', 'Departmental Charges', 'dept', 2018, 'PHY', 'PHY', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(184, 'level-2018-100-213', '\'Level\' Charges', 'level', 2018, '100', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(185, 'level-2018-200-214', '\'Level\' Charges', 'level', 2018, '200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(186, 'level-2018-300-215', '\'Level\' Charges', 'level', 2018, '300', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(187, 'level-2018-400-216', '\'Level\' Charges', 'level', 2018, '400', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(188, 'level-2018-500-217', '\'Level\' Charges', 'level', 2018, '500', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(189, 'student-2018-20163045050-219', 'Student Charges for', 'student', 2018, '20163045050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(190, 'student-2018-20171020050-220', 'Student Charges for', 'student', 2018, '20171020050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(191, 'student-2018-20133050200-221', 'Student Charges for', 'student', 2018, '20133050200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(192, 'student-2018-20163050080-222', 'Student Charges for', 'student', 2018, '20163050080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(193, 'student-2018-20173045125-223', 'Student Charges for', 'student', 2018, '20173045125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:21', '2018-11-29 09:12:21'),
(194, 'student-2018-20183045050-224', 'Student Charges for', 'student', 2018, '20183045050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(195, 'student-2018-20143055155-225', 'Student Charges for', 'student', 2018, '20143055155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(196, 'student-2018-20143050065-226', 'Student Charges for', 'student', 2018, '20143050065', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(197, 'student-2018-20131015070-227', 'Student Charges for', 'student', 2018, '20131015070', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(198, 'student-2018-20161020050-228', 'Student Charges for', 'student', 2018, '20161020050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(199, 'student-2018-20131030085-229', 'Student Charges for', 'student', 2018, '20131030085', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(200, 'student-2018-20181020050-230', 'Student Charges for', 'student', 2018, '20181020050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(201, 'student-2018-20171020200-231', 'Student Charges for', 'student', 2018, '20171020200', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:22', '2018-11-29 09:12:22'),
(202, 'student-2018-20161020185-232', 'Student Charges for', 'student', 2018, '20161020185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(203, 'student-2018-20153050065-233', 'Student Charges for', 'student', 2018, '20153050065', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(204, 'student-2018-20172040125-234', 'Student Charges for', 'student', 2018, '20172040125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(205, 'student-2018-20133055005-235', 'Student Charges for', 'student', 2018, '20133055005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(206, 'student-2018-20172040110-236', 'Student Charges for', 'student', 2018, '20172040110', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(207, 'student-2018-20151025005-237', 'Student Charges for', 'student', 2018, '20151025005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(208, 'student-2018-20133045170-238', 'Student Charges for', 'student', 2018, '20133045170', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(209, 'student-2018-20173045095-239', 'Student Charges for', 'student', 2018, '20173045095', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(210, 'student-2018-20183045065-240', 'Student Charges for', 'student', 2018, '20183045065', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(211, 'student-2018-20181020185-241', 'Student Charges for', 'student', 2018, '20181020185', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(212, 'student-2018-20182040035-242', 'Student Charges for', 'student', 2018, '20182040035', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:23', '2018-11-29 09:12:23'),
(213, 'student-2018-20143045140-243', 'Student Charges for', 'student', 2018, '20143045140', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(214, 'student-2018-20182035080-244', 'Student Charges for', 'student', 2018, '20182035080', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(215, 'student-2018-20153045260-245', 'Student Charges for', 'student', 2018, '20153045260', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(216, 'student-2018-20152035020-246', 'Student Charges for', 'student', 2018, '20152035020', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(217, 'student-2018-20142035065-247', 'Student Charges for', 'student', 2018, '20142035065', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(218, 'student-2018-20133055050-248', 'Student Charges for', 'student', 2018, '20133055050', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(219, 'student-2018-20132040015-249', 'Student Charges for', 'student', 2018, '20132040015', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(220, 'student-2018-20153055005-250', 'Student Charges for', 'student', 2018, '20153055005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(221, 'student-2018-20152035005-251', 'Student Charges for', 'student', 2018, '20152035005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(222, 'student-2018-20132040010-252', 'Student Charges for', 'student', 2018, '20132040010', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(223, 'student-2018-20173055005-253', 'Student Charges for', 'student', 2018, '20173055005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:24', '2018-11-29 09:12:24'),
(224, 'student-2018-20181025155-254', 'Student Charges for', 'student', 2018, '20181025155', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:25', '2018-11-29 09:12:25'),
(225, 'student-2018-20161030230-255', 'Student Charges for', 'student', 2018, '20161030230', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:25', '2018-11-29 09:12:25'),
(226, 'student-2018-20162040005-256', 'Student Charges for', 'student', 2018, '20162040005', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:25', '2018-11-29 09:12:25'),
(227, 'student-2018-20162040125-257', 'Student Charges for', 'student', 2018, '20162040125', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:25', '2018-11-29 09:12:25'),
(228, 'student-2018-20163055020-258', 'Student Charges for', 'student', 2018, '20163055020', 'Bursary', NULL, 'admin', 'pending', '2018-11-29 09:12:25', '2018-11-29 09:12:25');

-- --------------------------------------------------------

--
-- Table structure for table `mums_invoice_lines`
--

CREATE TABLE `mums_invoice_lines` (
  `id` int(11) NOT NULL,
  `invoice_id` varchar(191) DEFAULT NULL,
  `item` varchar(191) DEFAULT NULL,
  `amount` int(11) DEFAULT NULL,
  `unit` int(11) NOT NULL DEFAULT 1,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `item_no` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_invoice_lines`
--

INSERT INTO `mums_invoice_lines` (`id`, `invoice_id`, `item`, `amount`, `unit`, `created_at`, `updated_at`, `item_no`) VALUES
(1, 'university-2013--3', 'Student Health Insurance/Medical', 4500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(2, 'university-2013--3', 'Sport Fees', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(3, 'university-2013--3', 'Examination', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(4, 'university-2013--3', 'Registration', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(5, 'university-2013--3', 'Library Fees', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(6, 'university-2013--3', 'Laboratory Fees', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(7, 'university-2013--3', 'Municipal Charge', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(8, 'university-2013--3', 'Information Com. Tech. Fees', 8000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(9, 'university-2013--3', 'Student Union Dues', 800, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(10, 'university-2013--3', 'Accreditation Fee', 3000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(11, 'university-2013--3', 'PMF Levy', 5000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(12, 'university-2013--3', 'Development Levy', 5000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(13, 'faculty-2013-SOSC-4', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(14, 'faculty-2013-SOSC-4', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(15, 'faculty-2013-SOSC-4', 'Transportation', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(16, 'faculty-2013-SMAT-5', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(17, 'faculty-2013-SMAT-5', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(18, 'faculty-2013-SMAT-5', 'Transportation', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(19, 'faculty-2013-SEET-6', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(20, 'faculty-2013-SEET-6', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(21, 'faculty-2013-SEET-6', 'Transportation', 1000, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(22, 'dept-2013-BIO-8', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(23, 'dept-2013-BIO-8', 'BIO National Conference Fee', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(24, 'dept-2013-IMT-9', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(25, 'dept-2013-IMT-9', 'IMT National Conference Fee', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(26, 'dept-2013-MEE-10', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(27, 'dept-2013-MEE-10', 'MEE National Conference Fee', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(28, 'dept-2013-CIE-11', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:04', '2018-11-29 09:12:04', NULL),
(29, 'dept-2013-CIE-11', 'CIE National Conference Fee', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(30, 'dept-2013-PMT-12', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(31, 'dept-2013-PMT-12', 'PMT National Conference Fee', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(32, 'dept-2013-CHM-13', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(33, 'dept-2013-CHM-13', 'CHM National Conference Fee', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(34, 'dept-2013-CHE-14', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(35, 'dept-2013-CHE-14', 'CHE National Conference Fee', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(36, 'dept-2013-MTH-15', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(37, 'dept-2013-MTH-15', 'MTH National Conference Fee', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(38, 'dept-2013-PHY-16', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(39, 'dept-2013-PHY-16', 'PHY National Conference Fee', 1500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(40, 'level-2013-100-18', 'Acceptance Fee', 25000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(41, 'level-2013-100-18', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(42, 'level-2013-100-18', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(43, 'level-2013-100-18', 'Matriculation', 8000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(44, 'level-2013-100-18', 'Books', 18000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(45, 'level-2013-200-19', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(46, 'level-2013-200-19', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(47, 'level-2013-200-19', 'Books', 13000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(48, 'level-2013-300-20', 'Laboratory Fee', 2000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(49, 'level-2013-300-20', 'Books', 10000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(50, 'level-2013-400-21', 'Industial Attachment', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(51, 'level-2013-400-21', 'Books', 5000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(52, 'level-2013-500-22', 'Final Project', 10000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(53, 'level-2013-500-22', 'Convocation', 8000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(54, 'level-2013-500-22', 'Books', 8000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(55, 'university-2014--26', 'Student Health Insurance/Medical', 4500, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(56, 'university-2014--26', 'Sport Fees', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(57, 'university-2014--26', 'Examination', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(58, 'university-2014--26', 'Registration', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(59, 'university-2014--26', 'Library Fees', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(60, 'university-2014--26', 'Laboratory Fees', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(61, 'university-2014--26', 'Municipal Charge', 1000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(62, 'university-2014--26', 'Information Com. Tech. Fees', 8000, 1, '2018-11-29 09:12:05', '2018-11-29 09:12:05', NULL),
(63, 'university-2014--26', 'Student Union Dues', 800, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(64, 'university-2014--26', 'Accreditation Fee', 3000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(65, 'university-2014--26', 'PMF Levy', 5000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(66, 'university-2014--26', 'Development Levy', 5000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(67, 'faculty-2014-SOSC-27', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(68, 'faculty-2014-SOSC-27', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(69, 'faculty-2014-SOSC-27', 'Transportation', 1000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(70, 'faculty-2014-SMAT-28', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(71, 'faculty-2014-SMAT-28', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(72, 'faculty-2014-SMAT-28', 'Transportation', 1000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(73, 'faculty-2014-SEET-29', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(74, 'faculty-2014-SEET-29', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(75, 'faculty-2014-SEET-29', 'Transportation', 1000, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(76, 'dept-2014-BIO-31', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(77, 'dept-2014-BIO-31', 'BIO National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(78, 'dept-2014-IMT-32', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(79, 'dept-2014-IMT-32', 'IMT National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(80, 'dept-2014-MEE-33', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(81, 'dept-2014-MEE-33', 'MEE National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(82, 'dept-2014-CIE-34', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(83, 'dept-2014-CIE-34', 'CIE National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(84, 'dept-2014-PMT-35', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(85, 'dept-2014-PMT-35', 'PMT National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(86, 'dept-2014-CHM-36', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(87, 'dept-2014-CHM-36', 'CHM National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(88, 'dept-2014-CHE-37', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(89, 'dept-2014-CHE-37', 'CHE National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(90, 'dept-2014-MTH-38', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(91, 'dept-2014-MTH-38', 'MTH National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(92, 'dept-2014-PHY-39', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(93, 'dept-2014-PHY-39', 'PHY National Conference Fee', 1500, 1, '2018-11-29 09:12:06', '2018-11-29 09:12:06', NULL),
(94, 'level-2014-100-41', 'Acceptance Fee', 25000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(95, 'level-2014-100-41', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(96, 'level-2014-100-41', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(97, 'level-2014-100-41', 'Matriculation', 8000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(98, 'level-2014-100-41', 'Books', 18000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(99, 'level-2014-200-42', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(100, 'level-2014-200-42', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(101, 'level-2014-200-42', 'Books', 13000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(102, 'level-2014-300-43', 'Laboratory Fee', 2000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(103, 'level-2014-300-43', 'Books', 10000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(104, 'level-2014-400-44', 'Industial Attachment', 1000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(105, 'level-2014-400-44', 'Books', 5000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(106, 'level-2014-500-45', 'Final Project', 10000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(107, 'level-2014-500-45', 'Convocation', 8000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(108, 'level-2014-500-45', 'Books', 8000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(109, 'student-2014-20132035080-47', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(110, 'student-2014-20132035080-47', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(111, 'student-2014-20132035080-47', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(112, 'student-2014-20133055140-48', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(113, 'student-2014-20133055140-48', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(114, 'student-2014-20133055140-48', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(115, 'student-2014-20143055185-49', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(116, 'student-2014-20143055185-49', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(117, 'student-2014-20143055185-49', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(118, 'student-2014-20131030215-50', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(119, 'student-2014-20131030215-50', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(120, 'student-2014-20131030215-50', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(121, 'student-2014-20133045110-51', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(122, 'student-2014-20133045110-51', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(123, 'student-2014-20133045110-51', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(124, 'student-2014-20131025050-52', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(125, 'student-2014-20131025050-52', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(126, 'student-2014-20131025050-52', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:07', '2018-11-29 09:12:07', NULL),
(127, 'student-2014-20133050020-53', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(128, 'student-2014-20133050020-53', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(129, 'student-2014-20133050020-53', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(130, 'student-2014-20141015185-54', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(131, 'student-2014-20141015185-54', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(132, 'student-2014-20141015185-54', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(133, 'university-2015--57', 'Student Health Insurance/Medical', 4500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(134, 'university-2015--57', 'Sport Fees', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(135, 'university-2015--57', 'Examination', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(136, 'university-2015--57', 'Registration', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(137, 'university-2015--57', 'Library Fees', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(138, 'university-2015--57', 'Laboratory Fees', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(139, 'university-2015--57', 'Municipal Charge', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(140, 'university-2015--57', 'Information Com. Tech. Fees', 8000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(141, 'university-2015--57', 'Student Union Dues', 800, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(142, 'university-2015--57', 'Accreditation Fee', 3000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(143, 'university-2015--57', 'PMF Levy', 5000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(144, 'university-2015--57', 'Development Levy', 5000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(145, 'faculty-2015-SOSC-58', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(146, 'faculty-2015-SOSC-58', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(147, 'faculty-2015-SOSC-58', 'Transportation', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(148, 'faculty-2015-SMAT-59', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(149, 'faculty-2015-SMAT-59', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(150, 'faculty-2015-SMAT-59', 'Transportation', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(151, 'faculty-2015-SEET-60', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(152, 'faculty-2015-SEET-60', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(153, 'faculty-2015-SEET-60', 'Transportation', 1000, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(154, 'dept-2015-BIO-62', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(155, 'dept-2015-BIO-62', 'BIO National Conference Fee', 1500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(156, 'dept-2015-IMT-63', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(157, 'dept-2015-IMT-63', 'IMT National Conference Fee', 1500, 1, '2018-11-29 09:12:08', '2018-11-29 09:12:08', NULL),
(158, 'dept-2015-MEE-64', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(159, 'dept-2015-MEE-64', 'MEE National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(160, 'dept-2015-CIE-65', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(161, 'dept-2015-CIE-65', 'CIE National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(162, 'dept-2015-PMT-66', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(163, 'dept-2015-PMT-66', 'PMT National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(164, 'dept-2015-CHM-67', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(165, 'dept-2015-CHM-67', 'CHM National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(166, 'dept-2015-CHE-68', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(167, 'dept-2015-CHE-68', 'CHE National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(168, 'dept-2015-MTH-69', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(169, 'dept-2015-MTH-69', 'MTH National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(170, 'dept-2015-PHY-70', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(171, 'dept-2015-PHY-70', 'PHY National Conference Fee', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(172, 'level-2015-100-72', 'Acceptance Fee', 25000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(173, 'level-2015-100-72', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(174, 'level-2015-100-72', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(175, 'level-2015-100-72', 'Matriculation', 8000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(176, 'level-2015-100-72', 'Books', 18000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(177, 'level-2015-200-73', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(178, 'level-2015-200-73', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(179, 'level-2015-200-73', 'Books', 13000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(180, 'level-2015-300-74', 'Laboratory Fee', 2000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(181, 'level-2015-300-74', 'Books', 10000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(182, 'level-2015-400-75', 'Industial Attachment', 1000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(183, 'level-2015-400-75', 'Books', 5000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(184, 'level-2015-500-76', 'Final Project', 10000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(185, 'level-2015-500-76', 'Convocation', 8000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(186, 'level-2015-500-76', 'Books', 8000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(187, 'student-2015-20131020125-78', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(188, 'student-2015-20131020125-78', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:09', '2018-11-29 09:12:09', NULL),
(189, 'student-2015-20131020125-78', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(190, 'student-2015-20143055185-79', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(191, 'student-2015-20143055185-79', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(192, 'student-2015-20143055185-79', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(193, 'student-2015-20153055005-80', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(194, 'student-2015-20153055005-80', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(195, 'student-2015-20153055005-80', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(196, 'student-2015-20132040005-81', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(197, 'student-2015-20132040005-81', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(198, 'student-2015-20132040005-81', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(199, 'student-2015-20153055275-82', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(200, 'student-2015-20153055275-82', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(201, 'student-2015-20153055275-82', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(202, 'student-2015-20153045245-83', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(203, 'student-2015-20153045245-83', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(204, 'student-2015-20153045245-83', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(205, 'student-2015-20133055095-84', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(206, 'student-2015-20133055095-84', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(207, 'student-2015-20152035005-85', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(208, 'student-2015-20152035005-85', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(209, 'student-2015-20152035005-85', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(210, 'student-2015-20151025155-86', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(211, 'student-2015-20151025155-86', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(212, 'student-2015-20151025155-86', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(213, 'student-2015-20153055050-87', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(214, 'student-2015-20153055050-87', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(215, 'student-2015-20153055050-87', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(216, 'student-2015-20141025050-88', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(217, 'student-2015-20141025050-88', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(218, 'student-2015-20141025050-88', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(219, 'student-2015-20143045005-89', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(220, 'student-2015-20143045005-89', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(221, 'student-2015-20143045005-89', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:10', '2018-11-29 09:12:10', NULL),
(222, 'student-2015-20133050125-90', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(223, 'student-2015-20133050125-90', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(224, 'student-2015-20133050125-90', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(225, 'student-2015-20141015185-91', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(226, 'student-2015-20141015185-91', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(227, 'student-2015-20141015185-91', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(228, 'student-2015-20132040015-92', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(229, 'student-2015-20132040015-92', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(230, 'student-2015-20132040015-92', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(231, 'student-2015-20132040010-93', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(232, 'student-2015-20132040010-93', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(233, 'student-2015-20132040010-93', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(234, 'university-2016--96', 'Student Health Insurance/Medical', 4500, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(235, 'university-2016--96', 'Sport Fees', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(236, 'university-2016--96', 'Examination', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(237, 'university-2016--96', 'Registration', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(238, 'university-2016--96', 'Library Fees', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(239, 'university-2016--96', 'Laboratory Fees', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(240, 'university-2016--96', 'Municipal Charge', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(241, 'university-2016--96', 'Information Com. Tech. Fees', 8000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(242, 'university-2016--96', 'Student Union Dues', 800, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(243, 'university-2016--96', 'Accreditation Fee', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(244, 'university-2016--96', 'PMF Levy', 5000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(245, 'university-2016--96', 'Development Levy', 5000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(246, 'faculty-2016-SOSC-97', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(247, 'faculty-2016-SOSC-97', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(248, 'faculty-2016-SOSC-97', 'Transportation', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(249, 'faculty-2016-SMAT-98', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(250, 'faculty-2016-SMAT-98', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(251, 'faculty-2016-SMAT-98', 'Transportation', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(252, 'faculty-2016-SEET-99', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(253, 'faculty-2016-SEET-99', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(254, 'faculty-2016-SEET-99', 'Transportation', 1000, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(255, 'dept-2016-BIO-101', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:11', '2018-11-29 09:12:11', NULL),
(256, 'dept-2016-BIO-101', 'BIO National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(257, 'dept-2016-IMT-102', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(258, 'dept-2016-IMT-102', 'IMT National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(259, 'dept-2016-MEE-103', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(260, 'dept-2016-MEE-103', 'MEE National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(261, 'dept-2016-CIE-104', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(262, 'dept-2016-CIE-104', 'CIE National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(263, 'dept-2016-PMT-105', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(264, 'dept-2016-PMT-105', 'PMT National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(265, 'dept-2016-CHM-106', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(266, 'dept-2016-CHM-106', 'CHM National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(267, 'dept-2016-CHE-107', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(268, 'dept-2016-CHE-107', 'CHE National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(269, 'dept-2016-MTH-108', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(270, 'dept-2016-MTH-108', 'MTH National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(271, 'dept-2016-PHY-109', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(272, 'dept-2016-PHY-109', 'PHY National Conference Fee', 1500, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(273, 'level-2016-100-111', 'Acceptance Fee', 25000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(274, 'level-2016-100-111', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(275, 'level-2016-100-111', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(276, 'level-2016-100-111', 'Matriculation', 8000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(277, 'level-2016-100-111', 'Books', 18000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(278, 'level-2016-200-112', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(279, 'level-2016-200-112', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(280, 'level-2016-200-112', 'Books', 13000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(281, 'level-2016-300-113', 'Laboratory Fee', 2000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(282, 'level-2016-300-113', 'Books', 10000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(283, 'level-2016-400-114', 'Industial Attachment', 1000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(284, 'level-2016-400-114', 'Books', 5000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(285, 'level-2016-500-115', 'Final Project', 10000, 1, '2018-11-29 09:12:12', '2018-11-29 09:12:12', NULL),
(286, 'level-2016-500-115', 'Convocation', 8000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(287, 'level-2016-500-115', 'Books', 8000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(288, 'student-2016-20153055125-117', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(289, 'student-2016-20153055125-117', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(290, 'student-2016-20141025050-118', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(291, 'student-2016-20141025050-118', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(292, 'student-2016-20141025050-118', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(293, 'student-2016-20132035080-119', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(294, 'student-2016-20132035080-119', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(295, 'student-2016-20132035080-119', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(296, 'student-2016-20133050125-120', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(297, 'student-2016-20133050125-120', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(298, 'student-2016-20133050125-120', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(299, 'student-2016-20161030095-121', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(300, 'student-2016-20161030095-121', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(301, 'student-2016-20161030095-121', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(302, 'student-2016-20143055125-122', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(303, 'student-2016-20143055125-122', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(304, 'student-2016-20143055125-122', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(305, 'student-2016-20153045110-123', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(306, 'student-2016-20153045110-123', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(307, 'student-2016-20153045110-123', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(308, 'student-2016-20141030200-124', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(309, 'student-2016-20141030200-124', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(310, 'student-2016-20141030200-124', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(311, 'student-2016-20153050200-125', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(312, 'student-2016-20153050200-125', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(313, 'student-2016-20153050200-125', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:13', '2018-11-29 09:12:13', NULL),
(314, 'student-2016-20163045200-126', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(315, 'student-2016-20163045200-126', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(316, 'student-2016-20163045200-126', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(317, 'student-2016-20151015050-127', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(318, 'student-2016-20151015050-127', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(319, 'student-2016-20151015050-127', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(320, 'student-2016-20131020125-128', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(321, 'student-2016-20131020125-128', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(322, 'student-2016-20131020125-128', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(323, 'student-2016-20161015140-129', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(324, 'student-2016-20161015140-129', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(325, 'student-2016-20161015140-129', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(326, 'student-2016-20161015080-130', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(327, 'student-2016-20161015080-130', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(328, 'student-2016-20151020170-131', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(329, 'student-2016-20151020170-131', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(330, 'student-2016-20161015110-132', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(331, 'student-2016-20161015110-132', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(332, 'student-2016-20161015110-132', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(333, 'student-2016-20142035065-133', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(334, 'student-2016-20142035065-133', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(335, 'student-2016-20142035065-133', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(336, 'student-2016-20153055275-134', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(337, 'student-2016-20153055275-134', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(338, 'student-2016-20142035020-135', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(339, 'student-2016-20142035020-135', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(340, 'student-2016-20142035020-135', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(341, 'student-2016-20161025155-136', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(342, 'student-2016-20161025155-136', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:14', '2018-11-29 09:12:14', NULL),
(343, 'student-2016-20141025155-137', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(344, 'student-2016-20141025155-137', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(345, 'student-2016-20141025155-137', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(346, 'student-2016-20161020170-138', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(347, 'student-2016-20161020170-138', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(348, 'student-2016-20131025140-139', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(349, 'student-2016-20131025140-139', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(350, 'student-2016-20151020140-140', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(351, 'student-2016-20151020140-140', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(352, 'student-2016-20151020140-140', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(353, 'university-2017--143', 'Student Health Insurance/Medical', 4500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(354, 'university-2017--143', 'Sport Fees', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(355, 'university-2017--143', 'Examination', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(356, 'university-2017--143', 'Registration', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(357, 'university-2017--143', 'Library Fees', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(358, 'university-2017--143', 'Laboratory Fees', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(359, 'university-2017--143', 'Municipal Charge', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(360, 'university-2017--143', 'Information Com. Tech. Fees', 8000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(361, 'university-2017--143', 'Student Union Dues', 800, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(362, 'university-2017--143', 'Accreditation Fee', 3000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(363, 'university-2017--143', 'PMF Levy', 5000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(364, 'university-2017--143', 'Development Levy', 5000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(365, 'faculty-2017-SOSC-144', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(366, 'faculty-2017-SOSC-144', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(367, 'faculty-2017-SOSC-144', 'Transportation', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(368, 'faculty-2017-SMAT-145', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(369, 'faculty-2017-SMAT-145', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(370, 'faculty-2017-SMAT-145', 'Transportation', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(371, 'faculty-2017-SEET-146', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(372, 'faculty-2017-SEET-146', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(373, 'faculty-2017-SEET-146', 'Transportation', 1000, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(374, 'dept-2017-BIO-148', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(375, 'dept-2017-BIO-148', 'BIO National Conference Fee', 1500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(376, 'dept-2017-IMT-149', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:15', '2018-11-29 09:12:15', NULL),
(377, 'dept-2017-IMT-149', 'IMT National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(378, 'dept-2017-MEE-150', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(379, 'dept-2017-MEE-150', 'MEE National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(380, 'dept-2017-CIE-151', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(381, 'dept-2017-CIE-151', 'CIE National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(382, 'dept-2017-PMT-152', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(383, 'dept-2017-PMT-152', 'PMT National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(384, 'dept-2017-CHM-153', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(385, 'dept-2017-CHM-153', 'CHM National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(386, 'dept-2017-CHE-154', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(387, 'dept-2017-CHE-154', 'CHE National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(388, 'dept-2017-MTH-155', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(389, 'dept-2017-MTH-155', 'MTH National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(390, 'dept-2017-PHY-156', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(391, 'dept-2017-PHY-156', 'PHY National Conference Fee', 1500, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(392, 'level-2017-100-158', 'Acceptance Fee', 25000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(393, 'level-2017-100-158', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(394, 'level-2017-100-158', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(395, 'level-2017-100-158', 'Matriculation', 8000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(396, 'level-2017-100-158', 'Books', 18000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(397, 'level-2017-200-159', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(398, 'level-2017-200-159', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(399, 'level-2017-200-159', 'Books', 13000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(400, 'level-2017-300-160', 'Laboratory Fee', 2000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(401, 'level-2017-300-160', 'Books', 10000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(402, 'level-2017-400-161', 'Industial Attachment', 1000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(403, 'level-2017-400-161', 'Books', 5000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(404, 'level-2017-500-162', 'Final Project', 10000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(405, 'level-2017-500-162', 'Convocation', 8000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(406, 'level-2017-500-162', 'Books', 8000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(407, 'student-2017-20171030230-164', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:16', '2018-11-29 09:12:16', NULL),
(408, 'student-2017-20171030230-164', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(409, 'student-2017-20171030230-164', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(410, 'student-2017-20161020125-165', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(411, 'student-2017-20161020125-165', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(412, 'student-2017-20161020125-165', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(413, 'student-2017-20162035080-166', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(414, 'student-2017-20162035080-166', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(415, 'student-2017-20162035080-166', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(416, 'student-2017-20141030200-167', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(417, 'student-2017-20141030200-167', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(418, 'student-2017-20141030200-167', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(419, 'student-2017-20173045230-168', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(420, 'student-2017-20173045230-168', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(421, 'student-2017-20173045230-168', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(422, 'student-2017-20163045125-169', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(423, 'student-2017-20163045125-169', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(424, 'student-2017-20163045125-169', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(425, 'student-2017-20131015070-170', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(426, 'student-2017-20131015070-170', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(427, 'student-2017-20131015070-170', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(428, 'student-2017-20143055155-171', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL);
INSERT INTO `mums_invoice_lines` (`id`, `invoice_id`, `item`, `amount`, `unit`, `created_at`, `updated_at`, `item_no`) VALUES
(429, 'student-2017-20143055155-171', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(430, 'student-2017-20143055155-171', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(431, 'student-2017-20173045125-172', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(432, 'student-2017-20173045125-172', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(433, 'student-2017-20173045125-172', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(434, 'student-2017-20143045005-173', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(435, 'student-2017-20143045005-173', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(436, 'student-2017-20143045005-173', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(437, 'student-2017-20171030095-174', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(438, 'student-2017-20171030095-174', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(439, 'student-2017-20171030095-174', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:17', '2018-11-29 09:12:17', NULL),
(440, 'student-2017-20132035050-175', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(441, 'student-2017-20132035050-175', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(442, 'student-2017-20163055185-176', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(443, 'student-2017-20163055185-176', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(444, 'student-2017-20163055185-176', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(445, 'student-2017-20153045320-177', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(446, 'student-2017-20153045320-177', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(447, 'student-2017-20141025155-178', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(448, 'student-2017-20141025155-178', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(449, 'student-2017-20141025155-178', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(450, 'student-2017-20163050140-179', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(451, 'student-2017-20163050140-179', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(452, 'student-2017-20163050140-179', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(453, 'student-2017-20163050035-180', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(454, 'student-2017-20163050035-180', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(455, 'student-2017-20163050035-180', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(456, 'student-2017-20133055140-181', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(457, 'student-2017-20133055140-181', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(458, 'student-2017-20133055140-181', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(459, 'student-2017-20141030125-182', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(460, 'student-2017-20141030125-182', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(461, 'student-2017-20141030125-182', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(462, 'student-2017-20163050215-183', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(463, 'student-2017-20163050215-183', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(464, 'student-2017-20163050215-183', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(465, 'student-2017-20131015155-184', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(466, 'student-2017-20131015155-184', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(467, 'student-2017-20131015155-184', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(468, 'student-2017-20133055095-185', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:18', '2018-11-29 09:12:18', NULL),
(469, 'student-2017-20133055095-185', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(470, 'student-2017-20133055095-185', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(471, 'student-2017-20141030005-186', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(472, 'student-2017-20141030005-186', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(473, 'student-2017-20143055185-187', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(474, 'student-2017-20143055185-187', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(475, 'student-2017-20143055185-187', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(476, 'student-2017-20143055095-188', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(477, 'student-2017-20143055095-188', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(478, 'student-2017-20143055095-188', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(479, 'student-2017-20143050035-189', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(480, 'student-2017-20143050035-189', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(481, 'student-2017-20143050035-189', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(482, 'student-2017-20163055155-190', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(483, 'student-2017-20163055155-190', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(484, 'student-2017-20163055155-190', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(485, 'student-2017-20172035080-191', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(486, 'student-2017-20172035080-191', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(487, 'student-2017-20172035080-191', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(488, 'student-2017-20173050215-192', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(489, 'student-2017-20173050215-192', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(490, 'student-2017-20173050215-192', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(491, 'student-2017-20163045065-193', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(492, 'student-2017-20163045065-193', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(493, 'student-2017-20133050020-194', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(494, 'student-2017-20133050020-194', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(495, 'student-2017-20133050020-194', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(496, 'student-2017-20171015140-195', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(497, 'student-2017-20171015140-195', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(498, 'university-2018--198', 'Student Health Insurance/Medical', 4500, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(499, 'university-2018--198', 'Sport Fees', 1000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(500, 'university-2018--198', 'Examination', 1000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(501, 'university-2018--198', 'Registration', 1000, 1, '2018-11-29 09:12:19', '2018-11-29 09:12:19', NULL),
(502, 'university-2018--198', 'Library Fees', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(503, 'university-2018--198', 'Laboratory Fees', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(504, 'university-2018--198', 'Municipal Charge', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(505, 'university-2018--198', 'Information Com. Tech. Fees', 8000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(506, 'university-2018--198', 'Student Union Dues', 800, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(507, 'university-2018--198', 'Accreditation Fee', 3000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(508, 'university-2018--198', 'PMF Levy', 5000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(509, 'university-2018--198', 'Development Levy', 5000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(510, 'faculty-2018-SOSC-199', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(511, 'faculty-2018-SOSC-199', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(512, 'faculty-2018-SOSC-199', 'Transportation', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(513, 'faculty-2018-SMAT-200', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(514, 'faculty-2018-SMAT-200', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(515, 'faculty-2018-SMAT-200', 'Transportation', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(516, 'faculty-2018-SEET-201', 'Maintenance Fee', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(517, 'faculty-2018-SEET-201', 'National Conference Fee', 3000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(518, 'faculty-2018-SEET-201', 'Transportation', 1000, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(519, 'dept-2018-BIO-203', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(520, 'dept-2018-BIO-203', 'BIO National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(521, 'dept-2018-IMT-204', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(522, 'dept-2018-IMT-204', 'IMT National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(523, 'dept-2018-MEE-205', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(524, 'dept-2018-MEE-205', 'MEE National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(525, 'dept-2018-CIE-206', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(526, 'dept-2018-CIE-206', 'CIE National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(527, 'dept-2018-PMT-207', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(528, 'dept-2018-PMT-207', 'PMT National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(529, 'dept-2018-CHM-208', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(530, 'dept-2018-CHM-208', 'CHM National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(531, 'dept-2018-CHE-209', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(532, 'dept-2018-CHE-209', 'CHE National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(533, 'dept-2018-MTH-210', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(534, 'dept-2018-MTH-210', 'MTH National Conference Fee', 1500, 1, '2018-11-29 09:12:20', '2018-11-29 09:12:20', NULL),
(535, 'dept-2018-PHY-211', 'Departmental Levy', 1500, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(536, 'dept-2018-PHY-211', 'PHY National Conference Fee', 1500, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(537, 'level-2018-100-213', 'Acceptance Fee', 25000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(538, 'level-2018-100-213', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(539, 'level-2018-100-213', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(540, 'level-2018-100-213', 'Matriculation', 8000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(541, 'level-2018-100-213', 'Books', 18000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(542, 'level-2018-200-214', 'Workshop Practice', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(543, 'level-2018-200-214', 'Laboratory Fee', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(544, 'level-2018-200-214', 'Books', 13000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(545, 'level-2018-300-215', 'Laboratory Fee', 2000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(546, 'level-2018-300-215', 'Books', 10000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(547, 'level-2018-400-216', 'Industial Attachment', 1000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(548, 'level-2018-400-216', 'Books', 5000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(549, 'level-2018-500-217', 'Final Project', 10000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(550, 'level-2018-500-217', 'Convocation', 8000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(551, 'level-2018-500-217', 'Books', 8000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(552, 'student-2018-20163045050-219', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(553, 'student-2018-20163045050-219', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(554, 'student-2018-20163045050-219', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(555, 'student-2018-20171020050-220', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(556, 'student-2018-20171020050-220', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(557, 'student-2018-20171020050-220', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(558, 'student-2018-20133050200-221', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(559, 'student-2018-20133050200-221', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(560, 'student-2018-20163050080-222', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(561, 'student-2018-20163050080-222', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(562, 'student-2018-20163050080-222', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(563, 'student-2018-20173045125-223', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(564, 'student-2018-20173045125-223', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:21', '2018-11-29 09:12:21', NULL),
(565, 'student-2018-20173045125-223', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(566, 'student-2018-20183045050-224', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(567, 'student-2018-20183045050-224', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(568, 'student-2018-20183045050-224', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(569, 'student-2018-20143055155-225', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(570, 'student-2018-20143055155-225', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(571, 'student-2018-20143055155-225', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(572, 'student-2018-20143050065-226', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(573, 'student-2018-20143050065-226', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(574, 'student-2018-20143050065-226', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(575, 'student-2018-20131015070-227', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(576, 'student-2018-20131015070-227', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(577, 'student-2018-20131015070-227', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(578, 'student-2018-20161020050-228', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(579, 'student-2018-20161020050-228', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(580, 'student-2018-20161020050-228', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(581, 'student-2018-20131030085-229', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(582, 'student-2018-20131030085-229', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(583, 'student-2018-20131030085-229', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(584, 'student-2018-20181020050-230', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(585, 'student-2018-20181020050-230', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(586, 'student-2018-20171020200-231', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(587, 'student-2018-20171020200-231', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(588, 'student-2018-20171020200-231', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:22', '2018-11-29 09:12:22', NULL),
(589, 'student-2018-20161020185-232', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(590, 'student-2018-20161020185-232', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(591, 'student-2018-20161020185-232', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(592, 'student-2018-20153050065-233', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(593, 'student-2018-20153050065-233', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(594, 'student-2018-20153050065-233', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(595, 'student-2018-20172040125-234', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(596, 'student-2018-20172040125-234', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(597, 'student-2018-20172040125-234', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(598, 'student-2018-20133055005-235', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(599, 'student-2018-20133055005-235', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(600, 'student-2018-20133055005-235', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(601, 'student-2018-20172040110-236', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(602, 'student-2018-20172040110-236', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(603, 'student-2018-20172040110-236', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(604, 'student-2018-20151025005-237', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(605, 'student-2018-20151025005-237', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(606, 'student-2018-20151025005-237', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(607, 'student-2018-20133045170-238', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(608, 'student-2018-20133045170-238', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(609, 'student-2018-20133045170-238', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(610, 'student-2018-20173045095-239', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(611, 'student-2018-20173045095-239', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(612, 'student-2018-20173045095-239', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(613, 'student-2018-20183045065-240', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(614, 'student-2018-20183045065-240', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(615, 'student-2018-20183045065-240', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(616, 'student-2018-20181020185-241', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(617, 'student-2018-20181020185-241', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(618, 'student-2018-20181020185-241', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(619, 'student-2018-20182040035-242', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:23', '2018-11-29 09:12:23', NULL),
(620, 'student-2018-20182040035-242', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(621, 'student-2018-20182040035-242', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(622, 'student-2018-20143045140-243', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(623, 'student-2018-20143045140-243', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(624, 'student-2018-20143045140-243', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(625, 'student-2018-20182035080-244', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(626, 'student-2018-20182035080-244', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(627, 'student-2018-20153045260-245', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(628, 'student-2018-20153045260-245', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(629, 'student-2018-20153045260-245', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(630, 'student-2018-20152035020-246', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(631, 'student-2018-20152035020-246', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(632, 'student-2018-20152035020-246', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(633, 'student-2018-20142035065-247', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(634, 'student-2018-20142035065-247', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(635, 'student-2018-20142035065-247', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(636, 'student-2018-20133055050-248', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(637, 'student-2018-20133055050-248', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(638, 'student-2018-20133055050-248', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(639, 'student-2018-20132040015-249', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(640, 'student-2018-20132040015-249', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(641, 'student-2018-20132040015-249', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(642, 'student-2018-20153055005-250', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(643, 'student-2018-20153055005-250', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(644, 'student-2018-20152035005-251', 'Damage of School Property', 7000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(645, 'student-2018-20152035005-251', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(646, 'student-2018-20152035005-251', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(647, 'student-2018-20132040010-252', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(648, 'student-2018-20132040010-252', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(649, 'student-2018-20132040010-252', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(650, 'student-2018-20173055005-253', 'Payment for Hall Venue', 2000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(651, 'student-2018-20173055005-253', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(652, 'student-2018-20173055005-253', 'Bus Transportation Services', 10000, 1, '2018-11-29 09:12:24', '2018-11-29 09:12:24', NULL),
(653, 'student-2018-20181025155-254', 'Payment for Hall Venue', 4000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(654, 'student-2018-20181025155-254', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(655, 'student-2018-20181025155-254', 'Bus Transportation Services', 15000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(656, 'student-2018-20161030230-255', 'Payment for Hall Venue', 2500, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(657, 'student-2018-20161030230-255', 'Payment for Association Ownership', 3000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(658, 'student-2018-20161030230-255', 'Bus Transportation Services', 8000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(659, 'student-2018-20162040005-256', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(660, 'student-2018-20162040005-256', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(661, 'student-2018-20162040005-256', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(662, 'student-2018-20162040125-257', 'Damage of Louvers', 1000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(663, 'student-2018-20162040125-257', 'Damage of Hostel Installation', 2500, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(664, 'student-2018-20162040125-257', 'Fine for Illegal Electrical Connection', 1000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(665, 'student-2018-20163055020-258', 'Damage of School Property', 4000, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL),
(666, 'student-2018-20163055020-258', 'Damage of Hostel Installation', 1500, 1, '2018-11-29 09:12:25', '2018-11-29 09:12:25', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mums_menus`
--

CREATE TABLE `mums_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_menus`
--

INSERT INTO `mums_menus` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', '2018-11-09 22:49:37', '2018-11-09 22:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `mums_menu_items`
--

CREATE TABLE `mums_menu_items` (
  `id` int(10) UNSIGNED NOT NULL,
  `menu_id` int(10) UNSIGNED DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `url` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `target` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '_self',
  `icon_class` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `route` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` text COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_menu_items`
--

INSERT INTO `mums_menu_items` (`id`, `menu_id`, `title`, `url`, `target`, `icon_class`, `color`, `parent_id`, `order`, `created_at`, `updated_at`, `route`, `parameters`) VALUES
(1, 1, 'Dashboard', '', '_self', 'voyager-boat', NULL, NULL, 1, '2018-11-09 22:49:37', '2018-11-09 22:49:37', 'voyager.dashboard', NULL),
(2, 1, 'Media', '', '_self', 'voyager-images', NULL, NULL, 4, '2018-11-09 22:49:37', '2018-11-27 12:29:51', 'voyager.media.index', NULL),
(3, 1, 'Users', '', '_self', 'voyager-person', NULL, NULL, 3, '2018-11-09 22:49:37', '2018-11-09 22:49:37', 'voyager.users.index', NULL),
(4, 1, 'Roles', '', '_self', 'voyager-lock', NULL, NULL, 2, '2018-11-09 22:49:37', '2018-11-09 22:49:37', 'voyager.roles.index', NULL),
(5, 1, 'Tools', '', '_self', 'voyager-tools', NULL, NULL, 8, '2018-11-09 22:49:37', '2018-11-27 12:29:51', NULL, NULL),
(6, 1, 'Menu Builder', '', '_self', 'voyager-list', NULL, 5, 1, '2018-11-09 22:49:37', '2018-11-27 12:29:51', 'voyager.menus.index', NULL),
(7, 1, 'Database', '', '_self', 'voyager-data', NULL, 5, 2, '2018-11-09 22:49:37', '2018-11-27 12:29:51', 'voyager.database.index', NULL),
(8, 1, 'Compass', '', '_self', 'voyager-compass', NULL, 5, 3, '2018-11-09 22:49:37', '2018-11-27 12:29:51', 'voyager.compass.index', NULL),
(9, 1, 'BREAD', '', '_self', 'voyager-bread', NULL, 5, 4, '2018-11-09 22:49:38', '2018-11-27 12:29:51', 'voyager.bread.index', NULL),
(10, 1, 'Settings', '', '_self', 'voyager-settings', NULL, NULL, 17, '2018-11-09 22:49:38', '2018-11-30 14:51:39', 'voyager.settings.index', NULL),
(11, 1, 'Categories', '', '_self', 'voyager-categories', NULL, NULL, 7, '2018-11-09 22:49:44', '2018-11-27 12:29:51', 'voyager.categories.index', NULL),
(12, 1, 'Posts', '', '_self', 'voyager-news', NULL, NULL, 5, '2018-11-09 22:49:45', '2018-11-27 12:29:51', 'voyager.posts.index', NULL),
(13, 1, 'Pages', '', '_self', 'voyager-file-text', NULL, NULL, 6, '2018-11-09 22:49:46', '2018-11-27 12:29:51', 'voyager.pages.index', NULL),
(14, 1, 'Hooks', '', '_self', 'voyager-hook', NULL, 5, 5, '2018-11-09 22:49:47', '2018-11-27 12:29:51', 'voyager.hooks', NULL),
(15, 1, 'Discussions', '', '_self', 'voyager-chat', '#000000', 22, 2, '2018-11-10 10:32:19', '2018-11-29 18:02:13', 'voyager.mums-chatter-discussion.index', 'null'),
(16, 1, 'Posts', '', '_self', 'voyager-documentation', '#000000', 22, 1, '2018-11-10 10:43:45', '2018-11-29 18:02:13', 'voyager.mums-chatter-post.index', 'null'),
(19, 1, 'Invoices', '', '_self', 'voyager-receipt', NULL, NULL, 10, '2018-11-27 14:18:51', '2018-11-29 18:01:29', 'voyager.invoices.index', NULL),
(20, 1, 'Invoice Lines', '', '_self', 'voyager-window-list', '#000000', NULL, 11, '2018-11-27 16:02:33', '2018-11-29 18:01:29', 'voyager.invoice-lines.index', 'null'),
(21, 1, 'Notices', '', '_self', 'voyager-alarm-clock', '#000000', NULL, 12, '2018-11-29 17:58:13', '2018-11-30 14:51:39', 'voyager.notices.index', 'null'),
(22, 1, 'Forums', '#', '_self', 'voyager-logbook', '#000000', NULL, 9, '2018-11-29 18:01:11', '2018-11-29 18:03:22', NULL, ''),
(23, 1, 'School Calendars', '', '_self', 'voyager-calendar', NULL, NULL, 13, '2018-11-29 20:57:14', '2018-11-30 14:51:39', 'voyager.calendar.index', NULL),
(24, 1, 'Payments', '', '_self', 'voyager-paypal', NULL, NULL, 14, '2018-11-30 00:33:46', '2018-11-30 14:51:39', 'voyager.payments.index', NULL),
(25, 1, 'Time Tables', '', '_self', 'voyager-dashboard', '#000000', NULL, 15, '2018-11-30 00:50:46', '2018-11-30 14:52:20', 'voyager.time-table.index', 'null'),
(26, 1, 'Admin Accesses', '', '_self', 'voyager-anchor', '#000000', NULL, 16, '2018-11-30 01:05:55', '2018-11-30 14:51:51', 'voyager.admin-access.index', 'null');

-- --------------------------------------------------------

--
-- Table structure for table `mums_message_network`
--

CREATE TABLE `mums_message_network` (
  `id` int(11) NOT NULL,
  `user` varchar(185) DEFAULT NULL,
  `friend` varchar(185) DEFAULT NULL,
  `status` enum('requested','approved','rejected') NOT NULL DEFAULT 'requested',
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mums_migrations`
--

CREATE TABLE `mums_migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_migrations`
--

INSERT INTO `mums_migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_100000_create_password_resets_table', 1),
(2, '2017_07_19_082005_create_1500441605_permissions_table', 1),
(3, '2017_07_19_082006_create_1500441606_roles_table', 1),
(4, '2017_07_19_082009_create_1500441609_users_table', 1),
(5, '2017_07_19_082347_create_1500441827_courses_table', 1),
(6, '2017_07_19_082723_create_1500442043_lessons_table', 1),
(7, '2017_07_19_082724_create_media_table', 1),
(8, '2017_07_19_082929_create_1500442169_questions_table', 1),
(9, '2017_07_19_083047_create_1500442247_questions_options_table', 1),
(10, '2017_07_19_083236_create_1500442356_tests_table', 1),
(11, '2017_07_19_120427_create_596eec08307cd_permission_role_table', 1),
(12, '2017_07_19_120430_create_596eec0af366b_role_user_table', 1),
(13, '2017_07_19_120808_create_596eece522a6e_course_user_table', 1),
(14, '2017_07_19_121657_create_596eeef709839_question_test_table', 1),
(15, '2017_08_14_085956_create_course_students_table', 1),
(16, '2017_08_17_051131_create_tests_results_table', 1),
(17, '2017_08_17_051254_create_tests_results_answers_table', 1),
(18, '2017_08_18_054622_create_lesson_student_table', 1),
(19, '2017_08_18_060324_add_rating_to_course_student_table', 1),
(20, '2016_07_29_171118_create_chatter_categories_table', 2),
(21, '2016_07_29_171118_create_chatter_discussion_table', 2),
(22, '2016_07_29_171118_create_chatter_post_table', 2),
(23, '2016_07_29_171128_create_foreign_keys', 2),
(24, '2016_08_02_183143_add_slug_field_for_discussions', 2),
(25, '2016_08_03_121747_add_color_row_to_chatter_discussions', 2),
(26, '2017_01_16_121747_add_markdown_and_lock_to_chatter_posts', 2),
(27, '2017_01_16_121747_create_chatter_user_discussion_pivot_table', 2),
(28, '2017_08_07_165345_add_chatter_soft_deletes', 2),
(29, '2017_10_10_221227_add_chatter_last_reply_at_discussion', 2),
(30, '2016_01_01_000000_add_voyager_user_fields', 3),
(31, '2016_01_01_000000_create_data_types_table', 3),
(32, '2016_05_19_173453_create_menu_table', 3),
(33, '2016_10_21_190000_create_roles_table', 4),
(34, '2016_10_21_190000_create_settings_table', 4),
(35, '2016_11_30_135954_create_permission_table', 4),
(36, '2016_11_30_141208_create_permission_role_table', 4),
(37, '2016_12_26_201236_data_types__add__server_side', 4),
(38, '2017_01_13_000000_add_route_to_menu_items_table', 4),
(39, '2017_01_14_005015_create_translations_table', 4),
(40, '2017_01_15_000000_make_table_name_nullable_in_permissions_table', 4),
(41, '2017_03_06_000000_add_controller_to_data_types_table', 4),
(42, '2017_04_21_000000_add_order_to_data_rows_table', 4),
(43, '2017_07_05_210000_add_policyname_to_data_types_table', 4),
(44, '2017_08_05_000000_add_group_to_settings_table', 4),
(45, '2017_11_26_013050_add_user_role_relationship', 4),
(46, '2017_11_26_015000_create_user_roles_table', 4),
(47, '2018_03_11_000000_add_user_settings', 4),
(48, '2018_03_14_000000_add_details_to_data_types_table', 4),
(49, '2018_03_16_000000_make_settings_value_nullable', 4),
(50, '2016_01_01_000000_create_pages_table', 5),
(51, '2016_01_01_000000_create_posts_table', 5),
(52, '2016_02_15_204651_create_categories_table', 5),
(53, '2017_04_11_000000_alter_post_nullable_fields_table', 5);

-- --------------------------------------------------------

--
-- Table structure for table `mums_notices`
--

CREATE TABLE `mums_notices` (
  `id` int(11) NOT NULL,
  `title` longtext DEFAULT NULL,
  `content` longtext DEFAULT NULL,
  `system_id` varchar(255) DEFAULT NULL,
  `announcer` varchar(12) DEFAULT NULL,
  `display` varchar(255) NOT NULL DEFAULT 'show',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_notices`
--

INSERT INTO `mums_notices` (`id`, `title`, `content`, `system_id`, `announcer`, `display`, `created_at`, `updated_at`) VALUES
(1, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-01-31 00:00:00', '2017-01-31 00:00:00'),
(2, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-01-29 00:00:00', '2017-01-29 00:00:00'),
(3, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-01-28 00:00:00', '2017-01-28 00:00:00'),
(4, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-01-11 00:00:00', '2017-01-11 00:00:00'),
(5, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-01-31 00:00:00', '2017-01-31 00:00:00'),
(6, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-01-29 00:00:00', '2017-01-29 00:00:00'),
(7, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-01-28 00:00:00', '2017-01-28 00:00:00'),
(8, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-01-11 00:00:00', '2017-01-11 00:00:00'),
(9, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-02-05 00:00:00', '2017-02-05 00:00:00'),
(10, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-02-04 00:00:00', '2017-02-04 00:00:00'),
(11, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-01-18 00:00:00', '2017-01-18 00:00:00'),
(12, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-02-12 00:00:00', '2017-02-12 00:00:00'),
(13, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-02-11 00:00:00', '2017-02-11 00:00:00'),
(14, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-01-25 00:00:00', '2017-01-25 00:00:00'),
(15, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-02-19 00:00:00', '2017-02-19 00:00:00'),
(16, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-02-18 00:00:00', '2017-02-18 00:00:00'),
(17, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-02-01 00:00:00', '2017-02-01 00:00:00'),
(18, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-02-26 00:00:00', '2017-02-26 00:00:00'),
(19, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-02-25 00:00:00', '2017-02-25 00:00:00'),
(20, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-02-08 00:00:00', '2017-02-08 00:00:00'),
(21, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-03-05 00:00:00', '2017-03-05 00:00:00'),
(22, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-03-04 00:00:00', '2017-03-04 00:00:00'),
(23, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-02-15 00:00:00', '2017-02-15 00:00:00'),
(24, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-03-12 00:00:00', '2017-03-12 00:00:00'),
(25, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-03-11 00:00:00', '2017-03-11 00:00:00'),
(26, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-02-22 00:00:00', '2017-02-22 00:00:00'),
(27, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-03-19 00:00:00', '2017-03-19 00:00:00'),
(28, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-03-18 00:00:00', '2017-03-18 00:00:00'),
(29, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-03-01 00:00:00', '2017-03-01 00:00:00'),
(30, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-03-26 00:00:00', '2017-03-26 00:00:00'),
(31, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-03-25 00:00:00', '2017-03-25 00:00:00');
INSERT INTO `mums_notices` (`id`, `title`, `content`, `system_id`, `announcer`, `display`, `created_at`, `updated_at`) VALUES
(32, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-03-08 00:00:00', '2017-03-08 00:00:00'),
(33, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-04-02 00:00:00', '2017-04-02 00:00:00'),
(34, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-04-01 00:00:00', '2017-04-01 00:00:00'),
(35, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-03-15 00:00:00', '2017-03-15 00:00:00'),
(36, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-04-09 00:00:00', '2017-04-09 00:00:00'),
(37, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-04-08 00:00:00', '2017-04-08 00:00:00'),
(38, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-03-22 00:00:00', '2017-03-22 00:00:00'),
(39, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-04-16 00:00:00', '2017-04-16 00:00:00'),
(40, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-04-15 00:00:00', '2017-04-15 00:00:00'),
(41, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-03-29 00:00:00', '2017-03-29 00:00:00'),
(42, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-04-23 00:00:00', '2017-04-23 00:00:00'),
(43, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-04-22 00:00:00', '2017-04-22 00:00:00'),
(44, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-04-05 00:00:00', '2017-04-05 00:00:00'),
(45, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-04-30 00:00:00', '2017-04-30 00:00:00'),
(46, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-04-29 00:00:00', '2017-04-29 00:00:00'),
(47, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-04-12 00:00:00', '2017-04-12 00:00:00'),
(48, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-05-07 00:00:00', '2017-05-07 00:00:00'),
(49, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-05-06 00:00:00', '2017-05-06 00:00:00'),
(50, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-04-19 00:00:00', '2017-04-19 00:00:00'),
(51, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-05-14 00:00:00', '2017-05-14 00:00:00'),
(52, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-05-13 00:00:00', '2017-05-13 00:00:00'),
(53, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-04-26 00:00:00', '2017-04-26 00:00:00'),
(54, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-05-21 00:00:00', '2017-05-21 00:00:00'),
(55, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-05-20 00:00:00', '2017-05-20 00:00:00'),
(56, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-05-03 00:00:00', '2017-05-03 00:00:00'),
(57, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-05-28 00:00:00', '2017-05-28 00:00:00'),
(58, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-05-27 00:00:00', '2017-05-27 00:00:00'),
(59, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-05-10 00:00:00', '2017-05-10 00:00:00'),
(60, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-06-04 00:00:00', '2017-06-04 00:00:00'),
(61, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-06-03 00:00:00', '2017-06-03 00:00:00');
INSERT INTO `mums_notices` (`id`, `title`, `content`, `system_id`, `announcer`, `display`, `created_at`, `updated_at`) VALUES
(62, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-05-17 00:00:00', '2017-05-17 00:00:00'),
(63, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-06-11 00:00:00', '2017-06-11 00:00:00'),
(64, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-06-10 00:00:00', '2017-06-10 00:00:00'),
(65, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-05-24 00:00:00', '2017-05-24 00:00:00'),
(66, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-06-18 00:00:00', '2017-06-18 00:00:00'),
(67, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-06-17 00:00:00', '2017-06-17 00:00:00'),
(68, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-05-31 00:00:00', '2017-05-31 00:00:00'),
(69, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-06-25 00:00:00', '2017-06-25 00:00:00'),
(70, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-06-24 00:00:00', '2017-06-24 00:00:00'),
(71, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-06-07 00:00:00', '2017-06-07 00:00:00'),
(72, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-07-02 00:00:00', '2017-07-02 00:00:00'),
(73, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-07-01 00:00:00', '2017-07-01 00:00:00'),
(74, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-06-14 00:00:00', '2017-06-14 00:00:00'),
(75, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-07-09 00:00:00', '2017-07-09 00:00:00'),
(76, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-07-08 00:00:00', '2017-07-08 00:00:00'),
(77, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-06-21 00:00:00', '2017-06-21 00:00:00'),
(78, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-07-16 00:00:00', '2017-07-16 00:00:00'),
(79, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-07-15 00:00:00', '2017-07-15 00:00:00'),
(80, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-06-28 00:00:00', '2017-06-28 00:00:00'),
(81, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-07-23 00:00:00', '2017-07-23 00:00:00'),
(82, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-07-22 00:00:00', '2017-07-22 00:00:00'),
(83, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-07-05 00:00:00', '2017-07-05 00:00:00'),
(84, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-07-30 00:00:00', '2017-07-30 00:00:00'),
(85, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-07-29 00:00:00', '2017-07-29 00:00:00'),
(86, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-07-12 00:00:00', '2017-07-12 00:00:00'),
(87, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-08-06 00:00:00', '2017-08-06 00:00:00'),
(88, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-08-05 00:00:00', '2017-08-05 00:00:00'),
(89, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-07-19 00:00:00', '2017-07-19 00:00:00'),
(90, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-08-13 00:00:00', '2017-08-13 00:00:00'),
(91, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-08-12 00:00:00', '2017-08-12 00:00:00');
INSERT INTO `mums_notices` (`id`, `title`, `content`, `system_id`, `announcer`, `display`, `created_at`, `updated_at`) VALUES
(92, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-07-26 00:00:00', '2017-07-26 00:00:00'),
(93, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-08-20 00:00:00', '2017-08-20 00:00:00'),
(94, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-08-19 00:00:00', '2017-08-19 00:00:00'),
(95, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-08-02 00:00:00', '2017-08-02 00:00:00'),
(96, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-08-27 00:00:00', '2017-08-27 00:00:00'),
(97, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-08-26 00:00:00', '2017-08-26 00:00:00'),
(98, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-08-09 00:00:00', '2017-08-09 00:00:00'),
(99, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-09-03 00:00:00', '2017-09-03 00:00:00'),
(100, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-09-02 00:00:00', '2017-09-02 00:00:00'),
(101, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-08-16 00:00:00', '2017-08-16 00:00:00'),
(102, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-09-10 00:00:00', '2017-09-10 00:00:00'),
(103, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-09-09 00:00:00', '2017-09-09 00:00:00'),
(104, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-08-23 00:00:00', '2017-08-23 00:00:00'),
(105, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-09-17 00:00:00', '2017-09-17 00:00:00'),
(106, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-09-16 00:00:00', '2017-09-16 00:00:00'),
(107, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-08-30 00:00:00', '2017-08-30 00:00:00'),
(108, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-09-24 00:00:00', '2017-09-24 00:00:00'),
(109, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-09-23 00:00:00', '2017-09-23 00:00:00'),
(110, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-09-06 00:00:00', '2017-09-06 00:00:00'),
(111, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-10-01 00:00:00', '2017-10-01 00:00:00'),
(112, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-09-30 00:00:00', '2017-09-30 00:00:00'),
(113, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-09-13 00:00:00', '2017-09-13 00:00:00'),
(114, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-10-08 00:00:00', '2017-10-08 00:00:00'),
(115, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-10-07 00:00:00', '2017-10-07 00:00:00'),
(116, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-09-20 00:00:00', '2017-09-20 00:00:00'),
(117, 'EXTENSION OF ONLINE APPLICATION INTO PRE-DEGREE PROGRAM', '<p>The University community and the public are hereby notified that the application for admission into the pre-degree program for the 2016/2017 academic session has been extended to February 17, 2017.</p>', 'mums', 'registrar', 'show', '2017-10-15 00:00:00', '2017-10-15 00:00:00'),
(118, 'POSTGRADUATE DIPLOMA IN PUBLIC PROCUREMENT ADVERT\n', '<p>The Postgraduate School, Federal University of Technology, Owerri\n (FUTO) invites applications from suitably qualified candidates for\n consideration for admission into Postgraduate Diploma in Public\n Procurement for the 2016/2017 session. Any interested candidate should\n pay a non-refundable fee of eleven thousand, three hundred naira\n (N11,300.00) only at any of the Deposit Money Banks (DMB) through the\n Remita Gateway on the Postgraduate application platform and then\n complete the online application at www.futo.edu.ng</p>\n<ol type=\"1\">\n <li><h3>ADMISSION REQUIREMENTS</h3>\n  <p>All candidates applying for the postgraduate programme in Public\n   Procurement must satisfy the basic matriculation requirements\n   including English Language and Mathematics as required by the Federal\n   University of Technology Owerri, Nigeria. This is in addition to\n   having:</p>\n  <ul>\n   <li>A Higher National Diploma (HND), Upper Credit or</li>\n   <li>A Bachelor’s degree in any discipline from an accredited\n    University with at least Second Class Honours.</li>\n  </ul></li>\n <li><h3>DURATION OF PROGRAMME</h3>\n  <p>A minimum of two (2) semesters and a maximum of four (4) semesters\n   for full-time and minimum of four (4) semesters and a maximum of six\n   (6) semesters for part-time students.</p></li>\n <li>\n  <h3>METHOD OF APPLICATION</h3> Candidates are advised to apply on-line\n  following the procedure given below:\n  <ul>\n   <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n   <li>Step 2: Click on Prospective Students.</li>\n   <li>Go to PG Application Form 2016/2017</li>\n   <li>Click on on-line application for Public Procurement Research\n    Centre (PGD)</li>\n   <li>Click on generate 2016/2017 PG Application Invoice</li>\n   <li>Enter your Email Address</li>\n   <li>Click on Submit</li>\n   <li>Print your invoice containing your RRR number (Remita Retrieval\n    Reference) and proceed to the bank for payment or make payment via\n    ATM card.</li>\n   <li>Go to any bank with your invoice and pay a non-refundable fee of\n    Eleven thousand three hundred naira (N11,300.00) for FUTO PGS or\n    PPRC application depending on your interest.</li>\n   <li>Return to Step 4 above</li>\n   <li>Click on Fill PG Application form 2016/2017</li>\n   <li>Enter your Remita Number from the bank</li>\n   <li>Fill the on-line form accordingly<br> NOTE: Make sure the referee\n    email address supplied is valid as a referee form link would be sent\n    to your referees for completion online.\n   </li>\n   <li>Print out a copy of the Acknowledgement Slip when the form is\n    completed. Also an acknowledgement would be sent to the email\n    address you provided while filling the form. Therefore make sure you\n    provide a valid personal e-mail address.</li>\n  </ul>\n </li>\n <li>\n  <h3>DOCUMENTS TO UPLOAD TO THE PG SCHOOL PORTAL</h3> Candidates should\n  upload the following relevant documents in support of their\n  application online\n  <ul>\n   <li>University Degree Certificates/Statement of Result</li>\n   <li>NYSC Discharge Certificate/Exemption Certificate</li>\n   <li>\"O\" Level Result</li>\n  </ul>\n </li>\n <li><h3>SUBMISSION OF ACADEMIC TRANSCRIPT</h3>\n  <p>All applicants must forward their transcript to the the Deputy\n   Registrar/Secretary, Postgraduate School, Federal University of\n   Technology, Owerri, Imo State, Nigeria, not later than 14th April,\n   2017. Please note that any application without Transcript will not be\n   considered for admission.</p></li>\n <li>\n  <h3>CLOSING DATE</h3>\n  <p>The application portal will close on 17th March , 2017. All\n   successful candidates will be notified via email/SMS.</p>\n </li>\n</ol>\n<p>For more enquires send an email to ict@futo.edu.ng</p>', 'mums', 'registrar', 'show', '2017-10-14 00:00:00', '2017-10-14 00:00:00'),
(119, 'THE SECOND AND THIRD SUPPLEMENTARY ADMISSION LISTS OF THE 2016/2017 SESSION RELEASED', '<p>To check if you have been offered provisional admission,<p>\n<ul>\n <li>Visit www.futo.edu.ng</li>\n <li>Click on Admission</li>\n <li>Click on Admission Status</li>\n</ul>\n<p>Type in your registration number and click on submit</p>', 'mums', 'registrar', 'show', '2017-09-27 00:00:00', '2017-09-27 00:00:00'),
(120, 'SALE OF 2016/2017 PRE-DEGREE PROGRAMME APPLICATION FORMS', '<p>\n The 2016/2017 pre-degree programme for The Federal University of\n Technology, Owerri (FUTO) has commenced.<br> Amount per form: N11,000 (\n Eleven Thousand Naira only)<br> Duration of sales: October 29, 2016 to\n February 28, 2017<br> Qualification: Candidates wishing to purchase the\n form must:\n</p>\n<ul>\n <li>Possess at least five O’ Level credit-level passes in English\n  Language, Mathematics, Physics, Chemistry and Biology or Agricultural\n  Science.</li>\n <li>Have registered or intend to register for the 2017 UTME.</li>\n</ul>\n<h3>METHOD OF PAYMENT</h3>\nCandidates are advised to apply on-line following the procedure given\nbelow:\n<ul>\n <li>Step 1: Go to the FUTO Website – www.futo.edu.ng</li>\n <li>Step 2: Click on Prospective Students.</li>\n <li>Step 3: Go to 2016/2017 Pre Degree Application Form</li>\n <li>Step 5: Click on generate 2016/2017 Pre Degree Application Invoice\n </li>\n <li>Step 6: Enter your Mobile Number</li>\n <li>Step 7: Click on Fill other required details</li>\n <li>Step 8: Print your invoice containing your RRR number (Remita\n  Retrieval Reference) and proceed to any bank for payment</li>\n <li>Step 9: After successful payment in bank, return to the portal</li>\n <li>Step 10: Click on Fill 2016/2017 Pre Degree Application</li>\n <li>Step 11: Enter your phone number again and click on Apply.</li>\n <li>Step12: Proceed with the completion of the Application form.</li>\n <li>Step 13: Print your application Acknowledgment slip</li>\n <li>Step 14: Await for the Pre-Degree Admission announcement from the\n  University.</li>\n</ul>', 'mums', 'registrar', 'show', '2017-10-22 00:00:00', '2017-10-22 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `mums_pages`
--

CREATE TABLE `mums_pages` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('ACTIVE','INACTIVE') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'INACTIVE',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_pages`
--

INSERT INTO `mums_pages` (`id`, `author_id`, `title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `created_at`, `updated_at`) VALUES
(1, 454, 'Hello World', 'Hang the jib grog grog blossom grapple dance the hempen jig gangway pressgang bilge rat to go on account lugger. Nelsons folly gabion line draught scallywag fire ship gaff fluke fathom case shot. Sea Legs bilge rat sloop matey gabion long clothes run a shot across the bow Gold Road cog league.', '<p>Hello World. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', 'pages/page1.jpg', 'hello-world', 'Yar Meta Description', 'Keyword1, Keyword2', 'ACTIVE', '2018-11-09 22:49:46', '2018-11-10 00:25:33');

-- --------------------------------------------------------

--
-- Table structure for table `mums_password_resets`
--

CREATE TABLE `mums_password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `mums_permissions`
--

CREATE TABLE `mums_permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_permissions`
--

INSERT INTO `mums_permissions` (`id`, `key`, `table_name`, `created_at`, `updated_at`) VALUES
(1, 'browse_admin', NULL, '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(2, 'browse_bread', NULL, '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(3, 'browse_database', NULL, '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(4, 'browse_media', NULL, '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(5, 'browse_compass', NULL, '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(6, 'browse_menus', 'menus', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(7, 'read_menus', 'menus', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(8, 'edit_menus', 'menus', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(9, 'add_menus', 'menus', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(10, 'delete_menus', 'menus', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(11, 'browse_roles', 'roles', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(12, 'read_roles', 'roles', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(13, 'edit_roles', 'roles', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(14, 'add_roles', 'roles', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(15, 'delete_roles', 'roles', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(16, 'browse_users', 'users', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(17, 'read_users', 'users', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(18, 'edit_users', 'users', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(19, 'add_users', 'users', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(20, 'delete_users', 'users', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(21, 'browse_settings', 'settings', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(22, 'read_settings', 'settings', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(23, 'edit_settings', 'settings', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(24, 'add_settings', 'settings', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(25, 'delete_settings', 'settings', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(26, 'browse_categories', 'categories', '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(27, 'read_categories', 'categories', '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(28, 'edit_categories', 'categories', '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(29, 'add_categories', 'categories', '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(30, 'delete_categories', 'categories', '2018-11-09 22:49:44', '2018-11-09 22:49:44'),
(31, 'browse_posts', 'posts', '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(32, 'read_posts', 'posts', '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(33, 'edit_posts', 'posts', '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(34, 'add_posts', 'posts', '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(35, 'delete_posts', 'posts', '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(36, 'browse_pages', 'pages', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(37, 'read_pages', 'pages', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(38, 'edit_pages', 'pages', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(39, 'add_pages', 'pages', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(40, 'delete_pages', 'pages', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(41, 'browse_hooks', NULL, '2018-11-09 22:49:47', '2018-11-09 22:49:47'),
(47, 'browse_mums_chatter_discussion', 'mums_chatter_discussion', '2018-11-10 10:32:19', '2018-11-10 10:32:19'),
(48, 'read_mums_chatter_discussion', 'mums_chatter_discussion', '2018-11-10 10:32:19', '2018-11-10 10:32:19'),
(49, 'edit_mums_chatter_discussion', 'mums_chatter_discussion', '2018-11-10 10:32:19', '2018-11-10 10:32:19'),
(50, 'add_mums_chatter_discussion', 'mums_chatter_discussion', '2018-11-10 10:32:19', '2018-11-10 10:32:19'),
(51, 'delete_mums_chatter_discussion', 'mums_chatter_discussion', '2018-11-10 10:32:19', '2018-11-10 10:32:19'),
(52, 'browse_mums_chatter_post', 'mums_chatter_post', '2018-11-10 10:43:45', '2018-11-10 10:43:45'),
(53, 'read_mums_chatter_post', 'mums_chatter_post', '2018-11-10 10:43:45', '2018-11-10 10:43:45'),
(54, 'edit_mums_chatter_post', 'mums_chatter_post', '2018-11-10 10:43:45', '2018-11-10 10:43:45'),
(55, 'add_mums_chatter_post', 'mums_chatter_post', '2018-11-10 10:43:45', '2018-11-10 10:43:45'),
(56, 'delete_mums_chatter_post', 'mums_chatter_post', '2018-11-10 10:43:45', '2018-11-10 10:43:45'),
(62, 'browse_mums_invoices', 'mums_invoices', '2018-11-27 14:18:51', '2018-11-27 14:18:51'),
(63, 'read_mums_invoices', 'mums_invoices', '2018-11-27 14:18:51', '2018-11-27 14:18:51'),
(64, 'edit_mums_invoices', 'mums_invoices', '2018-11-27 14:18:51', '2018-11-27 14:18:51'),
(65, 'add_mums_invoices', 'mums_invoices', '2018-11-27 14:18:51', '2018-11-27 14:18:51'),
(66, 'delete_mums_invoices', 'mums_invoices', '2018-11-27 14:18:51', '2018-11-27 14:18:51'),
(67, 'browse_mums_invoice_lines', 'mums_invoice_lines', '2018-11-27 16:02:33', '2018-11-27 16:02:33'),
(68, 'read_mums_invoice_lines', 'mums_invoice_lines', '2018-11-27 16:02:33', '2018-11-27 16:02:33'),
(69, 'edit_mums_invoice_lines', 'mums_invoice_lines', '2018-11-27 16:02:33', '2018-11-27 16:02:33'),
(70, 'add_mums_invoice_lines', 'mums_invoice_lines', '2018-11-27 16:02:33', '2018-11-27 16:02:33'),
(71, 'delete_mums_invoice_lines', 'mums_invoice_lines', '2018-11-27 16:02:33', '2018-11-27 16:02:33'),
(72, 'browse_mums_notices', 'mums_notices', '2018-11-29 17:58:13', '2018-11-29 17:58:13'),
(73, 'read_mums_notices', 'mums_notices', '2018-11-29 17:58:13', '2018-11-29 17:58:13'),
(74, 'edit_mums_notices', 'mums_notices', '2018-11-29 17:58:13', '2018-11-29 17:58:13'),
(75, 'add_mums_notices', 'mums_notices', '2018-11-29 17:58:13', '2018-11-29 17:58:13'),
(76, 'delete_mums_notices', 'mums_notices', '2018-11-29 17:58:13', '2018-11-29 17:58:13'),
(77, 'browse_mums_school_calendar', 'mums_school_calendar', '2018-11-29 20:57:14', '2018-11-29 20:57:14'),
(78, 'read_mums_school_calendar', 'mums_school_calendar', '2018-11-29 20:57:14', '2018-11-29 20:57:14'),
(79, 'edit_mums_school_calendar', 'mums_school_calendar', '2018-11-29 20:57:14', '2018-11-29 20:57:14'),
(80, 'add_mums_school_calendar', 'mums_school_calendar', '2018-11-29 20:57:14', '2018-11-29 20:57:14'),
(81, 'delete_mums_school_calendar', 'mums_school_calendar', '2018-11-29 20:57:14', '2018-11-29 20:57:14'),
(82, 'browse_mums_payments', 'mums_payments', '2018-11-30 00:33:46', '2018-11-30 00:33:46'),
(83, 'read_mums_payments', 'mums_payments', '2018-11-30 00:33:46', '2018-11-30 00:33:46'),
(84, 'edit_mums_payments', 'mums_payments', '2018-11-30 00:33:46', '2018-11-30 00:33:46'),
(85, 'add_mums_payments', 'mums_payments', '2018-11-30 00:33:46', '2018-11-30 00:33:46'),
(86, 'delete_mums_payments', 'mums_payments', '2018-11-30 00:33:46', '2018-11-30 00:33:46'),
(87, 'browse_mums_time_table', 'mums_time_table', '2018-11-30 00:50:46', '2018-11-30 00:50:46'),
(88, 'read_mums_time_table', 'mums_time_table', '2018-11-30 00:50:46', '2018-11-30 00:50:46'),
(89, 'edit_mums_time_table', 'mums_time_table', '2018-11-30 00:50:46', '2018-11-30 00:50:46'),
(90, 'add_mums_time_table', 'mums_time_table', '2018-11-30 00:50:46', '2018-11-30 00:50:46'),
(91, 'delete_mums_time_table', 'mums_time_table', '2018-11-30 00:50:46', '2018-11-30 00:50:46'),
(92, 'browse_mums_admin_access', 'mums_admin_access', '2018-11-30 01:05:55', '2018-11-30 01:05:55'),
(93, 'read_mums_admin_access', 'mums_admin_access', '2018-11-30 01:05:55', '2018-11-30 01:05:55'),
(94, 'edit_mums_admin_access', 'mums_admin_access', '2018-11-30 01:05:55', '2018-11-30 01:05:55'),
(95, 'add_mums_admin_access', 'mums_admin_access', '2018-11-30 01:05:55', '2018-11-30 01:05:55'),
(96, 'delete_mums_admin_access', 'mums_admin_access', '2018-11-30 01:05:55', '2018-11-30 01:05:55');

-- --------------------------------------------------------

--
-- Table structure for table `mums_permission_role`
--

CREATE TABLE `mums_permission_role` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_permission_role`
--

INSERT INTO `mums_permission_role` (`permission_id`, `role_id`) VALUES
(1, 1),
(1, 3),
(1, 4),
(1, 5),
(1, 6),
(1, 7),
(2, 1),
(2, 7),
(3, 1),
(3, 7),
(4, 1),
(4, 4),
(4, 6),
(4, 7),
(5, 1),
(5, 7),
(6, 1),
(6, 7),
(7, 1),
(7, 7),
(8, 1),
(8, 7),
(9, 1),
(9, 7),
(10, 1),
(10, 7),
(11, 1),
(11, 7),
(12, 1),
(12, 7),
(13, 1),
(13, 7),
(14, 1),
(14, 7),
(15, 1),
(15, 7),
(16, 1),
(16, 3),
(16, 5),
(16, 7),
(17, 1),
(17, 3),
(17, 7),
(18, 1),
(18, 7),
(19, 1),
(19, 7),
(20, 1),
(20, 7),
(21, 1),
(21, 7),
(22, 1),
(22, 7),
(23, 1),
(23, 7),
(24, 1),
(24, 7),
(25, 1),
(25, 7),
(26, 1),
(26, 7),
(27, 1),
(27, 7),
(28, 1),
(28, 7),
(29, 1),
(29, 7),
(30, 1),
(30, 7),
(31, 1),
(31, 3),
(31, 4),
(31, 5),
(31, 6),
(31, 7),
(32, 1),
(32, 3),
(32, 4),
(32, 5),
(32, 6),
(32, 7),
(33, 1),
(33, 4),
(33, 7),
(34, 1),
(34, 4),
(34, 7),
(35, 1),
(35, 4),
(35, 7),
(36, 1),
(36, 3),
(36, 4),
(36, 5),
(36, 6),
(36, 7),
(37, 1),
(37, 3),
(37, 4),
(37, 5),
(37, 6),
(37, 7),
(38, 1),
(38, 4),
(38, 7),
(39, 1),
(39, 4),
(39, 7),
(40, 1),
(40, 4),
(40, 7),
(41, 1),
(41, 7),
(47, 1),
(47, 3),
(47, 7),
(48, 1),
(48, 3),
(48, 7),
(49, 1),
(49, 3),
(49, 7),
(50, 1),
(50, 3),
(50, 7),
(51, 1),
(51, 3),
(51, 7),
(52, 1),
(52, 3),
(52, 7),
(53, 1),
(53, 3),
(53, 7),
(54, 1),
(54, 3),
(54, 7),
(55, 1),
(55, 3),
(55, 7),
(56, 1),
(56, 3),
(56, 7),
(62, 1),
(62, 5),
(62, 7),
(63, 1),
(63, 5),
(63, 7),
(64, 1),
(64, 5),
(64, 7),
(65, 1),
(65, 5),
(65, 7),
(66, 1),
(66, 5),
(66, 7),
(67, 1),
(67, 5),
(67, 7),
(68, 1),
(68, 5),
(68, 7),
(69, 1),
(69, 5),
(69, 7),
(70, 1),
(70, 5),
(70, 7),
(71, 1),
(71, 5),
(71, 7),
(72, 1),
(72, 4),
(72, 7),
(73, 1),
(73, 4),
(73, 7),
(74, 1),
(74, 4),
(74, 7),
(75, 1),
(75, 4),
(75, 6),
(75, 7),
(76, 1),
(76, 4),
(76, 7),
(77, 1),
(77, 4),
(77, 7),
(78, 1),
(78, 4),
(78, 7),
(79, 1),
(79, 4),
(79, 7),
(80, 1),
(80, 4),
(80, 7),
(81, 1),
(81, 4),
(81, 7),
(82, 1),
(82, 5),
(82, 7),
(83, 1),
(83, 5),
(83, 7),
(84, 1),
(84, 5),
(84, 7),
(85, 1),
(85, 5),
(85, 7),
(86, 1),
(86, 5),
(86, 7),
(87, 1),
(87, 4),
(87, 6),
(87, 7),
(88, 1),
(88, 4),
(88, 6),
(88, 7),
(89, 1),
(89, 4),
(89, 7),
(90, 1),
(90, 4),
(90, 6),
(90, 7),
(91, 1),
(91, 4),
(91, 7),
(92, 1),
(93, 1),
(94, 1),
(95, 1),
(96, 1);

-- --------------------------------------------------------

--
-- Table structure for table `mums_posts`
--

CREATE TABLE `mums_posts` (
  `id` int(10) UNSIGNED NOT NULL,
  `author_id` int(11) NOT NULL,
  `category_id` int(11) DEFAULT NULL,
  `title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `seo_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `body` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `meta_description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `meta_keywords` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `status` enum('PUBLISHED','DRAFT','PENDING') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'DRAFT',
  `featured` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_posts`
--

INSERT INTO `mums_posts` (`id`, `author_id`, `category_id`, `title`, `seo_title`, `excerpt`, `body`, `image`, `slug`, `meta_description`, `meta_keywords`, `status`, `featured`, `created_at`, `updated_at`) VALUES
(1, 0, NULL, 'Lorem Ipsum Post', NULL, 'This is the excerpt for the Lorem Ipsum Post', '<p>This is the body of the lorem ipsum post</p>', 'posts/post1.jpg', 'lorem-ipsum-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(2, 0, NULL, 'My Sample Post', NULL, 'This is the excerpt for the sample Post', '<p>This is the body for the sample post, which includes the body.</p>\n                <h2>We can use all kinds of format!</h2>\n                <p>And include a bunch of other stuff.</p>', 'posts/post2.jpg', 'my-sample-post', 'Meta Description for sample post', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(3, 0, NULL, 'Latest Post', NULL, 'This is the excerpt for the latest post', '<p>This is the body for the latest post</p>', 'posts/post3.jpg', 'latest-post', 'This is the meta description', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-09 22:49:45', '2018-11-09 22:49:45'),
(4, 0, NULL, 'Yarr Post', NULL, 'Reef sails nipperkin bring a spring upon her cable coffer jury mast spike marooned Pieces of Eight poop deck pillage. Clipper driver coxswain galleon hempen halter come about pressgang gangplank boatswain swing the lead. Nipperkin yard skysail swab lanyard Blimey bilge water ho quarter Buccaneer.', '<p>Swab deadlights Buccaneer fire ship square-rigged dance the hempen jig weigh anchor cackle fruit grog furl. Crack Jennys tea cup chase guns pressgang hearties spirits hogshead Gold Road six pounders fathom measured fer yer chains. Main sheet provost come about trysail barkadeer crimp scuttle mizzenmast brig plunder.</p>\n<p>Mizzen league keelhaul galleon tender cog chase Barbary Coast doubloon crack Jennys tea cup. Blow the man down lugsail fire ship pinnace cackle fruit line warp Admiral of the Black strike colors doubloon. Tackle Jack Ketch come about crimp rum draft scuppers run a shot across the bow haul wind maroon.</p>\n<p>Interloper heave down list driver pressgang holystone scuppers tackle scallywag bilged on her anchor. Jack Tar interloper draught grapple mizzenmast hulk knave cable transom hogshead. Gaff pillage to go on account grog aft chase guns piracy yardarm knave clap of thunder.</p>', 'posts/post4.jpg', 'yarr-post', 'this be a meta descript', 'keyword1, keyword2, keyword3', 'PUBLISHED', 0, '2018-11-09 22:49:45', '2018-11-09 22:49:45');

-- --------------------------------------------------------

--
-- Table structure for table `mums_roles`
--

CREATE TABLE `mums_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_roles`
--

INSERT INTO `mums_roles` (`id`, `name`, `display_name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Administrator', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(2, 'user', 'Normal User', '2018-11-09 22:49:38', '2018-11-09 22:49:38'),
(3, 'forum_moderator', 'Forum Moderator', '2018-11-29 18:14:42', '2018-11-29 18:14:42'),
(4, 'registrar', 'Registrar', '2018-11-29 18:15:15', '2018-11-29 18:15:15'),
(5, 'accounts', 'Accounts', '2018-11-29 18:16:37', '2018-11-29 18:16:37'),
(6, 'hod', 'HOD', '2018-11-29 18:19:38', '2018-11-29 18:19:38'),
(7, 'administrator', 'Administrator', '2018-11-29 18:22:12', '2018-11-29 18:22:12');

-- --------------------------------------------------------

--
-- Table structure for table `mums_school_calendar`
--

CREATE TABLE `mums_school_calendar` (
  `id` int(11) NOT NULL,
  `title` varchar(185) NOT NULL,
  `start` datetime NOT NULL,
  `end` datetime NOT NULL,
  `event_type` varchar(185) NOT NULL DEFAULT 'Academic',
  `event_status` varchar(185) NOT NULL DEFAULT 'Hidden',
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_school_calendar`
--

INSERT INTO `mums_school_calendar` (`id`, `title`, `start`, `end`, `event_type`, `event_status`, `created_at`) VALUES
(1, 'Fresh student come into Residence', '2018-07-13 00:00:00', '2018-08-16 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(2, 'Registration of Fresh Students begins', '2018-07-14 00:00:00', '2018-08-16 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(3, 'Returning (old) students come into residence', '2018-07-20 00:00:00', '2018-08-16 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(4, 'Registration of returning students begins/Orientation Programme for fresh students', '2018-07-21 00:00:00', '2018-08-16 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(5, 'Senate', '2018-07-24 00:00:00', '2018-07-24 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(6, 'Lectures commence for all students/Lectures Week 1', '2018-07-28 00:00:00', '2018-08-03 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(7, 'Convocation', '2018-08-03 00:00:00', '2018-08-08 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(8, 'Lectures Week 2', '2018-08-05 00:00:00', '2018-08-10 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(9, 'Lectures Week 3', '2018-08-12 00:00:00', '2018-08-17 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(10, 'End of Registration', '2018-08-16 00:00:00', '2018-08-16 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(11, 'Lectures Week 4', '2018-08-19 00:00:00', '2018-08-24 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(12, 'Christmas and New Year Break', '2018-08-23 00:00:00', '2018-09-01 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(13, 'All students return to campus', '2018-09-02 00:00:00', '2018-09-02 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(14, 'Lectures Week 5', '2018-09-03 00:00:00', '2018-09-08 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(15, 'Lectures Week 6', '2018-09-09 00:00:00', '2018-09-14 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(16, 'Lectures Week 7', '2018-09-16 00:00:00', '2018-09-21 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(17, 'Lectures Week 8', '2018-09-23 00:00:00', '2018-09-28 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(18, 'Senate', '2018-09-26 00:00:00', '2018-09-26 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(19, 'Lectures Week 9', '2018-09-30 00:00:00', '2018-10-04 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(20, 'Lectures Week 10', '2018-10-06 00:00:00', '2018-10-11 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(21, 'Matriculation', '2018-10-10 00:00:00', '2018-10-10 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(22, 'Lectures Week 11', '2018-10-13 00:00:00', '2018-10-18 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(23, 'Lectures Week 12', '2018-10-20 00:00:00', '2018-10-25 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(24, 'Senate', '2018-10-23 00:00:00', '2018-10-23 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(25, 'Lectures Week 13', '2018-10-27 00:00:00', '2018-11-04 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(26, 'Revision', '2018-11-06 00:00:00', '2018-11-11 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(27, 'Harmattan Semester Examination Begin', '2018-11-13 00:00:00', '2018-11-18 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(28, 'Harmattan Semester Examination Continues', '2018-11-20 00:00:00', '2018-11-25 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(29, 'Harmattan Semester Examination Continues', '2018-11-27 00:00:00', '2018-12-01 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(30, 'Senate', '2018-11-30 00:00:00', '2018-11-30 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(31, 'End of Harmattan Semester', '2018-12-01 00:00:00', '2018-12-01 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(32, 'Harmattan Semester Break', '2018-12-03 00:00:00', '2018-12-21 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(33, 'All students come into Residence', '2018-12-23 00:00:00', '2018-12-23 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(34, 'Lectures Commence/Lectures Week I', '2018-12-24 00:00:00', '2018-12-29 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(35, 'Senate', '2018-12-27 00:00:00', '2018-12-27 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(36, 'Workers Day', '2019-01-01 00:00:00', '2019-01-01 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(37, 'Lectures Week 2', '2019-01-02 00:00:00', '2019-01-07 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(38, 'lectures Week 3', '2019-01-08 00:00:00', '2019-01-13 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(39, 'Lectures Week 4', '2019-01-15 00:00:00', '2019-01-20 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(40, 'Lectures Week 5', '2019-01-22 00:00:00', '2019-01-27 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(41, 'Senate', '2019-01-25 00:00:00', '2019-01-25 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(42, 'Lectures Week 6', '2019-01-29 00:00:00', '2019-02-03 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(43, 'Lectures Week 7', '2019-02-05 00:00:00', '2019-02-10 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(44, 'Lectures Week 8', '2019-02-12 00:00:00', '2019-02-17 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(45, 'Lectures Week 9', '2019-02-19 00:00:00', '2019-02-24 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(46, 'Lectures Week 10', '2019-02-26 00:00:00', '2019-03-01 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(47, 'Senate', '2019-02-28 00:00:00', '2019-02-28 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(48, 'Lectures Week 11', '2019-03-03 00:00:00', '2019-03-08 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(49, 'Lectures Week 12', '2019-03-10 00:00:00', '2019-03-15 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(50, 'Lectures Week 13', '2019-03-17 00:00:00', '2019-03-22 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(51, 'Revision', '2019-03-24 00:00:00', '2019-03-28 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(52, 'Senate', '2019-03-27 00:00:00', '2019-03-27 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(53, 'Rain Senate Examination Begin', '2019-03-28 00:00:00', '2019-04-05 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(54, 'Rain Semester Examination Continue', '2019-04-07 00:00:00', '2019-04-12 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(55, 'End of Rain Semester', '2019-04-12 00:00:00', '2019-04-12 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(56, 'Vacation', '2019-04-14 00:00:00', '2019-06-21 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(57, '2017/2018 Academic Session Begins', '2019-06-23 00:00:00', '2019-06-23 00:00:00', 'Academic', 'Hidden', '2018-01-02 20:53:27'),
(58, 'last_session', '2017-07-12 00:00:00', '2018-06-23 00:00:00', 'settings', 'Hidden', '2018-01-02 20:53:27'),
(59, 'current_session', '2018-07-13 00:00:00', '2019-06-23 00:00:00', 'settings', 'Hidden', '2018-01-02 20:53:27'),
(60, 'registration_period', '2018-12-14 00:00:00', '2019-05-30 00:00:00', 'settings', 'Hidden', '2018-01-02 20:53:27'),
(61, 'first_semester', '2018-07-13 00:00:00', '2018-11-21 00:00:00', 'settings', 'Hidden', '2018-01-02 20:53:27'),
(62, 'second_semester', '2018-12-01 00:00:00', '2019-06-12 00:00:00', 'settings', 'Hidden', '2018-01-02 20:53:27');

-- --------------------------------------------------------

--
-- Table structure for table `mums_sessions`
--

CREATE TABLE `mums_sessions` (
  `id` varchar(185) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `ip_address` varchar(45) DEFAULT NULL,
  `user_agent` longtext DEFAULT NULL,
  `payload` longtext NOT NULL,
  `last_activity` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mums_settings`
--

CREATE TABLE `mums_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `key` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL DEFAULT 1,
  `group` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_settings`
--

INSERT INTO `mums_settings` (`id`, `key`, `display_name`, `value`, `details`, `type`, `order`, `group`) VALUES
(1, 'site.title', 'Site Title', 'Site Title', '', 'text', 1, 'Site'),
(2, 'site.description', 'Site Description', 'Site Description', '', 'text', 2, 'Site'),
(3, 'site.logo', 'Site Logo', '', '', 'image', 3, 'Site'),
(4, 'site.google_analytics_tracking_id', 'Google Analytics Tracking ID', 'AIzaSyAa-q1QMtfA6inWAMD9z677Tr1NtlmmPlY', '', 'text', 4, 'Site'),
(5, 'admin.bg_image', 'Admin Background Image', '', '', 'image', 5, 'Admin'),
(6, 'admin.title', 'Admin Title', 'Mahadum', '', 'text', 1, 'Admin'),
(7, 'admin.description', 'Admin Description', 'Welcome to Mahadum Admin Panel. Control the App from the backend', '', 'text', 2, 'Admin'),
(8, 'admin.loader', 'Admin Loader', '', '', 'image', 3, 'Admin'),
(9, 'admin.icon_image', 'Admin Icon Image', '', '', 'image', 4, 'Admin'),
(10, 'admin.google_analytics_client_id', 'Google Analytics Client ID (used for admin dashboard)', 'AIzaSyAa-q1QMtfA6inWAMD9z677Tr1NtlmmPlY', '', 'text', 1, 'Admin'),
(11, 'admin.first_session', 'First Session', '2013', NULL, 'text', 6, 'Admin'),
(12, 'admin.current_session', 'Current Session', '2018', NULL, 'text', 7, 'Admin');

-- --------------------------------------------------------

--
-- Table structure for table `mums_time_table`
--

CREATE TABLE `mums_time_table` (
  `id` int(11) NOT NULL,
  `week_day` varchar(255) DEFAULT NULL,
  `location` varchar(255) DEFAULT NULL,
  `start_time` time DEFAULT NULL,
  `end_time` time DEFAULT NULL,
  `course_code` varchar(6) DEFAULT NULL,
  `system_course_id` varchar(75) DEFAULT NULL,
  `level` int(11) NOT NULL,
  `course_source` varchar(5) DEFAULT NULL,
  `faculty` varchar(5) DEFAULT NULL,
  `dept` varchar(5) DEFAULT NULL,
  `lecturer` varchar(12) DEFAULT NULL,
  `session` int(11) NOT NULL,
  `semester` int(11) NOT NULL,
  `created_at` datetime DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_time_table`
--

INSERT INTO `mums_time_table` (`id`, `week_day`, `location`, `start_time`, `end_time`, `course_code`, `system_course_id`, `level`, `course_source`, `faculty`, `dept`, `lecturer`, `session`, `semester`, `created_at`) VALUES
(1, 'Monday', 'SEET COMPLEX, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SEET_CHE', 100, 'MTH', 'SEET', 'CHE', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(2, 'Monday', 'SEET COMPLEX, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SEET_CIE', 100, 'MTH', 'SEET', 'CIE', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(3, 'Monday', 'SEET COMPLEX, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SEET_MEE', 100, 'MTH', 'SEET', 'MEE', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(4, 'Monday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SEET_CHE', 100, 'GST', 'SEET', 'CHE', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(5, 'Monday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SEET_CIE', 100, 'GST', 'SEET', 'CIE', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(6, 'Monday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SEET_MEE', 100, 'GST', 'SEET', 'MEE', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(7, 'Monday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SEET_CHE', 100, 'CHM', 'SEET', 'CHE', 'SP1427', 2016, 1, '2018-01-02 20:53:02'),
(8, 'Monday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SEET_CIE', 100, 'CHM', 'SEET', 'CIE', 'SP1427', 2016, 1, '2018-01-02 20:53:02'),
(9, 'Monday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SEET_MEE', 100, 'CHM', 'SEET', 'MEE', 'SP1427', 2016, 1, '2018-01-02 20:53:02'),
(10, 'Tuesday', 'SEET COMPLEX, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SEET_CHE', 100, 'BIO', 'SEET', 'CHE', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(11, 'Tuesday', 'SEET COMPLEX, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SEET_CIE', 100, 'BIO', 'SEET', 'CIE', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(12, 'Tuesday', 'SEET COMPLEX, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SEET_MEE', 100, 'BIO', 'SEET', 'MEE', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(13, 'Tuesday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SEET_CHE', 100, 'MEE', 'SEET', 'CHE', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(14, 'Tuesday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SEET_CIE', 100, 'MEE', 'SEET', 'CIE', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(15, 'Tuesday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SEET_MEE', 100, 'MEE', 'SEET', 'MEE', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(16, 'Wednesday', 'SEET COMPLEX, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SEET_CHE', 100, 'GST', 'SEET', 'CHE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(17, 'Wednesday', 'SEET COMPLEX, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SEET_CIE', 100, 'GST', 'SEET', 'CIE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(18, 'Wednesday', 'SEET COMPLEX, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SEET_MEE', 100, 'GST', 'SEET', 'MEE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(19, 'Wednesday', 'SEET COMPLEX, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SEET_CHE', 100, 'CHM', 'SEET', 'CHE', 'SP1427', 2016, 1, '2018-01-02 20:53:02'),
(20, 'Wednesday', 'SEET COMPLEX, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SEET_CIE', 100, 'CHM', 'SEET', 'CIE', 'SP1427', 2016, 1, '2018-01-02 20:53:02'),
(21, 'Wednesday', 'SEET COMPLEX, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SEET_MEE', 100, 'CHM', 'SEET', 'MEE', 'SP1427', 2016, 1, '2018-01-02 20:53:02'),
(22, 'Wednesday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SEET_CHE', 100, 'MEE', 'SEET', 'CHE', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(23, 'Wednesday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SEET_CIE', 100, 'MEE', 'SEET', 'CIE', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(24, 'Wednesday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SEET_MEE', 100, 'MEE', 'SEET', 'MEE', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(25, 'Thursday', 'SEET COMPLEX, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SEET_CHE', 100, 'PHY', 'SEET', 'CHE', 'SP1162', 2016, 1, '2018-01-02 20:53:02'),
(26, 'Thursday', 'SEET COMPLEX, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SEET_CIE', 100, 'PHY', 'SEET', 'CIE', 'SP1162', 2016, 1, '2018-01-02 20:53:02'),
(27, 'Thursday', 'SEET COMPLEX, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SEET_MEE', 100, 'PHY', 'SEET', 'MEE', 'SP1162', 2016, 1, '2018-01-02 20:53:02'),
(28, 'Thursday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SEET_CHE', 100, 'BIO', 'SEET', 'CHE', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(29, 'Thursday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SEET_CIE', 100, 'BIO', 'SEET', 'CIE', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(30, 'Thursday', 'SEET COMPLEX, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SEET_MEE', 100, 'BIO', 'SEET', 'MEE', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(31, 'Friday', 'SEET COMPLEX, HALL B', '08:30:00', '09:30:00', 'MTH101', 'MTH101_MTH_SEET_CHE', 100, 'MTH', 'SEET', 'CHE', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(32, 'Friday', 'SEET COMPLEX, HALL B', '08:30:00', '09:30:00', 'MTH101', 'MTH101_MTH_SEET_CIE', 100, 'MTH', 'SEET', 'CIE', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(33, 'Friday', 'SEET COMPLEX, HALL B', '08:30:00', '09:30:00', 'MTH101', 'MTH101_MTH_SEET_MEE', 100, 'MTH', 'SEET', 'MEE', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(34, 'Friday', 'SEET COMPLEX, HALL B', '09:30:00', '10:30:00', 'GST101', 'GST101_GST_SEET_CHE', 100, 'GST', 'SEET', 'CHE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(35, 'Friday', 'SEET COMPLEX, HALL B', '09:30:00', '10:30:00', 'GST101', 'GST101_GST_SEET_CIE', 100, 'GST', 'SEET', 'CIE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(36, 'Friday', 'SEET COMPLEX, HALL B', '09:30:00', '10:30:00', 'GST101', 'GST101_GST_SEET_MEE', 100, 'GST', 'SEET', 'MEE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(37, 'Friday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SEET_CHE', 100, 'PHY', 'SEET', 'CHE', 'SP1162', 2016, 1, '2018-01-02 20:53:02'),
(38, 'Friday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SEET_CIE', 100, 'PHY', 'SEET', 'CIE', 'SP1162', 2016, 1, '2018-01-02 20:53:02'),
(39, 'Friday', 'SEET COMPLEX, HALL B', '10:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SEET_MEE', 100, 'PHY', 'SEET', 'MEE', 'SP1162', 2016, 1, '2018-01-02 20:53:02'),
(40, 'Monday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_BIO', 100, 'GST', 'SOSC', 'BIO', 'SP1472', 2016, 1, '2018-01-02 20:53:02'),
(41, 'Monday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_PHY', 100, 'GST', 'SOSC', 'PHY', 'SP1472', 2016, 1, '2018-01-02 20:53:02'),
(42, 'Monday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_CHM', 100, 'GST', 'SOSC', 'CHM', 'SP1472', 2016, 1, '2018-01-02 20:53:02'),
(43, 'Monday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_MTH', 100, 'GST', 'SOSC', 'MTH', 'SP1472', 2016, 1, '2018-01-02 20:53:02'),
(44, 'Monday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SOSC_BIO', 100, 'CHM', 'SOSC', 'BIO', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(45, 'Monday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SOSC_PHY', 100, 'CHM', 'SOSC', 'PHY', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(46, 'Monday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SOSC_CHM', 100, 'CHM', 'SOSC', 'CHM', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(47, 'Monday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SOSC_MTH', 100, 'CHM', 'SOSC', 'MTH', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(48, 'Monday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SOSC_BIO', 100, 'MEE', 'SOSC', 'BIO', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(49, 'Monday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SOSC_PHY', 100, 'MEE', 'SOSC', 'PHY', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(50, 'Monday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SOSC_CHM', 100, 'MEE', 'SOSC', 'CHM', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(51, 'Monday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SOSC_MTH', 100, 'MEE', 'SOSC', 'MTH', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(52, 'Tuesday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SOSC_BIO', 100, 'PHY', 'SOSC', 'BIO', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(53, 'Tuesday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SOSC_PHY', 100, 'PHY', 'SOSC', 'PHY', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(54, 'Tuesday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SOSC_CHM', 100, 'PHY', 'SOSC', 'CHM', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(55, 'Tuesday', 'SOSC ARENA, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SOSC_MTH', 100, 'PHY', 'SOSC', 'MTH', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(56, 'Tuesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SOSC_BIO', 100, 'BIO', 'SOSC', 'BIO', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(57, 'Tuesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SOSC_PHY', 100, 'BIO', 'SOSC', 'PHY', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(58, 'Tuesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SOSC_CHM', 100, 'BIO', 'SOSC', 'CHM', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(59, 'Tuesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SOSC_MTH', 100, 'BIO', 'SOSC', 'MTH', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(60, 'Wednesday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_BIO', 100, 'MTH', 'SOSC', 'BIO', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(61, 'Wednesday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_PHY', 100, 'MTH', 'SOSC', 'PHY', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(62, 'Wednesday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_CHM', 100, 'MTH', 'SOSC', 'CHM', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(63, 'Wednesday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_MTH', 100, 'MTH', 'SOSC', 'MTH', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(64, 'Wednesday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SOSC_BIO', 100, 'GST', 'SOSC', 'BIO', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(65, 'Wednesday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SOSC_PHY', 100, 'GST', 'SOSC', 'PHY', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(66, 'Wednesday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SOSC_CHM', 100, 'GST', 'SOSC', 'CHM', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(67, 'Wednesday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SOSC_MTH', 100, 'GST', 'SOSC', 'MTH', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(68, 'Wednesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SOSC_BIO', 100, 'CHM', 'SOSC', 'BIO', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(69, 'Wednesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SOSC_PHY', 100, 'CHM', 'SOSC', 'PHY', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(70, 'Wednesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SOSC_CHM', 100, 'CHM', 'SOSC', 'CHM', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(71, 'Wednesday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SOSC_MTH', 100, 'CHM', 'SOSC', 'MTH', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(72, 'Thursday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SOSC_BIO', 100, 'BIO', 'SOSC', 'BIO', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(73, 'Thursday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SOSC_PHY', 100, 'BIO', 'SOSC', 'PHY', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(74, 'Thursday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SOSC_CHM', 100, 'BIO', 'SOSC', 'CHM', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(75, 'Thursday', 'SOSC ARENA, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SOSC_MTH', 100, 'BIO', 'SOSC', 'MTH', 'SP1522', 2016, 1, '2018-01-02 20:53:02'),
(76, 'Thursday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SOSC_BIO', 100, 'MEE', 'SOSC', 'BIO', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(77, 'Thursday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SOSC_PHY', 100, 'MEE', 'SOSC', 'PHY', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(78, 'Thursday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SOSC_CHM', 100, 'MEE', 'SOSC', 'CHM', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(79, 'Thursday', 'SOSC ARENA, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SOSC_MTH', 100, 'MEE', 'SOSC', 'MTH', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(80, 'Friday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_BIO', 100, 'GST', 'SOSC', 'BIO', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(81, 'Friday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_PHY', 100, 'GST', 'SOSC', 'PHY', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(82, 'Friday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_CHM', 100, 'GST', 'SOSC', 'CHM', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(83, 'Friday', 'SOSC ARENA, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SOSC_MTH', 100, 'GST', 'SOSC', 'MTH', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(84, 'Friday', 'SOSC ARENA, HALL B', '09:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_BIO', 0, 'MTH', 'SOSC', 'BIO', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(85, 'Friday', 'SOSC ARENA, HALL B', '09:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_PHY', 0, 'MTH', 'SOSC', 'PHY', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(86, 'Friday', 'SOSC ARENA, HALL B', '09:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_CHM', 0, 'MTH', 'SOSC', 'CHM', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(87, 'Friday', 'SOSC ARENA, HALL B', '09:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SOSC_MTH', 0, 'MTH', 'SOSC', 'MTH', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(88, 'Friday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'PHY101', 'PHY101_PHY_SOSC_BIO', 100, 'PHY', 'SOSC', 'BIO', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(89, 'Friday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'PHY101', 'PHY101_PHY_SOSC_PHY', 100, 'PHY', 'SOSC', 'PHY', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(90, 'Friday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'PHY101', 'PHY101_PHY_SOSC_CHM', 100, 'PHY', 'SOSC', 'CHM', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(91, 'Friday', 'SOSC ARENA, HALL B', '11:30:00', '12:30:00', 'PHY101', 'PHY101_PHY_SOSC_MTH', 100, 'PHY', 'SOSC', 'MTH', 'SP1442', 2016, 1, '2018-01-02 20:53:02'),
(92, 'Monday', 'SMAT ANNEX, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SMAT_IMT', 100, 'MTH', 'SMAT', 'IMT', 'SP1242', 2016, 1, '2018-01-02 20:53:02'),
(93, 'Monday', 'SMAT ANNEX, HALL B', '08:30:00', '10:30:00', 'MTH101', 'MTH101_MTH_SMAT_PMT', 100, 'MTH', 'SMAT', 'PMT', 'SP1242', 2016, 1, '2018-01-02 20:53:02'),
(94, 'Monday', 'SMAT ANNEX, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SMAT_IMT', 100, 'GST', 'SMAT', 'IMT', 'SP1472', 2016, 1, '2018-01-02 20:53:02'),
(95, 'Monday', 'SMAT ANNEX, HALL B', '10:30:00', '11:30:00', 'GST103', 'GST103_GST_SMAT_PMT', 100, 'GST', 'SMAT', 'PMT', 'SP1472', 2016, 1, '2018-01-02 20:53:02'),
(96, 'Monday', 'SMAT ANNEX, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SMAT_IMT', 100, 'CHM', 'SMAT', 'IMT', 'SP1447', 2016, 1, '2018-01-02 20:53:02'),
(97, 'Monday', 'SMAT ANNEX, HALL B', '11:30:00', '12:30:00', 'CHM101', 'CHM101_CHM_SMAT_PMT', 100, 'CHM', 'SMAT', 'PMT', 'SP1447', 2016, 1, '2018-01-02 20:53:02'),
(98, 'Tuesday', 'SMAT ANNEX, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SMAT_IMT', 100, 'BIO', 'SMAT', 'IMT', 'SP1382', 2016, 1, '2018-01-02 20:53:02'),
(99, 'Tuesday', 'SMAT ANNEX, HALL B', '08:30:00', '10:30:00', 'BIO101', 'BIO101_BIO_SMAT_PMT', 100, 'BIO', 'SMAT', 'PMT', 'SP1382', 2016, 1, '2018-01-02 20:53:02'),
(100, 'Tuesday', 'SMAT ANNEX, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SMAT_IMT', 100, 'MEE', 'SMAT', 'IMT', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(101, 'Tuesday', 'SMAT ANNEX, HALL B', '10:30:00', '11:30:00', 'ENG103', 'ENG103_MEE_SMAT_PMT', 100, 'MEE', 'SMAT', 'PMT', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(102, 'Wednesday', 'SMAT ANNEX, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SMAT_IMT', 100, 'GST', 'SMAT', 'IMT', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(103, 'Wednesday', 'SMAT ANNEX, HALL B', '08:30:00', '09:30:00', 'GST101', 'GST101_GST_SMAT_PMT', 100, 'GST', 'SMAT', 'PMT', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(104, 'Wednesday', 'SMAT ANNEX, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SMAT_IMT', 100, 'CHM', 'SMAT', 'IMT', 'SP1447', 2016, 1, '2018-01-02 20:53:02'),
(105, 'Wednesday', 'SMAT ANNEX, HALL B', '09:30:00', '11:30:00', 'CHM101', 'CHM101_CHM_SMAT_PMT', 100, 'CHM', 'SMAT', 'PMT', 'SP1447', 2016, 1, '2018-01-02 20:53:02'),
(106, 'Wednesday', 'SMAT ANNEX, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SMAT_IMT', 100, 'MEE', 'SMAT', 'IMT', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(107, 'Wednesday', 'SMAT ANNEX, HALL B', '11:30:00', '12:30:00', 'ENG101', 'ENG101_MEE_SMAT_PMT', 100, 'MEE', 'SMAT', 'PMT', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(108, 'Thursday', 'SMAT ANNEX, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SMAT_IMT', 100, 'PHY', 'SMAT', 'IMT', 'SP1387', 2016, 1, '2018-01-02 20:53:02'),
(109, 'Thursday', 'SMAT ANNEX, HALL B', '09:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SMAT_PMT', 100, 'PHY', 'SMAT', 'PMT', 'SP1387', 2016, 1, '2018-01-02 20:53:02'),
(110, 'Thursday', 'SMAT ANNEX, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SMAT_IMT', 100, 'BIO', 'SMAT', 'IMT', 'SP1382', 2016, 1, '2018-01-02 20:53:02'),
(111, 'Thursday', 'SMAT ANNEX, HALL B', '11:30:00', '12:30:00', 'BIO101', 'BIO101_BIO_SMAT_PMT', 100, 'BIO', 'SMAT', 'PMT', 'SP1382', 2016, 1, '2018-01-02 20:53:02'),
(112, 'Friday', 'SMAT ANNEX, HALL B', '08:30:00', '09:30:00', 'MTH101', 'MTH101_MTH_SMAT_IMT', 100, 'MTH', 'SMAT', 'IMT', 'SP1242', 2016, 1, '2018-01-02 20:53:02'),
(113, 'Friday', 'SMAT ANNEX, HALL B', '08:30:00', '09:30:00', 'MTH101', 'MTH101_MTH_SMAT_PMT', 100, 'MTH', 'SMAT', 'PMT', 'SP1242', 2016, 1, '2018-01-02 20:53:02'),
(114, 'Friday', 'SMAT ANNEX, HALL B', '09:30:00', '10:30:00', 'GST101', 'GST101_GST_SMAT_IMT', 100, 'GST', 'SMAT', 'IMT', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(115, 'Friday', 'SMAT ANNEX, HALL B', '09:30:00', '10:30:00', 'GST101', 'GST101_GST_SMAT_PMT', 100, 'GST', 'SMAT', 'PMT', 'SP1532', 2016, 1, '2018-01-02 20:53:02'),
(116, 'Friday', 'SMAT ANNEX, HALL B', '10:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SMAT_IMT', 100, 'PHY', 'SMAT', 'IMT', 'SP1387', 2016, 1, '2018-01-02 20:53:02'),
(117, 'Friday', 'SMAT ANNEX, HALL B', '10:30:00', '11:30:00', 'PHY101', 'PHY101_PHY_SMAT_PMT', 100, 'PHY', 'SMAT', 'PMT', 'SP1387', 2016, 1, '2018-01-02 20:53:02'),
(118, 'Monday', 'SEET COMPLEX, HALL C', '08:30:00', '09:30:00', 'MTH203', 'MTH203_MTH_SEET_CIE', 200, 'MTH', 'SEET', 'CIE', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(119, 'Monday', 'SEET COMPLEX, HALL C', '08:30:00', '09:30:00', 'MTH203', 'MTH203_MTH_SEET_CIE', 200, 'MTH', 'SEET', 'CIE', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(120, 'Monday', 'SEET COMPLEX, HALL C', '08:30:00', '09:30:00', 'MTH203', 'MTH203_MTH_SEET_MEE', 200, 'MTH', 'SEET', 'MEE', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(121, 'Monday', 'SEET COMPLEX, HALL C', '09:30:00', '10:30:00', 'GST201', 'GST201_GST_SEET_CHE', 200, 'GST', 'SEET', 'CHE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(122, 'Monday', 'SEET COMPLEX, HALL C', '09:30:00', '10:30:00', 'GST201', 'GST201_GST_SEET_CIE', 200, 'GST', 'SEET', 'CIE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(123, 'Monday', 'SEET COMPLEX, HALL C', '09:30:00', '10:30:00', 'GST201', 'GST201_GST_SEET_MEE', 200, 'GST', 'SEET', 'MEE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(124, 'Monday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'ENG203', 'ENG203_CHE_SEET_CHE', 200, 'CHE', 'SEET', 'CHE', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(125, 'Monday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'ENG203', 'ENG203_CHE_SEET_CIE', 200, 'CHE', 'SEET', 'CIE', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(126, 'Monday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'ENG203', 'ENG203_CHE_SEET_MEE', 200, 'CHE', 'SEET', 'MEE', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(127, 'Monday', 'SEET COMPLEX, ROOM A7', '08:30:00', '09:30:00', 'CIE201', 'CIE201_CIE_SEET_CIE', 200, 'CIE', 'SEET', 'CIE', 'SP1437', 2016, 1, '2018-01-02 20:53:02'),
(128, 'Tuesday', 'SEET COMPLEX, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SEET_CIE', 200, 'IMT', 'SEET', 'CIE', 'SP1462', 2016, 1, '2018-01-02 20:53:02'),
(129, 'Tuesday', 'SEET COMPLEX, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SEET_MEE', 200, 'IMT', 'SEET', 'MEE', 'SP1462', 2016, 1, '2018-01-02 20:53:02'),
(130, 'Tuesday', 'SEET COMPLEX, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SEET_CHE', 200, 'IMT', 'SEET', 'CHE', 'SP1462', 2016, 1, '2018-01-02 20:53:02'),
(131, 'Tuesday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'MTH201', 'MTH201_MTH_SEET_CIE', 200, 'MTH', 'SEET', 'CIE', 'SP1452', 2016, 1, '2018-01-02 20:53:02'),
(132, 'Tuesday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'MTH201', 'MTH201_MTH_SEET_MEE', 200, 'MTH', 'SEET', 'MEE', 'SP1452', 2016, 1, '2018-01-02 20:53:02'),
(133, 'Tuesday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'MTH201', 'MTH201_MTH_SEET_CHE', 200, 'MTH', 'SEET', 'CHE', 'SP1452', 2016, 1, '2018-01-02 20:53:02'),
(134, 'Tuesday', 'SEET COMPLEX, HALL C', '12:30:00', '13:30:00', 'ENG207', 'ENG207_MEE_SEET_CHE', 200, 'MEE', 'SEET', 'CHE', 'SP1562', 2016, 1, '2018-01-02 20:53:02'),
(135, 'Tuesday', 'SEET COMPLEX, HALL C', '12:30:00', '13:30:00', 'ENG207', 'ENG207_MEE_SEET_MEE', 200, 'MEE', 'SEET', 'MEE', 'SP1562', 2016, 1, '2018-01-02 20:53:02'),
(136, 'Tuesday', 'SEET COMPLEX, HALL C', '12:30:00', '13:30:00', 'ENG207', 'ENG207_MEE_SEET_CIE', 200, 'MEE', 'SEET', 'CIE', 'SP1562', 2016, 1, '2018-01-02 20:53:02'),
(137, 'Wednesday', 'SEET COMPLEX, ROOM A1', '08:30:00', '09:30:00', 'CHE201', 'CHE201_CHE_SEET_CHE', 200, 'CHE', 'SEET', 'CHE', 'SP1062', 2016, 1, '2018-01-02 20:53:02'),
(138, 'Wednesday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG203', 'ENG203_CHE_SEET_MEE', 200, 'CHE', 'SEET', 'MEE', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(139, 'Wednesday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG203', 'ENG203_CHE_SEET_CHE', 200, 'CHE', 'SEET', 'CHE', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(140, 'Wednesday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG203', 'ENG203_CHE_SEET_CIE', 200, 'CHE', 'SEET', 'CIE', 'SP1147', 2016, 1, '2018-01-02 20:53:02'),
(141, 'Wednesday', 'SEET COMPLEX, HALL C', '12:30:00', '13:30:00', 'GST201', 'GST201_GST_SEET_CHE', 200, 'GST', 'SEET', 'CHE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(142, 'Wednesday', 'SEET COMPLEX, HALL C', '12:30:00', '13:30:00', 'GST201', 'GST201_GST_SEET_CIE', 200, 'GST', 'SEET', 'CIE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(143, 'Wednesday', 'SEET COMPLEX, HALL C', '12:30:00', '13:30:00', 'GST201', 'GST201_GST_SEET_CIE', 200, 'GST', 'SEET', 'CIE', 'SP1577', 2016, 1, '2018-01-02 20:53:02'),
(144, 'Thursday', 'SEET COMPLEX, ROOM A1', '08:30:00', '09:30:00', 'CHE201', 'CHE201_CHE_SEET_CHE', 200, 'CHE', 'SEET', 'CHE', 'SP1062', 2016, 1, '2018-01-02 20:53:02'),
(145, 'Thursday', 'SEET COMPLEX, ROOM A7', '09:30:00', '11:30:00', 'CIE201', 'CIE201_CIE_SEET_CIE', 200, 'CIE', 'SEET', 'CIE', 'SP1437', 2016, 1, '2018-01-02 20:53:02'),
(146, 'Thursday', 'SEET COMPLEX, HALL C', '11:30:00', '12:30:00', 'CSC201', 'CSC201_IMT_SEET_MEE', 200, 'IMT', 'SEET', 'MEE', 'SP1462', 2016, 1, '2018-01-02 20:53:02'),
(147, 'Thursday', 'SEET COMPLEX, HALL C', '11:30:00', '12:30:00', 'CSC201', 'CSC201_IMT_SEET_MEE', 200, 'IMT', 'SEET', 'MEE', 'SP1462', 2016, 1, '2018-01-02 20:53:02'),
(148, 'Thursday', 'SEET COMPLEX, HALL C', '11:30:00', '12:30:00', 'CSC201', 'CSC201_IMT_SEET_CHE', 200, 'IMT', 'SEET', 'CHE', 'SP1462', 2016, 1, '2018-01-02 20:53:02'),
(149, 'Thursday', 'SEET COMPLEX, ROOM A3', '13:30:00', '15:30:00', 'MEE201', 'MEE201_MEE_SEET_MEE', 200, 'MEE', 'SEET', 'MEE', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(150, 'Friday', 'SEET COMPLEX, HALL C', '08:30:00', '10:30:00', 'MTH203', 'MTH203_MTH_SEET_MEE', 200, 'MTH', 'SEET', 'MEE', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(151, 'Friday', 'SEET COMPLEX, HALL C', '08:30:00', '10:30:00', 'MTH203', 'MTH203_MTH_SEET_CHE', 200, 'MTH', 'SEET', 'CHE', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(152, 'Friday', 'SEET COMPLEX, HALL C', '08:30:00', '10:30:00', 'MTH203', 'MTH203_MTH_SEET_CIE', 200, 'MTH', 'SEET', 'CIE', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(153, 'Friday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG201', 'ENG201_CIE_SEET_MEE', 200, 'CIE', 'SEET', 'MEE', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(154, 'Friday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG201', 'ENG201_CIE_SEET_CHE', 200, 'CIE', 'SEET', 'CHE', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(155, 'Friday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG201', 'ENG201_CIE_SEET_CIE', 200, 'CIE', 'SEET', 'CIE', 'SP1152', 2016, 1, '2018-01-02 20:53:02'),
(156, 'Friday', 'SEET COMPLEX, ROOM A3', '08:30:00', '09:30:00', 'MEE201', 'MEE201_MEE_SEET_MEE', 200, 'MEE', 'SEET', 'MEE', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(157, 'Monday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_BIO', 200, 'GST', 'SOSC', 'BIO', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(158, 'Monday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_CHM', 200, 'GST', 'SOSC', 'CHM', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(159, 'Monday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_MTH', 200, 'GST', 'SOSC', 'MTH', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(160, 'Monday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_PHY', 200, 'GST', 'SOSC', 'PHY', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(161, 'Monday', 'SOSC ARENA, HALL C', '09:30:00', '11:30:00', 'CSC201', 'CSC201_IMT_SOSC_BIO', 200, 'IMT', 'SOSC', 'BIO', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(162, 'Monday', 'SOSC ARENA, HALL C', '09:30:00', '11:30:00', 'CSC201', 'CSC201_IMT_SOSC_CHM', 200, 'IMT', 'SOSC', 'CHM', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(163, 'Monday', 'SOSC ARENA, HALL C', '09:30:00', '11:30:00', 'CSC201', 'CSC201_IMT_SOSC_MTH', 200, 'IMT', 'SOSC', 'MTH', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(164, 'Monday', 'SOSC ARENA, HALL C', '09:30:00', '11:30:00', 'CSC201', 'CSC201_IMT_SOSC_PHY', 200, 'IMT', 'SOSC', 'PHY', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(165, 'Monday', 'SOSC ARENA, ROOM A7', '11:30:00', '13:30:00', 'BIO201', 'BIO201_BIO_SOSC_BIO', 200, 'BIO', 'SOSC', 'BIO', 'SP1272', 2016, 1, '2018-01-02 20:53:02'),
(166, 'Monday', 'SOSC ARENA, ROOM A7', '11:30:00', '13:30:00', 'BIO201', 'BIO201_BIO_SOSC_CHM', 200, 'BIO', 'SOSC', 'CHM', 'SP1272', 2016, 1, '2018-01-02 20:53:02'),
(167, 'Tuesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_BIO', 200, 'MTH', 'SOSC', 'BIO', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(168, 'Tuesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_CHM', 200, 'MTH', 'SOSC', 'CHM', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(169, 'Tuesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_MTH', 200, 'MTH', 'SOSC', 'MTH', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(170, 'Tuesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_PHY', 200, 'MTH', 'SOSC', 'PHY', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(171, 'Tuesday', 'SOSC ARENA, ROOM A9', '10:30:00', '11:30:00', 'MTH211', 'MTH211_MTH_SOSC_MTH', 200, 'MTH', 'SOSC', 'MTH', 'SP1242', 2016, 1, '2018-01-02 20:53:02'),
(172, 'Tuesday', 'SOSC ARENA, ROOM A3', '10:30:00', '11:30:00', 'PHY201', 'PHY201_PHY_SOSC_PHY', 200, 'PHY', 'SOSC', 'PHY', 'SP1197', 2016, 1, '2018-01-02 20:53:02'),
(173, 'Tuesday', 'SOSC ARENA, HALL C', '11:30:00', '12:30:00', 'ENG201', 'ENG201_CIE_SOSC_BIO', 200, 'CIE', 'SOSC', 'BIO', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(174, 'Tuesday', 'SOSC ARENA, HALL C', '11:30:00', '12:30:00', 'ENG201', 'ENG201_CIE_SOSC_CHM', 200, 'CIE', 'SOSC', 'CHM', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(175, 'Tuesday', 'SOSC ARENA, HALL C', '11:30:00', '12:30:00', 'ENG201', 'ENG201_CIE_SOSC_MTH', 200, 'CIE', 'SOSC', 'MTH', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(176, 'Tuesday', 'SOSC ARENA, HALL C', '11:30:00', '12:30:00', 'ENG201', 'ENG201_CIE_SOSC_PHY', 200, 'CIE', 'SOSC', 'PHY', 'SP1392', 2016, 1, '2018-01-02 20:53:02'),
(177, 'Tuesday', 'SOSC ARENA, ROOM A1', '12:30:00', '14:30:00', 'CHM201', 'CHM201_CHM_SOSC_BIO', 200, 'CHM', 'SOSC', 'BIO', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(178, 'Tuesday', 'SOSC ARENA, ROOM A1', '12:30:00', '14:30:00', 'CHM201', 'CHM201_CHM_SOSC_CHM', 200, 'CHM', 'SOSC', 'CHM', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(179, 'Wednesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SOSC_BIO', 200, 'IMT', 'SOSC', 'BIO', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(180, 'Wednesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SOSC_CHM', 200, 'IMT', 'SOSC', 'CHM', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(181, 'Wednesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SOSC_MTH', 200, 'IMT', 'SOSC', 'MTH', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(182, 'Wednesday', 'SOSC ARENA, HALL C', '08:30:00', '10:30:00', 'CSC201', 'CSC201_IMT_SOSC_PHY', 200, 'IMT', 'SOSC', 'PHY', 'SP1572', 2016, 1, '2018-01-02 20:53:02'),
(183, 'Wednesday', 'SOSC ARENA, ROOM A9', '10:30:00', '12:30:00', 'MTH203', 'MTH203_MTH_SOSC_MTH', 200, 'MTH', 'SOSC', 'MTH', 'SP1252', 2016, 1, '2018-01-02 20:53:02'),
(184, 'Wednesday', 'SOSC ARENA, ROOM A9', '10:30:00', '12:30:00', 'MTH203', 'MTH203_MTH_SOSC_PHY', 200, 'MTH', 'SOSC', 'PHY', 'SP1252', 2016, 1, '2018-01-02 20:53:02'),
(185, 'Wednesday', 'SOSC ARENA, ROOM A1', '10:30:00', '12:30:00', 'CHM207', 'CHM207_CHM_SOSC_BIO', 200, 'CHM', 'SOSC', 'BIO', 'SP1222', 2016, 1, '2018-01-02 20:53:02'),
(186, 'Wednesday', 'SOSC ARENA, ROOM A1', '10:30:00', '12:30:00', 'CHM207', 'CHM207_CHM_SOSC_CHM', 200, 'CHM', 'SOSC', 'CHM', 'SP1222', 2016, 1, '2018-01-02 20:53:02'),
(187, 'Wednesday', 'SOSC ARENA, ROOM A7', '12:30:00', '13:30:00', 'BIO203', 'BIO203_BIO_SOSC_BIO', 200, 'BIO', 'SOSC', 'BIO', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(188, 'Wednesday', 'SOSC ARENA, ROOM A1', '12:30:00', '13:30:00', 'CHM203', 'CHM203_CHM_SOSC_CHM', 200, 'CHM', 'SOSC', 'CHM', 'SP1447', 2016, 1, '2018-01-02 20:53:02'),
(189, 'Wednesday', 'SOSC ARENA, ROOM A3', '13:30:00', '15:30:00', 'PHY203', 'PHY203_PHY_SOSC_MTH', 200, 'PHY', 'SOSC', 'MTH', 'SP1227', 2016, 1, '2018-01-02 20:53:02'),
(190, 'Wednesday', 'SOSC ARENA, ROOM A3', '13:30:00', '15:30:00', 'PHY203', 'PHY203_PHY_SOSC_PHY', 200, 'PHY', 'SOSC', 'PHY', 'SP1227', 2016, 1, '2018-01-02 20:53:02'),
(191, 'Thursday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_BIO', 200, 'GST', 'SOSC', 'BIO', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(192, 'Thursday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_CHM', 200, 'GST', 'SOSC', 'CHM', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(193, 'Thursday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_MTH', 200, 'GST', 'SOSC', 'MTH', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(194, 'Thursday', 'SOSC ARENA, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SOSC_PHY', 200, 'GST', 'SOSC', 'PHY', 'SP1267', 2016, 1, '2018-01-02 20:53:02'),
(195, 'Thursday', 'SOSC ARENA, ROOM A9', '09:30:00', '10:30:00', 'MTH203', 'MTH203_MTH_SOSC_MTH', 200, 'MTH', 'SOSC', 'MTH', 'SP1252', 2016, 1, '2018-01-02 20:53:02'),
(196, 'Thursday', 'SOSC ARENA, ROOM A9', '09:30:00', '10:30:00', 'MTH203', 'MTH203_MTH_SOSC_PHY', 200, 'MTH', 'SOSC', 'PHY', 'SP1252', 2016, 1, '2018-01-02 20:53:02'),
(197, 'Thursday', 'SOSC ARENA, ROOM A3', '10:30:00', '11:30:00', 'PHY207', 'PHY207_PHY_SOSC_PHY', 200, 'PHY', 'SOSC', 'PHY', 'SP1117', 2016, 1, '2018-01-02 20:53:02'),
(198, 'Thursday', 'SOSC ARENA, ROOM A3', '10:30:00', '11:30:00', 'PHY207', 'PHY207_PHY_SOSC_MTH', 200, 'PHY', 'SOSC', 'MTH', 'SP1117', 2016, 1, '2018-01-02 20:53:02'),
(199, 'Thursday', 'SOSC ARENA, ROOM A1', '09:30:00', '11:30:00', 'CHM203', 'CHM203_CHM_SOSC_CHM', 200, 'CHM', 'SOSC', 'CHM', 'SP1447', 2016, 1, '2018-01-02 20:53:02'),
(200, 'Thursday', 'SOSC ARENA, ROOM A7', '09:30:00', '11:30:00', 'BIO203', 'BIO203_BIO_SOSC_BIO', 200, 'BIO', 'SOSC', 'BIO', 'SP1557', 2016, 1, '2018-01-02 20:53:02'),
(201, 'Thursday', 'SOSC ARENA, ROOM A3', '11:30:00', '12:30:00', 'PHY203', 'PHY203_PHY_SOSC_MTH', 200, 'PHY', 'SOSC', 'MTH', 'SP1227', 2016, 1, '2018-01-02 20:53:02'),
(202, 'Thursday', 'SOSC ARENA, ROOM A3', '11:30:00', '12:30:00', 'PHY203', 'PHY203_PHY_SOSC_PHY', 200, 'PHY', 'SOSC', 'PHY', 'SP1227', 2016, 1, '2018-01-02 20:53:02'),
(203, 'Friday', 'SOSC ARENA, HALL C', '09:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_BIO', 200, 'MTH', 'SOSC', 'BIO', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(204, 'Friday', 'SOSC ARENA, HALL C', '09:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_CHM', 200, 'MTH', 'SOSC', 'CHM', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(205, 'Friday', 'SOSC ARENA, HALL C', '09:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_MTH', 200, 'MTH', 'SOSC', 'MTH', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(206, 'Friday', 'SOSC ARENA, HALL C', '09:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SOSC_PHY', 200, 'MTH', 'SOSC', 'PHY', 'SP1367', 2016, 1, '2018-01-02 20:53:02'),
(207, 'Friday', 'SOSC ARENA, ROOM A1', '10:30:00', '11:30:00', 'CHM201', 'CHM201_CHM_SOSC_BIO', 200, 'CHM', 'SOSC', 'BIO', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(208, 'Friday', 'SOSC ARENA, ROOM A1', '10:30:00', '11:30:00', 'CHM201', 'CHM201_CHM_SOSC_CHM', 200, 'CHM', 'SOSC', 'CHM', 'SP1332', 2016, 1, '2018-01-02 20:53:02'),
(209, 'Friday', 'SOSC ARENA, ROOM B7', '11:30:00', '12:30:00', 'BIO201', 'BIO201_BIO_SOSC_BIO', 200, 'BIO', 'SOSC', 'BIO', 'SP1272', 2016, 1, '2018-01-02 20:53:02'),
(210, 'Friday', 'SOSC ARENA, ROOM B7', '11:30:00', '12:30:00', 'BIO201', 'BIO201_BIO_SOSC_CHM', 200, 'BIO', 'SOSC', 'CHM', 'SP1272', 2016, 1, '2018-01-02 20:53:02'),
(211, 'Friday', 'SOSC ARENA, ROOM A9', '11:30:00', '13:30:00', 'MTH211', 'MTH211_MTH_SOSC_MTH', 200, 'MTH', 'SOSC', 'MTH', 'SP1242', 2016, 1, '2018-01-02 20:53:02'),
(212, 'Friday', 'SOSC ARENA, ROOM A3', '11:30:00', '13:30:00', 'PHY201', 'PHY201_PHY_SOSC_PHY', 200, 'PHY', 'SOSC', 'PHY', 'SP1197', 2016, 1, '2018-01-02 20:53:02'),
(213, 'Monday', 'SMAT ANNEX, HALL C', '08:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SMAT_IMT', 200, 'MTH', 'SMAT', 'IMT', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(214, 'Monday', 'SMAT ANNEX, HALL C', '08:30:00', '10:30:00', 'MTH201', 'MTH201_MTH_SMAT_PMT', 200, 'MTH', 'SMAT', 'PMT', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(215, 'Monday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'ENG203', 'ENG203_CHE_SMAT_IMT', 200, 'CHE', 'SMAT', 'IMT', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(216, 'Monday', 'SEET COMPLEX, HALL C', '10:30:00', '12:30:00', 'ENG203', 'ENG203_CHE_SMAT_PMT', 200, 'CHE', 'SMAT', 'PMT', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(217, 'Monday', 'SMAT ANNEX, ROOM A1', '12:30:00', '13:30:00', 'PMT203', 'PMT203_PMT_SMAT_PMT', 200, 'PMT', 'SMAT', 'PMT', 'SP1352', 2016, 1, '2018-01-02 20:53:02'),
(218, 'Monday', 'SMAT ANNEX, ROOM A3', '12:30:00', '13:30:00', 'IMT203', 'IMT203_IMT_SMAT_IMT', 200, 'IMT', 'SMAT', 'IMT', 'SP1302', 2016, 1, '2018-01-02 20:53:02'),
(219, 'Tuesday', 'SMAT ANNEX, ROOM A3', '08:30:00', '09:30:00', 'MTH201', 'MTH201_MTH_SMAT_PMT', 200, 'MTH', 'SMAT', 'PMT', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(220, 'Tuesday', 'SMAT ANNEX, ROOM A3', '08:30:00', '09:30:00', 'MTH201', 'MTH201_MTH_SMAT_IMT', 200, 'MTH', 'SMAT', 'IMT', 'SP1317', 2016, 1, '2018-01-02 20:53:02'),
(221, 'Tuesday', 'SMAT ANNEX, HALL C', '09:30:00', '11:30:00', 'CSC201', 'CSC201_IMT_SMAT_PMT', 200, 'IMT', 'SMAT', 'PMT', 'SP1497', 2016, 1, '2018-01-02 20:53:02'),
(222, 'Tuesday', 'SMAT ANNEX, HALL C', '09:30:00', '11:30:00', 'CSC201', 'CSC201_IMT_SMAT_IMT', 200, 'IMT', 'SMAT', 'IMT', 'SP1497', 2016, 1, '2018-01-02 20:53:02'),
(223, 'Tuesday', 'SMAT ANNEX, HALL C', '11:30:00', '13:30:00', 'MTH211', 'MTH211_MTH_SMAT_PMT', 200, 'MTH', 'SMAT', 'PMT', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(224, 'Tuesday', 'SMAT ANNEX, HALL C', '11:30:00', '13:30:00', 'MTH211', 'MTH211_MTH_SMAT_IMT', 200, 'MTH', 'SMAT', 'IMT', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(225, 'Wednesday', 'SMAT ANNEX, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SMAT_PMT', 200, 'GST', 'SMAT', 'PMT', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(226, 'Wednesday', 'SMAT ANNEX, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SMAT_IMT', 200, 'GST', 'SMAT', 'IMT', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(227, 'Wednesday', 'SMAT ANNEX, ROOM A1', '09:30:00', '10:30:00', 'PMT201', 'PMT201_PMT_SMAT_PMT', 200, 'PMT', 'SMAT', 'PMT', 'SP1477', 2016, 1, '2018-01-02 20:53:02'),
(228, 'Wednesday', 'SMAT ANNEX, ROOM A3', '09:30:00', '10:30:00', 'IMT203', 'IMT203_IMT_SMAT_IMT', 200, 'IMT', 'SMAT', 'IMT', 'SP1302', 2016, 1, '2018-01-02 20:53:02'),
(229, 'Wednesday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG203', 'ENG203_CHE_SMAT_PMT', 200, 'CHE', 'SMAT', 'PMT', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(230, 'Wednesday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG203', 'ENG203_CHE_SMAT_IMT', 200, 'CHE', 'SMAT', 'IMT', 'SP1312', 2016, 1, '2018-01-02 20:53:02'),
(231, 'Wednesday', 'SMAT ANNEX, ROOM A1', '10:30:00', '11:30:00', 'PMT203', 'PMT203_PMT_SMAT_PMT', 200, 'PMT', 'SMAT', 'PMT', 'SP1352', 2016, 1, '2018-01-02 20:53:02'),
(232, 'Wednesday', 'SMAT ANNEX, ROOM A3', '10:30:00', '12:30:00', 'IMT201', 'IMT201_IMT_SMAT_IMT', 200, 'IMT', 'SMAT', 'IMT', 'SP1527', 2016, 1, '2018-01-02 20:53:02'),
(233, 'Thursday', 'SMAT ANNEX, HALL C', '08:30:00', '09:30:00', 'CSC201', 'CSC201_IMT_SMAT_PMT', 200, 'IMT', 'SMAT', 'PMT', 'SP1497', 2016, 1, '2018-01-02 20:53:02'),
(234, 'Thursday', 'SMAT ANNEX, HALL C', '08:30:00', '09:30:00', 'CSC201', 'CSC201_IMT_SMAT_IMT', 200, 'IMT', 'SMAT', 'IMT', 'SP1497', 2016, 1, '2018-01-02 20:53:02'),
(235, 'Thursday', 'SMAT ANNEX, ROOM A1', '10:30:00', '12:30:00', 'PMT201', 'PMT201_PMT_SMAT_PMT', 200, 'PMT', 'SMAT', 'PMT', 'SP1477', 2016, 1, '2018-01-02 20:53:02'),
(236, 'Thursday', 'SMAT ANNEX, HALL C', '12:30:00', '13:30:00', 'MTH211', 'MTH211_MTH_SMAT_PMT', 200, 'MTH', 'SMAT', 'PMT', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(237, 'Thursday', 'SMAT ANNEX, HALL C', '12:30:00', '13:30:00', 'MTH211', 'MTH211_MTH_SMAT_IMT', 200, 'MTH', 'SMAT', 'IMT', 'SP1432', 2016, 1, '2018-01-02 20:53:02'),
(238, 'Friday', 'SMAT ANNEX, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SMAT_PMT', 200, 'GST', 'SMAT', 'PMT', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(239, 'Friday', 'SMAT ANNEX, HALL C', '08:30:00', '09:30:00', 'GST201', 'GST201_GST_SMAT_IMT', 200, 'GST', 'SMAT', 'IMT', 'SP1337', 2016, 1, '2018-01-02 20:53:02'),
(240, 'Friday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG201', 'ENG201_CIE_SMAT_IMT', 200, 'CIE', 'SMAT', 'IMT', 'SP1097', 2016, 1, '2018-01-02 20:53:02'),
(241, 'Friday', 'SEET COMPLEX, HALL C', '10:30:00', '11:30:00', 'ENG201', 'ENG201_CIE_SMAT_PMT', 200, 'CIE', 'SMAT', 'PMT', 'SP1097', 2016, 1, '2018-01-02 20:53:02'),
(242, 'Friday', 'SMAT ANNEX, ROOM A3', '11:30:00', '12:30:00', 'IMT201', 'IMT201_IMT_SMAT_IMT', 200, 'IMT', 'SMAT', 'IMT', 'SP1527', 2016, 1, '2018-01-02 20:53:02');

-- --------------------------------------------------------

--
-- Table structure for table `mums_translations`
--

CREATE TABLE `mums_translations` (
  `id` int(10) UNSIGNED NOT NULL,
  `table_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `column_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `foreign_key` int(10) UNSIGNED NOT NULL,
  `locale` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `mums_translations`
--

INSERT INTO `mums_translations` (`id`, `table_name`, `column_name`, `foreign_key`, `locale`, `value`, `created_at`, `updated_at`) VALUES
(1, 'data_types', 'display_name_singular', 5, 'pt', 'Post', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(2, 'data_types', 'display_name_singular', 6, 'pt', 'Página', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(3, 'data_types', 'display_name_singular', 1, 'pt', 'Utilizador', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(4, 'data_types', 'display_name_singular', 4, 'pt', 'Categoria', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(5, 'data_types', 'display_name_singular', 2, 'pt', 'Menu', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(6, 'data_types', 'display_name_singular', 3, 'pt', 'Função', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(7, 'data_types', 'display_name_plural', 5, 'pt', 'Posts', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(8, 'data_types', 'display_name_plural', 6, 'pt', 'Páginas', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(9, 'data_types', 'display_name_plural', 1, 'pt', 'Utilizadores', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(10, 'data_types', 'display_name_plural', 4, 'pt', 'Categorias', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(11, 'data_types', 'display_name_plural', 2, 'pt', 'Menus', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(12, 'data_types', 'display_name_plural', 3, 'pt', 'Funções', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(13, 'categories', 'slug', 1, 'pt', 'categoria-1', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(14, 'categories', 'name', 1, 'pt', 'Categoria 1', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(15, 'categories', 'slug', 2, 'pt', 'categoria-2', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(16, 'categories', 'name', 2, 'pt', 'Categoria 2', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(17, 'pages', 'title', 1, 'pt', 'Olá Mundo', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(18, 'pages', 'slug', 1, 'pt', 'ola-mundo', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(19, 'pages', 'body', 1, 'pt', '<p>Olá Mundo. Scallywag grog swab Cat o\'nine tails scuttle rigging hardtack cable nipper Yellow Jack. Handsomely spirits knave lad killick landlubber or just lubber deadlights chantey pinnace crack Jennys tea cup. Provost long clothes black spot Yellow Jack bilged on her anchor league lateen sail case shot lee tackle.</p>\r\n<p>Ballast spirits fluke topmast me quarterdeck schooner landlubber or just lubber gabion belaying pin. Pinnace stern galleon starboard warp carouser to go on account dance the hempen jig jolly boat measured fer yer chains. Man-of-war fire in the hole nipperkin handsomely doubloon barkadeer Brethren of the Coast gibbet driver squiffy.</p>', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(20, 'menu_items', 'title', 1, 'pt', 'Painel de Controle', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(21, 'menu_items', 'title', 2, 'pt', 'Media', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(22, 'menu_items', 'title', 12, 'pt', 'Publicações', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(23, 'menu_items', 'title', 3, 'pt', 'Utilizadores', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(24, 'menu_items', 'title', 11, 'pt', 'Categorias', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(25, 'menu_items', 'title', 13, 'pt', 'Páginas', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(26, 'menu_items', 'title', 4, 'pt', 'Funções', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(27, 'menu_items', 'title', 5, 'pt', 'Ferramentas', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(28, 'menu_items', 'title', 6, 'pt', 'Menus', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(29, 'menu_items', 'title', 7, 'pt', 'Base de dados', '2018-11-09 22:49:46', '2018-11-09 22:49:46'),
(30, 'menu_items', 'title', 10, 'pt', 'Configurações', '2018-11-09 22:49:46', '2018-11-09 22:49:46');

-- --------------------------------------------------------

--
-- Table structure for table `mums_undergraduate_attendance`
--

CREATE TABLE `mums_undergraduate_attendance` (
  `attendance_id` int(11) NOT NULL,
  `undergraduate_id` varchar(45) NOT NULL,
  `course_code` varchar(45) DEFAULT NULL,
  `atttendance_type` varchar(255) NOT NULL DEFAULT 'Lecture',
  `attendance_status` varchar(255) DEFAULT NULL,
  `staff_id` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Table structure for table `mums_users`
--

CREATE TABLE `mums_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED DEFAULT NULL,
  `name` varchar(25) NOT NULL,
  `forum_name` varchar(255) DEFAULT NULL,
  `password` varchar(185) NOT NULL,
  `remember_token` varchar(100) DEFAULT NULL,
  `settings` text DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `school` varchar(45) DEFAULT NULL,
  `dept` varchar(45) DEFAULT NULL,
  `user_type` varchar(185) NOT NULL DEFAULT 'undergraduate',
  `user_view` varchar(185) DEFAULT NULL,
  `email` varchar(185) NOT NULL,
  `avatar` varchar(191) DEFAULT 'users/default.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_users`
--

INSERT INTO `mums_users` (`id`, `role_id`, `name`, `forum_name`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `school`, `dept`, `user_type`, `user_view`, `email`, `avatar`) VALUES
(1, NULL, 'SP1302', 'yschristopher', '$2y$10$G8i/N6mmTF4lcb57dx/Vx.PWh8PYcACngKovdtIzxK1NsJVjWLdoC', 'XeQUVAo8gLWJ5zlKhYkVgmdH5cS0RseBq9kdvs7Z14d8IVjuuSTSY9xr97Om', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'yschristopher@anymail.com', 'users/default.png'),
(2, NULL, 'SP1227', 'upmichael', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'upmichael@anymail.com', 'users/default.png'),
(3, NULL, 'SP1467', 'umejiroghene', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'umejiroghene@anymail.com', 'users/default.png'),
(4, NULL, 'SP1097', 'uidoris', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'uidoris@anymail.com', 'users/default.png'),
(5, NULL, 'SP1267', 'uctochukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOGS', 'GST', 'staff', 'lecturer', 'uctochukwu@anymail.com', 'users/default.png'),
(6, NULL, 'SP1462', 'ucginika', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'ucginika@anymail.com', 'users/default.png'),
(7, NULL, 'SP1512', 'ubchinyere', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'ubchinyere@anymail.com', 'users/default.png'),
(8, NULL, 'SP1287', 'uapatience', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'uapatience@anymail.com', 'users/default.png'),
(9, NULL, 'SP1292', 'tomartins', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'tomartins@anymail.com', 'users/default.png'),
(10, NULL, 'SP1252', 'tnpetronilla', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'tnpetronilla@anymail.com', 'users/default.png'),
(11, NULL, 'SP1047', 'tamohammed', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'tamohammed@anymail.com', 'users/default.png'),
(12, NULL, 'SP1297', 'sumahmoud', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'sumahmoud@anymail.com', 'users/default.png'),
(13, NULL, 'SP1397', 'sgbalarabe', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'sgbalarabe@anymail.com', 'users/default.png'),
(14, NULL, 'SP1117', 'sewilson', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'sewilson@anymail.com', 'users/default.png'),
(15, NULL, 'SP1122', 'samudasiru', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOGS', 'GST', 'staff', 'lecturer', 'samudasiru@anymail.com', 'users/default.png'),
(16, NULL, 'SP1032', 'ouuvie', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'R4s8HNVMa1nql2FPBDK1YfLehvXB3eUp9v1enOjCz3wDOZXvLswKBdAKp3t3', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'ouuvie@anymail.com', 'users/default.png'),
(17, NULL, 'SP1052', 'oustanley', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'oustanley@anymail.com', 'users/default.png'),
(18, NULL, 'SP1492', 'oucharles', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'oucharles@anymail.com', 'users/default.png'),
(19, NULL, 'SP1527', 'osossai', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'osossai@anymail.com', 'users/default.png'),
(20, NULL, 'SP1197', 'osokechukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'osokechukwu@anymail.com', 'users/default.png'),
(21, NULL, 'SP1562', 'oovincent', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'oovincent@anymail.com', 'users/default.png'),
(22, NULL, 'SP1262', 'ooreginald', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'ooreginald@anymail.com', 'users/default.png'),
(23, NULL, 'SP1477', 'ooelijah', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'ooelijah@anymail.com', 'users/default.png'),
(24, NULL, 'SP1502', 'ooclement', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'ooclement@anymail.com', 'users/default.png'),
(25, NULL, 'SP1352', 'ooaustin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'ooaustin@anymail.com', 'users/default.png'),
(26, NULL, 'SP1432', 'onselina', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'onselina@anymail.com', 'users/default.png'),
(27, NULL, 'SP1237', 'onesther', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'onesther@anymail.com', 'users/default.png'),
(28, NULL, 'SP1372', 'onaugustine', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'onaugustine@anymail.com', 'users/default.png'),
(29, NULL, 'SP1232', 'okingsleypchima', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'okingsleypchima@anymail.com', 'users/default.png'),
(30, NULL, 'SP1282', 'ojosaretin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'ojosaretin@anymail.com', 'users/default.png'),
(31, NULL, 'SP1447', 'ojbasden', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'ojbasden@anymail.com', 'users/default.png'),
(32, NULL, 'SP1522', 'oisamuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'oisamuel@anymail.com', 'users/default.png'),
(33, NULL, 'SP1247', 'oihenry', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'oihenry@anymail.com', 'users/default.png'),
(34, NULL, 'SP1412', 'oiemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'oiemmanuel@anymail.com', 'users/default.png'),
(35, NULL, 'SP1482', 'oibasil', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'oibasil@anymail.com', 'users/default.png'),
(36, NULL, 'SP1357', 'oiangela', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'oiangela@anymail.com', 'users/default.png'),
(37, NULL, 'SP1367', 'oialbert', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'oialbert@anymail.com', 'users/default.png'),
(38, NULL, 'SP1572', 'ogoladimeji', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'ogoladimeji@anymail.com', 'users/default.png'),
(39, NULL, 'SP1162', 'oevincent', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'oevincent@anymail.com', 'users/default.png'),
(40, NULL, 'SP1042', 'oebarbara', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'oebarbara@anymail.com', 'users/default.png'),
(41, NULL, 'SP1362', 'odpeter', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'odpeter@anymail.com', 'users/default.png'),
(42, NULL, 'SP1337', 'ocndudim', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOGS', 'GST', 'staff', 'lecturer', 'ocndudim@anymail.com', 'users/default.png'),
(43, NULL, 'SP1202', 'ocfaith', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'ocfaith@anymail.com', 'users/default.png'),
(44, NULL, 'SP1452', 'ocesther', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'ocesther@anymail.com', 'users/default.png'),
(45, NULL, 'SP1332', 'obayodele', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'obayodele@anymail.com', 'users/default.png'),
(46, NULL, 'SP1312', 'obdike', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'qh0nKLkYBt9vdwFz113KATV18I2DZnkAeHyoi1LKnbvIMBEYSPKNVGGOtpbx', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'obdike@anymail.com', 'users/default.png'),
(47, NULL, 'SP1472', 'oavictor', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOGS', 'GST', 'staff', 'lecturer', 'oavictor@anymail.com', 'users/default.png'),
(48, NULL, 'SP1567', 'oaadeola', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'oaadeola@anymail.com', 'users/default.png'),
(49, NULL, 'SP1157', 'nuchibundo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'nuchibundo@anymail.com', 'users/default.png'),
(50, NULL, 'SP1177', 'nnandrew', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'nnandrew@anymail.com', 'users/default.png'),
(51, NULL, 'SP1327', 'nechibuike', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'nechibuike@anymail.com', 'users/default.png'),
(52, NULL, 'SP1017', 'nachima', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'nachima@anymail.com', 'users/default.png'),
(53, NULL, 'SP1272', 'msmuhammad', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'msmuhammad@anymail.com', 'users/default.png'),
(54, NULL, 'SP1547', 'mkkamilu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'mkkamilu@anymail.com', 'users/default.png'),
(55, NULL, 'SP1037', 'mcogechi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'mcogechi@anymail.com', 'users/default.png'),
(56, NULL, 'SP1307', 'kmphilip', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'kmphilip@anymail.com', 'users/default.png'),
(57, NULL, 'SP1127', 'kmkamilu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'kmkamilu@anymail.com', 'users/default.png'),
(58, NULL, 'SP1092', 'kaibrahim', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'kaibrahim@anymail.com', 'users/default.png'),
(59, NULL, 'SP1167', 'jcevelyn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'jcevelyn@anymail.com', 'users/default.png'),
(60, NULL, 'SP1107', 'ismuhammmad', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'ismuhammmad@anymail.com', 'users/default.png'),
(61, NULL, 'SP1382', 'ioolukemi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'ioolukemi@anymail.com', 'users/default.png'),
(62, NULL, 'SP1317', 'ioemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'ioemmanuel@anymail.com', 'users/default.png'),
(63, NULL, 'SP1537', 'iigodsent', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'iigodsent@anymail.com', 'users/default.png'),
(64, NULL, 'SP1087', 'icvincent', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'icvincent@anymail.com', 'users/default.png'),
(65, NULL, 'SP1057', 'icanthonia', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'icanthonia@anymail.com', 'users/default.png'),
(66, NULL, 'SP1082', 'iasamuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'iasamuel@anymail.com', 'users/default.png'),
(67, NULL, 'SP1402', 'ianneka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'ianneka@anymail.com', 'users/default.png'),
(68, NULL, 'SP1457', 'foayodele', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'foayodele@anymail.com', 'users/default.png'),
(69, NULL, 'SP1497', 'fboluranti', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'fboluranti@anymail.com', 'users/default.png'),
(70, NULL, 'SP1277', 'euemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'euemmanuel@anymail.com', 'users/default.png'),
(71, NULL, 'SP1552', 'esuriah', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'esuriah@anymail.com', 'users/default.png'),
(72, NULL, 'SP1347', 'epmadubueze', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'epmadubueze@anymail.com', 'users/default.png'),
(73, NULL, 'SP1187', 'ecjohn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'ecjohn@anymail.com', 'users/default.png'),
(74, NULL, 'SP1342', 'echumphrey', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'echumphrey@anymail.com', 'users/default.png'),
(75, NULL, 'SP1142', 'ecanthony', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'ecanthony@anymail.com', 'users/default.png'),
(76, NULL, 'SP1427', 'easunday', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'easunday@anymail.com', 'users/default.png'),
(77, NULL, 'SP1487', 'dssolomon', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'dssolomon@anymail.com', 'users/default.png'),
(78, NULL, 'SP1392', 'dizira', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'dizira@anymail.com', 'users/default.png'),
(79, NULL, 'SP1207', 'daadekola', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'daadekola@anymail.com', 'users/default.png'),
(80, NULL, 'SP1152', 'ckcaleb', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'ckcaleb@anymail.com', 'users/default.png'),
(81, NULL, 'SP1557', 'caciroma', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'caciroma@anymail.com', 'users/default.png'),
(82, NULL, 'SP1112', 'bode-tafidelia', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'bode-tafidelia@anymail.com', 'users/default.png'),
(83, NULL, 'SP1387', 'benancy', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'benancy@anymail.com', 'users/default.png'),
(84, NULL, 'SP1507', 'bbiretiola', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'EywK3AohwgcffpDtvRlp7ZZuCfbcd4kmUIMjdrD64mUSsuklEh4G0tVkljLG', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'bbiretiola@anymail.com', 'users/default.png'),
(85, NULL, 'SP1407', 'armacleans', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'armacleans@anymail.com', 'users/default.png'),
(86, NULL, 'SP1417', 'apolayinka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'apolayinka@anymail.com', 'users/default.png'),
(87, NULL, 'SP1222', 'aovictor', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'aovictor@anymail.com', 'users/default.png'),
(88, NULL, 'SP1147', 'aoraphael', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'aoraphael@anymail.com', 'users/default.png'),
(89, NULL, 'SP1182', 'aopeter', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'aopeter@anymail.com', 'users/default.png'),
(90, NULL, 'SP1217', 'aomustafa', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'aomustafa@anymail.com', 'users/default.png'),
(91, NULL, 'SP1442', 'aofolashade', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'aofolashade@anymail.com', 'users/default.png'),
(92, NULL, 'SP1577', 'aoanthony', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOGS', 'GST', 'staff', 'lecturer', 'aoanthony@anymail.com', 'users/default.png'),
(93, NULL, 'SP1517', 'amibrahim', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'amibrahim@anymail.com', 'users/default.png'),
(94, NULL, 'SP1532', 'akolagoke', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOGS', 'GST', 'staff', 'lecturer', 'akolagoke@anymail.com', 'users/default.png'),
(95, NULL, 'SP1242', 'akadedeji', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'akadedeji@anymail.com', 'users/default.png'),
(96, NULL, 'SP1072', 'aigladys', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'nJjkaGg3d9zud6I7OnxVMckkR4zjdcdTAGIFK9lpll0bQPykAtW1biwLDY4t', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'aigladys@anymail.com', 'users/default.png'),
(97, NULL, 'SP1067', 'aifrederick', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'aifrederick@anymail.com', 'users/default.png'),
(98, NULL, 'SP1437', 'agumar', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'agumar@anymail.com', 'users/default.png'),
(99, NULL, 'SP1022', 'aegandhi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'hiG1nUSSxwMNdbMpbwmzxrVtZsbSAKqIfJcKY3OQq2HYEc1hrJLHsNhnqVbF', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'staff', 'lecturer', 'aegandhi@anymail.com', 'users/default.png'),
(100, NULL, 'SP1137', 'aedodo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'aedodo@anymail.com', 'users/default.png'),
(101, NULL, 'SP1422', 'aeadedeji', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'aeadedeji@anymail.com', 'users/default.png'),
(102, NULL, 'SP1077', 'acpascal', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'acpascal@anymail.com', 'users/default.png'),
(103, NULL, 'SP1322', 'acmartin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'acmartin@anymail.com', 'users/default.png'),
(104, NULL, 'SP1257', 'achumphrey', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'staff', 'lecturer', 'achumphrey@anymail.com', 'users/default.png'),
(105, NULL, 'SP1012', 'acbenedict', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'acbenedict@anymail.com', 'users/default.png'),
(106, NULL, 'SP1132', 'abyahaya', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'abyahaya@anymail.com', 'users/default.png'),
(107, NULL, 'SP1062', 'aarasaaq', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'staff', 'lecturer', 'aarasaaq@anymail.com', 'users/default.png'),
(108, NULL, 'SP1212', 'aaoluwole', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'staff', 'lecturer', 'aaoluwole@anymail.com', 'users/default.png'),
(109, NULL, 'SP1192', 'aamohaamed', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'staff', 'lecturer', 'aamohaamed@anymail.com', 'users/default.png'),
(110, NULL, 'SP1542', 'aamatthew', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'staff', 'lecturer', 'aamatthew@anymail.com', 'users/default.png'),
(111, NULL, 'SP1377', 'aaebenezer', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'aaebenezer@anymail.com', 'users/default.png'),
(112, NULL, 'SP1027', 'aaadeyemi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'staff', 'lecturer', 'aaadeyemi@anymail.com', 'users/default.png'),
(113, NULL, 'SP1172', 'aaabiodun', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'staff', 'lecturer', 'aaabiodun@anymail.com', 'users/default.png'),
(114, NULL, '20171020050', 'igbannyjuloma@anymail.culomam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'igbannyjuloma@anymail.culomam', 'users/default.png'),
(115, NULL, '20171020065', 'ezeokoliajacinta@anymail.cjacintam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'ezeokoliajacinta@anymail.cjacintam', 'users/default.png'),
(116, NULL, '20171020125', 'paulcchikamso@anymail.cchikamsom', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'paulcchikamso@anymail.cchikamsom', 'users/default.png'),
(117, NULL, '20171020170', 'anthonynchibuzor@anymail.cchibuzorm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'anthonynchibuzor@anymail.cchibuzorm', 'users/default.png'),
(118, NULL, '20171020185', 'ezenwasa@anymail.cam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'ezenwasa@anymail.cam', 'users/default.png'),
(119, NULL, '20171020200', 'olehoprecious@anymail.cpreciousm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'olehoprecious@anymail.cpreciousm', 'users/default.png'),
(120, NULL, '20171020215', 'nwanyanwufc@anymail.ccm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'nwanyanwufc@anymail.ccm', 'users/default.png'),
(121, NULL, '20171020260', 'obioramtobechukwu@anymail.ctobechukwum', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', '72nOzVoXNOXlounLWfdjeLwjOlaKM3EiDsfljLdGv7bdJh1mh9G7BXQsubI3', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'obioramtobechukwu@anymail.ctobechukwum', 'users/default.png'),
(122, NULL, '20173045050', 'umunnabuikejt@anymail.ctm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'umunnabuikejt@anymail.ctm', 'users/default.png'),
(123, NULL, '20173045065', 'ibegbulamizuchukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'ibegbulamizuchukwu@anymail.com', 'users/default.png'),
(124, NULL, '20173045095', 'iregbuemmanuela', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'iregbuemmanuela@anymail.com', 'users/default.png'),
(125, NULL, '20173045125', 'chimarokenz@anymail.czm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'chimarokenz@anymail.czm', 'users/default.png'),
(126, NULL, '20173045200', 'fedrickcezenwa@anymail.cezenwam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'fedrickcezenwa@anymail.cezenwam', 'users/default.png'),
(127, NULL, '20173045230', 'okonnamercy', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'okonnamercy@anymail.com', 'users/default.png'),
(128, NULL, '20171015020', 'olubunmui-thomasgo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'olubunmui-thomasgo@anymail.com', 'users/default.png'),
(129, NULL, '20171015080', 'iheanachougochi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'iheanachougochi@anymail.com', 'users/default.png'),
(130, NULL, '20171015110', 'ndubuezeikenna', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'ndubuezeikenna@anymail.com', 'users/default.png'),
(131, NULL, '20171015140', 'cletusmunachi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'cletusmunachi@anymail.com', 'users/default.png'),
(132, NULL, '20171015245', 'nwadikepaschal', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'nwadikepaschal@anymail.com', 'users/default.png'),
(133, NULL, '20173050035', 'nwanyalilian', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'nwanyalilian@anymail.com', 'users/default.png'),
(134, NULL, '20173050080', 'onyechereeberechi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'onyechereeberechi@anymail.com', 'users/default.png'),
(135, NULL, '20173050140', 'azuikechijioke', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'azuikechijioke@anymail.com', 'users/default.png'),
(136, NULL, '20173050170', 'solomonfo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'solomonfo@anymail.com', 'users/default.png'),
(137, NULL, '20173050215', 'ezenachukwuvn@anymail.cnm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'ezenachukwuvn@anymail.cnm', 'users/default.png'),
(138, NULL, '20172035020', 'udohemediong', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'udohemediong@anymail.com', 'users/default.png'),
(139, NULL, '20172035050', 'umunabuikejohnbosco', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'umunabuikejohnbosco@anymail.com', 'users/default.png'),
(140, NULL, '20172035080', 'echiclifford', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'echiclifford@anymail.com', 'users/default.png'),
(141, NULL, '20172035095', 'dikeifeanyi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'dikeifeanyi@anymail.com', 'users/default.png'),
(142, NULL, '20173055005', 'ikehenry', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ikehenry@anymail.com', 'users/default.png'),
(143, NULL, '20173055020', 'richardchigoziek', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'richardchigoziek@anymail.com', 'users/default.png'),
(144, NULL, '20173055110', 'igbojianyaoluchi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'igbojianyaoluchi@anymail.com', 'users/default.png'),
(145, NULL, '20173055155', 'onwumereconfidence', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'onwumereconfidence@anymail.com', 'users/default.png'),
(146, NULL, '20173055185', 'uchenn@anymail.cnm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'uchenn@anymail.cnm', 'users/default.png'),
(147, NULL, '20171030005', 'obujiachiwendu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'obujiachiwendu@anymail.com', 'users/default.png'),
(148, NULL, '20171030035', 'okaforce@anymail.cem', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'okaforce@anymail.cem', 'users/default.png'),
(149, NULL, '20171030095', 'ikonneku@anymail.cum', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'ikonneku@anymail.cum', 'users/default.png'),
(150, NULL, '20171030230', 'okoroaforsamuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'okoroaforsamuel@anymail.com', 'users/default.png'),
(151, NULL, '20171025155', 'chimeziefc@anymail.ccm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'chimeziefc@anymail.ccm', 'users/default.png'),
(152, NULL, '20171025275', 'uwattuudeme@anymail.cudemem', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'uwattuudeme@anymail.cudemem', 'users/default.png'),
(153, NULL, '20171025290', 'onuohachukwuebuka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'onuohachukwuebuka@anymail.com', 'users/default.png'),
(154, NULL, '20172040005', 'anyanwupa@anymail.cam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'anyanwupa@anymail.cam', 'users/default.png'),
(155, NULL, '20172040035', 'iheanachocnick@anymail.cnickm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'iheanachocnick@anymail.cnickm', 'users/default.png'),
(156, NULL, '20172040065', 'archibongvictor', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'archibongvictor@anymail.com', 'users/default.png'),
(157, NULL, '20172040110', 'enyiyekonjohn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'enyiyekonjohn@anymail.com', 'users/default.png'),
(158, NULL, '20172040125', 'nnameziecs@anymail.csm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'nnameziecs@anymail.csm', 'users/default.png'),
(159, NULL, '20161020050', 'agbokenechukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'agbokenechukwu@anymail.com', 'users/default.png'),
(160, NULL, '20161020065', 'adubiosunday@anymail.csundaym', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'adubiosunday@anymail.csundaym', 'users/default.png'),
(161, NULL, '20161020125', 'kennethgodwin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'kennethgodwin@anymail.com', 'users/default.png'),
(162, NULL, '20161020170', 'udochukwuvi@anymail.cim', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'udochukwuvi@anymail.cim', 'users/default.png'),
(163, NULL, '20161020185', 'ohakachiamaka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'ohakachiamaka@anymail.com', 'users/default.png'),
(164, NULL, '20161020200', 'orjiugofgodson@anymail.cgodsonm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'orjiugofgodson@anymail.cgodsonm', 'users/default.png'),
(165, NULL, '20161020215', 'moorechimezie', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'moorechimezie@anymail.com', 'users/default.png'),
(166, NULL, '20161020260', 'obatarheeprince@anymail.cprincem', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'obatarheeprince@anymail.cprincem', 'users/default.png'),
(167, NULL, '20163045050', 'ahamefulatochukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'ahamefulatochukwu@anymail.com', 'users/default.png'),
(168, NULL, '20163045065', 'divineikara', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'divineikara@anymail.com', 'users/default.png'),
(169, NULL, '20163045095', 'ololojonathan', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'ololojonathan@anymail.com', 'users/default.png'),
(170, NULL, '20163045125', 'segiusdaniel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'segiusdaniel@anymail.com', 'users/default.png'),
(171, NULL, '20163045200', 'nwokerghandi@anymail.cghandim', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'nwokerghandi@anymail.cghandim', 'users/default.png'),
(172, NULL, '20163045230', 'nnorommo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'nnorommo@anymail.com', 'users/default.png'),
(173, NULL, '20161015020', 'chinwenduuche', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'chinwenduuche@anymail.com', 'users/default.png'),
(174, NULL, '20161015080', 'kwaferuldolf', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'kwaferuldolf@anymail.com', 'users/default.png'),
(175, NULL, '20161015110', 'humphreyac@anymail.ccm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'humphreyac@anymail.ccm', 'users/default.png'),
(176, NULL, '20161015140', 'mbakwefe@anymail.cem', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'mbakwefe@anymail.cem', 'users/default.png'),
(177, NULL, '20161015245', 'uchedaniel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'uchedaniel@anymail.com', 'users/default.png'),
(178, NULL, '20163050035', 'nworuhpa@anymail.cam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'nworuhpa@anymail.cam', 'users/default.png'),
(179, NULL, '20163050080', 'tonyjerffery', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'tonyjerffery@anymail.com', 'users/default.png'),
(180, NULL, '20163050140', 'nwachukwuebuka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'nwachukwuebuka@anymail.com', 'users/default.png'),
(181, NULL, '20163050170', 'nwokoyedz@anymail.czm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'nwokoyedz@anymail.czm', 'users/default.png'),
(182, NULL, '20163050215', 'enwelunkechinyere', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'enwelunkechinyere@anymail.com', 'users/default.png'),
(183, NULL, '20162035020', 'unegbuoluchi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'unegbuoluchi@anymail.com', 'users/default.png'),
(184, NULL, '20162035050', 'oparadchukwuemeka@anymail.cchukwuemekam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'oparadchukwuemeka@anymail.cchukwuemekam', 'users/default.png'),
(185, NULL, '20162035080', 'iroh-idumavictor', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'iroh-idumavictor@anymail.com', 'users/default.png'),
(186, NULL, '20162035095', 'ezekorlauretta', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'ezekorlauretta@anymail.com', 'users/default.png'),
(187, NULL, '20163055005', 'osuagwuebuka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'osuagwuebuka@anymail.com', 'users/default.png'),
(188, NULL, '20163055020', 'oko-ucheokechukwug', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'qxjXBwk7VvE6nbmRWwuIktPDMSb7ttH5AnhgnkukV7Vn1hazAjvcUOQIBnTt', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'oko-ucheokechukwug@anymail.com', 'users/default.png'),
(189, NULL, '20163055110', 'okerekederrick', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', '40WVbHxlt1AbayqhNk0d3Jrok9DppCvqp1nDpdESgcVDVriRHtCOMpsZ21AT', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'okerekederrick@anymail.com', 'users/default.png'),
(190, NULL, '20163055155', 'preciouscharles', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'preciouscharles@anymail.com', 'users/default.png'),
(191, NULL, '20163055185', 'kemealagoaj', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'pXWY5WAky2vBdwVf13vlFpWVYZHL4Sfd2kpwuhJDEoJ74i2piJKeMQwv0NxE', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'kemealagoaj@anymail.com', 'users/default.png'),
(192, NULL, '20161030005', 'maxwellprecious', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'maxwellprecious@anymail.com', 'users/default.png'),
(193, NULL, '20161030035', 'nzeemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'nzeemmanuel@anymail.com', 'users/default.png'),
(194, NULL, '20161030095', 'umunnaifesinachi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'umunnaifesinachi@anymail.com', 'users/default.png'),
(195, NULL, '20161030230', 'maduikejudith', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'maduikejudith@anymail.com', 'users/default.png'),
(196, NULL, '20161025155', 'obidoochinaemerem', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'tNL9lSNSnzUfoygmRFH2mSI28knMLfp8pmFW7q1jGqhlhc2ixwjW1UeV5r61', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'obidoochinaemerem@anymail.com', 'users/default.png'),
(197, NULL, '20161025275', 'nworuhgodwinner', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'nworuhgodwinner@anymail.com', 'users/default.png'),
(198, NULL, '20161025290', 'ogechisolomon', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', '3A6q5jOLd1ZjvCsRXTVup5Ed48pTm4VHT9l4LJcD8Z4hCwRy2szeGqZzZTQC', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'ogechisolomon@anymail.com', 'users/default.png'),
(199, NULL, '20162040005', 'amaugokindnessi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'amaugokindnessi@anymail.com', 'users/default.png');
INSERT INTO `mums_users` (`id`, `role_id`, `name`, `forum_name`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `school`, `dept`, `user_type`, `user_view`, `email`, `avatar`) VALUES
(200, NULL, '20162040035', 'chibundukelechi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'chibundukelechi@anymail.com', 'users/default.png'),
(201, NULL, '20162040065', 'ihemhurudarlighton', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'ihemhurudarlighton@anymail.com', 'users/default.png'),
(202, NULL, '20162040110', 'nnalueanthony', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'nnalueanthony@anymail.com', 'users/default.png'),
(203, NULL, '20162040125', 'uzoruchenna', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'uzoruchenna@anymail.com', 'users/default.png'),
(204, NULL, '20151025185', 'ekwesiprince', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'ekwesiprince@anymail.com', 'users/default.png'),
(205, NULL, '20153050350', 'ucherejoice', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'ucherejoice@anymail.com', 'users/default.png'),
(206, NULL, '20153050335', 'eronduikechukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'eronduikechukwu@anymail.com', 'users/default.png'),
(207, NULL, '20153045320', 'jimnewmans', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'jimnewmans@anymail.com', 'users/default.png'),
(208, NULL, '20153055305', 'ndukwenwokec', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'zsWJNXqqPiDcc9Tb8ZMf9tS2Xvybz528khkcLIIJAsF6I43p3AauCwDN0r7F', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ndukwenwokec@anymail.com', 'users/default.png'),
(209, NULL, '20153050290', 'enyiekekonjohna', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'enyiekekonjohna@anymail.com', 'users/default.png'),
(210, NULL, '20152040065', 'chinagoromchigozirimg', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'chinagoromchigozirimg@anymail.com', 'users/default.png'),
(211, NULL, '20153055275', 'chinyereokonkwo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'chinyereokonkwo@anymail.com', 'users/default.png'),
(212, NULL, '20153045260', 'nwokomadorcas', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'nwokomadorcas@anymail.com', 'users/default.png'),
(213, NULL, '20153045245', 'akpomedeyerocko', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'akpomedeyerocko@anymail.com', 'users/default.png'),
(214, NULL, '20152035050', 'elendukahlanf', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'elendukahlanf@anymail.com', 'users/default.png'),
(215, NULL, '20153055230', 'akhigbegodswillo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'akhigbegodswillo@anymail.com', 'users/default.png'),
(216, NULL, '20151020170', 'nwachukwuadaeze', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'nwachukwuadaeze@anymail.com', 'users/default.png'),
(217, NULL, '20153045215', 'igwezedaniel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'igwezedaniel@anymail.com', 'users/default.png'),
(218, NULL, '20153050200', 'ikechukwugodswill', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'ikechukwugodswill@anymail.com', 'users/default.png'),
(219, NULL, '20151025155', 'esomonuchidinma', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'esomonuchidinma@anymail.com', 'users/default.png'),
(220, NULL, '20151020140', 'eluwaemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'eluwaemmanuel@anymail.com', 'users/default.png'),
(221, NULL, '20153045185', 'ezeriohamaryjane', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'ezeriohamaryjane@anymail.com', 'users/default.png'),
(222, NULL, '20153050170', 'egbomucheemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'egbomucheemmanuel@anymail.com', 'users/default.png'),
(223, NULL, '20153055155', 'uwakwejusticet', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'uwakwejusticet@anymail.com', 'users/default.png'),
(224, NULL, '20153050140', 'okenagbafaiths', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'okenagbafaiths@anymail.com', 'users/default.png'),
(225, NULL, '20151020125', 'okoliugochukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'okoliugochukwu@anymail.com', 'users/default.png'),
(226, NULL, '20151020100', 'oliverchidera', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'oliverchidera@anymail.com', 'users/default.png'),
(227, NULL, '20151025095', 'umembaemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'umembaemmanuel@anymail.com', 'users/default.png'),
(228, NULL, '20152035035', 'egwuatuchuka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'egwuatuchuka@anymail.com', 'users/default.png'),
(229, NULL, '20153055125', 'ekekechinaza', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'd8AwaTvb3ZPBgbUIVKShs74ODEmkLm2mck9H0QNXDcis8c30mRLrrK9dNnro', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ekekechinaza@anymail.com', 'users/default.png'),
(230, NULL, '20153045110', 'godwinmercye', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'godwinmercye@anymail.com', 'users/default.png'),
(231, NULL, '20153045095', 'ekehgodwinm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'ekehgodwinm@anymail.com', 'users/default.png'),
(232, NULL, '20151015080', 'josephemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'josephemmanuel@anymail.com', 'users/default.png'),
(233, NULL, '20153055080', 'ahanekuezinne', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ahanekuezinne@anymail.com', 'users/default.png'),
(234, NULL, '20153050065', 'emoleonyinyechi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'mTJ0bstieDwaQrTmoPoVZyzkDd5TSw0LLAEMOzSvGYpGtiNe2aZSMTr04ZLC', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'emoleonyinyechi@anymail.com', 'users/default.png'),
(235, NULL, '20153055050', 'danielseguisn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'danielseguisn@anymail.com', 'users/default.png'),
(236, NULL, '20152035020', 'nnamakajohne', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'nnamakajohne@anymail.com', 'users/default.png'),
(237, NULL, '20153055035', 'mbachuchisomp', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'mbachuchisomp@anymail.com', 'users/default.png'),
(238, NULL, '20151020065', 'azubuikechinwendu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'azubuikechinwendu@anymail.com', 'users/default.png'),
(239, NULL, '20152035005', 'adewolooluwasoore', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'adewolooluwasoore@anymail.com', 'users/default.png'),
(240, NULL, '20151015050', 'ahamefulachiedozie', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'ahamefulachiedozie@anymail.com', 'users/default.png'),
(241, NULL, '20153045020', 'anyaerubachukwudi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'anyaerubachukwudi@anymail.com', 'users/default.png'),
(242, NULL, '20151015035', 'okekeamara', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'okekeamara@anymail.com', 'users/default.png'),
(243, NULL, '20153055005', 'naabonracheal', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'naabonracheal@anymail.com', 'users/default.png'),
(244, NULL, '20151020020', 'danemefon', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'danemefon@anymail.com', 'users/default.png'),
(245, NULL, '20151025005', 'obioramiraclet', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'obioramiraclet@anymail.com', 'users/default.png'),
(246, NULL, '20143045200', 'igboginikam', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'igboginikam@anymail.com', 'users/default.png'),
(247, NULL, '20143055185', 'uchenkechinyere', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'uchenkechinyere@anymail.com', 'users/default.png'),
(248, NULL, '20141025215', 'samuelawah', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'samuelawah@anymail.com', 'users/default.png'),
(249, NULL, '20142040080', 'adelowooluwasoore', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'adelowooluwasoore@anymail.com', 'users/default.png'),
(250, NULL, '20141030200', 'obujianaghac', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'xj582eZgHs4bq00b2cWtN2KfMIl7S1Z5LFtpa9ScFmHyvc69t5XjhzUCwaoe', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'obujianaghac@anymail.com', 'users/default.png'),
(251, NULL, '20141015185', 'chiechefugodianc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'chiechefugodianc@anymail.com', 'users/default.png'),
(252, NULL, '20143055170', 'naabonrachaelz', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'naabonrachaelz@anymail.com', 'users/default.png'),
(253, NULL, '20141030170', 'okaforchibuezec', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'okaforchibuezec@anymail.com', 'users/default.png'),
(254, NULL, '20143055155', 'eseericau', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'eseericau@anymail.com', 'users/default.png'),
(255, NULL, '20141025155', 'odorpreciousc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'odorpreciousc@anymail.com', 'users/default.png'),
(256, NULL, '20141025140', 'akorinelson', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'akorinelson@anymail.com', 'users/default.png'),
(257, NULL, '20141030125', 'amadipraiseo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'amadipraiseo@anymail.com', 'users/default.png'),
(258, NULL, '20142035065', 'godwingodvessela', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'godwingodvessela@anymail.com', 'users/default.png'),
(259, NULL, '20143045140', 'uchennakelechi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'uchennakelechi@anymail.com', 'users/default.png'),
(260, NULL, '20142040050', 'nwawiheanthonyp', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'nwawiheanthonyp@anymail.com', 'users/default.png'),
(261, NULL, '20143055125', 'kenechidukeo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'kenechidukeo@anymail.com', 'users/default.png'),
(262, NULL, '20143055110', 'nwachukwurichard', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'nwachukwurichard@anymail.com', 'users/default.png'),
(263, NULL, '20141030110', 'nwayanwufrederich', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'nwayanwufrederich@anymail.com', 'users/default.png'),
(264, NULL, '20143055095', 'ohanachooziomachukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ohanachooziomachukwu@anymail.com', 'users/default.png'),
(265, NULL, '20142035035', 'orjisamuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'orjisamuel@anymail.com', 'users/default.png'),
(266, NULL, '20142035020', 'onwuzurikejoseph', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'onwuzurikejoseph@anymail.com', 'users/default.png'),
(267, NULL, '20143055080', 'omakablessingj', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'omakablessingj@anymail.com', 'users/default.png'),
(268, NULL, '20141020095', 'njokuwisdomk', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'njokuwisdomk@anymail.com', 'users/default.png'),
(269, NULL, '20141025080', 'nworuhchristain', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'nworuhchristain@anymail.com', 'users/default.png'),
(270, NULL, '20143050065', 'ogemdiadindu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'ogemdiadindu@anymail.com', 'users/default.png'),
(271, NULL, '20141025065', 'obasiemmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'obasiemmanuel@anymail.com', 'users/default.png'),
(272, NULL, '20143050050', 'ezechioma', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'ezechioma@anymail.com', 'users/default.png'),
(273, NULL, '20141025050', 'irokaprincewill', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'irokaprincewill@anymail.com', 'users/default.png'),
(274, NULL, '20141020035', 'nwekestanley', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', '0d4BgPxwzIsHysEoQhQfi8cXJw3HqGsujxiTth6U12YqDj4gW7trHomUkh1S', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'nwekestanley@anymail.com', 'users/default.png'),
(275, NULL, '20143050035', 'chibundujoyce', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'chibundujoyce@anymail.com', 'users/default.png'),
(276, NULL, '20143045020', 'fakoredesodiq', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'krVRgmZfBj0bIuk33y1X2JWYbBIzkk9aKvFkel0LJi2DKo21Zftm3cUU0MEh', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'fakoredesodiq@anymail.com', 'users/default.png'),
(277, NULL, '20143045005', 'oziomaisrealc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'oziomaisrealc@anymail.com', 'users/default.png'),
(278, NULL, '20141025020', 'nwokeghandir', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'nwokeghandir@anymail.com', 'users/default.png'),
(279, NULL, '20142035005', 'agochukwuruth', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'agochukwuruth@anymail.com', 'users/default.png'),
(280, NULL, '20141030005', 'okorieonyedikachi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'okorieonyedikachi@anymail.com', 'users/default.png'),
(281, NULL, '20131030215', 'uzomamarvin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'uzomamarvin@anymail.com', 'users/default.png'),
(282, NULL, '20133050200', 'onyinyechipaule', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'onyinyechipaule@anymail.com', 'users/default.png'),
(283, NULL, '20131015200', 'ikengaderrick', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'ikengaderrick@anymail.com', 'users/default.png'),
(284, NULL, '20133050185', 'okereaforelvish', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'okereaforelvish@anymail.com', 'users/default.png'),
(285, NULL, '20133045170', 'izidehfortune', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'izidehfortune@anymail.com', 'users/default.png'),
(286, NULL, '20133050155', 'ibechioma', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'ibechioma@anymail.com', 'users/default.png'),
(287, NULL, '20132040040', 'anipaulfredrickn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'hOtyHSUtNQ09lDk2C1tfHJeBNsyN29mfOwnM0hpikxDM5bHy8qbWAzYHtOiY', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'anipaulfredrickn@anymail.com', 'users/default.png'),
(288, NULL, '20131030085', 'ejioguvictorc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'ejioguvictorc@anymail.com', 'users/default.png'),
(289, NULL, '20132040010', 'okolintagodwin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'okolintagodwin@anymail.com', 'users/default.png'),
(290, NULL, '20132035095', 'oduleyeseun', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'oduleyeseun@anymail.com', 'users/default.png'),
(291, NULL, '20131015070', 'ezegloria', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'ezegloria@anymail.com', 'users/default.png'),
(292, NULL, '20132035080', 'chukwukaemmanuelo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'chukwukaemmanuelo@anymail.com', 'users/default.png'),
(293, NULL, '20131015155', 'chidozied', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'chidozied@anymail.com', 'users/default.png'),
(294, NULL, '20133055140', 'ezeofortobechukwuk', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'jL6uHYj3rKDnNj6UPF0EPHQzeLlBVpA1K1ocCxBbUEixhZLBgUs4FwNuDhZT', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ezeofortobechukwuk@anymail.com', 'users/default.png'),
(295, NULL, '20133050125', 'diojilovethc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'diojilovethc@anymail.com', 'users/default.png'),
(296, NULL, '20133045110', 'irokadavid', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'irokadavid@anymail.com', 'users/default.png'),
(297, NULL, '20133055095', 'umannamdii', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'umannamdii@anymail.com', 'users/default.png'),
(298, NULL, '20133050080', 'amobivictoryo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'qbwvsJRYXviTGxh5sXjf1a9GbKyYsrLHxX0Txs5lQTLMNRubkHQGkUTPLgpD', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'amobivictoryo@anymail.com', 'users/default.png'),
(299, NULL, '20132040065', 'ohakachiamakas', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'ohakachiamakas@anymail.com', 'users/default.png'),
(300, NULL, '20131025140', 'ogwublessingn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'ogwublessingn@anymail.com', 'users/default.png'),
(301, NULL, '20133055065', 'afolabimarym', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'afolabimarym@anymail.com', 'users/default.png'),
(302, NULL, '20131020125', 'chibuzorikechukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'chibuzorikechukwu@anymail.com', 'users/default.png'),
(303, NULL, '20131015110', 'nwachukwufaithc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'nwachukwufaithc@anymail.com', 'users/default.png'),
(304, NULL, '20131030095', 'akubavictoro', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'akubavictoro@anymail.com', 'users/default.png'),
(305, NULL, '20133055050', 'adumadanielj', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'adumadanielj@anymail.com', 'users/default.png'),
(306, NULL, '20132035050', 'franksolomonm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'franksolomonm@anymail.com', 'users/default.png'),
(307, NULL, '20131025080', 'okorochiamakaq', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'okorochiamakaq@anymail.com', 'users/default.png'),
(308, NULL, '20131030065', 'innocentfelixk', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'innocentfelixk@anymail.com', 'users/default.png'),
(309, NULL, '20131025050', 'edemekongsamuelm', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'edemekongsamuelm@anymail.com', 'users/default.png'),
(310, NULL, '20131025035', 'ihemhuruchizobamp', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'ihemhuruchizobamp@anymail.com', 'users/default.png'),
(311, NULL, '20132040035', 'nzeribegodfrey', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'nzeribegodfrey@anymail.com', 'users/default.png'),
(312, NULL, '20132040025', 'hokuamachundim', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'hokuamachundim@anymail.com', 'users/default.png'),
(313, NULL, '20133055035', 'dominicpreciousg', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'dominicpreciousg@anymail.com', 'users/default.png'),
(314, NULL, '20133050020', 'oparaochachidozied', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'oparaochachidozied@anymail.com', 'users/default.png'),
(315, NULL, '20131030020', 'nduvictoro', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'nduvictoro@anymail.com', 'users/default.png'),
(316, NULL, '20132040015', 'adubisundayo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'adubisundayo@anymail.com', 'users/default.png'),
(317, NULL, '20132040005', 'ezekennetho', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'ezekennetho@anymail.com', 'users/default.png'),
(318, NULL, '20133055005', 'nduovictor@gmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'nduovictor@gmail.com', 'users/default.png'),
(319, NULL, '20131025005', 'nkwoparamaryc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'nkwoparamaryc@anymail.com', 'users/default.png'),
(320, NULL, 'REGINALD', 'REGINALD', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'ps49dV1Zqgp08eY9BLfn0XKsberMgIP1tNamqdIcqvmdWtnwEvI2qQsJ0q93', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'REGINALD@anymail.com', 'users/default.png'),
(321, NULL, 'selina', 'selina', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'CzlLhiLdQC4qgq1EOPyrSYm0wzJLamJgmUHAML3pF4PuNUKf9vi0xEQBGXbY', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'selina@anymail.com', 'users/default.png'),
(322, NULL, 'solomon', 'solomon', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'solomon@anymail.com', 'users/default.png'),
(323, NULL, 'nneka', 'nneka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'nneka@anymail.com', 'users/default.png'),
(324, NULL, 'olagoke', 'olagoke', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'olagoke@anymail.com', 'users/default.png'),
(325, NULL, 'olayinka', 'olayinka', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'olayinka@anymail.com', 'users/default.png'),
(326, NULL, 'samuel', 'samuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'samuel@anymail.com', 'users/default.png'),
(327, NULL, 'gladys', 'gladys', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'gladys@anymail.com', 'users/default.png'),
(328, NULL, 'humphrey', 'humphrey', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'humphrey@anymail.com', 'users/default.png'),
(329, NULL, 'john', 'john', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'john@anymail.com', 'users/default.png'),
(330, NULL, 'ogechi', 'ogechi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'ogechi@anymail.com', 'users/default.png'),
(331, NULL, 'charles', 'charles', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'charles@anymail.com', 'users/default.png'),
(332, NULL, 'chinyere', 'chinyere', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'chinyere@anymail.com', 'users/default.png'),
(333, NULL, 'christopher', 'christopher', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'christopher@anymail.com', 'users/default.png'),
(334, NULL, 'godsent', 'godsent', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'godsent@anymail.com', 'users/default.png'),
(335, NULL, 'adedeji ', 'adedeji ', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'adedeji @anymail.com', 'users/default.png'),
(336, NULL, 'adeyemi', 'adeyemi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'adeyemi@anymail.com', 'users/default.png'),
(337, NULL, 'albert', 'albert', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'albert@anymail.com', 'users/default.png'),
(338, NULL, 'chibundo', 'chibundo', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'chibundo@anymail.com', 'users/default.png'),
(339, NULL, 'adekola', 'adekola', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'adekola@anymail.com', 'users/default.png'),
(340, NULL, 'okechukwu', 'okechukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'okechukwu@anymail.com', 'users/default.png'),
(341, NULL, 'henry', 'henry', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'henry@anymail.com', 'users/default.png'),
(342, NULL, 'chima', 'chima', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'chima@anymail.com', 'users/default.png'),
(343, NULL, 'adeola', 'adeola', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'adeola@anymail.com', 'users/default.png'),
(344, NULL, 'yahaya', 'yahaya', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'yahaya@anymail.com', 'users/default.png'),
(345, NULL, 'rasaaq', 'rasaaq', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'rasaaq@anymail.com', 'users/default.png'),
(346, NULL, 'petronilla', 'petronilla', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'petronilla@anymail.com', 'users/default.png'),
(347, NULL, 'nancy', 'nancy', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'nancy@anymail.com', 'users/default.png'),
(348, NULL, 'mudasiru', 'mudasiru', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'mudasiru@anymail.com', 'users/default.png'),
(349, NULL, 'ginika', 'ginika', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'ginika@anymail.com', 'users/default.png'),
(350, NULL, 'fidelia', 'fidelia', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'fidelia@anymail.com', 'users/default.png'),
(351, NULL, 'caleb', 'caleb', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'caleb@anymail.com', 'users/default.png'),
(352, NULL, 'basden', 'basden', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'basden@anymail.com', 'users/default.png'),
(353, NULL, 'abiodun', 'abiodun', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'abiodun@anymail.com', 'users/default.png'),
(354, NULL, 'raphael', 'raphael', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'raphael@anymail.com', 'users/default.png'),
(355, NULL, 'uvie', 'uvie', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'uvie@anymail.com', 'users/default.png'),
(356, NULL, 'vincent', 'vincent', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'vincent@anymail.com', 'users/default.png'),
(357, NULL, 'mustafa', 'mustafa', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'mustafa@anymail.com', 'users/default.png'),
(358, NULL, 'osaretin', 'osaretin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'osaretin@anymail.com', 'users/default.png'),
(359, NULL, 'patience', 'patience', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'patience@anymail.com', 'users/default.png'),
(360, NULL, 'gandhi', 'gandhi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'gandhi@anymail.com', 'users/default.png'),
(361, NULL, 'martin', 'martin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'martin@anymail.com', 'users/default.png'),
(362, NULL, 'michael', 'michael', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'michael@anymail.com', 'users/default.png'),
(363, NULL, 'benedict', 'benedict', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'benedict@anymail.com', 'users/default.png'),
(364, NULL, 'ebenezer', 'ebenezer', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'ebenezer@anymail.com', 'users/default.png'),
(365, NULL, 'esther', 'esther', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'esther@anymail.com', 'users/default.png'),
(366, NULL, 'austin', 'austin', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'austin@anymail.com', 'users/default.png'),
(367, NULL, 'balarabe ', 'balarabe ', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'balarabe @anymail.com', 'users/default.png'),
(368, NULL, 'sunday', 'sunday', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'sunday@anymail.com', 'users/default.png'),
(369, NULL, 'oluranti', 'oluranti', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'oluranti@anymail.com', 'users/default.png'),
(370, NULL, 'macleans', 'macleans', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'macleans@anymail.com', 'users/default.png'),
(371, NULL, 'clement', 'clement', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'clement@anymail.com', 'users/default.png'),
(372, NULL, 'angela', 'angela', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'angela@anymail.com', 'users/default.png'),
(373, NULL, 'umar g', 'umar g', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'umar g@anymail.com', 'users/default.png'),
(374, NULL, 'oluwole', 'oluwole', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'oluwole@anymail.com', 'users/default.png'),
(375, NULL, 'mahmoud', 'mahmoud', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'mahmoud@anymail.com', 'users/default.png'),
(376, NULL, 'doris', 'doris', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'doris@anymail.com', 'users/default.png'),
(377, NULL, 'anthony', 'anthony', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'anthony@anymail.com', 'users/default.png'),
(378, NULL, 'philip', 'philip', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'philip@anymail.com', 'users/default.png'),
(379, NULL, 'muhammad', 'muhammad', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'muhammad@anymail.com', 'users/default.png'),
(380, NULL, 'folashade', 'folashade', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'folashade@anymail.com', 'users/default.png'),
(381, NULL, 'basil', 'basil', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'basil@anymail.com', 'users/default.png'),
(382, NULL, 'tochukwu', 'tochukwu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'tochukwu@anymail.com', 'users/default.png'),
(383, NULL, 'oluwafunke', 'oluwafunke', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'oluwafunke@anymail.com', 'users/default.png'),
(384, NULL, 'madubueze', 'madubueze', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'madubueze@anymail.com', 'users/default.png'),
(385, NULL, 'dike', 'dike', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'dike@anymail.com', 'users/default.png'),
(386, NULL, 'anthonia', 'anthonia', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'anthonia@anymail.com', 'users/default.png'),
(387, NULL, 'victor', 'victor', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'victor@anymail.com', 'users/default.png'),
(388, NULL, 'pascal', 'pascal', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'pascal@anymail.com', 'users/default.png'),
(389, NULL, 'matthew', 'matthew', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'matthew@anymail.com', 'users/default.png'),
(390, NULL, 'emmanuel', 'emmanuel', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'emmanuel@anymail.com', 'users/default.png'),
(391, NULL, 'ayodele', 'ayodele', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'ayodele@anymail.com', 'users/default.png'),
(392, NULL, 'wilson', 'wilson', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'wilson@anymail.com', 'users/default.png'),
(393, NULL, 'peter', 'peter', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'peter@anymail.com', 'users/default.png'),
(394, NULL, 'mohammed', 'mohammed', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'mohammed@anymail.com', 'users/default.png'),
(395, NULL, 'evelyn', 'evelyn', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'evelyn@anymail.com', 'users/default.png'),
(396, NULL, 'barbara', 'barbara', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'barbara@anymail.com', 'users/default.png'),
(397, NULL, 'stanley', 'stanley', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'stanley@anymail.com', 'users/default.png'),
(398, NULL, 'olukemi', 'olukemi', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'olukemi@anymail.com', 'users/default.png'),
(399, NULL, 'kamilu', 'kamilu', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'kamilu@anymail.com', 'users/default.png'),
(400, NULL, 'ciroma', 'ciroma', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'ciroma@anymail.com', 'users/default.png');
INSERT INTO `mums_users` (`id`, `role_id`, `name`, `forum_name`, `password`, `remember_token`, `settings`, `created_at`, `updated_at`, `school`, `dept`, `user_type`, `user_view`, `email`, `avatar`) VALUES
(401, NULL, 'andrew', 'andrew', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', NULL, NULL, 'guardian', NULL, 'andrew@anymail.com', 'users/default.png'),
(402, NULL, '20181020050', 'ibeawuchiekpe@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'ibeawuchiekpe@newmail.com', 'users/default.png'),
(403, NULL, '20181020065', 'temitopeolabode@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'temitopeolabode@newmail.com', 'users/default.png'),
(404, NULL, '20181020125', 'abiola olabisi@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'abiola olabisi@newmail.com', 'users/default.png'),
(405, NULL, '20181020170', 'adeyipeter@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'adeyipeter@newmail.com', 'users/default.png'),
(406, NULL, '20181020185', 'alaotaiwo@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'alaotaiwo@newmail.com', 'users/default.png'),
(407, NULL, '20181020200', 'jibrinsunday@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'jibrinsunday@newmail.com', 'users/default.png'),
(408, NULL, '20181020215', 'toryilasolomon@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'toryilasolomon@newmail.com', 'users/default.png'),
(409, NULL, '20181020260', 'idiahirievelyn@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'BIO', 'undergraduate', NULL, 'idiahirievelyn@newmail.com', 'users/default.png'),
(410, NULL, '20183045050', 'chiemekaemmanuel@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'chiemekaemmanuel@newmail.com', 'users/default.png'),
(411, NULL, '20183045065', 'ejoradams@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'ejoradams@newmail.com', 'users/default.png'),
(412, NULL, '20183045095', 'onwukaijeoma@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'onwukaijeoma@newmail.com', 'users/default.png'),
(413, NULL, '20183045125', 'stephentemitope@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'stephentemitope@newmail.com', 'users/default.png'),
(414, NULL, '20183045200', 'onojayefesampson@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'onojayefesampson@newmail.com', 'users/default.png'),
(415, NULL, '20183045230', 'kehindeomolola@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CHE', 'undergraduate', NULL, 'kehindeomolola@newmail.com', 'users/default.png'),
(416, NULL, '20181015020', 'danielolagoke@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'danielolagoke@newmail.com', 'users/default.png'),
(417, NULL, '20181015080', 'ukehchidi@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'ukehchidi@newmail.com', 'users/default.png'),
(418, NULL, '20181015110', 'fadeyicomfort@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'fadeyicomfort@newmail.com', 'users/default.png'),
(419, NULL, '20181015140', 'gbaderohezekiah@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'gbaderohezekiah@newmail.com', 'users/default.png'),
(420, NULL, '20181015245', 'okorovictor@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'CHM', 'undergraduate', NULL, 'okorovictor@newmail.com', 'users/default.png'),
(421, NULL, '20183050035', 'adigunoluwakemi@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'adigunoluwakemi@newmail.com', 'users/default.png'),
(422, NULL, '20183050080', 'jonathanadokige@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'jonathanadokige@newmail.com', 'users/default.png'),
(423, NULL, '20183050140', 'michaeljoseph@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'michaeljoseph@newmail.com', 'users/default.png'),
(424, NULL, '20183050170', 'onifadeomowunmi@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'onifadeomowunmi@newmail.com', 'users/default.png'),
(425, NULL, '20183050215', 'obiobopaul@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'CIE', 'undergraduate', NULL, 'obiobopaul@newmail.com', 'users/default.png'),
(426, NULL, '20182035020', 'udenyisunday@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'udenyisunday@newmail.com', 'users/default.png'),
(427, NULL, '20182035050', 'odabavictoria@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'odabavictoria@newmail.com', 'users/default.png'),
(428, NULL, '20182035080', 'okedelesamuel@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'okedelesamuel@newmail.com', 'users/default.png'),
(429, NULL, '20182035095', 'idahosapaul@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'idahosapaul@newmail.com', 'users/default.png'),
(430, NULL, '20183055005', 'badmusmichael@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'badmusmichael@newmail.com', 'users/default.png'),
(431, NULL, '20183055020', 'odedirananuoluwa@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'odedirananuoluwa@newmail.com', 'users/default.png'),
(432, NULL, '20183055110', 'ibrahimadeola@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'ibrahimadeola@newmail.com', 'users/default.png'),
(433, NULL, '20183055155', 'abisoyestephen@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'abisoyestephen@newmail.com', 'users/default.png'),
(434, NULL, '20183055185', 'anisandra@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SEET', 'MEE', 'undergraduate', NULL, 'anisandra@newmail.com', 'users/default.png'),
(435, NULL, '20181030005', 'jesuloluwasefunmi@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'jesuloluwasefunmi@newmail.com', 'users/default.png'),
(436, NULL, '20181030035', 'owolabidavid@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'owolabidavid@newmail.com', 'users/default.png'),
(437, NULL, '20181030095', 'egentiemmanuel@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'egentiemmanuel@newmail.com', 'users/default.png'),
(438, NULL, '20181030230', 'oluwatayomichael@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'MTH', 'undergraduate', NULL, 'oluwatayomichael@newmail.com', 'users/default.png'),
(439, NULL, '20181025155', 'rufusebere@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'rufusebere@newmail.com', 'users/default.png'),
(440, NULL, '20181025275', 'olatuyiolayinka@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'olatuyiolayinka@newmail.com', 'users/default.png'),
(441, NULL, '20181025290', 'ekechidi@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SOSC', 'PHY', 'undergraduate', NULL, 'ekechidi@newmail.com', 'users/default.png'),
(442, NULL, '20182040005', 'akajitochukwu@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'akajitochukwu@newmail.com', 'users/default.png'),
(443, NULL, '20182040035', 'ciromaagnes@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'ciromaagnes@newmail.com', 'users/default.png'),
(444, NULL, '20182040065', 'templeshokanitan@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'templeshokanitan@newmail.com', 'users/default.png'),
(445, NULL, '20182040110', 'samsonadelaja@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'samsonadelaja@newmail.com', 'users/default.png'),
(446, NULL, '20182040125', 'godfreyokorieh@newmail.com', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', NULL, NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'PMT', 'undergraduate', NULL, 'godfreyokorieh@newmail.com', 'users/default.png'),
(447, NULL, '20132035025', 'charlespreciousc', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', '9fgbNBc73cuxJAOOYNyf30dWymLWfC5mw56MEasHul8O27RxXH41UbvfjXpo', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'undergraduate', NULL, 'charlespreciousc@anymail.com', 'users/default.png'),
(448, NULL, 'SP1102', 'ocoluwafunke', '$2y$10$neUcXm/YSGdv1KkEuOvpj.vSZeXiGPIm4NVRvY5KGNFpQeLW59ndW', 'nPkxZA4Uk2QNiiO87UVjp5vd3VAc6NHvVwuX03B77bcIoYr6Rj6Em5esiA4V', NULL, '2018-01-09 09:21:37', '2018-01-09 09:21:37', 'SMAT', 'IMT', 'staff', 'lecturer', 'ocoluwafunke@anymail.com', 'users/default.png'),
(449, NULL, 'nduovictor', 'tweenmailer@gmail.com', '$2y$10$TtCsmQ239K4t9iQDTzrrXu.JZglDQyXJpvHq67dv/vUw54uV6X1He', NULL, NULL, '2018-09-07 13:34:27', '2018-09-07 13:34:27', NULL, NULL, 'guardian', NULL, 'tweenmailer@gmail.com', 'users/default.png'),
(452, NULL, 'nvictor', 'greenwhitedev@gmail.com', '$2y$10$77pcgdLTB2sra3ojP.CEUuWc7GUdf/ICMHDdvnDYudquE5Z6dIFrq', NULL, NULL, '2018-09-09 20:50:43', '2018-09-09 20:50:43', NULL, NULL, 'guardian', NULL, 'greenwhitedev@gmail.com', 'users/default.png'),
(454, 1, 'Admin', 'admin', '$2y$10$77pcgdLTB2sra3ojP.CEUuWc7GUdf/ICMHDdvnDYudquE5Z6dIFrq', 'ZSGtmxeAmgX0aeK8YolB95V1uz4HWiVdlIkUmmpFUN4H3zwa6r0GFKvxlZxh', '{\"locale\":\"en\"}', '2018-09-09 20:50:43', '2018-11-30 00:59:54', NULL, NULL, 'staff', 'admin\n', 'admin@admin.com', 'users/default.png'),
(455, 5, 'Account', NULL, '$2y$10$77pcgdLTB2sra3ojP.CEUuWc7GUdf/ICMHDdvnDYudquE5Z6dIFrq', NULL, '{\"locale\":\"en\"}', '2018-11-29 19:04:52', '2018-11-29 19:23:32', NULL, NULL, 'staff', 'admin\r\n', 'account@mahadum.com', 'users/default.png'),
(456, 4, 'Registrar', NULL, '$2y$10$77pcgdLTB2sra3ojP.CEUuWc7GUdf/ICMHDdvnDYudquE5Z6dIFrq', 'kfgJXxXqVNNmFFM4IBQzPciJkSaD64sAlOZQhDzhWouSbXXIkWBpS7H86FSm', '{\"locale\":\"en\"}', '2018-11-29 19:06:50', '2018-11-29 19:23:12', NULL, NULL, 'staff', 'admin\r\n', 'registrar@mahadum.com', 'users/default.png'),
(457, 3, 'Forum Moderator', NULL, '$2y$10$77pcgdLTB2sra3ojP.CEUuWc7GUdf/ICMHDdvnDYudquE5Z6dIFrq', NULL, '{\"locale\":\"en\"}', '2018-11-29 19:09:07', '2018-11-29 19:22:50', NULL, NULL, 'staff', 'admin\r\n', 'forum@mahadum.dev', 'users/default.png'),
(458, 1, 'Administrator', NULL, '$2y$10$XSmOnWbza1dM6/C4JlNufO8gbqjwGwcgnW88YufdpgIjt5qpXZ5fy', NULL, '{\"locale\":\"en\"}', '2018-11-30 01:01:14', '2018-11-30 01:01:14', NULL, NULL, 'undergraduate', NULL, 'admin@mahadum.com', 'users/default.png');

-- --------------------------------------------------------

--
-- Table structure for table `mums_users_guardian`
--

CREATE TABLE `users_guardian` (
  `id` int(11) NOT NULL,
  `guardian_id` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `name_initials` varchar(255) DEFAULT NULL,
  `sex` enum('male','female') DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `pending` text DEFAULT NULL,
  `created_at` datetime DEFAULT current_timestamp(),
  `updated_at` datetime DEFAULT current_timestamp(),
  `blocked` text DEFAULT NULL,
  `approved` text DEFAULT NULL,
  `address` text DEFAULT NULL,
  `photo_location` varchar(255) DEFAULT NULL,
  `phone_no` varchar(185) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_users_guardian`
--

INSERT INTO `users_guardian` (`id`, `guardian_id`, `title`, `first_name`, `last_name`, `name_initials`, `sex`, `email`, `pending`, `created_at`, `updated_at`, `blocked`, `approved`, `address`, `photo_location`, `phone_no`) VALUES
(1, 'REGINALD', NULL, 'Reginald', NULL, NULL, 'female', 'REGINALD@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(2, 'selina', NULL, 'Selina', NULL, NULL, 'female', 'selina@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(3, 'solomon', NULL, 'Solomon', NULL, NULL, 'male', 'solomon@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(4, 'nneka', NULL, 'Nneka', NULL, NULL, 'female', 'nneka@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(5, 'olagoke', NULL, 'Olagoke', NULL, NULL, 'male', 'olagoke@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(6, 'olayinka', NULL, 'Olayinka', NULL, NULL, 'female', 'olayinka@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(7, 'samuel', NULL, 'Samuel', NULL, NULL, 'male', 'samuel@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(8, 'gladys', NULL, 'Gladys', NULL, NULL, 'female', 'gladys@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(9, 'humphrey', NULL, 'Humphrey', NULL, NULL, 'male', 'humphrey@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(10, 'john', NULL, 'John', NULL, NULL, 'male', 'john@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(11, 'ogechi', NULL, 'Ogechi', NULL, NULL, 'female', 'ogechi@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(12, 'charles', NULL, 'Charles', NULL, NULL, 'male', 'charles@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(13, 'chinyere', NULL, 'Chinyere', NULL, NULL, 'female', 'chinyere@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(14, 'christopher', NULL, 'Christopher', NULL, NULL, 'male', 'christopher@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(15, 'godsent', NULL, 'Godsent', NULL, NULL, 'male', 'godsent@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(16, 'adedeji ', NULL, 'Adedeji ', NULL, NULL, 'female', 'adedeji @anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(17, 'adeyemi', NULL, 'Adeyemi', NULL, NULL, 'male', 'adeyemi@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(18, 'albert', NULL, 'Albert', NULL, NULL, 'female', 'albert@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(19, 'chibundo', NULL, 'Chibundo', NULL, NULL, 'male', 'chibundo@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(20, 'adekola', NULL, 'Adekola', NULL, NULL, 'female', 'adekola@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(21, 'samuel', NULL, 'Samuel', NULL, NULL, 'male', 'samuel@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(22, 'okechukwu', NULL, 'Okechukwu', NULL, NULL, 'male', 'okechukwu@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(23, 'henry', NULL, 'Henry', NULL, NULL, 'male', 'henry@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(24, 'chima', NULL, 'Chima', NULL, NULL, 'male', 'chima@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(25, 'adeola', NULL, 'Adeola', NULL, NULL, 'male', 'adeola@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(26, 'yahaya', NULL, 'Yahaya', NULL, NULL, 'male', 'yahaya@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(27, 'rasaaq', NULL, 'Rasaaq', NULL, NULL, 'male', 'rasaaq@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(28, 'petronilla', NULL, 'Petronilla', NULL, NULL, 'female', 'petronilla@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(29, 'nancy', NULL, 'Nancy', NULL, NULL, 'female', 'nancy@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(30, 'mudasiru', NULL, 'Mudasiru', NULL, NULL, 'male', 'mudasiru@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(31, 'ginika', NULL, 'Ginika', NULL, NULL, 'female', 'ginika@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(32, 'fidelia', NULL, 'Fidelia', NULL, NULL, 'female', 'fidelia@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(33, 'caleb', NULL, 'Caleb', NULL, NULL, 'male', 'caleb@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(34, 'basden', NULL, 'Basden', NULL, NULL, 'female', 'basden@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(35, 'abiodun', NULL, 'Abiodun', NULL, NULL, 'male', 'abiodun@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(36, 'raphael', NULL, 'Raphael', NULL, NULL, 'male', 'raphael@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(37, 'uvie', NULL, 'Uvie', NULL, NULL, 'female', 'uvie@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(38, 'vincent', NULL, 'Vincent', NULL, NULL, 'female', 'vincent@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(39, 'mustafa', NULL, 'Mustafa', NULL, NULL, 'male', 'mustafa@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(40, 'osaretin', NULL, 'Osaretin', NULL, NULL, 'male', 'osaretin@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(41, 'patience', NULL, 'Patience', NULL, NULL, 'female', 'patience@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(42, 'gandhi', NULL, 'Gandhi', NULL, NULL, 'male', 'gandhi@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(43, 'martin', NULL, 'Martin', NULL, NULL, 'male', 'martin@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(44, 'michael', NULL, 'Michael', NULL, NULL, 'male', 'michael@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(45, 'benedict', NULL, 'Benedict', NULL, NULL, 'male', 'benedict@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(46, 'ebenezer', NULL, 'Ebenezer', NULL, NULL, 'male', 'ebenezer@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(47, 'esther', NULL, 'Esther', NULL, NULL, 'female', 'esther@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(48, 'austin', NULL, 'Austin', NULL, NULL, 'male', 'austin@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(49, 'balarabe ', NULL, 'Balarabe ', NULL, NULL, 'male', 'balarabe @anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(50, 'sunday', NULL, 'Sunday', NULL, NULL, 'male', 'sunday@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(51, 'oluranti', NULL, 'Oluranti', NULL, NULL, 'male', 'oluranti@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(52, 'macleans', NULL, 'Macleans', NULL, NULL, 'male', 'macleans@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(53, 'clement', NULL, 'Clement', NULL, NULL, 'male', 'clement@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(54, 'angela', NULL, 'Angela', NULL, NULL, 'female', 'angela@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(55, 'umar g', NULL, 'Umar G', NULL, NULL, 'female', 'umar g@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(56, 'oluwole', NULL, 'Oluwole', NULL, NULL, 'male', 'oluwole@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(57, 'mahmoud', NULL, 'Mahmoud', NULL, NULL, 'male', 'mahmoud@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(58, 'doris', NULL, 'Doris', NULL, NULL, 'female', 'doris@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(59, 'anthony', NULL, 'Anthony', NULL, NULL, 'female', 'anthony@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(60, 'philip', NULL, 'Philip', NULL, NULL, 'male', 'philip@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(61, 'muhammad', NULL, 'Muhammad', NULL, NULL, 'male', 'muhammad@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(62, 'folashade', NULL, 'Folashade', NULL, NULL, 'male', 'folashade@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(63, 'basil', NULL, 'Basil', NULL, NULL, 'male', 'basil@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(64, 'tochukwu', NULL, 'Tochukwu', NULL, NULL, 'male', 'tochukwu@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(65, 'oluwafunke', NULL, 'Oluwafunke', NULL, NULL, 'female', 'oluwafunke@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(66, 'madubueze', NULL, 'Madubueze', NULL, NULL, 'male', 'madubueze@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(67, 'dike', NULL, 'Dike', NULL, NULL, 'male', 'dike@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(68, 'anthonia', NULL, 'Anthonia', NULL, NULL, 'female', 'anthonia@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(69, 'victor', NULL, 'Victor', NULL, NULL, 'male', 'victor@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(70, 'pascal', NULL, 'Pascal', NULL, NULL, 'female', 'pascal@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(71, 'matthew', NULL, 'Matthew', NULL, NULL, 'male', 'matthew@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(72, 'emmanuel', NULL, 'Emmanuel', NULL, NULL, 'male', 'emmanuel@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(73, 'ayodele', NULL, 'Ayodele', NULL, NULL, 'male', 'ayodele@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(74, 'wilson', NULL, 'Wilson', NULL, NULL, 'female', 'wilson@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(75, 'peter', NULL, 'Peter', NULL, NULL, 'male', 'peter@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(76, 'mohammed', NULL, 'Mohammed', NULL, NULL, 'male', 'mohammed@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_2.png', NULL),
(77, 'evelyn', NULL, 'Evelyn', NULL, NULL, 'female', 'evelyn@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(78, 'barbara', NULL, 'Barbara', NULL, NULL, 'female', 'barbara@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_1.png', NULL),
(79, 'stanley', NULL, 'Stanley', NULL, NULL, 'male', 'stanley@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(80, 'olukemi', NULL, 'Olukemi', NULL, NULL, 'female', 'olukemi@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_2.png', NULL),
(81, 'kamilu', NULL, 'Kamilu', NULL, NULL, 'male', 'kamilu@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL),
(82, 'ciroma', NULL, 'Ciroma', NULL, NULL, 'female', 'ciroma@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_female_3.png', NULL),
(83, 'andrew', NULL, 'Andrew', NULL, NULL, 'male', 'andrew@anymail.com', NULL, '2018-01-02 21:48:55', '2018-01-02 21:48:55', NULL, NULL, NULL, 'images/global/avatars/_avatar_male_1.png', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `mums_users_staffs`
--

CREATE TABLE `mums_users_staffs` (
  `id` int(11) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `name_initials` varchar(12) DEFAULT NULL,
  `former_name` varchar(255) DEFAULT NULL,
  `sex` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `marital_status` varchar(255) NOT NULL,
  `staff_id` varchar(15) NOT NULL DEFAULT 'N/A',
  `dept` varchar(3) NOT NULL,
  `faculty` varchar(4) NOT NULL DEFAULT 'SOSC',
  `position` varchar(45) DEFAULT NULL,
  `function` varchar(45) DEFAULT '0',
  `photo_location` longtext DEFAULT NULL,
  `permantent_address` longtext DEFAULT NULL,
  `contact_address` longtext DEFAULT NULL,
  `phone_number` varchar(15) DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `blood_group` varchar(45) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `place_birth` longtext DEFAULT NULL,
  `next_kin` varchar(255) DEFAULT NULL,
  `next_kin_relationship` varchar(45) DEFAULT NULL,
  `next_kin_address` longtext DEFAULT NULL,
  `sponsor` varchar(45) DEFAULT NULL,
  `sponsor_address` longtext DEFAULT NULL,
  `religion` varchar(255) NOT NULL,
  `place_origin_state` varchar(45) DEFAULT NULL,
  `place_origin_lga` varchar(45) DEFAULT NULL,
  `place_origin_town` varchar(45) DEFAULT NULL,
  `highest_qualification` varchar(255) NOT NULL,
  `institution_obtained` varchar(255) DEFAULT NULL,
  `mode_study` varchar(255) NOT NULL,
  `physical_condition` varchar(255) NOT NULL,
  `signature_path` longtext DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `user_type` varchar(255) DEFAULT 'staff'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_users_staffs`
--

INSERT INTO `mums_users_staffs` (`id`, `first_name`, `last_name`, `name_initials`, `former_name`, `sex`, `title`, `marital_status`, `staff_id`, `dept`, `faculty`, `position`, `function`, `photo_location`, `permantent_address`, `contact_address`, `phone_number`, `email`, `blood_group`, `date_birth`, `place_birth`, `next_kin`, `next_kin_relationship`, `next_kin_address`, `sponsor`, `sponsor_address`, `religion`, `place_origin_state`, `place_origin_lga`, `place_origin_town`, `highest_qualification`, `institution_obtained`, `mode_study`, `physical_condition`, `signature_path`, `created_at`, `updated_at`, `user_type`) VALUES
(1, 'Benedict', 'Anisiuba', 'Chukwuemeka', NULL, 'Male', '', 'Married', 'SP1012', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(2, 'Chima', 'Nwokocha', 'A', NULL, 'Male', '', 'Married', 'SP1017', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(3, 'Barbara', 'Otaigbe', 'Edewele', NULL, 'Female', '', 'Married', 'SP1042', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(4, 'Rasaaq', 'Adebayo', 'Ayodele', NULL, 'Male', '', 'Married', 'SP1062', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(5, 'Kamilu', 'Karaye', 'Musa', NULL, 'Male', '', 'Married', 'SP1127', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(6, 'Yahaya', 'Adamu', 'Baba', NULL, 'Male', '', 'Married', 'SP1132', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(7, 'Evelyn', 'Josephs', 'Chikamso', NULL, 'Female', '', 'Married', 'SP1167', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(8, 'Peter', 'Adeoye', 'Oladapo', NULL, 'Male', '', 'Married', 'SP1182', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(9, 'Patience', 'Udo', 'Aniekan', NULL, 'Female', '', 'Married', 'SP1287', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(10, 'Angela', 'Odike', 'Ifeoma', NULL, 'Female', '', 'Married', 'SP1357', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(11, 'Balarabe ', 'Sani', 'Garko', NULL, 'Male', '', 'Married', 'SP1397', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(12, 'Nneka', 'Ikeme', 'Angela', NULL, 'Female', '', 'Married', 'SP1402', 'CHE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(13, 'Anthonia', 'Ibegbulam', 'Chibuzor', NULL, 'Female', '', 'Married', 'SP1057', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(14, 'Pascal', 'Azuh', 'Chinedu', NULL, 'Female', '', 'Married', 'SP1077', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(15, 'Samuel', 'Ike', 'A', NULL, 'Male', '', 'Married', 'SP1082', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(16, 'Abiodun', 'Akintunde', 'Adeseye', NULL, 'Male', '', 'Married', 'SP1172', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(17, 'Adekola', 'Dada', NULL, NULL, 'Female', '', 'Married', 'SP1207', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(18, 'Martins', 'Thomas', 'Oluwafemi', NULL, 'Male', '', 'Married', 'SP1292', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(19, 'Peter', 'Okoh', 'D', NULL, 'Male', '', 'Married', 'SP1362', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(20, 'Ebenezer', 'Ajayi', 'Adekunle', NULL, 'Male', '', 'Married', 'SP1377', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(21, 'Umar G', 'Adamu ', NULL, NULL, 'Female', '', 'Married', 'SP1437', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(22, 'Basil', 'Okeahialam', 'Ikechukwu', NULL, 'Male', '', 'Single', 'SP1482', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(23, 'Chinyere', 'Uzodinma', 'Blessing', NULL, 'Female', '', 'Married', 'SP1512', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(24, 'Kamilu', 'Karaye', 'Musa', NULL, 'Male', '', 'Single', 'SP1547', 'CIE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(25, 'Gandhi', 'Anyanhun', 'E', NULL, 'Male', '', 'Married', 'SP1022', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(26, 'Vincent', 'Ikeh', 'C', NULL, 'Female', '', 'Married', 'SP1087', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(27, 'Doris', 'Uchenna', 'Ihuoma', NULL, 'Female', '', 'Married', 'SP1097', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(28, 'Raphael', 'Anakwue', 'Omekannaya', NULL, 'Male', '', 'Married', 'SP1147', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(29, 'Caleb', 'Chundusu', 'Kenechi', NULL, 'Male', '', 'Married', 'SP1152', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(30, 'Faith', 'Onyekwulu', 'Chinaza', NULL, 'Female', '', 'Married', 'SP1202', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(31, 'Philip', 'Kolo', 'Manma', NULL, 'Male', '', 'Married', 'SP1307', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(32, 'Dike', 'Ojji', 'Bevis', NULL, 'Male', '', 'Married', 'SP1312', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(33, 'Zira', 'Delia', 'Ibrahim', NULL, 'Female', '', 'Married', 'SP1392', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(34, 'Emmanuel', 'Onwubuya', 'Ikechukwu', NULL, 'Male', '', 'Married', 'SP1412', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(35, 'Clement', 'Odigwe', 'O', NULL, 'Male', '', 'Married', 'SP1502', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(36, 'Vincent', 'Ofuonye', 'Obiajulu', NULL, 'Male', '', 'Married', 'SP1562', 'MEE', 'SEET', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(37, 'Uvieosen', 'Onakpoya', 'Ufuoma', NULL, 'Female', '', 'Married', 'SP1032', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(38, 'Oluwafunke', 'Opasina', 'Christianah', NULL, 'Female', '', 'Married', 'SP1102', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(39, 'Dodo', 'Yakubu', NULL, NULL, 'Female', '', 'Married', 'SP1137', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(40, 'Reginald', 'Ofoegbu', 'Ogu', NULL, 'Female', '', 'Single', 'SP1262', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(41, 'Mahmoud', 'Sani', 'Umar', NULL, 'Male', '', 'Single', 'SP1297', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(42, 'Christopher', 'Yilgwan', 'Sabo', NULL, 'Male', '', 'Married', 'SP1302', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(43, 'Madubueze', 'Ezemba', 'Peter', NULL, 'Male', '', 'Married', 'SP1347', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(44, 'Ginika', 'Ugonabo', 'C', NULL, 'Female', '', 'Married', 'SP1462', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(45, 'Ejiroghene', 'Umuerri', 'Martha', NULL, 'Female', '', 'Married', 'SP1467', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(46, 'Oluranti', 'Familoni', 'Babatope', NULL, 'Male', '', 'Married', 'SP1497', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(47, 'Ossai', 'Ocheli', 'S', NULL, 'Female', '', 'Married', 'SP1527', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(48, 'Oladimeji', 'Opadijo', 'George', NULL, 'Male', '', 'Married', 'SP1572', 'IMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(49, 'Ogechi', 'Maduka', 'Celestina', NULL, 'Female', '', 'Married', 'SP1037', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(50, 'Ibrahim', 'Katibi', 'Adeola', NULL, 'Female', '', 'Married', 'SP1092', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(51, 'Chibundo', 'Nwaneli', 'Uchenna', NULL, 'Male', '', 'Married', 'SP1157', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(52, 'Mohaamed', 'Aminu', 'A', NULL, 'Male', '', 'Married', 'SP1192', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(53, 'Martin', 'Aghaji', 'C', NULL, 'Male', '', 'Married', 'SP1322', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(54, 'Chibuike', 'Nwafor', 'Eze', NULL, 'Male', '', 'Married', 'SP1327', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(55, 'Austin', 'Obasohan', 'O', NULL, 'Male', '', 'Married', 'SP1352', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(56, 'Augustine', 'Odili', 'Nonso', NULL, 'Male', '', 'Married', 'SP1372', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(57, 'Elijah', 'Obidike', 'Osita', NULL, 'Female', '', 'Married', 'SP1477', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(58, 'Solomon', 'Danbauchi', 'Sulei', NULL, 'Male', '', 'Married', 'SP1487', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(59, 'Iretiola', 'Babaniyi', 'Bosede', NULL, 'Female', '', 'Married', 'SP1507', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(60, 'Ibrahim', 'Aliu', NULL, NULL, 'Female', '', 'Married', 'SP1517', 'PMT', 'SMAT', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(61, 'Stanley', 'Okugbo', 'Ukadike', NULL, 'Male', '', 'Single', 'SP1052', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(62, 'Frederick', 'Aigbe', 'Igbens', NULL, 'Female', '', 'Married', 'SP1067', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(63, 'Humphrey', 'Anyanwu', NULL, NULL, 'Male', '', 'Married', 'SP1257', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(64, 'Muhammad', 'Mijinyawa', 'Sani', NULL, 'Male', '', 'Married', 'SP1272', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(65, 'Emmanuel', 'Ejim', 'U', NULL, 'Male', '', 'Married', 'SP1277', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(66, 'Osaretin', 'Odia', 'James', NULL, 'Male', '', 'Married', 'SP1282', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(67, 'Olukemi', 'Ige', 'Omowumi', NULL, 'Female', '', 'Married', 'SP1382', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(68, 'Ayodele', 'Falase', 'Olajide', NULL, 'Male', '', 'Married', 'SP1457', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(69, 'Charles', 'Osuji', 'Ukachukwu', NULL, 'Male', '', 'Married', 'SP1492', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(70, 'Samuel', 'Omokhodion', 'Ilenre', NULL, 'Male', '', 'Married', 'SP1522', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(71, 'Ciroma', 'Chukwuka', 'Angela', NULL, 'Female', '', 'Married', 'SP1557', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(72, 'Adeola', 'Orogade ', 'Abosode', NULL, 'Male', '', 'Married', 'SP1567', 'BIO', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(73, 'Gladys', 'Ahaneku', 'Ifesinachi', NULL, 'Female', '', 'Married', 'SP1072', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(74, 'Anthony', 'Efobi', 'Chizoba', NULL, 'Female', '', 'Married', 'SP1142', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(75, 'Andrew', 'Nwafor', 'N', NULL, 'Male', '', 'Married', 'SP1177', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(76, 'Oluwole', 'Adebo', 'Ademola', NULL, 'Male', '', 'Married', 'SP1212', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(77, 'Mustafa', 'Asani', 'Ohikhena', NULL, 'Male', '', 'Married', 'SP1217', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(78, 'Victor', 'Ansa', 'O', NULL, 'Male', '', 'Married', 'SP1222', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(79, 'Chima', 'Ofoegbu', 'Kingsley Pas', NULL, 'Male', '', 'Married', 'SP1232', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(80, 'Ayodele', 'Omotoso', 'Babatunde', NULL, 'Male', '', 'Married', 'SP1332', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(81, 'Macleans', 'Akpa', 'R', NULL, 'Male', '', 'Single', 'SP1407', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(82, 'Sunday', 'Ediagbeni', 'A', NULL, 'Male', '', 'Married', 'SP1427', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(83, 'Basden', 'Onwubere', 'Jones', NULL, 'Female', '', 'Married', 'SP1447', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(84, 'Godsent', 'Isiguzo', 'Ikenna', NULL, 'Male', '', 'Married', 'SP1537', 'CHM', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(85, 'Adeyemi', 'Johnson', NULL, NULL, 'Male', '', 'Married', 'SP1027', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(86, 'Mohammed', 'Talle', 'Abdullahi', NULL, 'Male', '', 'Married', 'SP1047', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(87, 'John', 'Eze', 'C', NULL, 'Male', '', 'Single', 'SP1187', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(88, 'Adedeji ', 'Adebayo', 'Kola', NULL, 'Male', '', 'Married', 'SP1242', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(89, 'Henry', 'Okolie', 'Ifeanyichukw', NULL, 'Male', '', 'Married', 'SP1247', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(90, 'Petronilla', 'Tabansi', 'Nnenna', NULL, 'Female', '', 'Married', 'SP1252', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(91, 'Emmanuel', 'Ibenegbu', 'Osita', NULL, 'Male', '', 'Married', 'SP1317', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(92, 'Humphrey', 'Ezike', 'C', NULL, 'Male', '', 'Married', 'SP1342', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(93, 'Albert', 'Oyati', 'Ihmogene', NULL, 'Female', '', 'Married', 'SP1367', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(94, 'Olayinka', 'Akinwusi', 'Patience', NULL, 'Female', '', 'Single', 'SP1417', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(95, 'Selina', 'Okolo', 'Nnuaku', NULL, 'Female', '', 'Married', 'SP1432', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(96, 'Esther', 'Ogbemudia', 'chioma', NULL, 'Female', '', 'Married', 'SP1452', 'MTH', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(97, 'Muhammmad', 'Isa', 'Sani', NULL, 'Female', '', 'Married', 'SP1107', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(98, 'Fidelia', 'Bode-Thomas', 'A', NULL, 'Female', '', 'Married', 'SP1112', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(99, 'Wilson', 'Sadoh', 'Ehidiamen', NULL, 'Female', '', 'Married', 'SP1117', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(100, 'Vincent', 'Okwulehie', 'E', NULL, 'Male', '', 'Married', 'SP1162', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(101, 'Okechukwu', 'Ogah', 'Samuel', NULL, 'Male', '', 'Married', 'SP1197', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(102, 'Michael', 'Ufoegbunam', 'P', NULL, 'Male', '', 'Married', 'SP1227', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(103, 'Esther', 'Ohihoin', 'Ngozi', NULL, 'Female', '', 'Married', 'SP1237', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(104, 'Nancy', 'Bitto', 'E', NULL, 'Female', '', 'Married', 'SP1387', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(105, 'Adedeji ', 'Adebayo', NULL, NULL, 'Female', '', 'Married', 'SP1422', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(106, 'Folashade', 'Alli', 'Olaitan', NULL, 'Male', '', 'Married', 'SP1442', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(107, 'Matthew', 'Araoye', 'Akinyemi', NULL, 'Male', '', 'Married', 'SP1542', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(108, 'Uriah', 'Etawo', 'Stephen', NULL, 'Male', '', 'Married', 'SP1552', 'PHY', 'SOSC', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(109, 'Mudasiru', 'Salami', 'Adebayo', NULL, 'Male', '', 'Married', 'SP1122', 'GST', 'SOGS', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(110, 'Tochukwu', 'Uwaezuoke', 'C', NULL, 'Male', '', 'Married', 'SP1267', 'GST', 'SOGS', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(111, 'Ndudim', 'Obeka', 'Chukwu', NULL, 'Male', '', 'Married', 'SP1337', 'GST', 'SOGS', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(112, 'Victor', 'Adegboye', 'O', NULL, 'Male', '', 'Married', 'SP1472', 'GST', 'SOGS', NULL, '0', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(113, 'Olagoke', 'Ale', 'Korede', NULL, 'Male', '', 'Married', 'SP1532', 'GST', 'SOGS', NULL, '0', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff'),
(114, 'Anthony', 'Akintomide', 'Olubunmi', NULL, 'Female', '', 'Married', 'SP1577', 'GST', 'SOGS', NULL, '0', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '', NULL, NULL, NULL, '', NULL, '', '', NULL, '0002-11-30 00:00:00', '0002-11-30 00:00:00', 'staff');

-- --------------------------------------------------------

--
-- Table structure for table `mums_users_undergraduates`
--

CREATE TABLE `mums_users_undergraduates` (
  `id` int(11) NOT NULL,
  `first_name` varchar(185) NOT NULL,
  `last_name` varchar(185) NOT NULL,
  `name_initials` varchar(185) DEFAULT NULL,
  `former_name` varchar(185) DEFAULT NULL,
  `sex` varchar(185) NOT NULL,
  `marital_status` varchar(185) NOT NULL DEFAULT 'Single',
  `reg_no` varchar(185) NOT NULL DEFAULT 'N/A',
  `dept` varchar(3) NOT NULL,
  `faculty` varchar(4) NOT NULL DEFAULT 'SOSC',
  `course_study` varchar(185) NOT NULL,
  `year_entry` int(11) DEFAULT NULL,
  `mode_of_entry` varchar(185) DEFAULT NULL,
  `year_admission` int(11) DEFAULT NULL,
  `award_in_view` varchar(185) DEFAULT NULL,
  `course_duration` int(11) DEFAULT NULL,
  `guardian` varchar(185) DEFAULT '0',
  `photo_location` longtext DEFAULT NULL,
  `permantent_address` longtext DEFAULT NULL,
  `contact_address` longtext DEFAULT NULL,
  `phone_number` varchar(45) DEFAULT NULL,
  `email` varchar(185) DEFAULT NULL,
  `blood_group` varchar(45) DEFAULT NULL,
  `date_birth` date DEFAULT NULL,
  `place_birth` longtext DEFAULT NULL,
  `next_kin` varchar(185) DEFAULT NULL,
  `next_kin_relationship` varchar(45) DEFAULT NULL,
  `next_kin_address` longtext DEFAULT NULL,
  `sponsor` varchar(45) DEFAULT NULL,
  `sponsor_address` longtext DEFAULT NULL,
  `religion` varchar(185) DEFAULT NULL,
  `place_origin_state` varchar(45) DEFAULT NULL,
  `place_origin_lga` varchar(45) DEFAULT NULL,
  `place_origin_town` varchar(45) DEFAULT NULL,
  `highest_qualification` varchar(185) DEFAULT NULL,
  `institution_obtained` varchar(185) DEFAULT NULL,
  `mode_study` varchar(185) DEFAULT NULL,
  `physical_condition` varchar(185) DEFAULT NULL,
  `signature_path` longtext DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `can_register` int(11) NOT NULL DEFAULT 1,
  `user_type` varchar(185) DEFAULT 'undergraduate',
  `cleared_by` varchar(191) DEFAULT NULL,
  `status` enum('active','suspended','alumini') NOT NULL DEFAULT 'active'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `mums_users_undergraduates`
--

INSERT INTO `mums_users_undergraduates` (`id`, `first_name`, `last_name`, `name_initials`, `former_name`, `sex`, `marital_status`, `reg_no`, `dept`, `faculty`, `course_study`, `year_entry`, `mode_of_entry`, `year_admission`, `award_in_view`, `course_duration`, `guardian`, `photo_location`, `permantent_address`, `contact_address`, `phone_number`, `email`, `blood_group`, `date_birth`, `place_birth`, `next_kin`, `next_kin_relationship`, `next_kin_address`, `sponsor`, `sponsor_address`, `religion`, `place_origin_state`, `place_origin_lga`, `place_origin_town`, `highest_qualification`, `institution_obtained`, `mode_study`, `physical_condition`, `signature_path`, `created_at`, `updated_at`, `can_register`, `user_type`, `cleared_by`, `status`) VALUES
(1, 'Oko-uche', 'Okechukwu', 'G', NULL, 'Male', 'Single', '20161020065', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'reginald', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(2, 'Adewolo', 'Oluwasoore', NULL, NULL, 'Female', 'Single', '20152035005', 'IMT', 'SMAT', 'Information Management Technology', 2015, 'JME', NULL, 'B.Tech', 5, 'selina', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(3, 'Azubuike', 'Chinwendu', NULL, NULL, 'Female', 'Single', '20151020065', 'BIO', 'SOSC', 'Industrial Microbiology', 2015, 'JME', NULL, 'B.Sc', 5, 'solomon', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(4, 'Mbachu', 'Chisom', 'P', NULL, 'Male', 'Single', '20153055035', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'nneka', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(5, 'Nnamaka', 'John', 'E', NULL, 'Male', 'Single', '20152035020', 'IMT', 'SMAT', 'Information Management Technology', 2015, 'JME', NULL, 'B.Tech', 5, 'olagoke', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(6, 'Daniel', 'Seguis', 'N', NULL, 'Male', 'Single', '20153055050', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'DIRECT', NULL, 'B.Eng', 5, 'olayinka', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(7, 'Keme', 'Alagoa', 'Joyce', NULL, 'Female', 'Single', '20163050080', 'CIE', 'SEET', 'Civil Engineering', 2016, 'DIRECT', NULL, 'B.Eng', 5, 'samuel', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(8, 'Precious', 'Charles', NULL, NULL, 'Female', 'Single', '20162035050', 'IMT', 'SMAT', 'Information Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'gladys', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(9, 'Okereke', 'Derrick', NULL, NULL, 'Male', 'Single', '20162040065', 'PMT', 'SMAT', 'Project Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'humphrey', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(10, 'Osuagwu', 'Ebuka', 'J. P.', NULL, 'Male', 'Single', '20161015080', 'CHM', 'SOSC', 'Industrial Chemistry', 2016, 'JME', NULL, 'B.Sc', 5, 'john', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(11, 'Ezekor', 'Lauretta', NULL, NULL, 'Female', 'Single', '20163045095', 'CHE', 'SEET', 'Chemical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'ogechi', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(12, 'Iroh-Iduma', 'Victor', NULL, NULL, 'Male', 'Single', '20163055110', 'MEE', 'SEET', 'Mechanical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'charles', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(13, 'Opara', 'Daniel', 'Chukwuemeka', NULL, 'Male', 'Single', '20163045125', 'CHE', 'SEET', 'Chemical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'chinyere', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(14, 'Unegbu', 'Oluchi', NULL, NULL, 'Female', 'Single', '20161030095', 'MTH', 'SOSC', 'Mathematics', 2016, 'JME', NULL, 'B.Sc', 5, 'christopher', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(15, 'Enwelu', 'Nkechinyere', NULL, NULL, 'Female', 'Single', '20162035080', 'IMT', 'SMAT', 'Information Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'godsent', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(16, 'Nwokoye', 'Dumbei', 'Z', NULL, 'Female', 'Single', '20161015110', 'CHM', 'SOSC', 'Industrial Chemistry', 2016, 'JME', NULL, 'B.Sc', 5, 'adedeji ', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(17, 'Nwachukwu', 'Ebuka', NULL, NULL, 'Male', 'Single', '20161020125', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'adeyemi', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(18, 'Tony', 'Jerffery', NULL, NULL, 'Male', 'Single', '20161015140', 'CHM', 'SOSC', 'Industrial Chemistry', 2016, 'JME', NULL, 'B.Sc', 5, 'albert', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(19, 'Nworuh', 'Praise', 'A', NULL, 'Female', 'Single', '20163050140', 'CIE', 'SEET', 'Civil Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'chibundo', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(20, 'Uche', 'Daniel', NULL, NULL, 'Male', 'Single', '20163055155', 'MEE', 'SEET', 'Mechanical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'adekola', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(21, 'Ahamefula', 'Tochukwu', NULL, NULL, 'Male', 'Single', '20161020215', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'samuel', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(22, 'Obatarhe', 'Eseri', 'Prince', NULL, 'Male', 'Single', '20161030230', 'MTH', 'SOSC', 'Mathematics', 2016, 'JME', NULL, 'B.Sc', 5, 'okechukwu', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(23, 'Moore', 'Chimezie', NULL, NULL, 'Male', 'Single', '20161015245', 'CHM', 'SOSC', 'Industrial Chemistry', 2016, 'JME', NULL, 'B.Sc', 5, 'henry', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(24, 'Orjiugo', 'Favor', 'Godson', NULL, 'Female', 'Single', '20161020260', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'chima', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(25, 'Ohaka', 'Chiamaka', NULL, NULL, 'Female', 'Single', '20163045200', 'CHE', 'SEET', 'Chemical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'adeola', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(26, 'Udochukwu', 'Victor', 'I', NULL, 'Male', 'Single', '20161025275', 'PHY', 'SOSC', 'Industrial Physics', 2016, 'JME', NULL, 'B.Sc', 5, 'yahaya', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(27, 'Kenneth', 'Godwin', NULL, NULL, 'Male', 'Single', '20161025290', 'PHY', 'SOSC', 'Industrial Physics', 2016, 'DIRECT', NULL, 'B.Sc', 5, 'rasaaq', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(28, 'Adubi', 'Opeyemi', 'Sunday', NULL, 'Male', 'Single', '20163050215', 'CIE', 'SEET', 'Civil Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'petronilla', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(29, 'Agbo', 'Kenechukwu', NULL, NULL, 'Male', 'Single', '20163045230', 'CHE', 'SEET', 'Chemical Engineering', 2016, 'DIRECT', NULL, 'B.Eng', 5, 'nancy', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(30, 'Nnamezie', 'Chimezie', 'S', NULL, 'Male', 'Single', '20171020200', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'mudasiru', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(31, 'Enyiyekon', 'John', NULL, NULL, 'Male', 'Single', '20173050080', 'CIE', 'SEET', 'Civil Engineering', 2017, 'DIRECT', NULL, 'B.Eng', 5, 'ginika', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(32, 'Archibong', 'Victor', NULL, NULL, 'Male', 'Single', '20172035050', 'IMT', 'SMAT', 'Information Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'fidelia', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(33, 'Iheanacho', 'Caleb', 'Nick', NULL, 'Male', 'Single', '20172040035', 'PMT', 'SMAT', 'Project Management Technology', 2017, 'DIRECT', NULL, 'B.Tech', 5, 'caleb', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(34, 'Anyanwu', 'Peace', 'A', NULL, 'Female', 'Single', '20171015020', 'CHM', 'SOSC', 'Industrial Chemistry', 2017, 'JME', NULL, 'B.Sc', 5, 'basden', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(35, 'Onuoha', 'Chukwuebuka', NULL, NULL, 'Male', 'Single', '20173045200', 'CHE', 'SEET', 'Chemical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'abiodun', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(36, 'Uwatt', 'Uko', 'Udeme', NULL, 'Male', 'Single', '20173055020', 'MEE', 'SEET', 'Mechanical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'raphael', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(37, 'Chimezie', 'Faith', 'C', NULL, 'Female', 'Single', '20173045125', 'CHE', 'SEET', 'Chemical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'uvie', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(38, 'ObujiA', 'Chiwendu', NULL, NULL, 'Female', 'Single', '20171030005', 'MTH', 'SOSC', 'Mathematics', 2017, 'JME', NULL, 'B.Sc', 5, 'vincent', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(39, 'Uche', 'Nkechinyere', 'N', NULL, 'Female', 'Single', '20172035080', 'IMT', 'SMAT', 'Information Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'mustafa', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(40, 'Onwumere', 'Confidence', NULL, NULL, 'Female', 'Single', '20171015080', 'CHM', 'SOSC', 'Industrial Chemistry', 2017, 'JME', NULL, 'B.Sc', 5, 'osaretin', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(41, 'Iheanacho', 'Ugochi', NULL, NULL, 'Female', 'Single', '20171020065', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'patience', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(42, 'Nwachukwu', 'Faith', 'Chisom', NULL, 'Female', 'Single', '20131015110', 'CHM', 'SOSC', 'Industrial Chemistry', 2013, 'DIRECT', NULL, 'B.Sc', 5, 'gandhi', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(43, 'Uma', 'Nnamdi', 'Idika', NULL, 'Male', 'Single', '20133055095', 'MEE', 'SEET', 'Mechanical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'martin', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(44, 'Iroka', 'David', NULL, NULL, 'Male', 'Single', '20133045110', 'CHE', 'SEET', 'Chemical Engineering', 2013, 'DIRECT', NULL, 'B.Eng', 5, 'michael', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(45, 'Dioji', 'Loveth', 'C', NULL, 'Female', 'Single', '20133050125', 'CIE', 'SEET', 'Civil Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'benedict', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(46, 'Ezeofor', 'Tobechukwu', 'K', NULL, 'Male', 'Single', '20133055140', 'MEE', 'SEET', 'Mechanical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'ebenezer', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(47, 'Chidozie', 'D', NULL, NULL, 'Male', 'Single', '20131015155', 'CHM', 'SOSC', 'Industrial Chemistry', 2013, 'JME', NULL, 'B.Sc', 5, 'esther', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(48, 'Chukwuka', 'Emmanuel', 'O', NULL, 'Male', 'Single', '20132035080', 'IMT', 'SMAT', 'Information Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'austin', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(49, 'Eze', 'Gloria', NULL, NULL, 'Female', 'Single', '20131015070', 'CHM', 'SOSC', 'Industrial Chemistry', 2013, 'JME', NULL, 'B.Sc', 5, 'balarabe ', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(50, 'Oduleye', 'Seun', NULL, NULL, 'Male', 'Single', '20132035095', 'IMT', 'SMAT', 'Information Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'sunday', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(51, 'Okolinta', 'Godwin', NULL, NULL, 'Male', 'Single', '20132040010', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'oluranti', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(52, 'Uzoma', 'Marvin', NULL, NULL, 'Male', 'Single', '20131030215', 'MTH', 'SOSC', 'Mathematics', 2013, 'JME', NULL, 'B.Sc', 5, 'macleans', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(53, 'Esomonu', 'Chidinma', NULL, NULL, 'Female', 'Single', '20151025155', 'PHY', 'SOSC', 'Industrial Physics', 2015, 'JME', NULL, 'B.Sc', 5, 'clement', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(54, 'Ikechukwu', 'Godswill', NULL, NULL, 'Male', 'Single', '20153050200', 'CIE', 'SEET', 'Civil Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'angela', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(55, 'Igweze', 'Daniel', NULL, NULL, 'Male', 'Single', '20153045215', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'umar g', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(56, 'Nwachukwu', 'Adaeze', NULL, NULL, 'Female', 'Single', '20151020170', 'BIO', 'SOSC', 'Industrial Microbiology', 2015, 'JME', NULL, 'B.Sc', 5, 'oluwole', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(57, 'Akhigbe', 'Godswill', 'O', NULL, 'Male', 'Single', '20153055230', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'mahmoud', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(58, 'Elendu', 'Kahlan', 'F', NULL, 'Female', 'Single', '20152035050', 'IMT', 'SMAT', 'Information Management Technology', 2015, 'JME', NULL, 'B.Tech', 5, 'doris', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(59, 'Chinagorom', 'Chigozirim', 'G', NULL, 'Male', 'Single', '20152040065', 'PMT', 'SMAT', 'Project Management Technology', 2015, 'JME', NULL, 'B.Tech', 5, 'anthony', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(60, 'Enyiekekon', 'John', 'A', NULL, 'Male', 'Single', '20153050290', 'CIE', 'SEET', 'Civil Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'philip', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(61, 'Ndukwe', 'Nwoke', 'Chiziterem', NULL, 'Male', 'Single', '20153055305', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'muhammad', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(62, 'Jim', 'Newman', 'S', NULL, 'Male', 'Single', '20153045320', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'folashade', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(63, 'Erondu', 'Ikechukwu', NULL, NULL, 'Male', 'Single', '20153050335', 'CIE', 'SEET', 'Civil Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'basil', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(64, 'Uche', 'Rejoice', NULL, NULL, 'Female', 'Single', '20153050350', 'CIE', 'SEET', 'Civil Engineering', 2015, 'DIRECT', NULL, 'B.Eng', 5, 'tochukwu', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(65, 'Ekwesi', 'Prince', NULL, NULL, 'Male', 'Single', '20151025185', 'PHY', 'SOSC', 'Industrial Physics', 2015, 'JME', NULL, 'B.Sc', 5, 'oluwafunke', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(66, 'Uzor', 'Uchenna', NULL, NULL, 'Male', 'Single', '20162040005', 'PMT', 'SMAT', 'Project Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'madubueze', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(67, 'Nnalue', 'Anthony', NULL, NULL, 'Male', 'Single', '20163055005', 'MEE', 'SEET', 'Mechanical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'dike', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(68, 'Ihemhuru', 'Darlighton', NULL, NULL, 'Male', 'Single', '20161030005', 'MTH', 'SOSC', 'Mathematics', 2016, 'JME', NULL, 'B.Sc', 5, 'anthonia', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(69, 'Njoku', 'Wisdom', 'K', NULL, 'Male', 'Single', '20141020095', 'BIO', 'SOSC', 'Industrial Microbiology', 2014, 'JME', NULL, 'B.Sc', 5, 'victor', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(70, 'Ohanacho', 'Oziomachukwu', NULL, NULL, 'Male', 'Single', '20143055095', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'pascal', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(71, 'Nwayanwu', 'Frederich', NULL, NULL, 'Male', 'Single', '20141030110', 'MTH', 'SOSC', 'Mathematics', 2014, 'JME', NULL, 'B.Sc', 5, 'matthew', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(72, 'Nwachukwu', 'Richard', NULL, NULL, 'Male', 'Single', '20143055110', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'emmanuel', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(73, 'Kenechi', 'Duke', 'O', NULL, 'Male', 'Single', '20143055125', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'ayodele', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(74, 'Nwawihe', 'Anthony', 'P', NULL, 'Male', 'Single', '20142040050', 'PMT', 'SMAT', 'Project Management Technology', 2014, 'JME', NULL, 'B.Tech', 5, 'wilson', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(75, 'Uchenna', 'kelechi', NULL, NULL, 'Male', 'Single', '20143045140', 'CHE', 'SEET', 'Chemical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'peter', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(76, 'Godwin', 'Godvessel', 'A', NULL, 'Male', 'Single', '20142035065', 'IMT', 'SMAT', 'Information Management Technology', 2014, 'JME', NULL, 'B.Tech', 5, 'mohammed', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(77, 'Amadi', 'PraiseO', NULL, NULL, 'Male', 'Single', '20141030125', 'MTH', 'SOSC', 'Mathematics', 2014, 'JME', NULL, 'B.Sc', 5, 'evelyn', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(78, 'Odor', 'Precious', 'C', NULL, 'Female', 'Single', '20141025155', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'barbara', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(79, 'Ese', 'Erica', 'U', NULL, 'Female', 'Single', '20143055155', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'stanley', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(80, 'Okafor', 'Chibueze', 'Christian', NULL, 'Male', 'Single', '20141030170', 'MTH', 'SOSC', 'Mathematics', 2014, 'JME', NULL, 'B.Sc', 5, 'olukemi', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(81, 'Naabon', 'Rachael', 'Z', NULL, 'Male', 'Single', '20143055170', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'kamilu', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(82, 'Chiechefu', 'Godian', 'Chijioke', NULL, 'Male', 'Single', '20141015185', 'CHM', 'SOSC', 'Industrial Chemistry', 2014, 'DIRECT', NULL, 'B.Sc', 5, 'ciroma', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(83, 'Obuji', 'Anagha', 'Chiwendu', NULL, 'Female', 'Single', '20141030200', 'MTH', 'SOSC', 'Mathematics', 2014, 'JME', NULL, 'B.Sc', 5, 'andrew', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(84, 'Akori', 'Nelson', NULL, NULL, 'Male', 'Single', '20141025140', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'reginald', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(85, 'Emole', 'Onyinyechi', NULL, NULL, 'Female', 'Single', '20153050065', 'CIE', 'SEET', 'Civil Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'selina', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(86, 'Ahaneku', 'Ezinne', NULL, NULL, 'Female', 'Single', '20153055080', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'solomon', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(87, 'Joseph', 'Emmanuel', NULL, NULL, 'Male', 'Single', '20151015080', 'CHM', 'SOSC', 'Industrial Chemistry', 2015, 'JME', NULL, 'B.Sc', 5, 'nneka', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(88, 'Ekeh', 'Godwin', 'M', NULL, 'Male', 'Single', '20153045095', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'olagoke', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(89, 'Akpomedeye', 'Rock', 'O', NULL, 'Male', 'Single', '20153045245', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'olayinka', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(90, 'Chibuzor', 'Ikechukwu', NULL, NULL, 'Male', 'Single', '20131020125', 'BIO', 'SOSC', 'Industrial Microbiology', 2013, 'JME', NULL, 'B.Sc', 5, 'samuel', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(91, 'Afolabi', 'Mary', 'M', NULL, 'Female', 'Single', '20133055065', 'MEE', 'SEET', 'Mechanical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'gladys', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(92, 'Ogwu', 'Blessing', 'N', NULL, 'Female', 'Single', '20131025140', 'PHY', 'SOSC', 'Industrial Physics', 2013, 'JME', NULL, 'B.Sc', 5, 'humphrey', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(93, 'Ezenachukwu', 'Victoria', 'N', NULL, 'Male', 'Single', '20171015110', 'CHM', 'SOSC', 'Industrial Chemistry', 2017, 'JME', NULL, 'B.Sc', 5, 'john', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(94, 'Solomon', 'Faith', 'O', NULL, 'Female', 'Single', '20173050140', 'CIE', 'SEET', 'Civil Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'ogechi', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(95, 'Olubunmui-Thomas', 'Gideon', 'O', NULL, 'Male', 'Single', '20173055110', 'MEE', 'SEET', 'Mechanical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'charles', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(96, 'Azuike', 'Chijioke', NULL, NULL, 'Male', 'Single', '20171020215', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'chinyere', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(97, 'Onyechere', 'Eberechi', NULL, NULL, 'Female', 'Single', '20171030035', 'MTH', 'SOSC', 'Mathematics', 2017, 'JME', NULL, 'B.Sc', 5, 'christopher', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(98, 'Nwanya', 'Lilian', NULL, NULL, 'Female', 'Single', '20171015245', 'CHM', 'SOSC', 'Industrial Chemistry', 2017, 'JME', NULL, 'B.Sc', 5, 'godsent', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(99, 'Nwadike', 'Paschal', NULL, NULL, 'Male', 'Single', '20171020170', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'adedeji ', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(100, 'Ohaka', 'Chiamaka', 'S', NULL, 'Female', 'Single', '20132040065', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'adeyemi', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(101, 'Okonna', 'Mercy', NULL, NULL, 'Female', 'Single', '20173045095', 'CHE', 'SEET', 'Chemical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'albert', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(102, 'Nwokoma', 'Dorcas', NULL, NULL, 'Female', 'Single', '20153045260', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'DIRECT', NULL, 'B.Eng', 5, 'chibundo', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(103, 'Chinyere', 'Okonkwo', NULL, NULL, 'Female', 'Single', '20153055275', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'adekola', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(104, 'Maduike', 'Judith', NULL, NULL, 'Female', 'Single', '20163050035', 'CIE', 'SEET', 'Civil Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'samuel', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(105, 'Umunna', 'Ifesinachi', NULL, NULL, 'Male', 'Single', '20161020050', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'okechukwu', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(106, 'Nze', 'Emmanuel', NULL, NULL, 'Male', 'Single', '20163045050', 'CHE', 'SEET', 'Chemical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'henry', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(107, 'Maxwell', 'Precious', NULL, NULL, 'Female', 'Single', '20163045065', 'CHE', 'SEET', 'Chemical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'chima', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(108, 'Mbakwe', 'Frank', 'E', NULL, 'Male', 'Single', '20161025155', 'PHY', 'SOSC', 'Industrial Physics', 2016, 'JME', NULL, 'B.Sc', 5, 'adeola', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(109, 'Humphrey', 'Amarachi', 'C', NULL, 'Female', 'Single', '20163050170', 'CIE', 'SEET', 'Civil Engineering', 2016, 'DIRECT', NULL, 'B.Eng', 5, 'yahaya', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(110, 'Kwafe', 'Ruldolf', NULL, NULL, 'Male', 'Single', '20161020170', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'rasaaq', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(111, 'Chinwendu', 'Uche', NULL, NULL, 'Female', 'Single', '20162035095', 'IMT', 'SMAT', 'Information Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'petronilla', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(112, 'Nnorom', 'Miracle', 'O', NULL, 'Male', 'Single', '20162040110', 'PMT', 'SMAT', 'Project Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'nancy', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(113, 'Nwoke', 'Robert', 'Ghandi', NULL, 'Male', 'Single', '20163055185', 'MEE', 'SEET', 'Mechanical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'mudasiru', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(114, 'Segius', 'Daniel', NULL, NULL, 'Male', 'Single', '20162040125', 'PMT', 'SMAT', 'Project Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'ginika', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(115, 'Cletus', 'Munachi', NULL, NULL, 'Male', 'Single', '20171025275', 'PHY', 'SOSC', 'Industrial Physics', 2017, 'JME', NULL, 'B.Sc', 5, 'fidelia', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(116, 'Ndubueze', 'Ikenna', NULL, NULL, 'Male', 'Single', '20171025155', 'PHY', 'SOSC', 'Industrial Physics', 2017, 'JME', NULL, 'B.Sc', 5, 'caleb', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(117, 'Ejiogu', 'Victor', 'C', NULL, 'Male', 'Single', '20131030085', 'MTH', 'SOSC', 'Mathematics', 2013, 'JME', NULL, 'B.Sc', 5, 'basden', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(118, 'Ogemdi', 'Adindu', NULL, NULL, 'Female', 'Single', '20143050065', 'CIE', 'SEET', 'Civil Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'wilson', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(119, 'Jeff', 'James', 'Chinomso', NULL, 'Male', 'Single', '20133055005', 'MEE', 'SEET', 'Mechanical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'abiodun', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(120, 'Charles', 'Precious', 'Chidera', NULL, 'Female', 'Married', '20132035025', 'IMT', 'SMAT', 'Information Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'raphael', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(121, 'Anipaul', 'Fredrick', 'N', NULL, 'Male', 'Single', '20132040040', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'uvie', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active');
INSERT INTO `mums_users_undergraduates` (`id`, `first_name`, `last_name`, `name_initials`, `former_name`, `sex`, `marital_status`, `reg_no`, `dept`, `faculty`, `course_study`, `year_entry`, `mode_of_entry`, `year_admission`, `award_in_view`, `course_duration`, `guardian`, `photo_location`, `permantent_address`, `contact_address`, `phone_number`, `email`, `blood_group`, `date_birth`, `place_birth`, `next_kin`, `next_kin_relationship`, `next_kin_address`, `sponsor`, `sponsor_address`, `religion`, `place_origin_state`, `place_origin_lga`, `place_origin_town`, `highest_qualification`, `institution_obtained`, `mode_study`, `physical_condition`, `signature_path`, `created_at`, `updated_at`, `can_register`, `user_type`, `cleared_by`, `status`) VALUES
(122, 'Ibe', 'Chioma', NULL, NULL, 'Female', 'Single', '20133050155', 'CIE', 'SEET', 'Civil Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'vincent', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(123, 'Izideh', 'Fortune', NULL, NULL, 'Male', 'Single', '20133045170', 'CHE', 'SEET', 'Chemical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'mustafa', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(124, 'Okereafor', 'Elvis', 'Harry', NULL, 'Male', 'Single', '20133050185', 'CIE', 'SEET', 'Civil Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'osaretin', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(125, 'Ikenga', 'Derrick', NULL, NULL, 'Male', 'Single', '20131015200', 'CHM', 'SOSC', 'Industrial Chemistry', 2013, 'JME', NULL, 'B.Sc', 5, 'patience', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(126, 'Eze', 'Kenneth', 'O', NULL, 'Male', 'Single', '20132040005', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'gandhi', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(127, 'Adubi', 'SundayO', NULL, NULL, 'Male', 'Single', '20132040015', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'martin', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(128, 'Ololo', 'Jonathan', NULL, NULL, 'Male', 'Single', '20161020185', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'DIRECT', NULL, 'B.Sc', 5, 'michael', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(129, 'Divine', 'Ikara', NULL, NULL, 'Male', 'Single', '20161020200', 'BIO', 'SOSC', 'Industrial Microbiology', 2016, 'JME', NULL, 'B.Sc', 5, 'benedict', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(130, 'Okoroafor', 'Samuel', NULL, NULL, 'Male', 'Single', '20173050215', 'CIE', 'SEET', 'Civil Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'ebenezer', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(131, 'Ikonne', 'Kennedy', 'U', NULL, 'Male', 'Single', '20173045050', 'CHE', 'SEET', 'Chemical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'esther', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(132, 'Okafor', 'Chukwuka', 'E', NULL, 'Male', 'Single', '20172040005', 'PMT', 'SMAT', 'Project Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'austin', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(133, 'Oleh', 'Ogechukwu', 'Precious', NULL, 'Female', 'Single', '20173055155', 'MEE', 'SEET', 'Mechanical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'balarabe ', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(134, 'Ezenwa', 'Sopuruchukwu', 'A', NULL, 'Male', 'Single', '20171030230', 'MTH', 'SOSC', 'Mathematics', 2017, 'JME', NULL, 'B.Sc', 5, 'sunday', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(135, 'Anthony', 'Nnazue', 'Chibuzor', NULL, 'Male', 'Single', '20173050035', 'CIE', 'SEET', 'Civil Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'oluranti', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(136, 'Paul', 'Chris', 'Chikamso', NULL, 'Male', 'Single', '20171020125', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'macleans', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(137, 'Ezeokoli', 'Adanna', 'Jacinta', NULL, 'Female', 'Single', '20173045230', 'CHE', 'SEET', 'Chemical Engineering', 2017, 'DIRECT', NULL, 'B.Eng', 5, 'clement', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(138, 'Igbanny', 'Janet', 'Uloma', NULL, 'Female', 'Single', '20173045065', 'CHE', 'SEET', 'Chemical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'angela', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(139, 'Onyinyechi', 'Paul', 'Emole', NULL, 'Male', 'Single', '20133050200', 'CIE', 'SEET', 'Civil Engineering', 2013, 'DIRECT', NULL, 'B.Eng', 5, 'umar g', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(140, 'Okorie', 'Onyedikachi', NULL, NULL, 'Male', 'Single', '20141030005', 'MTH', 'SOSC', 'Mathematics', 2014, 'JME', NULL, 'B.Sc', 5, 'oluwole', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(141, 'Nzeribe', 'Godfrey', NULL, NULL, 'Male', 'Single', '20132040035', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'DIRECT', NULL, 'B.Tech', 5, 'mahmoud', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(142, 'Ihemhuru', 'ChizobamP', NULL, NULL, 'Male', 'Single', '20131025035', 'PHY', 'SOSC', 'Industrial Physics', 2013, 'JME', NULL, 'B.Sc', 5, 'doris', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(143, 'Edemekong', 'Samuel', 'M', NULL, 'Male', 'Single', '20131025050', 'PHY', 'SOSC', 'Industrial Physics', 2013, 'JME', NULL, 'B.Sc', 5, 'anthony', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(144, 'Innocent', 'felixK', NULL, NULL, 'Male', 'Single', '20131030065', 'MTH', 'SOSC', 'Mathematics', 2013, 'JME', NULL, 'B.Sc', 5, 'philip', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(145, 'Okoro', 'Chiamaka', 'Q', NULL, 'Female', 'Single', '20131025080', 'PHY', 'SOSC', 'Industrial Physics', 2013, 'JME', NULL, 'B.Sc', 5, 'muhammad', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(146, 'Frank', 'Solomon', 'Marvelous', NULL, 'Female', 'Married', '20132035050', 'IMT', 'SMAT', 'Information Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'folashade', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(147, 'Aduma', 'Daniel', 'Jackson', NULL, 'Male', 'Single', '20133055050', 'MEE', 'SEET', 'Mechanical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'basil', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(148, 'Akuba', 'Victor', 'O', NULL, 'Male', 'Single', '20131030095', 'MTH', 'SOSC', 'Mathematics', 2013, 'JME', NULL, 'B.Sc', 5, 'tochukwu', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(149, 'Agochukwu', 'Ruth', NULL, NULL, 'Female', 'Single', '20142035005', 'IMT', 'SMAT', 'Information Management Technology', 2014, 'JME', NULL, 'B.Tech', 5, 'oluwafunke', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(150, 'Nwoke', 'Ghandi', 'Robert', NULL, 'Male', 'Single', '20141025020', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'madubueze', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(151, 'Ozioma', 'Isreal', 'C', NULL, 'Male', 'Single', '20143045005', 'CHE', 'SEET', 'Chemical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'dike', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(152, 'Fakorede', 'Sodiq', NULL, NULL, 'Male', 'Single', '20143045020', 'CHE', 'SEET', 'Chemical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'anthonia', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(153, 'Chibundu', 'Joyce', NULL, NULL, 'Female', 'Married', '20143050035', 'CIE', 'SEET', 'Civil Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'victor', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(154, 'Nweke', 'Stanley', NULL, NULL, 'Male', 'Single', '20141020035', 'BIO', 'SOSC', 'Industrial Microbiology', 2014, 'DIRECT', NULL, 'B.Sc', 5, 'pascal', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(155, 'Iroka', 'Princewill', NULL, NULL, 'Male', 'Single', '20141025050', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'matthew', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(156, 'Eze', 'Chioma', NULL, NULL, 'Male', 'Single', '20143050050', 'CIE', 'SEET', 'Civil Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'emmanuel', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(157, 'Obasi', 'Emmanuel', NULL, NULL, 'Male', 'Single', '20141025065', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'ayodele', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(158, 'Nworuh', 'Christain', NULL, NULL, 'Male', 'Single', '20141025080', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'peter', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(159, 'Omaka', 'Blessing', 'Jane', NULL, 'Female', 'Single', '20143055080', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'mohammed', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(160, 'Onwuzurike', 'Joseph', NULL, NULL, 'Male', 'Single', '20142035020', 'IMT', 'SMAT', 'Information Management Technology', 2014, 'JME', NULL, 'B.Tech', 5, 'evelyn', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(161, 'Orji', 'Samuel', NULL, NULL, 'Male', 'Single', '20142035035', 'IMT', 'SMAT', 'Information Management Technology', 2014, 'DIRECT', NULL, 'B.Tech', 5, 'barbara', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(162, 'Adelowo', 'Oluwasoore', NULL, NULL, 'Female', 'Single', '20142040080', 'PMT', 'SMAT', 'Project Management Technology', 2014, 'JME', NULL, 'B.Tech', 5, 'stanley', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(163, 'Samuel', 'Awah', NULL, NULL, 'Male', 'Single', '20141025215', 'PHY', 'SOSC', 'Industrial Physics', 2014, 'JME', NULL, 'B.Sc', 5, 'olukemi', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(164, 'Uche', 'Nkechinyere', NULL, NULL, 'Female', 'Single', '20143055185', 'MEE', 'SEET', 'Mechanical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'kamilu', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(165, 'Igbo', 'Ginika', 'Mercy', NULL, 'Female', 'Married', '20143045200', 'CHE', 'SEET', 'Chemical Engineering', 2014, 'JME', NULL, 'B.Eng', 5, 'ciroma', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(166, 'Obiora', 'Miracle', 'T', NULL, 'Male', 'Single', '20151025005', 'PHY', 'SOSC', 'Industrial Physics', 2015, 'JME', NULL, 'B.Sc', 5, 'andrew', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(167, 'Dan', 'Emefon', NULL, NULL, 'Male', 'Single', '20151020020', 'BIO', 'SOSC', 'Industrial Microbiology', 2015, 'JME', NULL, 'B.Sc', 5, 'reginald', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(168, 'Amobi', 'Victory', 'O', NULL, 'Female', 'Single', '20133050080', 'CIE', 'SEET', 'Civil Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'selina', 'images/global/avatars/_avatar_female_3.jpg', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(169, 'Naabon', 'Racheal', NULL, NULL, 'Male', 'Single', '20153055005', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'solomon', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(170, 'Okeke', 'Amara', NULL, NULL, 'Female', 'Single', '20151015035', 'CHM', 'SOSC', 'Industrial Chemistry', 2015, 'DIRECT', NULL, 'B.Sc', 5, 'nneka', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(171, 'Anyaeruba', 'Chukwudi', NULL, NULL, 'Male', 'Single', '20153045020', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'olagoke', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(172, 'Ahamefula', 'Chiedozie', NULL, NULL, 'Male', 'Single', '20151015050', 'CHM', 'SOSC', 'Industrial Chemistry', 2015, 'JME', NULL, 'B.Sc', 5, 'olayinka', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(173, 'Godwin', 'Mercy', 'E', NULL, 'Male', 'Single', '20153045110', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'samuel', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(174, 'Ekeke', 'Chinaza', NULL, NULL, 'Male', 'Single', '20153055125', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'gladys', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(175, 'Egwuatu', 'Chuka', NULL, NULL, 'Male', 'Single', '20152035035', 'IMT', 'SMAT', 'Information Management Technology', 2015, 'DIRECT', NULL, 'B.Tech', 5, 'humphrey', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(176, 'Umemba', 'Emmanuel', NULL, NULL, 'Male', 'Single', '20151025095', 'PHY', 'SOSC', 'Industrial Physics', 2015, 'JME', NULL, 'B.Sc', 5, 'john', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(177, 'Oliver', 'Chidera', NULL, NULL, 'Female', 'Single', '20151020100', 'BIO', 'SOSC', 'Industrial Microbiology', 2015, 'JME', NULL, 'B.Sc', 5, 'ogechi', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(178, 'Okoli', 'Ugochukwu', NULL, NULL, 'Male', 'Single', '20151020125', 'BIO', 'SOSC', 'Industrial Microbiology', 2015, 'DIRECT', NULL, 'B.Sc', 5, 'charles', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(179, 'Okenagba', 'Faith', 'S', NULL, 'Female', 'Single', '20153050140', 'CIE', 'SEET', 'Civil Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'chinyere', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(180, 'Uwakwe', 'Justice', 'T', NULL, 'Male', 'Single', '20153055155', 'MEE', 'SEET', 'Mechanical Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'christopher', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(181, 'Egbomuche', 'Emmanuel', NULL, NULL, 'Male', 'Single', '20153050170', 'CIE', 'SEET', 'Civil Engineering', 2015, 'JME', NULL, 'B.Eng', 5, 'godsent', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(182, 'Ezerioha', 'MaryJane', NULL, NULL, 'Female', 'Single', '20153045185', 'CHE', 'SEET', 'Chemical Engineering', 2015, 'DIRECT', NULL, 'B.Eng', 5, 'adedeji ', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(183, 'Eluwa', 'Emmanuel', NULL, NULL, 'Male', 'Single', '20151020140', 'BIO', 'SOSC', 'Industrial Microbiology', 2015, 'JME', NULL, 'B.Sc', 5, 'adeyemi', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(184, 'Chibundu', 'Kelechi', NULL, NULL, 'Female', 'Single', '20161015020', 'CHM', 'SOSC', 'Industrial Chemistry', 2016, 'JME', NULL, 'B.Sc', 5, 'albert', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(185, 'Amaugo', 'Kindness', 'I', NULL, 'Male', 'Single', '20162035020', 'IMT', 'SMAT', 'Information Management Technology', 2016, 'JME', NULL, 'B.Tech', 5, 'chibundo', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(186, 'Ogechi', 'Solomon', NULL, NULL, 'Female', 'Single', '20162040035', 'PMT', 'SMAT', 'Project Management Technology', 2016, 'DIRECT', NULL, 'B.Tech', 5, 'adekola', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(187, 'Nworuh', 'Godwinner', NULL, NULL, 'Male', 'Single', '20161030035', 'MTH', 'SOSC', 'Mathematics', 2016, 'JME', NULL, 'B.Sc', 5, 'samuel', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(188, 'Obidoo', 'Chinaemerem', NULL, NULL, 'Male', 'Single', '20163055020', 'MEE', 'SEET', 'Mechanical Engineering', 2016, 'JME', NULL, 'B.Eng', 5, 'okechukwu', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(189, 'Igbojianya', 'Oluchi', NULL, NULL, 'Female', 'Single', '20171025290', 'PHY', 'SOSC', 'Industrial Physics', 2017, 'DIRECT', NULL, 'B.Sc', 5, 'henry', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(190, 'Richard', 'ChigozieK', NULL, NULL, 'Male', 'Single', '20173050170', 'CIE', 'SEET', 'Civil Engineering', 2017, 'DIRECT', NULL, 'B.Eng', 5, 'chima', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(191, 'Ike', 'Henry', NULL, NULL, 'Male', 'Single', '20171020050', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'adeola', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(192, 'Dike', 'Ifeanyi', NULL, NULL, 'Male', 'Single', '20172035095', 'IMT', 'SMAT', 'Information Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'yahaya', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(193, 'Echi', 'Clifford', NULL, NULL, 'Male', 'Single', '20172040065', 'PMT', 'SMAT', 'Project Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'rasaaq', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(194, 'Umunabuike', 'Johnbosco', NULL, NULL, 'Male', 'Single', '20173055005', 'MEE', 'SEET', 'Mechanical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'petronilla', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(195, 'Udoh', 'Emediong', NULL, NULL, 'Male', 'Single', '20172040110', 'PMT', 'SMAT', 'Project Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'nancy', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(196, 'Ndu', 'Victor', 'O', NULL, 'Male', 'Single', '20131030020', 'MTH', 'SOSC', 'Mathematics', 2013, 'JME', NULL, 'B.Sc', 5, 'mudasiru', 'images/global/avatars/passport.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(197, 'Oparaocha', 'ChidozieD', NULL, NULL, 'Male', 'Single', '20133050020', 'CIE', 'SEET', 'Civil Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'ginika', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(198, 'Dominic', 'Precious', 'Gold', NULL, 'Female', 'Single', '20133055035', 'MEE', 'SEET', 'Mechanical Engineering', 2013, 'JME', NULL, 'B.Eng', 5, 'fidelia', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(199, 'Hoku', 'Amachundi', 'M', NULL, 'Male', 'Single', '20132040025', 'PMT', 'SMAT', 'Project Management Technology', 2013, 'JME', NULL, 'B.Tech', 5, 'caleb', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(200, 'Nkwopara', 'Mary', 'C', NULL, 'Female', 'Single', '20131025005', 'PHY', 'SOSC', 'Industrial Physics', 2013, 'JME', NULL, 'B.Sc', 5, 'basden', 'images/global/avatars/_avatar_female_3.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(201, 'Fedrick', 'Chiemeka', 'Ezenwa', NULL, 'Male', 'Single', '20171020260', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'JME', NULL, 'B.Sc', 5, 'nvictor', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(202, 'Chimaroke', 'Nnamdi', 'Z', NULL, 'Male', 'Single', '20171020185', 'BIO', 'SOSC', 'Industrial Microbiology', 2017, 'DIRECT', NULL, 'B.Sc', 5, 'raphael', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(203, 'Iregbu', 'Emmanuela', NULL, NULL, 'Female', 'Single', '20171015140', 'CHM', 'SOSC', 'Industrial Chemistry', 2017, 'JME', NULL, 'B.Sc', 5, 'uvie', 'images/global/avatars/_avatar_female_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(204, 'Ibegbulam', 'Izuchukwu', NULL, NULL, 'Male', 'Single', '20172035020', 'IMT', 'SMAT', 'Information Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'vincent', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(205, 'Umunnabuike', 'Johnbosco', 'T', NULL, 'Male', 'Single', '20172040125', 'PMT', 'SMAT', 'Project Management Technology', 2017, 'JME', NULL, 'B.Tech', 5, 'mustafa', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(206, 'Obiora', 'Miracle', 'Tobechukwu', NULL, 'Male', 'Single', '20171030095', 'MTH', 'SOSC', 'Mathematics', 2017, 'JME', NULL, 'B.Sc', 5, 'osaretin', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(207, 'Nwanyanwu', 'Fredrick', 'C', NULL, 'Male', 'Single', '20173055185', 'MEE', 'SEET', 'Mechanical Engineering', 2017, 'JME', NULL, 'B.Eng', 5, 'patience', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(208, 'Ibeawuchi', 'Ekpe', 'N', NULL, 'Male', 'Single', '20181020050', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'gandhi', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'ibeawuchiekpe@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(209, 'Temitope', 'Olabode', 'H', NULL, 'Female', 'Single', '20183050035', 'CIE', 'SEET', 'Civil Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'martin', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'temitopeolabode@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(210, 'Abiola ', 'Olabisi', 'Michael', NULL, 'Female', 'Single', '20182035020', 'IMT', 'SMAT', 'Information Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'michael', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'abiola olabisi@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(211, 'Adeyi', 'Peter', 'Tosin', NULL, 'Male', 'Single', '20182040005', 'PMT', 'SMAT', 'Project Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'benedict', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'adeyipeter@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(212, 'Alao', 'Taiwo', 'O', NULL, 'Male', 'Single', '20181015020', 'CHM', 'SOSC', 'Industrial Chemistry', 2018, 'JME', NULL, 'B.Sc', 5, 'ebenezer', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'alaotaiwo@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(213, 'Jibrin', 'Sunday', 'E', NULL, 'Male', 'Single', '20183045050', 'CHE', 'SEET', 'Chemical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'esther', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'jibrinsunday@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(214, 'Toryila', 'Solomon', 'Mule', NULL, 'Male', 'Single', '20183055005', 'MEE', 'SEET', 'Mechanical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'austin', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'toryilasolomon@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(215, 'Idiahiri', 'Evelyn', 'M', NULL, 'Female', 'Single', '20183045065', 'CHE', 'SEET', 'Chemical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'balarabe ', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'idiahirievelyn@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(216, 'Chiemeka', 'Emmanuel', 'U', NULL, 'Male', 'Single', '20181030005', 'MTH', 'SOSC', 'Mathematics', 2018, 'JME', NULL, 'B.Sc', 5, 'sunday', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'chiemekaemmanuel@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(217, 'Ejor', 'Adams', 'Moses', NULL, 'Male', 'Single', '20182035050', 'IMT', 'SMAT', 'Information Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'oluranti', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'ejoradams@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(218, 'Onwuka', 'Ijeoma', NULL, NULL, 'Male', 'Single', '20181015080', 'CHM', 'SOSC', 'Industrial Chemistry', 2018, 'JME', NULL, 'B.Sc', 5, 'macleans', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'onwukaijeoma@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(219, 'Stephen', 'Temitope', 'K', NULL, 'Female', 'Single', '20181020065', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'clement', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'stephentemitope@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(220, 'Onojayefe', 'Sampson', 'Obehi', NULL, 'Male', 'Single', '20181015110', 'CHM', 'SOSC', 'Industrial Chemistry', 2018, 'JME', NULL, 'B.Sc', 5, 'angela', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'onojayefesampson@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(221, 'Kehinde', 'Omolola', 'E', NULL, 'Male', 'Single', '20183050080', 'CIE', 'SEET', 'Civil Engineering', 2018, 'DIRECT', NULL, 'B.Eng', 5, 'umar g', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'kehindeomolola@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(222, 'Daniel', 'Olagoke', 'A', NULL, 'Male', 'Single', '20183055020', 'MEE', 'SEET', 'Mechanical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'oluwole', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'danielolagoke@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(223, 'Ukeh', 'Chidi', 'N', NULL, 'Male', 'Single', '20181020125', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'mahmoud', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'ukehchidi@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(224, 'Fadeyi', 'Comfort', 'Damilola', NULL, 'Female', 'Single', '20181030035', 'MTH', 'SOSC', 'Mathematics', 2018, 'JME', NULL, 'B.Sc', 5, 'doris', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'fadeyicomfort@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(225, 'Gbadero', 'Hezekiah', 'D', NULL, 'Male', 'Single', '20181015140', 'CHM', 'SOSC', 'Industrial Chemistry', 2018, 'JME', NULL, 'B.Sc', 5, 'anthony', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'gbaderohezekiah@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(226, 'Okoro', 'Victor', 'O', NULL, 'Male', 'Single', '20181020170', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'philip', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'okorovictor@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(227, 'Adigun', 'Oluwakemi', 'Tolu', NULL, 'Female', 'Single', '20183045095', 'CHE', 'SEET', 'Chemical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'muhammad', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'adigunoluwakemi@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(228, 'Jonathan', 'Adokige', 'W', NULL, 'Male', 'Single', '20181025155', 'PHY', 'SOSC', 'Industrial Physics', 2018, 'JME', NULL, 'B.Sc', 5, 'folashade', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'jonathanadokige@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(229, 'Michael', 'Joseph', 'O', NULL, 'Male', 'Single', '20181025275', 'PHY', 'SOSC', 'Industrial Physics', 2018, 'JME', NULL, 'B.Sc', 5, 'basil', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'michaeljoseph@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(230, 'Onifade', 'Omowunmi', 'W', NULL, 'Male', 'Single', '20183050140', 'CIE', 'SEET', 'Civil Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'tochukwu', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'onifadeomowunmi@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(231, 'Obiobo', 'Paul', 'U. L.', NULL, 'Male', 'Single', '20183045125', 'CHE', 'SEET', 'Chemical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'oluwafunke', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'obiobopaul@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(232, 'Udenyi', 'Sunday', 'O', NULL, 'Male', 'Single', '20182040035', 'PMT', 'SMAT', 'Project Management Technology', 2018, 'DIRECT', NULL, 'B.Tech', 5, 'madubueze', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'udenyisunday@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(233, 'Odaba', 'Victoria', 'S', NULL, 'Female', 'Single', '20183055110', 'MEE', 'SEET', 'Mechanical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'dike', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'odabavictoria@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(234, 'Okedele', 'Samuel', 'Olaoluwa', NULL, 'Male', 'Single', '20181030095', 'MTH', 'SOSC', 'Mathematics', 2018, 'JME', NULL, 'B.Sc', 5, 'anthonia', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'okedelesamuel@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(235, 'Idahosa', 'Paul', NULL, NULL, 'Male', 'Single', '20183050170', 'CIE', 'SEET', 'Civil Engineering', 2018, 'DIRECT', NULL, 'B.Eng', 5, 'victor', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'idahosapaul@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(236, 'Badmus', 'Michael', 'K', NULL, 'Male', 'Single', '20181020185', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'DIRECT', NULL, 'B.Sc', 5, 'pascal', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'badmusmichael@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(237, 'Odediran', 'Anuoluwa', NULL, NULL, 'Female', 'Single', '20183045200', 'CHE', 'SEET', 'Chemical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'matthew', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'odedirananuoluwa@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(238, 'Ibrahim', 'Adeola', 'M', NULL, 'Female', 'Single', '20183045230', 'CHE', 'SEET', 'Chemical Engineering', 2018, 'DIRECT', NULL, 'B.Eng', 5, 'emmanuel', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'ibrahimadeola@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(239, 'Abisoye', 'Stephen', 'K', NULL, 'Male', 'Single', '20181025290', 'PHY', 'SOSC', 'Industrial Physics', 2018, 'DIRECT', NULL, 'B.Sc', 5, 'ayodele', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'abisoyestephen@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(240, 'Ani', 'Sandra', 'K', NULL, 'Female', 'Single', '20183050215', 'CIE', 'SEET', 'Civil Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'wilson', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'anisandra@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active');
INSERT INTO `mums_users_undergraduates` (`id`, `first_name`, `last_name`, `name_initials`, `former_name`, `sex`, `marital_status`, `reg_no`, `dept`, `faculty`, `course_study`, `year_entry`, `mode_of_entry`, `year_admission`, `award_in_view`, `course_duration`, `guardian`, `photo_location`, `permantent_address`, `contact_address`, `phone_number`, `email`, `blood_group`, `date_birth`, `place_birth`, `next_kin`, `next_kin_relationship`, `next_kin_address`, `sponsor`, `sponsor_address`, `religion`, `place_origin_state`, `place_origin_lga`, `place_origin_town`, `highest_qualification`, `institution_obtained`, `mode_study`, `physical_condition`, `signature_path`, `created_at`, `updated_at`, `can_register`, `user_type`, `cleared_by`, `status`) VALUES
(241, 'Jesuloluwa', 'Sefunmi', NULL, NULL, 'Female', 'Single', '20181020200', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'peter', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'jesuloluwasefunmi@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(242, 'Owolabi', 'David', 'Seyi', NULL, 'Male', 'Single', '20182035080', 'IMT', 'SMAT', 'Information Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'mohammed', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'owolabidavid@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(243, 'Egenti', 'Emmanuel', 'C', NULL, 'Male', 'Single', '20182040065', 'PMT', 'SMAT', 'Project Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'evelyn', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'egentiemmanuel@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(244, 'Oluwatayo', 'Michael', NULL, NULL, 'Male', 'Single', '20183055155', 'MEE', 'SEET', 'Mechanical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'barbara', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'oluwatayomichael@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(245, 'Rufus', 'Ebere', 'Clinton', NULL, 'Male', 'Single', '20182040110', 'PMT', 'SMAT', 'Project Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'stanley', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'rufusebere@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(246, 'Olatuyi', 'Olayinka', 'C', NULL, 'Male', 'Single', '20181020215', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'olukemi', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'olatuyiolayinka@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(247, 'Eke', 'Chidi', 'Collins', NULL, 'Male', 'Single', '20181020260', 'BIO', 'SOSC', 'Industrial Microbiology', 2018, 'JME', NULL, 'B.Sc', 5, 'kamilu', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'ekechidi@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(248, 'Akaji', 'Tochukwu', 'A', NULL, 'Male', 'Single', '20181015245', 'CHM', 'SOSC', 'Industrial Chemistry', 2018, 'JME', NULL, 'B.Sc', 5, 'ciroma', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'akajitochukwu@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(249, 'Ciroma', 'Agnes', 'C', NULL, 'Female', 'Single', '20182035095', 'IMT', 'SMAT', 'Information Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'andrew', 'images/global/avatars/_avatar_female_1.png', NULL, NULL, NULL, 'ciromaagnes@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(250, 'Temple', 'Shokanitan', 'E', NULL, 'Male', 'Single', '20182040125', 'PMT', 'SMAT', 'Project Management Technology', 2018, 'JME', NULL, 'B.Tech', 5, 'reginald', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'templeshokanitan@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(251, 'Samson', 'Adelaja', 'G', NULL, 'Male', 'Single', '20181030230', 'MTH', 'SOSC', 'Mathematics', 2018, 'JME', NULL, 'B.Sc', 5, 'selina', 'images/global/avatars/_avatar_male_2.png', NULL, NULL, NULL, 'samsonadelaja@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active'),
(252, 'Godfrey', 'Okorieh', 'M', NULL, 'Male', 'Single', '20183055185', 'MEE', 'SEET', 'Mechanical Engineering', 2018, 'JME', NULL, 'B.Eng', 5, 'solomon', 'images/global/avatars/_avatar_male_1.png', NULL, NULL, NULL, 'godfreyokorieh@newmail.com', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2018-01-09 09:31:26', '2018-01-09 09:31:26', 1, 'undergraduate', NULL, 'active');

-- --------------------------------------------------------

--
-- Table structure for table `mums_user_roles`
--

CREATE TABLE `mums_user_roles` (
  `user_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `test_course`
--

CREATE TABLE `test_course` (
  `id` int(11) NOT NULL,
  `old_course` varchar(191) DEFAULT NULL,
  `new_course` varchar(191) DEFAULT NULL,
  `semester` int(11) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `course_source` varchar(191) DEFAULT NULL,
  `course_destination_dept` varchar(191) DEFAULT NULL,
  `student_dept` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_course`
--

INSERT INTO `test_course` (`id`, `old_course`, `new_course`, `semester`, `level`, `course_source`, `course_destination_dept`, `student_dept`) VALUES
(1, 'PMT301', 'PMT501', 1, 500, 'PMT', 'PMT', 'PMT'),
(2, 'ENG303', 'PMT503', 1, 500, 'PMT', 'PMT', 'PMT'),
(3, 'PMT307', 'PMT505', 1, 500, 'PMT', 'PMT', 'PMT'),
(4, 'PMT303', 'PMT507', 1, 500, 'PMT', 'PMT', 'PMT'),
(5, 'ENG301', 'PMT509', 1, 500, 'PMT', 'PMT', 'PMT'),
(6, 'ENG307', 'PMT511', 1, 500, 'PMT', 'PMT', 'PMT'),
(7, 'PMT305', 'PMT513', 1, 500, 'PMT', 'PMT', 'PMT'),
(8, 'ENG302', 'PMT502', 2, 500, 'PMT', 'PMT', 'PMT'),
(9, 'PMT306', 'PMT504', 2, 500, 'PMT', 'PMT', 'PMT'),
(10, 'PMT302', 'PMT506', 2, 500, 'PMT', 'PMT', 'PMT'),
(11, 'PMT308', 'PMT508', 2, 500, 'PMT', 'PMT', 'PMT'),
(12, 'ENG304', 'PMT510', 2, 500, 'PMT', 'PMT', 'PMT'),
(13, 'PMT304', 'PMT512', 2, 500, 'PMT', 'PMT', 'PMT'),
(14, '', 'PMT514', 2, 500, 'PMT', 'PMT', ''),
(15, 'MTH303', 'PHY501', 1, 500, 'PHY', 'PHY', 'PHY'),
(16, 'PHY303', 'PHY503', 1, 500, 'PHY', 'PHY', 'PHY'),
(17, 'PHY307', 'PHY505', 1, 500, 'PHY', 'PHY', 'PHY'),
(18, 'PHY301', 'PHY507', 1, 500, 'PHY', 'PHY', 'PHY'),
(19, 'PHY309', 'PHY509', 1, 500, 'PHY', 'PHY', 'PHY'),
(20, 'PHY311', 'PHY511', 1, 500, 'PHY', 'PHY', 'PHY'),
(21, 'PHY305', 'PHY513', 1, 500, 'PHY', 'PHY', 'PHY'),
(22, 'PHY302', 'PHY502', 2, 500, 'PHY', 'PHY', 'PHY'),
(23, 'PHY310', 'PHY504', 2, 500, 'PHY', 'PHY', 'PHY'),
(24, 'MTH304', 'PHY506', 2, 500, 'PHY', 'PHY', 'PHY'),
(25, 'PHY312', 'PHY508', 2, 500, 'PHY', 'PHY', 'PHY'),
(26, 'PHY306', 'PHY510', 2, 500, 'PHY', 'PHY', 'PHY'),
(27, 'PHY304', 'PHY512', 2, 500, 'PHY', 'PHY', 'PHY'),
(28, 'PHY308', 'PHY514', 2, 500, 'PHY', 'PHY', 'PHY'),
(29, 'MTH307', 'MTH501', 1, 500, 'MTH', 'MTH', 'MTH'),
(30, 'MTH301', 'MTH503', 1, 500, 'MTH', 'MTH', 'MTH'),
(31, 'PHY305', 'MTH505', 1, 500, 'MTH', 'MTH', 'MTH'),
(32, 'MTH303', 'MTH507', 1, 500, 'MTH', 'MTH', 'MTH'),
(33, 'PHY303', 'PHY511', 1, 500, 'PHY', 'MTH', 'MTH'),
(34, 'MTH305', 'MTH502', 2, 500, 'MTH', 'MTH', 'MTH'),
(35, 'MTH309', 'MTH504', 2, 500, 'MTH', 'MTH', 'MTH'),
(36, 'PHY306', 'MTH506', 2, 500, 'MTH', 'MTH', 'MTH'),
(37, 'MTH308', 'MTH508', 2, 500, 'MTH', 'MTH', 'MTH'),
(38, 'MTH302', 'PHY512', 2, 500, 'PHY', 'MTH', 'MTH'),
(39, 'PHY304', 'MEE501', 1, 500, 'MEE', 'MEE', 'MTH'),
(40, 'MTH310', 'MEE503', 1, 500, 'MEE', 'MEE', 'MTH'),
(41, 'MTH304', 'MEE505', 1, 500, 'MEE', 'MEE', 'MTH'),
(42, 'MTH306', 'MEE509', 1, 500, 'MEE', 'MEE', 'MTH'),
(43, 'MEE303', 'MEE511', 1, 500, 'MEE', 'MEE', 'MEE'),
(44, 'ENG301', 'MEE513', 1, 500, 'MEE', 'MEE', 'MEE'),
(45, 'ENG307', 'MEE502', 2, 500, 'MEE', 'MEE', 'MEE'),
(46, 'MEE305', 'MEE504', 2, 500, 'MEE', 'MEE', 'MEE'),
(47, 'ENG303', 'MEE506', 2, 500, 'MEE', 'MEE', 'MEE'),
(48, 'MEE301', 'MEE510', 2, 500, 'MEE', 'MEE', 'MEE'),
(49, 'MEE309', 'MEE512', 2, 500, 'MEE', 'MEE', 'MEE'),
(50, 'MEE310', 'MEE514', 2, 500, 'MEE', 'MEE', 'MEE'),
(51, 'ENG304', 'IMT501', 1, 500, 'IMT', 'IMT', 'MEE'),
(52, 'MEE304', 'IMT503', 1, 500, 'IMT', 'IMT', 'MEE'),
(53, 'MEE306', 'IMT505', 1, 500, 'IMT', 'IMT', 'MEE'),
(54, 'ENG302', 'IMT507', 1, 500, 'IMT', 'IMT', 'MEE'),
(55, 'ENG308', 'IMT509', 1, 500, 'IMT', 'IMT', 'MEE'),
(56, 'MEE302', 'IMT511', 1, 500, 'IMT', 'IMT', 'MEE'),
(57, 'IMT307', 'IMT513', 1, 500, 'IMT', 'IMT', 'IMT'),
(58, 'IMT303', 'MTH501', 1, 500, 'MTH', 'IMT', 'IMT'),
(59, 'IMT309', 'MTH509', 1, 500, 'MTH', 'IMT', 'IMT'),
(60, 'MTH305', 'MTH509', 1, 500, 'MTH', 'IMT', 'IMT'),
(61, 'MTH309', 'IMT502', 2, 500, 'IMT', 'IMT', 'IMT'),
(62, 'IMT305', 'IMT504', 2, 500, 'IMT', 'IMT', 'IMT'),
(63, 'IMT301', 'IMT506', 2, 500, 'IMT', 'IMT', 'IMT'),
(64, 'IMT302', 'IMT508', 2, 500, 'IMT', 'IMT', 'IMT'),
(65, 'PMT204', 'IMT510', 2, 500, 'IMT', 'IMT', 'IMT'),
(66, 'IMT308', 'IMT512', 2, 500, 'IMT', 'IMT', 'IMT'),
(67, 'IMT304', 'IMT514', 2, 500, 'IMT', 'IMT', 'IMT'),
(68, 'IMT310', 'MTH502', 2, 500, 'MTH', 'IMT', 'IMT'),
(69, 'MTH306', 'MTH510', 2, 500, 'MTH', 'IMT', 'IMT'),
(70, 'IMT306', 'MTH510', 2, 500, 'MTH', 'IMT', 'IMT'),
(71, 'CIE305', 'CIE501', 1, 500, 'CIE', 'CIE', 'CIE'),
(72, 'CIE301', 'CIE503', 1, 500, 'CIE', 'CIE', 'CIE'),
(73, 'ENG303', 'CIE505', 1, 500, 'CIE', 'CIE', 'CIE'),
(74, 'CIE303', 'CIE507', 1, 500, 'CIE', 'CIE', 'CIE'),
(75, 'ENG301', 'CIE509', 1, 500, 'CIE', 'CIE', 'CIE'),
(76, 'ENG307', 'CIE511', 1, 500, 'CIE', 'CIE', 'CIE'),
(77, 'CIE304', 'CIE513', 1, 500, 'CIE', 'CIE', 'CIE'),
(78, 'ENG302', 'CIE502', 2, 500, 'CIE', 'CIE', 'CIE'),
(79, 'ENG308', 'CIE504', 2, 500, 'CIE', 'CIE', 'CIE'),
(80, 'CIE306', 'CIE506', 2, 500, 'CIE', 'CIE', 'CIE'),
(81, 'CIE302', 'CIE508', 2, 500, 'CIE', 'CIE', 'CIE'),
(82, 'ENG304', 'CIE510', 2, 500, 'CIE', 'CIE', 'CIE'),
(83, '', 'CIE512', 2, 500, 'CIE', 'CIE', ''),
(84, '', 'CIE514', 2, 500, 'CIE', 'CIE', ''),
(85, 'CHM305', 'BIO501', 1, 500, 'BIO', 'CHM', 'CHM'),
(86, 'BIO303', 'CHM501', 1, 500, 'CHM', 'CHM', 'CHM'),
(87, 'CHM301', 'CHM503', 1, 500, 'CHM', 'CHM', 'CHM'),
(88, 'CHM307', 'CHM505', 1, 500, 'CHM', 'CHM', 'CHM'),
(89, 'CHM303', 'CHM507', 1, 500, 'CHM', 'CHM', 'CHM'),
(90, 'PHY309', 'CHM509', 1, 500, 'CHM', 'CHM', 'CHM'),
(91, 'CHM309', 'BIO502', 2, 500, 'BIO', 'CHM', 'CHM'),
(92, 'CHM308', 'CHM502', 2, 500, 'CHM', 'CHM', 'CHM'),
(93, 'BIO304', 'CHM504', 2, 500, 'CHM', 'CHM', 'CHM'),
(94, 'CHM304', 'CHM506', 2, 500, 'CHM', 'CHM', 'CHM'),
(95, 'CHM310', 'CHM508', 2, 500, 'CHM', 'CHM', 'CHM'),
(96, 'PHY310', 'CHM510', 2, 500, 'CHM', 'CHM', 'CHM'),
(97, 'CHM306', 'CHE501', 1, 500, 'CHE', 'CHE', 'CHM'),
(98, 'CHM302', 'CHE503', 1, 500, 'CHE', 'CHE', 'CHM'),
(99, 'CHE305', 'CHE505', 1, 500, 'CHE', 'CHE', 'CHE'),
(100, 'ENG301', 'CHE507', 1, 500, 'CHE', 'CHE', 'CHE'),
(101, 'ENG307', 'CHE509', 1, 500, 'CHE', 'CHE', 'CHE'),
(102, 'CHE301', 'CHE511', 1, 500, 'CHE', 'CHE', 'CHE'),
(103, 'CHE303', 'CHE513', 1, 500, 'CHE', 'CHE', 'CHE'),
(104, 'ENG303', 'CHE502', 2, 500, 'CHE', 'CHE', 'CHE'),
(105, 'CHE302', 'CHE504', 2, 500, 'CHE', 'CHE', 'CHE'),
(106, 'CHE304', 'CHE506', 2, 500, 'CHE', 'CHE', 'CHE'),
(107, 'ENG304', 'CHE508', 2, 500, 'CHE', 'CHE', 'CHE'),
(108, 'CHE306', 'CHE510', 2, 500, 'CHE', 'CHE', 'CHE'),
(109, 'ENG302', 'CHE512', 2, 500, 'CHE', 'CHE', 'CHE'),
(110, 'ENG308', 'CHE514', 2, 500, 'CHE', 'CHE', 'CHE'),
(111, 'BIO303', 'BIO501', 1, 500, 'BIO', 'BIO', 'BIO'),
(112, 'CHM301', 'BIO503', 1, 500, 'BIO', 'BIO', 'BIO'),
(113, 'BIO305', 'BIO505', 1, 500, 'BIO', 'BIO', 'BIO'),
(114, 'PHY309', 'BIO507', 1, 500, 'BIO', 'BIO', 'BIO'),
(115, 'CHM203', 'CHM501', 1, 500, 'CHM', 'BIO', 'BIO'),
(116, 'BIO301', 'CHM509', 1, 500, 'CHM', 'BIO', 'BIO'),
(117, 'CHM204', 'BIO502', 2, 500, 'BIO', 'BIO', 'BIO'),
(118, 'PHY310', 'BIO504', 2, 500, 'BIO', 'BIO', 'BIO'),
(119, 'BIO302', 'BIO506', 2, 500, 'BIO', 'BIO', 'BIO'),
(120, 'CHM302', 'BIO508', 2, 500, 'BIO', 'BIO', 'BIO'),
(121, 'BIO306', 'CHM502', 2, 500, 'CHM', 'BIO', 'BIO'),
(122, 'BIO304', 'CHM510', 2, 500, 'CHM', 'BIO', 'BIO'),
(123, 'BIO301', 'BIO401', 1, 400, 'BIO', 'BIO', 'BIO'),
(124, 'BIO303', 'BIO403', 1, 400, 'BIO', 'BIO', 'BIO'),
(125, 'BIO305', 'BIO405', 1, 400, 'BIO', 'BIO', 'BIO'),
(126, 'CHM203', 'BIO407', 1, 400, 'BIO', 'BIO', 'BIO'),
(127, 'CHM301', 'CHM401', 1, 400, 'CHM', 'BIO', 'BIO'),
(128, 'PHY309', 'CHM409', 1, 400, 'CHM', 'BIO', 'BIO'),
(129, '', 'SIW400', 1, 400, 'SIW', 'BIO', 'BIO'),
(130, 'CHE301', 'CHE401', 1, 400, 'CHE', 'CHE', 'CHE'),
(131, 'CHE303', 'CHE403', 1, 400, 'CHE', 'CHE', 'CHE'),
(132, 'CHE305', 'CHE405', 1, 400, 'CHE', 'CHE', 'CHE'),
(133, 'ENG301', 'CHE407', 1, 400, 'CHE', 'CHE', 'CHE'),
(134, 'ENG303', 'CHE409', 1, 400, 'CHE', 'CHE', 'CHE'),
(135, 'ENG307', 'CHE411', 1, 400, 'CHE', 'CHE', 'CHE'),
(136, '', 'CHE413', 1, 400, 'CHE', 'CHE', 'CHE'),
(137, '', 'SIW400', 1, 400, 'SIW', 'CHE', 'CHE'),
(138, 'BIO303', 'BIO401', 1, 400, 'BIO', 'CHM', 'CHM'),
(139, 'CHM301', 'CHM407', 1, 400, 'CHM', 'CHM', 'CHM'),
(140, 'CHM303', 'CHM401', 1, 400, 'CHM', 'CHM', 'CHM'),
(141, 'CHM305', 'CHM403', 1, 400, 'CHM', 'CHM', 'CHM'),
(142, 'CHM307', 'CHM405', 1, 400, 'CHM', 'CHM', 'CHM'),
(143, 'CHM309', 'CHM409', 1, 400, 'CHM', 'CHM', 'CHM'),
(144, '', 'SIW400', 1, 400, 'SIW', 'CHM', 'CHM'),
(145, 'CIE301', 'CIE401', 1, 400, 'CIE', 'CIE', 'CIE'),
(146, 'CIE303', 'CIE403', 1, 400, 'CIE', 'CIE', 'CIE'),
(147, 'CIE305', 'CIE405', 1, 400, 'CIE', 'CIE', 'CIE'),
(148, 'ENG301', 'CIE407', 1, 400, 'CIE', 'CIE', 'CIE'),
(149, 'ENG303', 'CIE409', 1, 400, 'CIE', 'CIE', 'CIE'),
(150, 'ENG307', 'CIE411', 1, 400, 'CIE', 'CIE', 'CIE'),
(151, '', 'CIE413', 1, 400, 'CIE', 'CIE', 'CIE'),
(152, '', 'SIW400', 1, 400, 'SIW', 'CIE', 'CIE'),
(153, 'IMT301', 'MTH401', 1, 400, 'MTH', 'IMT', 'IMT'),
(154, 'IMT303', 'MTH409', 1, 400, 'MTH', 'IMT', 'IMT'),
(155, 'IMT305', 'IMT401', 1, 400, 'IMT', 'IMT', 'IMT'),
(156, 'IMT307', 'IMT403', 1, 400, 'IMT', 'IMT', 'IMT'),
(157, 'IMT309', 'IMT405', 1, 400, 'IMT', 'IMT', 'IMT'),
(158, 'MTH305', 'IMT407', 1, 400, 'IMT', 'IMT', 'IMT'),
(159, 'MTH309', 'IMT409', 1, 400, 'IMT', 'IMT', 'IMT'),
(160, '', 'IMT411', 1, 400, 'IMT', 'IMT', 'IMT'),
(161, '', 'IMT413', 1, 400, 'IMT', 'IMT', 'IMT'),
(162, '', 'SIW400', 1, 400, 'SIW', 'IMT', 'IMT'),
(163, 'ENG301', 'MEE401', 1, 400, 'MEE', 'MEE', 'MEE'),
(164, 'ENG303', 'MEE403', 1, 400, 'MEE', 'MEE', 'MEE'),
(165, 'ENG307', 'MEE405', 1, 400, 'MEE', 'MEE', 'MEE'),
(166, 'MEE301', 'MEE409', 1, 400, 'MEE', 'MEE', 'MEE'),
(167, 'MEE303', 'MEE411', 1, 400, 'MEE', 'MEE', 'MEE'),
(168, 'MEE305', 'MEE413', 1, 400, 'MEE', 'MEE', 'MEE'),
(169, '', 'SIW400', 1, 400, 'SIW', 'MEE', 'MEE'),
(170, 'MTH301', 'MTH401', 1, 400, 'MTH', 'MTH', 'MTH'),
(171, 'MTH303', 'MTH403', 1, 400, 'MTH', 'MTH', 'MTH'),
(172, 'MTH305', 'MTH405', 1, 400, 'MTH', 'MTH', 'MTH'),
(173, 'MTH307', 'MTH407', 1, 400, 'MTH', 'MTH', 'MTH'),
(174, 'MTH309', 'PHY411', 1, 400, 'PHY', 'MTH', 'MTH'),
(175, '', 'SIW400', 1, 400, 'SIW', 'MTH', 'MTH'),
(176, 'MTH303', 'PHY401', 1, 400, 'PHY', 'PHY', 'PHY'),
(177, 'PHY301', 'PHY403', 1, 400, 'PHY', 'PHY', 'PHY'),
(178, 'PHY303', 'PHY405', 1, 400, 'PHY', 'PHY', 'PHY'),
(179, 'PHY305', 'PHY407', 1, 400, 'PHY', 'PHY', 'PHY'),
(180, 'PHY307', 'PHY409', 1, 400, 'PHY', 'PHY', 'PHY'),
(181, 'PHY309', 'PHY411', 1, 400, 'PHY', 'PHY', 'PHY'),
(182, 'PHY311', 'PHY413', 1, 400, 'PHY', 'PHY', 'PHY'),
(183, '', 'SIW400', 1, 400, 'SIW', 'PHY', 'PHY'),
(184, 'ENG301', 'PMT401', 1, 400, 'PMT', 'PMT', 'PMT'),
(185, 'ENG303', 'PMT403', 1, 400, 'PMT', 'PMT', 'PMT'),
(186, 'ENG307', 'PMT405', 1, 400, 'PMT', 'PMT', 'PMT'),
(187, 'PMT301', 'PMT407', 1, 400, 'PMT', 'PMT', 'PMT'),
(188, 'PMT303', 'PMT409', 1, 400, 'PMT', 'PMT', 'PMT'),
(189, 'PMT305', 'PMT411', 1, 400, 'PMT', 'PMT', 'PMT'),
(190, 'PMT307', 'PMT413', 1, 400, 'PMT', 'PMT', 'PMT'),
(191, 'MTH306', 'SIW400', 1, 400, 'SIW', 'PMT', 'PMT'),
(192, 'BIO201', 'BIO301', 1, 300, 'BIO', 'BIO', 'BIO'),
(193, 'BIO203', 'PHY309', 1, 300, 'PHY', 'BIO', 'BIO'),
(194, 'CHM207', 'CHM301', 1, 300, 'CHM', 'BIO', 'BIO'),
(195, 'CSC201', 'BIO307', 1, 300, 'BIO', 'BIO', 'BIO'),
(196, 'ENG201', 'BIO305', 1, 300, 'BIO', 'BIO', 'BIO'),
(197, 'GST201', 'BI0303', 1, 300, 'BIO', 'BIO', 'BIO'),
(198, 'MTH201', 'CHM302', 2, 300, 'CHM', 'BIO', 'BIO'),
(199, 'BIO202', 'BIO308', 2, 300, 'BIO', 'BIO', 'BIO'),
(200, 'BIO204', 'BIO304', 2, 300, 'BIO', 'BIO', 'BIO'),
(201, 'CHM208', 'PHY310', 2, 300, 'PHY', 'BIO', 'BIO'),
(202, 'CSC202', 'BIO306', 2, 300, 'BIO', 'BIO', 'BIO'),
(203, 'ENG202', 'BIO302', 2, 300, 'BIO', 'BIO', 'BIO'),
(204, 'GST202', 'CHE303', 1, 300, 'CHE', 'CHE', 'CHE'),
(205, 'MTH202', 'ENG307', 1, 300, 'CHE', 'CHE', 'CHE'),
(206, 'CHE201', 'ENG303', 1, 300, 'MEE', 'CHE', 'CHE'),
(207, 'CSC201', 'ENG301', 1, 300, 'CIE', 'CHE', 'CHE'),
(208, 'ENG201', 'CHE301', 1, 300, 'CHE', 'CHE', 'CHE'),
(209, 'ENG203', 'CHE305', 1, 300, 'CHE', 'CHE', 'CHE'),
(210, 'ENG207', 'ENG308', 2, 300, 'CHE', 'CHE', 'CHE'),
(211, 'GST201', 'CHE306', 2, 300, 'CHE', 'CHE', 'CHE'),
(212, 'MTH201', 'CHE304', 2, 300, 'CHE', 'CHE', 'CHE'),
(213, 'MTH203', 'CHE302', 2, 300, 'CHE', 'CHE', 'CHE'),
(214, 'CHE202', 'ENG302', 2, 300, 'CIE', 'CHE', 'CHE'),
(215, 'CSC202', 'ENG304', 2, 300, 'MEE', 'CHE', 'CHE'),
(216, 'ENG202', 'CHM305', 1, 300, 'CHM', 'CHM', 'CHM'),
(217, 'ENG204', 'CHM307', 1, 300, 'CHM', 'CHM', 'CHM'),
(218, 'ENG208', 'CHM309', 1, 300, 'CHM', 'CHM', 'CHM'),
(219, 'GST202', 'CHM301', 1, 300, 'CHM', 'CHM', 'CHM'),
(220, 'MTH202', 'PHY309', 1, 300, 'PHY', 'CHM', 'CHM'),
(221, 'MTH204', 'CHM303', 1, 300, 'CHM', 'CHM', 'CHM'),
(222, 'BIO201', 'BI0303', 1, 300, 'BIO', 'CHM', 'CHM'),
(223, 'CHM201', 'CHM308', 2, 300, 'CHM', 'CHM', 'CHM'),
(224, 'CHM207', 'CHM302', 2, 300, 'CHM', 'CHM', 'CHM'),
(225, 'CSC201', 'BIO304', 2, 300, 'BIO', 'CHM', 'CHM'),
(226, 'ENG201', 'CHM306', 2, 300, 'CHM', 'CHM', 'CHM'),
(227, 'GST201', 'PHY310', 2, 300, 'PHY', 'CHM', 'CHM'),
(228, 'MTH201', 'CHM310', 2, 300, 'CHM', 'CHM', 'CHM'),
(229, 'BIO202', 'CHM304', 2, 300, 'CHM', 'CHM', 'CHM'),
(230, 'CHM202', 'ENG301', 1, 300, 'CIE', 'CIE', 'CIE'),
(231, 'CHM208', 'ENG307', 1, 300, 'CHE', 'CIE', 'CIE'),
(232, 'CSC202', 'CIE301', 1, 300, 'CIE', 'CIE', 'CIE'),
(233, 'ENG202', 'CIE303', 1, 300, 'CIE', 'CIE', 'CIE'),
(234, 'GST202', 'CIE305', 1, 300, 'CIE', 'CIE', 'CIE'),
(235, 'MTH202', 'ENG303', 1, 300, 'MEE', 'CIE', 'CIE'),
(236, 'CIE201', 'CIE306', 2, 300, 'CIE', 'CIE', 'CIE'),
(237, 'CSC201', 'CIE304', 2, 300, 'CIE', 'CIE', 'CIE'),
(238, 'ENG201', 'ENG304', 2, 300, 'MEE', 'CIE', 'CIE'),
(239, 'ENG203', 'ENG308', 2, 300, 'CHE', 'CIE', 'CIE'),
(240, 'ENG207', 'CIE302', 2, 300, 'CIE', 'CIE', 'CIE'),
(241, 'GST201', 'ENG302', 2, 300, 'CIE', 'CIE', 'CIE'),
(242, 'MTH201', 'IMT309', 1, 300, 'IMT', 'IMT', 'IMT'),
(243, 'MTH203', 'MTH309', 1, 300, 'MTH', 'IMT', 'IMT'),
(244, 'CIE202', 'IMT301', 1, 300, 'IMT', 'IMT', 'IMT'),
(245, 'CSC202', 'IMT303', 1, 300, 'IMT', 'IMT', 'IMT'),
(246, 'ENG202', 'IMT305', 1, 300, 'IMT', 'IMT', 'IMT'),
(247, 'ENG204', 'IMT307', 1, 300, 'IMT', 'IMT', 'IMT'),
(248, 'ENG208', 'MTH305', 1, 300, 'MTH', 'IMT', 'IMT'),
(249, 'GST202', 'IMT310', 2, 300, 'IMT', 'IMT', 'IMT'),
(250, 'MTH202', 'MTH306', 2, 300, 'MTH', 'IMT', 'IMT'),
(251, 'MTH204', 'MTH310', 2, 300, 'MTH', 'IMT', 'IMT'),
(252, 'CSC201', 'IMT302', 2, 300, 'IMT', 'IMT', 'IMT'),
(253, 'ENG201', 'IMT304', 2, 300, 'IMT', 'IMT', 'IMT'),
(254, 'ENG203', 'IMT306', 2, 300, 'IMT', 'IMT', 'IMT'),
(255, 'GST201', 'IMT308', 2, 300, 'IMT', 'IMT', 'IMT'),
(256, 'IMT201', 'ENG301', 1, 300, 'CIE', 'MEE', 'MEE'),
(257, 'IMT203', 'ENG303', 1, 300, 'MEE', 'MEE', 'MEE'),
(258, 'MTH201', 'ENG307', 1, 300, 'CHE', 'MEE', 'MEE'),
(259, 'MTH211', 'MEE309', 1, 300, 'MEE', 'MEE', 'MEE'),
(260, 'CSC202', 'MEE301', 1, 300, 'MEE', 'MEE', 'MEE'),
(261, 'ENG202', 'MEE303', 1, 300, 'MEE', 'MEE', 'MEE'),
(262, 'ENG204', 'MEE305', 1, 300, 'MEE', 'MEE', 'MEE'),
(263, 'GST202', 'MEE304', 2, 300, 'MEE', 'MEE', 'MEE'),
(264, 'IMT202', 'MEE310', 2, 300, 'MEE', 'MEE', 'MEE'),
(265, 'MTH202', 'ENG302', 2, 300, 'CIE', 'MEE', 'MEE'),
(266, 'MTH212', 'ENG304', 2, 300, 'MEE', 'MEE', 'MEE'),
(267, 'CSC201', 'ENG308', 2, 300, 'CHE', 'MEE', 'MEE'),
(268, 'ENG201', 'MEE306', 2, 300, 'MEE', 'MEE', 'MEE'),
(269, 'ENG203', 'MEE302', 2, 300, 'MEE', 'MEE', 'MEE'),
(270, 'ENG207', 'MTH303', 1, 300, 'MTH', 'MTH', 'MTH'),
(271, 'GST201', 'MTH305', 1, 300, 'MTH', 'MTH', 'MTH'),
(272, 'MEE201', 'MTH301', 1, 300, 'MTH', 'MTH', 'MTH'),
(273, 'MTH201', 'PHY303', 1, 300, 'PHY', 'MTH', 'MTH'),
(274, 'MTH203', 'PHY305', 1, 300, 'PHY', 'MTH', 'MTH'),
(275, 'CSC202', 'MTH309', 1, 300, 'MTH', 'MTH', 'MTH'),
(276, 'ENG202', 'MTH307', 1, 300, 'MTH', 'MTH', 'MTH'),
(277, 'ENG204', 'MTH302', 2, 300, 'MTH', 'MTH', 'MTH'),
(278, 'ENG208', 'PHY306', 2, 300, 'PHY', 'MTH', 'MTH'),
(279, 'GST202', 'PHY304', 2, 300, 'PHY', 'MTH', 'MTH'),
(280, 'MEE202', 'MTH310', 2, 300, 'MTH', 'MTH', 'MTH'),
(281, 'MTH202', 'MTH308', 2, 300, 'MTH', 'MTH', 'MTH'),
(282, 'MTH204', 'MTH306', 2, 300, 'MTH', 'MTH', 'MTH'),
(283, 'CSC201', 'MTH304', 2, 300, 'MTH', 'MTH', 'MTH'),
(284, 'ENG201', 'PHY305', 1, 300, 'PHY', 'PHY', 'PHY'),
(285, 'GST201', 'PHY309', 1, 300, 'PHY', 'PHY', 'PHY'),
(286, 'MTH201', 'PHY303', 1, 300, 'PHY', 'PHY', 'PHY'),
(287, 'MTH203', 'MTH303', 1, 300, 'MTH', 'PHY', 'PHY'),
(288, 'PHY201', 'PHY301', 1, 300, 'PHY', 'PHY', 'PHY'),
(289, 'PHY203', 'PHY311', 1, 300, 'PHY', 'PHY', 'PHY'),
(290, 'PHY207', 'PHY307', 1, 300, 'PHY', 'PHY', 'PHY'),
(291, 'CSC202', 'PHY302', 2, 300, 'PHY', 'PHY', 'PHY'),
(292, 'ENG202', 'MTH304', 2, 300, 'MTH', 'PHY', 'PHY'),
(293, 'GST202', 'PHY304', 2, 300, 'PHY', 'PHY', 'PHY'),
(294, 'MTH202', 'PHY306', 2, 300, 'PHY', 'PHY', 'PHY'),
(295, 'MTH204', 'PHY310', 2, 300, 'PHY', 'PHY', 'PHY'),
(296, 'PHY202', 'PHY308', 2, 300, 'PHY', 'PHY', 'PHY'),
(297, 'PHY204', 'PHY312', 2, 300, 'PHY', 'PHY', 'PHY'),
(298, 'PHY208', 'PMT307', 1, 300, 'PMT', 'PMT', 'PMT'),
(299, 'CSC201', 'PMT301', 1, 300, 'PMT', 'PMT', 'PMT'),
(300, 'ENG201', 'ENG307', 1, 300, 'CHE', 'PMT', 'PMT'),
(301, 'ENG203', 'ENG303', 1, 300, 'MEE', 'PMT', 'PMT'),
(302, 'MTH201', 'ENG301', 1, 300, 'CIE', 'PMT', 'PMT'),
(303, 'MTH211', 'PMT303', 1, 300, 'PMT', 'PMT', 'PMT'),
(304, 'PMT201', 'PMT305', 1, 300, 'PMT', 'PMT', 'PMT'),
(305, 'PMT203', 'PMT308', 2, 300, 'PMT', 'PMT', 'PMT'),
(306, 'CSC202', 'PMT306', 2, 300, 'PMT', 'PMT', 'PMT'),
(307, 'ENG202', 'PMT304', 2, 300, 'PMT', 'PMT', 'PMT'),
(308, 'ENG204', 'ENG308', 2, 300, 'CHE', 'PMT', 'PMT'),
(309, 'GST202', 'ENG304', 2, 300, 'MEE', 'PMT', 'PMT'),
(310, 'MTH202', 'ENG302', 2, 300, 'CIE', 'PMT', 'PMT'),
(311, 'PMT202', 'PMT302', 2, 300, 'PMT', 'PMT', 'PMT'),
(432, 'BIO101', 'GST201', 1, 200, 'BIO', 'BIO', 'BIO'),
(433, 'CHM101', 'CHM207', 1, 200, 'CHM', 'BIO', 'BIO'),
(434, 'ENG101', 'BIO203', 1, 200, 'MEE', 'BIO', 'BIO'),
(435, 'ENG103', 'BIO201', 1, 200, 'MEE', 'BIO', 'BIO'),
(436, 'GST101', 'CHM201', 1, 200, 'GST', 'BIO', 'BIO'),
(437, 'GST103', 'MTH201', 1, 200, 'GST', 'BIO', 'BIO'),
(438, 'MTH101', 'CSC201', 1, 200, 'MTH', 'BIO', 'BIO'),
(439, 'PHY101', 'ENG201', 1, 200, 'PHY', 'BIO', 'BIO'),
(440, 'BIO102', 'CHM202', 2, 200, 'BIO', 'BIO', 'BIO'),
(441, 'CHM102', 'BIO202', 2, 200, 'CHM', 'BIO', 'BIO'),
(442, 'ENG102', 'CHM208', 2, 200, 'MEE', 'BIO', 'BIO'),
(443, 'ENG104', 'MTH202', 2, 200, 'MEE', 'BIO', 'BIO'),
(444, 'GST102', 'CSC202', 2, 200, 'GST', 'BIO', 'BIO'),
(445, 'GST104', 'ENG202', 2, 200, 'GST', 'BIO', 'BIO'),
(446, 'MTH102', 'GST202', 2, 200, 'MTH', 'BIO', 'BIO'),
(447, 'PHY102', 'BIO204', 2, 200, 'PHY', 'BIO', 'BIO'),
(448, 'BIO101', 'CSC201', 1, 200, 'BIO', 'CHE', 'CHE'),
(449, 'CHM101', 'CHE201', 1, 200, 'CHM', 'CHE', 'CHE'),
(450, 'ENG101', 'ENG203', 1, 200, 'MEE', 'CHE', 'CHE'),
(451, 'ENG103', 'MTH203', 1, 200, 'MEE', 'CHE', 'CHE'),
(452, 'GST101', 'MTH201', 1, 200, 'GST', 'CHE', 'CHE'),
(453, 'GST103', 'GST201', 1, 200, 'GST', 'CHE', 'CHE'),
(454, 'MTH101', 'ENG207', 1, 200, 'MTH', 'CHE', 'CHE'),
(455, 'PHY101', 'ENG201', 1, 200, 'PHY', 'CHE', 'CHE'),
(456, 'BIO102', 'ENG204', 2, 200, 'BIO', 'CHE', 'CHE'),
(457, 'CHM102', 'MTH204', 2, 200, 'CHM', 'CHE', 'CHE'),
(458, 'ENG102', 'CSC202', 2, 200, 'MEE', 'CHE', 'CHE'),
(459, 'ENG104', 'ENG202', 2, 200, 'MEE', 'CHE', 'CHE'),
(460, 'GST102', 'MTH202', 2, 200, 'GST', 'CHE', 'CHE'),
(461, 'GST104', 'CHE202', 2, 200, 'GST', 'CHE', 'CHE'),
(462, 'MTH102', 'ENG208', 2, 200, 'MTH', 'CHE', 'CHE'),
(463, 'PHY102', 'GST202', 2, 200, 'PHY', 'CHE', 'CHE'),
(464, 'BIO101', 'CHM201', 1, 200, 'BIO', 'CHM', 'CHM'),
(465, 'CHM101', 'GST201', 1, 200, 'CHM', 'CHM', 'CHM'),
(466, 'ENG101', 'CHM207', 1, 200, 'MEE', 'CHM', 'CHM'),
(467, 'ENG103', 'CSC201', 1, 200, 'MEE', 'CHM', 'CHM'),
(468, 'GST101', 'MTH201', 1, 200, 'GST', 'CHM', 'CHM'),
(469, 'GST103', 'ENG201', 1, 200, 'GST', 'CHM', 'CHM'),
(470, 'MTH101', 'CHM203', 1, 200, 'MTH', 'CHM', 'CHM'),
(471, 'PHY101', 'BIO201', 1, 200, 'PHY', 'CHM', 'CHM'),
(472, 'BIO102', 'CHM204', 2, 200, 'BIO', 'CHM', 'CHM'),
(473, 'CHM102', 'GST202', 2, 200, 'CHM', 'CHM', 'CHM'),
(474, 'ENG102', 'BIO202', 2, 200, 'MEE', 'CHM', 'CHM'),
(475, 'ENG104', 'CHM202', 2, 200, 'MEE', 'CHM', 'CHM'),
(476, 'GST102', 'ENG202', 2, 200, 'GST', 'CHM', 'CHM'),
(477, 'GST104', 'CSC202', 2, 200, 'GST', 'CHM', 'CHM'),
(478, 'MTH102', 'MTH202', 2, 200, 'MTH', 'CHM', 'CHM'),
(479, 'PHY102', 'CHM208', 2, 200, 'PHY', 'CHM', 'CHM'),
(480, 'BIO101', 'CIE201', 1, 200, 'BIO', 'CIE', 'CIE'),
(481, 'CHM101', 'ENG207', 1, 200, 'CHM', 'CIE', 'CIE'),
(482, 'ENG101', 'MTH203', 1, 200, 'MEE', 'CIE', 'CIE'),
(483, 'ENG103', 'ENG203', 1, 200, 'MEE', 'CIE', 'CIE'),
(484, 'GST101', 'GST201', 1, 200, 'GST', 'CIE', 'CIE'),
(485, 'GST103', 'ENG201', 1, 200, 'GST', 'CIE', 'CIE'),
(486, 'MTH101', 'CSC201', 1, 200, 'MTH', 'CIE', 'CIE'),
(487, 'PHY101', 'MTH201', 1, 200, 'PHY', 'CIE', 'CIE'),
(488, 'BIO102', 'MTH202', 2, 200, 'BIO', 'CIE', 'CIE'),
(489, 'CHM102', 'CSC202', 2, 200, 'CHM', 'CIE', 'CIE'),
(490, 'ENG102', 'CIE202', 2, 200, 'MEE', 'CIE', 'CIE'),
(491, 'ENG104', 'ENG202', 2, 200, 'MEE', 'CIE', 'CIE'),
(492, 'GST102', 'GST202', 2, 200, 'GST', 'CIE', 'CIE'),
(493, 'GST104', 'MTH204', 2, 200, 'GST', 'CIE', 'CIE'),
(494, 'MTH102', 'ENG204', 2, 200, 'MTH', 'CIE', 'CIE'),
(495, 'PHY102', 'ENG208', 2, 200, 'PHY', 'CIE', 'CIE'),
(496, 'BIO101', 'IMT203', 1, 200, 'BIO', 'IMT', 'IMT'),
(497, 'CHM101', 'IMT201', 1, 200, 'CHM', 'IMT', 'IMT'),
(498, 'ENG101', 'ENG201', 1, 200, 'MEE', 'IMT', 'IMT'),
(499, 'ENG103', 'MTH211', 1, 200, 'MEE', 'IMT', 'IMT'),
(500, 'GST101', 'CSC201', 1, 200, 'GST', 'IMT', 'IMT'),
(501, 'GST103', 'MTH201', 1, 200, 'GST', 'IMT', 'IMT'),
(502, 'MTH101', 'GST201', 1, 200, 'MTH', 'IMT', 'IMT'),
(503, 'PHY101', 'ENG203', 1, 200, 'PHY', 'IMT', 'IMT'),
(504, 'BIO102', 'IMT202', 2, 200, 'BIO', 'IMT', 'IMT'),
(505, 'CHM102', 'MTH212', 2, 200, 'CHM', 'IMT', 'IMT'),
(506, 'ENG102', 'MTH202', 2, 200, 'MEE', 'IMT', 'IMT'),
(507, 'ENG104', 'ENG204', 2, 200, 'MEE', 'IMT', 'IMT'),
(508, 'GST102', 'CSC202', 2, 200, 'GST', 'IMT', 'IMT'),
(509, 'GST104', 'ENG202', 2, 200, 'GST', 'IMT', 'IMT'),
(510, 'MTH102', 'GST202', 2, 200, 'MTH', 'IMT', 'IMT'),
(511, 'PHY102', 'IMT204', 2, 200, 'PHY', 'IMT', 'IMT'),
(512, 'BIO101', 'GST201', 1, 200, 'BIO', 'MEE', 'MEE'),
(513, 'CHM101', 'ENG207', 1, 200, 'CHM', 'MEE', 'MEE'),
(514, 'ENG101', 'MEE201', 1, 200, 'MEE', 'MEE', 'MEE'),
(515, 'ENG103', 'MTH203', 1, 200, 'MEE', 'MEE', 'MEE'),
(516, 'GST101', 'MTH201', 1, 200, 'GST', 'MEE', 'MEE'),
(517, 'GST103', 'ENG203', 1, 200, 'GST', 'MEE', 'MEE'),
(518, 'MTH101', 'ENG201', 1, 200, 'MTH', 'MEE', 'MEE'),
(519, 'PHY101', 'CSC201', 1, 200, 'PHY', 'MEE', 'MEE'),
(520, 'BIO102', 'ENG204', 2, 200, 'BIO', 'MEE', 'MEE'),
(521, 'CHM102', 'GST202', 2, 200, 'CHM', 'MEE', 'MEE'),
(522, 'ENG102', 'CSC202', 2, 200, 'MEE', 'MEE', 'MEE'),
(523, 'ENG104', 'ENG202', 2, 200, 'MEE', 'MEE', 'MEE'),
(524, 'GST102', 'MTH204', 2, 200, 'GST', 'MEE', 'MEE'),
(525, 'GST104', 'MTH202', 2, 200, 'GST', 'MEE', 'MEE'),
(526, 'MTH102', 'MEE202', 2, 200, 'MTH', 'MEE', 'MEE'),
(527, 'PHY102', 'ENG208', 2, 200, 'PHY', 'MEE', 'MEE'),
(528, 'BIO101', 'MTH211', 1, 200, 'BIO', 'MTH', 'MTH'),
(529, 'CHM101', 'PHY203', 1, 200, 'CHM', 'MTH', 'MTH'),
(530, 'ENG101', 'ENG201', 1, 200, 'MEE', 'MTH', 'MTH'),
(531, 'ENG103', 'MTH203', 1, 200, 'MEE', 'MTH', 'MTH'),
(532, 'GST101', 'PHY207', 1, 200, 'GST', 'MTH', 'MTH'),
(533, 'GST103', 'GST201', 1, 200, 'GST', 'MTH', 'MTH'),
(534, 'MTH101', 'MTH201', 1, 200, 'MTH', 'MTH', 'MTH'),
(535, 'PHY101', 'CSC201', 1, 200, 'PHY', 'MTH', 'MTH'),
(536, 'BIO102', 'GST202', 2, 200, 'BIO', 'MTH', 'MTH'),
(537, 'CHM102', 'ENG202', 2, 200, 'CHM', 'MTH', 'MTH'),
(538, 'ENG102', 'MTH202', 2, 200, 'MEE', 'MTH', 'MTH'),
(539, 'ENG104', 'MTH204', 2, 200, 'MEE', 'MTH', 'MTH'),
(540, 'GST102', 'PHY208', 2, 200, 'GST', 'MTH', 'MTH'),
(541, 'GST104', 'CSC202', 2, 200, 'GST', 'MTH', 'MTH'),
(542, 'MTH102', 'PHY204', 2, 200, 'MTH', 'MTH', 'MTH'),
(543, 'PHY102', 'MTH212', 2, 200, 'PHY', 'MTH', 'MTH'),
(544, 'BIO101', 'ENG201', 1, 200, 'BIO', 'PHY', 'PHY'),
(545, 'CHM101', 'GST201', 1, 200, 'CHM', 'PHY', 'PHY'),
(546, 'ENG101', 'PHY207', 1, 200, 'MEE', 'PHY', 'PHY'),
(547, 'ENG103', 'CSC201', 1, 200, 'MEE', 'PHY', 'PHY'),
(548, 'GST101', 'MTH201', 1, 200, 'GST', 'PHY', 'PHY'),
(549, 'GST103', 'PHY201', 1, 200, 'GST', 'PHY', 'PHY'),
(550, 'MTH101', 'MTH203', 1, 200, 'MTH', 'PHY', 'PHY'),
(551, 'PHY101', 'PHY203', 1, 200, 'PHY', 'PHY', 'PHY'),
(552, 'BIO102', 'PHY202', 2, 200, 'BIO', 'PHY', 'PHY'),
(553, 'CHM102', 'MTH204', 2, 200, 'CHM', 'PHY', 'PHY'),
(554, 'ENG102', 'PHY204', 2, 200, 'MEE', 'PHY', 'PHY'),
(555, 'ENG104', 'CSC202', 2, 200, 'MEE', 'PHY', 'PHY'),
(556, 'GST102', 'GST202', 2, 200, 'GST', 'PHY', 'PHY'),
(557, 'GST104', 'MTH202', 2, 200, 'GST', 'PHY', 'PHY'),
(558, 'MTH102', 'PHY208', 2, 200, 'MTH', 'PHY', 'PHY'),
(559, 'PHY102', 'ENG202', 2, 200, 'PHY', 'PHY', 'PHY'),
(560, 'BIO101', 'PMT201', 1, 200, 'BIO', 'PMT', 'PMT'),
(561, 'CHM101', 'MTH201', 1, 200, 'CHM', 'PMT', 'PMT'),
(562, 'ENG101', 'CSC201', 1, 200, 'MEE', 'PMT', 'PMT'),
(563, 'ENG103', 'ENG201', 1, 200, 'MEE', 'PMT', 'PMT'),
(564, 'GST103', 'PMT203', 1, 200, 'GST', 'PMT', 'PMT'),
(565, 'MTH101', 'GST201', 1, 200, 'MTH', 'PMT', 'PMT'),
(566, 'PHY101', 'ENG203', 1, 200, 'PHY', 'PMT', 'PMT'),
(567, 'BIO102', 'MTH211', 2, 200, 'BIO', 'PMT', 'PMT'),
(568, 'CHM102', 'PMT204', 2, 200, 'CHM', 'PMT', 'PMT'),
(569, 'ENG102', 'PMT202', 2, 200, 'MEE', 'PMT', 'PMT'),
(570, 'ENG104', 'GST202', 2, 200, 'MEE', 'PMT', 'PMT'),
(571, 'GST102', 'ENG202', 2, 200, 'GST', 'PMT', 'PMT'),
(572, 'GST104', 'CSC202', 2, 200, 'GST', 'PMT', 'PMT'),
(573, 'MTH102', 'MTH202', 2, 200, 'MTH', 'PMT', 'PMT');

-- --------------------------------------------------------

--
-- Table structure for table `test_students`
--

CREATE TABLE `test_students` (
  `id` int(11) NOT NULL,
  `reg_no` varchar(191) DEFAULT NULL,
  `dept` varchar(191) DEFAULT NULL,
  `student_reg_no` varchar(191) DEFAULT NULL,
  `student_dept` varchar(191) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `test_students`
--

INSERT INTO `test_students` (`id`, `reg_no`, `dept`, `student_reg_no`, `student_dept`) VALUES
(1, '20171020200', 'BIO', '20161020050', 'BIO'),
(2, '20171020260', 'BIO', '20161020065', 'BIO'),
(3, '20171020215', 'BIO', '20161020125', 'BIO'),
(4, '20171020185', 'BIO', '20161020170', 'BIO'),
(5, '20171020170', 'BIO', '20161020185', 'BIO'),
(6, '20171020125', 'BIO', '20161020200', 'BIO'),
(7, '20171020065', 'BIO', '20161020215', 'BIO'),
(8, '20171020050', 'BIO', '20161020260', 'BIO'),
(9, '20173045065', 'CHE', '20163045050', 'CHE'),
(10, '20173045095', 'CHE', '20163045065', 'CHE'),
(11, '20173045125', 'CHE', '20163045095', 'CHE'),
(12, '20173045200', 'CHE', '20163045125', 'CHE'),
(13, '20173045230', 'CHE', '20163045200', 'CHE'),
(14, '20173045050', 'CHE', '20163045230', 'CHE'),
(15, '20171015080', 'CHM', '20161015020', 'CHM'),
(16, '20171015110', 'CHM', '20161015080', 'CHM'),
(17, '20171015020', 'CHM', '20161015110', 'CHM'),
(18, '20171015140', 'CHM', '20161015140', 'CHM'),
(19, '20171015245', 'CHM', '20161015245', 'CHM'),
(20, '20173050140', 'CIE', '20163050035', 'CIE'),
(21, '20173050170', 'CIE', '20163050080', 'CIE'),
(22, '20173050035', 'CIE', '20163050140', 'CIE'),
(23, '20173050080', 'CIE', '20163050170', 'CIE'),
(24, '20173050215', 'CIE', '20163050215', 'CIE'),
(25, '20172035095', 'IMT', '20162035020', 'IMT'),
(26, '20172035080', 'IMT', '20162035050', 'IMT'),
(27, '20172035020', 'IMT', '20162035080', 'IMT'),
(28, '20172035050', 'IMT', '20162035095', 'IMT'),
(29, '20173055185', 'MEE', '20163055005', 'MEE'),
(30, '20173055155', 'MEE', '20163055020', 'MEE'),
(31, '20173055110', 'MEE', '20163055110', 'MEE'),
(32, '20173055020', 'MEE', '20163055155', 'MEE'),
(33, '20173055005', 'MEE', '20163055185', 'MEE'),
(34, '20171030035', 'MTH', '20161030005', 'MTH'),
(35, '20171030005', 'MTH', '20161030035', 'MTH'),
(36, '20171030095', 'MTH', '20161030095', 'MTH'),
(37, '20171030230', 'MTH', '20161030230', 'MTH'),
(38, '20171025155', 'PHY', '20161025155', 'PHY'),
(39, '20171025275', 'PHY', '20161025275', 'PHY'),
(40, '20171025290', 'PHY', '20161025290', 'PHY'),
(41, '20172040005', 'PMT', '20162040005', 'PMT'),
(42, '20172040035', 'PMT', '20162040035', 'PMT'),
(43, '20172040065', 'PMT', '20162040065', 'PMT'),
(44, '20172040110', 'PMT', '20162040110', 'PMT'),
(45, '20172040125', 'PMT', '20162040125', 'PMT');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `mums_accounts_undergraduate_transactions`
--
ALTER TABLE `mums_accounts_undergraduate_transactions`
  ADD PRIMARY KEY (`transaction_id`);

--
-- Indexes for table `mums_admin_access`
--
ALTER TABLE `mums_admin_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_categories`
--
ALTER TABLE `mums_categories`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `categories_slug_unique` (`slug`),
  ADD KEY `categories_parent_id_foreign` (`parent_id`);

--
-- Indexes for table `mums_chatter_categories`
--
ALTER TABLE `mums_chatter_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_chatter_discussion`
--
ALTER TABLE `mums_chatter_discussion`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `chatter_discussion_slug_unique` (`slug`),
  ADD KEY `chatter_discussion_chatter_category_id_foreign` (`chatter_category_id`),
  ADD KEY `chatter_discussion_user_id_foreign` (`user_id`);

--
-- Indexes for table `mums_chatter_post`
--
ALTER TABLE `mums_chatter_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `chatter_post_chatter_discussion_id_foreign` (`chatter_discussion_id`),
  ADD KEY `chatter_post_user_id_foreign` (`user_id`);

--
-- Indexes for table `mums_chatter_user_discussion`
--
ALTER TABLE `mums_chatter_user_discussion`
  ADD PRIMARY KEY (`user_id`,`discussion_id`),
  ADD KEY `chatter_user_discussion_user_id_index` (`user_id`),
  ADD KEY `chatter_user_discussion_discussion_id_index` (`discussion_id`);

--
-- Indexes for table `mums_data_rows`
--
ALTER TABLE `mums_data_rows`
  ADD PRIMARY KEY (`id`),
  ADD KEY `data_rows_data_type_id_foreign` (`data_type_id`);

--
-- Indexes for table `mums_data_types`
--
ALTER TABLE `mums_data_types`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `data_types_name_unique` (`name`),
  ADD UNIQUE KEY `data_types_slug_unique` (`slug`);

--
-- Indexes for table `mums_guardian_student`
--
ALTER TABLE `mums_guardian_student`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_invoices`
--
ALTER TABLE `mums_invoices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_invoice_lines`
--
ALTER TABLE `mums_invoice_lines`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_menus`
--
ALTER TABLE `mums_menus`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `menus_name_unique` (`name`);

--
-- Indexes for table `mums_menu_items`
--
ALTER TABLE `mums_menu_items`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_items_menu_id_foreign` (`menu_id`);

--
-- Indexes for table `mums_message_network`
--
ALTER TABLE `mums_message_network`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_migrations`
--
ALTER TABLE `mums_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_notices`
--
ALTER TABLE `mums_notices`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_pages`
--
ALTER TABLE `mums_pages`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `pages_slug_unique` (`slug`);

--
-- Indexes for table `mums_password_resets`
--
ALTER TABLE `mums_password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `mums_permissions`
--
ALTER TABLE `mums_permissions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `permissions_key_index` (`key`);

--
-- Indexes for table `mums_permission_role`
--
ALTER TABLE `mums_permission_role`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `permission_role_permission_id_index` (`permission_id`),
  ADD KEY `permission_role_role_id_index` (`role_id`);

--
-- Indexes for table `mums_posts`
--
ALTER TABLE `mums_posts`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `posts_slug_unique` (`slug`);

--
-- Indexes for table `mums_roles`
--
ALTER TABLE `mums_roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_name_unique` (`name`);

--
-- Indexes for table `mums_school_calendar`
--
ALTER TABLE `mums_school_calendar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_sessions`
--
ALTER TABLE `mums_sessions`
  ADD UNIQUE KEY `sessions_id_unique` (`id`);

--
-- Indexes for table `mums_settings`
--
ALTER TABLE `mums_settings`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `settings_key_unique` (`key`);

--
-- Indexes for table `mums_time_table`
--
ALTER TABLE `mums_time_table`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_translations`
--
ALTER TABLE `mums_translations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `translations_table_name_column_name_foreign_key_locale_unique` (`table_name`,`column_name`,`foreign_key`,`locale`);

--
-- Indexes for table `mums_undergraduate_attendance`
--
ALTER TABLE `mums_undergraduate_attendance`
  ADD PRIMARY KEY (`attendance_id`);

--
-- Indexes for table `mums_users`
--
ALTER TABLE `mums_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email_UNIQUE` (`email`),
  ADD UNIQUE KEY `name_UNIQUE` (`name`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- Indexes for table `mums_users_guardian`
--
ALTER TABLE `mums_users_guardian`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mums_users_staffs`
--
ALTER TABLE `mums_users_staffs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mums_users_staffs_staff_id_key` (`staff_id`),
  ADD UNIQUE KEY `mums_users_staffs_staff_id_key1` (`staff_id`),
  ADD UNIQUE KEY `mums_users_staffs_staff_id_key2` (`staff_id`);

--
-- Indexes for table `mums_users_undergraduates`
--
ALTER TABLE `mums_users_undergraduates`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `mums_users_undergraduates_reg_no_key` (`reg_no`),
  ADD UNIQUE KEY `mums_users_undergraduates_reg_no_key1` (`reg_no`),
  ADD UNIQUE KEY `mums_users_undergraduates_reg_no_key2` (`reg_no`);

--
-- Indexes for table `mums_user_roles`
--
ALTER TABLE `mums_user_roles`
  ADD PRIMARY KEY (`user_id`,`role_id`),
  ADD KEY `user_roles_user_id_index` (`user_id`),
  ADD KEY `user_roles_role_id_index` (`role_id`);

--
-- Indexes for table `test_course`
--
ALTER TABLE `test_course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `test_students`
--
ALTER TABLE `test_students`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `mums_categories`
--
ALTER TABLE `mums_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `mums_chatter_categories`
--
ALTER TABLE `mums_chatter_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `mums_chatter_discussion`
--
ALTER TABLE `mums_chatter_discussion`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `mums_chatter_post`
--
ALTER TABLE `mums_chatter_post`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `mums_data_rows`
--
ALTER TABLE `mums_data_rows`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `mums_data_types`
--
ALTER TABLE `mums_data_types`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `mums_invoices`
--
ALTER TABLE `mums_invoices`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=229;

--
-- AUTO_INCREMENT for table `mums_invoice_lines`
--
ALTER TABLE `mums_invoice_lines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=667;

--
-- AUTO_INCREMENT for table `mums_menus`
--
ALTER TABLE `mums_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mums_menu_items`
--
ALTER TABLE `mums_menu_items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- AUTO_INCREMENT for table `mums_message_network`
--
ALTER TABLE `mums_message_network`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `mums_migrations`
--
ALTER TABLE `mums_migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=54;

--
-- AUTO_INCREMENT for table `mums_pages`
--
ALTER TABLE `mums_pages`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `mums_permissions`
--
ALTER TABLE `mums_permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=97;

--
-- AUTO_INCREMENT for table `mums_posts`
--
ALTER TABLE `mums_posts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `mums_roles`
--
ALTER TABLE `mums_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `mums_settings`
--
ALTER TABLE `mums_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `mums_translations`
--
ALTER TABLE `mums_translations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `mums_users`
--
ALTER TABLE `mums_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=459;

--
-- AUTO_INCREMENT for table `test_course`
--
ALTER TABLE `test_course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=574;

--
-- AUTO_INCREMENT for table `test_students`
--
ALTER TABLE `test_students`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=46;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `mums_categories`
--
ALTER TABLE `mums_categories`
  ADD CONSTRAINT `categories_parent_id_foreign` FOREIGN KEY (`parent_id`) REFERENCES `mums_categories` (`id`) ON DELETE SET NULL ON UPDATE CASCADE;

--
-- Constraints for table `mums_chatter_discussion`
--
ALTER TABLE `mums_chatter_discussion`
  ADD CONSTRAINT `chatter_discussion_chatter_category_id_foreign` FOREIGN KEY (`chatter_category_id`) REFERENCES `mums_chatter_categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatter_discussion_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `mums_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mums_chatter_post`
--
ALTER TABLE `mums_chatter_post`
  ADD CONSTRAINT `chatter_post_chatter_discussion_id_foreign` FOREIGN KEY (`chatter_discussion_id`) REFERENCES `mums_chatter_discussion` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `chatter_post_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `mums_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mums_chatter_user_discussion`
--
ALTER TABLE `mums_chatter_user_discussion`
  ADD CONSTRAINT `chatter_user_discussion_discussion_id_foreign` FOREIGN KEY (`discussion_id`) REFERENCES `mums_chatter_discussion` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `chatter_user_discussion_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `mums_users` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mums_data_rows`
--
ALTER TABLE `mums_data_rows`
  ADD CONSTRAINT `data_rows_data_type_id_foreign` FOREIGN KEY (`data_type_id`) REFERENCES `mums_data_types` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `mums_menu_items`
--
ALTER TABLE `mums_menu_items`
  ADD CONSTRAINT `menu_items_menu_id_foreign` FOREIGN KEY (`menu_id`) REFERENCES `mums_menus` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mums_permission_role`
--
ALTER TABLE `mums_permission_role`
  ADD CONSTRAINT `permission_role_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `mums_permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `permission_role_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `mums_roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `mums_users`
--
ALTER TABLE `mums_users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `mums_roles` (`id`);

--
-- Constraints for table `mums_user_roles`
--
ALTER TABLE `mums_user_roles`
  ADD CONSTRAINT `user_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `mums_roles` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `user_roles_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `mums_users` (`id`) ON DELETE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
