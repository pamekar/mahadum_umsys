<?php

namespace App\Services\student\undergraduate;

use App\Notice;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NoticeService
{

	/*
	 * drg>>This serves the home pages based on the tpe of user
	 *
	 */
	public function __construct ( )
	{
		//
	}
	// drg>> These are major functions (i.e. functions that are used outside the php class)
	//
	// drg>> these are minor functions (i.e. functions that are used inside the php class)
	function getNotices ( $limit = 10, $page = 1 )
	{
		$results_per_page = $limit;
		$start_from = ( $page - 1 ) * $results_per_page;
		$showing_from = $start_from + 1;
		$showing_to = $start_from + $results_per_page;
		// drg>> we get the category the user belongs in
		$type = Auth::user ( )->user_type;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		// drg>> we then set the various system_ids for the notices
		$general_sid = "mums";
		$type_sid = $general_sid . "_" . $type;
		$school_sid = $type_sid . "_" . $school;
		$dept_sid = $school_sid . "_" . $department;
		// drg>> we then query the notice database table
		$notice = Notice::join ( 'admin_access',
						function ( $join )
						{
							$join->on ( 'notices.system_id', '=', 'admin_access.system_id' )
								-> on ( 'notices.announcer', '=', 'admin_access.admin_function' );
						} )->where ( 'notices.system_id', $general_sid )
			-> orWhere ( 'notices.system_id', $type_sid )
			-> orWhere ( 'notices.system_id', $school_sid )
			-> orWhere ( 'notices.system_id', $dept_sid )
			-> offset ( $start_from )
			-> limit ( $results_per_page )
			-> latest ( )
			-> select ( DB::raw ( 'notices.*, admin_access.admin_title' ) )
			-> get ( );

		return $notice;

	}

	function getNotice ( $id )
	{
		// drg>> we get the category the user belongs in
		$type = Auth::user ( )->user_type;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		// drg>> we then set the various system_ids for the notices
		$general_sid = "mums";
		$type_sid = $general_sid . "_" . $type;
		$school_sid = $type_sid . "_" . $school;
		$dept_sid = $school_sid . "_" . $department;
		// drg>> we then query the notice database table
		$notice = Notice::join ( 'admin_access',
						function ( $join )
						{
							$join->on ( 'notices.system_id', '=', 'admin_access.system_id' )
								-> on ( 'notices.announcer', '=', 'admin_access.admin_function' );
						} )->where ( 'notices.id', '=', $id )
			-> where (
						function ( $query ) use ( $general_sid, $type_sid, $school_sid, $dept_sid )
						{
							$query->where ( 'notices.system_id', $general_sid )
								-> orWhere ( 'notices.system_id', $type_sid )
								-> orWhere ( 'notices.system_id', $school_sid )
								-> orWhere ( 'notices.system_id', $dept_sid );
						} )
			-> first ( );
		return $notice;

	}

	function countNotices ( )
	{
		$type = Auth::user ( )->user_type;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		// drg>> we then set the various system_ids for the notices
		$general_sid = "mums";
		$type_sid = $general_sid . "_" . $type;
		$school_sid = $type_sid . "_" . $school;
		$dept_sid = $school_sid . "_" . $department;
		// drg>> we then query the notice database table
		$notice = Notice::where ( 'notices.system_id', $general_sid )->orWhere ( 'notices.system_id', $type_sid )
			-> orWhere ( 'notices.system_id', $school_sid )
			-> orWhere ( 'notices.system_id', $dept_sid )
			-> count ( );
		return $notice;

	}
	// drg>> These functions will service JSON Requests
}