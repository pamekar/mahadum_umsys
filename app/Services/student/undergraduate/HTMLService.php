<?php

namespace App\Services\student\undergraduate;

use App\Services\student\undergraduate\UndergraduateDataService;
use App\Services\HTMLWriter;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use View;
use Hamcrest\Type\IsNumeric;

class HTMLService
{

	public function __construct ( UndergraduateDataService $user, HTMLWriter $html )
	{
		$this->userData = $user;
		$this->htmlWriter = $html;
		//
	}

	public function downloadResults ( $session = null )
	{
		$optionItems = "";
		$details = "";
		$col = "";
		if (  ! isset ( $session ) )
		{
			$session = $this->userData->getLastSession ( );
			$session = date ( 'Y', strtotime ( $session->start ) );
		}
		$heading = "$session/" . ( $session + 1 ) . " Academic Session Result";
		$heading = $this->htmlWriter->h ( $heading, 2, 'width-full text-center' );
		// drg >> to get the user details
		$userDetails = $this->userData->getUsersMeta ( );
		$detailsArr = array (
							[
								'Name',
								$userDetails->first_name . ' ' . $userDetails->last_name ],
							[
								'Reg No.',
								$userDetails->reg_no ],
							[
								'School',
								$userDetails->faculty ],
							[
								'Department',
								$userDetails->dept ],
							[
								'Level',
								$this->userData->getLevel ( ) ],
							[
								'Cummulative GPA',
								$this->userData->getCommulativeGPA ( ) ],
							[
								'Previous Session GPA',
								$this->userData->getSessionGPA ( ) ],
							[
								'Outstanding Courses',
								$this->userData->getOutstandingCourses ( ) ] );
		foreach ( $detailsArr as $row )
		{
			foreach ( $row as $column )
			{
				if ( $column === reset ( $row ) )
					$col .= $this->htmlWriter->td ( $column, '', '', '', ' width = "30%"' );
				else
					$col .= $this->htmlWriter->td ( $column, '', '', '', '' );
			}
			$details .= $this->htmlWriter->tr ( $col );
			$col = '';
		}
		$column = $this->htmlWriter->td ( );
		// $details = $this->htmlWriter->table ( $details, 'media-body' );
		$photo = $this->htmlWriter->td (
						$this->htmlWriter->img ( $userDetails->photo_location, 'sample', '', '90', '100' ),
						'', '', '', ' rowspan = "9" align = "center" valign = "middle"' );
		$photo = $this->htmlWriter->tr ( $column . $column . $photo, '', '', '', '' );
		$details = $this->htmlWriter->table ( $photo . $details, '', '', '', '' );
		// drg >> the control tab
		$firstSemester = $this->getFirstSemesterResults ( $session, true ); // drg >> get first semester results
		$secondSemester = $this->getSecondSemesterResults ( $session, true ); // drg >> get second semester results
		$resultSummary = $this->getResultSummary ( $session, true );
		$results = $this->htmlWriter->div ( $firstSemester . $secondSemester . $resultSummary, '', 'area' );
		$html = $this->htmlWriter->div ( $heading . $details . $results, '' );
		return $html;

	}

	public function downloadCourseRegistration ( $session = null, $courses = array() )
	{
		$optionItems = "";
		$details = "";
		$col = "";
		if (  ! isset ( $session ) )
		{
			$session = $this->userData->getCurrentSession ( );
			$session = date ( 'Y', strtotime ( $session->start ) );
		}
		$title = "$session/" . ( $session + 1 ) . " Academic Session Registration";
		$title = $this->htmlWriter->h ( $title, 2, 'width-full text-center' );
		// drg >> to get the user details
		$userDetails = $this->userData->getUsersMeta ( );
		$semester = $this->userData->getCurrentSemester ( );
		if ( $semester == 1 )
			$semester = "Harmattan Semester";
		elseif ( $semester == 2 )
			$semester = "Rain Semester";
		$detailsArr = array (
							[
								'Name',
								$userDetails->first_name . ' ' . $userDetails->last_name ],
							[
								'Reg No.',
								$userDetails->reg_no ],
							[
								'School',
								$userDetails->faculty ],
							[
								'Department',
								$userDetails->dept ],
							[
								'Level',
								$this->userData->getLevel ( ) ],
							[
								'Session',
								$session ],
							[
								'Semester',
								$semester ] );
		foreach ( $detailsArr as $row )
		{
			foreach ( $row as $column )
			{
				if ( $column === reset ( $row ) )
					$col .= $this->htmlWriter->td ( $column, '', '', '', ' width = "30%"' );
				else
					$col .= $this->htmlWriter->td ( $column, '', '', '', '' );
			}
			$details .= $this->htmlWriter->tr ( $col );
			$col = '';
		}
		$column = $this->htmlWriter->td ( );
		// $details = $this->htmlWriter->table ( $details, 'media-body' );
		$photo = $this->htmlWriter->td (
						$this->htmlWriter->img ( $userDetails->photo_location, '', '', '', '', '90', '100' ),
						'', '', '', ' rowspan = "8" align = "center" valign = "middle"' );
		$photo = $this->htmlWriter->tr ( $column . $photo, '', '', '', '' );
		$details = $this->htmlWriter->table ( $photo . $details, '', '', '', '' );
		$details = $this->htmlWriter->div ( $details );
		$heading = $this->htmlWriter->h ( 'Registered Courses', 3, 'card-header blue width-full text-center' );
		$courses = $this->getCourseRegistration ( $courses );
		$courses = $this->htmlWriter->div ( $courses, '', 'area' );
		$html = $this->htmlWriter->div ( $title . $details . $heading . $courses, '' );
		return $html;

	}

	public function getAcademicSessions ( $id = null )
	{
		if (  ! isset ( $id ) )
			$id = Auth::user ( )->name;
		$sessions = DB::table ( 'undergraduate_results' )->where ( 'student_reg_no', $id )
			-> select ( 'session' )
			-> distinct ( )
			-> get ( );
		return $sessions;

	}

	public function getCourseRegistration ( $array )
	{
		$rows = "";
		$totalUnits = 0;
		$tableSetting = ' cellpadding = "6" width = "100%"';
		$column = "";
		$column .= $this->htmlWriter->th ( 'Code', '', '', '', 'width = "14%"' );
		$column .= $this->htmlWriter->th ( 'Course Title', '', '', '', 'width = "50%"' );
		$column .= $this->htmlWriter->th ( 'Units', '', '', '', 'width = "8%"' );
		$column .= $this->htmlWriter->th ( 'Type', '', '', '', 'width = "14%"' );
		// $column .= $this->htmlWriter->th ( 'Status', '', '', '', 'width = "14%"' );
		$tableHeader = $this->htmlWriter->tr ( $column, 'thead-default' );
		$tableHeader = $this->htmlWriter->thead ( $tableHeader, '', '', '', ' cellpadding = "6"' );
		$column = "";
		try {
            foreach ($array as $courses) {
                $column .= $this->htmlWriter->td($courses->course_code, '', '',
                    '', 'width = "14%"');
                $column .= $this->htmlWriter->td($courses->course_title, '', '',
                    '', 'width = "50%"');
                $column .= $this->htmlWriter->td($courses->units, '', '', '',
                    'width = "8%"');
                $column .= $this->htmlWriter->td($courses->course_type, '', '',
                    '', 'width = "14%"');
                // $column .= $this->htmlWriter->td ( $courses->remarks, '', '', '', 'width = "14%"' );
                $rows .= $this->htmlWriter->tr($column, '');
                $column = "";
                $totalUnits += $courses->units;
            }
        }
        catch(\Exception $e){
		    echo $e->getMessage().'<br>';
		    var_dump($array);
        }
		$column = $this->htmlWriter->td ( '', '', '', '', 'width = "14%"' );
		$column .= $this->htmlWriter->th ( 'Total Units', '', '', '', 'width = "50%"' );
		$totalUnits = $this->htmlWriter->th ( $totalUnits, '', '', '', 'width = "8%"' );
		$rows .= $this->htmlWriter->tr ( $column . $totalUnits, 'thead-default' );
		$course = $this->htmlWriter->table ( $tableHeader . $rows, 'table table-hover', '', '', $tableSetting );
		return $course;

	}

	private function getCurrentLevelCourses ( )
	{
		$level = $this->userData->getLevel ( );
		$reg_no = Auth::user ( )->name;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		$courses = DB::table ( 'undergraduate_courses' )->leftJoin ( 'admin_access',
						'undergraduate_courses.system_id', '=', 'admin_access.system_id' )
			-> leftJoin ( 'undergraduate_results',
						function ( $join ) use ( $reg_no )
						{
							$join->on ( 'undergraduate_courses.course_code', '=',
											'undergraduate_results.course_code' )
								-> where ( 'undergraduate_results.student_reg_no', '=', $reg_no );
						} )
			-> leftJoin ( 'users_staffs', 'users_staffs.staff_id', '=', 'admin_access.admin_id' )
			-> where (
						[
							[
								'undergraduate_courses.level',
								$level ],
							[
								'undergraduate_courses.course_destination_school',
								$school ],
							[
								'undergraduate_courses.course_destination_dept',
								$department ] ] )
			-> orderBy ( 'undergraduate_courses.semester', 'asc' )
			-> orderBy ( 'undergraduate_courses.units', 'desc' )
			-> orderBy ( 'undergraduate_courses.course_code', 'asc' )
			-> select ( 'undergraduate_courses.*',
						DB::raw (
										"concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer" ),
						'undergraduate_results.remarks as status', 'undergraduate_results.result_grade as grade' )
			-> get ( );
		return $courses;

	}

	private function getCurrentSemesterCourses ( )
	{
		$level = $this->userData->getLevel ( );
		$semester = $this->userData->getCurrentSemester ( );
		$reg_no = Auth::user ( )->name;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		$courses = DB::table ( 'undergraduate_courses' )->leftJoin ( 'admin_access',
						'undergraduate_courses.system_id', '=', 'admin_access.system_id' )
			-> leftJoin ( 'undergraduate_results',
						function ( $join ) use ( $reg_no )
						{
							$join->on ( 'undergraduate_courses.course_code', '=',
											'undergraduate_results.course_code' )
								-> where ( 'undergraduate_results.student_reg_no', '=', $reg_no );
						} )
			-> leftJoin ( 'users_staffs', 'users_staffs.staff_id', '=', 'admin_access.admin_id' )
			-> where (
						[
							[
								'undergraduate_courses.level',
								$level ],
							[
								'undergraduate_courses.semester',
								$semester ],
							[
								'undergraduate_courses.course_destination_school',
								$school ],
							[
								'undergraduate_courses.course_destination_dept',
								$department ] ] )
			-> orderBy ( 'undergraduate_courses.semester', 'asc' )
			-> orderBy ( 'undergraduate_courses.units', 'desc' )
			-> orderBy ( 'undergraduate_courses.course_code', 'asc' )
			-> select ( 'undergraduate_courses.*',
						DB::raw (
										"concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer" ),
						'undergraduate_results.remarks as status', 'undergraduate_results.result_grade as grade' )
			-> get ( );
		return $courses;

	}

	public function getStatusMessage ( $type, $error, $array1 = null, $array2 = null )
	{
		$html = "";
		$rows1 = "";
		$rows2 = "";
		$reportArray = array ();
		switch ( $type )
		{
			case ( 'VerifyRegistration' ) :
				switch ( $error )
				{
					case ( 0 ) :
						$reportArray = array (
											'Reasons for this error could be: ',
											'The selected courses are not unique, some duplicate courses were identified',
											'You tried selecting a course twice, or',
											'An uninterpretable error occured on processing your course registration.',
											'Please contact administrator on this error message.' );
						break;
					case ( 1 ) :
						$reportArray = array (
											'Reasons for this error could be: ',
											'You have registered one or more of the courses previously, or',
											'An uninterpretable error occured on processing your course registration.',
											'Please contact administrator on this error message.' );
						break;
					case ( 2 ) :
						$reportArray = array (
											'Reasons for this error could be: ',
											'You CANNOT register for one or more of the selected courses, or',
											'One or more of the selected courses are INVALID, or',
											'An uninterpretable error occured on processing your course registration.',
											'Please contact administrator on this error message.' );
						break;
					case ( 3 ) :
						$reportArray = array (
											'Reasons for this error could be: ',
											'The SUM OF UNITS for the selected courses is LESS THAN the minimum sum of units required for your course registration, or',
											'You need to add more courses to your list of selected courses, or',
											'An uninterpretable error occured on processing your course registration.',
											'Please contact administrator on this error message.' );
						break;
					case ( 4 ) :
						$reportArray = array (
											'Reasons for this error could be: ',
											'The SUM OF UNITS for the selected courses is GREATER THAN the maximum sum of units allowed for your course registration., or',
											'You need to remove some courses to your list of selected courses, or',
											'An uninterpretable error has occured with your registration.',
											'Please contact administrator on this error message.' );
						break;
					case ( 5 ) :
						$reportArray = array (
											'Reasons for this error could be: ',
											'You are currently not allowed to register courses.',
											'Your registration has been rejected by the system.',
											'An uninterpretable error has occured with your registration.',
											'Please contact administrator on this error message.' );
						break;
				}
				$icon = $this->htmlWriter->i ( 'fa fa-remove' );
				$msg = $this->htmlWriter->h ( 'Registration Failed' . $icon, 1, 'red', '', 'font-size : 3em;' );
				$reportItems = '';
				$i = 0;
				end ( $reportArray );
				$j = key ( $reportArray );
				foreach ( $reportArray as $list )
				{
					if ( $i == 0 )
						$reportItems .= $this->htmlWriter->li ( '<strong>' . $list . '</strong>',
										'list-group-item-heading strong p-3 text-left' );
					elseif ( $i == $j )
						$reportItems .= $this->htmlWriter->li ( $list, 'p-3 text-center red' );
					else
						$reportItems .= $this->htmlWriter->li ( $list, 'list-group-item p-3 text-left' );
					$i ++ ;
				}
				$report = $this->htmlWriter->ul ( $reportItems, 'list-group list-unstyled my-3 card text-left' );
				if ( isset ( $array1 ) )
				{
					$tableHeader = array (
										'Course Code',
										'Course Title',
										'Units',
										'Type' );
					$tableHeader = $this->htmlWriter->tableRows ( $tableHeader, true, 'thead-default' );
					foreach ( $array1 as $courses )
					{
						foreach ( $courses as $course )
						{
							$rows1 .= $this->htmlWriter->tableRows ( ( array ) $course, false, 'table-danger' );
						}
					}
					$course = $this->htmlWriter->table ( $tableHeader . $rows1, 'table ' );
				} else
					$course = "";
				$backButton = $this->htmlWriter->a (
								'Go back  ' . $this->htmlWriter->i ( 'fa fa-mail-reply ml-1' ),
								"undergraduate/courses/register", 'btn btn-success btn-lg', 'download', '', '' );
				$button = $this->htmlWriter->div ( $backButton, 'p-3 m-1' );
				// $button = $this->htmlWriter->div ( $backButton, 'card p-3' );
				$html = $this->htmlWriter->div ( $msg . $report . $course . $button, 'card p-3 my-3' );
				break;
			case ( 'RegisterCourse' ) :
				if ( $error )
				{
				} else // drg >> if this request is not an error
				{
					$reportArray = array (
										'Congratulations on a successful registration: ',
										'You can print this course registration as a document by clicking the print button below.',
										'Your courses are yet to be approved. Contact your course adviser concerning the approval.',
										'You are allowed edit the selection for your course registration only once, within 2 weeks after registration or while the registration awaits approval. ',
										'Below is a list of your registered courses.' );
					$icon = $this->htmlWriter->i ( 'fa fa-check' );
					$msg = $this->htmlWriter->h ( 'Successful' . $icon, 1, ' text-center green', '',
									'font-size : 3.5em;' );
					$html = $this->htmlWriter->div ( $msg, 'card p-3 my-3' );
					$reportItems = '';
					$i = 0;
					end ( $reportArray );
					$j = key ( $reportArray );
					foreach ( $reportArray as $list )
					{
						if ( $i == 0 )
							$reportItems .= $this->htmlWriter->li ( '<strong>' . $list . '</strong>',
											'list-group-item-heading strong p-3 text-left blue' );
						elseif ( $i == $j )
							$reportItems .= $this->htmlWriter->li ( $list, 'p-3 text-center green' );
						else
							$reportItems .= $this->htmlWriter->li ( $list, 'list-group-item p-3 text-left' );
						$i ++ ;
					}
					$report = $this->htmlWriter->ul ( $reportItems, 'list-group list-unstyled my-3 card' );
					if ( isset ( $array1 ) && isset ( $array2 ) ) // drg >> check if some registrations passed while some failed
					{
						$passMsg = $this->htmlWriter->h ( 'Registered Courses', 1, ' text-center green' );
						$failMsg = $this->htmlWriter->h ( 'Courses not Registered', 1, ' text-center red' );
						$tableHeader = array (
											'Course Code',
											'Course Title',
											'Units',
											'Type' );
						$tableHeader = $this->htmlWriter->tableRows ( $tableHeader, true, 'thead-default' );
						$rows1 = $course = $this->getCourseRegistration ( $array1 );
						$rows2 = $course = $this->getCourseRegistration ( $array2 );
						$course = $this->htmlWriter->table (
										$passMsg . $tableHeader . $rows1 . $failMsg . $tableHeader . $rows2, 'table ' );
					} elseif ( isset ( $array1 ) ) // drg >> check if registration was a success, and array sent
					{
						$course = $this->getCourseRegistration ( $array1 );
					} else
						$course = "";
					$backButton = $this->htmlWriter->a (
									'Go back ' . $this->htmlWriter->i ( 'fa fa-mail-reply ml-1' ),
									"undergraduate/courses/register", 'btn btn-success', 'download', '', '' );
					$backButton = $this->htmlWriter->div ( $backButton, 'pull-left m-1' );
					$downloadButton = $this->htmlWriter->a (
									'Print ' . $this->htmlWriter->i ( 'fa fa-file-pdf-o ml-1' ),
									"undergraduate/download/registration/pdf", 'btn btn-success', 'print', '', '',
									'_blank' );
					$downloadButton = $this->htmlWriter->div ( $downloadButton, 'pull-right m-1' );
					$button = $this->htmlWriter->div ( $backButton . $downloadButton );
					$button = $this->htmlWriter->div ( $button, 'card p-3' );
					$html = $this->htmlWriter->div ( $msg . $report . $course . $button, 'card p-3 my-3' );
				}
				break;
			case ( 'VerifyDocument' ) :
				switch ( $error )
				{
					case ( true ) :
						$icon = $this->htmlWriter->i ( 'fa fa-check' );
						$msg = $this->htmlWriter->h ( 'Verified' . $icon, 1, 'green', '', 'font-size : 3em;' );
						$html = $this->htmlWriter->div ( $msg, 'card p-3 my-3' );
						break;
					case ( false ) :
						$icon = $this->htmlWriter->i ( 'fa fa-remove' );
						$msg = $this->htmlWriter->h ( 'Verification Failed' . $icon, 1, 'red', '',
										'font-size : 3em;' );
						$reportArray = array (
											'Reasons for this could be: ',
											'You do not have access to verify this user,',
											'The given input is not valid for the given user,',
											'The document to be verified is no more valid, or',
											'An uninterpretable error has occured with your verification.',
											'Please contact administrator on this error message.' );
						$reportItems = '';
						$i = 0;
						end ( $reportArray );
						$j = key ( $reportArray );
						foreach ( $reportArray as $list )
						{
							if ( $i == 0 )
								$reportItems .= $this->htmlWriter->li ( '<strong>' . $list . '</strong>',
												'list-group-item-heading strong p-3 text-left' );
							elseif ( $i == $j )
								$reportItems .= $this->htmlWriter->li ( $list, 'p-3 text-center red' );
							else
								$reportItems .= $this->htmlWriter->li ( $list, 'list-group-item p-3 text-left' );
							$i ++ ;
						}
						$report = $this->htmlWriter->ul ( $reportItems, 'list-group list-unstyled my-3 card' );
						$html = $this->htmlWriter->div ( $msg . $report, 'card p-3 my-3' );
						break;
				}
				break;
		}
		return $html;

	}

	public function getFirstSemesterResults ( $session, $download = false )
	{
		$reg_no = Auth::user ( )->name;
		$rows = '';
		$heading = $this->htmlWriter->h ( 'First Semester', 3, 'card-header blue width-full text-center' );
		$results = DB::table ( 'undergraduate_results' )->where (
						[
							[
								'student_reg_no',
								$reg_no ],
							[
								'session',
								$session ],
							[
								'semester',
								1 ] ] )
			-> orderBy ( 'course_units', 'desc' )
			-> orderBy ( 'course_code', 'asc' )
			-> get ( );
		$results = $this->setResultsDisplay ( $results, $download );
		$results = $heading . $results;
		return $results;

	}

	public function getLevelCourses ( $level )
	{
		$reg_no = Auth::user ( )->name;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		$courses = DB::table ( 'undergraduate_courses' )->leftJoin ( 'admin_access',
						'undergraduate_courses.system_id', '=', 'admin_access.system_id' )
			-> leftJoin ( 'undergraduate_results',
						function ( $join ) use ( $reg_no )
						{
							$join->on ( 'undergraduate_courses.course_code', '=',
											'undergraduate_results.course_code' )
								-> where ( 'undergraduate_results.student_reg_no', '=', $reg_no );
						} )
			-> leftJoin ( 'users_staffs', 'users_staffs.staff_id', '=', 'admin_access.admin_id' )
			-> where (
						[
							[
								'undergraduate_courses.level',
								$level ],
							[
								'undergraduate_courses.course_destination_school',
								$school ],
							[
								'undergraduate_courses.course_destination_dept',
								$department ] ] )
			-> orderBy ( 'undergraduate_courses.semester', 'asc' )
			-> orderBy ( 'undergraduate_courses.units', 'desc' )
			-> orderBy ( 'undergraduate_courses.course_code', 'asc' )
			-> select ( 'undergraduate_courses.*',
						DB::raw (
										"concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer" ),
						'undergraduate_results.remarks as status', 'undergraduate_results.result_grade as grade' )
			-> get ( );
		return $courses;

	}

	private function getOutstandingCourses ( )
	{
		$reg_no = Auth::user ( )->name;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		$courses = DB::table ( 'undergraduate_courses' )->join ( 'undergraduate_results',
						function ( $join ) use ( $reg_no )
						{
							$join->on ( 'undergraduate_courses.course_code', '=',
											'undergraduate_results.course_code' )
								-> where (
											[
												[
													'undergraduate_results.student_reg_no',
													'=',
													$reg_no ],
												[
													'undergraduate_results.remarks',
													'Outstanding' ] ] );
						} )
			-> leftJoin ( 'admin_access', 'undergraduate_courses.system_id', '=', 'admin_access.system_id' )
			-> leftJoin ( 'users_staffs', 'users_staffs.staff_id', '=', 'admin_access.admin_id' )
			-> where (
						[
							[
								'undergraduate_courses.course_destination_school',
								$school ],
							[
								'undergraduate_courses.course_destination_dept',
								$department ] ] )
			-> orderBy ( 'undergraduate_results.session', 'asc' )
			-> orderBy ( 'undergraduate_courses.semester', 'asc' )
			-> orderBy ( 'undergraduate_courses.units', 'desc' )
			-> orderBy ( 'undergraduate_courses.course_code', 'asc' )
			-> select ( 'undergraduate_courses.*',
						DB::raw (
										"concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer" ),
						'undergraduate_results.remarks as status', 'undergraduate_results.result_grade as grade',
						'undergraduate_results.session as session' )
			-> get ( );
		return $courses;

	}

	public function getResultSummary ( $session, $download = false )
	{
		$rows = '';
		if ( $download )
			$padding = ' cellpadding = "2"';
		else
			$padding = "";
		$heading = $this->htmlWriter->h ( 'Result Summary', 3, 'card-header red width-full text-center' );
		// drg >> to get first semester gpa
		$firstGPA = array (
							'First Semester GPA',
							$this->userData->getGPA ( $session, 1 ),
							'' );
		$rows .= $this->htmlWriter->tableRows ( $firstGPA );
		$secondGPA = array (
							'Second Semester GPA',
							$this->userData->getGPA ( $session, 2 ),
							'' );
		$rows .= $this->htmlWriter->tableRows ( $secondGPA );
		$sessionGPA = array (
							"$session/" . ( $session + 1 ) . " Session GPA",
							$this->userData->getSessionGPA ( $session ),
							'' );
		$rows .= $this->htmlWriter->tableRows ( $sessionGPA );
		$summary = $this->htmlWriter->table ( $rows, 'table', '', '', $padding );
		$summary = $this->htmlWriter->div ( $heading . $summary, '' );
		return $summary;

	}

	public function getSecondSemesterResults ( $session, $download = false )
	{
		$reg_no = Auth::user ( )->name;
		$heading = $this->htmlWriter->h ( 'Second Semester', 3, 'card-header blue width-full text-center' );
		$results = DB::table ( 'undergraduate_results' )->where (
						[
							[
								'student_reg_no',
								$reg_no ],
							[
								'session',
								$session ],
							[
								'semester',
								2 ] ] )
			-> orderBy ( 'course_units', 'desc' )
			-> orderBy ( 'course_code', 'asc' )
			-> get ( );
		$results = $this->setResultsDisplay ( $results, $download );
		$results = $heading . $results;
		return $results;

	}

	public function getSemesterCourses ( $semester )
	{
		$reg_no = Auth::user ( )->name;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		$courses = DB::table ( 'undergraduate_courses' )->leftJoin ( 'admin_access',
						'undergraduate_courses.system_id', '=', 'admin_access.system_id' )
			-> leftJoin ( 'undergraduate_results',
						function ( $join ) use ( $reg_no )
						{
							$join->on ( 'undergraduate_courses.course_code', '=',
											'undergraduate_results.course_code' )
								-> where ( 'undergraduate_results.student_reg_no', '=', $reg_no );
						} )
			-> leftJoin ( 'users_staffs', 'users_staffs.staff_id', '=', 'admin_access.admin_id' )
			-> where (
						[
							[
								'undergraduate_courses.semester',
								$semester ],
							[
								'undergraduate_courses.course_destination_school',
								$school ],
							[
								'undergraduate_courses.course_destination_dept',
								$department ] ] )
			-> orderBy ( 'undergraduate_courses.semester', 'asc' )
			-> orderBy ( 'undergraduate_courses.units', 'desc' )
			-> orderBy ( 'undergraduate_courses.course_code', 'asc' )
			-> select ( 'undergraduate_courses.*',
						DB::raw (
										"concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer" ),
						'undergraduate_results.remarks as status', 'undergraduate_results.result_grade as grade' )
			-> get ( );
		return $courses;

	}

	public function isValidSession ( $session = null )
	{
		$academicSessions = $this->getAcademicSessions ( ); // drg >> to get the number of sections with results
		$valid = false;
		foreach ( $academicSessions as $value )
		{
			if ( $session == $value->session )
				$valid = true;
		}
		if ( $valid )
			return true;
		else
			return false;

	}

	public function setCourseDisplay ( $array )
	{
		$result = "";
		foreach ( $array as $course )
		{
			$heading = $this->htmlWriter->h (
							$course->course_title . ' ' .
											 $this->htmlWriter->small ( "(" . $course->course_code . ")" ), 5,
											'green notice-heading' );
			if (  ! empty ( $course->status ) )
				$status = $course->status;
			else
				$status = "Not Registered";
			if ( strlen ( $course->lecturer ) > 2 )
				$lecturer = $course->lecturer;
			else
				$lecturer = "Not Set";
			if ( $course->semester == 1 )
				$semester = "Harmattan";
			elseif ( $course->semester == 2 )
				$semester = "Rain";
			switch ( $course->grade )
			{
				case ( '0' ) :
					$grade = "F";
					break;
				case ( '1' ) :
					$grade = "E";
					break;
				case ( '2' ) :
					$grade = "D";
					break;
				case ( '3' ) :
					$grade = "C";
					break;
				case ( '4' ) :
					$grade = "B";
					break;
				case ( '5' ) :
					$grade = "A";
					break;
				default :
					$grade = "Not Set";
					break;
			}
			$infoArray = array (
								'Units: ' . $course->units,
								'Semester: ' . $semester,
								'Lecturer: ' . $lecturer,
								'Status: ' . $status,
								'Grade: ' . $grade );
			$infoItems = '';
			foreach ( $infoArray as $list )
			{
				$infoItems .= $this->htmlWriter->li ( $list, 'list-inline-item' );
			}
			$infoLine = $this->htmlWriter->ul ( $infoItems, 'list-inline text-muted small' );
			// $divider = $this->addElements ( '', 'div', 'divider' );
			$content = $this->htmlWriter->p ( $course->course_description, 'text-justify small' );
			$result .= $this->htmlWriter->div ( $heading . $infoLine . $content, 'list-group-item bghover' );
		}
		$result = $this->htmlWriter->div ( $result, 'list-group notice' );
		return $result;

	}

	public function setResultsDisplay ( $array, $download = false )
	{
		// drg >> to get the table headers
		$rows = '';
		if ( $download )
			$padding = ' cellpadding = "2"';
		else
			$padding = "";
		$tableHeader = array (
							'Course Code',
							'Course Units',
							'Score',
							'Grade',
							'Remarks' );
		$tableHeader = $this->htmlWriter->tableRows ( $tableHeader, true, 'thead-default' );
		// drg >> to get the table rows
		$rows = "";
		foreach ( $array as $result )
		{
			switch ( $result->result_grade )
			{
				case ( '0' ) :
					$grade = "F";
					break;
				case ( '1' ) :
					$grade = "E";
					break;
				case ( '2' ) :
					$grade = "D";
					break;
				case ( '3' ) :
					$grade = "C";
					break;
				case ( '4' ) :
					$grade = "B";
					break;
				case ( '5' ) :
					$grade = "A";
					break;
				default :
					$grade = "Not Set";
					break;
			}
			if ( is_null ( $result->result_lab ) )
				$score = $result->result_test + $result->result_exam;
			else
				$score = $result->result_test + $result->result_lab + $result->result_exam;
			$row = array (
						$result->course_code,
						$result->course_units,
						$score,
						$grade,
						$result->remarks );
			if ( $result->remarks == 'Outstanding' )
				$rows .= $this->htmlWriter->tableRows ( $row, false, 'table-danger' );
			else
				$rows .= $this->htmlWriter->tableRows ( $row );
		}
		$table = $this->htmlWriter->table ( $tableHeader . $rows, 'table table-hover', '', '', $padding );
		return $table;

	}

	public function showCourseRegistration ( )
	{
		return "Register your Courses";

	}

	public function showCurrentLevelCourses ( )
	{
		$courses = $this->getCurrentLevelCourses ( );
		$body = "";
		$content = "";
		$first = "";
		$second = "";
		$firstSemester = array ();
		$secondSemester = array ();
		$headerContent = "Showing Courses for <span class = 'green'>" . $this->userData->getLevel ( ) .
						 " level</span>";
		$header = $this->htmlWriter->h ( $headerContent, 3, 'card-header blue width-full' );
		foreach ( $courses as $course )
		{
			switch ( $course->semester )
			{
				case ( 1 ) :
					array_push ( $firstSemester, $course );
					break;
				case ( 2 ) :
					array_push ( $secondSemester, $course );
					break;
				default :
					array_push ( $firstSemester, $course );
					break;
			}
		}
		$first = $this->setCourseDisplay ( $firstSemester );
		$second = $this->setCourseDisplay ( $secondSemester );
		$divider = $this->htmlWriter->div ( '', 'divider' );
		$headingFirstSemester = $this->htmlWriter->h ( 'First Semester', 4, 'card-header blue width-full' );
		$headingSecondSemester = $this->htmlWriter->h ( 'Second Semester', 4, 'card-header blue width-full' );
		$html = $this->htmlWriter->div (
						$header . $headingFirstSemester . $first . $divider . $headingSecondSemester . $second, '' );
		return $html;

	}

	public function showCurrentSemesterCourses ( )
	{
		$courses = $this->getCurrentSemesterCourses ( );
		$level = $this->userData->getLevel ( );
		$semester = $this->userData->getCurrentSemester ( );
		if ( $semester == 1 )
			$semester = "Harmattan Semester";
		elseif ( $semester == 2 )
			$semester = "Rain Semester";
		$body = "";
		$content = "";
		$first = "";
		$second = "";
		$headerContent = "Showing Courses for " . $level . " level, <span class = 'green'>" . $semester . "</span>";
		$header = $this->htmlWriter->h ( $headerContent, 3, 'card-header blue' );
		$body = $this->setCourseDisplay ( $courses );
		$html = $this->htmlWriter->div ( $header . $body, '' );
		return $html;

	}

	public function showLevelCourses ( $level = null )
	{
		if (  ! isset ( $level ) )
			$level = $this->userData->getLevel ( );
		$courses = $this->getLevelCourses ( $level );
		$firstSemester = array ();
		$secondSemester = array ();
		$headerContent = "Showing Courses for <span class = 'green'>" . $level . " level</span>";
		$heading = $this->htmlWriter->h ( $headerContent, 3, 'blue pull-left', 'header' );
		$currentLevel = $this->htmlWriter->input ( 'hidden', $level, '', 'option', true );
		$optionArray = array (
							[
								1,
								'100 level' ],
							[
								2,
								'200 level' ],
							[
								3,
								'300 level' ],
							[
								4,
								'400 level' ],
							[
								5,
								'500 level' ] );
		$optionItems = ''; // $this->addElements ( $optionArray, 'option' );
		foreach ( $optionArray as $options )
		{
			$optionItems .= $this->htmlWriter->option ( $options [ 0 ], $options [ 1 ], 'list-inline-item' );
		}
		$selectTag = $this->htmlWriter->select ( $optionItems, 'form-control', 'specify', 'level' );
		$button = $this->htmlWriter->button ( 'Show', 'btn cool-button form-control', 'specify-btn',
						'changeHTML()' );
		$form = $this->htmlWriter->div ( $currentLevel . $selectTag . $button, 'form-group list-inline pull-right' );
		$header = $this->htmlWriter->div ( $heading . $form, 'card-header blue width-full' );
		foreach ( $courses as $course )
		{
			switch ( $course->semester )
			{
				case ( 1 ) :
					array_push ( $firstSemester, $course );
					break;
				case ( 2 ) :
					array_push ( $secondSemester, $course );
					break;
				default :
					array_push ( $firstSemester, $course );
					break;
			}
		}
		$first = $this->setCourseDisplay ( $firstSemester );
		$second = $this->setCourseDisplay ( $secondSemester );
		$divider = $this->htmlWriter->div ( '', 'divider' );
		$headingFirstSemester = $this->htmlWriter->h ( 'First Semester', 4, 'card-header blue width-full' );
		$headingSecondSemester = $this->htmlWriter->h ( 'Second Semester', 4, 'card-header blue width-full' );
		$html = $this->htmlWriter->div (
						$header . $headingFirstSemester . $first . $divider . $headingSecondSemester . $second, '' );
		return $html;

	}
	// drg >> to show outstanding courses
	public function showOutstandingCourses ( )
	{
		$courses = $this->getOutstandingCourses ( );
		if ( count ( $courses ) > 0 )
		{
			$body = "";
			$content = "";
			$result = "";
			$divider = $this->htmlWriter->div ( '', 'divider' );
			$temp = "";
			$head = "";
			$heading = "";
			$header = $this->htmlWriter->h ( 'Showing <span class = "green">Outstanding</span> Courses', 3,
							'blue width-full card-header', 'header' );
			$i = 0;
			$j = 0;
			foreach ( $courses as $course )
			{
				$session = $course->session;
				if ( $temp !== $session )
				{
					$head = $this->htmlWriter->h (
									$course->session . '/' . ( $course->session + 1 ) . ' Academic Session', 4,
									'card-header blue width-full' );
					$result .= $head;
				}
				$heading = $this->htmlWriter->h (
								$course->course_title . ' ' .
												 $this->htmlWriter->small ( "(" . $course->course_code . ")" ), 5,
												'green notice-heading' );
				if (  ! empty ( $course->status ) )
					$status = $course->status;
				else
					$status = "Not Registered";
				if ( strlen ( $course->lecturer ) > 2 )
					$lecturer = $course->lecturer;
				else
					$lecturer = "Not Set";
				if ( $course->semester == 1 )
					$semester = "Harmattan";
				elseif ( $course->semester == 2 )
					$semester = "Rain";
				switch ( $course->grade )
				{
					case ( 0 ) :
						$grade = "F";
						break;
					case ( '1' ) :
						$grade = "E";
						break;
					case ( '2' ) :
						$grade = "D";
						break;
					case ( '3' ) :
						$grade = "C";
						break;
					case ( '4' ) :
						$grade = "B";
						break;
					case ( '5' ) :
						$grade = "A";
						break;
					default :
						$grade = "Not Set";
						break;
				}
				$infoArray = array (
									'Units: ' . $course->units,
									'Semester: ' . $semester,
									'Lecturer: ' . $lecturer,
									'Status: ' . $status,
									'Grade: ' . $grade );
				$infoItems = '';
				foreach ( $infoArray as $list )
				{
					$infoItems .= $this->htmlWriter->li ( $list, 'list-inline-item' );
				}
				$infoLine = $this->htmlWriter->ul ( $infoItems, 'list-inline text-muted small' );
				$content = $this->htmlWriter->p ( $course->course_description, 'text-justify small' );
				$result .= $this->htmlWriter->div ( $heading . $infoLine . $content, 'list-group-item bghover' );
				$j ++ ;
				$temp = $course->session;
			}
			$body = $this->htmlWriter->div ( $result, 'list-group notice' );
			$html = $this->htmlWriter->div ( $header . $body, '' );
		} else
			$html = $this->htmlWriter->h ( 'Congratulations, you have no outstanding course.', 3,
							'card-header green width-full' );
		return $html;

	}

	public function showSemesterCourses ( $semester = null )
	{
		if (  ! isset ( $semester ) )
			$semester = $this->userData->getCurrentSemester ( );
		$courses = $this->getSemesterCourses ( $semester );
		$firstyr = array ();
		$secondyr = array ();
		$thirdyr = array ();
		$fourthyr = array ();
		$finalyr = array ();
		$body = "";
		$content = "";
		$currentSemester = $this->htmlWriter->input ( 'hidden', $semester, '', 'option', true );
		if ( $semester == 1 )
			$semester = "Harmattan Semester";
		elseif ( $semester == 2 )
			$semester = "Rain Semester";
		$headerContent = "Showing Courses for <span class = 'green'>" . $semester . "</span>";
		$heading = $this->htmlWriter->h ( $headerContent, 3, 'blue pull-left', 'header' );
		$optionArray = array (
							[
								1,
								'First Semester' ],
							[
								2,
								'Second Semester' ] );
		$optionItems = '';
		foreach ( $optionArray as $options )
		{
			$optionItems .= $this->htmlWriter->option ( $options [ 0 ], $options [ 1 ], 'list-inline-item' );
		}
		$selectTag = $this->htmlWriter->select ( $optionItems, 'form-control', 'specify', 'semester' );
		$button = $this->htmlWriter->button ( 'Show', 'btn cooltbutton form-control', 'specify-btn',
						'changeHTML()' );
		$form = $this->htmlWriter->div ( $currentSemester . $selectTag . $button,
						'form-group list-inline pull-right' );
		$header = $this->htmlWriter->div ( $heading . $form, 'card-header blue width-full' );
		foreach ( $courses as $course )
		{
			switch ( $course->level )
			{
				case ( 100 ) :
					array_push ( $firstyr, $course );
					break;
				case ( 200 ) :
					array_push ( $secondyr, $course );
					break;
				case ( 300 ) :
					array_push ( $thirdyr, $course );
					break;
				case ( 400 ) :
					array_push ( $fourthyr, $course );
					break;
				case ( 500 ) :
					array_push ( $finalyr, $course );
					break;
				default :
					array_push ( $firstyr, $course );
					break;
			}
		}
		$first = $this->setCourseDisplay ( $firstyr );
		$second = $this->setCourseDisplay ( $secondyr );
		$third = $this->setCourseDisplay ( $thirdyr );
		$fourth = $this->setCourseDisplay ( $fourthyr );
		$final = $this->setCourseDisplay ( $finalyr );
		$divider = $this->htmlWriter->div ( '', 'divider' );
		$headingFirstYr = $this->htmlWriter->h ( '100 Level', 4, 'card-header blue width-full' );
		$headingSecondYr = $this->htmlWriter->h ( '200 Level', 4, 'card-header blue width-full' );
		$headingThirdYr = $this->htmlWriter->h ( '300 Level', 4, 'card-header blue width-full' );
		$headingFourthYr = $this->htmlWriter->h ( '400 Level', 4, 'card-header blue width-full' );
		$headingFinalYr = $this->htmlWriter->h ( '500 Level', 4, 'card-header blue width-full' );
		$html = $this->htmlWriter->div (
						$header . $headingFirstYr . $first . $divider . $headingSecondYr . $second . $headingThirdYr .
										 $third . $headingFourthYr . $fourth . $headingFinalYr . $final );
		return $html;

	}

	public function showResults ( $session = null, $control = true )
	{
		$optionItems = "";
		$divider = $this->htmlWriter->div ( '', 'divider' );
		if (  ! isset ( $session ) )
		{
			$session = $this->userData->getLastSession ( );
			$session = date ( 'Y', strtotime ( $session->start ) );
		}
		$heading = "Showing results for <span class = 'green' id = 'currentSession'>$session/" . ( $session + 1 ) .
						 "</span> Academic Session";
		$heading = $this->htmlWriter->h ( $heading, 2, 'card-header blue width-full text-center' );
		// drg >> to get the user details
		$userDetails = $this->userData->getUsersMeta ( );
		$detailsArr = array (
							[
								'Name',
								$userDetails->first_name . ' ' . $userDetails->last_name,
								'',
								'text-muted' ],
							[
								'Reg No.',
								$userDetails->reg_no,
								'',
								'text-muted' ],
							[
								'School',
								$userDetails->faculty,
								'',
								'text-muted' ],
							[
								'Department',
								$userDetails->dept,
								'',
								'text-muted' ],
							[
								'Level',
								$this->userData->getLevel ( ),
								'',
								'text-muted' ],
							[
								'Cummulative GPA',
								$this->userData->getCommulativeGPA ( ),
								'',
								'text-muted' ],
							[
								'Previous Session GPA',
								$this->userData->getSessionGPA ( ),
								'',
								'text-muted' ],
							[
								'Outstanding Courses',
								$this->htmlWriter->a ( $this->userData->getOutstandingCourses ( ),
												'undergraduate/courses/outstanding' ),
								'',
								'text-muted' ] );
		$details = $this->htmlWriter->dataList ( $detailsArr );
		$details = $this->htmlWriter->dl ( $details, 'dl-horizontal small green' );
		$details = $this->htmlWriter->div ( $details, 'media-body text-left' );
		$photo = $this->htmlWriter->img ( $userDetails->photo_location,
						'img img-thumbnail d-flex align-self-center', '',
						$userDetails->first_name . ' ' . $userDetails->last_name, '', '',
						'width: 7.5rem; height: auto;', '120', '' );
		$details = $this->htmlWriter->div ( $details . $photo, 'media mx-4 mt-3' );
		// drg >> the control tab
		$downloadButton = $this->htmlWriter->a ( 'Print ' . $this->htmlWriter->i ( 'fa fa-file-pdf-o ml-2' ),
						"undergraduate/download/result/$session/pdf", 'btn btn-success form-control pull-right',
						'download', '', '', '_blank' );
		$downloadButton = $this->htmlWriter->div ( $downloadButton, 'pr-3 pull-right', '',
						'border-right: 1px dotted;' );
		$academicSessions = $this->getAcademicSessions ( ); // drg >> to get the number of sections with results
		foreach ( $academicSessions as $options )
		{
			$optionItems .= $this->htmlWriter->option ( $options->session,
							$options->session . '/' . ( $options->session + 1 ), 'list-inline-item' );
		}
		$currentSession = $this->htmlWriter->input ( 'hidden', $session, '', 'option', true );
		$selectTag = $this->htmlWriter->select ( $optionItems, 'form-control', 'specify', 'session' );
		$button = $this->htmlWriter->button ( 'Show', 'btn cool-button form-control', 'specify-btn',
						'changeResult()' );
		$form = $this->htmlWriter->div ( $currentSession . $selectTag . $button, 'list-inline pl-3 pull-right' );
		$controlTab = $this->htmlWriter->div ( $form . $downloadButton, 'list-inline p-3 ' );
		$controlTab = $this->htmlWriter->div ( $controlTab, 'card' );
		$firstSemester = $this->getFirstSemesterResults ( $session ); // drg >> get first semester results
		$secondSemester = $this->getSecondSemesterResults ( $session ); // drg >> get second semester results
		$resultSummary = $this->getResultSummary ( $session );
		$results = $this->htmlWriter->div ( $firstSemester . $secondSemester . $resultSummary, '', 'area' );
		if (  ! $control )
			$controlTab = "";
		$html = $this->htmlWriter->div ( $heading . $details . $controlTab . $results, '' );
		return $html;

	}

}
