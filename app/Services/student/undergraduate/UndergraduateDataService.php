<?php

namespace App\Services\student\undergraduate;

use App\Http\Controllers\HomeController;
use App\Http\Controllers\student\undergraduate\FeesController;
use App\Http\Controllers\student\undergraduate\MessageController;
use App\Invoice;
use App\Invoice_line;
use App\Notice;
use App\Payment;
use App\SchoolCalendar;
use App\TimeTable;
use App\Users_undergraduate;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class UndergraduateDataService
{

    /*
     * drg>>This serves the home pages based on the tpe of user
     *
     */
    public function __construct($id = null)
    {
        //
        $this->id = $id;
    }

    public function getAcademicSessions($id = null)
    {
        if (!isset ($id)) {
            $id = Auth::user()->name;
        }
        $sessions = DB::table('undergraduate_results')
            ->where('student_reg_no', $id)
            ->select('session')->where('status', 'Verified')
            ->distinct()
            ->get();
        return $sessions;

    }

    public function getCurrentSession()
    {
        $currentSession = DB::table('school_calendar')
            ->where('title', 'current_session')
            ->first();
        return $currentSession;

    }

    public function getCurrentSemester()
    {
        $currentSemester = DB::table('school_calendar')->where(
            [
                [
                    'title',
                    'first_semester'
                ],
                [
                    'start',
                    '<=',
                    DB::raw('now()')
                ],
                [
                    'end',
                    '>=',
                    DB::raw('now()')
                ]
            ])
            ->orWhere(
                [
                    [
                        'title',
                        'second_semester'
                    ],
                    [
                        'start',
                        '<=',
                        DB::raw('now()')
                    ],
                    [
                        'end',
                        '>=',
                        DB::raw('now()')
                    ]
                ])
            ->first();
        if ($currentSemester) {
            if ($currentSemester->title == 'first_semester') {
                $currentSemester = 1;
            } elseif ($currentSemester->title == 'second_semester') {
                $currentSemester = 2;
            }
        } else {
            $currentSemester = 1;
        }
        return $currentSemester;

    }

    public function getLastSession()
    {
        $lastSession = DB::table('school_calendar')
            ->where('title', 'last_session')
            ->first();
        return $lastSession;

    }

    public function getCommulativeGPA()
    {
        $cond1 = "Verified";
        $cond2 = "Corrected";
        $userid = $this->id ?: Auth::user()->name;
        // drg >> to get the total grade point
        $tgp = DB::table('undergraduate_results')->where(
            function ($query) use ($cond1, $cond2) {
                $query->where('status', $cond1)
                    ->orWhere('status', $cond2);
            })
            ->where('student_reg_no', $userid)->where('status', 'Verified')
            ->sum(DB::raw('result_grade * course_units'));
        // drg >> to get the total number of units
        $tnu = DB::table('undergraduate_results')->where(
            function ($query) use ($cond1, $cond2) {
                $query->where('status', $cond1)
                    ->orWhere('status', $cond2);
            })->where('status', 'Verified')
            ->where('student_reg_no', $userid)
            ->sum('course_units');
        //
        try {
            if ($tgp == 0 || $tnu == 0) {
                throw new Exception ();
            }
            $cgp = $tgp / $tnu;
            $cgpa = floor($cgp * 100) / 100;
        } catch (Exception $e) {
            $cgpa = 0;
        }
        return $cgpa;

    }

    public function getCourseDuration()
    {
        $userMeta = $this->getUsersMeta();
        $courseDuration = $userMeta->course_duration * 100;
        return $courseDuration;

    }

    public function getDeptStaff()
    {
        $admissionYear = $this->getUsersMeta()->year_entry;
        $hod = "HOD_" . Auth::user()->school . "_" . Auth::user()->dept;
        $ca = "CA_" . $admissionYear . "_" . Auth::user()->school . "_"
            . Auth::user()->dept;
        $dean = "DEAN_";
        $staffs = array();
        $deptStaff = DB::table('admin_access')
            ->join('users_staffs', 'admin_access.admin_id', '=',
                'users_staffs.staff_id')
            ->where(
                [
                    [
                        'admin_function',
                        'head_of_department'
                    ],
                    [
                        'system_id',
                        $hod
                    ]
                ])
            ->orWhere(
                [
                    [
                        'admin_function',
                        'course_adviser'
                    ],
                    [
                        'system_id',
                        $ca
                    ]
                ])
            ->orWhere(
                [
                    [
                        'admin_function',
                        'dean'
                    ],
                    [
                        'system_id',
                        $dean
                    ]
                ])
            ->select(DB::raw("concat(users_staffs.first_name, ' ', users_staffs.last_name) as name"),
                'admin_access.admin_function')
            ->get();
        foreach ($deptStaff as $staff) {
            switch ($staff->admin_function) {
                case ('dean') :
                    $staffs ['dean'] = $staff->name;
                    break;
                case ('head_of_department') :
                    $staffs ['hod'] = $staff->name;
                    break;
                case ('course_adviser') :
                    $staffs ['ca'] = $staff->name;
                    break;
            }
        }
        return ( object )$staffs;

    }

    public function getGPA($session = null, $semester = 1)
    {
        $cond1 = "Verified";
        $cond2 = "Corrected";
        $userid = $this->id ?: Auth::user()->name;
        // drg>>to fetch date for last session and current session
        if (!isset ($session)) {
            $session = date('Y', strtotime($this->getsession()->start));
        }
        // drg >> to get the commulative cgpa
        $tgp = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $userid
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ]
            ])
            ->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })->where('status', 'Verified')
            ->sum(DB::raw('result_grade*course_units'));
        $tnu = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $userid
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ]
            ])
            ->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })->where('status', 'Verified')
            ->sum('course_units');
        try {
            if ($tgp == 0 || $tnu == 0) {
                throw new Exception ();
            }
            $gp = $tgp / $tnu;
            $gpa = floor($gp * 100) / 100;
        } catch (Exception $e) {
            $gpa = 0;
        }
        return $gpa;

    }

    public function getMsgCount()
    {
        $message = new MessageController();
        $count = $message->countUnreadMessages($message->getInbox());
        return $count;
    }

    public function getSemesterResults($session, $semester, $download = false)
    {
        $reg_no = $this->id ?: Auth::user()->name;
        $results = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $reg_no
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ]
            ])->where('status', 'Verified')
            ->orderBy('course_units', 'desc')
            ->orderBy('course_code', 'asc')
            ->get();
        return $results;

    }

    public function getSessionGPA($session = null)
    {
        $cond1 = "Verified";
        $cond2 = "Corrected";
        $userid = $this->id ?: Auth::user()->name;
        // drg>>to fetch date for last session and current session
        if (!isset ($session)) {
            $session = date('Y', strtotime($this->getLastSession()->start));
        }
        // drg >> to get the commulative cgpa
        $tgp = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $userid
                ],
                [
                    'session',
                    $session
                ]
            ])
            ->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })->where('status', 'Verified')
            ->sum(DB::raw('result_grade*course_units'));
        $tnu = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $userid
                ],
                [
                    'session',
                    $session
                ]
            ])
            ->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })->where('status', 'Verified')
            ->sum('course_units');
        if ($tnu == 0 || $tnu == null || $tgp == 0 || $tgp == null) {
            $gp = 0;
        } else {
            $gp = $tgp / $tnu;
        }
        $gpa = floor($gp * 100) / 100;
        return $gpa;

    }

    public function setUndergraduateData(Request $request)
    {
        $fees=new FeesController();
        // drg>> to get user's level of study
        $data ['level'] = $this->getLevel();
        // drg>> this query wil get the users details
        $data ['user_meta'] = $this->getUsersMeta();
        $data ['gpa_commulative'] = $this->getCommulativeGPA();
        $data ['gpa_previous'] = $this->getSessionGPA();
        $data ['outstanding_courses'] = $this->getOutstandingCourses();
        $data ['outstanding_fees'] = $fees->getOutstandingFees();
        $data ['deptStaff'] = $this->getDeptStaff();
        $home = new HomeController();
        $data ['uid'] = $home->getUniqueId(Auth::user());
        $data ['notices'] = $this->getNotices(3);
        $data['msgCount'] = $this->getMsgCount();
        $request->session()->put('userData', (object)$data);
    }

    function getUsersMeta()
    {
        $userid = $this->id ?: Auth::user()->name;;
        $usersMeta = DB::table('users_undergraduates')->where('reg_no', $userid)
            ->first();
        return $usersMeta;

    }

    // drg>> these are minor functions (i.e. functions that are used inside the php class)
    function getNotices($limit = 3)
    {
        // drg>> we get the category the user belongs in
        $type = Auth::user()->user_type;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        // drg>> we then set the various system_ids for the notices
        $general_sid = "mums";
        $type_sid = $general_sid . "_" . $type;
        $school_sid = $type_sid . "_" . $school;
        $dept_sid = $school_sid . "_" . $department;
        // drg>> we then query the notice database table
        $notice = Notice::join('admin_access',
            function ($join) {
                $join->on('notices.system_id', '=', 'admin_access.system_id')
                    ->on('notices.announcer', '=',
                        'admin_access.admin_function');
            })->where('notices.system_id', $general_sid)
            ->orWhere('notices.system_id', $type_sid)
            ->orWhere('notices.system_id', $school_sid)
            ->orWhere('notices.system_id', $dept_sid)
            ->latest()
            ->select(DB::raw('notices.*, admin_access.admin_title'))
            ->paginate($limit);
        return $notice;

    }

    public function getCalendar(
        $start,
        $end
    ) // drg>> function services JSON request
    {
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        $events = SchoolCalendar::whereDate('start', '>=', $start)
            ->whereDate('end', '<=', $end)
            ->get();
        return $events;

    }

    public function getLevel($exact = false)
    {
        $lastSession = $this->getLastSession();
        $currentSession = $this->getCurrentSession();
        // drg>>to fetch date for last session and current session
        $lastYr = date('Y', strtotime($lastSession->start));
        $currentYr = date('Y', strtotime($currentSession->start));
        $courseDuration = $this->getCourseDuration();
        // drg>> this query wil get the users details
        $userMeta = $this->getUsersMeta();
        // drg>> to get user's level of study
        $yearsInSchool = $currentYr - ($userMeta->year_entry) + 1;
        if ($userMeta->mode_of_entry == "DIRECT") {
            $level = ($yearsInSchool + 1) * 100;
        } else {
            $level = $yearsInSchool * 100;
        }
        // drg>> to check if user has passed graduation year
        if ($exact) {
            $level = $level;
        } elseif (!$exact && $level > $courseDuration) {
            $level = $courseDuration;
        }
        return $level;

    }

    public function getOutstandingCourses()
    {
        $userid = $this->id ?: Auth::user()->name;
        $outstanding = DB::table('undergraduate_results')->where(
            [
                [
                    'remarks',
                    'Outstanding'
                ],
                [
                    'student_reg_no',
                    $userid
                ]
            ])->where('status', 'Verified')
            ->count();
        return $outstanding;

    }

    public function getTimeTable(
        $start,
        $end
    ) // drg>> function services JSON request
    {
        $start = date('l', strtotime($start));
        $end = date('l', strtotime($end));
        $data = session('userData');
        $timeTable = TimeTable::join('users_staffs', 'time_table.lecturer', '=',
            'users_staffs.staff_id')->where(
            [
                [
                    'time_table.week_day',
                    '=',
                    DB::raw("'" . $start . "'")
                ],
                [
                    'time_table.dept',
                    '=',
                    $data->user_meta->dept
                ],
                [
                    'time_table.faculty',
                    '=',
                    $data->user_meta->faculty
                ],
                [
                    'time_table.level',
                    '=',
                    $data->level
                ]
            ])
            ->select(
                DB::raw(
                    "concat(time_table.course_code,' Lecture\nLocation: ', time_table.location,'\nLecturer: ',
						users_staffs.first_name ,' ', users_staffs.last_name, '.\nDuration: ', time_table.start_time, ' to ',
						time_table.end_time ) as title, time_table.start_time as start, time_table.end_time as end"))
            ->get();
        return $timeTable;

    }

}