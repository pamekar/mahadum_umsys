<?php

namespace App\Services\student\undergraduate;

use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class CourseService
{

    public function __construct(
        UndergraduateDataService $user,
        HTMLService $HTML
    ) {
        $this->userData = $user;
        $this->html = $HTML;

    }

    public function canRegister()
    {
        $canRegister = DB::table('school_calendar')
            ->where('title', 'registration_period')
            ->whereDate('start', '<=', Carbon::now())
            ->whereDate('end', '>=', Carbon::now())
            ->first();
        return $canRegister;

    }

    public function getHTML($page = 'default')
    {
        switch ($page) {
            case ('current-level') :
                $html = $this->showCurrentLevelCourses();
                break;
            case ('current-semester') :
                $html = $this->showCurrentSemesterCourses();
                break;
            case ('default') :
                $html = $this->showCurrentLevelCourses();
                break;
            case ('level') :
                $html = $this->showLevelCourses();
                break;
            case ('outstanding') :
                $html = $this->showOutstandingCourses();
                break;
            case ('results') :
                $html = $this->showResults();
                break;
            case ('semester') :
                $html = $this->showSemesterCourses();
                break;
            default :
                $html = $this->showCurrentLevelCourses();
                break;
        }
        return $html;

    }

    function isRegistered()
    {
        $id = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $session = $this->userData->getCurrentSession();
        $session = date('Y', strtotime($session->start));
        $semester = $this->userData->getCurrentSemester();
        $isRegistered = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $id
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ]
            ])
            ->where(
                function ($query) {
                    $query->where('status', 'Registered')
                        ->orWhere('status', 'Approved');
                })
            ->first();
        return $isRegistered;
    }

    function isApproved()
    {
        $id = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $session = $this->userData->getCurrentSession();
        $session = date('Y', strtotime($session->start));
        $semester = $this->userData->getCurrentSemester();
        $approved = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $id
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ]
            ])
            ->whereNotIn('status', [
                'Registered'
            ])
            ->whereIn('status', [
                'Approved'
            ])
            ->get();
        if (sizeof($approved) >= 1) {
            return true;
        } else {
            return false;
        }

    }

    public function getPageTitle($page = 'default')
    {
        switch ($page) {
            case ('current-level') :
                $title = "Current Level Courses";
                break;
            case ('current-semester') :
                $title = "Current Semester Courses";
                break;
            case ('default') :
                $title = "Current Level Courses";
                break;
            case ('level') :
                $title = "Courses by level";
                break;
            case ('outstanding') :
                $title = "Outstanding Courses";
                break;
            case ('register') :
//                if ($this->canRegister()) {
                if ($this->isRegistered()) {
                    $title = "Registered Courses";
                } else {
                    $title = "Course Registration";
                }
                /*} else {
                    $title = "Register Course";
                }*/
                break;
            case ('results') :
                $title = "Results";
                break;
            case ('semester') :
                $title = "Courses by Semester";
                break;
            case ('outstanding') :
                $title = "Outstanding Courses";
                break;
            case ('registered') :
                $title = "Registered Courses";
                break;
            default :
                $title = "Courses";
                break;
        }
        return $title;

    }

    public function getCourseRegistrationArray($strictlyRegistered = false)
    {
        if ($strictlyRegistered) {
            // drg >> this checks if the request if for a download page
            $remarks = [
                'Registered',
                'Approved'
            ];
        } elseif ($this->isRegistered()) {
            $remarks = [
                'Registered',
                'Approved',
                'Outstanding'
            ];
        } else {
            $remarks = [
                'Outstanding'
            ];
        }
        $id = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $level = $this->userData->getLevel(true);
        $semester = $this->userData->getCurrentSemester();
        $courses = DB::table('undergraduate_courses')
            ->leftJoin('undergraduate_results',
                function ($join) use ($id, $semester) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->on('undergraduate_courses.course_destination_dept',
                            '=',
                            'undergraduate_results.student_dept')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $id);
                })
            ->where('undergraduate_courses.course_destination_school', $school)
            ->where('undergraduate_courses.course_destination_dept',
                $department)
            ->where('undergraduate_courses.level', $level)
            ->where('undergraduate_courses.semester', $semester)
            ->orWhere(
                function ($query) use ($remarks, $semester) {
                    $query->whereIn('undergraduate_results.remarks', $remarks)
                        ->where('undergraduate_courses.semester', $semester);
                })
            ->select(DB::raw('undergraduate_courses.id + 37 AS id'),
                'undergraduate_courses.course_code',
                'undergraduate_courses.course_title',
                'undergraduate_courses.units',
                'undergraduate_courses.course_type',
                'undergraduate_results.remarks')
            ->orderBy('undergraduate_results.remarks', 'ASC')
            ->orderBy('undergraduate_courses.units', 'DESC')
            ->orderBy('undergraduate_courses.level', 'ASC')
            ->orderBy('undergraduate_courses.course_code', 'ASC')
            ->get(); // return response()->json($courses);
        return $courses;

    }

    private function getCurrentSemesterCourses()
    {
        $level = $this->userData->getLevel();
        $semester = $this->userData->getCurrentSemester();
        $reg_no = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')->leftJoin('admin_access',
            'undergraduate_courses.system_id', '=', 'admin_access.system_id')
            ->leftJoin('undergraduate_results',
                function ($join) use ($reg_no) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $reg_no);
                })
            ->leftJoin('users_staffs', 'users_staffs.staff_id', '=',
                'admin_access.admin_id')
            ->where(
                [
                    [
                        'undergraduate_courses.level',
                        $level
                    ],
                    [
                        'undergraduate_courses.semester',
                        $semester
                    ],
                    [
                        'undergraduate_courses.course_destination_school',
                        $school
                    ],
                    [
                        'undergraduate_courses.course_destination_dept',
                        $department
                    ]
                ])
            ->orderBy('undergraduate_courses.semester', 'asc')
            ->orderBy('undergraduate_courses.units', 'desc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->select('undergraduate_courses.*',
                DB::raw(
                    "concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer"),
                'undergraduate_results.remarks as status',
                'undergraduate_results.result_grade as grade')
            ->get();
        return $courses;

    }

    public function getLevelCourses($level)
    {
        $reg_no = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')->leftJoin('admin_access',
            'undergraduate_courses.system_id', '=', 'admin_access.system_id')
            ->leftJoin('undergraduate_results',
                function ($join) use ($reg_no) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $reg_no);
                })
            ->leftJoin('users_staffs', 'users_staffs.staff_id', '=',
                'admin_access.admin_id')
            ->where(
                [
                    [
                        'undergraduate_courses.level',
                        $level
                    ],
                    [
                        'undergraduate_courses.course_destination_school',
                        $school
                    ],
                    [
                        'undergraduate_courses.course_destination_dept',
                        $department
                    ]
                ])
            ->orderBy('undergraduate_courses.semester', 'asc')
            ->orderBy('undergraduate_courses.units', 'desc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->select('undergraduate_courses.*',
                DB::raw(
                    "concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer"),
                'undergraduate_results.remarks as status',
                'undergraduate_results.result_grade as grade')
            ->get();
        return $courses;

    }

    private function getOutstandingCourses()
    {
        $reg_no = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')
            ->join('undergraduate_results',
                function ($join) use ($reg_no) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->where(
                            [
                                [
                                    'undergraduate_results.student_reg_no',
                                    '=',
                                    $reg_no
                                ],
                                [
                                    'undergraduate_results.remarks',
                                    'Outstanding'
                                ]
                            ]);
                })
            ->leftJoin('admin_access', 'undergraduate_courses.system_id', '=',
                'admin_access.system_id')
            ->leftJoin('users_staffs', 'users_staffs.staff_id', '=',
                'admin_access.admin_id')
            ->where(
                [
                    [
                        'undergraduate_courses.course_destination_school',
                        $school
                    ],
                    [
                        'undergraduate_courses.course_destination_dept',
                        $department
                    ]
                ])
            ->orderBy('undergraduate_results.session', 'asc')
            ->orderBy('undergraduate_courses.semester', 'asc')
            ->orderBy('undergraduate_courses.units', 'desc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->select('undergraduate_courses.*',
                DB::raw(
                    "concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer"),
                'undergraduate_results.remarks as status',
                'undergraduate_results.result_grade as grade',
                'undergraduate_results.session as session')
            ->get();
        return $courses;

    }

    public function getResults($session)
    {
        $user = new UndergraduateDataService();
        $data['firstGPA'] = $user->getGPA($session, 1);
        $data['secondGPA'] = $user->getGPA($session, 2);
        $data['sessionGPA'] = $user->getSessionGPA($session);
        $data['firstSemester'] = $user->getSemesterResults($session, 1);
        $data['secondSemester'] = $user->getSemesterResults($session, 2);
        $data['session'] = $session;
        $results = View::make('student.undergraduate.partials.resultTable',
            $data);
        $html = $results->render();
        return $html;
    }

    public function getSemesterCourses($semester)
    {
        $reg_no = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')->leftJoin('admin_access',
            'undergraduate_courses.system_id', '=', 'admin_access.system_id')
            ->leftJoin('undergraduate_results',
                function ($join) use ($reg_no) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $reg_no);
                })
            ->leftJoin('users_staffs', 'users_staffs.staff_id', '=',
                'admin_access.admin_id')
            ->where(
                [
                    [
                        'undergraduate_courses.semester',
                        $semester
                    ],
                    [
                        'undergraduate_courses.course_destination_school',
                        $school
                    ],
                    [
                        'undergraduate_courses.course_destination_dept',
                        $department
                    ]
                ])
            ->orderBy('undergraduate_courses.semester', 'asc')
            ->orderBy('undergraduate_courses.units', 'desc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->select('undergraduate_courses.*',
                DB::raw(
                    "concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer"),
                'undergraduate_results.remarks as status',
                'undergraduate_results.result_grade as grade')
            ->get();
        return $courses;

    }

    public function getSpecifiedHTML($page = 'default', $id)
    {
        switch ($page) {
            case ('default') :
                $html = $this->html->showCurrentLevelCourses();
                break;
            case ('level') :
                if ($id <= 5 && $id >= 1) {
                    $html = $this->html->showLevelCourses($id);
                } else {
                    $html = $this->html->showCurrentLevelCourses();
                }
                break;
            case ('semester') :
                if ($id <= 2 && $id >= 1) {
                    $html = $this->html->showSemesterCourses($id);
                } else {
                    $html = $this->html->showCurrentSemesterCourses();
                }
                break;
            default :
                $html = $this->html->showCurrentLevelCourses();
                break;
        }
        return $html;

    }

    public function isOutstandingCourse($course = null)
    {
        $id = Auth::user()->name;
        $semester = $this->userData->getCurrentSemester();
        $outstanding = DB::table('undergraduate_results')
            ->join('undergraduate_courses',
                function ($join) use ($course, $id) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->on('undergraduate_courses.course_destination_dept',
                            '=',
                            'undergraduate_results.student_dept')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $id)
                        ->where('undergraduate_courses.id', '=', $course);
                })
            ->where('undergraduate_courses.semester', $semester)
            ->where('undergraduate_results.remarks', 'Outstanding')
            ->get();
        return $outstanding;

    }

    public function isRegisteredCourse($course = null)
    {
        $id = Auth::user()->name;
        $semester = $this->userData->getCurrentSemester();
        $registered = DB::table('undergraduate_results')
            ->join('undergraduate_courses',
                function ($join) use ($course, $id) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->on('undergraduate_courses.course_destination_dept',
                            '=',
                            'undergraduate_results.student_dept')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $id)
                        ->where('undergraduate_courses.id', '=', $course);
                })
            ->where('undergraduate_courses.semester', $semester)
            ->where(
                function ($query) {
                    $query->whereIn('undergraduate_results.remarks',
                        [
                            'Registered',
                            'Approved'
                        ])
                        ->whereNotIn('undergraduate_results.remarks',
                            [
                                'Outstanding'
                            ]);
                })
            ->get();
        return $registered;

    }

    public function registerCourse(
        $courses = array(),
        $semester = null,
        $session = null
    ) {
        if (!isset ($session)) {
            $session = $this->userData->getCurrentSession();
            $session = date('Y', strtotime($session->start));
        }
        if (!isset ($semester)) {
            $semester = $this->userData->getCurrentSemester();
        }
        $studentRegNo = Auth::user()->name;
        $studentDept = Auth::user()->dept;
        $inserted = true;
        $fails = array();
        $pass = array();
        $i = 0;
        foreach ($courses as $id) {
            $values = DB::table('undergraduate_courses')->where('id', ($id))
                ->get();
            foreach ($values as $course) {
                if ($this->isOutstandingCourse($id)) {
                    DB::table('undergraduate_results')->where(
                        [
                            [
                                'student_reg_no',
                                $studentRegNo
                            ],
                            [
                                'student_dept',
                                $studentDept
                            ],
                            [
                                'semester',
                                $semester
                            ],
                            [
                                'remarks',
                                'Outstanding'
                            ],
                            [
                                'course_code',
                                $course->course_code
                            ]
                        ])
                        ->update([
                            'remarks' => 'Carry Over'
                        ]);
                }
                $insert = DB::table('undergraduate_results')->insert(
                    [
                        'student_reg_no'    => $studentRegNo,
                        'student_dept'      => $studentDept,
                        'course_code'       => $course->course_code,
                        'course_units'      => $course->units,
                        'course_department' => $course->course_source,
                        'session'           => $session,
                        'semester'          => $semester,
                        'result_test'       => null,
                        'result_lab'        => null,
                        'result_exam'       => null,
                        'result_grade'      => null,
                        'remarks'           => 'Registered',
                        'status'            => 'Registered'
                    ]);
                if (!$insert) {
                    $inserted = false;
                    $fails [$i] = DB::table('undergraduate_courses')
                        ->where('undergraduate_courses.id', $id)
                        ->select('undergraduate_courses.course_code',
                            'undergraduate_courses.course_title',
                            'undergraduate_courses.units',
                            'undergraduate_courses.course_type')
                        ->first();
                } else {
                    $pass [$i] = DB::table('undergraduate_courses')
                        ->where('undergraduate_courses.id', $id)
                        ->select('undergraduate_courses.course_code',
                            'undergraduate_courses.course_title',
                            'undergraduate_courses.units',
                            'undergraduate_courses.course_type')
                        ->first();
                }
            }
            $i++;
        }
        if ($inserted) {
            $html = $this->html->getStatusMessage('RegisterCourse', false,
                $pass);
            return $html;
        } else {
            $html = $this->html->getStatusMessage('RegisterCourse', true,
                $fails, $pass);
            return $html;
        }

    }

    public function userCanRegister()
    {
        $regNo = Auth::user()->name;
        $userCanRegister = DB::table('users_undergraduates')
            ->where('reg_no', $regNo)
            ->where('can_register', '=', '1')
            ->first();

        return $userCanRegister;
    }

    public function updateCourse(
        $courses = array(),
        $semester = null,
        $session = null
    ) {
        if (!isset ($session)) { // drg >> the section will be used by all non-admin users admin users can skip this section
            $session = $this->userData->getCurrentSession();
            $session = date('Y', strtotime($session->start));
        }
        if (!isset ($semester)) {
            $semester = $this->userData->getCurrentSemester();
        }
        $studentRegNo = Auth::user()->name;
        $studentDept = Auth::user()->dept;
        $isRegistered = false;
        $inserted = true;
        $fails = null;
        $pass = null;
        $i = 0;
        $courseArr = DB::table('undergraduate_courses')->whereIn('id', $courses)
            ->pluck('course_code');
        $delete = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $studentRegNo
                ],
                [
                    'student_dept',
                    $studentDept
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ],
                [
                    'status',
                    'Registered'
                ]
            ])
            ->whereNotIn('course_code', $courseArr)
            ->delete();
        DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $studentRegNo
                ],
                [
                    'student_dept',
                    $studentDept
                ],
                [
                    'semester',
                    $semester
                ],
                [
                    'remarks',
                    'Carry Over'
                ]
            ])
            ->whereNotIn('course_code', $courseArr)
            ->update([
                'remarks' => 'Outstanding'
            ]);
        foreach ($courses as $id) {
            $registered = $this->isRegisteredCourse($id);
            if (sizeof($registered) >= 1) {
                $isRegistered = true;
                $pass [$i] = DB::table('undergraduate_courses')
                    ->where('undergraduate_courses.id', $id)
                    ->select('undergraduate_courses.course_code',
                        'undergraduate_courses.course_title',
                        'undergraduate_courses.units',
                        'undergraduate_courses.course_type')
                    ->get();
            } else {
                $values = DB::table('undergraduate_courses')->where('id', $id)
                    ->get();
                foreach ($values as $course) {
                    $insert = DB::table('undergraduate_results')->insert(
                        [
                            'student_reg_no'    => $studentRegNo,
                            'student_dept'      => $studentDept,
                            'course_code'       => $course->course_code,
                            'course_units'      => $course->units,
                            'course_department' => $course->course_source,
                            'session'           => $session,
                            'semester'          => $semester,
                            'result_test'       => null,
                            'result_lab'        => null,
                            'result_exam'       => null,
                            'result_grade'      => null,
                            'remarks'           => 'Registered',
                            'status'            => 'Registered'
                        ]);
                    if (!$insert) {
                        $inserted = false;
                        $fails [$i] = DB::table('undergraduate_courses')
                            ->where('undergraduate_courses.id',
                                $id)
                            ->select('undergraduate_courses.course_code',
                                'undergraduate_courses.course_title',
                                'undergraduate_courses.units',
                                'undergraduate_courses.course_type')
                            ->first();
                    } else {
                        $pass [$i] = DB::table('undergraduate_courses')
                            ->where('undergraduate_courses.id',
                                $id)
                            ->select('undergraduate_courses.course_code',
                                'undergraduate_courses.course_title',
                                'undergraduate_courses.units',
                                'undergraduate_courses.course_type')
                            ->first();
                    }
                }
            }
            $i++;
        }
        if ($inserted) {
            $html = $this->html->getStatusMessage('RegisterCourse', false,
                $pass);
            DB::table('users_undergraduates')->where('reg_no', $studentRegNo)
                ->decrement('can_register', 1);
            return $html;
        } else {
            $html = $this->html->getStatusMessage('RegisterCourse', true,
                $fails, $pass);
            return $html;
        }

    }

    public function showCurrentLevelCourses()
    {
        $level = $this->userData->getLevel();
        $courses = $this->getLevelCourses($level);
        $firstSemester = array();
        $secondSemester = array();
        $data['level'] = $this->userData->getLevel();
        foreach ($courses as $course) {
            switch ($course->semester) {
                case (1) :
                    array_push($firstSemester, $course);
                    break;
                case (2) :
                    array_push($secondSemester, $course);
                    break;
                default :
                    array_push($firstSemester, $course);
                    break;
            }
        }
        $data['firstSemester'] = $firstSemester;
        $data['secondSemester'] = $secondSemester;
        $courses
            = View::make('student.undergraduate.partials.coursesCurrentLevel',
            $data);
        $html = $courses->render();
        return $html;

    }

    public function showCurrentSemesterCourses()
    {
        $data['courses'] = $this->getCurrentSemesterCourses();
        $level = $this->userData->getLevel();
        $semester = $this->userData->getCurrentSemester();
        if ($semester == 1) {
            $semester = "Harmattan Semester";
        } elseif ($semester == 2) {
            $semester = "Rain Semester";
        }
        $data['level'] = $level;
        $data['semester'] = $semester;

        $courses
            = View::make('student.undergraduate.partials.coursesCurrentSemester',
            $data);
        $html = $courses->render();
        return $html;

    }

    public function showLevelCourses($level = null)
    {
        if (!isset($level)) {
            $level = $this->userData->getLevel();
        }
        $courses = $this->getLevelCourses($level);
        $firstSemester = array();
        $secondSemester = array();
        $data['level'] = $level;
        foreach ($courses as $course) {
            switch ($course->semester) {
                case (1) :
                    array_push($firstSemester, $course);
                    break;
                case (2) :
                    array_push($secondSemester, $course);
                    break;
                default :
                    array_push($firstSemester, $course);
                    break;
            }
        }
        $data['firstSemester'] = $firstSemester;
        $data['secondSemester'] = $secondSemester;
        $courses
            = View::make('student.undergraduate.partials.coursesLevel',
            $data);
        $html = $courses->render();
        return $html;

    }

    public function showOutstandingCourses()
    {
        $data['courses'] = $this->getOutstandingCourses();
        $courses
            = View::make('student.undergraduate.partials.coursesOutstanding',
            $data);
        $html = $courses->render();
        return $html;

    }

    public function showResults($session = null)
    {
        if (!isset ($session)) {
            $session = $this->userData->getLastSession();
            $session = date('Y', strtotime($session->start));
        }
        $user = new UndergraduateDataService();
        $userDetails = $user->getUsersMeta();
        $userDetails->level = $user->getLevel();
        $userDetails->cgpa = $user->getCommulativeGPA();
        $userDetails->pgpa = $user->getSessionGPA();
        $userDetails->outstanding = $user->getOutstandingCourses();
        $data['firstGPA'] = $user->getGPA($session, 1);
        $data['secondGPA'] = $user->getGPA($session, 2);
        $data['sessionGPA'] = $user->getSessionGPA($session);
        $data['firstSemester'] = $user->getSemesterResults($session, 1);
        $data['secondSemester'] = $user->getSemesterResults($session, 2);
        $data['session'] = $session;
        $data['userDetails'] = $userDetails;
        $data['academicSessions'] = $user->getAcademicSessions();

        $results = View::make('student.undergraduate.partials.results', $data);
        $html = $results->render();
        return $html;
    }

    public function showSemesterCourses($semester = null)
    {
        if (!isset ($semester)) {
            $semester = $this->userData->getCurrentSemester();
        }
        $courses = $this->getSemesterCourses($semester);
        $firstyr = array();
        $secondyr = array();
        $thirdyr = array();
        $fourthyr = array();
        $finalyr = array();
        if ($semester == 1) {
            $semester = "Harmattan Semester";
        } elseif ($semester == 2) {
            $semester = "Rain Semester";
        }
        $data['semester'] = $semester;
        foreach ($courses as $course) {
            switch ($course->level) {
                case (100) :
                    array_push($firstyr, $course);
                    break;
                case (200) :
                    array_push($secondyr, $course);
                    break;
                case (300) :
                    array_push($thirdyr, $course);
                    break;
                case (400) :
                    array_push($fourthyr, $course);
                    break;
                case (500) :
                    array_push($finalyr, $course);
                    break;
                default :
                    array_push($firstyr, $course);
                    break;
            }
        }
        $data['first'] = $firstyr;
        $data['second'] = $secondyr;
        $data['third'] = $thirdyr;
        $data['fourth'] = $fourthyr;
        $data['final'] = $finalyr;

        $courses
            = View::make('student.undergraduate.partials.coursesSemester',
            $data);
        $html = $courses->render();

        return $html;

    }

    public function verifyCourses(Request $request, $action = 'insert')
    {
        if ($this->canRegister() && $this->userCanRegister()) {
            $id = Auth::user()->name;
            $school = Auth::user()->school;
            $department = Auth::user()->dept;
            $level = $this->userData->getLevel(true);
            $courseDuration = $this->userData->getCourseDuration();
            $semester = $this->userData->getCurrentSemester();
            $courses = $request->all();
            $sumOfUnits = 0;
            $minUnits = 12;
            $maxUnits = 24; // drg >> boolean flags
            $isValidCourse = true;
            $isRegistered = false;
            $isCarryover = false;
            $carryoverCourse = false;
            $isDuplicate = false;
            $isGreater = false;
            $isLess = false;
            $verifyArray = array();
            $courseArray = array();
            $courses = array_splice($courses,
                2); // drg >> remove the token from the list of array values
            array_values($courses);
            $i = 0;
            end($courses);
            $j = key($courses);
            foreach ($courses as &$course) {
                $course = $course - 37;
            }
            unset ($course); // drg >> to check sum of units selected by user
            $sumOfUnits = DB::table('undergraduate_courses')
                ->whereIn('id', $courses)
                ->sum('units');
            if ($sumOfUnits > $maxUnits) {
                $isGreater = true;
            } elseif ($sumOfUnits < $minUnits) {
                $isLess = true;
            }
            if (sizeof(array_unique($courses)) !== sizeof($courses)) {
                $isDuplicate = true;
            } // drg >> to check if the course selected is a valid course
            foreach ($courses as $course) {
                if ($level > $courseDuration) {
                    $isCarryover = true;
                    $carryoverCourse = DB::table('undergraduate_courses')
                        ->where('id', $course)
                        ->first();
                    if (!$carryoverCourse) {
                        $isValidCourse = false;
                    }
                } else {
                    $valid = DB::table('undergraduate_courses')->where(
                        [
                            [
                                'id',
                                $course
                            ],
                            [
                                'semester',
                                $semester
                            ],
                            [
                                'course_destination_dept',
                                $department
                            ],
                            [
                                'course_destination_school',
                                $school
                            ]
                        ])
                        ->first();
                    if (!$valid) {
                        $isValidCourse = false;
                    }
                }

                switch ($action) {
                    case ('insert') :
                        $registered = $this->isRegisteredCourse($course);
                        if (sizeof($registered) >= 1) {
                            $isRegistered = true;
                        }
                        break;
                    case ('update') :
                        $isValidCourse = $this->canRegister();
                        break;
                    default :
                        break;
                }
                if ($isRegistered || !$isValidCourse || $isDuplicate) {
                    $courseArray [$i] = DB::table('undergraduate_courses')
                        ->where(
                            [
                                [
                                    'undergraduate_courses.semester',
                                    $semester
                                ],
                                [
                                    'undergraduate_courses.id',
                                    $course
                                ]
                            ])
                        ->select('undergraduate_courses.course_code',
                            'undergraduate_courses.course_title',
                            'undergraduate_courses.units',
                            'undergraduate_courses.course_type')
                        ->get();
                }
                $i++;
            }
            if ($isDuplicate) {
                return array(
                    false,  // drg >> result of test
                    0,
                    $courseArray
                ); // drg >> message index when the selected courses have been previously registered
            }
            if ($isRegistered) {
                return array(
                    false,  // drg >> result of test
                    1,
                    $courseArray
                ); // drg >> message index when the selected courses have been previously registered
            } elseif (!$isValidCourse) {
                return array(
                    false,  // drg >> result of test
                    2,
                    $courseArray
                );
                // drg >> message index when the selected courses is not a valid course
            } elseif ($sumOfUnits < $minUnits) {
                return array(
                    false,  // drg >> result of test
                    3,
                    null
                ); // drg >> message index when sum of courses if greater than max units
            } elseif ($sumOfUnits > $maxUnits) {
                return array(
                    false,  // drg >> result of test
                    4,
                    null
                ); // drg >> message index when sum of courses if greater than max units
            } else {
                return array(
                    true,
                    $courses
                ); // drg >> result of test
            }
        } else {
            return array(
                false,  // drg >> result of test
                5,
                null
            ); // drg >> message index when sum of courses if greater than max units
        }

    }

}