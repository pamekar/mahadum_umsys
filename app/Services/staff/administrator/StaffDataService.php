<?php

namespace App\Services\student\undergraduate;

use App\Notice;
use App\SchoolCalendar;
use App\TimeTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;

class StaffDataService
{

	/*
	 * drg>>This serves the home pages based on the tpe of user
	 *
	 */
	public function __construct ( )
	{
		//
	}

	public function getCurrentSession ( )
	{
		$currentSession = DB::table ( 'school_calendar' )->where ( 'title', 'current_session' )
			-> first ( );
		return $currentSession;

	}

	public function getCurrentSemester ( )
	{
		$currentSemester = DB::table ( 'school_calendar' )->where (
						[
							[
								'title',
								'first_semester' ],
							[
								'start',
								'<=',
								DB::raw ( 'now()' ) ],
							[
								'end',
								'>=',
								DB::raw ( 'now()' ) ] ] )
			-> orWhere (
						[
							[
								'title',
								'second_semester' ],
							[
								'start',
								'<=',
								DB::raw ( 'now()' ) ],
							[
								'end',
								'>=',
								DB::raw ( 'now()' ) ] ] )
			-> first ( );
		if ( $currentSemester->title == 'first_semester' )
			$currentSemester = 1;
		elseif ( $currentSemester->title == 'second_semester' )
			$currentSemester = 2;
		return $currentSemester;

	}

	public function getLastSession ( )
	{
		$lastSession = DB::table ( 'school_calendar' )->where ( 'title', 'last_session' )
			-> first ( );
		return $lastSession;

	}

	public function getStaffData ( )
	{
		$userid = Auth::user ( )->name;
		// drg>> to get user's level of study
		$data [ 'level' ] = $this->getLevel ( );
		// drg>> this query wil get the users details
		$data [ 'user_meta' ] = $this->getUsersMeta ( );
		$data [ 'outstanding_courses' ] = $this->getOutstandingCourses ( );
		$data [ 'notices' ] = $this->getNotices ( 4 );
		return $data;

	}

	function getUsersMeta ( )
	{
		$userid = Auth::user ( )->name;
		$usersMeta = DB::table ( 'users_staffs' )->where ( 'staff_id', $userid )
			-> first ( );
		return $usersMeta;

	}
	// drg>> these are minor functions (i.e. functions that are used inside the php class)
	function getNotices ( $limit = 3 )
	{
		// drg>> we get the category the user belongs in
		$type = Auth::user ( )->user_type;
		$school = Auth::user ( )->school;
		$department = Auth::user ( )->dept;
		// drg>> we then set the various system_ids for the notices
		$general_sid = "mums";
		$type_sid = $general_sid . "_" . $type;
		$school_sid = $type_sid . "_" . $school;
		$dept_sid = $school_sid . "_" . $department;
		// drg>> we then query the notice database table
		$notice = Notice::join ( 'admin_access',
						function ( $join )
						{
							$join->on ( 'notices.system_id', '=', 'admin_access.system_id' )
								-> on ( 'notices.announcer', '=', 'admin_access.admin_function' );
						} )->where ( 'notices.system_id', $general_sid )
			-> orWhere ( 'notices.system_id', $type_sid )
			-> orWhere ( 'notices.system_id', $school_sid )
			-> orWhere ( 'notices.system_id', $dept_sid )
			-> latest ( )
			-> select ( DB::raw ( 'notices.*, admin_access.admin_title' ) )
			-> paginate ( $limit );
		return $notice;

	}

	public function getCalendar ( $start, $end ) // drg>> function services JSON request
	{
		$start = date ( 'Y-m-d', strtotime ( $start ) );
		$end = date ( 'Y-m-d', strtotime ( $end ) );
		$events = SchoolCalendar::whereDate ( 'start', '>=', $start )->whereDate ( 'end', '<=', $end )
			-> get ( );
		return $events;

	}

	public function getTimeTable ( $start, $end ) // drg>> function services JSON request
	{
		$start = date ( 'l', strtotime ( $start ) );
		$end = date ( 'l', strtotime ( $end ) );
		$data = $this->getUndergraduateData ( );
		$timeTable = TimeTable::join ( 'users_staffs', 'time_table.lecturer', '=', 'users_staffs.staff_id' )->where (
						[
							[
								'time_table.week_day',
								'=',
								DB::raw ( "'" . $start . "'" ) ],
							[
								'time_table.dept',
								'=',
								$data [ 'user_meta' ]->dept ],
							[
								'time_table.faculty',
								'=',
								$data [ 'user_meta' ]->faculty ],
							[
								'time_table.level',
								'=',
								$data [ 'level' ] ] ] )
			-> select (
						DB::raw (
										"concat(time_table.course_code,' Lecture\nLocation: ', time_table.location,'\nLecturer: ',
						users_staffs.first_name ,' ', users_staffs.last_name, '.\nDuration: ', time_table.start_time, ' to ',
						time_table.end_time ) as title, time_table.start_time as start, time_table.end_time as end" ) )
			-> get ( );
		return $timeTable;

	}

}