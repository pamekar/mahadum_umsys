<?php

namespace App\Services;

class HTMLWriter
{

    /*
     * drg>>This serves the home pages based on the tpe of user
     *
     */
    public function __construct()
    {
        //
    }

    public function a(
        $content = '',
        $href = '',
        $class = '',
        $id = '',
        $style = '',
        $onClick = '',
        $target = ''
    ) {
        $html = "\n\t<a class = '$class' id = '$id' href= '" . url($href)
            . "' target = $target>$content</a>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function button(
        $content = '',
        $class = '',
        $id = '',
        $onClick = '',
        $style = ''
    ) {
        $html
            = "\n\t<button class = '$class' id = '$id' onClick = '$onClick'>$content</button>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function dataList($list = array())
    {
        $datalist = '';
        foreach ($list as $data) {
            $dt = $this->dt($data [0], $data [2]);
            $dd = $this->dd($data [1], $data [3]);
            $datalist .= "\n\t" . $dt . $dd . "\n\t";
        }
        return $datalist;

    }

    public function div($content = '', $class = '', $id = '', $style = '')
    {
        $html
            = "\n\t<div class = '$class' id = '$id' style = '$style'>$content</div>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function dd($content = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<dd class = '$class' id = '$id'>$content</dd>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function dl($content = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<dl class = '$class' id = '$id'>$content</dl>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function dt($list = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<dt class = '$class' id = '$id'>$list</dt>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function form(
        $content = '',
        $class = '',
        $id = '',
        $action = '',
        $method = '',
        $style = '',
        $onClick = ''
    ) {

    }

    public function h(
        $content = '',
        $size = 4,
        $class = '',
        $id = '',
        $style = ''
    ) {
        $html
            = "\n\t<h$size class = '$class' id = '$id' style = '$style'>$content</h$size>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function i($class = '', $id = '')
    {
        $html = "\n\t<i class = '$class' id = '$id'></i>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function img(
        $src = '',
        $alt = '',
        $style = '',
        $width = '',
        $height = ''
    ) {
        $public = '';
        if (config('app.env') == 'production') {
            $public = 'public';
        }
        $html
            = "\n\t<img src = '$public/$src' alt = '$alt' style = '$style' width = '$width' height = '$height' border='0'/>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function input(
        $type = 'text',
        $value = '',
        $class = '',
        $id = '',
        $required = true,
        $placeholder = '',
        $style = ''
    ) {
        if ($required) {
            $required = "required";
        } else {
            $required = "";
        }
        $html
            = "<input type = '$type' class = '$class' id = '$id' value = '$value' placeholder = '$placeholder' $required />";
        $html = $this->stripClean($html);
        return $html;
        //
    }

    public function li($content = '', $class = '', $id = '', $active = false)
    {
        $html = "\n\t<li class = '$class' id = '$id'>$content</li>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function ol($list = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<ol class = '$class' id = '$id'>$list</ol>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function option(
        $value = '',
        $content = '',
        $name = '',
        $selected = false
    ) {
        $html = "\n\t<option value = '$value'>$content</option>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function p($content = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<p class = '$class' id = '$id'>$content</p>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function small($content = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<small class = '$class' id = '$id'>$content</small>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function select(
        $content = '',
        $class = '',
        $id = '',
        $name = '',
        $style = ''
    ) {
        $html
            = "\n\t<select class = '$class' id = '$id' name = '$name'>$content</select>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function span($content = '', $class = '', $id = '', $style = '')
    {
        $html = "\n\t<span class = '$class' id = '$id'>$content</span>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    private function stripClean($content)
    {
        $html = str_replace("'", '"', $content);
        return $html;

    }

    public function table(
        $content = '',
        $class = '',
        $id = '',
        $style = '',
        $others = ''
    ) {
        $html
            = "\n\t<table class = '$class' id = '$id' $others>$content</table>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function tableRows(
        $array,
        $thead = false,
        $trClass = '',
        $tdClass = ''
    ) {
        $row = "";
        $array = ( array )$array;
        if ($thead) {
            foreach ($array as $column) {
                $row .= $this->th($column, $tdClass);
            }
            $row = $this->thead($this->tr($row), $trClass);
        } else {
            foreach ($array as $column) {
                $row .= $this->td($column, $tdClass);
            }
            $row = $this->tr($row, $trClass);
        }
        return $row;

    }

    public function td(
        $content = '',
        $class = '',
        $id = '',
        $style = '',
        $others = ''
    ) {
        $html
            = "\n\t<td class = '$class' id = '$id' style = '$style' $others>$content</td>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function th(
        $content = '',
        $class = '',
        $id = '',
        $style = '',
        $others = ''
    ) {
        $html
            = "\n\t<th class = '$class' id = '$id' style = '$style' $others>$content</th>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function thead(
        $content = '',
        $class = '',
        $id = '',
        $style = '',
        $others = ''
    ) {
        $html
            = "\n\t<thead class = '$class' id = '$id' style = '$style' $others>$content</thead>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function tr(
        $content = '',
        $class = '',
        $id = '',
        $style = '',
        $others = ''
    ) {
        $html
            = "\n\t<tr class = '$class' id = '$id' style = '$style' $others>$content</tr>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

    public function ul($list = '', $class = '', $id = '', $style = '')
    {
        $html
            = "\n\t<ul class = '$class' id = '$id' style = '$style'>$list</ul>\n\t";
        $html = $this->stripClean($html);
        return $html;

    }

}
