<?php

namespace App\Services\guardian;

use App\Http\Controllers\student\undergraduate\ChartController;
use App\Invoice;
use App\Notice;
use App\Payment;
use App\SchoolCalendar;
use App\TimeTable;
use App\Services\student\undergraduate\UndergraduateDataService;
use App\User;
use App\User_undergraduate;
use App\Users_guardian;
use App\Users_undergraduate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Exception;
use Illuminate\Support\Facades\View;

class GuardianDataService
{

    /*
     * drg>>This serves the home pages based on the tpe of user
     *
     */
    public function __construct()
    {
        //
    }

    public function getCurrentSession()
    {
        $currentSession = DB::table('school_calendar')
            ->where('title', 'current_session')
            ->first();
        return $currentSession;

    }

    public function getCurrentSemester()
    {
        $currentSemester = DB::table('school_calendar')->where(
            [
                [
                    'title',
                    'first_semester'
                ],
                [
                    'start',
                    '<=',
                    DB::raw('now()')
                ],
                [
                    'end',
                    '>=',
                    DB::raw('now()')
                ]
            ])
            ->orWhere(
                [
                    [
                        'title',
                        'second_semester'
                    ],
                    [
                        'start',
                        '<=',
                        DB::raw('now()')
                    ],
                    [
                        'end',
                        '>=',
                        DB::raw('now()')
                    ]
                ])
            ->first();
        if ($currentSemester->title == 'first_semester') {
            $currentSemester = 1;
        } elseif ($currentSemester->title == 'second_semester') {
            $currentSemester = 2;
        }
        return $currentSemester;

    }

    public function getLastSession()
    {
        $lastSession = DB::table('school_calendar')
            ->where('title', 'last_session')
            ->first();
        return $lastSession;

    }

    public function getInvoices($id)
    {
        $user = User::where('name', $id)->first();

        $target = [$user->dept, $user->school, $user->name];
        $currentSession = setting('admin.current_session');
        $year_entry = Users_undergraduate::where('reg_no', $user->name)
            ->value('year_entry');

        $payments = Payment::where('user_id', $user->name)
            ->pluck('invoice_id')->toArray();

        $invoices = Invoice::join('invoice_lines', 'invoices.invoice_id', '=',
            'invoice_lines.invoice_id')->where(function ($query) use (
            $target,
            $year_entry,
            $currentSession
        ) {
            foreach (range($year_entry, $currentSession) as $session) {
                $query->orWhere('invoices.session', $session)
                    ->whereIn('target', $target)
                    ->orWhere('invoices.type', 'university');
                $query->orWhere(function ($query) use (
                    $session,
                    $year_entry,
                    $currentSession
                ) {
                    $query->where('invoices.type', 'level')
                        ->where('invoices.session', $session)
                        ->where('invoices.target',
                            ($currentSession - $session + 1) * 100);
                });
            }
        })->whereNotIn('invoices.invoice_id', $payments)
            ->select('invoices.id', 'invoices.invoice_id', 'invoices.title',
                'invoices.session', 'invoices.issuer',
                DB::raw("SUM(invoice_lines.amount*invoice_lines.quantity) as amount"))
            ->groupBy('invoices.invoice_id')
            ->orderBy('invoices.created_at', 'desc')->get();

        return $invoices;
    }

    public function getGuardianData()
    {
        $userid = Auth::user()->name;
        $data ['user_meta'] = $this->getUsersMeta();
        $data['wards'] = $this->getWards();
        $data ['notices'] = $this->getNotices(4);
        return $data;

    }

    function getUsersMeta()
    {
        $userid = Auth::user()->name;
        $usersMeta = DB::table('users_guardians')->where('guardian_id', $userid)
            ->first();
        return $usersMeta;

    }

    function getWards()
    {
        $guardian = (array)$this->getUsersMeta();
        $statuses = ['active', 'pending', 'blocked'];
        $wards = [];
        foreach ($statuses as $status) {
            $wards[$status] = Users_undergraduate::whereIn('reg_no',
                explode(';', $guardian[$status]))
                ->where('guardian', Auth::user()->name)->get();
        }
        return $wards;
    }

    // drg>> these are minor functions (i.e. functions that are used inside the php class)
    function getNotices($limit = 3)
    {
        // drg>> we get the category the user belongs in
        $type = Auth::user()->user_type;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        // drg>> we then set the various system_ids for the notices
        $general_sid = "mums";
        $type_sid = $general_sid . "_" . $type;
        $school_sid = $type_sid . "_" . $school;
        $dept_sid = $school_sid . "_" . $department;
        // drg>> we then query the notice database table
        $notice = Notice::join('admin_access',
            function ($join) {
                $join->on('notices.system_id', '=', 'admin_access.system_id')
                    ->on('notices.announcer', '=',
                        'admin_access.admin_function');
            })->where('notices.system_id', $general_sid)
            ->orWhere('notices.system_id', $type_sid)
            ->orWhere('notices.system_id', $school_sid)
            ->orWhere('notices.system_id', $dept_sid)
            ->latest()
            ->select(DB::raw('notices.*, admin_access.admin_title'))
            ->paginate($limit);
        return $notice;

    }

    public function getCalendar(
        $start,
        $end
    ) // drg>> function services JSON request
    {
        $start = date('Y-m-d', strtotime($start));
        $end = date('Y-m-d', strtotime($end));
        $events = SchoolCalendar::whereDate('start', '>=', $start)
            ->whereDate('end', '<=', $end)
            ->get();
        return $events;

    }

    public function getResults($id, $session = null)
    {
        if (!isset ($session)) {
            $session = $this->getLastSession();
            $session = date('Y', strtotime($session->start));
        }
        $user = new UndergraduateDataService($id);
        $userDetails = $user->getUsersMeta();
        $userDetails->level = $user->getLevel();
        $userDetails->cgpa = $user->getCommulativeGPA();
        $userDetails->pgpa = $user->getSessionGPA();
        $userDetails->outstanding = $user->getOutstandingCourses();
        $data['firstGPA'] = $user->getGPA($session, 1);
        $data['secondGPA'] = $user->getGPA($session, 2);
        $data['sessionGPA'] = $user->getSessionGPA($session);
        $data['firstSemester'] = $user->getSemesterResults($session, 1);
        $data['secondSemester'] = $user->getSemesterResults($session, 2);
        $data['session'] = $session;
        $data['userDetails'] = $userDetails;

        $results = View::make('guardian.partials.results', $data);
        $html = $results->render();
        return $html;
    }

    public function hasProfile()
    {
        $profile = Users_guardian::where('guardian_id', Auth::user()->name)
            ->first();
        return $profile;
    }

    public function setGuardianData(Request $request)
    {
        $data = $this->getGuardianData();
        $request->session()->flash('userData', (object)$data);
    }
}