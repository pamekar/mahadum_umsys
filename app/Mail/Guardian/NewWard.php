<?php

namespace App\Mail\Guardian;

use App\Users_undergraduate;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Facades\DB;

class NewWard extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($guardian, $ward)
    {
        $this->guardian = $guardian;
        $this->ward = Users_undergraduate::where('reg_no', $ward)
            ->select('first_name','last_name','reg_no')->first();
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("Ward request from ".$this->ward->first_name)
            ->markdown('emails.guardian.newWard')
            ->with(['guardian' => $this->guardian, 'ward' => $this->ward]);
    }
}
