<?php
// drg >>
namespace App\Http\Controllers\staff\lecturer;

use App\Http\Controllers\Controller;
use App\Services\staff\lecturer\LecturerDataService;
use Excel;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use PHPExcel_Worksheet_MemoryDrawing;
use View;

class CourseController extends Controller
{

    public function __construct(LecturerDataService $data)
    {
        $this->userData = $data;
    }

    public function index($category = 'default', $action = 'default')
    {
        switch ($category) {
            case ('default'):
                return redirect('/staff/lecturer/courses/results/view');
                break;
            case ('attendance'):
                switch ($action) {
                    case ('view'):
                        $data['pageTitle'] = "View $category";
                        $html
                            = View::make('staff.lecturer.courses.viewAttendance',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";


                        break;
                    case ('download'):
                        $data['pageTitle'] = "Download $category";
                        $html
                            = View::make('staff.lecturer.courses.downloadAttendance',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        break;
                    case ('upload'):
                        $data['pageTitle'] = "Upload $category";
                        $html
                            = View::make('staff.lecturer.courses.uploadAttendance',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            case ('course'):
                switch ($action) {
                    case ('view'):
                        $subData['firstSemester']
                            = $this->getSemesterCourses(1);
                        $subData['secondSemester']
                            = $this->getSemesterCourses(2);
                        $html = View::make('staff.lecturer.courses.viewCourse',
                            $subData);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        $data['pageTitle'] = "View $category" . "s";
                        break;
                    case ('session'):
                        $data['html'] = "$action</br>$category";
                        $data['activeNavbar'] = "courses";
                        $data['pageTitle'] = "Download $category";
                        break;
                    case ('semester'):
                        $subData['currentSemester']
                            = $this->getSemesterCourses($this->userData->getCurrentSemester());
                        $html = View::make('staff.lecturer.courses.viewCourse',
                            $subData);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        $data['pageTitle'] = "View Current Semester $category"
                            . "s";
                        break;
                    case ('departmental'):
                        $subData['departmentalCourses']
                            = $this->getDepartmentalCourses();
                        $html = View::make('staff.lecturer.courses.viewCourse',
                            $subData);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        $data['pageTitle'] = "Departmental $category" . "s";
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            case ('registration'):
                switch ($action) {
                    case ('view'):
                        $data['html'] = "$action</br>$category";
                        $data['activeNavbar'] = "courses";
                        $data['pageTitle'] = "View $category";
                        break;
                    case ('download'):
                        $data['html'] = "$action</br>$category";
                        $data['activeNavbar'] = "courses";
                        $data['pageTitle'] = "Download $category";
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            case ('results'):
                switch ($action) {
                    case ('view'):
                        $data['formAction']
                            = "staff/lecturer/courses/$category/$action";
                        $data['pageTitle'] = "View $category";
                        $data['sessions'] = $this->getSessions($category,
                            $action);
                        $data['courses'] = $this->getCourseCodes($category,
                            $action);
                        $html = View::make('staff.lecturer.courses.viewResult',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        break;
                    case ('download'):
                        $data['formAction']
                            = "staff/lecturer/courses/$category/$action";
                        $data['pageTitle'] = "Download $category";
                        $data['sessions'] = $this->getSessions($category,
                            $action);
                        $data['courses'] = $this->getCourseCodes($category,
                            $action);
                        $html
                            = View::make('staff.lecturer.courses.downloadResult',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        break;
                    case ('upload'):
                        $data['formAction']
                            = "staff/lecturer/courses/$category/$action";
                        $data['pageTitle'] = "Upload $category";
                        $data['sessions'] = $this->getSessions($category,
                            $action);
                        $data['courses'] = $this->getCourseCodes($category,
                            $action);
                        $html
                            = View::make('staff.lecturer.courses.uploadResult',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            default:
                return view('errors.404');
        }
        return view('staff.lecturer.courses', $data);
    }

    public function getSemesterCourses($semester)
    {
        $staff_id = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')->join('admin_access',
            'undergraduate_courses.system_id', '=', 'admin_access.system_id')
            ->join('users_staffs',
                function ($join) use ($staff_id) {
                    $join->on('users_staffs.staff_id', '=',
                        'admin_access.admin_id')
                        ->where('users_staffs.staff_id', '=', $staff_id);
                })
            ->where(
                [
                    [
                        'undergraduate_courses.semester',
                        $semester
                    ],
                    [
                        'undergraduate_courses.course_destination_school',
                        $school
                    ],
                    [
                        'undergraduate_courses.course_destination_dept',
                        $department
                    ]
                ])
            ->orderBy('undergraduate_courses.level', 'asc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->orderBy('undergraduate_courses.units', 'desc')
            ->get();
        return $courses;

    }

    private function getDepartmentalCourses()
    {
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')->where(
            [
                [
                    'undergraduate_courses.course_destination_school',
                    $school
                ],
                [
                    'undergraduate_courses.course_destination_dept',
                    $department
                ]
            ])
            ->orderBy('undergraduate_courses.level', 'asc')
            ->orderBy('undergraduate_courses.semester', 'asc')
            ->orderBy('undergraduate_courses.course_source', 'asc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->get();
        return $courses;


    }

    private function getSessions($category, $action)
    {
        $courses = $this->getCourseCodes($category, $action);

        $course = array();
        $dept = array();
        $i = 0;
        foreach ($courses as $code) {
            $courseCode = explode('_', $code->system_id);
            $course[$i] = $courseCode[0];
            $dept[$i] = $courseCode[3];
            $i++;
        }
        switch ($category) {
            case ('results'):
                switch ($action) {
                    case ('upload'):
                        $sessions = DB::table('undergraduate_results')->where([
                            [
                                'course_department',
                                Auth::user()->dept
                            ],
                            [
                                'status',
                                'Registered'
                            ],
                            [
                                'remarks',
                                'Registered'
                            ]
                        ])->whereIn('course_code', $course)
                            ->whereIn('student_dept', $dept)->select('session')
                            ->distinct()->orderBy('session', 'desc')->get();
                        break;
                    default:
                        $sessions = DB::table('undergraduate_results')
                            ->where('student_dept', Auth::user()->dept)
                            ->select('session')->distinct()
                            ->orderBy('session', 'desc')->get();
                        break;
                }
                break;
            default:
                $sessions = DB::table('undergraduate_results')
                    ->where('student_dept', Auth::user()->dept)
                    ->select('session')->distinct()->orderBy('session', 'desc')
                    ->get();
                break;
        }

        return $sessions;
    }

    private function getCourseCodes($category = null, $action = null)
    {
        $courseCodes = DB::table('admin_access')->where([
            [
                'admin_id',
                Auth::user()->name
            ],
            [
                'admin_function',
                'course_lecturer'
            ]
        ])->select('system_id')->orderBy('system_id')->get();
        switch ($category) {
            case ('results'):
                switch ($action) {
                    case ('upload'):
                        $course = array();
                        $dept = array();
                        $i = 0;
                        foreach ($courseCodes as $code) {
                            $courseCode = explode('_', $code->system_id);
                            $course[$i] = $courseCode[0];
                            $dept[$i] = $courseCode[3];
                            $i++;
                        }
                        $courseCodes = DB::table('undergraduate_results')
                            ->where([
                                [
                                    'course_department',
                                    Auth::user()->dept
                                ],
                                [
                                    'status',
                                    'Registered'
                                ],
                                [
                                    'remarks',
                                    'Registered'
                                ]
                            ])->whereIn('course_code', $course)
                            ->whereIn('student_dept', $dept)
                            ->select(DB::raw("concat(course_code,'_',course_department,'_','"
                                . Auth::user()->school
                                . "','_',student_dept) as system_id"))
                            ->distinct()->orderBy('system_id', 'desc')->get();
                        break;
                    default:
                        return $courseCodes;
                }
                break;
            default:
                return $courseCodes;
        }

        return $courseCodes;
    }

    public function request(
        $category = 'default',
        $action = 'default',
        Request $request
    ) {
        switch ($category) {
            case ('default'):
                $data['html'] = "$action</br>$category";
                $data['activeNavbar'] = "courses";
                $data['pageTitle'] = "Courses";

                return view('staff.lecturer.courses', $data);
                break;
            case ('attendance'):
                switch ($action) {
                    case ('view'):
                        $data['pageTitle'] = "View $category";
                        $html
                            = View::make('staff.lecturer.courses.viewAttendance',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";

                        return view('staff.lecturer.courses', $data);
                        break;
                    case ('download'):
                        $data['pageTitle'] = "Download $category";
                        $html
                            = View::make('staff.lecturer.courses.downloadAttendance',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";

                        return view('staff.lecturer.courses', $data);
                        break;
                    case ('upload'):
                        $data['pageTitle'] = "Upload $category";
                        $html
                            = View::make('staff.lecturer.courses.uploadAttendance',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";

                        return view('staff.lecturer.courses', $data);
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            case ('course'):
                switch ($action) {
                    case ('view'):
                        break;
                    case ('session'):
                        break;
                    case ('semester'):
                        break;
                    case ('search'):
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            case ('registration'):
                switch ($action) {
                    case ('view'):
                        break;
                    case ('download'):
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            case ('results'):
                switch ($action) {
                    case ('view'):
                        $session = $request->input('session', null);
                        $courseId = $request->input('courseCode', null);
                        $data['pageTitle'] = "View $category";
                        $data['formAction']
                            = "staff/lecturer/courses/$category/$action";
                        $data['sessions'] = $this->getSessions($category,
                            $action);
                        $data['courses'] = $this->getCourseCodes($category,
                            $action);
                        $data['resultSession'] = $session;
                        $data['resultCourse'] = $courseId;
                        $data['courseDetails']
                            = $this->getCourseDetails($courseId);
                        $data['results'] = $this->getResults($session,
                            $courseId);
                        $html = View::make('staff.lecturer.courses.viewResult',
                            $data);
                        $data['html'] = $html->render();
                        $data['activeNavbar'] = "courses";

                        return view('staff.lecturer.courses', $data);
                        break;
                    case ('download'):
                        $session = $request->input('session', null);
                        $systemId = $request->input('courseCode', null);
                        $format = $request->input('fileType', 'xlsx');
                        $this->downloadResult($session, $systemId, $format);
                        break;
                    case ('upload'):
                        if ($request->file('resultFile')->isValid()) {
                            //
                            $session = $request->input('session', null);
                            $systemId = $request->input('courseCode', null);
                            $resultFile = $request->file('resultFile');

                            $data['file'] = $request->file('resultFile');
                            $data['pageTitle'] = "Upload $category";
                            $data['formAction']
                                = "staff/lecturer/courses/$category/$action";
                            $data['sessions'] = $this->getSessions($category,
                                $action);
                            $data['courses'] = $this->getCourseCodes($category,
                                $action);
                            $html
                                = View::make('staff.lecturer.courses.uploadResult',
                                $data);
                            $data['html'] = $html->render();
                            $data['activeNavbar'] = "courses";
                            //$data['result']       = $this->uploadResult($session, $systemId, $resultFile);
                            $this->uploadResult($session, $systemId,
                                $resultFile);
                            //return view('staff.lecturer.courses', $data);
                            //
                        } else {
                            return redirect('/staff/lecturer/courses/results/upload');
                        }
                        break;
                    default:
                        return view('errors.404');
                }
                break;
            default:
                return view('errors.404');
        }
    }

    private function getCourseDetails($systemId)
    {
        $courseIds = $this->getCourseCodes();
        $valid = false;
        foreach ($courseIds as $id) {
            if ($id->system_id == $systemId) {
                $valid = true;
                break;
            }
        }
        if ($valid) {
            $courseDetails = DB::table('undergraduate_courses')
                ->where('system_id', $systemId)->first();
        } else {
            $courseDetails = array();
        }

        return $courseDetails;
    }

    private function getResults($session, $courseCode)
    {
        $courseCode = explode('_', $courseCode);
        $results = DB::table('undergraduate_results')
            ->leftJoin('users_undergraduates',
                'undergraduate_results.student_reg_no', '=',
                'users_undergraduates.reg_no')->where([
            [
                'undergraduate_results.session',
                $session
            ],
            [
                'undergraduate_results.course_code',
                $courseCode[0]
            ],
            [
                'undergraduate_results.student_dept',
                $courseCode[3]
            ]
        ])->select('undergraduate_results.*',
            DB::raw("concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"))
            ->orderBy('undergraduate_results.student_reg_no', 'asc')->get();
        return $results;
    }

    // drg >> get courses and course details

    private function downloadResult($session, $systemId, $format)
    {
        $courseDetails = $this->getCourseDetails($systemId);
        $results = $this->getResults($session, $systemId);

        switch ($format) {
            /*case ('pdf'):
            case ('excel'):*/
            default:
                $format = "xlsx";

                return Excel::create($courseDetails->course_title . '_'
                    . $session . '_' . ($session) . '_' . date('ynd'),
                    function ($excel) use ($courseDetails, $session, $results) {
                        // Set the title
                        $excel->setTitle($courseDetails->course_title
                            . ' results for ' . $session . '/' . ($session + 1)
                            . ' Academic Session');
                        $excel->setDescription('An auto generated document for '
                            . $courseDetails->course_title . ' results for '
                            . $session . '/' . ($session)
                            . ' Academic Session');

                        $excel->sheet("$courseDetails->course_code ($courseDetails->course_destination_dept)",
                            function ($sheet) use (
                                $results,
                                $courseDetails,
                                $session
                            ) {
                                $i = 2;
                                $a = $b = $c = $d = $e = $f = 0;
                                $sheet->setStyle(array(
                                    'font' => array(
                                        'name' => 'Calibri',
                                        'size' => 12
                                    )
                                ));
                                $sheet->setHeight(array(
                                    2 => 30,
                                    3 => 25
                                ));

                                $sheet->mergeCells("A$i:J$i");
                                $sheet->cell("A$i", function ($cell) {

                                    // manipulate the cell
                                    $cell->setValue("MAHADUM University Management System")
                                        ->setFont(array(
                                            'size' => '18',
                                            'bold' => true
                                        ))->setAlignment('center')
                                        ->setValignment('center');
                                });
                                $gdImage
                                    = imagecreatefrompng(public_path('images/global/_icon.png'));
                                // Add a drawing to the worksheetecho date('H:i:s') . " Add a drawing to the worksheet\n";
                                $objDrawing
                                    = new PHPExcel_Worksheet_MemoryDrawing();
                                $objDrawing->setName('Mahadum');
                                $objDrawing->setDescription('Mahadum');
                                $objDrawing->setImageResource($gdImage);
                                $objDrawing->setRenderingFunction(PHPExcel_Worksheet_MemoryDrawing::RENDERING_JPEG);
                                $objDrawing->setMimeType(PHPExcel_Worksheet_MemoryDrawing::MIMETYPE_DEFAULT);
                                $objDrawing->setHeight(70);
                                $objDrawing->setCoordinates('A2');
                                $objDrawing->setOffsetX(15);
                                $objDrawing->setWorksheet($sheet);
                                $sheet->prependRow($i, array());
                                $i += 2;
                                $sheet->mergeCells("A$i:J$i");
                                $sheet->cell("A$i", function ($cell) {

                                    // manipulate the cell
                                    $cell->setValue("Official Grade Report")
                                        ->setFontSize(14)->setFontWeight('bold')
                                        ->setAlignment('center')
                                        ->setValignment('center');
                                });
                                $i++;
                                $sheet->row($i, array());
                                $i++;
                                $details1 = array(
                                    'School of student: '
                                    . $courseDetails->course_destination_school,
                                    'Student Dept: '
                                    . $courseDetails->course_destination_dept,
                                    'Title of course: '
                                    . $courseDetails->course_title,
                                    'School offering course: '
                                    . Auth::user()->school
                                );
                                if ($courseDetails->semester == 1) {
                                    $semester = "Harmattan";
                                } elseif ($courseDetails->semester == 2) {
                                    $semester = "Rain";
                                }
                                $details2 = array(
                                    'Semester: ' . $semester,
                                    'Session: ' . $session . "/" . ($session
                                        + 1),
                                    'Course code: '
                                    . $courseDetails->course_code,
                                    'Units: ' . $courseDetails->units
                                );
                                for (
                                    $j = 0; $j < max([
                                    sizeof($details1),
                                    sizeof($details2)
                                ]); $i++, $j++
                                ) {
                                    $sheet->mergeCells("A$i:C$i");
                                    $sheet->cell("A$i", function ($cell) use (
                                        $details1,
                                        $details2,
                                        $j
                                    ) {
                                        // manipulate the cell
                                        $cell->setValue($details1[$j])
                                            ->setFontSize(12)
                                            ->setFontWeight('normal')
                                            ->setAlignment('left')
                                            ->setValignment('center');
                                    });
                                    $sheet->mergeCells("E$i:J$i");
                                    $sheet->cell("E$i", function ($cell) use (
                                        $details1,
                                        $details2,
                                        $j
                                    ) {
                                        // manipulate the cell
                                        $cell->setValue($details2[$j])
                                            ->setFontSize(12)
                                            ->setFontWeight('normal')
                                            ->setAlignment('left')
                                            ->setValignment('center');
                                    });
                                }
                                $sheet->row($i, array());
                                $i++;

                                // drg >> list out the column heading
                                $sheet->row($i, array(
                                    'S/No',
                                    'Names',
                                    'Reg. No.',
                                    'Dept.',
                                    'Test',
                                    'Lab',
                                    'Exam',
                                    'Total',
                                    'Grade',
                                    'Remark'
                                ));
                                $sheet->row($i, function ($row) {

                                    // call cell manipulation methods
                                    $row->setFontWeight('bold')
                                        ->setAlignment('center')
                                        ->setValignment('center');
                                });
                                $i++;
                                $j = 1;
                                $summaryRangeA = $i;
                                foreach ($results as $result) {
                                    if (is_null($result->result_lab)) {
                                        $score = $result->result_test
                                            + $result->result_exam;
                                    } else {
                                        $score = $result->result_test
                                            + $result->result_lab
                                            + $result->result_exam;
                                    }
                                    $sheet->row($i, array(
                                        $j,
                                        $result->name,
                                        $result->student_reg_no,
                                        $result->student_dept,
                                        $result->result_test,
                                        $result->result_lab,
                                        $result->result_exam,
                                        "=SUM(E$i:G$i)",
                                        "=IF(AND(NOT(H$i=" . '""' . "),H$i=0),"
                                        . '"F"' . ",IF(AND((H$i>=70),H$i<=100),"
                                        . '"A"' . ",(IF(AND(H$i>=60,H$i<=69),"
                                        . '"B"' . ",(IF(AND(H$i>=50,H$i<=59),"
                                        . '"C"' . ",(IF(AND(H$i>=45,H$i<=49),"
                                        . '"D"' . ",(IF(AND(H$i>0,H$i<=44),"
                                        . '"F"' . ",))))))))))",
                                        "=IF(NOT(I$i=" . '"F"), "PASS","FAIL")',
                                        $result->id + 37
                                    ));
                                    $i++;
                                    $j++;
                                }
                                $summaryRangeB = $i - 1;
                                $sheet->row($i, array());
                                $i++;
                                $sheet->row($i, array(
                                    '',
                                    'Grade',
                                    'Frequency'
                                ));
                                $sheet->cells("B$i:C$i", function ($cell) {
                                    // manipulate the cell
                                    $cell->setFontSize(12)
                                        ->setFontWeight('bold')
                                        ->setAlignment('Left')
                                        ->setValignment('center');
                                });
                                $i++;
                                $summaryStart = $i;
                                $summaryArray = [
                                    [
                                        '70% - 100%: A',
                                        "=COUNTIF(I$summaryRangeA:I$summaryRangeB,"
                                        . '"A")'
                                    ],
                                    [
                                        '60% - 69%: B',
                                        "=COUNTIF(I$summaryRangeA:I$summaryRangeB,"
                                        . '"B")'
                                    ],
                                    [
                                        '50% - 59%: C',
                                        "=COUNTIF(I$summaryRangeA:I$summaryRangeB,"
                                        . '"C")'
                                    ],
                                    [
                                        '45% - 49%: D',
                                        "=COUNTIF(I$summaryRangeA:I$summaryRangeB,"
                                        . '"D")'
                                    ],
                                    [
                                        '0% - 44%: F',
                                        "=COUNTIF(I$summaryRangeA:I$summaryRangeB,"
                                        . '"F")'
                                    ]
                                ];
                                for (
                                    $j = 0; $j < sizeof($summaryArray); $i++,
                                    $j++
                                ) {
                                    $sheet->cell("B$i", function ($cell) use (
                                        $summaryArray,
                                        $j
                                    ) {
                                        // manipulate the cell
                                        $cell->setValue($summaryArray[$j][0])
                                            ->setFontSize(12)
                                            ->setFontWeight('normal')
                                            ->setAlignment('left')
                                            ->setValignment('center');
                                    });
                                    $sheet->cell("C$i", function ($cell) use (
                                        $summaryArray,
                                        $j
                                    ) {
                                        // manipulate the cell
                                        $cell->setValue($summaryArray[$j][1])
                                            ->setFontSize(12)
                                            ->setFontWeight('normal')
                                            ->setAlignment('left')
                                            ->setValignment('center');
                                    });
                                }
                                $summaryEnd = $i;
                                // drg >> set document details
                                // drg >> document encryption algorithm with unique id
                                $uid = md5(Auth::user()->name
                                    . $courseDetails->course_destination_dept
                                    . $courseDetails->course_code . $session
                                    . $courseDetails->semester
                                    . $courseDetails->units . $summaryRangeA
                                    . $summaryRangeB);
                                $index = Auth::user()->id % 37;
                                $uid = substr(sha1($uid), $index, 4);
                                // drg >> [staff_id, department, school, course_code, session, semester, units, rows_begin,
                                // rows_end]
                                $sheetMetaArray = [
                                    Auth::user()->name,
                                    Auth::user()->dept,
                                    $courseDetails->course_code,
                                    $courseDetails->course_destination_dept,
                                    $courseDetails->course_destination_school,
                                    $session,
                                    $courseDetails->semester,
                                    $courseDetails->units,
                                    $summaryRangeA,
                                    $summaryRangeB,
                                    $uid
                                ];
                                $sheet->row(1, $sheetMetaArray);
                                // drg >> hide the first row and last column
                                $sheet->getRowDimension(1)->setVisible(false);
                                $sheet->getColumnDimension('K')
                                    ->setVisible(false);
                                // drg >> seting the borders
                                $sheet->setBorder('A' . ($summaryRangeA - 1)
                                    . ":J$summaryRangeB", 'thin');
                                // drg >> PROTECT with your password
                                $sheet->protect('tarzan');
                                // drg >> UNPROTECT the cell range
                                $sheet->getStyle("E$summaryRangeA:G$summaryRangeB")
                                    ->getProtection()
                                    ->setLocked(\PHPExcel_Style_Protection::PROTECTION_UNPROTECTED);
                                // drg >> make the formula for result computation hidden
                                $sheet->getStyle("H$summaryRangeA:J$summaryRangeB")
                                    ->getProtection()
                                    ->setHidden(\PHPExcel_Style_Protection::PROTECTION_PROTECTED);

                                $sheet->getStyle("C$summaryStart:C$summaryEnd")
                                    ->getProtection()
                                    ->setHidden(\PHPExcel_Style_Protection::PROTECTION_PROTECTED);
                                // drg >> PROTECT worksheet sheet
                                $sheet->getProtection()->setSheet(true);

                            });
                    })->download($format);
                break;
        }
    }

    private function uploadResult($session, $systemID, $resultFile)
    {
        $status = true;
        $objPHPExcel = \PHPExcel_IOFactory::load($resultFile);

        Excel::load($resultFile, function ($reader) use ($session, $systemID) {


            $sheet = $reader->getActiveSheet();

            // drg >> Get document details in format
            //[staff_id, department, course_code, CD_department, CD_school, session, semester,
            // units, rows_begin, rows_end, unique_id]
            // try {
            $documentMeta = [
                'staffID'                   => $sheet->getCell('A1')
                    ->getValue(),
                'staffDept'                 => $sheet->getCell('B1')
                    ->getValue(),
                'course_code'               => $sheet->getCell('C1')
                    ->getValue(),
                'course_destination_dept'   => $sheet->getCell('D1')
                    ->getValue(),
                'course_destination_school' => $sheet->getCell('E1')
                    ->getValue(),
                'session'                   => $sheet->getCell('F1')
                    ->getValue(),
                'semester'                  => $sheet->getCell('G1')
                    ->getValue(),
                'units'                     => $sheet->getCell('H1')
                    ->getValue(),
                'begin'                     => $sheet->getCell('I1')
                    ->getValue(),
                'end'                       => $sheet->getCell('J1')
                    ->getValue(),
                'uid'                       => $sheet->getCell('K1')->getValue()
            ];
            // course_code,'_',course_department,'_','" . Auth::user()->school . "','_',student_dept
            $documentID = $documentMeta['course_code'] . '_'
                . Auth::user()->dept . '_' . Auth::user()->school . '_'
                . $documentMeta['course_destination_dept'];

            $hasAccess = DB::table('admin_access')->where([
                [
                    'admin_id',
                    Auth::user()->name
                ],
                [
                    'admin_function',
                    'course_lecturer'
                ],
                [
                    'system_id',
                    $systemID
                ],
                [
                    'system_id',
                    $documentID
                ]
            ])->first();
            $isValid = DB::table('undergraduate_results')->where([
                [
                    'course_department',
                    Auth::user()->dept
                ],
                [
                    'status',
                    'Registered'
                ],
                [
                    'remarks',
                    'Registered'
                ],
                [
                    'course_code',
                    $documentMeta['course_code']
                ],
                [
                    'session',
                    $documentMeta['session']
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $documentMeta['semester']
                ],
                [
                    'student_dept',
                    $documentMeta['course_destination_dept']
                ]
            ])->first();
            if ($isValid && $hasAccess) {
                // drg >> check document encryption algorithm with unique id
                $uid = md5(Auth::user()->name
                    . $documentMeta['course_destination_dept']
                    . $documentMeta['course_code'] . $session
                    . $documentMeta['semester'] . $documentMeta['units']
                    . $documentMeta['begin'] . $documentMeta['end']);
                $index = Auth::user()->id % 37;
                $uid = substr(sha1($uid), $index, 4);
                if ($uid == $documentMeta['uid']) {
                    for (
                        $i = $documentMeta['begin']; $i <= $documentMeta['end'];
                        $i++
                    ) {
                        $reg_no = $sheet->getCell("C$i")->getValue();
                        $dept = $sheet->getCell("D$i")->getValue();
                        $test = $sheet->getCell("E$i")->getValue();
                        $lab = $sheet->getCell("F$i")->getValue();
                        $exam = $sheet->getCell("G$i")->getValue();
                        $id = $sheet->getCell("K$i")->getValue();
                        if (!(is_null($test) && is_null($lab)
                            && is_null($exam))
                        ) {
                            $res = DB::table('undergraduate_results')->where([
                                [
                                    'id',
                                    '=',
                                    ($id - 37)
                                ],
                                [
                                    'student_reg_no',
                                    '=',
                                    $reg_no
                                ],
                                [
                                    'student_dept',
                                    '=',
                                    $dept
                                ]
                            ])->update([
                                'result_test' => $test,
                                'result_lab'  => $lab,
                                'result_exam' => $exam,
                                'updated_at'  => now()
                            ]);
                            if ($res) {
                                echo 'good';
                            } else {
                                echo 'bad';
                            }
                            echo "$reg_no has been updated $test $lab $exam <br>";
                        } else {
                            echo "$reg_no was not updated<br>";
                        }
                    }
                } else {
                    echo "bake";
                } //throw new Exception('upload');
            } else {
                echo "brood";
            }//throw new Exception('upload');
            /*}catch (Exception $e){
                $subData['type'] = $e->getMessage();
                $html = View::make ( 'staff.lecturer.courses.error', $subData );
            }*/
            /*print_r($documentMeta);
            for ($i = 1; $i <= 11; $i++) {
                echo $sheet->getCellByColumnAndRow($i, 1)->getValue() . '<br>';
            }*/
        });
    }


}