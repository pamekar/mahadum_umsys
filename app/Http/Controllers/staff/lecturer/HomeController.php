<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Services\student\undergraduate\UndergraduateDataService;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $data_service;

    public function __construct(UndergraduateDataService $data_service)
    {

        $this->user_data = $data_service;

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user_type = Auth::user()->user_type;
        switch ($user_type) {
            case ('undergraduate') :
                $data = $this->user_data->getUndergraduateData();
                $data ['activeNavbar'] = "home";
                return view('student.undergraduate.home', $data);
                break;
            case ('postgraduate') :
                $data ['activeNavbar'] = "home";
                $data = $this->user_data->getPostgraduateData();
                return view('student.postgraduate.home', $data);
                break;
            case ('guardian') :
                $data ['activeNavbar'] = "home";
                $data = $this->user_data->getGuardianData();
                return view('guardian.home', $data);
                break;
            case ('staff') :
                $data ['activeNavbar'] = "home";
                $data = $this->user_data->getStaffData();
                return view('staff.home', $data);
                break;
            default :
                return view('auth.login');
                break;
        }

    }

    public function getEvents(Request $request)
    {
        $start = $request->input('start');
        $end = $request->input('end');
        $data = $this->user_data->getCalendar($start, $end);
        return $data;

    }

    public function getTimeTable(Request $request)
    {
        $start = $request->input('start');
        $end = $request->input('end');
        $data = $this->user_data->getTimeTable($start, $end);
        return $data;

    }

}
