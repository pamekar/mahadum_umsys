<?php

namespace App\Http\Controllers\staff\lecturer;

use App\Admin_access;
use App\Attendance;
use App\Services\staff\lecturer\LecturerDataService;
use App\Undergraduate_result;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class AttendanceController extends Controller
{
    //
    public function createAttendance(Request $request)
    {
        $system_id = $request->id;
        $course = explode('_', $system_id);
        $adminAccess = Admin_access::where('system_id', $system_id)
            ->where('admin_id', Auth::user()->name)->first();
        $hasOpenAttendance = Attendance::where('system_id', $system_id)
            ->where('signed_by', Auth::user()->name)->where('status', 'open')
            ->first();
        if ($adminAccess && !$hasOpenAttendance) {
            $attendance = [];
            $attendance_id = md5($system_id . str_random() . date('ymdHis'));
            $service = new LecturerDataService();
            $session = $service->getCurrentSession();
            $session = date('Y', strtotime($session->start));

            $students = Undergraduate_result::where('course_code', $course[0])
                ->where('course_department', $course[1])
                ->where('student_dept', $course[3])
                ->where('remarks', 'Registered')->get();

            // drg >> add array for attendance
            foreach ($students as $student) {
                array_push($attendance, [
                    'attendance_id' => $attendance_id,
                    'system_id'     => $system_id,
                    'signed_by'     => Auth::user()->name,
                    'signed_user'   => $student->student_reg_no,
                    'type'          => 'lecture',
                    'status'        => 'open',
                    'session'       => $session,
                ]);
            }

            DB::table('attendance')->insert($attendance);
            $status['type'] = "success";
            $status['message']
                = "You have opened a new attendance for $course[0]";
        } else {
            $status['type'] = "danger";
            $status['message'] = !$adminAccess
                ? "Oops an error occurred! You cannot create this attendance for $course[0]"
                : $hasOpenAttendance
                    ? "Oops an error occurred! You have an open attendance for $course[0]"
                    : null;
        }
        $request->session()->flash('notification', (object)$status);
        $data['courses'] = Admin_access::where('admin_id',
            Auth::user()->name)
            ->where('admin_function', 'course_lecturer')
            ->pluck('system_id');
        $data['attendances'] = $this->getAttendance();

        return view("staff.lecturer.attendance.create", $data);

    }

    public function getAttendance($id = null)
    {
        $attendance = [];
        if ($id) {
            echo 'boom';
        } else {
            $attendance = Attendance::where('signed_by', Auth::user()->name)
                ->select('attendance_id', 'system_id', 'status',
                    'session', 'created_at', DB::raw('count(*) as count'))
                ->orderBy('type')->orderBy('status')->orderBy('system_id')
                ->latest()
                ->groupBy('attendance_id')->get();
        }

        return $attendance;
    }

    public function getPage($action)
    {
        $data = [];
        switch ($action) {
            case 'create':
                $data['courses'] = Admin_access::where('admin_id',
                    Auth::user()->name)
                    ->where('admin_function', 'course_lecturer')
                    ->pluck('system_id');
                $data['attendances'] = $this->getAttendance();
                break;
            case 'update':
                $data['courses'] = Admin_access::where('admin_id',
                    Auth::user()->name)
                    ->where('admin_function', 'course_lecturer')
                    ->pluck('system_id');
                break;
            default:
                return view('errors.404');
                break;
        }
        $data['attendance'] = $this->getAttendance();

        return view("staff.lecturer.attendance.$action", $data);
    }

    public function viewAttendance($id)
    {
        $attendance = Attendance::where('attendance_id', $id)
            ->where('signed_by', Auth::user()->name)       ->select('attendance_id', 'system_id', 'status',
                'session', 'created_at', DB::raw('count(*) as count'))->groupBy('attendance_id')->first();
        if ($attendance) {
            $data['details'] = $attendance;
            $data['attendances'] = Attendance::join('users_undergraduates','attendance.signed_user','=','users_undergraduates.reg_no')->where('attendance_id', $id)
                ->where('signed_by', Auth::user()->name)
                ->select(DB::raw('concat(users_undergraduates.first_name," ",users_undergraduates.last_name) as student_name'),'attendance.id','attendance.attendance_id','attendance.signed_by','attendance.signed_user','attendance.remark','attendance.signed_at')->get();

            return view('staff.lecturer.attendance.view', $data);
        }
        return view('errors.404');
    }
}
