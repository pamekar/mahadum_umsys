<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use App\Services\student\undergraduate\HTMLService;
use PDF;
use App\Services\student\undergraduate\CourseService;
use App\Services\student\undergraduate\UndergraduateDataService;

class DownloadController extends Controller
{

    public function __construct(
        HTMLService $html,
        CourseService $course,
        UndergraduateDataService $user
    ) {

        $this->courses = $course;
        $this->html = $html;
        $this->userData = $user;

    }

    public function index()
    {
        // $data [ 'html' ] = $html;
        // $this->html->downloadResults ( $id, true );
        // return view ( 'pdfDownload', $data );
    }

    // drg >> to download result document
    public function result($session = null, $format = null)
    {
        if ($this->html->isValidSession($session)) {
            $html = $this->html->downloadResults($session, true);
            $html = '<style>'
                . file_get_contents(public_path('css/resultsDownload.css'))
                . '</style>' . $html;
            // set style for barcode
            $style = array(
                'border'        => true,
                'vpadding'      => 'auto',
                'hpadding'      => 'auto',
                'fgcolor'       => array(
                    0,
                    0,
                    0
                ),
                'bgcolor'       => false,  // array(255,255,255)
                'module_width'  => 1,  // width of a single module in points
                'module_height' => 1
            ); // height of a single module in points
            PDF::SetProtection(array(
                'copy',
                'modify',
                'extract',
                'assemble'
            ), '', null, 0, null);
            PDF::setPrintHeader(true);
            PDF::setPrintFooter(true);
            // drg >> custom header
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
            PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
            PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);
            PDF::setHeaderCallback(
                function ($pdf) {
                    $image_file
                        = file_get_contents(public_path(PDF_HEADER_LOGO)); // K_PATH_IMAGES . PDF_HEADER_LOGO;
                    $pdf->Image('@' . $image_file, 25, 10, 20, '', 'JPG',
                        url(''), 'T', false, 300, '', false, false, 0,
                        false, false, false);
                    // Set font
                    $pdf->SetFont('helvetica', 'B', 18);
                    // Title
                    // set cell padding
                    // $pdf->setCellPaddings(1, 1, 1, 1);
                    // set cell margins
                    $pdf->setCellMargins(5, 8, 1, 1);
                    $pdf->Cell(0, 15, 'MAHADUM University Management System', 0,
                        true, 'l', 0, '', 0,
                        false, 'M', 'M');
                });
            PDF::setFooterCallback(
                function ($pdf) {
                    // Position at 15 mm from bottom
                    // $pdf->SetY ( - 15 );
                    // Set font
                    $pdf->SetFont('helvetica', 'I', 8);
                    // Page number
                    $pdf->Cell(0, 10,
                        'page' . $pdf->getAliasNumPage() . '/'
                        . $pdf->getAliasNbPages(),
                        0, false, 'R', 0, '', 0, false, 'T', 'C');
                });
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::Cell(0);
            PDF::SetCreator(PDF_CREATOR);
            PDF::SetAuthor(PDF_AUTHOR);
            PDF::SetTitle("$session/" . ($session + 1)
                . " Academic Session Results - MAHADUM");
            PDF::SetSubject(Auth::user()->name);
            PDF::SetKeywords("Mahadum, Results, $session/" . ($session + 1)
                . "," . Auth::user()->name);
            // set image scale factor
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once(dirname(__FILE__) . '/lang/eng.php');
                PDF::setLanguageArray($l);
            }
            PDF::SetFont('dejavusans', '', 10);
            // add a page
            PDF::AddPage();
            // output the HTML content
            PDF::writeHTML($html, true, false, true, false, 'J');
            $index = Auth::user()->id % 37;
            $documentID = md5(Auth::user()->name . 'undergraduate_result'
                . $session);
            $documentID = substr(sha1($documentID), $index, 4);
            PDF::Text(157, 245, $documentID);
            PDF::write2DBarcode(
                url(
                    'verify/courses/result/' . $session . '/' . $documentID
                    . '/' .
                    Auth::user()->name), 'QRCODE,M', 150, 250, 25, 25,
                $style, 'M');
            PDF::lastPage();
            // ---------------------------------------------------------
            // Close and output PDF document
            PDF::Output("$session" . "_" . ($session + 1) . "_results_"
                . date('ynd') . ".pdf", 'I');
            // ============================================================+
            // END OF FILE
            // ============================================================+
        } else {
            return redirect('/undergraduate/courses/results');
        }

    }

    // drg >> to download course registration document
    public function registration($format = null)
    {
        // return view ( 'errors.601' );
        if ($this->courses->isRegistered()) {
            $session = $this->userData->getCurrentSession();
            $session = date('Y', strtotime($session->start));
            $html = $this->html->downloadCourseRegistration($session,
                $this->courses->getCourseRegistrationArray(true));
            $html = '<style>'
                . file_get_contents(public_path('css/resultsDownload.css'))
                . '</style>' . $html;
            // set style for barcode
            $style = array(
                'border'        => true,
                'vpadding'      => 'auto',
                'hpadding'      => 'auto',
                'fgcolor'       => array(
                    0,
                    0,
                    0
                ),
                'bgcolor'       => false,  // array(255,255,255)
                'module_width'  => 1,  // width of a single module in points
                'module_height' => 1
            ); // height of a single module in points
            PDF::SetProtection(array(
                'copy',
                'modify',
                'extract',
                'assemble'
            ), '', null, 0, null);
            PDF::setPrintHeader(true);
            PDF::setPrintFooter(true);
            // drg >> custom header
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::SetHeaderMargin(PDF_MARGIN_HEADER);
            PDF::SetFooterMargin(PDF_MARGIN_FOOTER);
            PDF::setImageScale(PDF_IMAGE_SCALE_RATIO);
            PDF::setHeaderCallback(
                function ($pdf) {
                    $image_file
                        = file_get_contents(public_path(PDF_HEADER_LOGO)); // K_PATH_IMAGES . PDF_HEADER_LOGO;
                    $pdf->Image('@' . $image_file, 25, 10, 20, '', 'JPG',
                        url(''), 'T', false, 300, '', false, false, 0,
                        false, false, false);
                    // Set font
                    $pdf->SetFont('helvetica', 'B', 18);
                    // Title
                    // set cell padding
                    // $pdf->setCellPaddings(1, 1, 1, 1);
                    // set cell margins
                    $pdf->setCellMargins(5, 8, 1, 1);
                    $pdf->Cell(0, 15, 'MAHADUM University Management System', 0,
                        true, 'l', 0, '', 0,
                        false, 'M', 'M');
                });
            PDF::setFooterCallback(
                function ($pdf) {
                    // Position at 15 mm from bottom
                    // $pdf->SetY ( - 15 );
                    // Set font
                    $pdf->SetFont('helvetica', 'I', 8);
                    // Page number
                    $pdf->Cell(0, 10,
                        'page' . $pdf->getAliasNumPage() . '/'
                        . $pdf->getAliasNbPages(),
                        0, false, 'R', 0, '', 0, false, 'T', 'C');
                });
            PDF::SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            PDF::Cell(0);
            PDF::SetCreator(PDF_CREATOR);
            PDF::SetAuthor(PDF_AUTHOR);
            PDF::SetTitle("$session/" . ($session + 1)
                . " Course Registration - MAHADUM");
            PDF::SetSubject(Auth::user()->name);
            PDF::SetKeywords(
                "Mahadum, course_registration, $session/" . ($session + 1) . ","
                . Auth::user()->name);
            // set image scale factor
            // set some language-dependent strings (optional)
            if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
                require_once(dirname(__FILE__) . '/lang/eng.php');
                PDF::setLanguageArray($l);
            }
            PDF::SetFont('dejavusans', '', 10);
            // add a page
            PDF::AddPage();
            // output the HTML content
            PDF::writeHTML($html, true, false, true, false, 'J');
            $index = Auth::user()->id % 37;
            $documentID = md5(Auth::user()->name
                . 'undergraduate_course_registration' . $session);
            $documentID = substr(sha1($documentID), $index, 4);
            PDF::Text(157, 245, $documentID);
            PDF::write2DBarcode(
                url(
                    'verify/course/register/' . $session . '/' . $documentID
                    . '/' .
                    Auth::user()->name), 'QRCODE,M', 150, 250, 25, 25,
                $style, 'M');
            PDF::lastPage();
            // ---------------------------------------------------------
            // Close and output PDF document
            PDF::Output("$session" . "_" . ($session + 1) . "_registration_"
                . date('ynd') . ".pdf", 'I');
            // ============================================================+
            // END OF FILE
            // ============================================================+
        } else {
            return redirect('/undergraduate/courses/register');
        }

    }

}
