<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

class LoginController extends Controller
{
    /*
     * |--------------------------------------------------------------------------
     * | Login Controller
     * |--------------------------------------------------------------------------
     * |
     * | This controller handles authenticating users for the application and
     * | redirecting them to your home screen. The controller uses a trait
     * | to conveniently provide its functionality to your applications.
     * |
     */
    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', [
            'except' => 'logout'
        ]);

    }

    protected function authenticated(Request $request, $user)
    {
        //
        $request->session()->flash('welcome',true);
    }

    public function checkUserType()
    {
        return true;

    }

    public function showLoginCategoryForm()
    {
        return view('auth.login_category');

    }

    public function redirectToCategory(Request $request)
    {
        $user_category = $request->input('user-category', 'User');
        switch ($user_category) {
            case ('undergraduate') :
                $data ['category'] = "Undergraduate";
                return redirect('login/undergraduate');
                break;
            case ('postgraduate') :
                $data ['category'] = "Postgraduate";
                return redirect('login/postgraduate');
                break;
            case ('guardian') :
                $data ['category'] = "Guardian";
                return redirect('login/guardian');
                break;
            case ('staff') :
                $data ['category'] = "Staff";
                return redirect('login/staff');
                break;
            default :
                $data ['error'] = "Please select a category";
                return view('auth.login_category', $data);
                break;
        }

    }

    public function showLoginForm($category)
    {
        switch ($category) {
            case ('undergraduate') :
                $data ['category'] = "Undergraduate";
                return view('auth.login', $data);
                break;
            case ('postgraduate') :
                $data ['category'] = "Postgraduate";
                return view('auth.login', $data);
                break;
            case ('guardian') :
                $data ['category'] = "Guardian";
                return view('auth.login', $data);
                break;
            case ('staff') :
                $data ['category'] = "Staff";
                return view('auth.login', $data);
                break;
            case ('User') :
                $data ['category'] = $category;
                return view('auth.login', $data);
                break;
            default :
                $data ['error'] = "Please select a category";
                return view('auth.login_category', $data);
                break;
        }

    }

    public function username()
    {
        return 'name';

    }

}
