<?php

namespace App\Http\Controllers;

use App\Http\Controllers\student\undergraduate\ChartController;
use App\Services\guardian\GuardianDataService;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use App\Services\student\undergraduate\UndergraduateDataService;
use App\Http\Controllers\Controller;
use App\Services\staff\lecturer\LecturerDataService;

class HomeController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    private $data_service;

    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        switch (Auth::user()->user_type) {
            case ('undergraduate') :
                $userData = new UndergraduateDataService ();
                $chart = new ChartController();

                $data['results'] = $chart->results();
                $data['attendance_present'] = $chart->attendance('present');
                $data['attendance_absent'] = $chart->attendance('absent');
                $data['payments'] = $chart->payments();
                $userData->setUndergraduateData($request);
                return view('student.undergraduate.home', $data);
                break;
            case ('postgraduate') :
                $data ['activeNavbar'] = "home";
                $data = $this->userData->getPostgraduateData();
                return view('student.postgraduate.home', $data);
                break;
            case ('guardian') :
                $userData = new GuardianDataService();
                if (!$userData->hasProfile()) {
                    return new Response(view('guardian.newProfile'), 401);
                }
                $data ['activeNavbar'] = "home";
                $userData->setGuardianData($request);
                return view('guardian.home', $data);
                break;
            case ('staff') :
                switch (Auth::user()->user_view) {
                    case ('admin') :
                        return redirect('/admin');
                        break;
                    case ('administrator') :
                        $data ['activeNavbar'] = "home";
                        $data = $this->administratorData->getStaffData();
                        return view('staff.administrator.home', $data);
                        break;
                    case ('lecturer') :
                        $userData = new LecturerDataService ();
                        $userData->setLecturerData($request);

                        return view('staff.lecturer.home');
                        break;
                }
                break;
            default :
                return view('auth.login');
                break;
        }

    }

    public function getEvents(Request $request)
    {
        switch (Auth::user()->user_type) {
            case ('undergraduate') :
                $userData = new UndergraduateDataService ();
                break;
            case ('postgraduate') :
                break;
            case ('guardian') :
                $userData = new GuardianDataService();
                break;
            case ('staff') :
                switch (Auth::user()->user_view) {
                    case ('admin') :
                        break;
                    case ('administrator') :
                        break;
                    case ('lecturer') :
                        $userData = new LecturerDataService ();
                        break;
                }
                break;
            default :
                return view('auth.login');
                break;
        }
        $start = $request->input('start');
        $end = $request->input('end');
        $data = $userData->getCalendar($start, $end);
        return $data;

    }

    public function getTimeTable(Request $request)
    {
        switch (Auth::user()->user_type) {
            case ('undergraduate') :
                $userData = new UndergraduateDataService ();
                break;
            case ('postgraduate') :
                break;
            case ('guardian') :
                break;
            case ('staff') :
                switch (Auth::user()->user_view) {
                    case ('admin') :
                        break;
                    case ('administrator') :
                        break;
                    case ('lecturer') :
                        $userData = new LecturerDataService ();
                        break;
                }
                break;
            default :
                return view('auth.login');
                break;
        }
        $start = $request->input('start');
        $end = $request->input('end');
        $data = $userData->getTimeTable($start, $end);
        return $data;

    }

    public function getUniqueId($user)
    {
        $uid = md5($user->name);
        $index = $user->id % 37;
        $uniqueID = substr(sha1($uid), $index, 4);
        return $uniqueID;
    }

}