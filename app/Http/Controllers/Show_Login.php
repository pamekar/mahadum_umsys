<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Show_Login extends Controller
{

    /**
     * Show the profile for the given user.
     *
     * @param int $id
     *
     * @return Response
     */
    public function __invoke(Request $request)
    {
        $user_category = $request->input('user-category');
        switch ($user_category) {
            case ('undergraduate') :
                $data ['category'] = "undergraduate";
                return view('student.undergraduate.login', $data);
                break;
            case ('postgraduate') :
                $data ['category'] = "postgraduate";
                return view('student.postgraduate.login', $data);
                break;
            case ('guardian') :
                $data ['category'] = "guardian";
                return view('guardian.login', $data);
                break;
            case ('staff') :
                $data ['category'] = "staff";
                return view('staff.login', $data);
                break;
            default :
                return view('auth.login');
                break;
        }

    }

}