<?php

namespace App\Http\Controllers\guardian;

use App\Http\Controllers\student\undergraduate\FeesController;
use App\Invoice_line;
use App\TimeTable;
use App\Services\guardian\GuardianDataService;
use App\Services\student\undergraduate\UndergraduateDataService;
use App\User;
use App\Users_guardian;
use App\Users_undergraduate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class GuardianController extends Controller
{
    //
    public function approve($action, $ward){
        switch($action){
            case 'accept':
                $wards= $this->getStatus('approved',$ward);

                dd($wards);
                break;
            case 'reject':

                break;
            case 'block':

                break;
        }
    }

    public function getStatus($action,$ward=null){
        $wards=Users_guardian::where('guardian_id',Auth::user()->name)->first();
        return $wards;
    }

    public function viewWard($id, Request $request)
    {
        $guardian = new GuardianDataService();
        $data = $guardian->getGuardianData();
        $data['result'] = $guardian->getResults($id);
        $data['invoices'] = $guardian->getInvoices($id);
        $data['user'] = $id;
        $ward = User::where('name', $id)->first();
        $chart = new ChartController();

        $data['results'] = $chart->results($ward);
        $data['attendance_present'] = $chart->attendance('present', $ward);
        $data['attendance_absent'] = $chart->attendance('absent', $ward);
        $data['payments'] = $chart->payments($ward);
        return view('guardian.ward', $data);
    }

    public function getTimeTable(
        $id,
        Request $request
    ) // drg>> function services JSON request
    {
        $start = date('l', strtotime($request->start));
        $end = date('l', strtotime($request->end));

        $userData = new UndergraduateDataService($id);
        $user = Users_undergraduate::where('reg_no', $id)->first();
        $start = date('l', strtotime($start));
        $end = date('l', strtotime($end));
        $level = $userData->getLevel();
        $timeTable = TimeTable::join('users_staffs', 'time_table.lecturer',
            '=',
            'users_staffs.staff_id')->where(
            [
                [
                    'time_table.week_day',
                    '=',
                    DB::raw("'" . $start . "'")
                ],
                [
                    'time_table.dept',
                    '=',
                    $user->dept
                ],
                [
                    'time_table.faculty',
                    '=',
                    $user->faculty
                ],
                [
                    'time_table.level',
                    '=',
                    $level
                ]
            ])
            ->select(
                DB::raw(
                    "concat(time_table.course_code,' Lecture\nLocation: ', time_table.location,'\nLecturer: ',
						users_staffs.first_name ,' ', users_staffs.last_name, '.\nDuration: ', time_table.start_time, ' to ',
						time_table.end_time ) as title, time_table.start_time as start, time_table.end_time as end"))
            ->get();
        return $timeTable;

    }

    public function paymentInvoice($id,Request $request)
    {
        $invoices = $request->invoices;
        $fees = new FeesController();
        $userData = new UndergraduateDataService($id);
        $payment
            = true; // drg >> flag to indicate that invoice is for a payment transaction
        $lines = Invoice_line::whereIn('invoice_id', $invoices)
            ->orderBy('invoice_id', 'desc')->orderBy('item', 'desc')->get();

        $amount = $fees->totalInvoice($invoices);

        $charge = $fees->calcTransactionCharge($amount);
        $transactionCharge = $charge < 2500 ? $charge : 2500;
        $invoice['invoices'] = $invoices;
        $invoice['student'] = $id;
        $invoice['amount'] = $amount * 100;
        $invoice['charge'] = $transactionCharge * 100;

        $request->session()->flash('invoice', (object)$invoice);
        $data = (object)['user_meta' => $userData->getUsersMeta()];
        //var_dump($data);exit;
        $results = View::make('student.undergraduate.partials.invoiceList',
            compact('lines', 'payment', 'data'));
        $html = $results->render();

        return response()->json([
            'html' => $html
        ]);
    }

    public function viewProfile(){
        return view('guardian.profile');
    }
}
