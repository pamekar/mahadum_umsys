<?php

namespace App\Http\Controllers\guardian;

use App\Services\guardian\GuardianDataService;
use App\User;
use App\Users_guardian;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\View;

class SettingsController extends Controller
{
    //
    public function profile(Request $request)
    {
        $guardian = Users_guardian::where('guardian_id', Auth::user()->name)
            ->first();
        $columns = ['first_name', 'last_name', 'gender', 'phone_no', 'address'];
        $values = $request->all($columns);
        $routeName = 'home';

        if ($guardian) {
            DB::table('users_guardians')
                ->where('guardian_id', Auth::user()->name)->update($values);
            $routeName = 'guardian.profile';
        } else {
            $values['guardian_id'] = Auth::user()->name;
            DB::table('users_guardians')->insert($values);
        }
        $status = [
            'type'    => 'Success',
            'message' => "Your profile has been updated."
        ];

        return redirect(route($routeName))->with('notification',
            (object)$status);
    }

    public function photo(Request $request)
    {
        $guardian = Users_guardian::where('guardian_id', Auth::user()->name)
            ->first();
        $user = User::find(Auth::user()->id);
        $status = [
            'type'    => 'Danger',
            'message' => "Oops! We couldn't process, please try again later."
        ];
        if ($request->hasFile('photo')
            && $request->file('photo')->isValid()
        ) {
            $location = $request->file('photo')
                ->store("greenwhitedev/mahadum_ums/mahadum/app/user/$user->name/photo");
            $location_url = Storage::url($location);
            $user->photo_location = $location_url;

            if ($guardian) {
                $guardianUpdate = Users_guardian::find($guardian->id);
                $guardianUpdate->photo_location = $location_url;
                $guardianUpdate->save();
            } else {
                $request->session()->put('photo_location', $location_url);
            }
            if ($user->save()) {
                $status = [
                    'type'    => 'Success',
                    'message' => 'Your photo has been uploaded'
                ];
            }
        }

        return response()->json($status);
    }

    public function ward($ward, $action, Request $request)
    {
        $state = $request->state;
        $status = [];

        $isValid = Schema::connection('mysql')
            ->hasColumns('users_guardians', [$action, $state]);
        if ($isValid) {
            $isWard = Users_guardian::where('guardian_id', Auth::user()->name)
                ->where($state, 'like', "%$ward%")->first()->toArray();

            if ($isWard) {
                $currentState = explode(';', $isWard["$state"]);
                $currentState = implode(';',
                    array_diff($currentState, [$ward]));
                $newState = explode(';', $isWard[$action]);
                array_push($newState, $ward);
                $newState = implode(';', $newState);
                //$newState = implode(';', $currentState);

                DB::table('users_guardians')
                    ->where('guardian_id', Auth::user()->name)
                    ->update([
                        $state       => $currentState,
                        $action      => $newState,
                        'updated_at' => Carbon::now()
                    ]);

                $guardian= new GuardianDataService();
                $wards = $guardian->getWards();
                $html = View::make('guardian.partials.wards', compact('wards'));
                $html = $html->render();
                $status = [
                    'type'    => 'success',
                    'message' => "Update was successful, $ward is now $action.",
                    'html'    => $html
                ];
            } else {
                $status = [
                    'type'    => 'danger',
                    'message' => "Oops! Your request has some errors, we couldnt identify the ward."
                ];
            }
        } else {
            $status = [
                'type'    => 'danger',
                'message' => "Oops! Your input was incorrect, try again."
            ];
        }


        return response()->json($status);

    }
}
