<?php

namespace App\Http\Controllers\guardian;

use App\Attendance;
use App\Invoice;
use App\Invoice_line;
use App\Payment;
use App\Services\staff\lecturer\LecturerDataService;
use App\Undergraduate_result;
use App\Users_undergraduate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    //
    public function results($ward)
    {
        $results = Undergraduate_result::where('student_reg_no',
            $ward->name)->select(
            DB::raw('count(result_grade) as count'))
            ->groupBy('result_grade')->orderBy('result_grade', 'desc')
            ->pluck('count');

        return $results;
    }

    public function attendance($remark,$ward)
    {
        $attendance = Attendance::where('signed_user', $ward->name)
            ->where('type', 'lecture')
            ->where('status', 'closed')->where('remark', $remark)
            ->select(DB::raw('count(remark) as count'))->groupBy('session')
            ->orderBy('session')->pluck('count');

        return $attendance;
    }


    public function payments($ward)
    {
        $target = [$ward->dept, $ward->school, $ward->name];
        $currentSession = setting('admin.current_session');
        $year_entry = Users_undergraduate::where('reg_no', $ward->name)
            ->value('year_entry');
        $invoice_id = [];

        $invoices = Invoice::where(function ($query) use (
            $target,
            $year_entry,
            $currentSession
        ) {
            foreach (range($year_entry, $currentSession) as $session) {
                $query->orWhere('session', $session)->whereIn('target', $target)
                    ->orWhere('type', 'university');
                $query->orWhere(function ($query) use (
                    $session,
                    $year_entry,
                    $currentSession
                ) {
                    $query->where('type', 'level')
                        ->where('session', $session)
                        ->where('target',
                            ($currentSession - $session + 1) * 100);
                });
            }
        })->pluck('invoice_id');

        $payment_received = Payment::whereIn('invoice_id',
            $invoices)->where('user_id', $ward->name)->sum('amount');
        $invoiceSum = Invoice_line::whereIn('invoice_id', $invoices)
            ->sum('amount');
        $payment_required = $invoiceSum;
        $paid = $payment_received/100;
        $debt = ($payment_required - $payment_received)/100;

        $payments = [$paid, $debt];


        return $payments;
    }
}
