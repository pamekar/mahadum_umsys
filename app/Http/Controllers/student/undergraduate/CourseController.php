<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Http\Controllers\Controller;
use App\Services\student\undergraduate\CourseService;
use App\Services\student\undergraduate\HTMLService;
use Illuminate\Http\Request;
use View;

class CourseController extends Controller
{

    public function __construct(CourseService $course, HTMLService $html)
    {

        $this->courses = $course;
        $this->html = $html;

    }

    // drg>> These are major functions (i.e. functions that are used outside the php class)
    public function index()
    {
        if ($this->courses->canRegister()) {
            if ($this->courses->isRegistered()) {
                return redirect('undergraduate/courses/register');
            } else {
                return redirect('undergraduate/courses/register');
            }
        } else {
            $data ['type'] = 'courses';
            // drg>> if otherwise
            $page = "default";
            $data ['canRegister'] = $this->courses->canRegister();
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = $this->courses->getPageTitle($page);
            $data ['page'] = $page;
            $data ['html'] = $this->courses->getHTML($page);
            return view('student.undergraduate.courses', $data);
        }

    }


    public function getJSON(Request $request)
    {
        $page = $request->input('action', 'level');
        $id = $request->input('id', 1);

        switch ($page) {
            case ('level') :
                if ($id <= 500 && $id >= 100) {
                    $html = $this->courses->showLevelCourses($id);
                } else {
                    $html = $this->courses->showLevelCourses();
                }
                break;
            case ('semester') :
                $html = $this->html->showSemesterCourses($id);
                break;
            case ('session') :
                $html = $this->courses->getResults($id);
                break;
            default :
                $html = $this->html->showCurrentLevelCourses();
                break;
        }
        return response()->json([
            'html' => $html
        ]);

    }

    public function getPage($page = 'default')
    {
        $data ['activeNavbar'] = "courses";
        $data ['pageTitle'] = $this->courses->getPageTitle($page);
        $data ['page'] = $page;
        if ($page == 'results') {
            $data ['type'] = 'results';
        } else {
            $data ['type'] = 'courses';
        }
        $data ['html'] = $this->courses->getHTML($page);
        return view('student.undergraduate.courses', $data);

    }

    public function getRegistrationPage($action = null)
    {
        // drg >> check if user can register courses
//        if ($this->courses->canRegister()) {
        $requestType = 'registered';
        $edit = false;
        if (isset ($action) && $action == 'edit'
            && $this->courses->userCanRegister()
            && $this->courses->canRegister()
        ) {
            $requestType = 'edit';
            $edit = true;
        }
        if ($this->courses->userCanRegister()
            && $this->courses->canRegister()
        ) {
            // drg >> check if user has registered any courses
            if ($this->courses->isRegistered()) {
                // drg >> Yes, user has registered some courses
                $page = 'register';
                $data ['activeNavbar'] = "courses";
                $data ['pageTitle'] = $this->courses->getPageTitle($page);
                $data ['page'] = $page;
                $data ['type'] = $page;
                $data ['edit'] = $edit;
                $data ['courses']
                    = $this->courses->getCourseRegistrationArray();
                $subData ['courses']
                    = $this->courses->getCourseRegistrationArray();
                $subData ['approved'] = $this->courses->isApproved();
                $subData ['html'] = $requestType;
                $subData['isRegistered'] = $this->courses->isRegistered();
                $subData ['userCanRegister']
                    = $this->courses->userCanRegister();
                $html = View::make('student.undergraduate.register',
                    $subData);
                $data ['html'] = $html->render();
                return view('student.undergraduate.courses', $data);
            } else {
                $page = 'register';
                $data ['activeNavbar'] = "courses";
                $data ['pageTitle'] = $this->courses->getPageTitle($page);
                $data ['page'] = $page;
                $data ['type'] = $page;
                $data ['courses']
                    = $this->courses->getCourseRegistrationArray();
                $subData ['courses']
                    = $this->courses->getCourseRegistrationArray();
                $subData ['html'] = $page;
                $subData['isRegistered'] = $this->courses->isRegistered();
                $subData ['userCanRegister']
                    = $this->courses->userCanRegister();
                $html = View::make('student.undergraduate.register',
                    $subData);
                $data ['html'] = $html->render();
                return view('student.undergraduate.courses', $data);
            }
        } else {
            $page = 'register';
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = $this->courses->getPageTitle($page);
            $data ['page'] = $page;
            $data ['type'] = $page;
            $data ['edit'] = $edit;
            $data ['courses']
                = $this->courses->getCourseRegistrationArray();
            $subData ['courses']
                = $this->courses->getCourseRegistrationArray();
            $subData ['approved'] = $this->courses->isApproved();
            $subData ['html'] = $requestType;
            $subData ['userCanRegister'] = $this->courses->userCanRegister()
                && $this->courses->canRegister();
            $subData['isRegistered'] = $this->courses->isRegistered();
            $html = View::make('student.undergraduate.register', $subData);
            $data ['html'] = $html->render();
            return view('student.undergraduate.courses', $data);
        }
//    }
//            return redirect('/undergraduate/courses');
    }

    public function getResults($session)
    {
        if ($this->html->isValidSession($session)) {
            $page = 'results';
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = $this->courses->getPageTitle($page);
            $data ['page'] = $page;
            $data ['type'] = $page;
            $data ['html'] = $this->html->showResults($session);
            return view('student.undergraduate.courses', $data);
        } else {
            return redirect('/undergraduate/courses/results');
        }

    }

    public function registerCourse(Request $request)
    {
        $verifyCourse = $this->courses->verifyCourses($request, 'insert');
        if ($verifyCourse [0]) {
            $page = "registered";
            $register = $this->courses->registerCourse($verifyCourse [1]);
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = $this->courses->getPageTitle($page);
            $data ['page'] = $page;
            $data ['type'] = $page;
            $data ['html'] = $register;
            return view('student.undergraduate.courses', $data);
            // return view ( 'errors.601' );
        } else {
            $data ['html'] = $this->html->getStatusMessage('VerifyRegistration',
                $verifyCourse [1],
                $verifyCourse [2]);
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = "Course Registration";
            return view('student.undergraduate/verified', $data);
        }
        // return view ( 'errors.601' );
    }

    public function updateCourse(Request $request)
    {
        $verifyCourse = $this->courses->verifyCourses($request, 'update');
        if ($verifyCourse [0]) {
            $page = "registered";
            $register = $this->courses->updateCourse($verifyCourse [1]);
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = $this->courses->getPageTitle($page);
            $data ['page'] = $page;
            $data ['type'] = $page;
            $data ['html'] = $register;
            return view('student.undergraduate.courses', $data);
            // return view ( 'errors.601' );
        } else {
            $data ['html'] = $this->html->getStatusMessage('VerifyRegistration',
                $verifyCourse [1],
                $verifyCourse [2]);
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = "Course Registration";
            return view('student.undergraduate/verified', $data);
        }
        // return view ( 'errors.601' );
    }

}
