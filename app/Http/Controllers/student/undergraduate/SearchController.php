<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Message;
use App\Notice;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class SearchController extends Controller
{
    //
    public function index(Request $request)
    {
        $query = $request->input('search','');

        $terms = preg_split('/[\s,;:]+/', $query);
        $terms = array_filter($terms);

        $data = $this->search($terms);
        $data['query'] = $query;
        return view('student.undergraduate.search', $data);
    }

    public function courses($terms)
    {
        $reg_no = Auth::user()->name;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        $courses = DB::table('undergraduate_courses')->leftJoin('admin_access',
            'undergraduate_courses.system_id', '=', 'admin_access.system_id')
            ->leftJoin('undergraduate_results',
                function ($join) use ($reg_no) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $reg_no);
                })
            ->leftJoin('users_staffs', 'users_staffs.staff_id', '=',
                'admin_access.admin_id')
            ->where(
                [
                    [
                        'undergraduate_courses.course_destination_school',
                        $school
                    ],
                    [
                        'undergraduate_courses.course_destination_dept',
                        $department
                    ]
                ])
            ->where(
                function ($query) use ($terms) {
                    for ($i = 0; $i < count($terms); $i++) {
                        $query->where(DB::raw("LOWER(undergraduate_courses.course_title)"),
                            'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(undergraduate_courses.course_code)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(undergraduate_courses.course_description)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(users_staffs.first_name)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(users_staffs.last_name)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"));
                    }
                })
            ->orderBy('undergraduate_courses.semester', 'asc')
            ->orderBy('undergraduate_courses.units', 'desc')
            ->orderBy('undergraduate_courses.course_code', 'asc')
            ->select('undergraduate_courses.*',
                DB::raw(
                    "concat(users_staffs.first_name,' ', users_staffs.last_name,' ', users_staffs.name_initials) as lecturer"),
                'undergraduate_results.remarks as status',
                'undergraduate_results.result_grade as grade')
            ->get();


        return $courses;
    }

    public function messages($terms)
    {
        $messages = Message::leftJoin('users_undergraduates', 'from', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'from', '=', 'staff_id')
            ->where(function ($query) {
                $query->where('to', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('cc', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('bcc', 'like', '%' . Auth::user()->name . '%');
            })
            ->where(
                function ($query) use ($terms) {
                    for ($i = 0; $i < count($terms); $i++) {
                        $query->where(DB::raw("LOWER(messages.subject)"),
                            'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(messages.message)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(users_staffs.first_name)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(users_staffs.last_name)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"));
                    }
                })
            ->where('messages.status', 'sent')
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->orderBy('messages.created_at', 'desc')->get();

        return $messages;
    }

    public function notices($terms, $limit = 10, $page = 1)
    {
        $results_per_page = $limit;
        $start_from = ($page - 1) * $results_per_page;
        $showing_from = $start_from + 1;
        $showing_to = $start_from + $results_per_page;
        // drg>> we get the category the user belongs in
        $type = Auth::user()->user_type;
        $school = Auth::user()->school;
        $department = Auth::user()->dept;
        // drg>> we then set the various system_ids for the notices
        $general_sid = "mums";
        $type_sid = $general_sid . "_" . $type;
        $school_sid = $type_sid . "_" . $school;
        $dept_sid = $school_sid . "_" . $department;
        // drg>> we then query the notice database table
        $notice = Notice::join('admin_access',
            function ($join) {
                $join->on('notices.system_id', '=', 'admin_access.system_id')
                    ->on('notices.announcer', '=',
                        'admin_access.admin_function');
            })->where(
            function ($query) use (
                $general_sid,
                $type_sid,
                $school_sid,
                $dept_sid
            ) {
                $query->where('notices.system_id', $general_sid)
                    ->orWhere('notices.system_id', $type_sid)
                    ->orWhere('notices.system_id', $school_sid)
                    ->orWhere('notices.system_id', $dept_sid);
            })
            ->where(
                function ($query) use ($terms) {
                    for ($i = 0; $i < count($terms); $i++) {
                        $query->where(DB::raw("LOWER(notices.title)"),
                            'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(notices.content)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"))
                            ->orWhere(DB::raw("LOWER(notices.announcer)"),
                                'like', DB::raw("LOWER('%$terms[$i]%')"));
                    }
                })
            ->latest()
            ->select(DB::raw('notices.*, admin_access.admin_title'))
            ->get();
        return $notice;

    }

    public function search($terms)
    {
        $data['courses'] = $this->courses($terms);
        $data['notices'] = $this->notices($terms);
        $data['messages'] = $this->messages($terms);

        return $data;
    }
}
