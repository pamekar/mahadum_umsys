<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Mail\Guardian\NewWard;
use App\User;
use App\Users_guardian;
use App\Users_undergraduate;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class SettingsController extends Controller
{
    public function changeGuardian(Request $request)
    {
        $name = $request->guardian;
        $isGuardian = Users_guardian::where('guardian_id', $name)->first();

        $type = "danger";
        $message = "";
        if ($isGuardian) {
            $cantRequest = Users_guardian::where('guardian_id',
                $isGuardian->guardian_id)
            ->where('blocked', 'like', '%'.Auth::user()->name.'%')->first();
            $doubleRequest = Users_guardian::where('guardian_id',
                $isGuardian->guardian_id)
                ->where(function ($query) {
                    $query->where('pending', 'like', '%'.Auth::user()->name.'%')
                        ->orWhere('blocked', 'like', '%'.Auth::user()->name.'%')
                        ->where('active', 'like', '%'.Auth::user()->name.'%');
                })->first();
            if (!$cantRequest && !$doubleRequest) {
                DB::table('users_undergraduates')
                    ->where('reg_no', Auth::user()->name)
                    ->update([
                        'guardian'   => $isGuardian->guardian_id,
                        'guardian_access'=>'statistics;fees',
                        'updated_at' => Carbon::now()
                    ]);

                $guardian = Users_guardian::find($isGuardian->id);

                $guardian->pending = isset($guardian->pending)
                    ? "$guardian->pending;" . Auth::user()->name
                    : ';' . Auth::user()->name;

                Mail::to($isGuardian->email)
                    ->send(new NewWard($isGuardian->name,
                        Auth::user()->name));
                $guardian->save();
                $type = 'success';
                $message
                    = 'Your request was submitted successfully. Awaiting guardian approval';
            } else {
                if ($cantRequest) {
                    $message = "Oops! You can't send a request to this user.";
                } elseif ($doubleRequest) {
                    $message
                        = "Oops! You still have a pending request for this user.";
                }
            }
        } else {
            $message = "Oops! We couldn't find your guardian.";
        }

        // drg >> register status of the request
        $status = [
            'type'    => $type,
            'message' => $message
        ];
        return response()->json($status);
    }

    public function updateGuardian(Request $request)
    {
        $access = $request->guardian_access;
        $id = session('userData')->user_meta->id;
        $user = Users_undergraduate::find($id);
        $user->guardian_access = implode(';', $access);
        if ($user->save()) {
            $status = [
                'type'    => 'success',
                'message' => "Update was successful"
            ];
        } else {
            $status = [
                'type'    => 'danger',
                'message' => 'We could not update your settings'
            ];
        }

        return response()->json($status);
    }
}
