<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Services\student\undergraduate\NoticeService;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NoticesController extends Controller
{

    public function __construct(NoticeService $service)
    {

        $this->notice = $service;

    }

    // drg>> These are major functions (i.e. functions that are used outside the php class)
    public function index(Request $request, $page = 1)
    {
        $limit = $request->input('limit',
            10); // drg >> this is to limit the number of results to be displayed
        $data ['notices'] = $this->notice->getNotices($limit, $page); //
        $data ['limit'] = $limit;
        $data ['page'] = $page;
        $data ['result_num'] = $this->notice->countNotices();
        $data ['activeNavbar'] = "notices";
        $user_type = Auth::user()->user_type;
        switch ($user_type) {
            case ('undergraduate') :
                return view('student.undergraduate.notices', $data);
                break;
            case ('postgraduate') :
                return view('student.undergraduate.notices', $data);
                break;
            case ('guardian') :
                return view('student.undergraduate.notices', $data);
                break;
            case ('staff') :
                return view('student.undergraduate.notices', $data);
                break;
            default :
                return view('auth.login');
                break;
        }

    }

    public function display($id)
    {
        $data ['notice'] = $this->notice->getNotice($id); //
        $data ['activeNavbar'] = "notices";
        $user_type = Auth::user()->user_type;
        switch ($user_type) {
            case ('undergraduate') :
                return view('student.undergraduate.notice', $data);
                break;
            case ('postgraduate') :
                return view('student.undergraduate.notice', $data);
                break;
            case ('guardian') :
                return view('student.undergraduate.notice', $data);
                break;
            case ('staff') :
                return view('student.undergraduate.notice', $data);
                break;
            default :
                return view('auth.login');
                break;
        }

    }
    // drg>> these are minor functions (i.e. functions that are used inside the php class)
}
