<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Invoice;
use App\Invoice_line;
use App\Payment;
use App\Users_undergraduate;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class FeesController extends Controller
{
    //
    public function index()
    {
        return $this->viewInvoices();
    }

    public function calcTransactionCharge($amount)
    {

        $waiver = 100;
        $siteCommision = 0;
        $gatewayCommission = 1;

        for (
            $charge = 0.015; $siteCommision < $gatewayCommission;
            $charge += 0.0000005
        ) {
            if ($amount < 2500) {
                $siteCommision = $amount * $charge;
                $gatewayCommission = ($siteCommision + $amount) * 0.015;
            } else {
                $siteCommision = $amount * $charge + $waiver;
                $gatewayCommission = ($siteCommision + $amount) * 0.015
                    + $waiver;
            }
            if ($siteCommision > 2500) {
                $siteCommision = 2500;
                break;
            }
        }

        return $siteCommision;
    }

    public function getOutstandingFees()
    {
        $user = Auth::user();
        $target = [$user->dept, $user->school, $user->name];
        $currentSession = setting('admin.current_session');
        $year_entry = Users_undergraduate::where('reg_no', $user->name)
            ->value('year_entry');
        $invoice_id = [];

        $invoices = Invoice::where(function ($query) use (
            $target,
            $year_entry,
            $currentSession
        ) {
            foreach (range($year_entry, $currentSession) as $session) {
                $query->orWhere('session', $session)->whereIn('target', $target)
                    ->orWhere('type', 'university');
                $query->orWhere(function ($query) use (
                    $session,
                    $year_entry,
                    $currentSession
                ) {
                    $query->where('type', 'level')
                        ->where('session', $session)
                        ->where('target',
                            ($currentSession - $session + 1) * 100);
                });
            }
        })->pluck('invoice_id');

        $payment_received = Payment::whereIn('invoice_id',
            $invoices)->where('user_id', $user->name)->sum('amount');
        $invoiceSum = Invoice_line::whereIn('invoice_id', $invoices)
            ->sum('amount');
        $payment_required = $invoiceSum;
        $outstanding = $payment_required - $payment_received;

        return $outstanding / 100;
    }

    public function paymentInvoice(Request $request)
    {
        $invoices = $request->invoices;
        $payment
            = true; // drg >> flag to indicate that invoice is for a payment transaction
        $lines = Invoice_line::whereIn('invoice_id', $invoices)
            ->orderBy('invoice_id', 'desc')->orderBy('item', 'desc')->get();

        $amount = $this->totalInvoice($invoices);

        $charge = $this->calcTransactionCharge($amount);
        $transactionCharge = $charge < 2500 ? $charge : 2500;

        $invoice['invoices'] = $invoices;
        $invoice['student'] = Auth::user()->name;
        $invoice['amount'] = $amount * 100;
        $invoice['charge'] = $transactionCharge * 100;

        $request->session()->flash('invoice', (object)$invoice);

        $results = View::make('student.undergraduate.partials.invoiceList',
            compact('lines', 'payment'));
        $html = $results->render();

        return response()->json([
            'html' => $html
        ]);
    }

    public function viewHistory()
    {
        $user = Auth::user();

        $chart = new ChartController();
        $fees = $chart->payments();

        $target = [$user->dept, $user->school, $user->name];
        $currentSession = setting('admin.current_session');
        $year_entry = Users_undergraduate::where('reg_no', $user->name)
            ->value('year_entry');

        $payments = Payment::join('invoices', 'payments.invoice_id', '=',
            'invoices.invoice_id')->where('payments.user_id', $user->name)
            ->where(function ($query) use (
                $target,
                $year_entry,
                $currentSession
            ) {
                foreach (range($year_entry, $currentSession) as $session) {
                    $query->orWhere('session', $session)
                        ->whereIn('target', $target)
                        ->orWhere('type', 'university');
                    $query->orWhere(function ($query) use (
                        $session,
                        $year_entry,
                        $currentSession
                    ) {
                        $query->where('type', 'level')
                            ->where('session', $session)
                            ->where('target',
                                ($currentSession - $session + 1) * 100);
                    });
                }
            })->select('invoices.title', 'invoices.session',
                'invoices.issuer', 'payments.id', 'payments.amount',
                'payments.created_at')
            ->orderBy('payments.created_at', 'desc')->get();

        $title = 'Payment History';

        return view('student.undergraduate.fees.history',
            compact('fees', 'payments', 'title'));
    }

    public function viewInvoice($id)
    {
        $invoice = Invoice::find($id - 331);

        $lines = Invoice_line::where('invoice_id', $invoice->invoice_id)->get();
        $payments = Payment::where('user_id', Auth::user()->name)
            ->pluck('invoice_id')
            ->toArray();

        $title = $invoice->title;
        return view('student.undergraduate.fees.invoice',
            compact('invoice', 'lines', 'payments', 'title'));
    }

    public function viewInvoices()
    {
        $user = Auth::user();

        $chart = new ChartController();
        $fees = $chart->payments();

        $target = [$user->dept, $user->school, $user->name];
        $currentSession = setting('admin.current_session');
        $year_entry = Users_undergraduate::where('reg_no', $user->name)
            ->value('year_entry');

        $invoices = Invoice::where(function ($query) use (
            $target,
            $year_entry,
            $currentSession
        ) {
            foreach (range($year_entry, $currentSession) as $session) {
                $query->orWhere('session', $session)->whereIn('target', $target)
                    ->orWhere('type', 'university');
                $query->orWhere(function ($query) use (
                    $session,
                    $year_entry,
                    $currentSession
                ) {
                    $query->where('type', 'level')
                        ->where('session', $session)
                        ->where('target',
                            ($currentSession - $session + 1) * 100);
                });
            }
        })->select('id', 'invoice_id', 'title', 'session', 'issuer')
            ->orderBy('created_at', 'desc')->get();
        $payments = Payment::where('user_id', $user->name)
            ->pluck('invoice_id')->toArray();

        $title = "Invoices";
        return view('student.undergraduate.fees.invoices',
            compact('fees', 'invoices', 'payments', 'title'));
    }

    public function viewPayment()
    {
        $user = Auth::user();

        $chart = new ChartController();
        $fees = $chart->payments();

        $target = [$user->dept, $user->school, $user->name];
        $currentSession = setting('admin.current_session');
        $year_entry = Users_undergraduate::where('reg_no', $user->name)
            ->value('year_entry');

        $payments = Payment::where('user_id', $user->name)
            ->pluck('invoice_id')->toArray();

        $invoices = Invoice::join('invoice_lines', 'invoices.invoice_id', '=',
            'invoice_lines.invoice_id')->where(function ($query) use (
            $target,
            $year_entry,
            $currentSession
        ) {
            foreach (range($year_entry, $currentSession) as $session) {
                $query->orWhere('invoices.session', $session)
                    ->whereIn('target', $target)
                    ->orWhere('invoices.type', 'university');
                $query->orWhere(function ($query) use (
                    $session,
                    $year_entry,
                    $currentSession
                ) {
                    $query->where('invoices.type', 'level')
                        ->where('invoices.session', $session)
                        ->where('invoices.target',
                            ($currentSession - $session + 1) * 100);
                });
            }
        })->whereNotIn('invoices.invoice_id', $payments)
            ->select('invoices.id', 'invoices.invoice_id', 'invoices.title',
                'invoices.session', 'invoices.issuer',
                DB::raw("SUM(invoice_lines.amount*invoice_lines.quantity) as amount"))
            ->groupBy('invoices.invoice_id')
            ->orderBy('invoices.created_at', 'desc')->get();

        $title = "Invoices";
        return view('student.undergraduate.fees.payment',
            compact('fees', 'invoices', 'payments', 'title'));
    }

    function totalInvoice($invoices)
    {
        $invoiceLines = is_array($invoices)
            ? Invoice_line::whereIn('invoice_id',
                $invoices)->get()
            : Invoice_line::where('invoice_id',
                $invoices)->get();
        $total = 0;

        foreach ($invoiceLines as $line) {
            $total += $line->amount * $line->quantity;
        }
        return $total / 100;

    }
}
