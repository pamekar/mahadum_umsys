<?php
// drg >>
namespace App\Http\Controllers\student\undergraduate;

use App\Http\Controllers\Controller;
use App\Services\student\undergraduate\HTMLService;
use Illuminate\Support\Facades\Auth;
use App\Services\student\undergraduate\CourseService;
use View;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class VerifyController extends Controller
{

    public function __construct(HTMLService $html, CourseService $course)
    {
        $this->html = $html;
        $this->courses = $course;

    }

    function getRequest($action, Request $request)
    {
        // $access = $this->getPermissions ( );
        $code = $request->input('code', null);
        $session = $request->input('session', null);
        $user = $request->input('user', null);
        switch ($action) {
            case ('result') :
                $data ['html'] = $this->verifyResult($code, $session, $user);
                if ($data ['html']) {
                    $data ['activeNavbar'] = "courses";
                    $data ['pageTitle'] = "Result Verification";
                    return view('student.undergraduate/verified', $data);
                    break;
                } else {
                    $data ['html']
                        = $this->html->getStatusMessage('VerifyDocument',
                        false);
                    $data ['activeNavbar'] = "courses";
                    $data ['pageTitle'] = "Result Verification";
                    return view('staff.lecturer.verification', $data);
                }
            case ('course') :
                break;
            case ('fees') :
                break;
            case ('student') :
                break;
            default :
                return view('errors.404');
        }

    }

    public function verifyResult($code, $resultSession = null, $user = null)
    {
        $verify = false;
        $data = "";
        $id = "";
        $regNo = Auth::user()->name;
        $documentID = md5($regNo . 'undergraduate_result' . $resultSession);
        $index = Auth::user()->id % 37;
        $documentID = substr(sha1($documentID), $index, 4);
        if ($code == $documentID) {
            $verify = true;
            $id = $resultSession;
        }
        if ($verify) {
            $subData ['firstSemesterResult']
                = $this->getSemesterResults($resultSession, 1);
            $subData ['secondSemesterResult']
                = $this->getSemesterResults($resultSession, 2);
            $subData ['firstGPA'] = $this->getGPA($resultSession, 1);
            $subData ['secondGPA'] = $this->getGPA($resultSession, 2);
            $subData ['sessionGPA'] = $this->getGPA($resultSession, null);
            $subData ['userDetails'] = $this->getUsersMeta();
            $html
                = View::make('student.undergraduate.verification.resultVerified',
                $subData);
            return $html->render();
        } else {
            $data ['html'] = $this->html->getStatusMessage('VerifyDocument',
                false);
            $data ['activeNavbar'] = "courses";
            $data ['pageTitle'] = "Result Verification";
            return view('student.undergraduate/verified', $data);
        }

    }

    public function getSemesterResults($session, $semester)
    {
        $regNo = Auth::user()->name;
        $results = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $regNo
                ],
                [
                    'session',
                    $session
                ],
                [
                    'semester',
                    $semester
                ]
            ])->where('status','Verified')
            ->orderBy('course_units', 'desc')
            ->orderBy('course_code', 'asc')
            ->get();
        return $results;

    }

    public function getGPA($session = null, $semester = null)
    {
        $regNo = Auth::user()->name;
        $cond1 = "Verified";
        $cond2 = "Corrected";
        if (isset ($semester)) {
            $semester = [
                $semester
            ];
        } else {
            $semester = [
                1,
                2
            ];
        }
        // drg>>to fetch date for last session and current session
        if (!isset ($session)) {
            $session = date('Y', strtotime($this->getsession()->start));
        }
        // drg >> to get the commulative cgpa
        $tgp = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $regNo
                ],
                [
                    'session',
                    $session
                ]
            ])
            ->whereIn('semester', $semester)
            ->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })->where('status','Verified')
            ->sum(DB::raw('result_grade*course_units'));
        $tnu = DB::table('undergraduate_results')->where(
            [
                [
                    'student_reg_no',
                    $regNo
                ],
                [
                    'session',
                    $session
                ]
            ])
            ->whereIn('semester', $semester)
            ->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })->where('status','Verified')
            ->sum('course_units');
        try {
            if ($tgp == 0 || $tnu == 0) {
                throw new Exception ();
            }
            $gp = $tgp / $tnu;
            $gpa = floor($gp * 100) / 100;
        } catch (Exception $e) {
            $gpa = 0;
        }
        return $gpa;

    }

    function getUsersMeta()
    {
        $regNo = Auth::user()->name;
        $usersMeta = DB::table('users_undergraduates')->where('reg_no', $regNo)
            ->first();
        return $usersMeta;

    }

    public function registration($session, $code, $user)
    {
        $verify = false;
        $data = "";
        $id = "";
        switch (Auth::user()->user_type) {
            case ('undergraduate') :
                $documentID = md5(Auth::user()->name
                    . 'undergraduate_course_registration' . $session);
                $index = Auth::user()->id % 37;
                $documentID = substr(sha1($documentID), $index, 4);
                if ($code == $documentID) {
                    $verify = true;
                    $id = $session;
                }
                if ($verify) {
                    $subData ['courses']
                        = $this->courses->getCourseRegistrationArray(true);
                    $subData ['approved'] = $this->courses->isApproved();
                    $subData ['html'] = "registered";
                    $subData ['userCanRegister']
                        = $this->courses->userCanRegister();
                    $html = View::make('student.undergraduate.register',
                        $subData);
                    $registration = $html->render();
                    $data ['html']
                        = $this->html->getStatusMessage('VerifyDocument', true)
                        . $registration;
                    $data ['activeNavbar'] = "courses";
                    $data ['pageTitle'] = "Registration Verification";
                    return view('student.undergraduate/verified', $data);
                } else {
                    $data ['html']
                        = $this->html->getStatusMessage('VerifyDocument',
                        false);
                    $data ['activeNavbar'] = "courses";
                    $data ['pageTitle'] = "Result Verification";
                    return view('student.undergraduate/verified', $data);
                }
                break;
            case ('staff') :
                break;
            default :
                return view('errors.404');
                break;
        }

    }

}