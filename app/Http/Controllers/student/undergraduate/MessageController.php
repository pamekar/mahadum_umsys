<?php

namespace App\Http\Controllers\student\undergraduate;

use App\Message;
use App\Services\student\undergraduate\NoticeService;
use App\Http\Controllers\Controller;
use App\User;
use App\users;
use App\Users_undergraduate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class MessageController extends Controller
{

    public function __construct()
    {

    }

    // drg>> These are major functions (i.e. functions that are used outside the php class)
    public function index($type = '', Request $request)
    {
        $request->session()->keep(['notification']);
        $messages = '';
        switch ($type) {
            case 'compose':
                return $this->viewComposeMessage();
                break;
            case 'draft':
                $messages = $this->getDraft();
                break;
            case 'inbox':
                $messages = $this->getInbox();
                break;
            case 'sent':
                $messages = $this->getSent();
                break;
            case 'starred':
                $messages = $this->getStarred();
                break;
            case 'trash':
                $messages = $this->getTrash();
                break;
            default:
                $messages = $this->getInbox();
                $type = 'inbox';
                break;
        }

        $data['contacts'] = $this->getRecentContacts();
        $data['messages'] = $messages;
        $data['title'] = $type;
        $data['count'] = $this->getCount();

        return view('student.undergraduate.messages.index', $data);
    }

    public function action($type = '', Request $request)
    {
        switch ($type) {
            case 'count':
                return response()->json($this->getCount());
                break;
            case 'selected':
                switch ($request->action) {
                    case 'star':
                        $selected = $request->all();
                        foreach ($selected as $id => $message_id) {
                            $message = Message::where('id', (int)$id - 120321)
                                ->where('message_id', $message_id)->first();
                            if ($message) {
                                $starredArray = explode(';', $message->starred);
                                if (in_array(Auth::user()->name,
                                    $starredArray)
                                ) {
                                    $index = array_search(Auth::user()->name,
                                        $starredArray);
                                    unset($starredArray[$index]);
                                } else {
                                    array_push($starredArray,
                                        ';' . Auth::user()->name);
                                }
                                $starred = implode(';',
                                    array_unique($starredArray));
                                $star = Message::find($message->id);
                                $star->starred = $starred;
                                $star->save();
                            }
                        }
                        $status = [
                            'type'    => 'success',
                            'message' => 'Your message(s) have been starred.'
                        ];

                        return redirect()->back()
                            ->with(['notification' => (object)$status]);
                        break;
                    case 'trash':
                        $selected = $request->all();
                        foreach ($selected as $id => $message_id) {
                            $message = Message::where('id', (int)$id - 120321)
                                ->where('message_id', $message_id)->first();
                            if ($message) {
                                $trashedArray = explode(';', $message->trashed);
                                if (in_array(Auth::user()->name,
                                    $trashedArray)
                                ) {
                                    $index = array_search(Auth::user()->name,
                                        $trashedArray);
                                    unset($trashedArray[$index]);
                                } else {
                                    array_push($trashedArray,
                                        ';' . Auth::user()->name);
                                }
                                $trashed = implode(';',
                                    array_unique($trashedArray));
                                $trash = Message::find($message->id);
                                $trash->trashed = $trashed;
                                $trash->save();
                            }
                        }
                        $status = [
                            'type'    => 'success',
                            'message' => 'Your message(s) have been moved to trash.'
                        ];

                        return redirect()->back()
                            ->with(['notification' => (object)$status]);
                        break;
                }
                break;
            case 'star':
                return $this->starMessage($request);
                break;
            case 'trash':
                return $this->trashMessage($request);
                break;
        }
    }

    public function countUnreadMessages($messages)
    {
        $count = 0;
        foreach ($messages as $message) {
            if (!in_array(Auth::user()->name, explode(';', $message->seen))) {
                $count++;
            }
        }
        return $count;
    }

    public function discardMessage(Request $request)
    {
        $status = [
            'type'    => 'danger',
            'message' => 'Your message has been discarded!'
        ];
        $request->session()->flash('notification', (object)$status);
        return response()->json($status);
    }

    public function getCount()
    {
        $data['inbox'] = $this->countUnreadMessages($this->getInbox());
        $data['draft'] = $this->countUnreadMessages($this->getDraft());
        $data['sent'] = $this->countUnreadMessages($this->getSent());
        $data['starred'] = $this->countUnreadMessages($this->getStarred());
        $data['trash'] = $this->countUnreadMessages($this->getTrash());

        return (object)$data;
    }

    public function getDestinations($string)
    {
        if (isset($string)) {
            $array = explode(',', $string);
            array_walk($array, function (&$value, $key) {
                $value = (int)$value - 37951;
            });

            $destArray = User::whereIn('id', $array)
                ->where('dept', Auth::user()->dept)->pluck('name')->toArray();

            $destinations = implode(',', $destArray);
        } else {
            $destinations = null;
        }
        return $destinations;
    }

    public function getDraft()
    {
        $sent = Message::leftJoin('users_undergraduates', 'to', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'to', '=', 'staff_id')
            ->where('from', Auth::user()->name)
            ->where('messages.status', 'draft')->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->orderBy('messages.created_at', 'desc')
            ->get();
        return $sent;
    }

    public function getInbox()
    {
        $inbox = Message::leftJoin('users_undergraduates', 'from', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'from', '=', 'staff_id')
            ->where(function ($query) {
                $query->where('to', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('cc', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('bcc', 'like', '%' . Auth::user()->name . '%');
            })
            ->where('messages.status', 'sent')
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->orderBy('messages.created_at', 'desc')->get();

        return $inbox;
    }

    public function getMessageInfo($message, $action)
    {
        $info = "";

        switch ($action) {
            case 'forward':
                $date = date('jS M, Y H:i:s', strtotime($message->created_at));
                $from = $this->getNames($message->from);
                $to = $this->getNames($message->to) ? "<br>To: "
                    . $this->getNames($message->to) : null;
                $cc = $this->getNames($message->cc) ? "<br>Cc: "
                    . $this->getNames($message->cc) : null;
                $bcc = $this->getNames($message->bcc) ? "<br>Bcc: "
                    . $this->getNames($message->bcc) : null;
                $heading
                    = "<br><br>
                        ---------- Forwarded message ----------<br>
                        From: $from<br>
                        Date: $date<br>
                        Subject: $message->subject
                        $to $cc $bcc 
                        <br>----------------------------------------<br>";
                $body = $message->message;

                $info = $heading . $body;
                break;
            case 'reply':
                $date = date('jS M, Y H:i:s', strtotime($message->created_at));
                $from = '"' . $this->getNames($message->from) . '"';
                $heading
                    = "<br><br>
                        ----------------------------------------<br>
                        On $date, $from wrote:<br><br>";
                $body = $message->message;
                $info = $heading . $body;
                break;
        }

        return $info;
    }

    public function getNames($names, $detail = false)
    {
        $nameArray = preg_split("/[\s,;]+/", $names);
        $getNames = User::leftJoin('users_undergraduates', 'name', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'name', '=', 'staff_id')
            ->whereIn('name', $nameArray)
            ->select('users.id', 'users.user_type',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'),
                DB::raw('IFNULL(users_undergraduates.photo_location, users_staffs.photo_location) as avatar'))
            ->get();

        if (isset($getNames)) {
            if ($detail) {
                $names = [];
                foreach ($getNames as $name) {
                    if (Auth::id() !== $name->id) {
                        array_push($names, [
                            'id'   => $name->id + 37951,
                            'type' => $name->user_type,
                            'name' => $name->name
                        ]);
                    }
                }
                return $names;
            } else {
                $names = "";
                foreach ($getNames as $name) {
                    $names .= "$name->name, ";
                }
                return $names;
            }
        }

        return null;
    }

    public function getRecentContacts()
    {
        $contacts = Message::leftJoin('users_undergraduates', 'from', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'from', '=', 'staff_id')
            ->where(function ($query) {
                $query->where('to', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('cc', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('bcc', 'like', '%' . Auth::user()->name . '%');
            })
            ->where('messages.status', 'sent')
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->select(
                DB::raw('IFNULL(users_undergraduates.user_type, users_staffs.user_type) as type'),
                DB::raw('IFNULL(users_undergraduates.photo_location, users_staffs.photo_location) as avatar'),
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->orderBy('messages.created_at', 'desc')
            ->limit(4)
            ->get();
        return $contacts;
    }

    public function getSent()
    {
        $sent = Message::leftJoin('users_undergraduates', 'to', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'to', '=', 'staff_id')
            ->where('from', Auth::user()->name)
            ->where('messages.status', 'sent')->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->orderBy('messages.created_at', 'desc')
            ->get();
        return $sent;
    }

    public function getStarred()
    {
        $starred = Message::leftJoin('users_undergraduates', 'from', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'from', '=', 'staff_id')
            ->where(function ($query) {
                $query->where('to', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('cc', 'like', '%' . Auth::user()->name . '%')
                    ->orWhere('bcc', 'like', '%' . Auth::user()->name . '%');
            })
            ->where('starred', 'like', '%' . Auth::user()->name . '%')
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->where('messages.status', 'sent')
            ->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->orderBy('messages.created_at', 'desc')->get();

        return $starred;
    }

    public function getTrash()
    {
        $trash = Message::leftJoin('users_undergraduates', 'from', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'to', '=', 'staff_id')
            ->where(function ($query) {
                $query->where('from', Auth::user()->name)->orWhere(function (
                    $query
                ) {
                    $query->where('to', 'like', '%' . Auth::user()->name . '%')
                        ->orWhere('cc', 'like', '%' . Auth::user()->name . '%')
                        ->orWhere('bcc', 'like',
                            '%' . Auth::user()->name . '%');
                    $query->where('messages.status', 'sent');
                });
            })
            ->where('trashed', 'like', '%' . Auth::user()->name . '%')
            ->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'))
            ->orderBy('messages.created_at', 'desc')
            ->get();
        return $trash;
    }

    public function saveMessage(Request $request)
    {
        $to = $this->getDestinations($request->to);
        $cc = $this->getDestinations($request->cc);
        $bcc = $this->getDestinations($request->bcc);
        $subject = $request->subject;
        $message = $request->message;
        $isSaved = isset($request->message_id);
        $message_id = $isSaved
            ? $request->message_id
            : md5($to . $cc . $bcc . date('Ymdhis')
                . str_random());
        if ($isSaved) {
            $saveMessage = Message::where('message_id', $message_id)
                ->where('messages.status', 'draft')->first();
            if ($saveMessage) {
                DB::table('messages')->where('message_id', $message_id)
                    ->update([
                        'subject'    => $subject,
                        'message'    => $message,
                        'from'       => Auth::user()->name,
                        'to'         => $to,
                        'cc'         => $cc,
                        'bcc'        => $bcc,
                        'messages.status'     => 'draft',
                        'created_at' => date('Y-m-d H:i:s'),
                        'updated_at' => date('Y-m-d H:i:s')
                    ]);
            } else {
                $status = [
                    'type'    => 'danger',
                    'message' => "Oops! There's a problem with your message"
                ];
                return response()->json($status, 500);
            }

        } else {
            DB::table('messages')->insert([
                'message_id' => $message_id,
                'subject'    => $subject,
                'message'    => $message,
                'from'       => Auth::user()->name,
                'to'         => $to,
                'cc'         => $cc,
                'bcc'        => $bcc,
                'messages.status'     => 'draft',
                'created_at' => date('Y-m-d H:i:s')
            ]);
        }

        $status = [
            'type'    => 'info',
            'message' => 'Your message has been saved to drafts!'
        ];
        $request->session()->flash('notification', (object)$status);
        return response()->json($status);
    }

    public function sendDraft(Request $request)
    {
        $to = $this->getDestinations($request->to);
        $cc = $this->getDestinations($request->cc);
        $bcc = $this->getDestinations($request->bcc);
        $subject = $request->subject;
        $message = $request->message;
        $message_id = $request->message_id;
        $isMessage = Message::where('message_id', $message_id)
            ->where('messages.status', 'draft')->first();

        if ($isMessage) {
            DB::table('messages')->where('message_id', $message_id)->update([
                'subject'    => $subject,
                'message'    => $message,
                'from'       => Auth::user()->name,
                'to'         => $to,
                'cc'         => $cc,
                'bcc'        => $bcc,
                'messages.status'     => 'sent',
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            ]);
        } else {
            $status = [
                'type'    => 'danger',
                'message' => "Oops! There's a problem with your message"
            ];
            return response()->json($status, 500);
        }
        $status = [
            'type'    => 'success',
            'message' => 'Your message has been sent!'
        ];
        $request->session()->flash('notification', (object)$status);
        return response()->json($status);
    }

    public function sendMessage(Request $request)
    {
        if (isset($request->message_id)) {
            return $this->sendDraft($request);
        }

        $to = $this->getDestinations($request->to);
        $cc = $this->getDestinations($request->cc);
        $bcc = $this->getDestinations($request->bcc);
        $subject = $request->subject;
        $message = $request->message;
        $message_id = md5($to . $cc . $bcc . date('Ymdhis') . str_random());
        DB::table('messages')->insert([
            'message_id' => $message_id,
            'subject'    => $subject,
            'message'    => $message,
            'from'       => Auth::user()->name,
            'to'         => $to,
            'cc'         => $cc,
            'bcc'        => $bcc,
            'messages.status'     => 'sent',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        $status = [
            'type'    => 'success',
            'message' => 'Your message has been sent!'
        ];
        $request->session()->flash('notification', (object)$status);
        return response()->json($status);
    }

    public function starMessage(Request $request)
    {
        $id = $request->id;
        $message = Message::where('message_id', $id)
            ->where('trashed', 'not like', '%' . Auth::user()->name . '%')
            ->first();
        if ($message) {
            $starredArray = explode(';', $message->starred);
            if (in_array(Auth::user()->name, $starredArray)) {
                $index = array_search(Auth::user()->name, $starredArray);
                unset($starredArray[$index]);
                $isStarred = false;
            } else {
                array_push($starredArray, ';' . Auth::user()->name);
                $isStarred = true;
            }
            $starred = implode(';', array_unique($starredArray));
            $star = Message::find($message->id);
            $star->starred = $starred;
            $star->save();
            return response()->json(['starred' => $isStarred], 200);
        }
        return response()->json(['messages.status' => 0], 500);
    }

    public function suggestNodes(Request $request)
    {
        $keyword = $request->input('keyword');
        Log::info($keyword);
        $nodes = User::leftJoin('users_undergraduates', 'name', '=', 'reg_no')
            ->leftJoin('users_staffs', 'name', '=', 'staff_id')
            ->where('users.dept', Auth::user()->dept)
            ->where(function ($query) use ($keyword) {
                $query->where('users_undergraduates.first_name', 'like',
                    '%' . $keyword . '%')
                    ->orWhere('users_undergraduates.last_name', 'like',
                        '%' . $keyword . '%')
                    ->orWhere('users_staffs.first_name', 'like',
                        '%' . $keyword . '%')
                    ->orWhere('users_staffs.last_name', 'like',
                        '%' . $keyword . '%');
            })
            ->select(DB::raw('users.id+37951 as id'),
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'),
                DB::raw('IFNULL(users_undergraduates.photo_location, users_staffs.photo_location) as avatar'),
                'users.user_type')
            ->get();
        return json_encode($nodes);
    }

    public function trashMessage(Request $request)
    {
        $id = $request->id;
        $message = Message::where('message_id', $id)->first();
        if ($message) {
            $trashedArray = explode(';', $message->trashed);
            if (in_array(Auth::user()->name, $trashedArray)) {
                $index = array_search(Auth::user()->name, $trashedArray);
                unset($trashedArray[$index]);
                $istrashed = false;
            } else {
                array_push($trashedArray, ';' . Auth::user()->name);
                $istrashed = true;
            }
            $trashed = implode(';', array_unique($trashedArray));
            $trash = Message::find($message->id);
            $trash->trashed = $trashed;
            $trash->save();
            return response()->json(['trashed' => $istrashed], 200);
        }
        return response()->json(['messages.status' => 0], 500);
    }

    public function viewComposeMessage()
    {
        $data['count'] = $this->getCount();
        $data['contacts'] = $this->getRecentContacts();
        $data['title'] = null;

        return view('student.undergraduate.messages.compose', $data);
    }

    public function viewForwardMessage($id)
    {
        $action = 'forward';
        $message = Message::where('message_id', $id)->first();

        if ($message) {
            $data['count'] = $this->getCount();
            $data['contacts'] = $this->getRecentContacts();
            $data['message'] = $this->getMessageInfo($message, $action);
            $data['subject'] = "Fwd: $message->subject";
            $data['action'] = $action;
            $data['title'] = $message->subject;

            return view('student.undergraduate.messages.compose', $data);
        }
        return redirect('/undergraduate/messages/compose');
    }

    public function viewMessage($id = null)
    {
        $message = Message::leftJoin('users_undergraduates', 'from', '=',
            'reg_no')
            ->leftJoin('users_staffs', 'from', '=', 'staff_id')
            ->where('message_id', $id)->select('messages.*',
                DB::raw('IFNULL(concat(users_undergraduates.first_name," ",users_undergraduates.last_name), concat(users_staffs.first_name," ",users_staffs.last_name)) as name'),
                DB::raw('IFNULL(users_undergraduates.photo_location, users_staffs.photo_location) as avatar'))
            ->first();

        $seenUsers = explode(';', $message->seen);
        array_push($seenUsers, Auth::user()->name);
        $seen = implode(';', array_unique($seenUsers));
        Message::where('message_id', $id)->update(['seen' => $seen]);

        $isDraft = $message->status == 'draft';
        $data['contacts'] = $this->getRecentContacts();
        $data['to'] = $this->getNames($message->to, $isDraft);
        $data['cc'] = $this->getNames($message->cc, $isDraft);
        $data['bcc'] = $this->getNames($message->bcc);
        $data['count'] = $this->getCount();
        $data['title'] = $message->subject;

        if ($isDraft) {
            $data['message'] = $message->message;
            $data['subject'] = $message->subject;
            $data['message_id'] = $message->message_id;
            return view('student.undergraduate.messages.compose', $data);
        }

        $data['message'] = $message;

        return view('student.undergraduate.messages.view', $data);
    }

    public function viewReplyAllMessage($id)
    {
        $action = 'reply';
        $message = Message::where('message_id', $id)->first();

        if ($message) {
            $data['count'] = $this->getCount();
            $data['contacts'] = $this->getRecentContacts();
            $data['message'] = $this->getMessageInfo($message, $action);
            $data['subject'] = "Re: $message->subject";
            $data['to'] = $this->getNames($message->from, true);
            $data['cc'] = $this->getNames("$message->cc;$message->to", true);
            $data['action'] = $action;
            $data['title'] = $message->subject;

            return view('student.undergraduate.messages.compose', $data);
        }
        return redirect('/undergraduate/messages/compose');
    }

    public function viewReplyMessage($id)
    {
        $action = 'reply';
        $message = Message::where('message_id', $id)->first();

        if ($message) {
            $data['count'] = $this->getCount();
            $data['contacts'] = $this->getRecentContacts();
            $data['message'] = $this->getMessageInfo($message, $action);
            $data['subject'] = "Re: $message->subject";
            $data['to'] = $this->getNames($message->from, true);
            $data['action'] = $action;
            $data['title'] = $message->subject;

            return view('student.undergraduate.messages.compose', $data);
        }
        return redirect('/undergraduate/messages/compose');
    }


}
