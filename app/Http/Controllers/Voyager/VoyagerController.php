<?php

namespace App\Http\Controllers\Voyager;

use App\Http\Controllers\Admin\ChartController;
use Khill\Lavacharts\Laravel\LavachartsFacade;
use TCG\Voyager\Facades\Voyager;
use TCG\Voyager\Http\Controllers\VoyagerController as BaseVoyagerController;

class VoyagerController extends BaseVoyagerController
{
    //
    public function index()
    {
        $chart = new ChartController();

        $data['result_2014'] = $chart->results(2014);
        $data['result_2015'] = $chart->results(2015);
        $data['result_2016'] = $chart->results(2016);
        $data['result_2017'] = $chart->results(2017);
        $data['attendance_present'] = $chart->attendance('present');
        $data['attendance_absent'] = $chart->attendance('absent');
        $data['students_gender']= $chart->students('gender');
        $data['students_faculty']=$chart->students('faculty');
        $data['students_year']=$chart->students('year');
        $data['payments']=$chart->payments();

        return Voyager::view('voyager::index', $data);
    }
}
