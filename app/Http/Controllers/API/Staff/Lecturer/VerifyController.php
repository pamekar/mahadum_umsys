<?php
// drg >>
namespace App\Http\Controllers\API\Staff\Lecturer;

use App\Http\Controllers\Controller;
use App\Undergraduate_results;
use App\Services\staff\lecturer\LecturerDataService;
use App\Services\student\undergraduate\HTMLService;
use App\Undergraduate_course;
use App\User;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\View;

class VerifyController extends Controller
{

    public function __construct(LecturerDataService $data, HTMLService $html)
    {
        $this->userData = $data;
        $this->html = $html;

    }

    public function index(
        $action,
        $code,
        $user,
        $session = null,
        Request $request
    ) {
        $name = $request->name;
        $verifiedBy = User::where('name', $name)->first();
        $data =[];
        switch ($action) {
            case ('result') : // drg >> verify RESULTS
                $data['html'] = $this->verifyResult($code, $session, $user,
                    $verifiedBy);
                $data['activeNavbar'] = "verification";
                $data['title'] = "Result Verified";
                break;
            case ('course') : // drg >> verify COURSE Registration
                $data['html'] = $this->verifyRegistration($code, $user,
                    $verifiedBy);
                $data['activeNavbar'] = "verification";
                $data['title'] = "Course Verified";
                break;
            case ('fees') :// drg >> verify FEES
                break;
            case ('student') :
                $data['html'] = $this->verifyStudent($code, $user,
                    $verifiedBy);
                $data['activeNavbar'] = "verification";
                $data['title'] = "Student Verified";
                break;
            case ('exam') :
                $courseid = $request->cid;
                $data['html'] = $this->verifyExam($code, $user, $courseid,
                    $verifiedBy);
                $data['activeNavbar'] = "verification";
                $data['title'] = "Student Verified";
                break;
            default :
                return response()->json(['error' => 'Oops! An error occured'],
                    404);
                break;
        }

        return response()->json($data, 200);

    }

    function getRequest($action, Request $request)
    {
        $code = $request->input('code', null);
        $session = $request->input('session', null);
        $user = $request->input('user', null);
        return $this->index($action, $code, $user, $session, $request);

    }

    function verifyResult(
        $code = null,
        $session = null,
        $student = null,
        $verifiedBy
    ) {
        $grantAccess = $this->getPermissions($verifiedBy);
        $verify = false;
        $data = "";
        $resultSession = "";
        $html = "";
        $regNo = null;
        $sessions = $this->getSessions($verifiedBy, false);
        if (isset ($code) && isset ($session) && isset ($student)) {
            $students = DB::table('users')->where(
              [
                  [
                        'name',
                        $student
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ]
                ])
                ->value('id');
            // drg >> encryption algorithm
            $uid = md5($student . 'undergraduate_result' . $session);
            $index = $students % 37;
            $uid = substr(sha1($uid), $index, 4);
            if ($code == $uid) {
                $verify = true;
                $regNo = $student;
                $resultSession = $session;
            }
        } elseif (isset ($code) && !isset ($session) && !isset ($student)) {

            $students = DB::table('users')->where(
              [
                  [
                        'school',
                        $verifiedBy->school
                    ],
                  [
                        'dept',
                        $verifiedBy->dept
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ]
                ])
                ->select('id', 'name')
                ->get();
            foreach ($sessions as $session) {
                foreach ($students as $student) {
                    $uid = md5($student->name . 'undergraduate_result'
                        . $session->session);
                    $index = $student->id % 37;
                    $uid = substr(sha1($uid), $index, 4);
                    if ($code == $uid) {
                        $verify = true;
                        $regNo = $student->name;
                        $resultSession = $session->session;
                        break;
                    }
                }
                if ($verify) {
                    break;
                }
            }
        } elseif (!isset ($code) && isset ($session) && isset ($student)) {
            $students = DB::table('users')->where(
              [
                  [
                        'name',
                        $student
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ]
                ])
                ->get();
            if (sizeof($students) > 0) {
                $regNo = $student;
                $resultSession = $session;
                $verify = true;
            }
        }
        if ($verify) {
            $details = $this->getUndergraduateUsersMeta($regNo);
            $level = $this->getUndergraduateLevel(false, $regNo);
            if ((isset ($grantAccess['hod'])
                    && $grantAccess['hod']['school'] == $details->faculty
                    && $grantAccess['hod']['department'] == $details->dept)
                || (isset (
                        $grantAccess['courseadviser'])
                    && $grantAccess['courseadviser']['school']
                    == $details->faculty
                    && $grantAccess['courseadviser']['department']
                    == $details->dept
                    && $grantAccess['courseadviser']['level'] == $level)
            ) {
                $subData['firstSemesterResult']
                    = $this->getUndergraduateSemesterResults($regNo,
                    $resultSession,
                    1);
                $subData['secondSemesterResult']
                    = $this->getUndergraduateSemesterResults($regNo,
                    $resultSession, 2);
                $subData['firstGPA'] = $this->getUndergraduateGPA($regNo,
                    $resultSession, 1);
                $subData['secondGPA'] = $this->getUndergraduateGPA($regNo,
                    $resultSession, 2);
                $subData['sessionGPA'] = $this->getUndergraduateGPA($regNo,
                    $resultSession);
                $subData['cgpa'] = $this->getUndergraduateGPA($regNo);
                $subData['psgpa'] = $this->getUndergraduateGPA($regNo,
                    $resultSession - 1);
                $subData['userDetails'] = $details;
                $subData['level'] = $level;
                $subData['session'] = $resultSession;
                $subData['grantAccess'] = 'ca_hod';
                $html = View::make('staff.lecturer.verification.result',
                    $subData);
            } elseif (isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $courseResults = $this->getUndergraduateCourseResults($regNo,
                    $resultSession, $courseCodes,
                    $sourceDepartments, $destDepartments);
                if (sizeof($courseResults) > 0) {
                    $subData['userDetails'] = $details;
                    $subData['level'] = $level;
                    $subData['courseResults'] = $courseResults;
                    $subData['grantAccess'] = 'courselecturer';
                    $subData['session'] = $resultSession;
                    $html = View::make('staff.lecturer.verification.result',
                        $subData);
                } else {
                    $subData['grantAccess'] = false;
                    $subData['errorState'] = 1;
                    $html = View::make('staff.lecturer.verification.result',
                        $subData);
                }
            } else {
                $subData['grantAccess'] = false;
                $subData['errorState'] = 1;
                $html = View::make('staff.lecturer.verification.result',
                    $subData);
            }
        } else {
            $subData['grantAccess'] = false;
            $subData['errorState'] = 1;
            $html = View::make('staff.lecturer.verification.result', $subData);
        }
        return $html->render();

    }

    function getPermissions($verifiedBy)
    {
        $staffID = $verifiedBy->name;
        $array = array();
        $isCourseAdviser = DB::table('admin_access')->where(
          [
              [
                    'admin_id',
                    $staffID
                ],
              [
                    'admin_function',
                    'course_adviser'
                ]
            ])
            ->value('system_id');
        $isHOD = DB::table('admin_access')->where(
          [
              [
                    'admin_id',
                    $staffID
                ],
              [
                    'admin_function',
                    'head_of_department'
                ]
            ])
            ->value('system_id');
        $isLecturer = DB::table('admin_access')->where(
          [
              [
                    'admin_id',
                    $staffID
                ],
              [
                    'admin_function',
                    'lecturer'
                ]
            ])
            ->value('system_id');
        $isCourseLecturer = DB::table('admin_access')->where(
          [
              [
                    'admin_id',
                    $staffID
                ],
              [
                    'admin_function',
                    'course_lecturer'
                ]
            ])
            ->select('system_id')
            ->get();
        if ($isCourseAdviser) {
            // drg >> will have access privileges granted for only departmental and level access
            $param = explode("_", $isCourseAdviser);
            $currentSession = $this->userData->getCurrentSession();
            $currentYr = date('Y', strtotime($currentSession->start));
            $level = $currentYr - $param[1] + 1;
            $level = $level * 100;
            $array['courseadviser'] = array(
                'department' => $param[3],
                'school'     => $param[2],
                'year'       => $param[1],
                'level'      => $level
            );
        }
        if ($isLecturer) {
            // drg >> will have lower access privileges
            $param = explode("_", $isLecturer);
            $array['lecturer'] = array(
                'school'     => $param[1],
                'department' => $param[2]
            );
        }
        if ($isCourseLecturer) {
            // drg >> access privileges will be limited to course being handled
            $array['courselecturer'] = array(
                'courses' => $isCourseLecturer
            );
        }
        if ($isHOD) {
            // drg >> will have super access privilegees
            $param = explode("_", $isHOD);
            $array['hod'] = array(
                'school'     => $param[1],
                'department' => $param[2]
            );
        }
        return $array;

    }

    private function getSessions($verifiedBy, $latest)
    {
        if ($latest) {
            $sessions = DB::table('undergraduate_results')
                ->where('student_dept', $verifiedBy->dept)
                ->max('session');
        } else {
            $sessions = DB::table('undergraduate_results')
                ->where('student_dept', $verifiedBy->dept)
                ->select('session')
                ->distinct()
                ->orderBy('session', 'desc')
                ->get();
        }
        return $sessions;

    }

    function getUndergraduateUsersMeta($regNo)
    {
        $usersMeta = DB::table('users_undergraduates')->where('reg_no', $regNo)
            ->first();
        return $usersMeta;

    }

    public function getUndergraduateLevel($exact = false, $regNo)
    {
        $lastSession = $this->userData->getLastSession();
        $currentSession = $this->userData->getCurrentSession();
        // drg>>to fetch date for last session and current session
        $lastYr = date('Y', strtotime($lastSession->start));
        $currentYr = date('Y', strtotime($currentSession->start));
        $userMeta = $this->getUndergraduateUsersMeta($regNo);
        $courseDuration = $userMeta->course_duration * 100;
        $yearsInSchool = $currentYr - ($userMeta->year_entry) + 1;
        if ($userMeta->mode_of_entry == "DIRECT") {
            $level = ($yearsInSchool + 1) * 100;
        } else {
            $level = $yearsInSchool * 100;
        }
        // drg>> to check if user has passed graduation year
        if (!$exact && $level > $courseDuration) {
            $level = $courseDuration;
        }
        return $level;

    }

    private function getUndergraduateSemesterResults(
        $regNo,
        $session,
        $semester
    ) {
        $results = DB::table('undergraduate_results')->where(
          [
              [
                    'student_reg_no',
                    $regNo
                ],
              [
                    'session',
                    $session
                ],
              [
                    'semester',
                    $semester
                ]
            ])
            ->orderBy('course_units', 'desc')
            ->orderBy('course_code', 'asc')
            ->get();
        return $results;

    }

    public function getUndergraduateGPA(
        $regNo,
        $session = null,
        $semester = null
    ) {
        $cond1 = "Verified";
        $cond2 = "Corrected";
        if (isset ($semester)) {
            $semester =[
                $semester
            ];
        } else {
            $semester =[
                1,
                2
            ];
        }
        if (isset ($session)) {
            $tgp = DB::table('undergraduate_results')->where(
              [
                  [
                        'student_reg_no',
                        $regNo
                    ],
                  [
                        'session',
                        $session
                    ]
                ])
                ->whereIn('semester', $semester)
                ->where(
                    function ($query) use ($cond1, $cond2) {
                        $query->where('status', $cond1)
                            ->orWhere('status', $cond2);
                    })
                ->sum(DB::raw('result_grade*course_units'));
            $tnu = DB::table('undergraduate_results')->where(
              [
                  [
                        'student_reg_no',
                        $regNo
                    ],
                  [
                        'session',
                        $session
                    ]
                ])
                ->whereIn('semester', $semester)
                ->where(
                    function ($query) use ($cond1, $cond2) {
                        $query->where('status', $cond1)
                            ->orWhere('status', $cond2);
                    })
                ->sum('course_units');
        } else {
            $tgp = DB::table('undergraduate_results')->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })
                ->where('student_reg_no', $regNo)
                ->sum(DB::raw('result_grade * course_units'));
            // drg >> to get the total number of units
            $tnu = DB::table('undergraduate_results')->where(
                function ($query) use ($cond1, $cond2) {
                    $query->where('status', $cond1)
                        ->orWhere('status', $cond2);
                })
                ->where('student_reg_no', $regNo)
                ->sum('course_units');
        }
        try {
            if ($tgp == 0 || $tnu == 0) {
                throw new Exception ();
            }
            $gp = $tgp / $tnu;
            $gpa = floor($gp * 100) / 100;
        } catch (Exception $e) {
            $gpa = null;
        }
        return $gpa;

    }

    private function getUndergraduateCourseResults(
        $regNo,
        $session,
        $courseCodes,
        $sourceDepartments,
        $destDepartments
    ) {
        $results = DB::table('undergraduate_results')->where(
          [
              [
                    'student_reg_no',
                    $regNo
                ],
              [
                    'session',
                    $session
                ]
            ])
            ->whereIn('student_dept', $destDepartments)
            ->whereIn('course_department', $sourceDepartments)
            ->whereIn('course_code', $courseCodes)
            ->orderBy('course_units', 'desc')
            ->orderBy('course_code', 'asc')
            ->get();
        return $results;

    }

    function verifyRegistration($code = null, $student = null, $verifiedBy)
    {
        $grantAccess = $this->getPermissions($verifiedBy);
        $verify = false;
        $regNo = null;
        $session = $this->userData->getCurrentSession();
        $session = date('Y', strtotime($session->start));
        if (isset ($code) && isset ($student)) {
            $id = DB::table('users')->where(
              [
                  [
                        'user_type',
                        'undergraduate'
                    ],
                  [
                        'name',
                        $student
                    ]
                ])
                ->value('id');
            $uid = md5($student . 'undergraduate_course_registration'
                . $session);
            $index = $id % 37;
            $uid = substr(sha1($uid), $index, 4);
            if ($code == $uid) {
                $verify = true;
                $regNo = $student;
            }
        } elseif (isset ($code) && !isset ($student)) {
            $students = DB::table('users')->where('user_type', 'undergraduate')
                ->select('id', 'name')
                ->get();
            foreach ($students as $student) {
                $uid = md5($student->name . 'undergraduate_course_registration'
                    . $session);
                $index = $student->id % 37;
                $uid = substr(sha1($uid), $index, 4);
                if ($code == $uid) {
                    $verify = true;
                    $regNo = $student->name;
                    break;
                }
            }
        } elseif (!isset ($code) && isset ($student)) {
            $students = DB::table('users')->where(
              [
                  [
                        'name',
                        $student
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ]
                ])
                ->get();
            if (sizeof($students) > 0) {
                $regNo = $student;
                $session = $session;
                $verify = true;
            }
        }
        if ($verify) {
            $details = $this->getUndergraduateUsersMeta($regNo);
            $level = $this->getUndergraduateLevel(false, $regNo);
            if ((isset ($grantAccess['hod'])
                    && $grantAccess['hod']['school'] == $details->faculty
                    && $grantAccess['hod']['department'] == $details->dept)
                || (isset (
                        $grantAccess['courseadviser'])
                    && $grantAccess['courseadviser']['school']
                    == $details->faculty
                    && $grantAccess['courseadviser']['department']
                    == $details->dept
                    && $grantAccess['courseadviser']['level'] == $level)
            ) {
                $subData['courses']
                    = $this->getCourseRegistrationArray($regNo);
                $subData['userDetails'] = $details;
                $subData['session'] = $session;
                $subData['level'] = $level;
                $html = View::make('staff.lecturer.verification.register',
                    $subData);
            } elseif (isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $courseResults = $this->getUndergraduateCourseResults($regNo,
                    $session, $courseCodes,
                    $sourceDepartments, $destDepartments);
                if (sizeof($courseResults) > 0) {
                    $subData['userDetails'] = $details;
                    $subData['level'] = $level;
                    $subData['courseResults'] = $courseResults;
                    $subData['grantAccess'] = 'courselecturer';
                    $subData['session'] = $session;
                    $html = View::make('staff.lecturer.verification.result',
                        $subData);
                } else {
                    $subData['grantAccess'] = false;
                    $subData['errorState'] = 1;
                    $html = View::make('staff.lecturer.verification.result',
                        $subData);
                }
            } else {
                $subData['grantAccess'] = false;
                $subData['errorState'] = 1;
                $html = View::make('staff.lecturer.verification.result',
                    $subData);
            }
        } else {
            $subData['grantAccess'] = false;
            $subData['errorState'] = 1;
            $html = View::make('staff.lecturer.verification.result', $subData);
        }
        return $html->render();

    }

    public function getCourseRegistrationArray($regNo, $verifiedBy)
    {
        $remarks =[
            'Registered',
            'Approved'
        ];
        $school = $verifiedBy->school;
        $department = $verifiedBy->dept;
        $level = $this->getUndergraduateLevel(false, $regNo);
        $semester = $this->userData->getCurrentSemester();
        $courses = DB::table('undergraduate_courses')
            ->leftJoin('undergraduate_results',
                function ($join) use ($regNo, $semester) {
                    $join->on('undergraduate_courses.course_code', '=',
                        'undergraduate_results.course_code')
                        ->on('undergraduate_courses.course_destination_dept',
                            '=',
                            'undergraduate_results.student_dept')
                        ->where('undergraduate_results.student_reg_no', '=',
                            $regNo);
                })
            ->where('undergraduate_courses.course_destination_school', $school)
            ->where('undergraduate_courses.course_destination_dept',
                $department)
            ->where('undergraduate_courses.level', $level)
            ->where('undergraduate_courses.semester', $semester)
            ->orWhere(
                function ($query) use ($remarks, $semester) {
                    $query->whereIn('undergraduate_results.remarks', $remarks)
                        ->where('undergraduate_courses.semester', $semester);
                })
            ->select(DB::raw('undergraduate_courses.id + 37 AS id'),
                'undergraduate_courses.course_code',
                'undergraduate_courses.course_title',
                'undergraduate_courses.units',
                'undergraduate_courses.course_type',
                'undergraduate_results.remarks')
            ->orderBy('undergraduate_results.remarks', 'ASC')
            ->orderBy('undergraduate_courses.units', 'DESC')
            ->orderBy('undergraduate_courses.level', 'ASC')
            ->orderBy('undergraduate_courses.course_code', 'ASC')
            ->get(); // return response ( )->json ( $courses );
        return $courses;

    }

    function verifyExam(
        $code = null,
        $student = null,
        $courseid = null,
        $verifiedBy
    ) {

        $course = Undergraduate_course::where('id', $courseid - 3327)->first();
        $lecturer = new LecturerDataService();
        $session = $lecturer->getCurrentSession();
        $html = "";
        if ($course) {
            if (in_array($verifiedBy->dept,
              [$course->course_source, $course->course_destination_dept])
            ) {
                $verify = false;
                $regNo = null;

                if (isset ($student)) {

                    $students = Undergraduate_results::where('course_code',
                        $course->course_code)->where('student_dept',
                        $course->course_destination_dept)
                        ->where('course_department', $course->course_source)
                        ->where('session',
                            date('Y', strtotime($session->start)))
                        ->pluck('student_reg_no')->toArray();

                    if (in_array($student, (array)$students)) {
                        $verify = true;
                        $regNo = $student;
                    }

                } elseif (isset ($code)) {

                    $students = Undergraduate_results::leftJoin('users',
                        'undergraduate_results.student_reg_no', '=',
                        'users.name')
                        ->where('undergraduate_results.course_code',
                            $course->course_code)
                        ->where('undergraduate_results.student_dept',
                            $course->course_destination_dept)
                        ->where('undergraduate_results.course_department',
                            $course->course_source)
                        ->where('session',
                            date('Y', strtotime($session->start)))
                        ->select('users.id',
                            'undergraduate_results.student_reg_no')
                        ->get();

                    foreach ($students as $student) {
                        $uid = md5($student->student_reg_no);
                        $index = $student->id % 37;
                        $uid = substr(sha1($uid), $index, 4);
                        if ($code == $uid) {
                            $verify = true;
                            $regNo = $student->student_reg_no;
                            break;
                        }
                    }
                }

                if ($verify) {
                    $data['userDetails']
                        = $this->getUndergraduateUsersMeta($regNo);
                    $data['course'] = $course;
                    $data['isVerified'] = true;
                    $data['level'] = $this->getUndergraduateLevel(false,
                        $regNo);

                    $html = View::make('staff.lecturer.verification.exam',
                        $data);

                } else {
                    $data['grantAccess'] = false;
                    $data['errorState'] = 1;
                    $html = View::make('staff.lecturer.verification.result',
                        $data);
                }
            }
        }

        return $html->render();
    }

    function verifyStudent($code = null, $student = null, $verifiedBy)
    {
        $grantAccess = $this->getPermissions($verifiedBy);
        $verify = false;
        $regNo = null;

        if (isset ($code) && isset ($student)) {
            $id = DB::table('users')->where(
              [
                  [
                        'school',
                        $verifiedBy->school
                    ],
                  [
                        'dept',
                        $verifiedBy->dept
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ],
                  [
                        'name',
                        $student
                    ]
                ])
                ->value('id');
            $uid = md5($student);
            $index = $id % 37;
            $uid = substr(sha1($uid), $index, 4);
            if ($code == $uid) {
                $verify = true;
                $regNo = $student;
            }
        } elseif (isset ($code) && !isset ($student)) {
            $students = DB::table('users')->where(
              [
                  [
                        'school',
                        $verifiedBy->school
                    ],
                  [
                        'dept',
                        $verifiedBy->dept
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ]
                ])
                ->select('id', 'name')
                ->get();

            foreach ($students as $student) {
                $uid = md5($student->name);
                $index = $student->id % 37;
                $uid = substr(sha1($uid), $index, 4);
                if ($code == $uid) {
                    $verify = true;
                    $regNo = $student->name;
                    break;
                }
            }
        } elseif (!isset ($code) && isset ($student)) {
            $students = DB::table('users')->where(
              [
                  [
                        'school',
                        $verifiedBy->school
                    ],
                  [
                        'dept',
                        $verifiedBy->dept
                    ],
                  [
                        'name',
                        $student
                    ],
                  [
                        'user_type',
                        'undergraduate'
                    ]
                ])
                ->get();
            if (sizeof($students) > 0) {
                $verify = true;
                $regNo = $student;
            }
        }

        if ($verify) {
            $session = $this->userData->getCurrentSession();
            $session = date('Y', strtotime($session->start));
            $details = $this->getUndergraduateUsersMeta($regNo);
            $level = $this->getUndergraduateLevel(false, $regNo);

            // drg >> check if is HOD
            if ((isset ($grantAccess['hod'])
                    && $grantAccess['hod']['school'] == $details->faculty
                    && $grantAccess['hod']['department'] == $details->dept)
                || (isset (
                        $grantAccess['courseadviser'])
                    && $grantAccess['courseadviser']['school']
                    == $details->faculty
                    && $grantAccess['courseadviser']['department']
                    == $details->dept
                    && $grantAccess['courseadviser']['level'] == $level)
            ) {
                $subData['userDetails'] = $details;
                $subData['session'] = $session;
                $subData['level'] = $level;
                $subData['cgpa'] = $this->getUndergraduateGPA($regNo);
                $subData['psgpa'] = $this->getUndergraduateGPA($regNo,
                    $session - 1);
                $subData['grantAccess'] = 'ca_hod';
                $html = View::make('staff.lecturer.verification.student',
                    $subData);
            } elseif (isset ($grantAccess['lecturer'])
                && $grantAccess['lecturer']['school'] == $details->faculty
                && $grantAccess['lecturer']['department'] == $details->dept
            ) {
                $subData['userDetails'] = $details;
                $subData['session'] = $session;
                $subData['level'] = $level;
                $subData['grantAccess'] = 'lecturer';
                $html = View::make('staff.lecturer.verification.student',
                    $subData);
            } else {
                $subData['grantAccess'] = false;
                $subData['errorState'] = 1;
                $html = View::make('staff.lecturer.verification.result',
                    $subData);
            }
        } else {
            $subData['grantAccess'] = false;
            $subData['errorState'] = 1;
            $html = View::make('staff.lecturer.verification.result', $subData);
        }
        return $html->render();

    }

    public function getAcademicSessions($id)
    {
        $sessions = DB::table('undergraduate_results')
            ->where('student_reg_no', $id)
            ->select('session')
            ->distinct()
            ->get();
        return $sessions;

    }

    function getCodePage($action, Request $request)
    {
        $verifiedBy = User::where('name', $request->name)->first();
        if ($action == 'exam') {
            $subData['courses'] = Undergraduate_course::where('course_source',
                $verifiedBy->dept)->orWhere('course_destination_dept',
                $verifiedBy->dept)
                ->select('id', 'course_code', 'course_source',
                    'course_destination_dept')->orderBy('course_source')->get();
        }

        $data['activeNavbar'] = "verification";
        $data['pageTitle'] = ucfirst($action) . " Verification";
        $subData['pageTitle'] = ucfirst($action) . " Verification";
        $subData['type'] = $action;
        $subdata['mobile'] = true;
        $subData['formAction']
            = "javascript:router('verify','$action/code/verify')";
        $html = View::make('staff.lecturer.verification.code', $subData);
        $data['html'] = $html->render();
        return response()->json($data, 200);

    }

    function getMatricnoPage($action, Request $request)
    {
        $verifiedBy = User::where('name', $request->name)->first();
        if ($action == 'exam') {
            $subData['courses'] = Undergraduate_course::where('course_source',
                $verifiedBy->dept)->orWhere('course_destination_dept',
                $verifiedBy->dept)
                ->select('id', 'course_code', 'course_source',
                    'course_destination_dept')->orderBy('course_source')->get();
        }
        $data['activeNavbar'] = "verification";
        $data['pageTitle'] = ucfirst($action) . " Verification";
        $subData['pageTitle'] = ucfirst($action) . " Verification";
        $subData['type'] = $action;
        $subdata['mobile'] = true;
        $subData['formAction']
            = "javascript:router('verify','$action/matricno/verify')";
        if ($action == 'course') {
            $subData['sessions'] = $this->getSessions($verifiedBy, true);
        } else {
            $subData['sessions'] = $this->getSessions($verifiedBy, false);
        }
        $subData['sessionOpt'] = $this->getSessions($verifiedBy, true);
        $html = View::make('staff.lecturer.verification.matricno', $subData);
        $data['html'] = $html->render();
        return response()->json($data, 200);
    }

    function getSearchPage($action, Request $request)
    {
        $verifiedBy = User::where('name', $request->name)->first();
        $session = $this->getSessions($verifiedBy, true);
        $session = $request->input('session', $session);
        $query = $request->input('query', null);
        $data['activeNavbar'] = "verification";
        $data['pageTitle'] = ucfirst($action) . " Verification";
        $deepData['session'] = $session;
        switch ($action) {
            case ('result') :
                $deepData['students'] = $this->getResultsSearchOutput($query,
                    $session, $verifiedBy);
                break;
            case ('course') :
                $deepData['students'] = $this->getCourseSearchOutput($query,
                    $verifiedBy);
                break;
            case ('student') :
                $deepData['students'] = $this->getStudentSearchOutput($query,
                    $verifiedBy);
                break;
            case('exam'):
                $courseid = $request->cid;
                $deepData['students'] = $this->getExamSearchOutput($query,
                    $courseid, $verifiedBy);
                break;
            default :
                $deepData['students'] = array();
                break;
        }
        $deepData['type'] = $action;
        $deepData['action'] = $action;
        $deepData['query'] = $query;
        $deepData['mobile'] = true;
        $deepHtml = View::make('staff.lecturer.verification.searchResults',
            $deepData);
        $subData['html'] = $deepHtml->render();
        $subData['pageTitle'] = ucfirst($action) . " Verification";

        $subData['type'] = $action;
        $subData['formAction']
            = "javascript:router('verify','$action/search/verify')";
        if ($action == 'course') {
            $subData['sessions'] = $this->getSessions($verifiedBy, true);
        } else {
            $subData['sessions'] = $this->getSessions($verifiedBy, false);
        }
        $subData['sessionOpt'] = $session;
        $subData['query'] = $query;
        if ($action == 'exam') {
            $subData['courseid'] = $request->cid;
            $subData['courses'] = Undergraduate_course::where('course_source',
                $verifiedBy->dept)->orWhere('course_destination_dept',
                $verifiedBy->dept)
                ->select('id', 'course_code', 'course_source',
                    'course_destination_dept')->orderBy('course_source')->get();
        }
        $html = View::make('staff.lecturer.verification.search', $subData);
        $data['html'] = $html->render();
        return response()->json($data, 200);
    }

    private function getResultsSearchOutput(
        $query = null,
        $session = null,
        $verifiedBy
    ) {
        $grantAccess = $this->getPermissions($verifiedBy);
        if (is_null($session)) {
            $session = $this->userData->getCurrentSession();
            $session = date('Y', strtotime($session->start));
        }
        if (!empty ($query)) {
            $terms = preg_split('/[\s,;:]+/', $query);
            $terms = array_filter($terms);
            if (isset ($grantAccess['hod'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['hod']['department'])
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->orWhere('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])
                && isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->orWhere('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->orWhere(
                        function ($query) use (
                            $sourceDepartments,
                            $destDepartments,
                            $courseCodes,
                            $session,
                            $terms
                        ) {
                            $query->where('undergraduate_results.session',
                                $session)
                                ->whereIn('undergraduate_results.student_dept',
                                    $sourceDepartments)
                                ->whereIn('undergraduate_results.course_department',
                                    $destDepartments)
                                ->whereIn('undergraduate_results.course_code',
                                    $courseCodes)
                                ->where(
                                    function ($query_) use ($terms) {
                                        for ($i = 0; $i < count($terms); $i++) {
                                            if ($i == 0) {
                                                $query_->where(
                                                    'undergraduate_results.student_reg_no',
                                                    'like', "%$terms[$i]%")
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.first_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"))
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.last_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"));
                                            } else {
                                                $query_->orWhere(
                                                    'undergraduate_results.student_reg_no',
                                                    'like', "%$terms[$i]%")
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.first_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"))
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.last_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"));
                                            }
                                        }
                                    });
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->orWhere('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->whereIn('undergraduate_results.student_dept',
                        $destDepartments)
                    ->whereIn('undergraduate_results.course_department',
                        $sourceDepartments)
                    ->whereIn('undergraduate_results.course_code', $courseCodes)
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->orWhere('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            }
        } else {
            if (isset ($grantAccess['hod'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['hod']['department'])
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])
                && isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->orWhere(
                        function ($query) use (
                            $sourceDepartments,
                            $destDepartments,
                            $courseCodes,
                            $session
                        ) {
                            $query->where('undergraduate_results.session',
                                $session)
                                ->whereIn('undergraduate_results.student_dept',
                                    $sourceDepartments)
                                ->whereIn('undergraduate_results.course_department',
                                    $destDepartments)
                                ->whereIn('undergraduate_results.course_code',
                                    $courseCodes);
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name, users_undergraduates.reg_no, users_undergraduates.dept, users.id"))
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->where('undergraduate_results.session', $session)
                    ->whereIn('undergraduate_results.student_dept',
                        $destDepartments)
                    ->whereIn('undergraduate_results.course_department',
                        $sourceDepartments)
                    ->whereIn('undergraduate_results.course_code', $courseCodes)
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            }
        }
        return $results;

    }

    private function getCourseSearchOutput($query = null, $verifiedBy)
    {
        $remarks =[
            'Registered',
            'Approved'
        ];
        $grantAccess = $this->getPermissions($verifiedBy);
        $session = $this->userData->getCurrentSession();
        $session = date('Y', strtotime($session->start));
        $results = array();
        if (!empty ($query)) {
            $terms = preg_split('/[\s,;:]+/', $query);
            $terms = array_filter($terms);
            if (isset ($grantAccess['hod'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['hod']['department'])
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])
                && isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->orWhere(
                        function ($query) use (
                            $sourceDepartments,
                            $destDepartments,
                            $courseCodes,
                            $session,
                            $terms
                        ) {
                            $query->where('undergraduate_results.session',
                                $session)
                                ->whereIn('undergraduate_results.student_dept',
                                    $sourceDepartments)
                                ->whereIn('undergraduate_results.course_department',
                                    $destDepartments)
                                ->whereIn('undergraduate_results.course_code',
                                    $courseCodes)
                                ->where(
                                    function ($query) use ($terms) {
                                        for ($i = 0; $i < count($terms); $i++) {
                                            if ($i == 0) {
                                                $query->where(
                                                    'undergraduate_results.student_reg_no',
                                                    'like', "%$terms[$i]%")
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.first_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"))
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.last_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"))
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(undergraduate_courses.course_code)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"));
                                            } else {
                                                $query->where(
                                                    'undergraduate_results.student_reg_no',
                                                    'like', "%$terms[$i]%")
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.first_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"))
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(users_undergraduates.last_name)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"))
                                                    ->orWhere(
                                                        DB::raw(
                                                            "LOWER(undergraduate_courses.course_code)"),
                                                        'like',
                                                        DB::raw("LOWER('%$terms[$i]%')"));
                                            }
                                        }
                                    });
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->whereIn('undergraduate_results.student_dept',
                        $destDepartments)
                    ->whereIn('undergraduate_results.course_department',
                        $sourceDepartments)
                    ->whereIn('undergraduate_results.course_code', $courseCodes)
                    ->where(
                        function ($query) use ($terms) {
                            for ($i = 0; $i < count($terms); $i++) {
                                if ($i == 0) {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                } else {
                                    $query->where('undergraduate_results.student_reg_no',
                                        'like',
                                        "%$terms[$i]%")
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"))
                                        ->orWhere(
                                            DB::raw("LOWER(undergraduate_courses.course_code)"),
                                            'like',
                                            DB::raw("LOWER('%$terms[$i]%')"));
                                }
                            }
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_n', 'asc')
                    ->get();
            }
        } else {
            if (isset ($grantAccess['hod'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['hod']['department'])
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])
                && isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->orWhere(
                        function ($query) use (
                            $sourceDepartments,
                            $destDepartments,
                            $courseCodes,
                            $session
                        ) {
                            $query->where('undergraduate_results.session',
                                $session)
                                ->whereIn('undergraduate_results.student_dept',
                                    $sourceDepartments)
                                ->whereIn('undergraduate_results.course_department',
                                    $destDepartments)
                                ->whereIn('undergraduate_results.course_code',
                                    $courseCodes);
                        })
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courseadviser'])) {
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->where('undergraduate_results.student_reg_no', 'like',
                        $grantAccess['courseadviser']['year'] . '%')
                    ->where('undergraduate_results.student_dept',
                        $grantAccess['courseadviser']['department'])
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            } elseif (isset ($grantAccess['courselecturer'])
                && isset ($grantAccess['courselecturer']['courses'])
            ) {
                $courseCodes = array();
                $sourceDepartments = array();
                $destDepartments = array();
                foreach (
                    $grantAccess['courselecturer']['courses'] as $courses
                ) {
                    $course = explode('_', $courses->system_id);
                    array_push($courseCodes, $course[0]);
                    array_push($sourceDepartments, $course[1]);
                    array_push($destDepartments, $course[3]);
                }
                $results = DB::table('undergraduate_results')
                    ->leftJoin('users_undergraduates',
                        'users_undergraduates.reg_no', '=',
                        'undergraduate_results.student_reg_no')
                    ->leftJoin('users', 'users.name', '=',
                        'users_undergraduates.reg_no')
                    ->leftJoin('undergraduate_courses',
                        function ($join) {
                            $join->on('undergraduate_courses.course_code', '=',
                                'undergraduate_results.course_code')
                                ->on('undergraduate_courses.course_destination_dept',
                                    '=',
                                    'undergraduate_results.student_dept');
                        })
                    ->where('undergraduate_results.session', $session)
                    ->whereIn('undergraduate_results.student_dept',
                        $destDepartments)
                    ->whereIn('undergraduate_results.course_department',
                        $sourceDepartments)
                    ->whereIn('undergraduate_results.course_code', $courseCodes)
                    ->select(
                        DB::raw(
                            "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                        'users_undergraduates.reg_no',
                        'users_undergraduates.dept', 'users.id')
                    ->distinct()
                    ->orderBy('users_undergraduates.reg_no', 'asc')
                    ->get();
            }
        }
        return $results;

    }


    private function getStudentSearchOutput($query = null, $verifiedBy)
    {
        if (!empty ($query)) {
            $terms = preg_split('/[\s,;:]+/', $query);
            $terms = array_filter($terms);
            $results = DB::table('users_undergraduates')
                ->leftJoin('users', 'users.name', '=',
                    'users_undergraduates.reg_no')
                ->where(
                  [
                      [
                            'users_undergraduates.dept',
                            $verifiedBy->dept
                        ],
                      [
                            'users_undergraduates.faculty',
                            $verifiedBy->school
                        ]
                    ])
                ->where(
                    function ($query) use ($terms) {
                        for ($i = 0; $i < count($terms); $i++) {
                            if ($i == 0) {
                                $query->where('users_undergraduates.reg_no',
                                    'like', "%$terms[$i]%")
                                    ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                        'like',
                                        DB::raw("LOWER('%$terms[$i]%')"))
                                    ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                        'like',
                                        DB::raw("LOWER('%$terms[$i]%')"));
                            } else {
                                $query->where('users_undergraduates.reg_no',
                                    'like', "%$terms[$i]%")
                                    ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                        'like',
                                        DB::raw("LOWER('%$terms[$i]%')"))
                                    ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                        'like',
                                        DB::raw("LOWER('%$terms[$i]%')"));
                            }
                        }
                    })
                ->select(
                    DB::raw(
                        "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                    'users_undergraduates.reg_no', 'users_undergraduates.dept',
                    'users.id')
                ->distinct()
                ->orderBy('users_undergraduates.reg_no', 'asc')
                ->get();
        } else {
            $results = DB::table('users_undergraduates')
                ->leftJoin('users', 'users.name', '=',
                    'users_undergraduates.reg_no')
                ->where(
                  [
                      [
                            'users_undergraduates.dept',
                            $verifiedBy->dept
                        ],
                      [
                            'users_undergraduates.faculty',
                            $verifiedBy->school
                        ]
                    ])
                ->select(
                    DB::raw(
                        "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                    'users_undergraduates.reg_no', 'users_undergraduates.dept',
                    'users.id')
                ->distinct()
                ->orderBy('users_undergraduates.reg_no', 'asc')
                ->get();
        }
        return $results;

    }

    private function getExamSearchOutput(
        $query = null,
        $courseid = null,
        $verifiedBy
    ) {
        $course = Undergraduate_course::where('id', $courseid - 3327)
            ->where(function ($query) use ($verifiedBy) {
                $query->where('course_source',
                    $verifiedBy->dept)->orWhere('course_destination_dept',
                    $verifiedBy->dept);
            })->first();
        $lecturer = new LecturerDataService();
        $session = $lecturer->getCurrentSession();
        // drg >> condition checks if query and course is valid.
        if (!empty ($query) && $course) {
            $terms = preg_split('/[\s,;:]+/', $query);
            $terms = array_filter($terms);

            $results = DB::table('undergraduate_results')
                ->join('users', 'undergraduate_results.student_reg_no',
                    'users.name')
                ->join('users_undergraduates', 'users_undergraduates.reg_no',
                    '=',
                    'undergraduate_results.student_reg_no')
                ->where('course_code', $course->course_code)
                ->where('student_dept',
                    $course->course_destination_dept)
                ->where('course_department', $course->course_source)
                ->where('session',
                    date('Y', strtotime($session->start)))
                ->where(function ($query) use ($terms) {
                    for ($i = 0; $i < count($terms); $i++) {
                        if ($i == 0) {
                            $query->where('users_undergraduates.reg_no',
                                'like', "%$terms[$i]%")
                                ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                    'like',
                                    DB::raw("LOWER('%$terms[$i]%')"))
                                ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                    'like',
                                    DB::raw("LOWER('%$terms[$i]%')"));
                        } else {
                            $query->where('users_undergraduates.reg_no',
                                'like', "%$terms[$i]%")
                                ->orWhere(DB::raw("LOWER(users_undergraduates.first_name)"),
                                    'like',
                                    DB::raw("LOWER('%$terms[$i]%')"))
                                ->orWhere(DB::raw("LOWER(users_undergraduates.last_name)"),
                                    'like',
                                    DB::raw("LOWER('%$terms[$i]%')"));
                        }
                    }
                })
                ->select(
                    DB::raw(
                        "concat(users_undergraduates.first_name, ' ', users_undergraduates.last_name) as name"),
                    'users_undergraduates.reg_no', 'users_undergraduates.dept',
                    'users.id')
                ->distinct()
                ->orderBy('users_undergraduates.reg_no', 'asc')
                ->get();

        } else {
            $results =[];
        }


        return $results;

    }
}