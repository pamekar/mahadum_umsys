<?php

namespace App\Http\Controllers\API;

use App\Services\staff\lecturer\LecturerDataService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\View;

class HomeController extends Controller
{
    //
    public function index(Request $request)
    {
        $userid = $request->name;
        $category = $request->category;

        $userData = new LecturerDataService();
        $subData['data'] = (object) $userData->getLecturerData($userid);
        $subData['public']=null;
        $html = View::make('layouts.staff.lecturer.side-overlay', $subData);
        $data['sideOverlay'] = $html->render();

        return response()->json($data, 200);
    }
}
