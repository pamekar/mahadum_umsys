<?php

namespace App\Http\Controllers\Admin;

use App\Attendance;
use App\Invoice;
use App\Invoice_line;
use App\Payment;
use App\Services\staff\lecturer\LecturerDataService;
use App\Undergraduate_result;
use App\Users_undergraduate;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class ChartController extends Controller
{
    //
    public function results($session)
    {
        $results = Undergraduate_result::where('session', $session)->select(
            DB::raw('count(result_grade) as count'))->groupBy('session')
            ->groupBy('result_grade')->orderBy('result_grade', 'desc')
            ->pluck('count');

        return $results;
    }

    public function attendance($remark)
    {
        $attendance = Attendance::where('type', 'lecture')
            ->where('status', 'closed')->where('remark', $remark)
            ->select(DB::raw('count(remark) as count'))->groupBy('session')
            ->orderBy('session')->pluck('count');

        return $attendance;
    }

    public function students($action)
    {
        $students = [];
        $userData = new LecturerDataService();
        $session = $userData->getCurrentSession();
        $session = date('Y', strtotime($session->start));

        switch ($action) {
            case 'gender':
                $students = Users_undergraduate::groupBy('sex')
                    ->select(DB::raw('count(sex) as count'))->orderBy('sex')
                    ->pluck('count');
                break;
            case 'faculty':
                $students['list'] = Users_undergraduate::distinct()
                    ->select('faculty')->orderBy('faculty')->pluck('faculty');
                $students['count']
                    = Users_undergraduate::select(DB::raw('count(faculty) as count'))
                    ->groupBy('faculty')
                    ->orderBy('faculty')->pluck('count');
                break;
            case 'year':
                $students['list'] = Users_undergraduate::distinct()
                    ->select(DB::raw("concat((($session - year_entry)*100),' level') as level"))
                    ->orderBy('year_entry')->pluck('level');
                $students['count']
                    = Users_undergraduate::select(DB::raw('count(year_entry) as count'))
                    ->groupBy('year_entry')
                    ->orderBy('year_entry')->pluck('count');
                break;
        }

        return $students;
    }

    public function payments()
    {
        $types = ['university', 'faculty', 'dept', 'level', 'student'];
        $faculties = ['SOSC', 'SMAT', 'SEET'];
        $levels = [100, 200, 300, 400, 500];

        $paid = [];
        $debts = [];
        $currentSession = setting('admin.current_session');
        foreach ($types as $type) {
            switch ($type) {
                case 'university':
                    $invoices = Invoice::where('session', $currentSession)
                        ->where('type', $type)->pluck('invoice_id');

                    $payment_received = Payment::whereIn('invoice_id',
                        $invoices)->sum('amount');
                    $student_count = Users_undergraduate::where('status',
                        'active')->count();
                    $invoiceSum = Invoice_line::whereIn('invoice_id', $invoices)
                        ->sum('amount');
                    $payment_required = $invoiceSum * $student_count;
                    $debt = $payment_required - $payment_received;
                    array_push($paid, $payment_received);
                    array_push($debts, $debt);
                    break;
                case 'faculty':
                    foreach ($faculties as $faculty) {
                        $invoices = Invoice::where('session', $currentSession)
                            ->where('type', $type)->where('target', $faculty)
                            ->pluck('invoice_id');
                        $amount = 0;

                        $payment_received = Payment::whereIn('invoice_id',
                            $invoices)->sum('amount');
                        $student_count = Users_undergraduate::where('status',
                            'active')->where('faculty', $faculty)->count();
                        $invoiceSum = Invoice_line::whereIn('invoice_id',
                            $invoices)->sum('amount');
                        $payment_required = $invoiceSum * $student_count;
                        $debt = $payment_required - $payment_received;
                        array_push($paid, $payment_received);
                        array_push($debts, $debt);
                    }
                    break;
                case 'level':
                    foreach ($levels as $level) {
                        $invoices = Invoice::where('session', $currentSession)
                            ->where('type', $type)->where('target', $level)
                            ->pluck('invoice_id');
                        $amount = 0;

                        $payment_received = Payment::whereIn('invoice_id',
                            $invoices)->sum('amount');
                        $student_count = Users_undergraduate::where('status',
                            'active')
                            ->where(DB::raw("($currentSession-year_entry+1)*100"),
                                $level)->count();
                        $invoiceSum = Invoice_line::whereIn('invoice_id',
                            $invoices)->sum('amount');
                        $payment_required = $invoiceSum * $student_count;
                        $debt = $payment_required - $payment_received;
                        array_push($paid, $payment_received/100);
                        array_push($debts, $debt/100);
                    }
                    break;

            }
        }

        $payments['labels'] = array_merge(['university'], $faculties, $levels);
        $payments['pointsA'] = $paid;
        $payments['pointsB'] = $debts;

        return $payments;
    }
}
