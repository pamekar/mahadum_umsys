<?php
// drg >>
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\student\undergraduate\HTMLService;
use App\User;
use Illuminate\Support\Facades\Auth;
use App\Services\student\undergraduate\CourseService;
use LaravelQRCode\Facades\QRCode;
use View;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;

class VerifyController extends Controller
{

    public function __construct(HTMLService $html, CourseService $course)
    {
        $this->html = $html;
        $this->courses = $course;

    }

    public function index(
        $action,
        $code,
        $user,
        $session = null,
        Request $request
    ) {
        switch (Auth::user()->user_type) {
            case ('undergraduate') :

                return redirect()
                    ->action('student\undergraduate\VerifyController@getRequest',
                        [
                            'action'      => $action,
                            'request'     => $request,
                            'code'        => $code,
                            'session'     => $session,
                            'user'        => $user
                        ])->withInput();
                break;
            case ('postgraduate') :
                break;
            case ('guardian') :
                break;
            case ('staff') :
                switch (Auth::user()->user_view) {
                    case ('admin') :
                        break;
                    case ('administrator') :
                        break;
                    case ('lecturer') :
                        return redirect()
                            ->action('staff\lecturer\VerifyController@getRequest',
                                [
                                    'action'      => $action,
                                    'request'     => $request,
                                    'session'     => $session,
                                    'code'        => $code,
                                    'user'        => $user,
                                ])->withInput();
                        break;
                }
                break;
            default :
                return view('auth.login');
                break;
        }

    }

}