<?php

namespace App\Http\Middleware;

use App\Services\guardian\GuardianDataService;
use Closure;
use Illuminate\Http\Response;

class IsGuardian
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userData = new GuardianDataService();
        if ($request->user()->user_type != 'guardian') {
            return new Response(view('errors.602'));
        }
        if (!$userData->hasProfile()
            && $request->url() !== route('guardian.settings.profile')
        ) {
            return new Response(view('guardian.newProfile'), 401);
        }
        $userData->setGuardianData($request);

        return $next ($request);
    }
}
