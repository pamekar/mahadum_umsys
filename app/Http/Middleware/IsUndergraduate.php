<?php

namespace App\Http\Middleware;

use App\Services\student\undergraduate\UndergraduateDataService;
use Closure;
use Illuminate\Http\Response;

class IsUndergraduate
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->user_type != 'undergraduate') {
            return new Response (view('errors.602'));
        }
        $userData = new UndergraduateDataService();
        $userData->setUndergraduateData($request);
        return $next ($request);

    }

}
