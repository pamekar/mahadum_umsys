<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\DB;

class CheckUser
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $input = $request->all();
        $isUser = DB::table('users')->where('name', strtoupper($input ['name']))
            ->orWhere('name', strtolower($input ['name']))
            ->first();
        if ($isUser) {
            $isType = DB::table('users')
                ->where('user_type', strtolower($input ['category']))
                ->where(
                    function ($query) use ($input) {
                        $query->where('name', strtoupper($input ['name']))
                            ->orWhere('name', strtolower($input ['name']));
                    })
                ->first();
            if ($isType) {
                if (isset ($input ['name'])) {
                    $input ['name'] = strtoupper($input ['name']);
                }
                if (isset ($input ['remember'])
                    && isset ($input ['category'])
                ) {
                    if ($input ['category'] == 'Staff') {
                        $input ['remember'] = false;
                    }
                }
                $request->replace($input);
                return $next ($request);
            } else {
                if ($request->isJson()) {
                    return response()->json([
                        'error'   => 'typeError',
                        'message' => __('auth.typeError'). $input['category']
                    ], 401);
                }
                return redirect()->back()
                    ->withInput($request->only('name', 'remember', 'category'))
                    ->withErrors([
                        'typeError' => __('auth.typeError')
                    ]);
            }
        } else {
            if ($request->isJson()) {
                return response()->json([
                    'error'   => 'notFound',
                    'message' => __('auth.notFound')
                ], 401);
            }
            return redirect()->back()
                ->withInput($request->only('name', 'remember', 'category'))
                ->withErrors([
                    'notFound' => __('auth.notFound')
                ]);
        }

    }

}
