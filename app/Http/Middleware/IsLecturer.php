<?php

namespace App\Http\Middleware;

use App\Services\staff\lecturer\LecturerDataService;
use Closure;
use Illuminate\Http\Response;

class IsLecturer
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->user_view != 'lecturer') {
            return new Response (view('errors.602'));
        }

        $userData = new LecturerDataService();
        $userData->setLecturerData($request);

        return $next ($request);

    }

}
