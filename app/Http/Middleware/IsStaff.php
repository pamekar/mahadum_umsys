<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Response;

class IsStaff
{

    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure                 $next
     *
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->user()->user_type != 'staff') {
            return new Response (view('errors.602'));
        } else {
            return $next ($request);
        }

    }

}
