<?php

namespace App\Providers;

use App\Invoice;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = ['App\Events\SomeEvent' => ['App\Listeners\EventListener']];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        // drg >> when invoice is created
        Invoice::created(function ($record) {
            $invoice = Invoice::find($record->id);
            /*$invoice->invoice_id = md5($record->id . str_random()
                . date('YmdHis'));*/
            $invoice->invoice_code
                = "$record->type-$record->session-$record->target-$record->id";
            $invoice->created_by = Auth::user()->name;
            $invoice->save();
        });

        // drg >> when invoice is updated
        Invoice::updated(function ($record) {
            $invoice = Invoice::find($record->id);
            $invoice->invoice_code
                = "$record->type-$record->session-$record->target-$record->id";
            $invoice->save();
        });
    }

}
