<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Admin_access extends Model
{
    //
    protected $table = 'admin_access';
    protected $perPage = 25;
}
