<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SchoolCalendar extends Model
{
	//
	protected $table = 'school_calendar';
    protected $perPage = 25;
}
