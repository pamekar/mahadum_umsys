<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Chatter_discussion extends Model
{
    //
    protected $table = 'chatter_discussion';
    protected $perPage = 25;
}
