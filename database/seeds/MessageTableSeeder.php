<?php

use Illuminate\Database\Seeder;

class MessageTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $depts = [
            'IMT',
            'PHY',
            'GST',
            'CIE',
            'CHE',
            'MTH',
            'BIO',
            'PMT',
            'CHM'
        ];

        foreach ($depts as $dept) {
            $users = \App\User::where('user_type', '<>', 'guardian')
                ->where('dept', $dept)->pluck('name')->toArray();
            foreach (range(1, 700) as $index) {
                $to = [
                    $faker->randomElement($users),
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users) . ';'
                    . $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    $faker->randomElement($users),
                    $faker->randomElement($users),
                    $faker->randomElement($users),
                    $faker->randomElement($users)
                ];
                $cc = [
                    $faker->randomElement($users),
                    '',
                    '',
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    '',
                    '',
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users) . ';'
                    . $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    '',
                    '',
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    '',
                    '',
                    '',
                    '',
                    $faker->randomElement($users) . ';'
                    . $faker->randomElement($users),
                    '',
                    $faker->randomElement($users),
                    '',
                    '',
                    $faker->randomElement($users),
                    '',
                    $faker->randomElement($users),
                    '',
                    '',
                    $faker->randomElement($users),
                    '',
                    '',
                    '',
                    ''
                ];
                \Illuminate\Support\Facades\DB::table('messages')->insert([
                    'message_id' => $faker->unique()->md5,
                    'subject'    => $faker->realText(50),
                    'message'    => $faker->paragraphs(4, true),
                    'from'       => $faker->randomElement($users),
                    'to'         => $faker->randomElement($to),
                    'cc'         => $faker->randomElement($cc),
                    'bcc'        => $faker->randomElement($cc),
                    'starred'    => $faker->randomElement([0, 1]),
                    'archived'   => $faker->randomElement([0, 1]),
                    'status'     => $faker->randomElement([
                        'draft',
                        'sent'
                    ]),
                    'created_at' => $faker->dateTimeBetween('-2 years',
                        'now')
                ]);

            }
        }
    }
}
