<?php

use Illuminate\Database\Seeder;

class InvoiceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();

        $sessions = [2013, 2014, 2015, 2016, 2017, 2018];
        $types = ['university', 'faculty', 'dept', 'level', 'student'];
        $faculties = ['SOSC', 'SMAT', 'SEET'];
        $depts = [
            'BIO',
            'IMT',
            'MEE',
            'CIE',
            'PMT',
            'CHM',
            'CHE',
            'MTH',
            'PHY'
        ];
        $levels = [100, 200, 300, 400, 500];
        $i = 1;
        foreach ($sessions as $session) {
            $i++;
            foreach ($types as $type) {
                $i++;
                switch ($type) {
                    case 'university':
                        $target = '';
                        $invoice
                            = [
                            "invoice_id" => "$type-$session-$target-$i",
                            "title"        => "School Fees Invoice",
                            "type"         => $type,
                            "session"      => $session,
                            "target"       => $target,
                            "issuer"       => "Bursary",
                            "created_by"   => "admin",
                        ];
                        $this->createInvoice($invoice);
                        break;
                    case 'faculty':
                        foreach ($faculties as $faculty) {
                            $invoice
                                = [
                                "invoice_id" => "$type-$session-$faculty-$i",
                                "title"        => "Faculty Charges",
                                "type"         => $type,
                                "session"      => $session,
                                "target"       => $faculty,
                                "issuer"       => $faculty,
                                "created_by"   => "admin",
                            ];
                            $this->createInvoice($invoice);
                            $i++;
                        }
                        break;
                    case 'dept':
                        foreach ($depts as $dept) {
                            $invoice
                                = [
                                "invoice_id" => "$type-$session-$dept-$i",
                                "title"        => "Departmental Charges",
                                "type"         => $type,
                                "session"      => $session,
                                "target"       => $dept,
                                "issuer"       => $dept,
                                "created_by"   => "admin",
                            ];
                            $this->createInvoice($invoice);
                            $i++;
                        }
                        break;
                    case 'level':
                        foreach ($levels as $level) {
                            $invoice
                                = [
                                "invoice_id" => "$type-$session-$level-$i",
                                "title"        => "'Level' Charges",
                                "type"         => $type,
                                "session"      => $session,
                                "target"       => $level,
                                "issuer"       => "Bursary",
                                "created_by"   => "admin",
                            ];
                            $this->createInvoice($invoice);
                            $i++;
                        }
                        break;
                    case 'student':
                        $limit = ($session - 2013) * 8;
                        $students
                            = \App\Users_undergraduate::where('year_entry',
                            '<=', $session)->inRandomOrder()->limit($limit)
                            ->pluck('reg_no');
                        foreach ($students as $student) {
                            $invoice
                                = [
                                "invoice_id" => "$type-$session-$student-$i",
                                "title"        => "Student Charges for",
                                "type"         => $type,
                                "session"      => $session,
                                "target"       => $student,
                                "issuer"       => "Bursary",
                                "created_by"   => "admin",
                            ];
                            $this->createInvoice($invoice);
                            $i++;
                        }
                        break;
                }


            }

        }
    }

    public function createInvoice($invoice)
    {
        \Illuminate\Support\Facades\DB::table('invoices')
            ->insert($invoice);
        $this->createInvoiceLines($invoice);
    }

    public function createInvoiceLines($invoice)
    {
        switch ($invoice['type']) {
            case 'university':
                $this->createUniversityInvoice($invoice);
                break;
            case 'faculty':
                $this->createFacultyInvoice($invoice);
                break;
            case 'dept':
                $this->createDeptInvoice($invoice);
                break;
            case 'level':
                $this->createLevelInvoice($invoice);
                break;
            case 'student':
                $this->createStudentInvoice($invoice);
                break;
        }

    }

    public function createUniversityInvoice($invoice)
    {
        $lines = [
            'Student Health Insurance/Medical' => 4500,
            'Sport Fees'                       => 1000,
            'Examination'                      => 1000,
            'Registration'                     => 1000,
            'Library Fees'                     => 1000,
            'Laboratory Fees'                  => 1000,
            'Municipal Charge'                 => 1000,
            'Information Com. Tech. Fees'      => 8000,
            'Student Union Dues'               => 800,
            'Accreditation Fee'                => 3000,
            'PMF Levy'                         => 5000,
            'Development Levy'                 => 5000
        ];
        $invoice_id = $invoice['invoice_id'];

        foreach ($lines as $item => $amount) {

            \Illuminate\Support\Facades\DB::table('invoice_lines')
                ->insert([
                    'invoice_id' => $invoice_id,
                    'item'       => $item,
                    'amount' => $amount,
                    'unit'  => 1
                ]);
        }
    }

    public function createFacultyInvoice($invoice)
    {
        $lines = [
            'Maintenance Fee'         => 1000,
            'National Conference Fee' => 3000,
            'Transportation'          => 1000,
        ];
        $invoice_id = $invoice['invoice_id'];

        foreach ($lines as $item => $amount) {

            \Illuminate\Support\Facades\DB::table('invoice_lines')
                ->insert([
                    'invoice_id' => $invoice_id,
                    'item'       => $item,
                    'amount' => $amount,
                    'unit'  => 1
                ]);
        }
    }

    public function createDeptInvoice($invoice)
    {
        $lines = [
            'Departmental Levy'                             => 1500,
            $invoice['issuer'] . " National Conference Fee" => 1500,
        ];
        if (in_array($invoice['issuer'], ['MEE', 'CIE', 'CHE'])) {
            array_replace($lines, ['Excursion Fee' => 5000]);
            array_replace($lines,
                [$invoice['issuer'] . " National Conference Fee" => 2500]);
        } elseif (in_array($invoice['issuer'], ['BIO', 'CHM', 'PHY'])) {
            array_replace($lines, ['Excursion Fee' => 3000]);
            array_replace($lines,
                [$invoice['issuer'] . " National Conference Fee" => 2000]);
        }
        $invoice_id = $invoice['invoice_id'];

        foreach ($lines as $item => $amount) {

            \Illuminate\Support\Facades\DB::table('invoice_lines')
                ->insert([
                    'invoice_id' => $invoice_id,
                    'item'       => $item,
                    'amount' => $amount,
                    'unit'  => 1
                ]);
        }
    }

    public function createLevelInvoice($invoice)
    {
        $lines = [];
        switch ($invoice['target']) {
            case '100':
                $lines = [
                    'Acceptance Fee'    => 25000,
                    "Workshop Practice" => 3000,
                    "Laboratory Fee"    => 3000,
                    "Matriculation"     => 8000,
                    "Books"             => 18000
                ];
                break;
            case '200':
                $lines = [
                    "Workshop Practice" => 3000,
                    "Laboratory Fee"    => 3000,
                    "Books"             => 13000
                ];
                break;
            case '300':
                $lines = [
                    "Laboratory Fee" => 2000,
                    "Books"          => 10000
                ];
                break;
            case '400':
                $lines = [
                    "Industial Attachment" => 1000,
                    "Books"                => 5000
                ];
                break;
            case '500':
                $lines = [
                    "Final Project" => 10000,
                    "Convocation"   => 8000,
                    "Books"         => 8000
                ];
                break;
        }
        $invoice_id = $invoice['invoice_id'];

        foreach ($lines as $item => $amount) {

            \Illuminate\Support\Facades\DB::table('invoice_lines')
                ->insert([
                    'invoice_id' => $invoice_id,
                    'item'       => $item,
                    'amount' => $amount,
                    'unit'  => 1
                ]);
        }
    }

    public function createStudentInvoice($invoice)
    {
        $lines = [
            [
                "Damage of Louvers"                      => 1000,
                "Damage of Hostel Installation"          => 2500,
                "Fine for Illegal Electrical Connection" => 1000
            ],
            [
                "Damage of School Property"     => 4000,
                "Damage of Hostel Installation" => 1500,
            ],
            [
                "Damage of School Property"              => 7000,
                "Damage of Hostel Installation"          => 1500,
                "Fine for Illegal Electrical Connection" => 1000
            ],
            [
                "Payment for Hall Venue"            => 4000,
                "Payment for Association Ownership" => 3000,
                "Bus Transportation Services"       => 15000
            ],
            [
                "Payment for Hall Venue"            => 2000,
                "Payment for Association Ownership" => 3000,
                "Bus Transportation Services"       => 10000
            ],
            [
                "Payment for Hall Venue"            => 2500,
                "Payment for Association Ownership" => 3000,
                "Bus Transportation Services"       => 8000
            ]
        ];
        $invoice_id = $invoice['invoice_id'];

        $index = mt_rand(0, 5);
        foreach ($lines[$index] as $item => $amount) {

            \Illuminate\Support\Facades\DB::table('invoice_lines')
                ->insert([
                    'invoice_id' => $invoice_id,
                    'item'       => $item,
                    'amount' => $amount,
                    'unit'  => 1
                ]);
        }
    }
}
