<?php

use Illuminate\Database\Seeder;

class AttendanceTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = \Faker\Factory::create();
        $system_ids = \App\Admin_access::where('admin_function',
            'course_lecturer')
            ->select('system_id', 'admin_id')->get()->toArray();
        $remarks[2014] = [
            'present',
            'absent',
            'present',
            'absent',
            'present',
            'present',
            'present',
        ];

        $remarks[2015] = [
            'present',
            'absent',
            'absent',
            'present',
            'absent',
            'present',
            'present',
            'present',
        ];

        $remarks[2016] = [
            'present',
            'absent',
            'absent',
            'present',
            'present',
            'absent',
            'present',
            'absent',
            'present',
            'absent',
            'present',
            'present'
        ];
        $remarks[2017] = [
            'present',
            'absent',
            'present',
            'present',
            'absent',
            'present',
            'present',
            'absent',
            'absent',
            'present'
        ];
        $remarks[2018] = [
            'present',
            'absent',
            'present',
            'present',
            'present',
            'present',
            'absent',
            'present',
            'present',
            'absent',
            'present',
            'absent',
            'present',
            'absent',
            'present',
            'present'
        ];
        $sessions = [2014,2015, 2016, 2017, 2018];
        foreach ($sessions as $session) {
            foreach (range(1, 900) as $index) {
                $access = array_random($system_ids);
                $system_id = $access['system_id'];
                $user = $access['admin_id'];
                $attendance = [];
                $course = explode('_', $system_id);
                $attendance_id = md5($system_id . str_random()
                    . date('ymdHis'));
                $service
                    = new \App\Services\staff\lecturer\LecturerDataService();

                $students = \App\Undergraduate_result::where('course_code',
                    $course[0])
                    ->where('course_department', $course[1])
                    ->where('student_dept', $course[3])
                    ->where('remarks', 'Registered')->get();
                $created_at = $faker->dateTimeBetween('-5 months', '-20 days');
                $a = mt_rand(10, 15);
                $b = mt_rand(3, 10);
                $closed_at = date_add($created_at,
                    date_interval_create_from_date_string("$a days"));
                // drg >> add array for attendance
                foreach ($students as $student) {
                    $signed_at = date_add($created_at,
                        date_interval_create_from_date_string(
                            "$b days"));

                    array_push($attendance, [
                        'attendance_id' => $attendance_id,
                        'system_id'     => $system_id,
                        'signed_by'     => $user,
                        'signed_user'   => $student->student_reg_no,
                        'type'          => 'lecture',
                        'status'        => 'closed',
                        'session'       => $session,
                        'remark'        => $faker->randomElement($remarks[$session]),
                        'signed_at'     => $signed_at,
                        'closed_at'     => $closed_at,
                        'created_at'    => $created_at,
                        'updated_at'    => $closed_at
                    ]);
                }

                \Illuminate\Support\Facades\DB::table('attendance')
                    ->insert($attendance);
            }

        }
    }
}
