<?php

use Illuminate\Database\Seeder;

class PaymentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $invoices = \App\Invoice::all();
        foreach ($invoices as $invoice) {
            $this->makePayments($invoice);
        }
        $fraction = mt_rand(75, 95) / 100;
        $limit
            = (integer)(\App\Invoice::where('type', 'student')
                ->count() * $fraction);

        $students = \App\Invoice::where('type', 'student')->get();
        foreach ($students as $invoice) {
            $this->createPayments($invoice, (array)$invoice->target);
        }

    }

    public function makePayments($invoice)
    {
        $year = $invoice->session;
        $students = [];
        switch ($invoice->type) {
            case 'university':
                $fraction = mt_rand(65, 95) / 100;
                $limit
                    = (integer)(\App\Users_undergraduate::where('year_entry',
                        '<=', $year)->count() * $fraction);
                $students
                    = \App\Users_undergraduate::where('year_entry',
                    '<=', $year)->inRandomOrder()->limit($limit)
                    ->pluck('reg_no');

                break;
            case 'faculty':
                $fraction = mt_rand(75, 95) / 100;
                $limit
                    = (integer)(\App\Users_undergraduate::where('year_entry',
                        '<=', $year)->where('faculty', $invoice->target)
                        ->count() * $fraction);
                $students
                    = \App\Users_undergraduate::where('year_entry',
                    '<=', $year)->where('faculty', $invoice->target)
                    ->inRandomOrder()->limit($limit)
                    ->pluck('reg_no');
                break;
            case 'dept':
                $fraction = mt_rand(85, 95) / 100;
                $limit
                    = (integer)(\App\Users_undergraduate::where('year_entry',
                        '<=', $year)->where('dept', $invoice->target)
                        ->count() * $fraction);
                $students
                    = \App\Users_undergraduate::where('year_entry',
                    '<=', $year)->where('dept', $invoice->target)
                    ->inRandomOrder()->limit($limit)
                    ->pluck('reg_no');
                break;
            case 'level':
                $fraction = mt_rand(85, 95) / 100;
                $limit
                    = (integer)(\App\Users_undergraduate::where(\Illuminate\Support\Facades\DB::raw("($year-year_entry+1)*100"),
                        $invoice->target)
                        ->count() * $fraction);
                $students
                    = \App\Users_undergraduate::where(\Illuminate\Support\Facades\DB::raw("($year-year_entry+1)*100"),
                    $invoice->target)
                    ->inRandomOrder()->limit($limit)
                    ->pluck('reg_no');
                break;
        }
        $this->createPayments($invoice, $students, 'remita');
    }

    public function createPayments($invoice, $students, $gateway='paystack')
    {
        $amount = \App\Invoice_line::where('invoice_id',
            $invoice->invoice_id)->sum('amount');
        foreach ($students as $student) {
            $payment
                = [
                'payment_id' => md5($invoice->invoice_id
                    . $student . str_random()),
                'invoice_id' => $invoice->invoice_id,
                'user_id'    => $student,
                'gateway'    => $gateway,
                'amount'     => $amount
            ];
            \Illuminate\Support\Facades\DB::table('payments')
                ->insert($payment);
        }
    }
}
