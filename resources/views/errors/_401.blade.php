@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Unauthorized')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, authentication has failed, or has not yet been provided.</div>
    </div>
@endsection('content')

