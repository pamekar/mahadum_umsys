@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Internal Server Error')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, something unexpected happened.</div>
    </div>
@endsection('content')

