@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Loop detected')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, the server detected an infinite loop while processing the request.</div>
    </div>
@endsection('content')

