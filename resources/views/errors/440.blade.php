@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Time-out')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, client's session has expired.</div>
    </div>
@endsection('content')

