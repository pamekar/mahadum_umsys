@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Not Found')
@section('content')
    <div class="row invisible" data-toggle="appear">
        <div class="col-sm-6 text-center text-sm-right">
            <div class="display-1 text-danger font-w700">404</div>
        </div>
        <div class="col-sm-6 text-center d-sm-flex align-items-sm-center">
            <div class="display-1 text-muted font-w300">Error</div>
        </div>
    </div>
    <h1 class="h2 font-w700 mt-5 mb-3 invisible" data-toggle="appear"
        data-class="animated fadeInUp" data-timeout="300">Oops.. You just found an error
        page..</h1>
    <h2 class="h3 font-w400 text-muted mb-5 invisible" data-toggle="appear"
        data-class="animated fadeInUp" data-timeout="450">We are sorry but the page you are
        looking for was not found..</h2>

@endsection

