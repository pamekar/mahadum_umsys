@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Not Implemented')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, server could not fulfil the request.</div>
    </div>
@endsection('content')

