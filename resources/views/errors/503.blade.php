@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Service Unavailable')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, service is temporarily
            unavailable.
        </div>
    </div>
@endsection('content')

