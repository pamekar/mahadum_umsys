@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.error') @section('title','Bad Request')
@section('content')
    <div class="content">
        <div class="title text-alert">Sorry, the server cannot process the request due to apparent client error.</div>
    </div>
@endsection('content')

