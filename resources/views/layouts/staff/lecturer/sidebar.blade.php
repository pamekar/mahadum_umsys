<div class="bg-header-dark">
    <div class="content-header bg-white-10">
        <a class="link-fx font-w600 font-size-lg text-white" href="{{url('')}}">
            <span class="text-white-75">{{config('app.abbr')}}</span> <span class="text-white">UMS</span>
        </a>
        <div>
            <a class="js-class-toggle text-white-75" data-target="#sidebar-style-toggler"
               data-class="fa-toggle-off fa-toggle-on" data-toggle="layout"
               data-action="sidebar_style_toggle" href="javascript:void(0)">
                <i class="fa fa-toggle-off" id="sidebar-style-toggler"></i>
            </a>
            <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close"
               href="javascript:void(0)">
                <i class="fa fa-times-circle"></i>
            </a>
        </div>
    </div>
</div>
<div class="content-side content-side-full">
    <ul class="nav-main">
        <li class="nav-main-item">
            <a class="nav-main-link active" href="{{url('home')}}">
                <i class="nav-main-link-icon si si-cursor"></i>
                <span class="nav-main-link-name">Dashboard</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('/staff/lecturer/notices')}}">
                <i class="nav-main-link-icon si si-cursor"></i>
                <span class="nav-main-link-name">Notices</span>
            </a>
        </li>
        <li class="nav-main-heading">Courses</li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Results</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/results/view')}}">
                        <span class="nav-main-link-name">Check results</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/results/download')}}">
                        <span class="nav-main-link-name">Download results</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/results/upload')}}">
                        <span class="nav-main-link-name">Upload results</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Registration</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/registration/view')}}">
                        <span class="nav-main-link-name">View list</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/registration/download')}}">
                        <span class="nav-main-link-name">Download list</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Attendance</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/course/view')}}">
                        <span class="nav-main-link-name">View list</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/course/semester')}}">
                        <span class="nav-main-link-name">Download list</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/course/departmental')}}">
                        <span class="nav-main-link-name">Upload List</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">View Courses</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/current-level')}}">
                        <span class="nav-main-link-name">Show your courses</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/current-semester')}}">
                        <span class="nav-main-link-name">Show Current Semester</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/courses/level')}}">
                        <span class="nav-main-link-name">Departmental Courses</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-heading">Verification</li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Verify result</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/result/code')}}">
                        <span class="nav-main-link-name">Use code</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/result/matricno')}}">
                        <span class="nav-main-link-name">Matric No</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/result/search')}}">
                        <span class="nav-main-link-name">Search Students</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Verify Registered Courses</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/course/code')}}">
                        <span class="nav-main-link-name">Use code</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/course/matricno')}}">
                        <span class="nav-main-link-name">Matric No</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/course/search')}}">
                        <span class="nav-main-link-name">Search Students</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Verify Fees</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/fee/code')}}">
                        <span class="nav-main-link-name">Use code</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/fee/matricno')}}">
                        <span class="nav-main-link-name">Matric No</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/fee/search')}}">
                        <span class="nav-main-link-name">Search Students</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Verify Students</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/student/code')}}">
                        <span class="nav-main-link-name">Use code</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/student/matricno')}}">
                        <span class="nav-main-link-name">Matric No</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/student/search')}}">
                        <span class="nav-main-link-name">Search Students</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Verify Exams</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/exam/code')}}">
                        <span class="nav-main-link-name">Use code</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/exam/matricno')}}">
                        <span class="nav-main-link-name">Matric No</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/staff/lecturer/verification/exam/search')}}">
                        <span class="nav-main-link-name">Search exams</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-heading d-block">Attendance</li>
        <li class="nav-main-item d-block">
            <a class="nav-main-link" href="{{url('staff/lecturer/attendance/create')}}">
                <i class="nav-main-link-icon si si-envelope"></i>
                <span class="nav-main-link-name">Create Attendance</span>
            </a>
        </li>
        <li class="nav-main-item d-block">
            <a class="nav-main-link" href="{{url('staff/lecturer/attendance/update')}}">
                <i class="nav-main-link-icon si si-envelope"></i>
                <span class="nav-main-link-name">Update Attendance</span>
            </a>
        </li>
        <li class="nav-main-item d-block">
            <a class="nav-main-link" href="{{url('staff/lecturer/attendance/view')}}">
                <i class="nav-main-link-icon si si-envelope"></i>
                <span class="nav-main-link-name">View Attendance</span>
            </a>
        </li>
        <li class="nav-main-heading d-block d-md-none">Messages</li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('staff/lecturer/messages/inbox')}}">
                <i class="nav-main-link-icon si si-envelope"></i>
                <span class="nav-main-link-name">Inbox</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('staff/lecturer/messages/starred')}}">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Starred</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('staff/lecturer/messages/sent')}}">
                <i class="nav-main-link-icon si si-share-alt"></i>
                <span class="nav-main-link-name">Sent</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('staff/lecturer/messages/draft')}}">
                <i class="nav-main-link-icon si si-folder-alt"></i>
                <span class="nav-main-link-name">Draft</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('staff/lecturer/messages/trash')}}">
                <i class="nav-main-link-icon si si-trash"></i>
                <span class="nav-main-link-name">Trash</span>
            </a>
        </li>
        <li class="nav-main-heading">Payments</li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="javascript:void(0)">
                <i class="nav-main-link-icon fa fa-money"></i>
                <span class="nav-main-link-name">Fees</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="javascript:void(0)">
                <i class="nav-main-link-icon fa fa-building"></i>
                <span class="nav-main-link-name">Hostel</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('/forums')}}">
                <i class="nav-main-link-icon fa fa-users"></i>
                <span class="nav-main-link-name">Forums</span>
            </a>
        </li>
        <li class="nav-main-item mt-3">
            <a class="btn btn-block btn-outline-primary" href="{{ url('/logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i>
                <span class="nav-main-link-name">Logout</span>
            </a>
        </li>
    </ul>
</div>