<div class="bg-image" style="background-image: url('/{{$public}}/jpg/bg_side_overlay_header.jpg');">
    <div class="bg-success-op">
        <div class="content-header">
            <a class="img-link mr-1" href="{{url('')}}">
                <img class="img-avatar img-avatar48" src="{{asset("$public/".$data->user_meta->photo_location)}}"
                     alt="">
            </a>
            <div class="ml-2">
                <a class="text-white font-w600"
                   href="{{url('')}}">{{$data->user_meta->first_name.' '. $data->user_meta->last_name}}</a>
                <div class="text-white-75 font-size-sm font-italic">{{$data->user_meta->reg_no or null}}</div>
            </div>
            <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout"
               data-action="side_overlay_close">
                <i class="fa fa-times-circle"></i>
            </a>
        </div>
    </div>
</div>
<div class="content-side">
    <div class="block block-transparent pull-x pull-t">
        <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="#so-people">
                    <i class="far fa-fw fa-user-circle"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#so-profile">
                    <i class="far fa-fw fa-edit"></i>
                </a>
            </li>
        </ul>
        <div class="block-content tab-content overflow-hidden">
            <div class="tab-pane pull-x show fade fade-right active" id="so-people" role="tabpanel">
                <div class="block mb-0">
                    <div class="block-content block-content-sm block-content-full bg-body">
                        <span class="text-uppercase font-size-sm font-w700">Your Details</span>
                    </div>
                    <div class="block-content">
                        <ul class="nav-items small">
                            <li class="block block-bordered text-primary">
                                <div class="block-content">
                                    <dl class="dl-horizontal">
                                        <dt>Unique ID</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->uid}}</span>
                                        </dd>
                                        <dt>School</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->user_meta->faculty or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Department</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->user_meta->dept or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Level</dt>
                                        <dd>
                                                                <span class="text-muted">{{$data->level or 'Not Set'}}
                                                                    Level</span>
                                        </dd>
                                        <dt>Mode of Entry</dt>
                                        <dd>
										<span class="text-muted">{{$data->user_meta->mode_of_entry or 'Not
											Set'}}</span>
                                        </dd>
                                        <dt>Award in View</dt>
                                        <dd>
										<span class="text-muted">{{$data->user_meta->award_in_view or 'Not
											Set'}}</span>
                                        </dd>
                                        <dt>Course Duration</dt>
                                        <dd>
                                            <span class="text-muted">{{"Years"}}</span>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <li class="block block-bordered text-info">
                                <div class="block-content">
                                    <dl class="">
                                        <dt>Dean of School</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->deptStaff->dean or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Head of Department</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->deptStaff->hod or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Course Adviser</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->deptStaff->ca or 'Not Set'}}</span>
                                        </dd>
                                        <dt>GPA (Cummulative)</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->gpa_commulative or 'Not Found'}}</span>
                                        </dd>
                                        <dt>GPA (Previous Session)</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->gpa_previous or 'Not Set'}}</span>
                                        </dd>

                                    </dl>
                                </div>
                            </li>
                            <li class="block block-bordered text-danger">
                                <div class="block-content">
                                    <dl class="">
                                        <dt>Outstanding Courses</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->outstanding_courses or null}}</span>
                                        </dd>
                                        <dt>Outstanding Fees</dt>
                                        <dd>
                                            <span class="text-muted">Not Set</span>
                                        </dd>
                                    </dl>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-pane pull-x fade fade-left" id="so-profile" role="tabpanel">
                <form action="https://demo.pixelcave.com/dashmix/be_pages_dashboard.html" method="post"
                      onsubmit="return false;">
                    <div class="block mb-0">
                        <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Personal</span>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" readonly class="form-control" id="staticEmail"
                                       value="{{Auth::user()->name}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="so-profile-name">Name</label>
                                <input type="text" class="form-control" id="so-profile-name"
                                       name="so-profile-name"
                                       value="{{$data->user_meta->first_name." ".$data->user_meta->last_name}}"
                                       readonly>
                            </div>
                            <div class="form-group">
                                <label for="so-profile-email">Email</label>
                                <input type="email" class="form-control" id="so-profile-email"
                                       name="so-profile-email" value="{{Auth::user()->email}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="so-profile-email">Guardian Email</label>
                                <input type="email" class="form-control" id="so-profile-email"
                                       name="so-profile-email"
                                       value="{{$data->user_meta->guardian_email or null}}"
                                       readonly>
                            </div>
                        </div>
                        <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Password Update</span>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="form-group">
                                <label for="so-profile-password">Current Password</label>
                                <input type="password" class="form-control" id="so-profile-password"
                                       name="so-profile-password">
                            </div>
                            <div class="form-group">
                                <label for="so-profile-new-password">New Password</label>
                                <input type="password" class="form-control" id="so-profile-new-password"
                                       name="so-profile-new-password">
                            </div>
                            <div class="form-group">
                                <label for="so-profile-new-password-confirm">Confirm New Password</label>
                                <input type="password" class="form-control"
                                       id="so-profile-new-password-confirm"
                                       name="so-profile-new-password-confirm">
                            </div>
                        </div>
                        {{--<div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Options</span>
                        </div>
                        <div class="block-content">
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="so-settings-status"
                                       name="so-settings-status" value="1">
                                <label class="custom-control-label" for="so-settings-status">Online
                                    Status</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                Make your online status visible to other users of your app
                            </p>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input"
                                       id="so-settings-notifications" name="so-settings-notifications"
                                       value="1" checked>
                                <label class="custom-control-label" for="so-settings-notifications">Notifications</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                Receive desktop notifications regarding your projects and sales
                            </p>
                            <div class="custom-control custom-checkbox mb-1">
                                <input type="checkbox" class="custom-control-input" id="so-settings-updates"
                                       name="so-settings-updates" value="1" checked>
                                <label class="custom-control-label" for="so-settings-updates">Auto
                                    Updates</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                If enabled, we will keep all your applications and servers up to date with
                                the most recent features automatically
                            </p>
                        </div>--}}
                        <div class="block-content row justify-content-center border-top">
                            <div class="col-9">
                                <button type="submit" class="btn btn-block btn-hero-primary">
                                    <i class="fa fa-fw fa-save mr-1"></i> Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>