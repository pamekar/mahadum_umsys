@php    $public='';    if(config('app.env') == 'production')    $public ='/public'; @endphp
@inject('request', 'Illuminate\Http\Request')
<div class="bg-header-dark">
    <div class="content-header bg-white-10">
        <a class="link-fx font-w600 font-size-lg text-white" href="{{url('')}}">
            <span class="text-white-75">{{config('app.abbr')}}</span> <span class="text-white">UMS</span>
        </a>
        <div>
            <a class="js-class-toggle text-white-75" data-target="#sidebar-style-toggler"
               data-class="fa-toggle-off fa-toggle-on" data-toggle="layout"
               data-action="sidebar_style_toggle" href="javascript:void(0)">
                <i class="nav-main-link-icon fa fa-toggle-off" id="sidebar-style-toggler"></i>
            </a>
            <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close"
               href="javascript:void(0)">
                <i class="nav-main-link-icon fa fa-times-circle"></i>
            </a>
        </div>
    </div>
</div>
<div class="content-side content-side-full">
    <ul class="nav-main">
        <li class="nav-main-item">
            <a class="nav-main-link {{ $request->segment(1) == 'home' ? 'active' : '' }}" href="{{ url('/') }}">
                <i class="nav-main-link-icon fa fa-wrench"></i>
                <span class="nav-main-link-name">@lang('global.app_dashboard')</span>
            </a>
        </li>


        @can('user_management_access')
            <li class="nav-main-item">
                <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true" aria-expanded="false" href="#">
                    <i class="nav-main-link-icon fa fa-users"></i>
                    <span class="nav-main-link-name">@lang('global.user-management.title')</span>
                </a>
                <ul class="nav-main-submenu">

                    @can('permission_access')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ $request->segment(2) == 'permissions' ? 'active active-sub' : '' }}" href="{{ route('admin.permissions.index') }}">
                                <i class="nav-main-link-icon fa fa-briefcase"></i>
                                <span class="nav-main-link-name">
                                @lang('global.permissions.title')
                            </span>
                            </a>
                        </li>
                    @endcan
                    @can('role_access')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ $request->segment(2) == 'roles' ? 'active active-sub' : '' }}" href="{{ route('admin.roles.index') }}">
                                <i class="nav-main-link-icon fa fa-briefcase"></i>
                                <span class="nav-main-link-name">
                                @lang('global.roles.title')
                            </span>
                            </a>
                        </li>
                    @endcan
                    @can('user_access')
                        <li class="nav-main-item">
                            <a class="nav-main-link {{ $request->segment(2) == 'users' ? 'active active-sub' : '' }}" href="{{ route('admin.users.index') }}">
                                <i class="nav-main-link-icon fa fa-user"></i>
                                <span class="nav-main-link-name">
                                @lang('global.users.title')
                            </span>
                            </a>
                        </li>
                    @endcan
                </ul>
            </li>
        @endcan
        @can('course_access')
            <li class="nav-main-item">
                <a class="nav-main-link {{ $request->segment(2) == 'courses' ? 'active' : '' }}" href="{{ route('admin.courses.index') }}">
                    <i class="nav-main-link-icon fa fa-gears"></i>
                    <span class="nav-main-link-name">@lang('global.courses.title')</span>
                </a>
            </li>
        @endcan

        @can('lesson_access')
            <li class="nav-main-item">
                <a class="nav-main-link {{ $request->segment(2) == 'lessons' ? 'active' : '' }}" href="{{ route('admin.lessons.index') }}">
                    <i class="nav-main-link-icon fa fa-gears"></i>
                    <span class="nav-main-link-name">@lang('global.lessons.title')</span>
                </a>
            </li>
        @endcan

        @can('question_access')
            <li class="nav-main-item">
                <a class="nav-main-link {{ $request->segment(2) == 'questions' ? 'active' : '' }}" href="{{ route('admin.questions.index') }}">
                    <i class="nav-main-link-icon fa fa-question"></i>
                    <span class="nav-main-link-name">@lang('global.questions.title')</span>
                </a>
            </li>
        @endcan

        @can('questions_option_access')
            <li class="nav-main-item">
                <a class="nav-main-link {{ $request->segment(2) == 'questions_options' ? 'active' : '' }}" href="{{ route('admin.questions_options.index') }}">
                    <i class="nav-main-link-icon fa fa-gears"></i>
                    <span class="nav-main-link-name">@lang('global.questions-options.title')</span>
                </a>
            </li>
        @endcan

        @can('test_access')
            <li class="nav-main-item">
                <a class="nav-main-link {{ $request->segment(2) == 'tests' ? 'active' : '' }}" href="{{ route('admin.tests.index') }}">
                    <i class="nav-main-link-icon fa fa-gears"></i>
                    <span class="nav-main-link-name">@lang('global.tests.title')</span>
                </a>
            </li>
        @endcan


        <li class="nav-main-item">
            <a class="nav-main-link {{ $request->segment(2) == 'change_password' ? 'active' : '' }}" href="{{ route('auth.change_password') }}">
                <i class="nav-main-link-icon fa fa-key"></i>
                <span class="nav-main-link-name">Change password</span>
            </a>
        </li>

        <li class="nav-main-item mt-3">
            <a class="btn btn-block btn-outline-primary" href="{{ url('/logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="nav-main-link-icon far fa-fw fa-arrow-alt-circle-left mr-1"></i>
                <span class="nav-main-link-name">Logout</span>
            </a>
        </li>
    </ul>
</div>