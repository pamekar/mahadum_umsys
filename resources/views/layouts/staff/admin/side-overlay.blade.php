<div class="bg-image" style="background-image: url('/{{$public}}/jpg/bg_side_overlay_header.jpg');">
    <div class="bg-success-op">
        <div class="content-header">
            <a class="img-link mr-1" href="{{url('')}}">
                <img class="img-avatar img-avatar48" src=""
                     alt="">
            </a>
            <div class="ml-2">
                <a class="text-white font-w600"
                   href="{{url('')}}">{{Auth::user()->name}}</a>
                <div class="text-white-75 font-size-sm font-italic">{{Auth::user()->email}}</div>
            </div>
            <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout"
               data-action="side_overlay_close">
                <i class="fa fa-times-circle"></i>
            </a>
        </div>
    </div>
</div>
<div class="content-side">
    <div class="block block-transparent pull-x pull-t">
        <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="#so-profile">
                    <i class="far fa-fw fa-edit"></i>
                </a>
            </li>
        </ul>
        <div class="block-content tab-content overflow-hidden">
            <div class="tab-pane pull-x show fade fade-left active" id="so-profile" role="tabpanel">
                <form action="javascript:void(0)" method="post">
                    <div class="block mb-0">
                        <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Personal</span>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="form-group">
                                <label>Username</label>
                                <input type="text" readonly class="form-control" id="staticEmail"
                                       value="{{Auth::user()->name}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="so-profile-email">Email</label>
                                <input type="email" class="form-control" id="so-profile-email"
                                       name="so-profile-email" value="{{Auth::user()->email}}" readonly>
                            </div>
                            <div class="form-group">
                                <label for="so-profile-email">Guardian Email</label>
                                <input type="email" class="form-control" id="so-profile-email"
                                       name="so-profile-email"
                                       value="{{$data->user_meta->guardian_email or null}}"
                                       readonly>
                            </div>
                        </div>
                        <div class="block-content block-content-sm block-content-full bg-body">
                            <span class="text-uppercase font-size-sm font-w700">Password Update</span>
                        </div>
                        <div class="block-content block-content-full">
                            <div class="form-group">
                                <label for="so-profile-password">Current Password</label>
                                <input type="password" class="form-control" id="so-profile-password"
                                       name="so-profile-password">
                            </div>
                            <div class="form-group">
                                <label for="so-profile-new-password">New Password</label>
                                <input type="password" class="form-control" id="so-profile-new-password"
                                       name="so-profile-new-password">
                            </div>
                            <div class="form-group">
                                <label for="so-profile-new-password-confirm">Confirm New Password</label>
                                <input type="password" class="form-control"
                                       id="so-profile-new-password-confirm"
                                       name="so-profile-new-password-confirm">
                            </div>
                        </div>
                        <div class="block-content row justify-content-center border-top">
                            <div class="col-9">
                                <button type="submit" class="btn btn-block btn-hero-primary">
                                    <i class="fa fa-fw fa-save mr-1"></i> Save
                                </button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>