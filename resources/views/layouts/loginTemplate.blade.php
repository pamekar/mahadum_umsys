@php
    $public = '';
    if (config('app.env') == 'production')
        $public = 'public/';
@endphp
<html lang="en">
<head>
    <title>@yield('title') - MAHADUM</title>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-sale=1, shrink-to-fit=no"
    >
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset($public.'images/global/_icon.ico')}}"
          type="image/x-icon"
    >
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/css1f05.css?family=Nunito+Sans:300,400,400i,600,700')}}">
    <link rel="stylesheet" id="css-main" href="{{asset($public.'/css/dashmix.min-1.1.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/global.css')}}">
    @yield('style')

</head>
<body class="mahadum">
<div id="page-container">
    <main id="main-container">
        <div class="bg-image" style="background-image: url('/{{$public}}jpg/photo22%402x.jpg');">
            <div class="row no-gutters bg-primary-op">
                <div class="hero-static col-md-6 d-flex align-items-center bg-white">
                    <div class="p-3 w-100">
                        @yield('content')
                    </div>
                </div>
                <div class="hero-static col-md-6 d-none d-md-flex align-items-md-center justify-content-md-center text-md-center">
                    <div class="p-3">
                        <p class="display-4 font-w700 text-white mb-3">
                            {{config('app.name')}}
                        </p>
                        <p class="font-size-lg font-w600 text-white-75 mb-0">
                            Copyright &copy; <span class="js-year-copy">{{date('Y')}}</span>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="{{asset($public.'/js/dashmix.core.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/dashmix.app.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/jquery.validate.min.js')}}"></script>
<script src="{{asset($public.'/js/op_auth_signin.min.js')}}"></script>
</body>

</html>