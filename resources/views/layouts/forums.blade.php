<?php
$public = '';
if (config('app.env') == 'production')
    $public = 'public';
?>
<html lang="en">
<head>
    @if( Request::is( Config::get('chatter.routes.home')) )
        <title>Title for your forum homepage -  Website Name</title>
    @elseif( Request::is( Config::get('chatter.routes.home') . '/' . Config::get('chatter.routes.category') . '/*' ) && isset( $discussion ) )
        <title>{{ $discussion->category->name }} - Website Name</title>
    @elseif( Request::is( Config::get('chatter.routes.home') . '/*' ) && isset($discussion->title))
        <title>{{ $discussion->title }} - Website Name</title>
    @endif  
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-sale=1, shrink-to-fit=no"
    >
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset($public.'images/global/_icon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/css1f05.css?family=Nunito+Sans:300,400,400i,600,700')}}">
    <link rel="stylesheet" id="css-main" href="{{asset($public.'/css/dashmix.min-1.1.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/global.css')}}">
    @yield('css')
    @yield('style')

</head>
<body class="mahadum">
<div id="page-container" class="enable-page-overlay side-scroll main-content-boxed">
    <aside id="side-overlay">
        <div class="bg-image" style="background-image: url('/{{$public}}/jpg/bg_side_overlay_header.jpg');">
            <div class="bg-success-op">
                <div class="content-header">
                    <a class="img-link mr-1" href="{{url('')}}">
                        <img class="img-avatar img-avatar48" src=""
                             alt="">
                    </a>
                    <div class="ml-2">
                        <a class="text-white font-w600"
                           href="javascript:void(0)">{{Auth::user()->name}}</a>
                    </div>
                    <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout"
                       data-action="side_overlay_close">
                        <i class="fa fa-times-circle"></i>
                    </a>
                </div>
            </div>
        </div>
        <div class="content-side">
            <div class="block block-transparent pull-x pull-t">
                <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link" href="#so-profile">
                            <i class="far fa-fw fa-edit"></i>
                        </a>
                    </li>
                </ul>
                <div class="block-content tab-content overflow-hidden">
                    <div class="tab-pane pull-x show fade fade-left active" id="so-profile" role="tabpanel">
                        <form action="https://demo.pixelcave.com/dashmix/be_pages_dashboard.html" method="post"
                              onsubmit="return false;">
                            <div class="block mb-0">
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Personal</span>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" readonly class="form-control" id="staticEmail"
                                               value="{{Auth::user()->name}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="so-profile-email">Email</label>
                                        <input type="email" class="form-control" id="so-profile-email"
                                               name="so-profile-email" value="{{Auth::user()->email}}" readonly>
                                    </div>
                                </div>
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Password Update</span>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="form-group">
                                        <label for="so-profile-password">Current Password</label>
                                        <input type="password" class="form-control" id="so-profile-password"
                                               name="so-profile-password">
                                    </div>
                                    <div class="form-group">
                                        <label for="so-profile-new-password">New Password</label>
                                        <input type="password" class="form-control" id="so-profile-new-password"
                                               name="so-profile-new-password">
                                    </div>
                                    <div class="form-group">
                                        <label for="so-profile-new-password-confirm">Confirm New Password</label>
                                        <input type="password" class="form-control"
                                               id="so-profile-new-password-confirm"
                                               name="so-profile-new-password-confirm">
                                    </div>
                                </div>
                                <div class="block-content row justify-content-center border-top">
                                    <div class="col-9">
                                        <button type="submit" class="btn btn-block btn-hero-primary">
                                            <i class="fa fa-fw fa-save mr-1"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </aside>
    <header id="page-header">
        <div class="content-header">
            <div>
                <a class="link-fx font-size-lg text-dual" href="{{url('/')}}">
                    <span class="font-w700 text-dual">{{config('app.abbr')}}</span> <span class="font-w300">UMS</span>
                </a>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ url()->previous() }}"
                   class="btn btn-sm btn-dual">
                    <i class="fa fa-fw fa-arrow-alt-circle-left mr-1"></i> Back
                </a>
                <a href="{{ url('/') }}"
                   class="btn btn-sm btn-dual">
                    <i class="fa fa-fw fa-home mr-1"></i> Home
                </a>
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="btn btn-sm btn-dual">
                    <i class="fa fa-fw fa-sign-out-alt mr-1"></i> Logout
                </a>
                <button type="button" class="btn btn-sm btn-dual" data-toggle="layout"
                        data-action="side_overlay_toggle">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
            </div>
        </div>
        <div id="page-header-loader" class="overlay-header bg-white">
            <div class="content-header">
                <div class="w-100 text-center">
                    <i class="fa fa-fw fa-2x fa-sun fa-spin"></i>
                </div>
            </div>
        </div>
    </header>
    <main id="main-container">
        @yield('content')
    </main>
    <footer id="page-footer" class="bg-body-light">
        <div class="content py-3">
            <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                    Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600"
                                                                               href="{{config('app.designer_url')}}"
                                                                               target="_blank">{{config('app.designer')}}</a>
                </div>
                <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
                    <a class="font-w600" href="{{url('')}}" target="_blank">{{config('app.name')}}</a> &copy; <span
                            data-toggle="year-copy">{{date('Y')}}</span>
                </div>
            </div>
        </div>
    </footer>
</div>
<form id="logout-form" action="{{ url('/logout') }}"
      method="POST" style="display: none;"
>{{ csrf_field() }}</form>
</body>
<script src="{{asset($public.'/js/dashmix.core.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/dashmix.app.min-1.1.js')}}"></script>
@yield('js')
@yield('script')
</html>