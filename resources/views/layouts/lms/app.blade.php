@php
    $public = '';
    if (config('app.env') == 'production')
    $public = 'public';
@endphp
<html lang="en">
<head>
    <title>@yield('title') - MAHADUM</title>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-sale=1, shrink-to-fit=no"
    >
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset($public.'images/global/_icon.ico')}}"
          type="image/x-icon"
    >
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/css1f05.css?family=Nunito+Sans:300,400,400i,600,700')}}">
    <link rel="stylesheet" id="css-main" href="{{asset($public.'/css/dashmix.min-1.1.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/global.css')}}">
    @yield('style')
    <style>
        .btn-sm{
            padding: 1px 5px;
            font-size: 12px;
            line-height: 1.5;
            border-radius: 3px;
        }
    </style>
</head>

<body class="mahadum">
@if(isset($welcome))
    <div id="page-loader" class="show bg-primary"></div>
@endif
<div id="page-container"
     class="sidebar-o enable-page-overlay side-scroll page-header-dark page-header-fixed">
    <aside id="side-overlay">
        @include('layouts.staff.admin.side-overlay')
    </aside>
    <nav id="sidebar" aria-label="Main Navigation">
        @include('layouts.staff.admin.sidebar')
    </nav>
    <header id="page-header">
        <div class="content-header">
            <div>
                <button type="button" class="btn btn-dual mr-1" data-toggle="layout" data-action="sidebar_toggle">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="header_search_on">
                    <i class="fa fa-fw fa-search"></i> <span class="ml-1 d-none d-sm-inline-block">Search</span>
                </button>
            </div>
            <div>
                <div class="dropdown d-inline-block">
                    <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user d-sm-none"></i>
                        <span class="d-none d-sm-inline-block">{{Auth::user()->name}}</span>
                        <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown">
                        <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                            User Options
                        </div>
                        <div class="p-2">
                            <a class="dropdown-item" href="{{url('/undergraduate/profile')}}">
                                <i class="far fa-fw fa-user mr-1"></i> Profile
                            </a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout"
                               data-action="side_overlay_toggle">
                                <i class="far fa-fw fa-building mr-1"></i> Settings
                            </a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i> Sign Out
                            </a>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="far fa-fw fa-list-alt"></i>
                </button>
            </div>
        </div>
        <div id="page-header-search" class="overlay-header bg-primary">
            <div class="content-header">
                <form class="w-100" action="{{url('/undergraduate/search')}}"
                      method="get">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-primary" data-toggle="layout"
                                    data-action="header_search_off">
                                <i class="fa fa-fw fa-times-circle"></i>
                            </button>
                        </div>
                        <input type="text" class="form-control border-0" placeholder="Search or hit ESC.."
                               id="page-header-search-input" value="{{$query or null}}" name="search">
                    </div>
                </form>
            </div>
        </div>
        <div id="page-header-loader" class="overlay-header bg-primary-darker">
            <div class="content-header">
                <div class="w-100 text-center">
                    <span class="text-white fa-2x">Sending</span>
                    <i class="fa fa-fw fa-2x fa-spinner fa-spin text-white"></i>
                </div>
            </div>
        </div>
    </header>
    <main id="main-container">
        <div class="bg-body-light">
            <div class="block">
                <div class="block-content">
                    @yield('content')
                </div>
            </div>
        </div>
    </main>
    <footer id="page-footer" class="bg-body-light">
        <div class="content py-3">
            <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                    Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600"
                                                                               href="{{config('app.designer_url')}}"
                                                                               target="_blank">{{config('app.designer')}}</a>
                </div>
                <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
                    <a class="font-w600" href="{{url('')}}" target="_blank">{{config('app.name')}}</a> &copy; <span
                            data-toggle="year-copy">{{date('Y')}}</span>
                </div>
            </div>
        </div>
    </footer>
</div>
<form id="logout-form" action="{{ url('/logout') }}"
      method="POST" style="display: none;"
>{{ csrf_field() }}</form>
<script src="{{asset($public.'/js/dashmix.core.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/dashmix.app.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/bootstrap-notify.min.js')}}"></script>
<script src="{{asset($public.'/js/jquery.datatables.min.js')}}"></script>
<script src="{{asset($public.'/js/datatables.bootstrap4.min.js')}}"></script>
<script src="{{asset($public.'/js/datatables.buttons.min.js')}}"></script>
<script src="{{asset($public.'/js/buttons.print.min.js')}}"></script>
<script src="{{asset($public.'/js/buttons.html5.min.js')}}"></script>
<script src="{{asset($public.'/js/buttons.flash.min.js')}}"></script>
<script src="{{asset($public.'/js/buttons.colvis.min.js')}}"></script>
<script src="{{asset($public.'/js/be_tables_datatables.min.js')}}"></script>
<script src="{{asset($public.'/js/global.js')}}"></script>

<script>
    jQuery(function () {
        Dashmix.helpers('sparkline');
        @if(isset($notification))
        Dashmix.helpers('notify', {
            type: '{{$notification->type}}',
            icon: 'fa fa-info mr-1',
            message: '{{$notification->message}}'
        });
        @endif
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
@yield('script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5b8a8927f31d0f771d845bee/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>