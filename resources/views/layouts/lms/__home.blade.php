@php  $public='';    if(config('app.env') == 'production')    $public ='/public'; @endphp
        <!DOCTYPE html>
<html lang="en-US">
<meta http-equiv="content-type" content="text/html;charset=utf-8"/>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
    <style type="text/css" media="all">:root {
        }</style>
    <style type="text/css" media="all">
        .wishlist_table .add_to_cart, a.add_to_wishlist.button.alt {
            border-radius: 16px;
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px
        }
    </style>
    <style type="text/css" media="all">img.wp-smiley, img.emoji {
            display: inline !important;
            border: 0 !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -.1em !important;
            background: none !important;
            padding: 0 !important
        }</style>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_a43720e2d4481b63a55eba397cd2e161.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="screen" href="{{asset("$public/lms/css/breeze_39d9541a29871540df34bf21cd841f5d.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="screen" href="{{asset("$public/lms/css/breeze_0f1900834730b5c53a2a52f4836af68c.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_2dc64ceab77398d8f449e502a6d80bf8.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_044a85b2d889e24e1c1fe31458a481a9.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_0a520f29b989692aa063a95af8be3289.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_cb9e387abad67d3037a76ac80052641b.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_b6e72d9e15aa57f57d9004429e817599.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_19352647d0e6a6945e91cc6b7314b1f3.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_2fa63af70aa66ce2df2eb9610d1fb97d.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_09b52458c83fb1eb2c15ab4e0bd29d98.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_8c7803cbeb4dec71862250e6ae191f1f.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_8a4d8bd415f158c0847433ccab03a70a.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_5148557abb0aa5da46d3aaba42404c0f.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_ad2c2ad0d90cb083a4e6a592ea25ce2e.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_290fb39b9bec5e976d1d3af1532a5549.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_3ec7b267302ca4fc3cfc4257dc679a48.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_bd5e86c2d828ecb28e814e5342a1f1fa.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_5ae8388abe12efec006155ad5d7675c9.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_c1d5a507679fe94cf04915119ba7f3e1.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_7bd22f6a3a8f2461d28c783c86024439.css")}}"
          rel="stylesheet"/>
    <style type="text/css" media="all">
        .wcs-single__action .wcs-btn--action {
            color: rgba(255, 255, 255, 1);
            background-color: #bd322c
        }
    </style>
    <style type="text/css" media="all">.woocommerce form .form-row .required {
            visibility: visible
        }</style>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_2db6abd8ee6ce8cc089d06e03f60f2ee.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="only screen and (max-width: 768px)"
          href="{{asset("$public/lms/css/breeze_7926c39f057980735c1550a8ac109a73.css")}}" rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_d700e65fafd2f4695ec8129222ff44c9.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_3c36c6a9fd2d214e18bdc25af21f5bf0.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_cea29226f864c2aaa2c93804fd4ba435.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_770676eb672ea9bdf362d09d222ed5af.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_766ed914efa69542cf74ca8a1e17da03.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_b60d6d38bfe00f0f0ee7d5be4fb0c056.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_89e7b4fa4146f0ce22974d13af7f0a3c.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_fe7206c883dec7deb758efa2499209b1.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_07d5ba34443c018cf1fa7cbbf03497aa.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_793424396b9eca601db5f21fc3900f94.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_274ea02a878aa54fc9c996d512847246.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_8427c03d2aa9bd9e44ecf0ef911038d3.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_417e57957703a22b2d00d21de15d252e.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_1112e9eed585727e608d941b42cd7aca.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_8fd8203e1924c02004304aa0c410370a.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_8728faac37670405427847abb9b03215.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_2592d5559789662073b6715a25df3a0b.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_089da5d03021e3cac7754716e8977f6a.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_112bb43edd920f6b5c9fe80c03e6991d.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_3dd006e185b30492d776afe414e3d2a1.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_61c0042e3dc244f14770010cb255b0eb.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_14b0320b10c7ebd312683b93359b22f6.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_4bf9eff1e6064a53228e520c0611f296.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_8c209cbafac1216075f03435c7705925.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_5db9530a76cbb16c46c69610840becc7.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_3673312a9e30ddef79c3da7ca6858434.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_604c7b326eee1108b48cb4d1c02a7d6e.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_4e61f14e32ab7965f151369868e984a1.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_cd0cca0d304378c43d94190f895ce4f0.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_5d7c69d78f09a56033dac56078a2f1d3.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_3bd88ffb653269fdc0212d000cfade0d.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_33c44566ea75f795dc59d8427f0c162c.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_f6ec8f2fd97d4a30c09ccbbfdcb3a60a.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_6c4ea6aab0d47a52f1a83978d0b369ac.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_6e1a0545029d04c6bb9abc3345ae6909.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_de976eb8cff2586ac223ac5dd0d9eeba.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_2b08a74922d418ad884e8fa49d815ab9.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_a2d52e7e0a177ab342b5628eb533afdf.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_ee02eaa0ea6aa94ec8660226fd42ba80.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_9979a7c4c7c4dde620431bc7688fa908.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_96e615d5ed4cfeddca0aca16a0e26cd9.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_b52cb5a3195c4d87920e6a9d19fffd3a.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_765d980acc6013377e8d28b9bdea6c6b.css")}}"
          rel="stylesheet"/>
    <style type="text/css" media="all">
        .recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important
        }
    </style>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_37edb2bc7a6b9b3e0fc67f5e6ce85d52.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_bcaaf24114eb065badcc710c0a6840a9.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_cab15e672ecdc26982f5cf6233bb3e19.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_2fdb6cab047fddb6239161a3b3469e7e.css")}}"
          rel="stylesheet"/>
    <link type="text/css" media="all" href="{{asset("$public/lms/css/breeze_1553308e809728b4467b0dad5ccbc301.css")}}"
          rel="stylesheet"/>
    <title>LMS &#8211; {{config('app.name')}}</title>
    <link rel='dns-prefetch' href='{{asset("$public/lms/index-13.html")}}'/>
    <link rel='dns-prefetch' href='{{asset("$public/lms/index-14.html")}}'/>
    <link rel="alternate" type="application/rss+xml" title="Default &raquo; Feed"
          href="{{asset("$public/lms/index-15.html")}}"/>
    <link rel="alternate" type="application/rss+xml" title="Default &raquo; Comments Feed"
          href="{{asset("$public/lms/index-16.html")}}"/>
    <link rel="alternate" type="text/calendar" title="Default &raquo; iCal Feed"
          href="{{asset("$public/lms/indexedf3.html?ical=1")}}"/>
    <script type="text/javascript">window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "http:\/\/kalvi.dttheme.com\/default\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [
                            55356,
                            56826,
                            8203,
                            55356,
                            56819
                        ])) && (b = d([
                            55356,
                            57332,
                            56128,
                            56423,
                            56128,
                            56418,
                            56128,
                            56421,
                            56128,
                            56430,
                            56128,
                            56423,
                            56128,
                            56447
                        ], [
                            55356,
                            57332,
                            8203,
                            56128,
                            56423,
                            8203,
                            56128,
                            56418,
                            8203,
                            56128,
                            56421,
                            8203,
                            56128,
                            56430,
                            8203,
                            56128,
                            56423,
                            8203,
                            56128,
                            56447
                        ]), !b);
                    case"emoji":
                        return b = d([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);</script>
    <link rel='stylesheet' id='dtlms-google-fonts-css' href='css/cssd3c9.css?family=Poppins&amp;ver=4.9.8'
          type='text/css' media='all'/>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/jquery.js?ver=1.12.4")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/jquery-migrate.min.js?ver=1.4.1")}}'></script>
    <script type='text/javascript'>/*  */
        var TribeMiniCalendar = {"ajaxurl": "http:\/\/kalvi.dttheme.com\/default\/wp-admin\/admin-ajax.php"};
        /*  */</script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/widget-calendar.js?ver=4.4.27")}}'></script>
    <script type='text/javascript'>/*  */
        var BP_Confirm = {"are_you_sure": "Are you sure?"};
        /*  */</script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/confirm.min.js?ver=3.1.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/widget-members.min.js?ver=3.1.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/jquery-query.min.js?ver=3.1.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/vendor/jquery-cookie.min.js?ver=3.1.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/vendor/jquery-scroll-to.min.js?ver=3.1.0")}}'></script>
    <script type='text/javascript'>/*  */
        var BP_DTheme = {
            "accepted": "Accepted",
            "close": "Close",
            "comments": "comments",
            "leave_group_confirm": "Are you sure you want to leave this group?",
            "mark_as_fav": "Favorite",
            "my_favs": "My Favorites",
            "rejected": "Rejected",
            "remove_fav": "Remove Favorite",
            "show_all": "Show all",
            "show_all_comments": "Show all comments for this thread",
            "show_x_comments": "Show all comments (%d)",
            "unsaved_changes": "Your profile has unsaved changes. If you leave the page, the changes will be lost.",
            "view": "View"
        };
        /*  */</script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/buddypress.min.js?ver=3.1.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/chart.min.js?ver=4.9.8")}}'></script>
    <script type='text/javascript'>/*  */
        var wc_add_to_cart_params = {
            "ajax_url": "\/default\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "\/default\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "http:\/\/kalvi.dttheme.com\/default\/shop\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /*  */</script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/add-to-cart.min.js?ver=3.4.4")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/woocommerce-add-to-cart.js?ver=5.4.7")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/ultimate-params.min.js?ver=3.16.24")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/modernizr.custom.js?ver=4.9.8")}}'></script>
    <link rel='https://api.w.org/' href='{{asset("$public/lms/index-17.html")}}'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="{{asset("$public/lms/php/xmlrpc0db0.php?rsd")}}"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="{{asset("$public/lms/wlwmanifest.html")}}"/>
    <meta name="generator" content="WordPress 4.9.8"/>
    <meta name="generator" content="WooCommerce 3.4.4"/>
    <link rel="canonical" href="index.html"/>
    <link rel='shortlink' href='index.html'/>
    <link rel="alternate" type="application/json+oembed"
          href="{{asset("$public/lms/embed46de.html?url=http%3A%2F%2Fkalvi.dttheme.com%2Fdefault%2F")}}"/>
    <link rel="alternate" type="text/xml+oembed"
          href="{{asset("$public/lms/embeda9b5.html?url=http%3A%2F%2Fkalvi.dttheme.com%2Fdefault%2F&amp;format=xml")}}"/>
    {{--<script type="text/javascript">var ajaxurl = 'http://kalvi.dttheme.com/default/wp-admin/admin-ajax.php';</script>--}}
    <meta name=" tec-api-version
    " content="v1">
    <meta name="tec-api-origin" content="http://kalvi.dttheme.com/default">
    <link rel="https://theeventscalendar.com/" href="{{asset("$public/lms/index-18.html")}}"/>
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="{{asset(" $public/lms/css/vc_lte_ie9.min.css")}}" media="screen"><![endif]-->
    <link rel="icon" href="{{asset("$public/lms/favicon.html")}}" sizes="32x32"/>
    <link rel="icon" href="{{asset("$public/lms/favicon.html")}}" sizes="192x192"/>
    <link rel="apple-touch-icon-precomposed" href="{{asset("$public/lms/favicon.html")}}"/>
    <meta name="msapplication-TileImage"
          content="http://kalvi.dttheme.com/default/wp-content/uploads/sites/3/2017/12/favicon.png"/>
    <noscript>
        <style type="text/css">.wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
</head>
<body class="home-page bp-legacy home page-template-default page page-id-17810 woocommerce-no-js tribe-no-js layout-wide page-with-slider no-breadcrumb wpb-js-composer js-comp-ver-5.4.7 vc_responsive no-js">
<div class="loader">
    <div class="loader-inner"><span class="dot"></span><span class="dot dot1"></span><span class="dot dot2"></span><span
                class="dot dot3"></span><span class="dot dot4"></span></div>
</div>
<div class="wrapper">
    <div class="inner-wrapper">
        <div id="header-wrapper" class="header-top-relative">
            <header id="header">
                <div class="container">
                    <div id="header-4" class="dt-header-tpl header-4"><p>
                        <div data-vc-full-width="true" data-vc-full-width-init="false"
                             class="vc_row wpb_row vc_row-fluid dt-sc-dark-bg vc_custom_1512355560865 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <ul class="dtlms-custom-login ">
                                            <li><a href="#" title="Login" class="dtlms-login-link"
                                                   onclick="return false"><i class="fa fa-unlock-alt"></i>Login</a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <ul id='dt-1511766591436-f4d5ba34-3b04' class='dt-sc-sociable small right'
                                            data-default-style='none'
                                            data-default-border-radius='no'
                                            data-default-shape=''
                                            data-hover-style='filled'
                                            data-hover-border-radius='no'
                                            data-hover-shape='square'
                                        >
                                            <li class="facebook"><a href="#" title="Facebook" target=" _blank"> <span
                                                            class="dt-icon-default"> <span></span> </span> <i></i>Facebook
                                                    <span class="dt-icon-hover"> <span></span> </span> </a></li>
                                            <li class="flickr"><a href="#" title="Flickr" target=" _blank"> <span
                                                            class="dt-icon-default"> <span></span> </span> <i></i>Flickr
                                                    <span class="dt-icon-hover"> <span></span> </span> </a></li>
                                            <li class="google-plus"><a href="#" title="GooglePlus" target=" _blank">
                                                    <span class="dt-icon-default"> <span></span> </span> <i></i>GooglePlus
                                                    <span class="dt-icon-hover"> <span></span> </span> </a></li>
                                            <li class="pinterest"><a href="#" title="Pinterest" target=" _blank"> <span
                                                            class="dt-icon-default"> <span></span> </span> <i></i>Pinterest
                                                    <span class="dt-icon-hover"> <span></span> </span> </a></li>
                                            <li class="twitter"><a href="#" title="Twitter" target=" _blank"> <span
                                                            class="dt-icon-default"> <span></span> </span> <i></i>Twitter
                                                    <span class="dt-icon-hover"> <span></span> </span> </a></li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false"
                             class="vc_row wpb_row vc_row-fluid default-header vc_custom_1514278418729 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-offset-0 vc_col-md-offset-0 vc_col-sm-offset-3">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1514280041619-e4163d98-b76e" class="dt-sc-empty-space"></div>
                                        <div id="dt-1505558235344-642791a5-e86a"
                                             class="dt-logo-container logo-align-left"><a href="index.html"
                                                                                          rel="home"><img
                                                        src="{{asset("$public/lms/logo.html")}}" alt="Default"/></a>
                                        </div>
                                        <div id="1514360282101-befc016f-d57f" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-9">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div data-menu="main-menu" id="dt-1505559553043-0fc3b635-35b1"
                                             class="dt-header-menu mega-menu-page-equal right parallel-with-container"
                                             data-nav-item-divider="none" data-nav-item-highlight="underline"
                                             data-nav-item-display="simple">
                                            <div class="menu-container">
                                                <ul id="menu-main-menu" class="dt-primary-nav " data-menu="main-menu">
                                                    <li class="close-nav"></li>
                                                    <li id="menu-item-22"
                                                        class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home current-menu-ancestor current-menu-parent menu-item-has-children menu-item-22 dt-menu-item-22 ">
                                                        <a href="index.html"
                                                           class="item-has-icon icon-position-left"><span>Home</span></a>
                                                        <ul class="sub-menu is-hidden ">
                                                            <li class="go-back"><a href="javascript:void(0);"></a></li>
                                                            <li class="see-all"></li>
                                                            <li id="menu-item-12529"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-12529 dt-menu-item-12529 ">
                                                                <a href="index.html"
                                                                   class="item-has-icon icon-position-left"><span>Default</span></a>
                                                            </li>
                                                            <li id="menu-item-12530"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12530 dt-menu-item-12530 ">
                                                                <a href="http://kalvi.dttheme.com/online-learning/"
                                                                   class="item-has-icon icon-position-left"><span>Online Learning</span></a>
                                                            </li>
                                                            <li id="menu-item-12531"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12531 dt-menu-item-12531 ">
                                                                <a href="http://kalvi.dttheme.com/one-instructor/"
                                                                   class="item-has-icon icon-position-left"><span>Single Instructor</span></a>
                                                            </li>
                                                            <li id="menu-item-12532"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12532 dt-menu-item-12532 ">
                                                                <a href="http://kalvi.dttheme.com/one-course/"
                                                                   class="item-has-icon icon-position-left"><span>One Course</span></a>
                                                            </li>
                                                            <li id="menu-item-12533"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12533 dt-menu-item-12533 ">
                                                                <a href="http://kalvi.dttheme.com/kalvi-academy/"
                                                                   class="item-has-icon icon-position-left"><span>Kalvi Academy</span></a>
                                                            </li>
                                                            <li id="menu-item-12534"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12534 dt-menu-item-12534 ">
                                                                <a href="http://kalvi.dttheme.com/kindergarden/"
                                                                   class="item-has-icon icon-position-left"><span>Kindergarden</span></a>
                                                            </li>
                                                            <li id="menu-item-12535"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12535 dt-menu-item-12535 ">
                                                                <a href="http://kalvi.dttheme.com/kalvi-university/"
                                                                   class="item-has-icon icon-position-left"><span>Kalvi University</span></a>
                                                            </li>
                                                            <li id="menu-item-12536"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12536 dt-menu-item-12536 ">
                                                                <a href="http://kalvi.dttheme.com/dance-school/"
                                                                   class="item-has-icon icon-position-left"><span>Dance School</span></a>
                                                            </li>
                                                            <li id="menu-item-12537"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12537 dt-menu-item-12537 ">
                                                                <a href="http://kalvi.dttheme.com/points-system/"
                                                                   class="item-has-icon icon-position-left"><span>Points System</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-10794"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-mega-menu menu-item-10794 dt-menu-item-10794 ">
                                                        <a href="http://kalvi.dttheme.com/default/classes-listing/"
                                                           class="item-has-icon icon-position-left"><span>Classes</span></a>
                                                        <ul class="sub-menu is-hidden ">
                                                            <li class="go-back"><a href="javascript:void(0);"></a></li>
                                                            <li class="see-all"></li>
                                                            <li id="menu-item-12616"
                                                                class="menu-item menu-item-type-post_type menu-item-object-dt_mega_menus menu-item-12616 dt-menu-item-12616 ">
                                                                <div class="vc_row wpb_row vc_row-fluid vcr_float_right vc_custom_1527764606184 vc_row-has-fill">
                                                                    <div class="wpb_column vc_column_container vc_col-sm-3 vc_hidden-sm">
                                                                        <div class="vc_column-inner ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                                    <figure class="wpb_wrapper vc_figure">
                                                                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                            <img width="470"
                                                                                                 height="562"
                                                                                                 src="{{asset("$public/lms/menu-banner3-2.html")}}"
                                                                                                 class="vc_single_image-img attachment-full"
                                                                                                 alt=""
                                                                                                 srcset="{{asset("$public/lms/menu-banner3-2.jpg")}} 470w, {{asset("$public/lms/menu-banner3-2-251x300.jpg")}} 251w"
                                                                                                 sizes="(max-width: 470px) 100vw, 470px"/>
                                                                                        </div>
                                                                                    </figure>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-6">
                                                                        <div class="vc_column-inner ">
                                                                            <div class="wpb_wrapper"><h3
                                                                                        class="dtlms-title">Key
                                                                                    Features</h3>
                                                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                                                                        <div class="vc_column-inner ">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512118891114-706f0a72-9eae"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-classes-mega-menu-i-container">
                                                                                                        <ul id="menu-classes-mega-menu-i"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-12481"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12481">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/online-class/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Online Class</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12480"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12480">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/class-with-unlimited-tabs/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Class With Unlimited Tabs</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12478"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12478">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/class-with-purchase-option/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Class With Purchase Option</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12479"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12479">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/class-with-timetable-plugin/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Class With Timetable Plugin</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12477"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12477">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/class-with-certificate-badge/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Class With Certificate &#038; Badge</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-6">
                                                                                        <div class="vc_column-inner ">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512115004595-4cb29ca6-9e63"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-classes-mega-menu-ii-container">
                                                                                                        <ul id="menu-classes-mega-menu-ii"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-12486"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12486">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/onsite-class/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Onsite Class</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12487"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12487">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/featured-class/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Featured Class</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12485"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12485">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/onsite-class-with-start-date/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Onsite Class With Start Date</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12483"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12483">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/onsite-class-with-location-map/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Onsite Class With Location Map</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12484"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_classes menu-item-12484">
                                                                                                                <a href="http://kalvi.dttheme.com/default/classes/class-with-registration-option/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-hand-pointer-o"></i><span>Class With Registration Option</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wpb_column vc_column_container vc_col-sm-3 vc_hidden-sm">
                                                                        <div class="vc_column-inner ">
                                                                            <div class="wpb_wrapper">
                                                                                <div class="wpb_single_image wpb_content_element vc_align_left">
                                                                                    <figure class="wpb_wrapper vc_figure">
                                                                                        <div class="vc_single_image-wrapper   vc_box_border_grey">
                                                                                            <img width="470"
                                                                                                 height="562"
                                                                                                 src="{{asset("$public/lms/menu-banner4-2.html")}}"
                                                                                                 class="vc_single_image-img attachment-full"
                                                                                                 alt=""
                                                                                                 srcset="{{asset("$public/lms/menu-banner4-2.jpg")}} 470w, {{asset("$public/lms/menu-banner4-2-251x300.jpg")}} 251w"
                                                                                                 sizes="(max-width: 470px) 100vw, 470px"/>
                                                                                        </div>
                                                                                    </figure>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-10773"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-mega-menu menu-item-10773 dt-menu-item-10773 ">
                                                        <a href="index-11.html"
                                                           class="item-has-icon icon-position-left"><span>Courses</span></a>
                                                        <ul class="sub-menu is-hidden ">
                                                            <li class="go-back"><a href="javascript:void(0);"></a></li>
                                                            <li class="see-all"></li>
                                                            <li id="menu-item-12615"
                                                                class="menu-item menu-item-type-post_type menu-item-object-dt_mega_menus menu-item-12615 dt-menu-item-12615 ">
                                                                <div style='background-position: right bottom !important;'
                                                                     class="vc_row wpb_row vc_row-fluid vcr_float_right default-menu-bg vc_custom_1533634740727 vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                                                                    <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-4 vc_col-lg-4 vc_col-md-6">
                                                                        <div class="vc_column-inner vc_custom_1514360775656">
                                                                            <div class="wpb_wrapper">
                                                                                <div id="dt-1512125663368-f2a44151-253e"
                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                     data-default-style="none"
                                                                                     data-hover-style="none"
                                                                                     data-link-icon-position="outside"
                                                                                     data-default-decoration="none"
                                                                                     data-hover-decoration="none"
                                                                                     data-divider="yes">
                                                                                    <div class="menu-courses-mega-menu-i-container">
                                                                                        <ul id="menu-courses-mega-menu-i"
                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                            <li id="menu-item-12512"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12512">
                                                                                                <a href="index-10.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Featured Course</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12516"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12516">
                                                                                                <a href="http://kalvi.dttheme.com/default/courses/course-with-forum/"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Forum</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12496"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12496">
                                                                                                <a href="http://kalvi.dttheme.com/default/courses/course-with-members/"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Members</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12494"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12494">
                                                                                                <a href="index-4.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Featured Video</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12495"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12495">
                                                                                                <a href="index-6.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Free Curriculums</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12491"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12491">
                                                                                                <a href="index-7.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Certificate &#038; Badge</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12493"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12493">
                                                                                                <a href="http://kalvi.dttheme.com/default/courses/course-with-drip-feed-curriculum/"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Drip Feed Curriculum</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12513"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12513">
                                                                                                <a href="http://kalvi.dttheme.com/default/courses/course-with-group-news-events/"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Group, News &#038; Events</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-4 vc_col-lg-4 vc_col-md-6">
                                                                        <div class="vc_column-inner vc_custom_1514360761553">
                                                                            <div class="wpb_wrapper">
                                                                                <div id="dt-1512121618864-d6fe62bf-2098"
                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                     data-default-style="none"
                                                                                     data-hover-style="none"
                                                                                     data-link-icon-position="outside"
                                                                                     data-default-decoration="none"
                                                                                     data-hover-decoration="none"
                                                                                     data-divider="yes">
                                                                                    <div class="menu-courses-mega-menu-ii-container">
                                                                                        <ul id="menu-courses-mega-menu-ii"
                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                            <li id="menu-item-12514"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12514">
                                                                                                <a href="index-2.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Free Course</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12515"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12515">
                                                                                                <a href="index-9.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Paid Course</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12501"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12501">
                                                                                                <a href="index-3.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Start Date</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12500"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12500">
                                                                                                <a href="index-5.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Pre-requisite</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12498"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12498">
                                                                                                <a href="http://kalvi.dttheme.com/default/courses/course-with-related-courses/"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Related Courses</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12502"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12502">
                                                                                                <a href="http://kalvi.dttheme.com/default/courses/course-with-student-capacity/"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Student Capacity</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-12497"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-12497">
                                                                                                <a href="index-12.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Media Attachments</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                            <li id="menu-item-16044"
                                                                                                class="menu-item menu-item-type-post_type menu-item-object-dtlms_courses menu-item-16044">
                                                                                                <a href="index-8.html"
                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                            class="menu-item-icon fa fa-book"></i><span>Course With Curriculum Completion Lock</span></a><span
                                                                                                        class="divider"></span>
                                                                                            </li>
                                                                                        </ul>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="wpb_column vc_column_container vc_col-sm-4">
                                                                        <div class="vc_column-inner ">
                                                                            <div class="wpb_wrapper"></div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-13043"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-13043 dt-menu-item-13043 ">
                                                        <a href="http://kalvi.dttheme.com/default/our-packages/"
                                                           class="item-has-icon icon-position-left"><span>Pages</span></a>
                                                        <ul class="sub-menu is-hidden ">
                                                            <li class="go-back"><a href="javascript:void(0);"></a></li>
                                                            <li class="see-all"></li>
                                                            <li id="menu-item-10823"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10823 dt-menu-item-10823 ">
                                                                <a href="http://kalvi.dttheme.com/default/our-packages/"
                                                                   class="item-has-icon icon-position-left"><span>Our Packages</span></a>
                                                            </li>
                                                            <li id="menu-item-10822"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10822 dt-menu-item-10822 ">
                                                                <a href="http://kalvi.dttheme.com/default/our-instructors/"
                                                                   class="item-has-icon icon-position-left"><span>Instructors</span></a>
                                                            </li>
                                                            <li id="menu-item-16252"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-16252 dt-menu-item-16252 ">
                                                                <a href="http://kalvi.dttheme.com/default/features/features-class/"
                                                                   class="item-has-icon icon-position-left"><span>Features</span></a>
                                                                <ul class="sub-menu is-hidden ">
                                                                    <li class="go-back"><a
                                                                                href="javascript:void(0);"></a></li>
                                                                    <li class="see-all"></li>
                                                                    <li id="menu-item-12887"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12887 dt-menu-item-12887 ">
                                                                        <a href="http://kalvi.dttheme.com/default/features/features-class/"
                                                                           class="item-has-icon icon-position-left"><span>Features Class</span></a>
                                                                    </li>
                                                                    <li id="menu-item-12888"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12888 dt-menu-item-12888 ">
                                                                        <a href="http://kalvi.dttheme.com/default/features/features-course/"
                                                                           class="item-has-icon icon-position-left"><span>Features Course</span></a>
                                                                    </li>
                                                                    <li id="menu-item-12890"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12890 dt-menu-item-12890 ">
                                                                        <a href="http://kalvi.dttheme.com/default/features/features-quiz/"
                                                                           class="item-has-icon icon-position-left"><span>Features Quiz</span></a>
                                                                    </li>
                                                                    <li id="menu-item-12889"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12889 dt-menu-item-12889 ">
                                                                        <a href="http://kalvi.dttheme.com/default/features/statistics/"
                                                                           class="item-has-icon icon-position-left"><span>Statistics</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-10149"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10149 dt-menu-item-10149 ">
                                                                <a href="http://kalvi.dttheme.com/default/blog/"
                                                                   class="item-has-icon icon-position-left"><span>Blog</span></a>
                                                                <ul class="sub-menu is-hidden ">
                                                                    <li class="go-back"><a
                                                                                href="javascript:void(0);"></a></li>
                                                                    <li class="see-all"></li>
                                                                    <li id="menu-item-17741"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-17741 dt-menu-item-17741 ">
                                                                        <a href="http://kalvi.dttheme.com/default/blog/"
                                                                           class="item-has-icon icon-position-left"><span>Blog &#8211; Default</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10150"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10150 dt-menu-item-10150 ">
                                                                        <a href="http://kalvi.dttheme.com/default/blog/blog-date-and-author-left/"
                                                                           class="item-has-icon icon-position-left"><span>Blog – Date And Author Left</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10151"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10151 dt-menu-item-10151 ">
                                                                        <a href="http://kalvi.dttheme.com/default/blog/blog-date-left/"
                                                                           class="item-has-icon icon-position-left"><span>Blog – Date Left</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10152"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10152 dt-menu-item-10152 ">
                                                                        <a href="http://kalvi.dttheme.com/default/blog/blog-medium/"
                                                                           class="item-has-icon icon-position-left"><span>Blog – Medium</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10153"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10153 dt-menu-item-10153 ">
                                                                        <a href="http://kalvi.dttheme.com/default/blog/blog-medium-highlight/"
                                                                           class="item-has-icon icon-position-left"><span>Blog – Medium Highlight</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10154"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10154 dt-menu-item-10154 ">
                                                                        <a href="http://kalvi.dttheme.com/default/blog/blog-medium-skin-highlight/"
                                                                           class="item-has-icon icon-position-left"><span>Blog – Medium Skin Highlight</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-10156"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10156 dt-menu-item-10156 ">
                                                                <a href="http://kalvi.dttheme.com/default/portfolio/"
                                                                   class="item-has-icon icon-position-left"><span>Portfolio</span></a>
                                                                <ul class="sub-menu is-hidden ">
                                                                    <li class="go-back"><a
                                                                                href="javascript:void(0);"></a></li>
                                                                    <li class="see-all"></li>
                                                                    <li id="menu-item-10157"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10157 dt-menu-item-10157 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-art/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Art</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10158"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10158 dt-menu-item-10158 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-classic/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Classic</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10159"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10159 dt-menu-item-10159 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-girly/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Girly</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10160"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10160 dt-menu-item-10160 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-icons-only/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Icons Only</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10161"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10161 dt-menu-item-10161 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-minimal-icons/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Minimal Icons</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10162"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10162 dt-menu-item-10162 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-modern-title/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Modern Title</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10163"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10163 dt-menu-item-10163 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-presentation/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Presentation</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10164"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10164 dt-menu-item-10164 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-title-icons-overlay/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio &#8211; Title &#038; Icons Overlay</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10165"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10165 dt-menu-item-10165 ">
                                                                        <a href="http://kalvi.dttheme.com/default/portfolio/portfolio-title-overlay/"
                                                                           class="item-has-icon icon-position-left"><span>Portfolio – Title Overlay</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-10436"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-10436 dt-menu-item-10436 ">
                                                                <a href="http://kalvi.dttheme.com/default/activity/"
                                                                   class="item-has-icon icon-position-left"><span>BuddyPress</span></a>
                                                                <ul class="sub-menu is-hidden ">
                                                                    <li class="go-back"><a
                                                                                href="javascript:void(0);"></a></li>
                                                                    <li class="see-all"></li>
                                                                    <li id="menu-item-10435"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10435 dt-menu-item-10435 ">
                                                                        <a href="http://kalvi.dttheme.com/default/activity/"
                                                                           class="item-has-icon icon-position-left"><span>Activity</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10433"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10433 dt-menu-item-10433 ">
                                                                        <a href="http://kalvi.dttheme.com/default/groups/"
                                                                           class="item-has-icon icon-position-left"><span>Groups</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10434"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10434 dt-menu-item-10434 ">
                                                                        <a href="http://kalvi.dttheme.com/default/members/"
                                                                           class="item-has-icon icon-position-left"><span>Members</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10432"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10432 dt-menu-item-10432 ">
                                                                        <a href="http://kalvi.dttheme.com/default/sites/"
                                                                           class="item-has-icon icon-position-left"><span>Sites</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-10530"
                                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-10530 dt-menu-item-10530 ">
                                                                <a href="http://kalvi.dttheme.com/default/events/"
                                                                   class="item-has-icon icon-position-left"><span>Events</span></a>
                                                                <ul class="sub-menu is-hidden ">
                                                                    <li class="go-back"><a
                                                                                href="javascript:void(0);"></a></li>
                                                                    <li class="see-all"></li>
                                                                    <li id="menu-item-10532"
                                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10532 dt-menu-item-10532 ">
                                                                        <a href="http://kalvi.dttheme.com/default/events/"
                                                                           class="item-has-icon icon-position-left"><span>Events Month</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10533"
                                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10533 dt-menu-item-10533 ">
                                                                        <a href="http://kalvi.dttheme.com/default/events/list/"
                                                                           class="item-has-icon icon-position-left"><span>Events List</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10534"
                                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10534 dt-menu-item-10534 ">
                                                                        <a href="http://kalvi.dttheme.com/default/events/week/"
                                                                           class="item-has-icon icon-position-left"><span>Events Week</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10535"
                                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10535 dt-menu-item-10535 ">
                                                                        <a href="http://kalvi.dttheme.com/default/events/today/"
                                                                           class="item-has-icon icon-position-left"><span>Events Day</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10536"
                                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10536 dt-menu-item-10536 ">
                                                                        <a href="http://kalvi.dttheme.com/default/events/map/?tribe-bar-date=2017-10-31"
                                                                           class="item-has-icon icon-position-left"><span>Events Map</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10537"
                                                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10537 dt-menu-item-10537 ">
                                                                        <a href="http://kalvi.dttheme.com/default/events/photo/?tribe-bar-date=2017-10-31"
                                                                           class="item-has-icon icon-position-left"><span>Events Photo</span></a>
                                                                    </li>
                                                                    <li id="menu-item-10538"
                                                                        class="menu-item menu-item-type-post_type menu-item-object-tribe_events menu-item-10538 dt-menu-item-10538 ">
                                                                        <a href="http://kalvi.dttheme.com/default/event/discussion-about-the-latest-trends/"
                                                                           class="item-has-icon icon-position-left"><span>Single Events</span></a>
                                                                    </li>
                                                                </ul>
                                                            </li>
                                                            <li id="menu-item-12240"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12240 dt-menu-item-12240 ">
                                                                <a href="http://kalvi.dttheme.com/default/faq/"
                                                                   class="item-has-icon icon-position-left"><span>FAQ</span></a>
                                                            </li>
                                                            <li id="menu-item-12407"
                                                                class="menu-item menu-item-type-post_type_archive menu-item-object-forum menu-item-12407 dt-menu-item-12407 ">
                                                                <a href="http://kalvi.dttheme.com/default/forums/"
                                                                   class="item-has-icon icon-position-left"><span>All Forums</span></a>
                                                            </li>
                                                            <li id="menu-item-12673"
                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12673 dt-menu-item-12673 ">
                                                                <a href="http://kalvi.dttheme.com/default/about-us/"
                                                                   class="item-has-icon icon-position-left"><span>About Us</span></a>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-13029"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-mega-menu menu-item-13029 dt-menu-item-13029 ">
                                                        <a href="http://kalvi.dttheme.com/default/shortcodes/quotes-lists/"
                                                           class="item-has-icon icon-position-left"><span>Shortcodes-I</span></a>
                                                        <ul class="sub-menu is-hidden ">
                                                            <li class="go-back"><a href="javascript:void(0);"></a></li>
                                                            <li class="see-all"></li>
                                                            <li id="menu-item-13030"
                                                                class="menu-item menu-item-type-post_type menu-item-object-dt_mega_menus menu-item-13030 dt-menu-item-13030 ">
                                                                <p>
                                                                <div class="vc_row wpb_row vc_row-fluid vcr_float_right vc_custom_1514352131000 vc_row-has-fill">
                                                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                        <div class="vc_column-inner ">
                                                                            <div class="wpb_wrapper"><h3
                                                                                        class="dtlms-title">Theme
                                                                                    Shortcodes</h3>
                                                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1514352142790">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512118891114-706f0a72-9eae"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-theme-shortcode-1-container">
                                                                                                        <ul id="menu-theme-shortcode-1"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-10287"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10287">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/columns/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-columns"></i><span>Columns</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12549"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12549">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/timeline/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon far fa-clock"></i><span>Timeline</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12310"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12310">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/typography/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-info"></i><span>Typography</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-10284"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-10284">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/quotes-lists/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-quote-right"></i><span>Quotes &#038; Lists</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12312"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12312">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/newsletter/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon far fa-envelope"></i><span>Newsletter</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1514352153788">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512118891114-706f0a72-9eae"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-theme-shortcode-2-container">
                                                                                                        <ul id="menu-theme-shortcode-2"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-12541"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12541">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/team/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-users"></i><span>Team</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12539"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12539">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/icon-boxes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon far fa-address-card"></i><span>Icon Boxes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12542"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12542">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/contact-info/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-address-book"></i><span>Contact Info</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13031"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13031">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/buttons/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon far fa-hand-point-up"></i><span>Buttons</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13032"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13032">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/tabs-toggles/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-tasks"></i><span>Tabs</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1514352165069">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512208021881-88753aea-ac16"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-theme-shortcode-3-container">
                                                                                                        <ul id="menu-theme-shortcode-3"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-12546"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12546">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/carousel/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-ellipsis-h"></i><span>Carousel</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13035"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13035">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/progress-bar/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-align-left"></i><span>Progress Bar</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13034"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13034">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/number-counter/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-sort-numeric-up"></i><span>Number Counter</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13033"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13033">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/image-with-caption/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon far fa-image"></i><span>Image With Caption</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13037"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13037">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/toggles-accordions/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-toggle-on"></i><span>Toggles &#038; Accordions</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner ">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1513592931296-2c3dc709-d015"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-theme-shortcode-4-container">
                                                                                                        <ul id="menu-theme-shortcode-4"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-13040"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13040">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/testimonials/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-comment-alt"></i><span>Testimonials</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13042"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13042">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/pricing-table/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-dollar-sign"></i><span>Pricing Table</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13038"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13038">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/custom-posts/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fab fa-pagelines"></i><span>Custom Posts</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13041"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13041">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/miscellaneous/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fas fa-cogs"></i><span>Miscellaneous</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13039"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13039">
                                                                                                                <a href="http://kalvi.dttheme.com/default/shortcodes/testimonial-carousel/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fab fa-stack-exchange"></i><span>Testimonial Carousel</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                </p></li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-13027"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children has-mega-menu menu-item-13027 dt-menu-item-13027 ">
                                                        <a href="http://kalvi.dttheme.com/default/course-shortcodes/course-categories/"
                                                           class="item-has-icon icon-position-left"><span>Shortcodes-II</span></a>
                                                        <ul class="sub-menu is-hidden ">
                                                            <li class="go-back"><a href="javascript:void(0);"></a></li>
                                                            <li class="see-all"></li>
                                                            <li id="menu-item-13028"
                                                                class="menu-item menu-item-type-post_type menu-item-object-dt_mega_menus menu-item-13028 dt-menu-item-13028 ">
                                                                <div class="vc_row wpb_row vc_row-fluid vcr_float_right vc_custom_1514352245401 vc_row-has-fill">
                                                                    <div class="wpb_column vc_column_container vc_col-sm-12">
                                                                        <div class="vc_column-inner ">
                                                                            <div class="wpb_wrapper"><h3
                                                                                        class="dtlms-title">Addon
                                                                                    Shortcodes</h3>
                                                                                <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1514352255116">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512118891114-706f0a72-9eae"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-addon-shortcode-1-container">
                                                                                                        <ul id="menu-addon-shortcode-1"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-12337"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12337">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/class-search/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Class Search</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12334"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12334">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/class-instant-search/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Class Instant Search</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13525"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13525">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/free-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Free Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13521"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13521">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/paid-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Paid Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13519"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13519">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/upcoming-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Upcoming Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13520"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13520">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/recent-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Recent Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13522"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13522">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/most-membered-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Most Membered Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13524"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13524">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/highest-rated-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-file-text-o"></i><span>Highest Rated Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1514352268412">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512118891114-706f0a72-9eae"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-addon-shortcode-2-container">
                                                                                                        <ul id="menu-addon-shortcode-2"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-16413"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16413">
                                                                                                                <a href="http://kalvi.dttheme.com/default/our-instructors/instructor-shortcodes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-address-book-o"></i><span>Instructors Shortcodes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-16260"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16260">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/instructor-classes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-address-book-o"></i><span>Instructor Classes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-14318"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14318">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-grid-list/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Class Grid &#038; List</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13531"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13531">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/class-listings/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Class Listings</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13532"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13532">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-shortcodes/class-listings-isotope/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Class Listings Isotope</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13563"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13563">
                                                                                                                <a href="http://kalvi.dttheme.com/default/class-listing/class-carousel/class-carousel-default/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Class Carousel</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12597"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12597">
                                                                                                                <a href="http://kalvi.dttheme.com/default/our-packages/package-shortcodes/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-folder-open-o"></i><span>Package Shortcodes</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1514352283077">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512211206308-00c867f9-2005"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-addon-shortcode-3-container">
                                                                                                        <ul id="menu-addon-shortcode-3"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-12558"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12558">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/course-search/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Course Search</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-12559"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12559">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/course-instant-search/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Course Instant Search</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13540"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13540">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/free-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Free Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-16255"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16255">
                                                                                                                <a href="http://kalvi.dttheme.com/default/paid-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Paid Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13534"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13534">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/upcoming-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Upcoming Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13535"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13535">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/recent-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Recent Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13537"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13537">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/most-membered-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Most Membered Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13539"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13539">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/highest-rated-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Highest Rated Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                                                        <div class="vc_column-inner vc_custom_1512213401265">
                                                                                            <div class="wpb_wrapper">
                                                                                                <div id="dt-1512211206030-e99f1a6f-ca29"
                                                                                                     class="dt-custom-nav-wrapper no-bottom-space none"
                                                                                                     data-default-style="none"
                                                                                                     data-hover-style="none"
                                                                                                     data-link-icon-position="outside"
                                                                                                     data-default-decoration="none"
                                                                                                     data-hover-decoration="none"
                                                                                                     data-divider="yes">
                                                                                                    <div class="menu-addon-shortcode-4-container">
                                                                                                        <ul id="menu-addon-shortcode-4"
                                                                                                            class="custom-sub-nav dt-custom-nav">
                                                                                                            <li id="menu-item-16377"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16377">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/instructor-courses/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-language"></i><span>Instructor Courses</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-16378"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-16378">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/course-categories/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Course Categories</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-14329"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14329">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-grid/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Course Grid</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-14328"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-14328">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-list/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Course List</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13543"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13543">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-shortcodes/course-listings-isotope/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Course Listings Isotope</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                            <li id="menu-item-13547"
                                                                                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-13547">
                                                                                                                <a href="http://kalvi.dttheme.com/default/course-listing/course-carousel/course-carousel-default/"
                                                                                                                   class="item-has-icon icon-position-left"><i
                                                                                                                            class="menu-item-icon fa fa-tasks"></i><span>Course Carousel</span></a><span
                                                                                                                        class="divider"></span>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                    <li id="menu-item-12674"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-12674 dt-menu-item-12674 ">
                                                        <a href="http://kalvi.dttheme.com/default/contact/"
                                                           class="item-has-icon icon-position-left"><span>Contact</span></a>
                                                    </li>
                                                </ul>
                                                <div class="sub-menu-overlay"></div>
                                            </div>
                                        </div>
                                        <div id="dt-1505559553043-0fc3b635-35b1-mobile"
                                             class="mobile-nav-container mobile-nav-offcanvas-right"
                                             data-menu="main-menu">
                                            <div class="menu-trigger menu-trigger-icon" data-menu="main-menu"><i
                                                        class="fa fa-bars"></i><span>Menu</span></div>
                                            <div class="mobile-menu" data-menu="main-menu"></div>
                                            <div class="overlay"></div>
                                        </div>
                                        <div id="1514360336765-d72632fc-d763" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        </p></div>
                </div>
            </header>
            <div id="slider">
                <div id="dt-sc-custom-slider" class="dt-sc-main-slider">
                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5"
                         data-vc-parallax-image="{{asset("$public/lms/slider-image.jpg")}}"
                         class="vc_row wpb_row vc_row-fluid gradient-overlay vc_row-has-fill vc_general vc_parallax vc_parallax-content-moving">
                        <div class="dtlms-slider-overlay gradient-overlay-content wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="1528968550633-5dffcff6-6b63" class="dt-sc-empty-space"></div>
                                    <h2>Search your Favorite Course here</h2><h4 class="uppercase">Top Notch Courses
                                        from trained Professionals</h4>
                                    <div id="1528789511128-1c70c389-c592" class="dt-sc-empty-space"></div>
                                    <div class="dtlms-courses-listing-holder   dtlms-without-ajax-load">
                                        <form name="dtlmsCoursesListingSearchForm"
                                              action="http://kalvi.dttheme.com/default/courses-listing/" method="post">
                                            <div class="dtlms-courses-listing-filters">
                                                <div class="dtlms-column dtlms-one-third first">
                                                    <div class="dtlms-courses-search-filter"><input
                                                                name="dtlms-courses-search-text"
                                                                class="dtlms-courses-search-text dtlms-without-ajax-load"
                                                                type="text" value="" placeholder="Keywords"/></div>
                                                </div>
                                                <div class="dtlms-column dtlms-one-third ">
                                                    <div class="dtlms-courses-category-filter"><select
                                                                class="coursefilter-category dtlms-without-ajax-load dtlms-chosen-select"
                                                                name="coursefilter-category[]"
                                                                data-placeholder="Categories" multiple>
                                                            <option value="74">Architecture</option>
                                                            <option value="62">Education</option>
                                                            <option value="64">Engineering</option>
                                                            <option value="63">Geography</option>
                                                            <option value="65">Law</option>
                                                            <option value="71">Medical</option>
                                                            <option value="72">Quantum Physics</option>
                                                            <option value="73">Science</option>
                                                            <option value="70">Share Market</option>
                                                        </select></div>
                                                </div>
                                                <div class="dtlms-column dtlms-one-third ">
                                                    <div class="dtlms-courses-instructor-filter"><select
                                                                class="coursefilter-instructor dtlms-without-ajax-load dtlms-chosen-select"
                                                                name="coursefilter-instructor[]"
                                                                data-placeholder="Instructor" multiple>
                                                            <option value="2">Penny Hopkins</option>
                                                            <option value="25">Richard Smith</option>
                                                            <option value="5">Marguerite Wash</option>
                                                            <option value="9">Sylvia Hall</option>
                                                            <option value="10">Picabo Street</option>
                                                            <option value="1">Ram M</option>
                                                            <option value="12">Agnes Porter</option>
                                                        </select></div>
                                                </div>
                                            </div>
                                            <input type="submit" name="dtlms-courses-listing-searchform-submit"
                                                   class="dtlms-courses-listing-searchform-submit"
                                                   value="Search Courses"/></form>
                                    </div>
                                    <div id="1528968561815-3b8d9acf-2306" class="dt-sc-empty-space"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                    <div class="upb_grad"
                         data-grad="background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFCB21), color-stop(20%, #DEE145), color-stop(35%, #B5CE5B), color-stop(55%, #80C29F), color-stop(75%, #75C1B1), color-stop(90%, #40C3FF));background: -moz-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: -webkit-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: -o-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: -ms-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);"
                         data-bg-override="0" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout=""
                         data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30"
                         data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true" data-rtl="false"
                         data-custom-vc-row="" data-vc="5.4.7" data-is_old_vc="" data-theme-support=""
                         data-overlay="false" data-overlay-color="" data-overlay-pattern=""
                         data-overlay-pattern-opacity="" data-overlay-pattern-size=""></div>
                </div>
            </div>
        </div>
        <div id="main">
            <div class="container">
                <section id="primary" class="content-full-width">
                    <div id="post-17810" class="post-17810 page type-page status-publish hentry">
                        <div class="vc_row wpb_row vc_row-fluid dtlms-slider-sticky-bottom vc_column-gap-20">
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type1 yellow dt-sc-kalvi-default-counter'>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='zmdi zmdi-book zmdi-hc-fw'> </span></div>
                                                <div class='dt-sc-counter-number' data-value='1300'
                                                     data-append='+ Topics'>1300
                                                </div>
                                            </div>
                                            <h4>Wide Choice of Subjects</h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type1 pink dt-sc-kalvi-default-counter'>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='zmdi zmdi-puzzle-piece zmdi-hc-fw'> </span></div>
                                                <div class='dt-sc-counter-number' data-value='1276'
                                                     data-append=' Tests Taken'>1276
                                                </div>
                                            </div>
                                            <h4>Thats a lot</h4></div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-4">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-counter type1 skyblue dt-sc-kalvi-default-counter last'>
                                            <div class='dt-sc-couter-icon-holder'>
                                                <div class='icon-wrapper'><span
                                                            class='zmdi zmdi-coffee zmdi-hc-fw'> </span></div>
                                                <div class='dt-sc-counter-number' data-value='256'
                                                     data-append='+ Instructors'>256
                                                </div>
                                            </div>
                                            <h4>All trained professionals</h4></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row wpb_row vc_row-fluid dt-sc-process-with-caption">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1509358446805-c26c8bb2-dc79" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-icon-box type14 aligncenter dt-sc-kalvi-default-icon-box purple'>
                                            <div class="icon-wrapper"><img width="108" height="100"
                                                                           src="{{asset("$public/lms/iconbox-4.html")}}"
                                                                           class="attachment-full" alt=""/></div>
                                            <div class="icon-content"><h4>Expand Knowledge</h4>
                                                <p>Lorem ipsum dolor sit amet, conse ctetur adipiscing elit. Donec quis
                                                    ipsum nunc suscipit lacinia vehicula.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-icon-box type14 aligncenter dt-sc-kalvi-default-icon-box yellow'>
                                            <div class="icon-wrapper"><img width="108" height="100"
                                                                           src="{{asset("$public/lms/iconbox-3.html")}}"
                                                                           class="attachment-full" alt=""/></div>
                                            <div class="icon-content"><h4>Develop your Skill</h4>
                                                <p>Nam efficitur tincidunt blandit. Pellentesque dignissim ultricies
                                                    tellus, sit amet vehicula elit tempor at.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-icon-box type14 aligncenter dt-sc-kalvi-default-icon-box green'>
                                            <div class="icon-wrapper"><img width="108" height="100"
                                                                           src="{{asset("$public/lms/iconbox-2.html")}}"
                                                                           class="attachment-full" alt=""/></div>
                                            <div class="icon-content"><h4>Invest Time</h4>
                                                <p>Mauris vel porta dui, id maximus eros. Proin nec tedct euismod
                                                    tellus, a gravida ipsum eget quam finibus tet facilisis.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-3 vc_col-lg-3 vc_col-md-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-icon-box type14 aligncenter dt-sc-kalvi-default-icon-box pink'>
                                            <div class="icon-wrapper"><img width="108" height="100"
                                                                           src="{{asset("$public/lms/iconbox-1.html")}}"
                                                                           class="attachment-full" alt=""/></div>
                                            <div class="icon-content"><h4>Learn Yourself</h4>
                                                <p>Proin aliquet, metus fringilla mattis dignissim, nulla dolor
                                                    scelerisque purus, et egestas odio massa sit amet urna.</p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1528802500823-2a3d0f85-1651" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false"
                             class="vc_row wpb_row vc_row-fluid vc_custom_1528802156181 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1528802126782-ba9cc5a4-6738" class="dt-sc-empty-space"></div>
                                        <div class='dt-sc-title script-with-sub-title dt-sc-kalvi-default-title'><h2>
                                                Featured <strong>Classes</strong></h2><h6>only The Greatest Mind’s</h6>
                                        </div>
                                        <div id="1528802127029-9384692e-0e52" class="dt-sc-empty-space"></div>
                                        <div class="dtlms-classes-listing-holder grid " data-enablecarousel="false"
                                             data-postperpage="3" data-columns="3" data-applyisotope="false"
                                             data-disablefilters="true" data-defaultfilter=""
                                             data-defaultdisplaytype="grid" data-classitemids="10724, 10722, 10728"
                                             data-instructorids="" data-enablefullwidth="" data-type="type2">
                                            <div class="dtlms-classes-listing-containers grid ">
                                                <div id="dtlms-ajax-load-image" style="display:none;">
                                                    <div class="dtlms-loader-inner">
                                                        <div class="dtlms-loading"></div>
                                                        <div class="dtlms-pad">
                                                            <div class="dtlms-line dtlms-line1"></div>
                                                            <div class="dtlms-line dtlms-line2"></div>
                                                            <div class="dtlms-line dtlms-line3"></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="1528802127264-94493132-bfaf" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1509434204164-3ba5d642-4fc6" class="dt-sc-empty-space"></div>
                                        <div class='dt-sc-title script-with-sub-title dt-sc-kalvi-default-title'><h2>
                                                Course <strong>Categories</strong></h2><h6>Just Pick what you need to
                                                learn</h6></div>
                                        <div id="1510024051278-ca696960-a1f1" class="dt-sc-empty-space"></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third first ">
                                            <img src="{{asset("$public/lms/architecht.html")}}"
                                                 alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3>
                                                <a href="http://kalvi.dttheme.com/default/course-category/architecture/">Architecture</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third  ">
                                            <img src="{{asset("$public/lms/education.html")}}"
                                                 alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3><a href="http://kalvi.dttheme.com/default/course-category/education/">Education</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third  ">
                                            <img src="{{asset("$public/lms/engineering.html")}}"
                                                 alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3><a href="http://kalvi.dttheme.com/default/course-category/engineering/">Engineering</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third first ">
                                            <img src="{{asset("$public/lms/jpg/geo.jpg" )}}" alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3><a href="http://kalvi.dttheme.com/default/course-category/geography/">Geography</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third  ">
                                            <img src="{{asset("$public/lms/law.html")}}" alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3><a href="http://kalvi.dttheme.com/default/course-category/law/">Law</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third  ">
                                            <img src="{{asset("$public/lms/medical.html")}}" alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3><a href="http://kalvi.dttheme.com/default/course-category/medical/">Medical</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third first ">
                                            <img src="{{asset("$public/lms/quantom.html")}}" alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3>
                                                <a href="http://kalvi.dttheme.com/default/course-category/quantum-physics/">Quantum
                                                    Physics</a></h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third  ">
                                            <img src="{{asset("$public/lms/science.html")}}" alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3><a href="http://kalvi.dttheme.com/default/course-category/science/">Science</a>
                                            </h3></div>
                                        <div class="dtlms-course-category-item type2 dtlms-column dtlms-one-third  ">
                                            <img src="{{asset("$public/lms/share.html")}}" alt="Course Category Image"
                                                 title="Course Category Image"/>
                                            <h3>
                                                <a href="http://kalvi.dttheme.com/default/course-category/share-market/">Share
                                                    Market</a></h3></div>
                                        <div id="1509447717264-d57abec2-ccc3" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false"
                             class="vc_row wpb_row vc_row-fluid vc_custom_1528802156181 vc_row-has-fill">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1528802687786-da250746-5700" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-image-caption type7 dt-sc-kalvi-default-image-caption'>
                                            <div class='dt-sc-image-wrapper'><img width="372" height="235"
                                                                                  src="{{asset("$public/lms/jpg/image-caption-1-2.jpg" )}}"
                                                                                  class="attachment-full" alt=""
                                                                                  srcset="{{asset("$public/lms/image-caption-1-2.jpg")}}
                                                                                          372w, {{asset("$public/lms/image-caption-1-2-300x190.jpg")}} 300w"
                                                                                  sizes="(max-width: 372px) 100vw, 372px"/>
                                            </div>
                                            <div class='dt-sc-image-content'>
                                                <div class='dt-sc-image-title'><h3>Course <strong>Mela</strong></h3>
                                                </div>
                                                <p>Access the Award Winning Courses</p>
                                                <p><a href='#' target='_self' title=''
                                                      class='dt-sc-button   small icon-right with-icon    filled medium'>
                                                        Join Kalvi <span
                                                                class='zmdi zmdi-long-arrow-right zmdi-hc-fw'> </span></a>
                                                </p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="rs_col-sm-6 wpb_column vc_column_container vc_col-sm-6">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div class='dt-sc-image-caption type7 dt-sc-kalvi-default-image-caption'>
                                            <div class='dt-sc-image-wrapper'><img width="372" height="235"
                                                                                  src="{{asset("$public/lms/image-caption-2-1.html")}}"
                                                                                  class="attachment-full" alt=""
                                                                                  srcset="{{asset("$public/lms/image-caption-2-1.jpg")}} 372w, {{asset("$public/lms/image-caption-2-1-300x190.jpg")}} 300w"
                                                                                  sizes="(max-width: 372px) 100vw, 372px"/>
                                            </div>
                                            <div class='dt-sc-image-content'>
                                                <div class='dt-sc-image-title'><h3>Advanced <strong>E-learning</strong>
                                                    </h3></div>
                                                <p>Access the Award Winning Courses</p>
                                                <p><a href='#' target='_self' title=''
                                                      class='dt-sc-button   small icon-right with-icon    filled medium'>
                                                        Join Kalvi <span
                                                                class='zmdi zmdi-long-arrow-right zmdi-hc-fw'> </span></a>
                                                </p></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1528802688311-249518ef-2a29" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="vc_row wpb_row vc_row-fluid">
                            <div class="wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1510024290354-072df9d4-864a" class="dt-sc-empty-space"></div>
                                        <div class='dt-sc-title script-with-sub-title dt-sc-kalvi-default-title aligncenter'>
                                            <h2>Your Best <strong>Teachers</strong></h2><h6>Ignite your vision with
                                                us</h6></div>
                                        <div class="dtlms-instructor-item dtlms-column dtlms-one-third first type1 no-image-shadow with-bg rounded">
                                            <img src="{{asset("$public/lms/5b31e945cc9db-bpfull.html")}}"
                                                 alt="Instructor Image"
                                                 title="Instructor Image"/>
                                            <div class="dtlms-instructor-item-meta-data"><h4><a
                                                            href="http://kalvi.dttheme.com/default/author/antawn/"
                                                            rel="author"> Penny Hopkins </a></h4><h5>Human
                                                    Resources</h5>
                                                <div class="dtlms-team-social-links">
                                                    <ul class="dtlms-team-social">
                                                        <li><a class="fa fa-flickr" href="#"></a></li>
                                                        <li><a class="fa fa-twitter" href="#"></a></li>
                                                        <li><a class="fa fa-youtube" href="#"></a></li>
                                                        <li><a class="fa fa-facebook" href="#"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtlms-instructor-item dtlms-column dtlms-one-third  type1 no-image-shadow with-bg rounded">
                                            <img src="{{asset("$public/lms/5b31e95366f28-bpfull.html")}}"
                                                 alt="Instructor Image"
                                                 title="Instructor Image"/>
                                            <div class="dtlms-instructor-item-meta-data"><h4><a
                                                            href="http://kalvi.dttheme.com/default/author/justin/"
                                                            rel="author"> Richard Smith </a></h4><h5>Dancer &
                                                    Instructor</h5>
                                                <div class="dtlms-team-social-links">
                                                    <ul class="dtlms-team-social">
                                                        <li><a class="fa fa-twitter" href="http://twitter.com/"></a>
                                                        </li>
                                                        <li><a class="fa fa-facebook" href="http://facebook.com/"></a>
                                                        </li>
                                                        <li><a class="fa fa-google-plus"
                                                               href="http://googleplus.com/"></a></li>
                                                        <li><a class="fa fa-pinterest" href="http://pinterest.com/"></a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtlms-instructor-item dtlms-column dtlms-one-third  type1 no-image-shadow with-bg rounded">
                                            <img src="{{asset("$public/lms/5b31e95c1433f-bpfull.html")}}"
                                                 alt="Instructor Image"
                                                 title="Instructor Image"/>
                                            <div class="dtlms-instructor-item-meta-data"><h4><a
                                                            href="http://kalvi.dttheme.com/default/author/malivai/"
                                                            rel="author"> Marguerite Wash </a></h4><h5>Computer
                                                    Science</h5>
                                                <div class="dtlms-team-social-links">
                                                    <ul class="dtlms-team-social">
                                                        <li><a class="fa fa-flickr" href="#"></a></li>
                                                        <li><a class="fa fa-twitter" href="#"></a></li>
                                                        <li><a class="fa fa-facebook" href="#"></a></li>
                                                        <li><a class="fa fa-google-plus" href="#"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtlms-instructor-item dtlms-column dtlms-one-third first type1 no-image-shadow with-bg rounded">
                                            <img src="{{asset("$public/lms/jpg/5b31e964d2c27-bpfull.jpg" )}}"
                                                 alt="Instructor Image"
                                                 title="Instructor Image"/>
                                            <div class="dtlms-instructor-item-meta-data"><h4><a
                                                            href="http://kalvi.dttheme.com/default/author/monta/"
                                                            rel="author"> Sylvia Hall </a></h4><h5>Macro Economics</h5>
                                                <div class="dtlms-team-social-links">
                                                    <ul class="dtlms-team-social">
                                                        <li><a class="fa fa-flickr" href="#"></a></li>
                                                        <li><a class="fa fa-twitter" href="#"></a></li>
                                                        <li><a class="fa fa-facebook" href="#"></a></li>
                                                        <li><a class="fa fa-google-plus" href="#"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtlms-instructor-item dtlms-column dtlms-one-third  type1 no-image-shadow with-bg rounded">
                                            <img src="{{asset("$public/lms/5b31e968d20b5-bpfull.html")}}"
                                                 alt="Instructor Image"
                                                 title="Instructor Image"/>
                                            <div class="dtlms-instructor-item-meta-data"><h4><a
                                                            href="http://kalvi.dttheme.com/default/author/picabo/"
                                                            rel="author"> Picabo Street </a></h4><h5>Business</h5>
                                                <div class="dtlms-team-social-links">
                                                    <ul class="dtlms-team-social">
                                                        <li><a class="fa fa-flickr" href="#"></a></li>
                                                        <li><a class="fa fa-twitter" href="#"></a></li>
                                                        <li><a class="fa fa-facebook" href="#"></a></li>
                                                        <li><a class="fa fa-google-plus" href="#"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="dtlms-instructor-item dtlms-column dtlms-one-third  type1 no-image-shadow with-bg rounded">
                                            <img src="{{asset("$public/lms/jpg/5b31e96d5ecc1-bpfull.jpg" )}}"
                                                 alt="Instructor Image"
                                                 title="Instructor Image"/>
                                            <div class="dtlms-instructor-item-meta-data"><h4><a
                                                            href="http://kalvi.dttheme.com/default/author/ram/"
                                                            rel="author"> Ram M </a></h4><h5>Web Developer</h5>
                                                <div class="dtlms-team-social-links">
                                                    <ul class="dtlms-team-social">
                                                        <li><a class="fa fa-flickr" href="#"></a></li>
                                                        <li><a class="fa fa-twitter" href="#"></a></li>
                                                        <li><a class="fa fa-instagram" href="#"></a></li>
                                                        <li><a class="fa fa-facebook" href="#"></a></li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="1509677694052-6c6f651e-6977" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="aligncenter wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper"><a href='#' target=' _blank' title=''
                                                                class='dt-sc-button   large icon-right with-icon  filled  alternate-bg-color'>
                                            View All Authors <span
                                                    class='zmdi zmdi-long-arrow-right zmdi-hc-fw'> </span></a>
                                        <div id="1528885804790-1de45969-0598" class="dt-sc-empty-space"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="1.5"
                             data-vc-parallax-image="{{asset("$public/lms/tab-bg-image.jpg")}}"
                             class="vc_row wpb_row vc_row-fluid gradient-overlay vc_row-has-fill vc_row-o-equal-height vc_row-o-content-middle vc_row-flex vc_general vc_parallax vc_parallax-content-moving">
                            <div class="gradient-overlay-content wpb_column vc_column_container vc_col-sm-12">
                                <div class="vc_column-inner ">
                                    <div class="wpb_wrapper">
                                        <div id="1528882916846-c6e4eb67-0eeb" class="dt-sc-empty-space"></div>
                                        <div class='dt-sc-title script-with-sub-title dt-sc-kalvi-default-title no-decor aligncenter'>
                                            <h2>Why EduPlanet?</h2><h6>No hassles... Just Learn Now &amp; Become a Pro.
                                                Why the Wait?</h6></div>
                                        <div class='dt-sc-tabs-horizontal-frame-container type9' data-effect='default'>
                                            <ul class='dt-sc-tabs-horizontal-frame'>
                                                <li><a href="javascript:void(0);">
                                                        <div class="dt-sc-tab-image-holder"><img width="69" height="67"
                                                                                                 src="{{asset("$public/lms/icon-box2-3.html")}}"
                                                                                                 class="attachment-full"
                                                                                                 alt=""/></div>
                                                        Want to Learn?</a></li>
                                                <li><a href="javascript:void(0);">
                                                        <div class="dt-sc-tab-image-holder"><img width="41" height="67"
                                                                                                 src="{{asset("$public/lms/png/icon-box2-2.png")}}"
                                                                                                 class="attachment-full"
                                                                                                 alt=""/></div>
                                                        Benefits of EduPlanet</a></li>
                                                <li><a href="javascript:void(0);">
                                                        <div class="dt-sc-tab-image-holder"><img width="71" height="67"
                                                                                                 src="{{asset("$public/lms/png/icon-image.png")}}"
                                                                                                 class="attachment-full"
                                                                                                 alt=""/></div>
                                                        Create a Lesson</a></li>
                                            </ul>
                                            <div class='dt-sc-tabs-horizontal-frame-content'>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                                                    <div class="dt-sc-rhs-separator dt-skin-primary-border wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner vc_custom_1535356355594">
                                                            <div class="wpb_wrapper">
                                                                <div id="1535355224048-e4b27f6d-1b04"
                                                                     class="dt-sc-empty-space"></div>
                                                                <h4>Education is more fun with <strong>EduKalvi</strong>
                                                                </h4>
                                                                <div class="wpb_text_column wpb_content_element  vc_custom_1535356363007">
                                                                    <div class="wpb_wrapper"><p>Lorem ipsum dolor amet,
                                                                            conse ctetur adipiscing elit, sed do eiusmod
                                                                            tempor incididunt.</p></div>
                                                                </div>
                                                                <div id="1535355877438-364e40bb-9ddb"
                                                                     class="dt-sc-empty-space"></div>
                                                                <a href='#' target='_self' title=''
                                                                   class='dt-sc-button   medium icon-right with-icon    dt-skin-secondary-color-on-hover transparent'>
                                                                    READ MORE <span
                                                                            class='fa fa-angle-right'> </span></a>
                                                                <div id="1535355246120-51ee59f2-64a1"
                                                                     class="dt-sc-empty-space"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter'>
                                                                    <div class="icon-wrapper"><img width="53"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-8.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Varied <strong>Syllabus</strong>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet nullam id arcu
                                                                            tortor. Sed eget sit ame egestas, cursus
                                                                            felis quis, vestibulum.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter '>
                                                                    <div class="icon-wrapper"><img width="47"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-7.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Comprehensive <strong>Study</strong>
                                                                        </h4>
                                                                        <p>Nam efficitur tincidunt elit blandit.
                                                                            Pellente sqtfrue dign issim vehicula
                                                                            ultricies tellus, amet tempor at.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter'>
                                                                    <div class="icon-wrapper"><img width="53"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-6.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Best
                                                                            <strong>Outcome</strong></h4>
                                                                        <p>Sed euismodnon aliquet placerat, lectusmi
                                                                            molestie diam a, tempoerat metus. Mauris
                                                                            veldui maxim eros.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='dt-sc-tabs-horizontal-frame-content'>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                                                    <div class="dt-sc-rhs-separator dt-skin-primary-border wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner vc_custom_1509941808919">
                                                            <div class="wpb_wrapper">
                                                                <div id="1535355291304-1cb5b82a-4c10"
                                                                     class="dt-sc-empty-space"></div>
                                                                <h4>Education is more fun with <strong>EduKalvi</strong>
                                                                </h4>
                                                                <div class="wpb_text_column wpb_content_element  vc_custom_1535356417888">
                                                                    <div class="wpb_wrapper"><p>Aliquam erat volutpat.
                                                                            Vestibulum ante ipsum primis in faucibus
                                                                            orci luctus et ultric.</p></div>
                                                                </div>
                                                                <div id="1535356399421-cfff0e9c-4154"
                                                                     class="dt-sc-empty-space"></div>
                                                                <a href='#' target='_self' title=''
                                                                   class='dt-sc-button   medium icon-right with-icon    dt-skin-secondary-color-on-hover transparent'>
                                                                    READ MORE <span
                                                                            class='fa fa-angle-right'> </span></a>
                                                                <div id="1535355322759-7f58fa64-8285"
                                                                     class="dt-sc-empty-space"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter'>
                                                                    <div class="icon-wrapper"><img width="44"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-5.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Relevant <strong>Courses</strong>
                                                                        </h4>
                                                                        <p>Sed euismodnon aliquet placerat, lectusmi
                                                                            molestie diam a, tempoerat metus. Mauris
                                                                            veldui maxim eros.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter'>
                                                                    <div class="icon-wrapper"><img width="46"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-4.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Land a <strong>new
                                                                                job!</strong></h4>
                                                                        <p>Lorem ipsum dolor sit amet nullam id arcu
                                                                            tortor. Sed eget sit ame egestas, cursus
                                                                            felis quis, vestibulum.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter '>
                                                                    <div class="icon-wrapper"><img width="45"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-3.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Updated <strong>Syllabus</strong>
                                                                        </h4>
                                                                        <p>Nam efficitur tincidunt elit blandit.
                                                                            Pellente sqtfrue dign issim vehicula
                                                                            ultricies tellus, amet tempor at.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class='dt-sc-tabs-horizontal-frame-content'>
                                                <div class="vc_row wpb_row vc_inner vc_row-fluid vc_row-o-equal-height vc_row-o-content-middle vc_row-flex">
                                                    <div class="dt-sc-rhs-separator dt-skin-primary-border wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner vc_custom_1509941808919">
                                                            <div class="wpb_wrapper">
                                                                <div id="1535355359573-56a5fade-e931"
                                                                     class="dt-sc-empty-space"></div>
                                                                <h4>Education is more fun with <strong>EduKalvi</strong>
                                                                </h4>
                                                                <div class="wpb_text_column wpb_content_element  vc_custom_1535356567151">
                                                                    <div class="wpb_wrapper"><p>Vivamus mattis nibh non
                                                                            orci sollicitudin, eu semper leo laoreet.
                                                                            Sed non velit vitaes.</p></div>
                                                                </div>
                                                                <div id="1535356571596-2bb5c6f2-4515"
                                                                     class="dt-sc-empty-space"></div>
                                                                <a href='#' target='_self' title=''
                                                                   class='dt-sc-button   medium icon-right with-icon    dt-skin-secondary-color-on-hover transparent'>
                                                                    READ MORE <span
                                                                            class='fa fa-angle-right'> </span></a>
                                                                <div id="1535355371965-d79ccba2-a3b9"
                                                                     class="dt-sc-empty-space"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter'>
                                                                    <div class="icon-wrapper"><img width="51"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-2.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Land a <strong>new
                                                                                job!</strong></h4>
                                                                        <p>Nam efficitur tincidunt elit blandit.
                                                                            Pellente sqtfrue dign issim vehicula
                                                                            ultricies tellus, amet tempor at.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter '>
                                                                    <div class="icon-wrapper"><img width="47"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-7.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Updated <strong>Syllabus</strong>
                                                                        </h4>
                                                                        <p>Sed euismodnon aliquet placerat, lectusmi
                                                                            molestie diam a, tempoerat metus. Mauris
                                                                            veldui maxim eros.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wpb_column vc_column_container vc_col-sm-3">
                                                        <div class="vc_column-inner ">
                                                            <div class="wpb_wrapper">
                                                                <div class='dt-sc-icon-box type1 without-separator dt-sc-kalvi-default-icon-box aligncenter'>
                                                                    <div class="icon-wrapper"><img width="36"
                                                                                                   height="49"
                                                                                                   src="{{asset("$public/lms/png/icon-box1-1.png")}}"
                                                                                                   class="attachment-full"
                                                                                                   alt=""/></div>
                                                                    <div class="icon-content"><h4>Relevant <strong>Courses</strong>
                                                                        </h4>
                                                                        <p>Lorem ipsum dolor sit amet nullam id arcu
                                                                            tortor. Sed eget sit ame egestas, cursus
                                                                            felis quis, vestibulum.</p></div>
                                                                    <span class='fa fa-info-circle large-icon'> </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_row-full-width vc_clearfix"></div>
                        <div class="upb_grad"
                             data-grad="background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, #FFCB21), color-stop(20%, #DEE145), color-stop(35%, #B5CE5B), color-stop(55%, #80C29F), color-stop(75%, #75C1B1), color-stop(90%, #40C3FF));background: -moz-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: -webkit-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: -o-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: -ms-linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);background: linear-gradient(left,#FFCB21 0%,#DEE145 20%,#B5CE5B 35%,#80C29F 55%,#75C1B1 75%,#40C3FF 90%);"
                             data-bg-override="0" data-upb-overlay-color="" data-upb-bg-animation="" data-fadeout=""
                             data-fadeout-percentage="30" data-parallax-content="" data-parallax-content-sense="30"
                             data-row-effect-mobile-disable="true" data-img-parallax-mobile-disable="true"
                             data-rtl="false" data-custom-vc-row="" data-vc="5.4.7" data-is_old_vc=""
                             data-theme-support="" data-overlay="false" data-overlay-color="" data-overlay-pattern=""
                             data-overlay-pattern-opacity="" data-overlay-pattern-size=""></div>
                        <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-parallax="3"
                             data-vc-parallax-image="{{asset("$public/lms/png/map-bg.png")}}" )}}
                        "
                        class="vc_row wpb_row vc_row-fluid vc_custom_1528979248668 vc_row-has-fill vc_row-o-equal-height
                        vc_row-o-content-middle vc_row-flex vc_general vc_parallax vc_parallax-content-moving">
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="1509680644003-379c1650-42c1" class="dt-sc-empty-space"></div>
                                </div>
                            </div>
                        </div>
                        <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class='dt-sc-icon-box type8 with-bg'>
                                        <div class="icon-wrapper"><img width="58" height="69"
                                                                       src="{{asset("$public/lms/png/icon-box4-1.png")}}"
                                                                       class="attachment-full" alt=""/></div>
                                        <div class="icon-content"><h4>Choose your Subject</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class='dt-sc-icon-box type8 with-bg'>
                                        <div class="icon-wrapper"><img width="65" height="69"
                                                                       src="{{asset("$public/lms/png/icon-box4-2.png")}}"
                                                                       class="attachment-full" alt=""/></div>
                                        <div class="icon-content"><h4>Search for your Course</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                                    </div>
                                    <div id="1531916555985-48626a46-4d4b" class="dt-sc-empty-space"></div>
                                </div>
                            </div>
                        </div>
                        <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-4">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div class='dt-sc-icon-box type8 with-bg'>
                                        <div class="icon-wrapper"><img width="68" height="60"
                                                                       src="{{asset("$public/lms/png/icon-box4-3.png")}}"
                                                                       class="attachment-full" alt=""/></div>
                                        <div class="icon-content"><h4>Enroll in your Course</h4>
                                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-12">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper">
                                    <div id="1527138693504-63cbddbf-3ea3" class="dt-sc-empty-space"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
                    <div data-vc-full-width="true" data-vc-full-width-init="false" data-vc-stretch-content="true"
                         class="vc_row wpb_row vc_row-fluid dtlms-default-intro-section">
                        <div class="wpb_column vc_column_container vc_col-sm-3">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-6">
                            <div class="vc_column-inner " style=" text-align:center; ">
                                <div class="wpb_wrapper">
                                    <div id="1529058529270-9c249012-750c" class="dt-sc-empty-space"></div>
                                    <h6 class="uppercase">only The Greatest Mind’s</h6>
                                    <div id="1529058598875-65784441-cd87" class="dt-sc-empty-space"></div>
                                    <h4>Kalvi Provides the best Platform for your Education</h4>
                                    <h3 class="dt-sc-skin-color">Get the Most of it Now.</h3>
                                    <div id="1529059247850-03abcfa4-f63c" class="dt-sc-empty-space"></div>
                                    <div class="wpb_text_column wpb_content_element ">
                                        <div class="wpb_wrapper"><p>Lorem ipsum dolor sit amet, consectetur
                                                adipiscing elit, sed do eiusmod tempor <br>incididunt ut labore et
                                                dolore magna aliqua.</p></div>
                                    </div>
                                    <div id="dt-1529055406515-2f86eff5-3d8a"
                                         class="dt-custom-nav-wrapper center inline-horizontal"
                                         data-default-style="none" data-hover-style="none"
                                         data-link-icon-position="inside" data-default-decoration="none"
                                         data-hover-decoration="none">
                                        <div class="menu-custom-links-iv-container">
                                            <ul id="menu-custom-links-iv" class="custom-sub-nav dt-custom-nav">
                                                <li id="menu-item-16028"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16028">
                                                    <a href="#" class="item-has-icon icon-position-left"><i
                                                                class="menu-item-icon fas fa-caret-right"></i><span>Learn</span></a>
                                                </li>
                                                <li id="menu-item-16029"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16029">
                                                    <a href="#" class="item-has-icon icon-position-left"><i
                                                                class="menu-item-icon fas fa-caret-right"></i><span>Teach</span></a>
                                                </li>
                                                <li id="menu-item-16030"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16030">
                                                    <a href="#" class="item-has-icon icon-position-left"><i
                                                                class="menu-item-icon fas fa-caret-right"></i><span>Experiment</span></a>
                                                </li>
                                                <li id="menu-item-16031"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16031">
                                                    <a href="#" class="item-has-icon icon-position-left"><i
                                                                class="menu-item-icon fas fa-caret-right"></i><span>Win</span></a>
                                                </li>
                                                <li id="menu-item-16032"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-16032">
                                                    <a href="#" class="item-has-icon icon-position-left"><i
                                                                class="menu-item-icon fas fa-caret-right"></i><span>Achieve</span></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div id="1529059453231-3e514847-4655" class="dt-sc-empty-space"></div>
                                    <div class="vc_row wpb_row vc_inner vc_row-fluid">
                                        <div class="wpb_column vc_column_container vc_col-sm-12">
                                            <div class="vc_column-inner ">
                                                <div class="wpb_wrapper"><a href='#' target='_self' title=''
                                                                            class='dt-sc-button   medium icon-right with-icon  filled  dt-sc-skin-highlight'>
                                                        Get Free Trial <span class='fa fa-long-arrow-right'> </span></a><a
                                                            href='#' target='_self' title=''
                                                            class='dt-sc-button vc_custom_1529926901352  medium icon-right with-icon  filled  dt-skin-secondary-bg'>
                                                        Join Now <span class='fa fa-long-arrow-right'> </span></a><a
                                                            href='#' target='_self' title=''
                                                            class='dt-sc-button   medium icon-right with-icon  filled  dt-sc-skin-highlight alignleft'>
                                                        View Pricing <span
                                                                class='fa fa-long-arrow-right'> </span></a></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="1529059500447-c75340dc-f4fd" class="dt-sc-empty-space"></div>
                                    <div id="1529059597105-60c4991c-9bb3" class="dt-sc-empty-space"></div>
                                </div>
                            </div>
                        </div>
                        <div class="wpb_column vc_column_container vc_col-sm-3">
                            <div class="vc_column-inner ">
                                <div class="wpb_wrapper"></div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_row-full-width vc_clearfix"></div>
            </div>
            </section>
        </div>
    </div>
    <footer id="footer">
        <div class="container">
            <p>
                <div data-vc-full-width="true" data-vc-full-width-init="false"
                     class="vc_row wpb_row vc_row-fluid dt-sc-kalvi-default-footer vc_custom_1528884765672 vc_row-has-fill">
                    <div class="rs_col-sm-4 wpb_column vc_column_container vc_col-sm-3">
                        <div class="vc_column-inner ">
                            <div class="wpb_wrapper">
                                <div id="1509959177940-e0a03f96-4dc0" class="dt-sc-empty-space"></div>
                                <div id="dt-1509948082250-69ae94a2-fc8a" class="dt-logo-container logo-align-left">
                                    <a href="index.html" rel="home"><img src="{{asset("$public/lms/logo.html")}}"
                                                                         alt="Default"/></a></div>
                                <div id="1509948791571-16c933aa-b6c0" class="dt-sc-empty-space"></div>
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
            <p><strong>Kalvi is Education </strong>which is the passport to the future, for tomorrow belongs to
                those who prepare for it today. Because the future of the world is in my classroom today!</p></div>
</div>
<div class='dt-sc-contact-info  '><span class='zmdi zmdi-pin zmdi-hc-fw'> </span>123, New Design Street, Melbourne -
    Australia
</div>
<div class='dt-sc-single-line-dashed-separator '></div>
<div class='dt-sc-contact-info  '><span class='zmdi zmdi-email zmdi-hc-fw'> </span><a
            href="#">info@eduplanet.com</a></div>
<div class='dt-sc-single-line-dashed-separator '></div>
<div class='dt-sc-contact-info  '><span class='zmdi zmdi-phone zmdi-hc-fw'> </span>(091)-984261040006</div>
<div id="1509962907652-f5ea8614-dbde" class="dt-sc-empty-space"></div>
</div>
</div></div>
<div class="dt-sc-half-bg-footer rs_col-sm-4 wpb_column vc_column_container vc_col-sm-6 vc_col-has-fill">
    <div class="vc_column-inner vc_custom_1528885140912">
        <div class="wpb_wrapper">
            <div id="1509962817157-31858689-435b" class="dt-sc-empty-space"></div>
            <div class='dt-sc-title script-with-sub-title dt-sc-kalvi-default-title alignleft'><h3>Categories</h3>
                <h3></h3></div>
            <div class="vc_row wpb_row vc_inner vc_row-fluid">
                <div class="rs_col-sm-12 wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_col-md-12">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="dt-1509950991997-f095c756-d4ad" class="dt-custom-nav-wrapper same-on-mobile left"
                                 data-default-style="none" data-hover-style="none" data-link-icon-position="inside"
                                 data-link-icon-style="disc" data-default-decoration="none" data-hover-decoration="none"
                                 data-divider="yes">
                                <div class="menu-custom-links-container">
                                    <ul id="menu-custom-links" class="custom-sub-nav dt-custom-nav">
                                        <li id="menu-item-10974"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10974">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon fa fa-home"></i><span>Medicine</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10975"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10975">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Middle Eastern</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10976"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10976">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Music</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10977"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10977">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Nursing</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10978"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10978">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Occupational Theraphy</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10979"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10979">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Optometry</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10980"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10980">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Pharmacology</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10981"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10981">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Philosophy</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10982"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10982">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Physics &#038; Astronomy</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10983"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10983">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Physiotheraphy</span></a><span
                                                    class="divider"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="wpb_column vc_column_container vc_col-sm-6 vc_col-lg-6 vc_hidden-md vc_hidden-sm">
                    <div class="vc_column-inner ">
                        <div class="wpb_wrapper">
                            <div id="dt-1509954804899-8a2ad6c9-bfca" class="dt-custom-nav-wrapper same-on-mobile left"
                                 data-default-style="none" data-hover-style="none" data-link-icon-position="inside"
                                 data-link-icon-style="disc" data-default-decoration="none" data-hover-decoration="none"
                                 data-divider="yes">
                                <div class="menu-custom-links-ii-container">
                                    <ul id="menu-custom-links-ii" class="custom-sub-nav dt-custom-nav">
                                        <li id="menu-item-10964"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10964">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Fashion</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10965"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10965">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Film Making</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10966"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10966">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Food Science</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10967"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10967">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Forensic Science</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10968"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10968">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>French</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10969"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10969">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Geography</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10970"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10970">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>Geology</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10971"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10971">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>General Engineering</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10972"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10972">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>German</span></a><span
                                                    class="divider"></span></li>
                                        <li id="menu-item-10973"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-10973">
                                            <a href="#" class="item-has-icon icon-position-left"><i
                                                        class="menu-item-icon"></i><span>History</span></a><span
                                                    class="divider"></span></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div id="1509962743582-028f5f52-5434" class="dt-sc-empty-space"></div>
        </div>
    </div>
</div>
<div class="rs_col-sm-4 wpb_column vc_column_container vc_col-sm-3">
    <div class="vc_column-inner " style="z-index: 1;">
        <div class="wpb_wrapper">
            <div id="1509962821819-88449c89-26a1" class="dt-sc-empty-space"></div>
            <div class='dt-sc-title script-with-sub-title dt-sc-kalvi-default-title alignleft'><h3>Useful Links</h3>
                <h3></h3></div>
            <div id="dt-1509962679947-7c60299c-edaa" class="dt-custom-nav-wrapper same-on-mobile left"
                 data-default-style="none" data-hover-style="none" data-link-icon-position="inside"
                 data-link-icon-style="disc" data-default-decoration="none" data-hover-decoration="none"
                 data-divider="yes">
                <div class="menu-menu-1-container">
                    <ul id="menu-menu-1" class="custom-sub-nav dt-custom-nav">
                        <li id="menu-item-12517"
                            class="menu-item menu-item-type-custom menu-item-object-custom current-menu-item current_page_item menu-item-home menu-item-12517">
                            <a href="index.html" class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Default</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12518"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12518"><a
                                    href="http://kalvi.dttheme.com/online-learning/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Online Learning</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12519"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12519"><a
                                    href="http://kalvi.dttheme.com/one-instructor/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>One Instructor</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12520"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12520"><a
                                    href="http://kalvi.dttheme.com/one-course/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>One Course</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12521"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12521"><a
                                    href="http://kalvi.dttheme.com/kalvi-academy/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Kalvi Academy</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12522"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12522"><a
                                    href="http://kalvi.dttheme.com/kindergarden/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Kindergarden</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12523"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12523"><a
                                    href="http://kalvi.dttheme.com/kalvi-university/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Kalvi University</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12524"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12524"><a
                                    href="http://kalvi.dttheme.com/dance-school/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Dance School</span></a><span
                                    class="divider"></span></li>
                        <li id="menu-item-12525"
                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-12525"><a
                                    href="http://kalvi.dttheme.com/points-system/"
                                    class="item-has-icon icon-position-left"><i class="menu-item-icon"></i><span>Points System</span></a><span
                                    class="divider"></span></li>
                    </ul>
                </div>
            </div>
            <div id="1509962911251-039d060b-08bf" class="dt-sc-empty-space"></div>
        </div>
    </div>
</div>
</div>
<div class="vc_row-full-width vc_clearfix"></div>
<div class="vc_row wpb_row vc_row-fluid aligncenter">
    <div class="wpb_column vc_column_container vc_col-sm-12">
        <div class="vc_column-inner ">
            <div class="wpb_wrapper">
                <div id="1509966065253-269494a8-98bd" class="dt-sc-empty-space"></div>
                <ul id='dt-1505530538163-579e9a38-21c0' class='dt-sc-sociable medium center'
                    data-default-style='none'
                    data-default-border-radius='no'
                    data-default-shape=''
                    data-hover-style='none'
                    data-hover-border-radius='no'
                    data-hover-shape=''
                >
                    <li class="facebook"><a href="#" title="" target=" _blank"> <span
                                    class="dt-icon-default"> <span></span> </span> <i></i> <span class="dt-icon-hover"> <span></span> </span>
                        </a></li>
                    <li class="flickr"><a href="#" title="" target=" _blank"> <span
                                    class="dt-icon-default"> <span></span> </span> <i></i> <span class="dt-icon-hover"> <span></span> </span>
                        </a></li>
                    <li class="instagram"><a href="#" title="" target=" _blank"> <span
                                    class="dt-icon-default"> <span></span> </span> <i></i> <span class="dt-icon-hover"> <span></span> </span>
                        </a></li>
                    <li class="google-plus"><a href="#" title="" target=" _blank"> <span
                                    class="dt-icon-default"> <span></span> </span> <i></i> <span class="dt-icon-hover"> <span></span> </span>
                        </a></li>
                    <li class="twitter"><a href="#" title="" target=" _blank"> <span
                                    class="dt-icon-default"> <span></span> </span> <i></i> <span class="dt-icon-hover"> <span></span> </span>
                        </a></li>
                </ul>
                <div id="1510022944022-2f2cbd1a-cfb0" class="dt-sc-empty-space"></div>
                <div class="wpb_text_column wpb_content_element  vc_custom_1526970892970">
                    <div class="wpb_wrapper"><p>© 2018 All Rights Reserved. Kalvi by <a href="#">Design Themes</a></p>
                    </div>
                </div>
                <div id="1514455995697-be248236-acbb" class="dt-sc-empty-space"></div>
            </div>
        </div>
    </div>
</div>
</p></div></footer></div></div>
<script>(function (body) {
        'use strict';
        body.className = body.className.replace(/\btribe-no-js\b/, 'tribe-js');
    })(document.body);</script>
<script>/*  */
    var tribe_l10n_datatables = {
        "aria": {
            "sort_ascending": ": activate to sort column ascending",
            "sort_descending": ": activate to sort column descending"
        },
        "length_menu": "Show _MENU_ entries",
        "empty_table": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "info_empty": "Showing 0 to 0 of 0 entries",
        "info_filtered": "(filtered from _MAX_ total entries)",
        "zero_records": "No matching records found",
        "search": "Search:",
        "all_selected_text": "All items on this page were selected. ",
        "select_all_link": "Select all pages",
        "clear_selection": "Clear Selection.",
        "pagination": {"all": "All", "next": "Next", "previous": "Previous"},
        "select": {"rows": {"0": "", "_": ": Selected %d rows", "1": ": Selected 1 row"}},
        "datepicker": {
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "monthNamesShort": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "nextText": "Next",
            "prevText": "Prev",
            "currentText": "Today",
            "closeText": "Done"
        }
    };
    var tribe_system_info = {
        "sysinfo_optin_nonce": "11ce2583ef",
        "clipboard_btn_text": "Copy to clipboard",
        "clipboard_copied_text": "System info copied",
        "clipboard_fail_text": "Press \"Cmd + C\" to copy"
    };
    /*  */</script>
<script type="text/javascript">var c = document.body.className;
    c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
    document.body.className = c;</script>
<link rel='stylesheet' id='vc_google_fonts_roboto-css'
      href='{{asset("$public/lms/css/css1dbf.css?family=Roboto&amp;ver=4.9.8")}}'
      type='text/css' media='all'/>
<script type='text/javascript'
        src='{{asset("$public/lms/js/editor.js?ver=2.5.14-6684")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/comment-reply.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>/*  */
    var wpcf7 = {
        "apiSettings": {
            "root": "http:\/\/kalvi.dttheme.com\/default\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/scripts.js?ver=5.0.3")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.tabs.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.tipTip.minified.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.inview.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.animateNumber.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.donutchart.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/slick.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/shortcodes.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='http://kalvi.dttheme.com/default/wp-content/plugins/designthemes-core-features/custom-post-types/js/protfolio-custom.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ui/core.min.js?ver=1.11.4")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ui/widget.min.js?ver=1.11.4")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ui/mouse.min.js?ver=1.11.4")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ui/sortable.min.js?ver=1.11.4")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ui/datepicker.min.js?ver=1.11.4")}}'></script>
<script type='text/javascript'>jQuery(document).ready(function (jQuery) {
        jQuery.datepicker.setDefaults({
            "closeText": "Close",
            "currentText": "Today",
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            "nextText": "Next",
            "prevText": "Previous",
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
            "dateFormat": "MM d, yy",
            "firstDay": 1,
            "isRTL": false
        });
    });</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/chosen.jquery.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.knob.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.knob.custom.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.print.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>/*  */
    var dttheme_urls = {
        "theme_base_url": "http:\/\/kalvi.dttheme.com\/default\/wp-content\/themes\/kalvi",
        "framework_base_url": "http:\/\/kalvi.dttheme.com\/default\/wp-content\/themes\/kalvi\/framework\/",
        "ajaxurl": "http:\/\/kalvi.dttheme.com\/default\/wp-admin\/admin-ajax.php",
        "url": "http:\/\/kalvi.dttheme.com\/default",
        "isRTL": "",
        "loadingbar": "enable",
        "advOptions": "Show Advanced Options",
        "wpnonce": "b4b72cb88e"
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.nicescroll.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.tabs.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/swiper.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.sticky.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.downCount.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/isotope.pkgd.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.scrolltabs.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>/*  */
    var lmscommonobject = {
        "ajaxurl": "http:\/\/kalvi.dttheme.com\/default\/wp-admin\/admin-ajax.php",
        "noResult": "No Results Found!"
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/common.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>/*  */
    var lmsfrontendobject = {
        "pluginPath": "http:\/\/kalvi.dttheme.com\/default\/wp-content\/plugins\/designthemes-lms-addon\/",
        "ajaxurl": "http:\/\/kalvi.dttheme.com\/default\/wp-admin\/admin-ajax.php",
        "quizTimeout": "Timeout!",
        "noGraph": "No enough data to generate graph!",
        "onRefresh": "Refreshing this quiz page will mark this session as completed.",
        "onRefreshCurriculum": "Would you like to abort this quiz session, which will mark this session as completed ?.",
        "registrationSuccess": "You have successfully registered with our class!",
        "locationAlert1": "To get GPS location please fill address.",
        "locationAlert2": "Please add latitude and longitude",
        "submitCourse": "You can submit course only when you have completed all items in course.",
        "submitClass": "You can submit class only when you have submitted all courses.",
        "confirmRegistration": "Please confirm your registration to this class!",
        "closedRegistration": "Regsitration Closed",
        "primarColor": "#ffcc21",
        "quizTimerForegroundColor": "#40c4ff",
        "quizTimerBackgroundColor": "#ffcc21",
        "assignmentNotification": "Please make sure required fields are filled.",
        "printerTitle": "Certificate Printer"
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/frontend.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.pagewalkthrough-1.1.0.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/frontend.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.blockUI.min.js?ver=2.70")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/js.cookie.min.js?ver=2.1.4")}}'></script>
<script type='text/javascript'>/*  */
    var woocommerce_params = {
        "ajax_url": "\/default\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/default\/?wc-ajax=%%endpoint%%"
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/woocommerce.min.js?ver=3.4.4")}}'></script>
<script type='text/javascript'>/*  */
    var wc_cart_fragments_params = {
        "ajax_url": "\/default\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/default\/?wc-ajax=%%endpoint%%",
        "cart_hash_key": "wc_cart_hash_37c987ded18d657dd875e013d373c409",
        "fragment_name": "wc_fragments_37c987ded18d657dd875e013d373c409"
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/cart-fragments.min.js?ver=3.4.4")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.prettyPhoto.min.js?ver=3.1.6")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.selectBox.min.js?ver=1.2.0")}}'></script>
<script type='text/javascript'>/*  */
    var yith_wcwl_l10n = {
        "ajax_url": "\/default\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "is_user_logged_in": "",
        "ajax_loader_url": "http:\/\/kalvi.dttheme.com\/default\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
        "remove_from_wishlist_after_add_to_cart": "yes",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
            "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "move_to_another_wishlist_action": "move_to_another_wishlsit",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
        }
    };
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.yith-wcwl.js?ver=2.2.2")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.ui.totop.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.easing.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.caroufredsel.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.debouncedresize.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.prettyPhoto.min.js?ver=5.4.7")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.touchswipe.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.parallax.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.bxslider.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.fitvids.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.simple-sidebar.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.classie.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.placeholder.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery.visualNav.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ResizeSensor.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/theia-sticky-sidebar.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>/*  */
    var paceOptions = {"restartOnRequestAfter": "false", "restartOnPushState": "false"};
    /*  */</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/pace.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/custom.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/wp-embed.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='http://kalvi.dttheme.com/default/wp-content/plugins/kirki/modules/webfont-loader/vendor-typekit/webfontloader.js?ver=3.0.28")}}'></script>
<script type='text/javascript'>WebFont.load({
        google: {
            families: [
                'Poppins:700,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i,100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i:cyrillic,cyrillic-ext,devanagari,greek,greek-ext,khmer,latin,latin-ext,vietnamese,hebrew,arabic,bengali,gujarati,tamil,telugu,thai',
                'Roboto:300,100,100i,300,300i,400,400i,500,500i,700,700i,900,900i:cyrillic,cyrillic-ext,devanagari,greek,greek-ext,khmer,latin,latin-ext,vietnamese,hebrew,arabic,bengali,gujarati,tamil,telugu,thai'
            ]
        }
    });</script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/js_composer_front.min.js?ver=5.4.7")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/skrollr.min.js?ver=5.4.7")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/jquery-appear.min.js?ver=3.16.24")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/ultimate_bg.min.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='{{asset("$public/lms/js/custom.min.js?ver=3.16.24")}}'></script>
</body>
</html>

