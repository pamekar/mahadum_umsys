<!doctype html>
<!--[if IE 7 ]>
<html lang="en-gb" class="isie ie7 oldie no-js"> <![endif]-->
<!--[if IE 8 ]>
<html lang="en-gb" class="isie ie8 oldie no-js"> <![endif]-->
<!--[if IE 9 ]>
<html lang="en-gb" class="isie ie9 no-js"> <![endif]-->
<!--[if (gt IE 9)|!(IE)]><!-->
<html lang="en-US"> <!--<![endif]-->

<meta http-equiv="content-type" content="text/html;charset=UTF-8"/>
<head>
    <meta charset="utf-8">
    <meta name='viewport' content='width=device-width, initial-scale=1, maximum-scale=1'/>

    <link rel="profile" href="11.html"/>
    <link rel="pingback" href="php/xmlrpc.php"/>

    <script type="text/javascript">document.documentElement.className = document.documentElement.className + ' yes-js js_active js'</script>
    <title>LMS- {{config('app.name')}}</title>
    <style>
        .wishlist_table .add_to_cart, a.add_to_wishlist.button.alt {
            border-radius: 16px;
            -moz-border-radius: 16px;
            -webkit-border-radius: 16px;
        }            </style>

    <script type='text/javascript'>
        var mytheme_urls = {
            theme_base_url: 'https://lmstheme.wpengine.com/wp-content/themes/lms/'
            , framework_base_url: 'https://lmstheme.wpengine.com/wp-content/themes/lms/framework/'
            , ajaxurl: 'https://lmstheme.wpengine.com/wp-admin/admin-ajax.php'
            , url: 'https://lmstheme.wpengine.com'
            , scroll: 'disable'
            , loadisotope: '1'
            , stickynav: 'enable'
            , landingpage: ''
            , landingpagestickynav: 'enable'
            , is_admin: ''
            , skin: 'orange'
            , layout: ''
            , isLandingPage: ''
            , isRTL: ''
            , pluginURL: 'https://lmstheme.wpengine.com/wp-content/plugins/'
            , retinaSupport: 'disable'
            , isResponsive: 'enable'
            , layout_pattern: ''
            , themeName: 'lms'
        };
    </script>
    <link href='{{asset("$public/lms/png/favicon.png")}}' rel='shortcut icon' type='image/x-icon'/>
    <link href='{{asset("$public/lms/png/apple-touch-icon.png")}}' rel='apple-touch-icon-precomposed'/>
    <link href='{{asset("$public/lms/png/apple-touch-icon-114x114.png")}}' sizes='114x114'
          rel='apple-touch-icon-precomposed'/>
    <link href='{{asset("$public/lms/png/apple-touch-icon-72x72.png")}}' sizes='72x72' rel='apple-touch-icon-precomposed'/>
    <link href='{{asset("$public/lms/png/apple-touch-icon-144x144.png")}}' sizes='144x144'
          rel='apple-touch-icon-precomposed'/>

    <!-- All in One SEO Pack 2.7.2 by Michael Torbert of Semper Fi Web Design[732,786] -->
    <meta name="description"
          content="LMS is a e learning theme for online courses. Whether you run online colleges, schools, tutorials or distance learning, LMS is sure to support your lofty goals."/>

    <link rel="canonical" href="index.html"/>
    <!-- /all in one seo pack -->
    <link rel='dns-prefetch' href='index-2.html'/>
    <link rel='dns-prefetch' href='index-3.html'/>
    <link rel='dns-prefetch' href='index-4.html'/>
    <link rel="alternate" type="application/rss+xml" title="LMS &raquo; Feed" href="index-5.html"/>
    <link rel="alternate" type="application/rss+xml" title="LMS &raquo; Comments Feed" href="index-6.html"/>
    <link rel="alternate" type="text/calendar" title="LMS &raquo; iCal Feed" href="indexedf3.html?ical=1"/>
    <link rel="alternate" type="application/rss+xml" title="LMS &raquo; Home Comments Feed" href="index-7.html"/>
    <script type="text/javascript">
        window._wpemojiSettings = {
            "baseUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/72x72\/",
            "ext": ".png",
            "svgUrl": "https:\/\/s.w.org\/images\/core\/emoji\/11\/svg\/",
            "svgExt": ".svg",
            "source": {"concatemoji": "https:\/\/lmstheme.wpengine.com\/wp-includes\/js\/wp-emoji-release.min.js?ver=4.9.8"}
        };
        !function (a, b, c) {
            function d(a, b) {
                var c = String.fromCharCode;
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, a), 0, 0);
                var d = k.toDataURL();
                l.clearRect(0, 0, k.width, k.height), l.fillText(c.apply(this, b), 0, 0);
                var e = k.toDataURL();
                return d === e
            }

            function e(a) {
                var b;
                if (!l || !l.fillText) return !1;
                switch (l.textBaseline = "top", l.font = "600 32px Arial", a) {
                    case"flag":
                        return !(b = d([55356, 56826, 55356, 56819], [
                            55356,
                            56826,
                            8203,
                            55356,
                            56819
                        ])) && (b = d([
                            55356,
                            57332,
                            56128,
                            56423,
                            56128,
                            56418,
                            56128,
                            56421,
                            56128,
                            56430,
                            56128,
                            56423,
                            56128,
                            56447
                        ], [
                            55356,
                            57332,
                            8203,
                            56128,
                            56423,
                            8203,
                            56128,
                            56418,
                            8203,
                            56128,
                            56421,
                            8203,
                            56128,
                            56430,
                            8203,
                            56128,
                            56423,
                            8203,
                            56128,
                            56447
                        ]), !b);
                    case"emoji":
                        return b = d([55358, 56760, 9792, 65039], [55358, 56760, 8203, 9792, 65039]), !b
                }
                return !1
            }

            function f(a) {
                var c = b.createElement("script");
                c.src = a, c.defer = c.type = "text/javascript", b.getElementsByTagName("head")[0].appendChild(c)
            }

            var g, h, i, j, k = b.createElement("canvas"), l = k.getContext && k.getContext("2d");
            for (j = Array("flag", "emoji"), c.supports = {
                everything: !0,
                everythingExceptFlag: !0
            }, i = 0; i < j.length; i++) c.supports[j[i]] = e(j[i]), c.supports.everything = c.supports.everything && c.supports[j[i]], "flag" !== j[i] && (c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && c.supports[j[i]]);
            c.supports.everythingExceptFlag = c.supports.everythingExceptFlag && !c.supports.flag, c.DOMReady = !1, c.readyCallback = function () {
                c.DOMReady = !0
            }, c.supports.everything || (h = function () {
                c.readyCallback()
            }, b.addEventListener ? (b.addEventListener("DOMContentLoaded", h, !1), a.addEventListener("load", h, !1)) : (a.attachEvent("onload", h), b.attachEvent("onreadystatechange", function () {
                "complete" === b.readyState && c.readyCallback()
            })), g = c.source || {}, g.concatemoji ? f(g.concatemoji) : g.wpemoji && g.twemoji && (f(g.twemoji), f(g.wpemoji)))
        }(window, document, window._wpemojiSettings);
    </script>
    <style type="text/css">
        img.wp-smiley,
        img.emoji {
            display: inline !important;
            border: none !important;
            box-shadow: none !important;
            height: 1em !important;
            width: 1em !important;
            margin: 0 .07em !important;
            vertical-align: -0.1em !important;
            background: none !important;
            padding: 0 !important;
        }
    </style>
    <link rel='stylesheet' id='widget-calendar-pro-style-css'
          href='{{asset("$public/lms/css/widget-calendar-full43d0.css?ver=4.4.23")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='tribe_events-widget-calendar-pro-style-css'
          href='{{asset("$public/lms/css/widget-calendar-theme43d0.css?ver=4.4.23")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='tribe_events--widget-calendar-pro-override-style-css'
          href='{{asset("$public/lms/css/widget-calendar-theme43d0.css?ver=4.4.23")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='tribe_events-widget-calendar-pro-override-style-css'
          href='{{asset("$public/lms/css/widget-calendar-theme43d0.css?ver=4.4.23")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-sc-timepicker-addon-css-css'
          href='{{asset("$public/lms/css/jquery-ui-timepicker-addon5010.css?ver=4.9.8")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-animation-css-css' href='{{asset("$public/lms/css/animations5010.css?ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-sc-css-css' href='{{asset("$public/lms/css/shortcodes5010.css?ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='dt-woocommerce-css-css' href='{{asset("$public/lms/css/style5010.css?ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='layerslider-css' href='{{asset("$public/lms/css/layersliderbdeb.css?ver=6.7.0")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='contact-form-7-css' href='{{asset("$public/lms/css/stylesaead.css?ver=5.0.3")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='resmap-css' href='{{asset("$public/lms/css/resmap.mineb11.css?ver=4.4")}}' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='rs-plugin-settings-css' href='{{asset("$public/lms/css/settings8c43.css?ver=5.4.7.1")}}'
          type='text/css' media='all'/>
    <style id='rs-plugin-settings-inline-css")}}' type='text/css'>
        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out;
            line-height: 20px
        }

        .tp-caption a:hover {
            color: #ffa902
        }

        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out
        }

        .tp-caption a:hover {
            color: #ffa902
        }

        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out
        }

        .tp-caption a:hover {
            color: #ffa902
        }

        .largeredbtn {
            font-family: "Raleway", sans-serif;
            font-weight: 900;
            font-size: 16px;
            line-height: 60px;
            color: #fff !important;
            text-decoration: none;
            padding-left: 40px;
            padding-right: 80px;
            padding-top: 22px;
            padding-bottom: 22px;
            background: rgb(234, 91, 31);
            background: -moz-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(234, 91, 31, 1)), color-stop(100%, rgba(227, 58, 12, 1)));
            background: -webkit-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -o-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -ms-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: linear-gradient(to bottom, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ea5b1f', endColorstr='#e33a0c', GradientType=0)
        }

        .largeredbtn:hover {
            background: rgb(227, 58, 12);
            background: -moz-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(227, 58, 12, 1)), color-stop(100%, rgba(234, 91, 31, 1)));
            background: -webkit-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -o-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -ms-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: linear-gradient(to bottom, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e33a0c', endColorstr='#ea5b1f', GradientType=0)
        }

        .fullrounded img {
            -webkit-border-radius: 400px;
            -moz-border-radius: 400px;
            border-radius: 400px
        }

        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out
        }

        .tp-caption a:hover {
            color: #ffa902
        }

        .largeredbtn {
            font-family: "Raleway", sans-serif;
            font-weight: 900;
            font-size: 16px;
            line-height: 60px;
            color: #fff !important;
            text-decoration: none;
            padding-left: 40px;
            padding-right: 80px;
            padding-top: 22px;
            padding-bottom: 22px;
            background: rgb(234, 91, 31);
            background: -moz-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(234, 91, 31, 1)), color-stop(100%, rgba(227, 58, 12, 1)));
            background: -webkit-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -o-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: -ms-linear-gradient(top, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            background: linear-gradient(to bottom, rgba(234, 91, 31, 1) 0%, rgba(227, 58, 12, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#ea5b1f', endColorstr='#e33a0c', GradientType=0)
        }

        .largeredbtn:hover {
            background: rgb(227, 58, 12);
            background: -moz-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -webkit-gradient(linear, left top, left bottom, color-stop(0%, rgba(227, 58, 12, 1)), color-stop(100%, rgba(234, 91, 31, 1)));
            background: -webkit-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -o-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: -ms-linear-gradient(top, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            background: linear-gradient(to bottom, rgba(227, 58, 12, 1) 0%, rgba(234, 91, 31, 1) 100%);
            filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#e33a0c', endColorstr='#ea5b1f', GradientType=0)
        }

        .fullrounded img {
            -webkit-border-radius: 400px;
            -moz-border-radius: 400px;
            border-radius: 400px
        }
    </style>
    <link rel='stylesheet' id='bp-parent-css-css' href='{{asset("$public/lms/css/buddypressf0c3.css?ver=2.9.4")}}'
          type='text/css' media='screen'/>
    <link rel='stylesheet' id='wp-postratings-css' href='{{asset("$public/lms/css/postratings-css1098.css?ver=1.85")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='tribe-events-full-pro-calendar-style-css'
          href='{{asset("$public/lms/css/tribe-events-pro-full.min43d0.css?ver=4.4.23")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='tribe-events-calendar-pro-style-css'
          href='{{asset("$public/lms/css/tribe-events-pro-theme.min43d0.css?ver=4.4.23")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='tribe-events-calendar-full-pro-mobile-style-css'
          href='{{asset("$public/lms/css/tribe-events-pro-full-mobile.min43d0.css?ver=4.4.23")}}' type='text/css'
          media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' id='tribe-events-calendar-pro-mobile-style-css'
          href='{{asset("$public/lms/css/tribe-events-pro-theme-mobile.min43d0.css?ver=4.4.23")}}' type='text/css'
          media='only screen and (max-width: 768px)'/>
    <link rel='stylesheet' id='woocommerce_prettyPhoto_css-css'
          href='{{asset("$public/lms/css/prettyphotod617.css?ver=3.3.2")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-selectBox-css' href='{{asset("$public/lms/css/jquery.selectbox7359.css?ver=1.2.0")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='yith-wcwl-main-css' href='{{asset("$public/lms/css/style605a.css?ver=2.2.2")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='yith-wcwl-font-awesome-css'
          href='{{asset("$public/lms/css/font-awesome.min1849.css?ver=4.7.0")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='bsf-Defaults-css' href='defaults5010.html?ver=4.9.8")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='lms-style-css' href='{{asset("$public/lms/css/style5010-2.css?ver=4.9.8")}}' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='custom-font-awesome-css'
          href='{{asset("$public/lms/css/font-awesome.min5010.css?ver=4.9.8")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='stoke-gap-icons-css'
          href='{{asset("$public/lms/css/stroke-gap-icons.min5010.css?ver=4.9.8")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='skin-css' href='{{asset("$public/lms/css/style5010-3.css?ver=4.9.8")}}' type='text/css'
          media='all'/>
    <link rel='stylesheet' id='font-raleway-css'
          href='{{asset("$public/lms/css/cssfb13.css?family=Raleway%3A400%2C100%2C200%2C300%2C500%2C600%2C800%2C700%2C900&amp;ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='font-opensans-css'
          href='{{asset("$public/lms/css/css822c.css?family=Open+Sans%3A300italic%2C400italic%2C600italic%2C700italic%2C800italic%2C400%2C300%2C600%2C700%2C800&amp;ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='font-dancingscript-css'
          href='{{asset("$public/lms/css/css3a6b.css?family=Dancing+Script&amp;ver=4.9.8")}}' type='text/css' media='all'/>
    <link rel='stylesheet' id='jquery-ui-css' href='{{asset("$public/lms/css/jquery-ui5010.css?ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='responsive-css' href='{{asset("$public/lms/css/responsive5010.css?ver=4.9.8")}}'
          type='text/css' media='all'/>
    <link rel='stylesheet' id='ws-plugin--s2member-css'
          href='{{asset("$public/lms/css/s2member-o6903.css?ws_plugin__s2member_css=1&amp;qcABC=1&amp;ver=170722-170722-1325029721")}}'
          type='text/css' media='all'/>
    <!--[if IE]>
    <style type="text/css" media="screen">
        .team .social-icons li {
            behavior: url(https://lmstheme.wpengine.com/wp-content/themes/lms/PIE.php);
        }
    </style>
    <![endif]-->
    <script type='text/javascript' src='{{asset("$public/lms/js/jqueryb8ff.js?ver=1.12.4")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/jquery-migrate.min330a.js?ver=1.4.1")}}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var TribeMiniCalendar = {"ajaxurl": "https:\/\/lmstheme.wpengine.com\/wp-admin\/admin-ajax.php"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{asset("$public/lms/js/widget-calendar43d0.js?ver=4.4.23")}}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var LS_Meta = {"v": "6.7.0"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{asset("$public/lms/js/greensockb3a6.js?ver=1.19.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/layerslider.kreaturamedia.jquerybdeb.js?ver=6.7.0")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/layerslider.transitionsbdeb.js?ver=6.7.0")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/jquery.themepunch.tools.min8c43.js?ver=5.4.7.1")}}'></script>
    <script type='text/javascript'
            src='{{asset("$public/lms/js/jquery.themepunch.revolution.min8c43.js?ver=5.4.7.1")}}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var wc_add_to_cart_params = {
            "ajax_url": "\/wp-admin\/admin-ajax.php",
            "wc_ajax_url": "https:\/\/lmstheme.wpengine.com\/?wc-ajax=%%endpoint%%",
            "i18n_view_cart": "View cart",
            "cart_url": "https:\/\/lmstheme.wpengine.com\/cart\/",
            "is_cart": "",
            "cart_redirect_after_add": "no"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{asset("$public/lms/js/add-to-cart.mind617.js?ver=3.3.2")}}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var BP_Confirm = {"are_you_sure": "Are you sure?"};
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{asset("$public/lms/js/confirm.minf0c3.js?ver=2.9.4")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/widget-members.minf0c3.js?ver=2.9.4")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/jquery-query.minf0c3.js?ver=2.9.4")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/jquery-cookie.minf0c3.js?ver=2.9.4")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/jquery-scroll-to.minf0c3.js?ver=2.9.4")}}'></script>
    <script type='text/javascript'>
        /* <![CDATA[ */
        var BP_DTheme = {
            "accepted": "Accepted",
            "close": "Close",
            "comments": "comments",
            "leave_group_confirm": "Are you sure you want to leave this group?",
            "mark_as_fav": "Favorite",
            "my_favs": "My Favorites",
            "rejected": "Rejected",
            "remove_fav": "Remove Favorite",
            "show_all": "Show all",
            "show_all_comments": "Show all comments for this thread",
            "show_x_comments": "Show all comments (%d)",
            "unsaved_changes": "Your profile has unsaved changes. If you leave the page, the changes will be lost.",
            "view": "View"
        };
        /* ]]> */
    </script>
    <script type='text/javascript' src='{{asset("$public/lms/js/buddypress.minf0c3.js?ver=2.9.4")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/woocommerce-add-to-cart5243.js?ver=5.4.5")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/modernizr.min5010.js?ver=4.9.8")}}'></script>
    <script type='text/javascript' src='{{asset("$public/lms/js/jquery-ui5010.js?ver=4.9.8")}}'></script>
    <meta name="generator"
          content="Powered by LayerSlider 6.7.0 - Multi-Purpose, Responsive, Parallax, Mobile-Friendly Slider Plugin for WordPress."/>
    <!-- LayerSlider updates and docs at: https://layerslider.kreaturamedia.com -->

    <style type="text/css">
        #logo h2 a {
            color: #;
        }

        .page-id-569 .main-title-section-wrapper {
            margin: 0px;
        }

        .page-id-1521 .main-title-section-wrapper {
            margin: 0px;
        }
    </style>
    <link rel='https://api.w.org/' href='index-8.html'/>
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="php/xmlrpc0db0.php?rsd"/>
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="xml/wlwmanifest.xml"/>
    <link rel='shortlink' href='index.html'/>
    <link rel="alternate" type="application/json+oembed"
          href="json/embed0fb6.json?url=https%3A%2F%2Flmstheme.wpengine.com%2F"/>
    <link rel="alternate" type="text/xml+oembed"
          href="other/embedafba?url=https%3A%2F%2Flmstheme.wpengine.com%2F&amp;format=xml"/>

    <script type="text/javascript">var ajaxurl = 'admin-ajax.html';</script>

    <meta name="tec-api-version" content="v1">
    <meta name="tec-api-origin" content="https://lmstheme.wpengine.com">
    <link rel="https://theeventscalendar.com/" href="index-9.html"/>
    <noscript>
        <style>.woocommerce-product-gallery {
                opacity: 1 !important;
            }</style>
    </noscript>
    <style type="text/css">.recentcomments a {
            display: inline !important;
            padding: 0 !important;
            margin: 0 !important;
        }</style>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress."/>
    <!--[if lte IE 9]>
    <link rel="stylesheet" type="text/css"
          href="https://lmstheme.wpengine.com/wp-content/plugins/js_composer/assets/css/vc_lte_ie9.min.css"
          media="screen"><![endif]-->
    <meta name="generator"
          content="Powered by Slider Revolution 5.4.7.1 - responsive, Mobile-Friendly Slider Plugin for WordPress with comfortable drag and drop interface."/>
    <script type="text/javascript">function setREVStartSize(e) {
            try {
                e.c = jQuery(e.c);
                var i = jQuery(window).width(), t = 9999, r = 0, n = 0, l = 0, f = 0, s = 0, h = 0;
                if (e.responsiveLevels && (jQuery.each(e.responsiveLevels, function (e, f) {
                        f > i && (t = r = f, l = e), i > f && f > r && (r = f, n = e)
                    }), t > r && (l = n)), f = e.gridheight[l] || e.gridheight[0] || e.gridheight, s = e.gridwidth[l] || e.gridwidth[0] || e.gridwidth, h = i / s, h = h > 1 ? 1 : h, f = Math.round(h * f), "fullscreen" == e.sliderLayout) {
                    var u = (e.c.width(), jQuery(window).height());
                    if (void 0 != e.fullScreenOffsetContainer) {
                        var c = e.fullScreenOffsetContainer.split(",");
                        if (c) jQuery.each(c, function (e, i) {
                            u = jQuery(i).length > 0 ? u - jQuery(i).outerHeight(!0) : u
                        }), e.fullScreenOffset.split("%").length > 1 && void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 ? u -= jQuery(window).height() * parseInt(e.fullScreenOffset, 0) / 100 : void 0 != e.fullScreenOffset && e.fullScreenOffset.length > 0 && (u -= parseInt(e.fullScreenOffset, 0))
                    }
                    f = u
                } else void 0 != e.minHeight && f < e.minHeight && (f = e.minHeight);
                e.c.closest(".rev_slider_wrapper").css({height: f})
            } catch (d) {
                console.log("Failure at Presize of Slider:" + d)
            }
        };</script>
    <noscript>
        <style type="text/css"> .wpb_animate_when_almost_visible {
                opacity: 1;
            }</style>
    </noscript>
</head>
<body class="home-page bp-legacy home page-template page-template-tpl-fullwidth page-template-tpl-fullwidth-php page page-id-5009 page-parent page-with-slider tribe-no-js wpb-js-composer js-comp-ver-5.4.5 vc_responsive no-js">
<!-- **DesignThemes Style Picker Wrapper** -->
<div class="dt-style-picker-wrapper"><a href="#" title="" class="style-picker-ico"> <img
                src="{{asset("$public/lms/png/picker-icon.png")}}" alt="" title="" width="50" height="50"/> </a>
    <div id="dt-style-picker"><h2>Select Your Style</h2>
        <h3>Choose your layout</h3>
        <ul class="layout-picker">
            <li><a id="fullwidth" href="#" title="" class="selected"> <img src="{{asset("$public/lms/jpg/fullwidth.jpg")}}"
                                                                           alt="" title="" width="71" height="49"/> </a>
            </li>
            <li><a id="boxed" href="#" title=""> <img src="{{asset("$public/lms/jpg/boxed.jpg")}}" alt="" title=""
                                                      width="71" height="49"/> </a></li>
        </ul>
        <div class="hr"></div>
        <div id="pattern-holder" style="display:none;"><h3>Patterns for Boxed Layout</h3>
            <ul class="pattern-picker">
                <li><a data-image="pattern2.jpg" href="#" title=""><img src="{{asset("$public/lms/jpg/pattern2.jpg")}}"
                                                                        alt="pattern2" title="pattern2" width="30"
                                                                        height="30"/></a></li>
                <li><a data-image="pattern1.jpg" href="#" title=""><img src="{{asset("$public/lms/jpg/pattern1.jpg")}}"
                                                                        alt="pattern1" title="pattern1" width="30"
                                                                        height="30"/></a></li>
                <li><a data-image="pattern5.jpg" href="#" title=""><img src="{{asset("$public/lms/jpg/pattern5.jpg")}}"
                                                                        alt="pattern5" title="pattern5" width="30"
                                                                        height="30"/></a></li>
                <li><a data-image="pattern4.jpg" href="#" title=""><img src="{{asset("$public/lms/jpg/pattern4.jpg")}}"
                                                                        alt="pattern4" title="pattern4" width="30"
                                                                        height="30"/></a></li>
                <li><a data-image="pattern3.jpg" href="#" title=""><img src="{{asset("$public/lms/jpg/pattern3.jpg")}}"
                                                                        alt="pattern3" title="pattern3" width="30"
                                                                        height="30"/></a></li>
            </ul>
            <div class="hr"></div>
        </div>
        <h3>Color scheme</h3>
        <ul class="color-picker">
            <li><a id="cyan" href="#" title=""><img src="{{asset("$public/lms/jpg/cyan.jpg")}}" alt="color-cyan"
                                                    title="cyan" width="30" height="30"/></a></li>
            <li><a id="cyan-yellow" href="#" title=""><img src="{{asset("$public/lms/jpg/cyan-yellow.jpg")}}"
                                                           alt="color-cyan-yellow" title="cyan-yellow" width="30"
                                                           height="30"/></a></li>
            <li><a id="dark-pink" href="#" title=""><img src="{{asset("$public/lms/jpg/dark-pink.jpg")}}"
                                                         alt="color-dark-pink" title="dark-pink" width="30"
                                                         height="30"/></a></li>
            <li><a id="grayish-blue" href="#" title=""><img src="{{asset("$public/lms/jpg/grayish-blue.jpg")}}"
                                                            alt="color-grayish-blue" title="grayish-blue" width="30"
                                                            height="30"/></a></li>
            <li><a id="grayish-green" href="#" title=""><img src="{{asset("$public/lms/jpg/grayish-green.jpg")}}"
                                                             alt="color-grayish-green" title="grayish-green" width="30"
                                                             height="30"/></a></li>
            <li><a id="grayish-orange" href="#" title=""><img src="{{asset("$public/lms/jpg/grayish-orange.jpg")}}"
                                                              alt="color-grayish-orange" title="grayish-orange"
                                                              width="30" height="30"/></a></li>
            <li><a id="light-red" href="#" title=""><img src="{{asset("$public/lms/jpg/light-red.jpg")}}"
                                                         alt="color-light-red" title="light-red" width="30"
                                                         height="30"/></a></li>
            <li><a id="magenta" href="#" title=""><img src="{{asset("$public/lms/jpg/magenta.jpg")}}" alt="color-magenta"
                                                       title="magenta" width="30" height="30"/></a></li>
            <li><a id="orange" href="#" title=""><img src="{{asset("$public/lms/jpg/orange.jpg")}}" alt="color-orange"
                                                      title="orange" width="30" height="30"/></a></li>
            <li><a id="pink" href="#" title=""><img src="{{asset("$public/lms/jpg/pink.jpg")}}" alt="color-pink"
                                                    title="pink" width="30" height="30"/></a></li>
            <li><a id="white-avocado" href="#" title=""><img src="{{asset("$public/lms/jpg/white-avocado.jpg")}}"
                                                             alt="color-white-avocado" title="white-avocado" width="30"
                                                             height="30"/></a></li>
            <li><a id="white-blue" href="#" title=""><img src="{{asset("$public/lms/jpg/white-blue.jpg")}}"
                                                          alt="color-white-blue" title="white-blue" width="30"
                                                          height="30"/></a></li>
            <li><a id="white-blueiris" href="#" title=""><img src="{{asset("$public/lms/jpg/white-blueiris.jpg")}}"
                                                              alt="color-white-blueiris" title="white-blueiris"
                                                              width="30" height="30"/></a></li>
            <li><a id="white-blueturquoise" href="#" title=""><img
                            src="{{asset("$public/lms/jpg/white-blueturquoise.jpg")}}" alt="color-white-blueturquoise"
                            title="white-blueturquoise" width="30" height="30"/></a></li>
            <li><a id="white-brown" href="#" title=""><img src="{{asset("$public/lms/jpg/white-brown.jpg")}}"
                                                           alt="color-white-brown" title="white-brown" width="30"
                                                           height="30"/></a></li>
            <li><a id="white-burntsienna" href="#" title=""><img src="{{asset("$public/lms/jpg/white-burntsienna.jpg")}}"
                                                                 alt="color-white-burntsienna" title="white-burntsienna"
                                                                 width="30" height="30"/></a></li>
            <li><a id="white-chillipepper" href="#" title=""><img src="{{asset("$public/lms/jpg/white-chillipepper.jpg")}}"
                                                                  alt="color-white-chillipepper"
                                                                  title="white-chillipepper" width="30"
                                                                  height="30"/></a></li>
            <li><a id="white-eggplant" href="#" title=""><img src="{{asset("$public/lms/jpg/white-eggplant.jpg")}}"
                                                              alt="color-white-eggplant" title="white-eggplant"
                                                              width="30" height="30"/></a></li>
            <li><a id="white-electricblue" href="#" title=""><img src="{{asset("$public/lms/jpg/white-electricblue.jpg")}}"
                                                                  alt="color-white-electricblue"
                                                                  title="white-electricblue" width="30"
                                                                  height="30"/></a></li>
            <li><a id="white-graasgreen" href="#" title=""><img src="{{asset("$public/lms/jpg/white-graasgreen.jpg")}}"
                                                                alt="color-white-graasgreen" title="white-graasgreen"
                                                                width="30" height="30"/></a></li>
            <li><a id="white-gray" href="#" title=""><img src="{{asset("$public/lms/jpg/white-gray.jpg")}}"
                                                          alt="color-white-gray" title="white-gray" width="30"
                                                          height="30"/></a></li>
            <li><a id="white-green" href="#" title=""><img src="{{asset("$public/lms/jpg/white-green.jpg")}}"
                                                           alt="color-white-green" title="white-green" width="30"
                                                           height="30"/></a></li>
            <li><a id="white-lightred" href="#" title=""><img src="{{asset("$public/lms/jpg/white-lightred.jpg")}}"
                                                              alt="color-white-lightred" title="white-lightred"
                                                              width="30" height="30"/></a></li>
            <li><a id="white-orange" href="#" title=""><img src="{{asset("$public/lms/jpg/white-orange.jpg")}}"
                                                            alt="color-white-orange" title="white-orange" width="30"
                                                            height="30"/></a></li>
            <li><a id="white-palebrown" href="#" title=""><img src="{{asset("$public/lms/jpg/white-palebrown.jpg")}}"
                                                               alt="color-white-palebrown" title="white-palebrown"
                                                               width="30" height="30"/></a></li>
            <li><a id="white-pink" href="#" title=""><img src="{{asset("$public/lms/jpg/white-pink.jpg")}}"
                                                          alt="color-white-pink" title="white-pink" width="30"
                                                          height="30"/></a></li>
            <li><a id="white-radiantorchid" href="#" title=""><img
                            src="{{asset("$public/lms/jpg/white-radiantorchid.jpg")}}" alt="color-white-radiantorchid"
                            title="white-radiantorchid" width="30" height="30"/></a></li>
            <li><a id="white-red" href="#" title=""><img src="{{asset("$public/lms/jpg/white-red.jpg")}}"
                                                         alt="color-white-red" title="white-red" width="30"
                                                         height="30"/></a></li>
            <li><a id="white-skyblue" href="#" title=""><img src="{{asset("$public/lms/jpg/white-skyblue.jpg")}}"
                                                             alt="color-white-skyblue" title="white-skyblue" width="30"
                                                             height="30"/></a></li>
            <li><a id="white-yellow" href="#" title=""><img src="{{asset("$public/lms/jpg/white-yellow.jpg")}}"
                                                            alt="color-white-yellow" title="white-yellow" width="30"
                                                            height="30"/></a></li>
        </ul>
    </div>
</div><!-- **DesignThemes Style Picker Wrapper - End** --><!-- **Wrapper** -->
<div class="wrapper">
    <!-- **Inner Wrapper** -->
    <div class="inner-wrapper">

        <!-- Header Wrapper -->
        <div id="header-wrapper">            <!-- **Header** -->
            <header id="header" class="header1">

                <div class="container">
                    <!-- **Logo - Start** -->
                    <div id="logo">
                        <a href="index.html" title="LMS">
                            <img class="normal_logo" src="{{asset("$public/lms/png/logo.png")}}" alt="LMS" title="LMS"/>
                            <img class="retina_logo" src="{{asset("$public/lms/png/logo%402x.png")}}" alt="LMS" title="LMS"
                                 style="width:98px; height:99px;"/>
                        </a></div><!-- **Logo - End** -->

                    <div class="header-register">
                        <ul class="dt-sc-default-login">
                            <li><a href="index-10.html" title="Login / Register Now"><i
                                            class="fa fa-user"></i>Login<span> | </span>Register</a></li>
                            <li class="dt-sc-cart"><a href="index-11.html"><i class="fa fa-shopping-cart"></i></a></li>
                        </ul>
                    </div>

                    <!-- **Navigation** -->
                    <nav id="main-menu">
                        <div class="dt-menu-toggle" id="dt-menu-toggle">
                            Menu <span class="dt-menu-toggle-icon"></span>
                        </div>

                        <ul id="menu-top-menu" class="menu">
                            <li id="menu-item-5028"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-home current-menu-item page_item page-item-5009 current_page_item menu-item-has-children menu-item-depth-0 menu-item-simple-parent ">
                                <a href="index.html"><i class='fa fa-home'></i>Home</a>


                                <ul class="sub-menu">
                                    <li id="menu-item-4783"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-12.html">Home II</a></li>
                                    <li id="menu-item-4800"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-13.html">Home &#8211; Landing Page</a></li>
                                    <li id="menu-item-4832"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-14.html">Home – Subscription</a></li>
                                    <li id="menu-item-4869"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-15.html">Home – Course Search</a></li>
                                </ul>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-4749"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-simple-parent ">
                                <a href="index-16.html"><i class='fa fa-book'></i>Courses</a>


                                <ul class="sub-menu">
                                    <li id="menu-item-6911"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-17.html">Courses Template</a></li>
                                    <li id="menu-item-4750"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1">
                                        <a href="index-18.html">Lessons</a></li>
                                    <li id="menu-item-4755"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-19.html">Search Course</a></li>
                                    <li id="menu-item-6538"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-20.html">Membership</a></li>
                                </ul>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-4355"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-simple-parent ">
                                <a href="index-21.html"><i class='fa fa-paste'></i>Pages</a>


                                <ul class="sub-menu">
                                    <li id="menu-item-4405"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-1">
                                        <a href="index-22.html">BuddyPress</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-4402"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-2">
                                                <a href="index-23.html">Members</a>
                                                <ul class="sub-menu">
                                                    <li id="menu-item-4404"
                                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-3">
                                                        <a href="index-24.html">Activate</a></li>
                                                </ul>
                                                <a class="dt-menu-expand">+</a></li>
                                            <li id="menu-item-4400"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                <a href="index-22.html">Site-Wide Activity</a></li>
                                        </ul>
                                        <a class="dt-menu-expand">+</a></li>
                                    <li id="menu-item-4410"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                        <a href="index-25.html">Events</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-4411"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-2">
                                                <a href="index-26.html">Event Listing</a></li>
                                            <li id="menu-item-4412"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-2">
                                                <a href="index-27.html">Events Week</a></li>
                                            <li id="menu-item-4413"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-2">
                                                <a href="index-28.html">Events Day</a></li>
                                            <li id="menu-item-4414"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-2">
                                                <a href="index5d78.html?tribe-bar-date=2016-02-04">Events Map</a></li>
                                            <li id="menu-item-4415"
                                                class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-2">
                                                <a href="index-29.html">Events Photo</a></li>
                                        </ul>
                                        <a class="dt-menu-expand">+</a></li>
                                    <li id="menu-item-4224"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                        <a href="#">Headers</a>
                                        <ul class="sub-menu">
                                            <li id="menu-item-4261"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                <a href="index-30.html">Header1</a></li>
                                            <li id="menu-item-4262"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                <a href="index-31.html">Header2</a></li>
                                            <li id="menu-item-4263"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                <a href="index-32.html">Header3</a></li>
                                            <li id="menu-item-4264"
                                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                <a href="index-33.html">Header4</a></li>
                                        </ul>
                                        <a class="dt-menu-expand">+</a></li>
                                    <li id="menu-item-4364"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-34.html">Side Navigation</a></li>
                                    <li id="menu-item-4258"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-21.html">About</a></li>
                                    <li id="menu-item-4358"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a href="index-35.html">Teachers</a></li>
                                    <li id="menu-item-4665"
                                        class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1">
                                        <a href="index-36.html">Teachers Profile</a></li>
                                </ul>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-4260"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-megamenu-parent  megamenu-3-columns-group">
                                <a href="index-37.html"><i class='fa fa-rocket'></i>Shortcodes</a>
                                <div class='megamenu-child-container'>

                                    <ul class="sub-menu">
                                        <li id="menu-item-4962"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-with-widget-area ">
                                            <a href="#">Popular Courses</a>
                                            <div class="menu-item-widget-area-container">
                                                <ul>
                                                    <li id="my_course_widget-5" class="widget widget_popular_entries">
                                                        <div class='recent-course-widget'>
                                                            <ul>
                                                                <li>
                                                                    <img src="{{asset("$public/lms/jpg/course13-110x90.jpg")}}"
                                                                         alt="Power Electronics" width="110"
                                                                         height="90"/><h6><a href='index-38.html'>Power
                                                                            Electronics</a></h6><span
                                                                            class="dt-sc-course-price">
									<span class="amount">$50</span>
							</span></li>
                                                                <li>
                                                                    <img src="{{asset("$public/lms/jpg/course9-110x90.jpg")}}"
                                                                         alt="Processing Digital Signal" width="110"
                                                                         height="90"/><h6><a href='index-39.html'>Processing
                                                                            Digi...</a></h6><span
                                                                            class="dt-sc-course-price">
									<span class="amount">Free</span>
							</span></li>
                                                                <li>
                                                                    <img src="{{asset("$public/lms/jpg/course8-110x90.jpg")}}"
                                                                         alt="Analysis of Algorithms" width="110"
                                                                         height="90"/><h6><a href='index-40.html'>Analysis
                                                                            of Alg...</a></h6><span
                                                                            class="dt-sc-course-price">
									<span class="amount">Free</span>
							</span></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li id="menu-item-5263"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-with-widget-area ">
                                            <a href="#">Recent From Gallery</a>
                                            <div class="menu-item-widget-area-container">
                                                <ul>
                                                    <li id="my_portfolio_widget-8"
                                                        class="widget widget_popular_entries">
                                                        <div class='recent-portfolio-widget'>
                                                            <ul>
                                                                <li><h6><a href='index-41.html'>Life is a choice</a>
                                                                    </h6>
                                                                    <p>There are many variations...</p></li>
                                                                <li><h6><a href='index-42.html'>Reach for the stars</a>
                                                                    </h6>
                                                                    <p>Aenean leo ligula, porttitor...</p></li>
                                                                <li><h6><a href='index-43.html'>Live and let live</a>
                                                                    </h6>
                                                                    <p>There are many variations...</p></li>
                                                                <li><h6><a href='index-44.html'>Happiness Is A ...</a>
                                                                    </h6>
                                                                    <p>Aenean leo ligula, porttitor...</p></li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li id="menu-item-4963"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-with-widget-area ">
                                            <a href="#">Special Offers</a>
                                            <div class="menu-item-widget-area-container">
                                                <ul>
                                                    <li id="text-3" class="widget widget_text">
                                                        <div class="textwidget"><a href="#"> <img
                                                                        src="{{asset("$public/lms/png/lms-megamenu-deal1.png")}}"
                                                                        alt="" title="" width="220" height="171"/> </a>

                                                            <div class='dt-sc-hr-invisible-small  '></div>

                                                            <a href="#"> <img
                                                                        src="{{asset("$public/lms/jpg/lms-megamenu-deal2.jpg")}}"
                                                                        alt="" title="" width="220" height="93"/> </a>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li id="menu-item-5264"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-fullwidth  menu-item-with-widget-area  fill-three-columns ">
                                            <a href="#">Quick Links</a>
                                            <div class="menu-item-widget-area-container">
                                                <ul>
                                                    <li id="text-9" class="widget widget_text">
                                                        <div class="textwidget">
                                                            <ul class="quick-links">
                                                                <li><a href="index-37.html" title=""> Typography </a>
                                                                </li>
                                                                <li><a href="index-45.html" title=""> Tabs &
                                                                        Toggles </a></li>
                                                                <li><a href="index-46.html" title=""> Buttons &
                                                                        Lists </a></li>
                                                                <li><a href="index-47.html" title=""> Fancy Boxes </a>
                                                                </li>
                                                                <li><a href="index-48.html" title="">Pricing Tables </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li id="text-10" class="widget widget_text">
                                                        <div class="textwidget">
                                                            <ul class="quick-links">
                                                                <li><a href="index-49.html" title=""> Progress Bars </a>
                                                                </li>
                                                                <li><a href="index-50.html" title=""> Columns </a></li>
                                                                <li><a href="index-51.html" title=""> Quotes </a></li>
                                                                <li><a href="index-52.html" title=""> Callout Boxes </a>
                                                                </li>
                                                                <li><a href="index-53.html" title=""> Icon Boxes </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                    <li id="text-11" class="widget widget_text">
                                                        <div class="textwidget">
                                                            <ul class="quick-links">
                                                                <li><a href="index-54.html" title=""> Fullwidth
                                                                        parallax </a></li>
                                                                <li><a href="index-54.html#clients_carousel" title="">
                                                                        Client Carousel </a></li>
                                                                <li><a href="index-54.html#testimonial_carousel"
                                                                       title=""> Testimonial Parallax </a></li>
                                                                <li><a href="index-55.html" title=""> Miscellaneous </a>
                                                                </li>
                                                                <li><a href="index-56.html" title=""> Contact Info </a>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <a class="dt-menu-expand">+</a>
                                </div>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-6794"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-megamenu-parent  megamenu-4-columns-group">
                                <a href="index-57.html"><i class='fa fa-file-text'></i>Blog</a>
                                <div class='megamenu-child-container'>

                                    <ul class="sub-menu">
                                        <li id="menu-item-4227"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">I Column</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4280"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-58.html">Fullwidth</a></li>
                                                <li id="menu-item-4282"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-59.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4284"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-60.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4281"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-61.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4283"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-62.html">With Pagination</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4228"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">II Column</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4289"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-57.html">Fullwidth</a></li>
                                                <li id="menu-item-4286"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-63.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4288"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-64.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4285"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-65.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4287"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-66.html">With Pagination</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4229"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">III Column</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4290"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-67.html">Fullwidth</a></li>
                                                <li id="menu-item-4292"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-68.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4294"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-69.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4291"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-70.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4293"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-71.html">With Pagination</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4230"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">Thumb Image</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4295"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-72.html">Fullwidth</a></li>
                                                <li id="menu-item-4297"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-73.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4299"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-74.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4296"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-75.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4298"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-76.html">With Pagination</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4235"
                                            class="fulwidth-image-link menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1 menu-item-fullwidth ">
                                            <a href="#">Fullwidth image</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4236"
                                                    class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-2">
                                                    <span class="nolink-menu"></span>
                                                    <div class='dt-megamenu-custom-content'><img
                                                                src="{{asset("$public/lms/png/mega-menu-image.png")}}"
                                                                alt="" title="" width="970" height="255"/></div>
                                                </li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                    </ul>
                                    <a class="dt-menu-expand">+</a>
                                </div>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-4301"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-megamenu-parent  megamenu-4-columns-group">
                                <a href="index-77.html"><i class='fa fa-camera'></i>Gallery</a>
                                <div class='megamenu-child-container'>

                                    <ul class="sub-menu">
                                        <li id="menu-item-4232"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">II Columns</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4311"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-78.html">Fullwidth</a></li>
                                                <li id="menu-item-4313"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-79.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4315"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-80.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4312"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-81.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4317"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-82.html">Without Space</a></li>
                                                <li id="menu-item-4314"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-83.html">With Pagination</a></li>
                                                <li id="menu-item-4316"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-84.html">Without Filter</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4233"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">III Columns</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4318"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-85.html">Fullwidth</a></li>
                                                <li id="menu-item-4320"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-86.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4322"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-87.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4319"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-88.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4324"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-89.html">Without Space</a></li>
                                                <li id="menu-item-4321"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-90.html">With Pagination</a></li>
                                                <li id="menu-item-4323"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-91.html">Without Filter</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4234"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-depth-1">
                                            <a href="#">IV Columns</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4325"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-92.html">Fullwidth</a></li>
                                                <li id="menu-item-4327"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-93.html">With Left Sidebar</a></li>
                                                <li id="menu-item-4329"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-94.html">With Right Sidebar</a></li>
                                                <li id="menu-item-4326"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-95.html">With Both Sidebar</a></li>
                                                <li id="menu-item-4331"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-96.html">Without Space</a></li>
                                                <li id="menu-item-4328"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-97.html">With Pagination</a></li>
                                                <li id="menu-item-4330"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-98.html">Without Filter</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4965"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-with-widget-area ">
                                            <a href="#">Contact us Now</a>
                                            <div class="menu-item-widget-area-container">
                                                <ul>
                                                    <li id="text-4" class="widget widget_text">
                                                        <div class="textwidget">
                                                            <div role="form" class="wpcf7" id="wpcf7-f4961-o1"
                                                                 lang="en-US" dir="ltr">
                                                                <div class="screen-reader-response"></div>
                                                                <form action="https://lmstheme.wpengine.com/#wpcf7-f4961-o1"
                                                                      method="post" class="wpcf7-form"
                                                                      novalidate="novalidate">
                                                                    <div style="display: none;">
                                                                        <input type="hidden" name="_wpcf7"
                                                                               value="4961"/>
                                                                        <input type="hidden" name="_wpcf7_version"
                                                                               value="5.0.3"/>
                                                                        <input type="hidden" name="_wpcf7_locale"
                                                                               value="en_US"/>
                                                                        <input type="hidden" name="_wpcf7_unit_tag"
                                                                               value="wpcf7-f4961-o1"/>
                                                                        <input type="hidden"
                                                                               name="_wpcf7_container_post" value="0"/>
                                                                    </div>
                                                                    <p>
                                                                        <span class="wpcf7-form-control-wrap your-name"><input
                                                                                    type="text" name="your-name"
                                                                                    value="" size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required"
                                                                                    aria-required="true"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Name"/></span></p>
                                                                    <p><span class="wpcf7-form-control-wrap your-email"><input
                                                                                    type="email" name="your-email"
                                                                                    value="" size="40"
                                                                                    class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email"
                                                                                    aria-required="true"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Email"/></span></p>
                                                                    <p>
                                                                        <span class="wpcf7-form-control-wrap your-message"><textarea
                                                                                    name="your-message" cols="40"
                                                                                    rows="10"
                                                                                    class="wpcf7-form-control wpcf7-textarea"
                                                                                    aria-invalid="false"
                                                                                    placeholder="Message"></textarea></span>
                                                                    </p>
                                                                    <p><input type="submit" value="Submit"
                                                                              class="wpcf7-form-control wpcf7-submit"/>
                                                                    </p>
                                                                    <div class="wpcf7-response-output wpcf7-display-none"></div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul>
                                    <a class="dt-menu-expand">+</a>
                                </div>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-4338"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-megamenu-parent  megamenu-3-columns-group">
                                <a href="index-99.html"><i class='fa fa-shopping-cart'></i>Shop</a>
                                <div class='megamenu-child-container'>

                                    <ul class="sub-menu">
                                        <li id="menu-item-4970"
                                            class="menu-item menu-item-type-custom menu-item-object-custom menu-item-depth-1 menu-item-with-widget-area ">
                                            <a href="#">Popular Products</a>
                                            <div class="menu-item-widget-area-container">
                                                <ul>
                                                    <li id="woocommerce_products-5"
                                                        class="widget woocommerce widget_products">
                                                        <ul class="product_list_widget">
                                                            <li>

                                                                <a href="index-100.html">
                                                                    <img width="150" height="150"
                                                                         src="{{asset("$public/lms/jpg/dark-book.jpg")}}"
                                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                         alt=""
                                                                         srcset="//lmstheme.wpengine.com/wp-content/uploads/2013/06/dark-book.jpg 1000w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/dark-book-150x150.jpg 150w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/dark-book-300x300.jpg 300w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/dark-book-100x100.jpg 100w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/dark-book-95x95.jpg 95w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/dark-book-600x600.jpg 600w"
                                                                         sizes="(max-width: 150px) 100vw, 150px"/>
                                                                    <span class="product-title">Information Architecture</span>
                                                                </a>

                                                                <div class="star-rating"><span style="width:100%">Rated <strong
                                                                                class="rating">5.00</strong> out of 5</span>
                                                                </div>
                                                                <span class="woocommerce-Price-amount amount"><span
                                                                            class="woocommerce-Price-currencySymbol">&pound;</span>9.00</span>
                                                            </li>
                                                            <li>

                                                                <a href="index-101.html">
                                                                    <img width="150" height="150"
                                                                         src="{{asset("$public/lms/jpg/cover2.jpg")}}"
                                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                         alt=""
                                                                         srcset="//lmstheme.wpengine.com/wp-content/uploads/2013/06/cover2.jpg 1000w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/cover2-150x150.jpg 150w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/cover2-300x300.jpg 300w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/cover2-100x100.jpg 100w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/cover2-95x95.jpg 95w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/cover2-600x600.jpg 600w"
                                                                         sizes="(max-width: 150px) 100vw, 150px"/> <span
                                                                            class="product-title">Business Intelligence</span>
                                                                </a>

                                                                <div class="star-rating"><span
                                                                            style="width:60%">Rated <strong
                                                                                class="rating">3.00</strong> out of 5</span>
                                                                </div>
                                                                <span class="woocommerce-Price-amount amount"><span
                                                                            class="woocommerce-Price-currencySymbol">&pound;</span>9.00</span>
                                                            </li>
                                                            <li>

                                                                <a href="index-102.html">
                                                                    <img width="150" height="150"
                                                                         src="{{asset("$public/lms/jpg/note.jpg")}}"
                                                                         class="attachment-woocommerce_thumbnail size-woocommerce_thumbnail wp-post-image"
                                                                         alt=""
                                                                         srcset="//lmstheme.wpengine.com/wp-content/uploads/2013/06/note.jpg 1000w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/note-150x150.jpg 150w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/note-300x300.jpg 300w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/note-100x100.jpg 100w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/note-95x95.jpg 95w, //lmstheme.wpengine.com/wp-content/uploads/2013/06/note-600x600.jpg 600w"
                                                                         sizes="(max-width: 150px) 100vw, 150px"/> <span
                                                                            class="product-title">Advanced Technolgies</span>
                                                                </a>

                                                                <div class="star-rating"><span
                                                                            style="width:80%">Rated <strong
                                                                                class="rating">4.00</strong> out of 5</span>
                                                                </div>
                                                                <span class="woocommerce-Price-amount amount"><span
                                                                            class="woocommerce-Price-currencySymbol">&pound;</span>9.00</span>
                                                            </li>
                                                        </ul>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                        <li id="menu-item-4350"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-1">
                                            <a href="index-99.html">Shop Now</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4351"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-103.html">Shop Four Column</a></li>
                                                <li id="menu-item-4353"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-104.html">Shop Three Column</a></li>
                                                <li id="menu-item-4354"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-105.html">Shop Two column</a></li>
                                                <li id="menu-item-4342"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-11.html">Cart</a></li>
                                                <li id="menu-item-4339"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-106.html">Checkout</a></li>
                                                <li id="menu-item-4341"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-107.html">Checkout → Pay</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                        <li id="menu-item-4332"
                                            class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-1">
                                            <a href="index-108.html">My Account</a>
                                            <ul class="sub-menu">
                                                <li id="menu-item-4335"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-109.html">View Order</a></li>
                                                <li id="menu-item-4340"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-110.html">Order Received</a></li>
                                                <li id="menu-item-4552"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-111.html">Edit Account</a></li>
                                                <li id="menu-item-4336"
                                                    class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-2">
                                                    <a href="index-112.html">Edit My Address</a></li>
                                            </ul>
                                            <a class="dt-menu-expand">+</a></li>
                                    </ul>
                                    <a class="dt-menu-expand">+</a>
                                </div>
                                <a class="dt-menu-expand">+</a></li>
                            <li id="menu-item-4300"
                                class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-depth-0 menu-item-simple-parent ">
                                <a href="index-113.html"><i class='fa fa-location-arrow'></i>Contact</a>


                                <ul class="sub-menu">
                                    <li id="menu-item-4302"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a
                                                href="index-114.html">Contact Layout 2</a></li>
                                    <li id="menu-item-4303"
                                        class="menu-item menu-item-type-post_type menu-item-object-page menu-item-depth-1">
                                        <a
                                                href="index-115.html">Contact Layout 3</a></li>
                                </ul>
                                <a class="dt-menu-expand">+</a></li>
                        </ul>
                    </nav><!-- **Navigation - End** -->

                </div>
            </header><!-- **Header - End** -->            </div><!-- Header Wrapper -->

        <!-- **Main** -->
        <div id="main">

            <!-- Slider Section -->
            <!-- **Slider Section** -->
            <div id="slider">
                <div id="slider-container"><img src="{{asset("$public/lms/jpg/fullscreen-slider.jpg")}}" alt=""/></div>
                <div id="slider-search-container">
                    <div class="slider-search">
                        <form method="get" id="courses-search" class="courses-search"
                              action="https://lmstheme.wpengine.com/search-course/">
                            <input id="searchtext" name="searchtext" type="text" value="" placeholder="Search Course"/>
                            <input type="hidden" name="search-type" value="courses"/>
                            <input type="hidden" name="lang" value=""/>
                            <input type="submit" value="">
                            <div class="course-title-holder hidden">["Advanced Programming","Analysis of
                                Algorithms","Architecture Analysis & Design","Basic Laws and Policies","Emerging Trends
                                and
                                Technologies","Fitness Training Program","General Business Law","Healthcare
                                Delivery","Introduction to Calculus","Maintenance Training","Power
                                Electronics","Processing
                                Digital Signal"]
                            </div>
                        </form>
                        <h4>We have the largest collection of courses</h4><a href="index-116.html"
                                                                             title="View All Courses">
                            View All Courses <span class="fa fa-angle-double-right"> </span> </a></div>
                    <div class="dt-sc-clear"></div>

                    <div class='dt-sc-hr-invisible-medium  '></div>

                    <div class='dt-sc-hr-invisible-medium  '></div>

                    <div class='column dt-sc-one-fourth  space  first'>
                        <div class="dt-sc-counter" data-counter="332">
                            <div class="dt-sc-counter-number"> 332</div>
                            <h5> Courses <span> </span></h5>
                        </div>
                    </div>
                    <div class='column dt-sc-one-fourth  space  '>
                        <div class="dt-sc-counter" data-counter="1403">
                            <div class="dt-sc-counter-number"> 1403</div>
                            <h5> Members <span> </span></h5>
                        </div>
                    </div>
                    <div class='column dt-sc-one-fourth  space  '>
                        <div class="dt-sc-counter" data-counter="60">
                            <div class="dt-sc-counter-number"> 60</div>
                            <h5> Authors <span> </span></h5>
                        </div>
                    </div>
                    <div class='column dt-sc-one-fourth  space  '>
                        <div class="dt-sc-counter" data-counter="120">
                            <div class="dt-sc-counter-number"> 120</div>
                            <h5> Subjects <span> </span></h5>
                        </div>
                    </div>
                </div>
            </div><!-- **Slider Section - End** -->
            <!-- Sub Title Section -->
            <!-- Sub Title Section -->


            <!-- ** Primary Section ** -->
            <section id="primary" class="content-full-width"><!-- #post-5009 -->
                <div id="post-5009" class="post-5009 page type-page status-publish hentry">
                    <div class='fullwidth-section  '>
                        <div class="container">
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <div class='column dt-sc-one-fourth  space  first'>
                                <div class='dt-sc-ico-content type14'>
                                    <div class='custom-icon'><a href='#' target='_blank'><span
                                                    class='fa fa-custom-icon1'></span></a></div>
                                    <h4><a href='#' target='_blank'> Best Stimulations </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-fourth  space  '>
                                <div class='dt-sc-ico-content type14'>
                                    <div class='custom-icon'><a href='#' target='_blank'><span
                                                    class='fa fa-custom-icon2'></span></a></div>
                                    <h4><a href='#' target='_blank'> Group Seminars </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-fourth  space  '>
                                <div class='dt-sc-ico-content type14'>
                                    <div class='custom-icon'><a href='#' target='_blank'><span
                                                    class='fa fa-custom-icon3'></span></a></div>
                                    <h4><a href='#' target='_blank'> Analysed Syllabus </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-fourth  space  '>
                                <div class='dt-sc-ico-content type14'>
                                    <div class='custom-icon'><a href='#' target='_blank'><span
                                                    class='fa fa-custom-icon4'></span></a></div>
                                    <h4><a href='#' target='_blank'> Pratical Training </a></h4></div>
                            </div>
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <h2 class='border-title  '>Courses<span></span></h2>
                            <div class='column dt-sc-one-third  space  first'><h3>About Courses</h3>
                                <p>Student Registration and Administration Nemo enim ipsam voluptatem quia voluptas sit
                                    atur
                                    aut odit aut fugit, sed quia consequuntur magni res eos qui ratione voluptatem sequi
                                    nesciunt.</p>
                                <div class='dt-sc-hr-invisible-small  '></div>
                                <p>There are many variations of passages of Lorem Ipsum available, but the majority have
                                    suffered alteration in some form, by injected humour, or randomised believable.</p>
                                <div class='dt-sc-hr-invisible-small  '></div>
                                <a href='index-16.html' target='_blank' class='dt-sc-button   small  '>View All
                                    Courses</a>
                            </div>
                            <div class='column dt-sc-two-third  space  '>
                                <div class="dt-sc-course-carousel-wrapper">
                                    <ul class="dt-sc-course-carousel" data-column="2">
                                        <li class="column dt-sc-one-half">
                                            <article id="post-5993"
                                                     class="dt-sc-custom-course-type  post-5993 dt_courses type-dt_courses status-publish has-post-thumbnail hentry course_category-education">
                                                <div class="dt-sc-course-thumb">
                                                    <a href="index-38.html"><img
                                                                src='{{asset("$public/lms/jpg/course13-573x403.jpg")}}")}}'
                                                                width='573' height='403'/></a>
                                                    <div class="dt-sc-course-overlay">
                                                        <a title="Power Electronics" href="index-38.html"
                                                           class="dt-sc-button small white">View Course</a>
                                                    </div>
                                                </div>
                                                <div class="dt-sc-course-details"><span class="dt-sc-course-price"><span
                                                                class="amount">$50</span></span><h5><a
                                                                href="index-38.html"
                                                                title="Power Electronics">Power
                                                            Electronics</a></h5>
                                                    <div class="dt-sc-course-meta">
                                                        <p><a href="index-117.html" rel="tag">Education</a></p>
                                                        <p>6&nbsp;Lessons</p>
                                                    </div>
                                                    <div class="dt-sc-course-data">
                                                        <div class="dt-sc-course-duration">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span>05 : 35</span>
                                                        </div>
                                                        <span id="post-ratings-5993" class="post-ratings" itemscope
                                                              itemtype="http://schema.org/Article"
                                                              data-nonce="4ce3d39303"><img id="rating_5993_1"
                                                                                           src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                                           alt="1 Star" title="1 Star"
                                                                                           onmouseover="current_rating(5993, 1, '1 Star');"
                                                                                           onmouseout="ratings_off(4.2, 0, 0);"
                                                                                           onclick="rate_post();"
                                                                                           onkeypress="rate_post();"
                                                                                           style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5993_2"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="2 Stars" title="2 Stars"
                                                                    onmouseover="current_rating(5993, 2, '2 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5993_3"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="3 Stars" title="3 Stars"
                                                                    onmouseover="current_rating(5993, 3, '3 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5993_4"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="4 Stars" title="4 Stars"
                                                                    onmouseover="current_rating(5993, 4, '4 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5993_5"
                                                                    src="{{asset("$public/lms/gif/rating_off.gif")}}"
                                                                    alt="5 Stars" title="5 Stars"
                                                                    onmouseover="current_rating(5993, 5, '5 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/> (<strong>318</strong> votes, average: <strong>4.20</strong> out of 5)<span
                                                                    class="post-ratings-text"
                                                                    id="ratings_5993_text"></span><meta
                                                                    itemprop="headline" content="Power Electronics"/><meta
                                                                    itemprop="description"
                                                                    content="Ut ac euismod velit. Aliquam condimentum dolor accumsan, venenatis sapien eu, egestas nunc. Suspendisse euismod semper fermentum. Nulla eleifend nibh pretium mattis dapibus."/><meta
                                                                    itemprop="datePublished"
                                                                    content="2014-08-11T04:21:43+00:00"/><meta
                                                                    itemprop="dateModified"
                                                                    content="2015-12-22T05:53:46+00:00"/><meta
                                                                    itemprop="url" content="index-38.html"/><meta
                                                                    itemprop="author" content="ram"/><meta
                                                                    itemprop="mainEntityOfPage"
                                                                    content="index-38.html"/><div style="display: none;"
                                                                                                  itemprop="image"
                                                                                                  itemscope
                                                                                                  itemtype="https://schema.org/ImageObject"><meta
                                                                        itemprop="url"
                                                                        content="{{asset("$public/lms/jpg/course13-150x150.jpg")}}"/><meta
                                                                        itemprop="width" content="150"/><meta
                                                                        itemprop="height"
                                                                        content="150"/>
                                                    </div>
                                                    <div style="display: none;" itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <meta itemprop="name" content="LMS"/>
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content=""/>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;" itemprop="aggregateRating" itemscope
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="bestRating" content="5"/>
                                                        <meta itemprop="worstRating" content="1"/>
                                                        <meta itemprop="ratingValue" content="4.2"/>
                                                        <meta itemprop="ratingCount" content="318"/>
                                                    </div>
                                                    </span><span id="post-ratings-5993-loading"
                                                                 class="post-ratings-loading">
            <img src="{{asset("$public/lms/gif/loading.gif")}}" width="16" height="16" class="post-ratings-image"/>Loading...</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <li class="column dt-sc-one-half">
                                            <article id="post-5992"
                                                     class="dt-sc-custom-course-type  post-5992 dt_courses type-dt_courses status-publish has-post-thumbnail hentry course_category-mathematics">
                                                <div class="dt-sc-course-thumb">
                                                    <a href="index-118.html"><img
                                                                src='{{asset("$public/lms/jpg/course14-573x403.jpg")}}"
                 )}}' width='573' height='403'/></a>
                                                    <div class="dt-sc-course-overlay">
                                                        <a title="Introduction to Calculus" href="index-118.html"
                                                           class="dt-sc-button small white">View Course</a>
                                                    </div>
                                                </div>
                                                <div class="dt-sc-course-details"><span class="dt-sc-course-price"><span
                                                                class="amount">Free</span></span><h5><a
                                                                href="index-118.html" title="Introduction to Calculus">Introduction
                                                            to Calculus</a></h5>
                                                    <div class="dt-sc-course-meta">
                                                        <p><a href="index-119.html" rel="tag">Mathematics</a></p>
                                                        <p>5&nbsp;Lessons</p>
                                                    </div>
                                                    <div class="dt-sc-course-data">
                                                        <div class="dt-sc-course-duration">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span>03 : 15</span>
                                                        </div>
                                                        <span id="post-ratings-5992" class="post-ratings" itemscope
                                                              itemtype="http://schema.org/Article"
                                                              data-nonce="302776f11e"><img id="rating_5992_1"
                                                                                           src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                                           alt="1 Star" title="1 Star"
                                                                                           onmouseover="current_rating(5992, 1, '1 Star');"
                                                                                           onmouseout="ratings_off(4.2, 0, 0);"
                                                                                           onclick="rate_post();"
                                                                                           onkeypress="rate_post();"
                                                                                           style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5992_2"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="2 Stars" title="2 Stars"
                                                                    onmouseover="current_rating(5992, 2, '2 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5992_3"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="3 Stars" title="3 Stars"
                                                                    onmouseover="current_rating(5992, 3, '3 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5992_4"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="4 Stars" title="4 Stars"
                                                                    onmouseover="current_rating(5992, 4, '4 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5992_5"
                                                                    src="{{asset("$public/lms/gif/rating_off.gif")}}"
                                                                    alt="5 Stars" title="5 Stars"
                                                                    onmouseover="current_rating(5992, 5, '5 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/> (<strong>293</strong> votes, average: <strong>4.18</strong> out of 5)<span
                                                                    class="post-ratings-text"
                                                                    id="ratings_5992_text"></span><meta
                                                                    itemprop="headline"
                                                                    content="Introduction to Calculus"/><meta
                                                                    itemprop="description"
                                                                    content="Fusce commodo in neque vitae mollis. Sed dolor lorem, cursus et venenatis sed, sodales eu nisi. Nunc lacinia massa neque, eu tincidunt mi blandit eget. "/><meta
                                                                    itemprop="datePublished"
                                                                    content="2014-08-11T04:18:26+00:00"/><meta
                                                                    itemprop="dateModified"
                                                                    content="2014-08-11T11:40:57+00:00"/><meta
                                                                    itemprop="url" content="index-118.html"/><meta
                                                                    itemprop="author" content="ram"/><meta
                                                                    itemprop="mainEntityOfPage"
                                                                    content="index-118.html"/><div
                                                                    style="display: none;" itemprop="image" itemscope
                                                                    itemtype="https://schema.org/ImageObject"><meta
                                                                        itemprop="url"
                                                                        content="{{asset("$public/lms/jpg/course14-150x150.jpg")}}"/><meta
                                                                        itemprop="width" content="150"/><meta
                                                                        itemprop="height"
                                                                        content="150"/>
                                                    </div>
                                                    <div style="display: none;" itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <meta itemprop="name" content="LMS"/>
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content=""/>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;" itemprop="aggregateRating" itemscope
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="bestRating" content="5"/>
                                                        <meta itemprop="worstRating" content="1"/>
                                                        <meta itemprop="ratingValue" content="4.18"/>
                                                        <meta itemprop="ratingCount" content="293"/>
                                                    </div>
                                                    </span><span id="post-ratings-5992-loading"
                                                                 class="post-ratings-loading">
            <img src="{{asset("$public/lms/gif/loading.gif")}}" width="16" height="16" class="post-ratings-image"/>Loading...</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <li class="column dt-sc-one-half">
                                            <article id="post-5991"
                                                     class="dt-sc-custom-course-type  post-5991 dt_courses type-dt_courses status-publish has-post-thumbnail hentry course_category-law">
                                                <div class="dt-sc-course-thumb">
                                                    <a href="index-120.html"><img
                                                                src='{{asset("$public/lms/jpg/course15-573x403.jpg")}}"
                 )}}' width='573' height='403'/></a>
                                                    <div class="dt-sc-course-overlay">
                                                        <a title="Basic Laws and Policies" href="index-120.html"
                                                           class="dt-sc-button small white">View Course</a>
                                                    </div>
                                                </div>
                                                <div class="dt-sc-course-details"><span class="dt-sc-course-price"><span
                                                                class="amount">$40</span></span><h5><a
                                                                href="index-120.html"
                                                                title="Basic Laws and Policies">Basic
                                                            Laws and Policies</a></h5>
                                                    <div class="dt-sc-course-meta">
                                                        <p><a href="index-121.html" rel="tag">Law</a></p>
                                                        <p>5&nbsp;Lessons</p>
                                                    </div>
                                                    <div class="dt-sc-course-data">
                                                        <div class="dt-sc-course-duration">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span>05 : 10</span>
                                                        </div>
                                                        <span id="post-ratings-5991" class="post-ratings" itemscope
                                                              itemtype="http://schema.org/Article"
                                                              data-nonce="e4d24aae77"><img id="rating_5991_1"
                                                                                           src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                                           alt="1 Star" title="1 Star"
                                                                                           onmouseover="current_rating(5991, 1, '1 Star');"
                                                                                           onmouseout="ratings_off(4.2, 0, 0);"
                                                                                           onclick="rate_post();"
                                                                                           onkeypress="rate_post();"
                                                                                           style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5991_2"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="2 Stars" title="2 Stars"
                                                                    onmouseover="current_rating(5991, 2, '2 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5991_3"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="3 Stars" title="3 Stars"
                                                                    onmouseover="current_rating(5991, 3, '3 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5991_4"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="4 Stars" title="4 Stars"
                                                                    onmouseover="current_rating(5991, 4, '4 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5991_5"
                                                                    src="{{asset("$public/lms/gif/rating_off.gif")}}"
                                                                    alt="5 Stars" title="5 Stars"
                                                                    onmouseover="current_rating(5991, 5, '5 Stars');"
                                                                    onmouseout="ratings_off(4.2, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/> (<strong>124</strong> votes, average: <strong>4.15</strong> out of 5)<span
                                                                    class="post-ratings-text"
                                                                    id="ratings_5991_text"></span><meta
                                                                    itemprop="headline"
                                                                    content="Basic Laws and Policies"/><meta
                                                                    itemprop="description"
                                                                    content="Pellentesque venenatis orci vel enim malesuada, eget dignissim turpis egestas. Donec a ligula ac tellus ultrices lacinia. Nullam non nisi varius, posuere leo non, scelerisque orci."/><meta
                                                                    itemprop="datePublished"
                                                                    content="2014-08-11T04:16:14+00:00"/><meta
                                                                    itemprop="dateModified"
                                                                    content="2015-12-22T05:53:16+00:00"/><meta
                                                                    itemprop="url" content="index-120.html"/><meta
                                                                    itemprop="author" content="ram"/><meta
                                                                    itemprop="mainEntityOfPage"
                                                                    content="index-120.html"/><div
                                                                    style="display: none;" itemprop="image" itemscope
                                                                    itemtype="https://schema.org/ImageObject"><meta
                                                                        itemprop="url"
                                                                        content="{{asset("$public/lms/jpg/course15-150x150.jpg")}}"/><meta
                                                                        itemprop="width" content="150"/><meta
                                                                        itemprop="height"
                                                                        content="150"/>
                                                    </div>
                                                    <div style="display: none;" itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <meta itemprop="name" content="LMS"/>
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content=""/>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;" itemprop="aggregateRating" itemscope
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="bestRating" content="5"/>
                                                        <meta itemprop="worstRating" content="1"/>
                                                        <meta itemprop="ratingValue" content="4.15"/>
                                                        <meta itemprop="ratingCount" content="124"/>
                                                    </div>
                                                    </span><span id="post-ratings-5991-loading"
                                                                 class="post-ratings-loading">
            <img src="{{asset("$public/lms/gif/loading.gif")}}" width="16" height="16" class="post-ratings-image"/>Loading...</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <li class="column dt-sc-one-half">
                                            <article id="post-5990"
                                                     class="dt-sc-custom-course-type  post-5990 dt_courses type-dt_courses status-publish has-post-thumbnail hentry course_category-health">
                                                <div class="dt-sc-course-thumb">
                                                    <a href="index-122.html"><img
                                                                src='{{asset("$public/lms/jpg/course16-573x403.jpg")}}"
                 )}}' width='573' height='403'/></a>
                                                    <div class="dt-sc-course-overlay">
                                                        <a title="Healthcare Delivery" href="index-122.html"
                                                           class="dt-sc-button small white">View Course</a>
                                                    </div>
                                                </div>
                                                <div class="dt-sc-course-details"><span class="dt-sc-course-price"><span
                                                                class="amount">$50</span></span><h5><a
                                                                href="index-122.html"
                                                                title="Healthcare Delivery">Healthcare
                                                            Delivery</a></h5>
                                                    <div class="dt-sc-course-meta">
                                                        <p><a href="index-123.html" rel="tag">Health</a></p>
                                                        <p>4&nbsp;Lessons</p>
                                                    </div>
                                                    <div class="dt-sc-course-data">
                                                        <div class="dt-sc-course-duration">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span>03 : 25</span>
                                                        </div>
                                                        <span id="post-ratings-5990" class="post-ratings" itemscope
                                                              itemtype="http://schema.org/Article"
                                                              data-nonce="4f863229e2"><img id="rating_5990_1"
                                                                                           src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                                           alt="1 Star" title="1 Star"
                                                                                           onmouseover="current_rating(5990, 1, '1 Star');"
                                                                                           onmouseout="ratings_off(4.1, 0, 0);"
                                                                                           onclick="rate_post();"
                                                                                           onkeypress="rate_post();"
                                                                                           style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5990_2"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="2 Stars" title="2 Stars"
                                                                    onmouseover="current_rating(5990, 2, '2 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5990_3"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="3 Stars" title="3 Stars"
                                                                    onmouseover="current_rating(5990, 3, '3 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5990_4"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="4 Stars" title="4 Stars"
                                                                    onmouseover="current_rating(5990, 4, '4 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5990_5"
                                                                    src="{{asset("$public/lms/gif/rating_off.gif")}}"
                                                                    alt="5 Stars" title="5 Stars"
                                                                    onmouseover="current_rating(5990, 5, '5 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/> (<strong>69</strong> votes, average: <strong>4.06</strong> out of 5)<span
                                                                    class="post-ratings-text"
                                                                    id="ratings_5990_text"></span><meta
                                                                    itemprop="headline" content="Healthcare Delivery"/><meta
                                                                    itemprop="description"
                                                                    content="In bibendum eu justo vitae ornare. In et tellus ut magna vehicula ultricies. Duis blandit adipiscing dolor, ut tincidunt augue tincidunt eu. Duis non auctor enim."/><meta
                                                                    itemprop="datePublished"
                                                                    content="2014-08-11T04:13:36+00:00"/><meta
                                                                    itemprop="dateModified"
                                                                    content="2015-12-22T05:54:55+00:00"/><meta
                                                                    itemprop="url" content="index-122.html"/><meta
                                                                    itemprop="author" content="ram"/><meta
                                                                    itemprop="mainEntityOfPage"
                                                                    content="index-122.html"/><div
                                                                    style="display: none;" itemprop="image" itemscope
                                                                    itemtype="https://schema.org/ImageObject"><meta
                                                                        itemprop="url"
                                                                        content="{{asset("$public/lms/jpg/course16-150x150.jpg")}}"/><meta
                                                                        itemprop="width" content="150"/><meta
                                                                        itemprop="height"
                                                                        content="150"/>
                                                    </div>
                                                    <div style="display: none;" itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <meta itemprop="name" content="LMS"/>
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content=""/>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;" itemprop="aggregateRating" itemscope
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="bestRating" content="5"/>
                                                        <meta itemprop="worstRating" content="1"/>
                                                        <meta itemprop="ratingValue" content="4.06"/>
                                                        <meta itemprop="ratingCount" content="69"/>
                                                    </div>
                                                    </span><span id="post-ratings-5990-loading"
                                                                 class="post-ratings-loading">
            <img src="{{asset("$public/lms/gif/loading.gif")}}" width="16" height="16" class="post-ratings-image"/>Loading...</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <li class="column dt-sc-one-half">
                                            <article id="post-5989"
                                                     class="dt-sc-custom-course-type  post-5989 dt_courses type-dt_courses status-publish has-post-thumbnail hentry course_category-law">
                                                <div class="dt-sc-course-thumb">
                                                    <a href="index-124.html"><img
                                                                src='{{asset("$public/lms/jpg/course17-573x403.jpg")}}"
                 )}}' width='573' height='403'/></a>
                                                    <div class="dt-sc-course-overlay">
                                                        <a title="General Business Law" href="index-124.html"
                                                           class="dt-sc-button small white">View Course</a>
                                                    </div>
                                                </div>
                                                <div class="dt-sc-course-details"><span class="dt-sc-course-price"><span
                                                                class="amount">Free</span></span><h5><a
                                                                href="index-124.html" title="General Business Law">General
                                                            Business Law</a></h5>
                                                    <div class="dt-sc-course-meta">
                                                        <p><a href="index-121.html" rel="tag">Law</a></p>
                                                        <p>7&nbsp;Lessons</p>
                                                    </div>
                                                    <div class="dt-sc-course-data">
                                                        <div class="dt-sc-course-duration">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span>06 : 05</span>
                                                        </div>
                                                        <span id="post-ratings-5989" class="post-ratings" itemscope
                                                              itemtype="http://schema.org/Article"
                                                              data-nonce="fbe73c0935"><img id="rating_5989_1"
                                                                                           src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                                           alt="1 Star" title="1 Star"
                                                                                           onmouseover="current_rating(5989, 1, '1 Star');"
                                                                                           onmouseout="ratings_off(4.1, 0, 0);"
                                                                                           onclick="rate_post();"
                                                                                           onkeypress="rate_post();"
                                                                                           style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5989_2"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="2 Stars" title="2 Stars"
                                                                    onmouseover="current_rating(5989, 2, '2 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5989_3"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="3 Stars" title="3 Stars"
                                                                    onmouseover="current_rating(5989, 3, '3 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5989_4"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="4 Stars" title="4 Stars"
                                                                    onmouseover="current_rating(5989, 4, '4 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5989_5"
                                                                    src="{{asset("$public/lms/gif/rating_off.gif")}}"
                                                                    alt="5 Stars" title="5 Stars"
                                                                    onmouseover="current_rating(5989, 5, '5 Stars');"
                                                                    onmouseout="ratings_off(4.1, 0, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/> (<strong>89</strong> votes, average: <strong>4.08</strong> out of 5)<span
                                                                    class="post-ratings-text"
                                                                    id="ratings_5989_text"></span><meta
                                                                    itemprop="headline" content="General Business Law"/><meta
                                                                    itemprop="description"
                                                                    content="Fusce commodo in neque vitae mollis. Sed dolor lorem, cursus et venenatis sed, sodales eu nisi. Nunc lacinia massa neque, eu tincidunt mi blandit eget."/><meta
                                                                    itemprop="datePublished"
                                                                    content="2014-08-11T04:11:30+00:00"/><meta
                                                                    itemprop="dateModified"
                                                                    content="2014-08-11T11:43:39+00:00"/><meta
                                                                    itemprop="url" content="index-124.html"/><meta
                                                                    itemprop="author" content="ram"/><meta
                                                                    itemprop="mainEntityOfPage"
                                                                    content="index-124.html"/><div
                                                                    style="display: none;" itemprop="image" itemscope
                                                                    itemtype="https://schema.org/ImageObject"><meta
                                                                        itemprop="url"
                                                                        content="{{asset("$public/lms/jpg/course17-150x150.jpg")}}"/><meta
                                                                        itemprop="width" content="150"/><meta
                                                                        itemprop="height"
                                                                        content="150"/>
                                                    </div>
                                                    <div style="display: none;" itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <meta itemprop="name" content="LMS"/>
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content=""/>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;" itemprop="aggregateRating" itemscope
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="bestRating" content="5"/>
                                                        <meta itemprop="worstRating" content="1"/>
                                                        <meta itemprop="ratingValue" content="4.08"/>
                                                        <meta itemprop="ratingCount" content="89"/>
                                                    </div>
                                                    </span><span id="post-ratings-5989-loading"
                                                                 class="post-ratings-loading">
            <img src="{{asset("$public/lms/gif/loading.gif")}}" width="16" height="16" class="post-ratings-image"/>Loading...</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                        <li class="column dt-sc-one-half">
                                            <article id="post-5988"
                                                     class="dt-sc-custom-course-type  post-5988 dt_courses type-dt_courses status-publish has-post-thumbnail hentry course_category-engineering">
                                                <div class="dt-sc-course-thumb">
                                                    <a href="index-125.html"><img
                                                                src='{{asset("$public/lms/jpg/course18-573x403.jpg")}}"
                 )}}' width='573' height='403'/></a>
                                                    <div class="dt-sc-course-overlay">
                                                        <a title="Emerging Trends and Technologies"
                                                           href="index-125.html"
                                                           class="dt-sc-button small white">View Course</a>
                                                    </div>
                                                </div>
                                                <div class="dt-sc-course-details"><span class="dt-sc-course-price"><span
                                                                class="amount">Free</span></span><h5><a
                                                                href="index-125.html"
                                                                title="Emerging Trends and Technologies">Emerging Trends
                                                            and
                                                            Technologies</a></h5>
                                                    <div class="dt-sc-course-meta">
                                                        <p><a href="index-126.html" rel="tag">Engineering</a></p>
                                                        <p>4&nbsp;Lessons</p>
                                                    </div>
                                                    <div class="dt-sc-course-data">
                                                        <div class="dt-sc-course-duration">
                                                            <i class="fa fa-clock-o"> </i>
                                                            <span>03 : 10</span>
                                                        </div>
                                                        <span id="post-ratings-5988" class="post-ratings" itemscope
                                                              itemtype="http://schema.org/Article"
                                                              data-nonce="548835507f"><img id="rating_5988_1"
                                                                                           src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                                           alt="1 Star" title="1 Star"
                                                                                           onmouseover="current_rating(5988, 1, '1 Star');"
                                                                                           onmouseout="ratings_off(4.3, 5, 0);"
                                                                                           onclick="rate_post();"
                                                                                           onkeypress="rate_post();"
                                                                                           style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5988_2"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="2 Stars" title="2 Stars"
                                                                    onmouseover="current_rating(5988, 2, '2 Stars');"
                                                                    onmouseout="ratings_off(4.3, 5, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5988_3"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="3 Stars" title="3 Stars"
                                                                    onmouseover="current_rating(5988, 3, '3 Stars');"
                                                                    onmouseout="ratings_off(4.3, 5, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5988_4"
                                                                    src="{{asset("$public/lms/gif/rating_on.gif")}}"
                                                                    alt="4 Stars" title="4 Stars"
                                                                    onmouseover="current_rating(5988, 4, '4 Stars');"
                                                                    onmouseout="ratings_off(4.3, 5, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/><img
                                                                    id="rating_5988_5"
                                                                    src="{{asset("$public/lms/gif/rating_half.gif")}}"
                                                                    alt="5 Stars" title="5 Stars"
                                                                    onmouseover="current_rating(5988, 5, '5 Stars');"
                                                                    onmouseout="ratings_off(4.3, 5, 0);"
                                                                    onclick="rate_post();" onkeypress="rate_post();"
                                                                    style="cursor: pointer; border: 0px;"/> (<strong>64</strong> votes, average: <strong>4.25</strong> out of 5)<span
                                                                    class="post-ratings-text"
                                                                    id="ratings_5988_text"></span><meta
                                                                    itemprop="headline"
                                                                    content="Emerging Trends and Technologies"/><meta
                                                                    itemprop="description"
                                                                    content="Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut cursus magna nisi, vel pharetra ligula cursus eu. Sed ultricies condimentum velit et pharetra."/><meta
                                                                    itemprop="datePublished"
                                                                    content="2014-08-11T04:07:30+00:00"/><meta
                                                                    itemprop="dateModified"
                                                                    content="2014-08-11T11:44:35+00:00"/><meta
                                                                    itemprop="url" content="index-125.html"/><meta
                                                                    itemprop="author" content="ram"/><meta
                                                                    itemprop="mainEntityOfPage"
                                                                    content="index-125.html"/><div
                                                                    style="display: none;" itemprop="image" itemscope
                                                                    itemtype="https://schema.org/ImageObject"><meta
                                                                        itemprop="url"
                                                                        content="{{asset("$public/lms/jpg/course18-150x150.jpg")}}"/><meta
                                                                        itemprop="width" content="150"/><meta
                                                                        itemprop="height"
                                                                        content="150"/>
                                                    </div>
                                                    <div style="display: none;" itemprop="publisher" itemscope
                                                         itemtype="https://schema.org/Organization">
                                                        <meta itemprop="name" content="LMS"/>
                                                        <div itemprop="logo" itemscope
                                                             itemtype="https://schema.org/ImageObject">
                                                            <meta itemprop="url" content=""/>
                                                        </div>
                                                    </div>
                                                    <div style="display: none;" itemprop="aggregateRating" itemscope
                                                         itemtype="http://schema.org/AggregateRating">
                                                        <meta itemprop="bestRating" content="5"/>
                                                        <meta itemprop="worstRating" content="1"/>
                                                        <meta itemprop="ratingValue" content="4.25"/>
                                                        <meta itemprop="ratingCount" content="64"/>
                                                    </div>
                                                    </span><span id="post-ratings-5988-loading"
                                                                 class="post-ratings-loading">
            <img src="{{asset("$public/lms/gif/loading.gif")}}" width="16" height="16" class="post-ratings-image"/>Loading...</span>
                                                    </div>
                                                </div>
                                            </article>
                                        </li>
                                    </ul>
                                    <div class="carousel-arrows"><a class="course-prev" href="#"></a><a
                                                class="course-next" href="#"></a></div>
                                </div>
                            </div>
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <div class='dt-sc-hr-invisible-small  '></div>
                            <div class='column dt-sc-one-third  space  first'><p><img class="size-full wp-image-4762"
                                                                                      src="{{asset("$public/lms/jpg/lms-banner1.jpg")}}"
                                                                                      alt="ad1" width="420"
                                                                                      height="210"/></p></div>
                            <div class='column dt-sc-one-third  space  '><p><img
                                            class="size-full wp-image-4763"
                                            src="{{asset("$public/lms/jpg/lms-banner2.jpg")}}" alt="ad2"
                                            width="420" height="210"/></p></div>
                            <div class='column dt-sc-one-third  space  '><p><img
                                            class="size-full wp-image-4764"
                                            src="{{asset("$public/lms/jpg/lms-banner3.jpg")}}" alt="ad3"
                                            width="420" height="210"/></p></div>
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <h2 class='border-title aligncenter '>Blog<span></span></h2>
                            <div class='column dt-sc-one-third first'>
                                <article
                                        class="blog-entry no-border post-486 post type-post status-publish format-image has-post-thumbnail hentry category-post-format-audio category-photoshop tag-creative tag-design tag-news-2 post_format-post-format-image">
                                    <div class="blog-entry-inner">
                                        <div class="entry-thumb"><a href="index-127.html"><img
                                                        width="420" height="295"
                                                        src="{{asset("$public/lms/jpg/blog15-420x295.jpg")}}"
                                                        class="attachment-blogcourse-three-column-single-sidebar size-blogcourse-three-column-single-sidebar wp-post-image"
                                                        alt=""
                                                        srcset="https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-420x295.jpg 420w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-300x211.jpg 300w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-1024x719.jpg 1024w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-100x70.jpg 100w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15.jpg 1170w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-880x618.jpg 880w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-590x415.jpg 590w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-573x403.jpg 573w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-429x302.jpg 429w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog15-431x303.jpg 431w"
                                                        sizes="(max-width: 420px) 100vw, 420px"/></a>
                                            <div class="entry-thumb-desc">
                                                <p>
                                                <p>There are many variations of passages of Lorem
                                                    Ipsum...</p>
                                                </p>
                                            </div>
                                        </div> <!-- .entry-thumb -->
                                        <div class="entry-details">
                                            <div class="featured-post"><span
                                                        class="fa fa-trophy"> </span> <span
                                                        class="text">Featured</span></div>
                                            <div class="entry-meta">
                                                <div class="date">28 Jan</div>
                                                <a href='index-127.html' class='entry_format'></a>
                                            </div>
                                            <div class='entry-title'><h4><a href='index-127.html'>Image
                                                        - Minus id quod</a></h4></div>
                                            <div class="entry-metadata"><p class='author'><i
                                                            class='fa fa-user'> </i> <a
                                                            href='index-128.html'>ram</a></p>
                                                <span> | </span>
                                                <p class="tags"><i class="fa fa-tags"> </i><a
                                                            href="index-129.html"> creative</a>,<a
                                                            href="index-130.html"> design</a>,<a
                                                            href="index-131.html"> news</a></p>
                                                <span> | </span>
                                                <p class='comments'><a href='index-127.html#respond'
                                                                       class='comments'><span
                                                                class='fa fa-comments'> </span>
                                                        0</a>
                                                </p></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class='column dt-sc-one-third'>
                                <article
                                        class="blog-entry no-border post-484 post type-post status-publish format-gallery has-post-thumbnail hentry category-post-format-audio category-news category-photoshop tag-blog-2 tag-chat post_format-post-format-gallery">
                                    <div class="blog-entry-inner">
                                        <div class="entry-thumb">
                                            <ul class='entry-gallery-post-slider'>
                                                <li><img src='#' width='' height=''/></li>
                                                <li><img src='#' width='' height=''/></li>
                                                <li><img src='#' width='' height=''/></li>
                                                <li><img src='#' width='' height=''/></li>
                                            </ul>
                                            <div class="entry-thumb-desc"><p>
                                                <p>There are many variations of passages of Lorem
                                                    Ipsum...</p></p></div>
                                        </div> <!-- .entry-thumb -->
                                        <div class="entry-details">
                                            <div class="entry-meta">
                                                <div class="date">28 Jan</div>
                                                <a href='index-132.html' class='entry_format'></a>
                                            </div>
                                            <div class='entry-title'><h4><a href='index-132.html'>Gallery
                                                        - Cumque nihil impedit</a></h4></div>
                                            <div class="entry-metadata"><p class='author'><i
                                                            class='fa fa-user'> </i> <a
                                                            href='index-128.html'>ram</a></p>
                                                <span> | </span>
                                                <p class="tags"><i class="fa fa-tags"> </i><a
                                                            href="index-133.html"> blog</a>,<a
                                                            href="index-134.html"> chat</a></p>
                                                <span> | </span>
                                                <p class='comments'><a href='index-132.html#respond'
                                                                       class='comments'><span
                                                                class='fa fa-comments'> </span>
                                                        0</a>
                                                </p></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                            <div class='column dt-sc-one-third'>
                                <article
                                        class="blog-entry no-border post-482 post type-post status-publish format-status has-post-thumbnail hentry category-magazine category-photography category-photoshop tag-blog-2 tag-chat post_format-post-format-status">
                                    <div class="blog-entry-inner">
                                        <div class="entry-thumb"><a href="index-135.html"><img
                                                        width="420" height="295"
                                                        src="{{asset("$public/lms/jpg/blog17-420x295.jpg")}}"
                                                        class="attachment-blogcourse-three-column-single-sidebar size-blogcourse-three-column-single-sidebar wp-post-image"
                                                        alt=""
                                                        srcset="https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-420x295.jpg 420w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-300x211.jpg 300w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-1024x719.jpg 1024w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-100x70.jpg 100w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17.jpg 1170w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-880x618.jpg 880w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-590x415.jpg 590w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-573x403.jpg 573w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-429x302.jpg 429w, https://lmstheme.wpengine.com/wp-content/uploads/2014/08/blog17-431x303.jpg 431w"
                                                        sizes="(max-width: 420px) 100vw, 420px"/></a>
                                            <div class="entry-thumb-desc"><p>
                                                <p>There are many variations of passages of Lorem
                                                    Ipsum...</p></p></div>
                                        </div> <!-- .entry-thumb -->
                                        <div class="entry-details">
                                            <div class="entry-meta">
                                                <div class="date">28 Jan</div>
                                                <a href='index-135.html' class='entry_format'></a>
                                            </div>
                                            <div class='entry-title'><h4><a href='index-135.html'>Latin
                                                        words Ipsum</a></h4></div>
                                            <div class="entry-metadata"><p class='author'><i
                                                            class='fa fa-user'> </i> <a
                                                            href='index-128.html'>ram</a></p>
                                                <span> | </span>
                                                <p class="tags"><i class="fa fa-tags"> </i><a
                                                            href="index-133.html"> blog</a>,<a
                                                            href="index-134.html"> chat</a></p>
                                                <span> | </span>
                                                <p class='comments'><a href='index-135.html#respond'
                                                                       class='comments'><span
                                                                class='fa fa-comments'> </span>
                                                        0</a>
                                                </p></div>
                                        </div>
                                    </div>
                                </article>
                            </div>
                        </div>
                    </div>
                    <div class='dt-sc-hr-invisible-medium  '></div>
                    <div class='fullwidth-section custom-parallax dt-sc-parallax-section'
                         style="background-image:url({{asset("$public/lms/jpg/parallax-light-white.jpg")}});background-position:left top;background-attachment:fixed; ">
                        <div class="container">
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <div class='column dt-sc-one-sixth  space  first'>
                                <div class='dt-sc-ico-content type13'>
                                    <div class='custom-icon' style="background-color:#f2672e;"><a href='#'
                                                                                                  target='_blank'><span
                                                    class='fa fa-html5'></span></a></div>
                                    <h4><a href='#' target='_blank'> HTML </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-sixth  space  '>
                                <div class='dt-sc-ico-content type13'>
                                    <div class='custom-icon' style="background-color:#0171bb;"><a href='#'
                                                                                                  target='_blank'><span
                                                    class='fa fa-css3'></span></a></div>
                                    <h4><a href='#' target='_blank'> CSS </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-sixth  space  '>
                                <div class='dt-sc-ico-content type13'>
                                    <div class='custom-icon' style="background-color:#9fbf3a;"><a href='#'
                                                                                                  target='_blank'><span
                                                    class='fa fa-android'></span></a></div>
                                    <h4><a href='#' target='_blank'> Android </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-sixth  space  '>
                                <div class='dt-sc-ico-content type13'>
                                    <div class="custom-icon" style="background-color:#2b8bc0;"><span><a href="#"
                                                                                                        target="_blank"><img
                                                        src="{{asset("$public/lms/png/icon-ps.png")}}" title="Photoshop"
                                                        alt="Photoshop"></a></span></div>
                                    <h4><a href='#' target='_blank'> Photoshop </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-sixth  space  '>
                                <div class='dt-sc-ico-content type13'>
                                    <div class="custom-icon" style="background-color:#e5a329;"><span><a href="#"
                                                                                                        target="_blank"><img
                                                        src="{{asset("$public/lms/png/icon-js.png")}}" title="jQuery"
                                                        alt="jQuery"></a></span></div>
                                    <h4><a href='#' target='_blank'> jQuery </a></h4></div>
                            </div>
                            <div class='column dt-sc-one-sixth  space  '>
                                <div class='dt-sc-ico-content type13'>
                                    <div class="custom-icon" style="background-color:#d10101;"><span><a href="#"
                                                                                                        target="_blank"><img
                                                        src="{{asset("$public/lms/jpg/icon-ruby.jpg")}}"
                                                        title="Ruby" alt="Ruby"></a></span></div>
                                    <h4><a href='#' target='_blank'> Ruby </a></h4></div>
                            </div>
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <div class='dt-sc-hr-invisible  '></div>
                        </div>
                    </div>
                    <div class='dt-sc-hr-invisible-medium  '></div>
                    <div class='fullwidth-section  '>
                        <div class="container"><h2 class='border-title  '>Upcoming Events<span></span></h2>
                            <div class="dt-sc-events-carousel-wrapper">
                                <ul class="dt-sc-events-carousel">
                                    <li class="dt-sc-one-half column " id="post-3636">
                                        <div class="dt-sc-event-container">
                                            <div class="dt-sc-event-thumb"><a href="index-136.html"
                                                                              title="Welcoming 25th Batch"><img
                                                            width="420" height="295"
                                                            src="{{asset("$public/lms/jpg/course11-420x295.jpg")}}"
                                                            class="attachment-blogcourse-three-column size-blogcourse-three-column wp-post-image"
                                                            alt="" title="Welcoming 25th Batch"
                                                            srcset="https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-420x295.jpg 420w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-300x211.jpg 300w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-1024x719.jpg 1024w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-100x70.jpg 100w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11.jpg 1170w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-880x618.jpg 880w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-590x415.jpg 590w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-573x403.jpg 573w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-429x302.jpg 429w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course11-431x303.jpg 431w"
                                                            sizes="(max-width: 420px) 100vw, 420px"/></a><span
                                                        class="event-price">$60</span></div>
                                            <div class="dt-sc-event-content"><h2><a href="index-136.html">Welcoming
                                                        25th Batch</a></h2>
                                                <div class="dt-sc-event-meta"><p><i
                                                                class="fa fa-calendar-o"> </i>Jul
                                                        2015 @ 08 am - Mar 2025 @ 10 pm </p>
                                                    <p><i class="fa fa-map-marker"> </i><a
                                                                href="index-137.html">Peppard
                                                            Hill</a>, United States<a
                                                                href="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=AL+United+States"
                                                                title="Peppard Hill" target="_blank"> + Google
                                                            Map </a></p></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dt-sc-one-half column " id="post-3639">
                                        <div class="dt-sc-event-container">
                                            <div class="dt-sc-event-thumb"><a href="index-138.html"
                                                                              title="Fast Track Course Opening"><img
                                                            width="420" height="295"
                                                            src="{{asset("$public/lms/jpg/course9-420x295.jpg")}}"
                                                            class="attachment-blogcourse-three-column size-blogcourse-three-column wp-post-image"
                                                            alt="" title="Fast Track Course Opening"
                                                            srcset="https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-420x295.jpg 420w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-300x211.jpg 300w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-1024x719.jpg 1024w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-100x70.jpg 100w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9.jpg 1170w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-880x618.jpg 880w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-590x415.jpg 590w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-573x403.jpg 573w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-429x302.jpg 429w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course9-431x303.jpg 431w"
                                                            sizes="(max-width: 420px) 100vw, 420px"/></a><span
                                                        class="event-price">$20</span></div>
                                            <div class="dt-sc-event-content"><h2><a href="index-138.html">Fast
                                                        Track
                                                        Course Opening</a></h2>
                                                <div class="dt-sc-event-meta"><p><i
                                                                class="fa fa-calendar-o"> </i>Aug
                                                        2015 @ 09 am - Aug 2024 @ 06 pm </p>
                                                    <p><i class="fa fa-map-marker"> </i><a
                                                                href="index-139.html">Rippon
                                                            Building</a>, United Kingdom<a
                                                                href="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Ostrey+Lne+London+United+Kingdom"
                                                                title="Rippon Building" target="_blank"> +
                                                            Google
                                                            Map </a></p></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="dt-sc-one-half column " id="post-3633">
                                        <div class="dt-sc-event-container">
                                            <div class="dt-sc-event-thumb"><a href="index-140.html"
                                                                              title="Free Yoga Class at London"><img
                                                            width="420" height="295"
                                                            src="{{asset("$public/lms/jpg/course5-420x295.jpg")}}"
                                                            class="attachment-blogcourse-three-column size-blogcourse-three-column wp-post-image"
                                                            alt="" title="Free Yoga Class at London"
                                                            srcset="https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-420x295.jpg 420w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-300x211.jpg 300w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-1024x719.jpg 1024w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-100x70.jpg 100w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5.jpg 1170w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-880x618.jpg 880w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-590x415.jpg 590w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-573x403.jpg 573w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-429x302.jpg 429w, https://lmstheme.wpengine.com/wp-content/uploads/2014/05/course5-431x303.jpg 431w"
                                                            sizes="(max-width: 420px) 100vw, 420px"/></a></div>
                                            <div class="dt-sc-event-content"><h2><a href="index-140.html">Free
                                                        Yoga
                                                        Class at London</a></h2>
                                                <div class="dt-sc-event-meta"><p><i
                                                                class="fa fa-calendar-o"> </i>Sep
                                                        2019 @ 10 am - Oct 2024 @ 06 pm </p>
                                                    <p><i class="fa fa-map-marker"> </i><a
                                                                href="index-141.html">Pound
                                                            Lane</a>, United States<a
                                                                href="https://maps.google.com/maps?f=q&amp;source=s_q&amp;hl=en&amp;geocode=&amp;q=Thov+Bridge+Las+Angels+United+States"
                                                                title="Pound Lane" target="_blank"> + Google
                                                            Map </a></p></div>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                                <div class="carousel-arrows"><a class="events-prev" href="#"></a><a
                                            class="events-next" href="#"></a></div>
                            </div>
                        </div>
                    </div>
                    <div class='dt-sc-hr-invisible-medium  '></div>
                    <div class='fullwidth-section skin-color dt-sc-parallax-section'
                         style="background-image:url({{asset("$public/lms/png/newsletter-parallax.png")}});background-position:left top;background-attachment:fixed; ">
                        <div class="container">
                            <div class='dt-sc-hr-invisible-medium  '></div>
                            <section id="newsletter">
                                <h2 class="border-title aligncenter">Get in touch with us</h2>
                                <h6> There are many variations of passages of Lorem Ipsum available, but the
                                    majority have suffered alteration in some form, by injected humour, or
                                    randomised words which Ipsum slightly believable </h6>
                                <form name="frmNewsletter" method="post" class="dt-sc-subscribe-frm">
                                    <input type="email" name="dt_mc_emailid" id="dt_mc_emailid" required=""
                                           placeholder="Your Email Address"/>
                                    <input type="hidden" name="dt_mc_apikey" id="dt_mc_apikey"
                                           value="c94a1d38b6683d601a324aeeb4d4ffe3-us1"/>
                                    <input type="hidden" name="dt_mc_listid" id="dt_mc_listid"
                                           value="314bbca712"/>
                                    <input type="submit" name="submit" class="dt-sc-button small"
                                           value="Subscribe"/>
                                </form>
                                <div id="ajax_newsletter_msg"></div>
                            </section>
                            <div class='dt-sc-hr-invisible-medium  '></div>
                        </div>
                    </div>
                    <div class='dt-sc-hr-invisible-medium  '></div>
                    <div class='fullwidth-section  '>
                        <div class="container"><h2 class='border-title aligncenter '>Our Process<span></span>
                            </h2>
                            <div class='dt-sc-hr-invisible  '></div>
                            <div class="dt-sc-timeline-section">
                                <div class="dt-sc-timeline left ">
                                    <div class="dt-sc-one-half column first">
                                        <div class="dt-sc-timeline-content">
                                            <h2><a href="#"> Step 01 </a></h2>
                                            <h4>Search for your course</h4>
                                            <p><i class="fa fa-timeline-icon1"> </i>Nemo enim ipsam voluptatem
                                                quia
                                                voluptas sit atur aut odit aut fugit, sed quia consequuntur
                                                magni
                                                res.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-timeline right ">
                                    <div class="dt-sc-one-half column first">
                                        <div class="dt-sc-timeline-content">
                                            <h2><a href="#"> Step 02 </a></h2>
                                            <h4>Take a Sample Lesson</h4>
                                            <p><i class="fa fa-timeline-icon2"> </i>Nemo enim ipsam voluptatem
                                                quia
                                                voluptas sit atur aut odit aut fugit, sed quia consequuntur
                                                magni
                                                res.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-timeline left ">
                                    <div class="dt-sc-one-half column first">
                                        <div class="dt-sc-timeline-content">
                                            <h2><a href="#"> Step 03 </a></h2>
                                            <h4>Preview the Syllabus</h4>
                                            <p><i class="fa fa-timeline-icon3"> </i>Nemo enim ipsam voluptatem
                                                quia
                                                voluptas sit atur aut odit aut fugit, sed quia consequuntur
                                                magni
                                                res.</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="dt-sc-timeline right ">
                                    <div class="dt-sc-one-half column first">
                                        <div class="dt-sc-timeline-content">
                                            <h2><a href="#"> Step 04 </a></h2>
                                            <h4>Purchase the Course</h4>
                                            <p><i class="fa fa-timeline-icon4"> </i>Nemo enim ipsam voluptatem
                                                quia
                                                voluptas sit atur aut odit aut fugit, sed quia consequuntur
                                                magni
                                                res.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class='dt-sc-hr-invisible-medium  '></div>
                    <div class="container">
                        <div class="social-bookmark"></div>
                        <div class="social-share"></div>
                    </div>
                </div><!-- #post-3633 -->

            </section><!-- ** Primary Section End ** -->
        </div><!-- **Main - End** -->


        <!-- **Footer** -->
        <footer id="footer">

            <div class="footer-logo">
                <img class="normal_logo" src="{{asset("$public/lms/png/footer-logo.png")}}" alt="Footer Logo"
                     title="Footer Logo">
                <img class="retina_logo" src="{{asset("$public/lms/png/footer-logo%402x.png")}}" alt="LMS" title="LMS"
                     style="width:98px; height:99px;"/>
            </div>
            <div class="footer-widgets-wrapper">
                <div class="container">
                    <div class='column dt-sc-one-fourth first'>
                        <aside id="text-12" class="widget widget_text"><h3 class="widgettitle">About us</h3>
                            <div class="textwidget"><p> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut et
                                    lobortis diam. </p>
                                <p> Duis tellus enim, vestibulum eget varius id, vulputate et mi. Nullam feugiat, diam
                                    quis
                                    interdum varius </p>
                                <p><a href="index-16.html" title="" class="dt-sc-button small"> Start Learning Now </a>
                                </p>
                            </div>
                        </aside>
                    </div>
                    <div class='column dt-sc-one-fourth '>
                        <aside id="my_course_widget-4" class="widget widget_popular_entries"><h3 class="widgettitle">
                                Popular
                                Courses</h3>
                            <div class='recent-course-widget'>
                                <ul>
                                    <li><h6><a href='index-38.html'>Power Electronics</a></h6><span
                                                class="dt-sc-course-price">
									<span class="amount">$50</span>
							</span></li>
                                    <li><h6><a href='index-39.html'>Processing Digi...</a></h6><span
                                                class="dt-sc-course-price">
									<span class="amount">Free</span>
							</span></li>
                                    <li><h6><a href='index-40.html'>Analysis of Alg...</a></h6><span
                                                class="dt-sc-course-price">
									<span class="amount">Free</span>
							</span></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class='column dt-sc-one-fourth '>
                        <aside id="text-7" class="widget widget_text"><h3 class="widgettitle">Quick Links</h3>
                            <div class="textwidget">
                                <ul>
                                    <li><a href="#" title=""> All Courses </a></li>
                                    <li><a href="#" title=""> Summer Sessions </a></li>
                                    <li><a href="#" title=""> Professional Courses </a></li>
                                    <li><a href="#" title=""> Privacy Policy </a></li>
                                    <li><a href="#" title=""> Terms of Use </a></li>
                                </ul>
                            </div>
                        </aside>
                    </div>
                    <div class='column dt-sc-one-fourth '>
                        <aside id="text-8" class="widget widget_text"><h3 class="widgettitle">Contact us</h3>
                            <div class="textwidget">
                                <div class="dt-sc-contact-info address">
                                    <div class='icon'><i class='fa fa-location-arrow'></i></div>
                                    <p>The Design Themes Inc.<br>Mary Jane St, Sydney 2233 <br> Australia.
                                    <p><span></span></div>

                                <div class="dt-sc-contact-info">
                                    <div class='icon'><i class='fa fa-phone'></i></div>
                                    <p>+11 (2) 7654 2233</p><span></span></div>

                                <div class="dt-sc-contact-info">
                                    <div class='icon'><i class='fa fa-fax'></i></div>
                                    <p>+11 (5) 7654 2244</p><span></span></div>

                                <div class="dt-sc-contact-info">
                                    <div class='icon'><i class='fa fa-envelope'></i></div>
                                    <p><a href='mailto:lms@gmail.com'>lms@gmail.com</a></p><span></span></div>
                            </div>
                        </aside>
                    </div>
                </div>
            </div>

            <div class="copyright">
                <div class="container">
                    <div class="copyright-info">Copyright &copy; 2018 LMS Theme All Rights Reserved | <a
                                href="http://themeforest.net/user/designthemes" title=""> Design Themes </a></div>
                    <ul class='social-icons'>
                        <li><a href="#" target="_blank"><span class="fa fa-twitter"></span></a></li>
                        <li><a href="#" target="_blank"><span class="fa fa-youtube"></span></a></li>
                        <li><a href="#" target="_blank"><span class="fa fa-facebook"></span></a></li>
                        <li><a href="#" target="_blank"><span class="fa fa-skype"></span></a></li>
                    </ul>
                </div>
            </div>
        </footer><!-- **Footer - End** -->
    </div><!-- **Inner Wrapper - End** -->
</div><!-- **Wrapper - End** -->
<script>
    (function (body) {
        'use strict';
        body.className = body.className.replace(/\btribe-no-js\b/, 'tribe-js');
    })(document.body);
</script>
<script type='text/javascript'> /* <![CDATA[ */
    var tribe_l10n_datatables = {
        "aria": {
            "sort_ascending": ": activate to sort column ascending",
            "sort_descending": ": activate to sort column descending"
        },
        "length_menu": "Show _MENU_ entries",
        "empty_table": "No data available in table",
        "info": "Showing _START_ to _END_ of _TOTAL_ entries",
        "info_empty": "Showing 0 to 0 of 0 entries",
        "info_filtered": "(filtered from _MAX_ total entries)",
        "zero_records": "No matching records found",
        "search": "Search:",
        "all_selected_text": "All items on this page were selected. ",
        "select_all_link": "Select all pages",
        "clear_selection": "Clear Selection.",
        "pagination": {"all": "All", "next": "Next", "previous": "Previous"},
        "select": {"rows": {"0": "", "_": ": Selected %d rows", "1": ": Selected 1 row"}},
        "datepicker": {
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "monthNamesShort": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "nextText": "Next",
            "prevText": "Prev",
            "currentText": "Today",
            "closeText": "Done"
        }
    };
    /* ]]> */ </script>
<script type='text/javascript' src='{{asset("$public/lms/js/core.mine899.js?ver=1.11.4")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/datepicker.mine899.js?ver=1.11.4")}}'></script>
<script type='text/javascript'>
    jQuery(document).ready(function (jQuery) {
        jQuery.datepicker.setDefaults({
            "closeText": "Close",
            "currentText": "Today",
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "monthNamesShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
            "nextText": "Next",
            "prevText": "Previous",
            "dayNames": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
            "dayNamesShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
            "dayNamesMin": ["S", "M", "T", "W", "T", "F", "S"],
            "dateFormat": "MM d, yy",
            "firstDay": 1,
            "isRTL": false
        });
    });
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery-ui-timepicker-addon5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jsscplugins5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/shortcodes5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.knob5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.knob.custom5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.print5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var object = {
        "quizTimeout": "Timeout!",
        "noResult": "No Results Found!",
        "noGraph": "No enough data to generate graph!",
        "onRefresh": "Refreshing this quiz page will mark this session as completed.",
        "registrationSuccess": "You have successfully registered with our class!",
        "locationAlert1": "To get GPS location please fill address.",
        "locationAlert2": "Please add latitude and longitude"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/dt.custom5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript'
        src='https://maps.googleapis.com/maps/api/js?key=AIzaSyCRWEQ6h1tdGDIv01IrJj6lss2ISkqbYq0&amp;ver=4.9.8")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wpcf7 = {
        "apiSettings": {
            "root": "https:\/\/lmstheme.wpengine.com\/wp-json\/contact-form-7\/v1",
            "namespace": "contact-form-7\/v1"
        }, "recaptcha": {"messages": {"empty": "Please verify that you are not a robot."}}, "cached": "1"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/scriptsaead.js?ver=5.0.3")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.blockui.min44fd.js?ver=2.70")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/js.cookie.min6b25.js?ver=2.1.4")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "https:\/\/lmstheme.wpengine.com\/?wc-ajax=%%endpoint%%"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/woocommerce.mind617.js?ver=3.3.2")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "https:\/\/lmstheme.wpengine.com\/?wc-ajax=%%endpoint%%",
        "cart_hash_key": "wc_cart_hash_3e24484ab59e53105ff01c77c9b9195c",
        "fragment_name": "wc_fragments_3e24484ab59e53105ff01c77c9b9195c"
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/cart-fragments.mind617.js?ver=3.3.2")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/comment-reply.min5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var ratingsL10n = {
        "plugin_url": "https:\/\/lmstheme.wpengine.com\/wp-content\/plugins\/wp-postratings",
        "ajax_url": "https:\/\/lmstheme.wpengine.com\/wp-admin\/admin-ajax.php",
        "text_wait": "Please rate only 1 item at a time.",
        "image": "stars",
        "image_ext": "gif",
        "max": "5",
        "show_loading": "1",
        "show_fading": "1",
        "custom": "0"
    };
    var ratings_mouseover_image = new Image();
    ratings_mouseover_image.src = "{{asset("$public/lms/gif/rating_over.gif")}}";
    ;
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/postratings-js1098.js?ver=1.85")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.prettyphoto.min005e.js?ver=3.1.6")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.selectbox.min7359.js?ver=1.2.0")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var yith_wcwl_l10n = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "redirect_to_cart": "no",
        "multi_wishlist": "",
        "hide_add_button": "1",
        "is_user_logged_in": "",
        "ajax_loader_url": "https:\/\/lmstheme.wpengine.com\/wp-content\/plugins\/yith-woocommerce-wishlist\/assets\/images\/ajax-loader.gif",
        "remove_from_wishlist_after_add_to_cart": "yes",
        "labels": {
            "cookie_disabled": "We are sorry, but this feature is available only if cookies are enabled on your browser.",
            "added_to_cart_message": "<div class=\"woocommerce-message\">Product correctly added to cart<\/div>"
        },
        "actions": {
            "add_to_wishlist_action": "add_to_wishlist",
            "remove_from_wishlist_action": "remove_from_wishlist",
            "move_to_another_wishlist_action": "move_to_another_wishlsit",
            "reload_wishlist_and_adding_elem_action": "reload_wishlist_and_adding_elem"
        }
    };
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.yith-wcwl605a.js?ver=2.2.2")}}'></script>
<!--[if lt IE 9]>
<script type="text/javascript"
        src='https://lmstheme.wpengine.com/wp-content/themes/lms/framework//js/public/html5shiv.min.js?ver=4.9.8")}}'></script>
<![endif]-->
<script type='text/javascript' src='{{asset("$public/lms/js/jsplugins5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/jquery.cookie5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/picker5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/ajax-courses5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/ajax-classes5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/dashboard5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript'>
    /* <![CDATA[ */
    var DtCustomObject = {"joinedGroup": "Joined Group Successfully"};
    /* ]]> */
</script>
<script type='text/javascript' src='{{asset("$public/lms/js/custom5010.js?ver=4.9.8")}}'></script>
<script type='text/javascript' data-cfasync="false"
        src='{{asset("$public/lms/js/s2member-o6cfa.js?ws_plugin__s2member_js_w_globals=1&amp;qcABC=1&amp;ver=170722-170722-1325029721")}}'></script>
<script type='text/javascript' src='{{asset("$public/lms/js/wp-embed.min5010.js?ver=4.9.8")}}'></script>
</body>

<!-- Mirrored from lmstheme.wpengine.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Wed, 07 Nov 2018 17:53:04 GMT -->
</html>