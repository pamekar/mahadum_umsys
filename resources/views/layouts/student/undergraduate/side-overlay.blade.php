<div class="bg-image" style="background-image: url('/{{$public}}/jpg/bg_side_overlay_header.jpg');">
    <div class="bg-success-op">
        <div class="content-header">
            <a class="img-link mr-1" href="{{url('')}}">
                <img class="img-avatar img-avatar48" src="{{asset("$public/".$data->user_meta->photo_location)}}"
                     alt="">
            </a>
            <div class="ml-2">
                <a class="text-white font-w600"
                   href="{{url('')}}">{{$data->user_meta->first_name.' '. $data->user_meta->last_name}}</a>
                <div class="text-white-75 font-size-sm font-italic">{{$data->user_meta->reg_no}}</div>
            </div>
            <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout"
               data-action="side_overlay_close">
                <i class="fa fa-times-circle"></i>
            </a>
        </div>
    </div>
</div>
<div class="content-side">
    <div class="block block-transparent pull-x pull-t">
        <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
            <li class="nav-item">
                <a class="nav-link" href="#so-people">
                    <i class="far fa-fw fa-user-circle"></i>
                </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#so-profile">
                    <i class="far fa-fw fa-edit"></i>
                </a>
            </li>
        </ul>
        <div class="block-content tab-content overflow-hidden">
            <div class="tab-pane pull-x show fade fade-right active" id="so-people" role="tabpanel">
                <div class="block mb-0">
                    <div class="block-content block-content-sm block-content-full bg-body">
                        <span class="text-uppercase font-size-sm font-w700">Your Details</span>
                    </div>
                    <div class="block-content">
                        <ul class="nav-items small">
                            <li class="block block-bordered text-primary">
                                <div class="block-content block-bordered">
                                    {!! DNS2D::getBarcodeHTML(url(
                       'v/c/student/' . $data->uid . '/' .Auth::user()->name ), "QRCODE", 7, 5) !!}
                                </div>
                                <div class="block-content">
                                    <dl class="dl-horizontal">
                                        <dt>Unique ID</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->uid}}</span>
                                        </dd>
                                        <dt>School</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->user_meta->faculty}}</span>
                                        </dd>
                                        <dt>Department</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->user_meta->dept or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Level</dt>
                                        <dd>
                                                                <span class="text-muted">{{$data->level or 'Not Set'}}
                                                                    Level</span>
                                        </dd>
                                        <dt>Mode of Entry</dt>
                                        <dd>
										<span class="text-muted">{{$data->user_meta->mode_of_entry or 'Not
											Set'}}</span>
                                        </dd>
                                        <dt>Award in View</dt>
                                        <dd>
										<span class="text-muted">{{$data->user_meta->award_in_view or 'Not
											Set'}}</span>
                                        </dd>
                                        <dt>Course Duration</dt>
                                        <dd>
										<span class="text-muted">{{$data->user_meta->course_duration . "
											Years"}}</span>
                                        </dd>
                                    </dl>
                                </div>
                            </li>
                            <li class="block block-bordered text-info">
                                <div class="block-content">
                                    <dl class="">
                                        <dt>Dean of School</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->deptStaff->dean or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Head of Department</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->deptStaff->hod or 'Not Set'}}</span>
                                        </dd>
                                        <dt>Course Adviser</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->deptStaff->ca or 'Not Set'}}</span>
                                        </dd>
                                        <dt>GPA (Cummulative)</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->gpa_commulative or 'Not Found'}}</span>
                                        </dd>
                                        <dt>GPA (Previous Session)</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->gpa_previous or 'Not Set'}}</span>
                                        </dd>

                                    </dl>
                                </div>
                            </li>
                            <li class="block block-bordered text-danger">
                                <div class="block-content">
                                    <dl class="">
                                        <dt>Outstanding Courses</dt>
                                        <dd>
                                            <span class="text-muted">{{$data->outstanding_courses}}</span>
                                        </dd>
                                        <dt>Outstanding Fees</dt>
                                        <dd>
                                            <span class="text-muted">&#8358; {{number_format($data->outstanding_fees)}}</span>
                                        </dd>
                                    </dl>
                                </div>
                            </li>

                        </ul>
                    </div>
                </div>
            </div>
            <div class="tab-pane pull-x fade fade-left" id="so-profile" role="tabpanel">
                <div class="block mb-0">
                    <div class="block-content block-content-sm block-content-full bg-body">
                        <span class="text-uppercase font-size-sm font-w700">Personal</span>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="form-group">
                            <label>Username</label>
                            <input type="text" readonly class="form-control" id="staticEmail"
                                   value="{{Auth::user()->name}}" readonly>
                        </div>
                        <div class="form-group">
                            <label for="so-profile-name">Name</label>
                            <input type="text" class="form-control" id="so-profile-name"
                                   name="so-profile-name"
                                   value="{{$data->user_meta->first_name." ".$data->user_meta->last_name}}"
                                   readonly>
                        </div>
                        <div class="form-group">
                            <label for="user-email">Email</label>
                            <input type="email" class="form-control" id="user-email"
                                   name="so-profile-email" value="{{Auth::user()->email}}" readonly>
                        </div>
                    </div>
                    <div class="block-content block-content-sm block-content-full bg-body">
                        <span class="text-uppercase font-size-sm font-w700">Password Update</span>
                    </div>
                    <form action="javascript:void(0)" method="post" id="password-form">
                        {{method_field('patch')}}
                        <div class="block-content block-content-full">
                            <div class="form-group">
                                <label for="so-profile-password">Current Password</label>
                                <input type="password" class="form-control password" id="so-profile-password"
                                       name="current_password">
                            </div>
                            <div class="form-group">
                                <label for="so-profile-new-password">New Password</label>
                                <input type="password" class="form-control password" id="new-password"
                                       name="new_password">
                            </div>
                            <div class="form-group">
                                <label for="password-confirmation">Confirm New Password</label>
                                <input type="password" class="form-control password"
                                       id="so-profile-new-password-confirm"
                                       name="new_password_confirmation">
                            </div>
                        </div>
                        <div class="block-content row justify-content-center border-top">
                            <div class="col-9">
                                <button type="submit" class="btn btn-block btn-hero-primary" onclick="changePassword()">
                                    <i class="fa fa-fw fa-save mr-1"></i> Update
                                </button>
                            </div>
                        </div>
                    </form>

                    <div class="block-content block-content-sm block-content-full bg-body">
                        <span class="text-uppercase font-size-sm font-w700">Guardian Preferences</span>
                    </div>

                    <div class="block-content block-content-full">
                        <form action="javascript:void(0)" method="post">
                            <label for="guardian-name">Guardian </label>
                            <div class="input-group">
                                <input type="text" class="form-control" id="guardian-name"
                                       name="guardian"
                                       value="{{$data->user_meta->guardian or null}}" readonly
                                       placeholder="Guardian's username">
                                <div class="input-group-append">
                                    <button type="submit" class="btn btn-primary" id="guardian-button"
                                            onclick="editGuardian()">
                                        <i class="fa fa-edit"></i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="block-content block-content-full">
                        <form action="javascript:void(0)" method="post" id="guardian-form">
                            @php
                                $guardian_access=explode(';',$data->user_meta->guardian_access);
                            @endphp
                            <div class="custom-control custom-checkbox mb-1">
                                <input class="custom-control-input" id="guardian-schedule" name="guardian_access[]"
                                       value="schedules" type="checkbox"
                                       @if(in_array('schedules',$guardian_access))checked=""@endif>
                                <label class="custom-control-label" for="guardian-schedule">Student's Schedule</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                If selected, your guardian will be able to view your lecture days, time, and venues.
                            </p>
                            <div class="custom-control custom-checkbox mb-1">
                                <input class="custom-control-input" id="guardian-results" name="guardian_access[]"
                                       value="results" type="checkbox"
                                       @if(in_array('results',$guardian_access))checked=""@endif>
                                <label class="custom-control-label" for="guardian-results">Student's Results</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                If selected, your guardian will be able to view your results for the current session.
                            </p>

                            <div class="custom-control custom-checkbox mb-1">
                                <input class="custom-control-input" id="guardian-statistics"
                                       name="guardian_access[]" value="statistics"
                                       @if(in_array('statistics',$guardian_access))checked="" @endif type="checkbox">
                                <label class="custom-control-label"
                                       for="guardian-statistics">Student's Statistics</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                If selected, your guardian will be able to view your performance statistics presented in
                                charts.
                            </p>
                            <div class="custom-control custom-checkbox mb-1">
                                <input class="custom-control-input" id="guardian-updates" name="guardian_access[]"
                                       value="fees" @if(in_array('fees',$guardian_access))checked=""
                                       @endif type="checkbox">
                                <label class="custom-control-label" for="guardian-updates">Fees Payment</label>
                            </div>
                            <p class="text-muted font-size-sm">
                                If selected, guardian will be able to view your fee invoices, and can as well Pay for
                                the invoices.
                            </p>
                            <div class="block-content row justify-content-center border-top">
                                <div class="col-9">
                                    <button type="submit" class="btn btn-block btn-hero-primary"
                                            onclick="updateGuardian()">
                                        <i class="fa fa-fw fa-save mr-1"></i> Update
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div>
