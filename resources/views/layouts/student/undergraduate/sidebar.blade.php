<div class="bg-header-dark">
    <div class="content-header bg-white-10">
        <a class="link-fx font-w600 font-size-lg text-white" href="{{url('')}}">
            <span class="text-white-75">{{config('app.abbr')}}</span> <span class="text-white">UMS</span>
        </a>
        <div>
            <a class="d-lg-none text-white ml-2" data-toggle="layout" data-action="sidebar_close"
               href="javascript:void(0)">
                <i class="fa fa-times-circle"></i>
            </a>
        </div>
    </div>
</div>
<div class="content-side content-side-full">
    <ul class="nav-main">
        <li class="nav-main-item">
            <a class="nav-main-link active" href="{{url('home')}}">
                <i class="nav-main-link-icon si si-home"></i>
                <span class="nav-main-link-name">Dashboard</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('/undergraduate/notices')}}">
                <i class="nav-main-link-icon si si-info"></i>
                <span class="nav-main-link-name">Notices</span>
            </a>
        </li>
        <li class="nav-main-heading">Courses</li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('/undergraduate/courses/register')}}">
                <i class="nav-main-link-icon si si-note"></i>
                <span class="nav-main-link-name">Register Courses</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('undergraduate/courses/results')}}">
                <i class="nav-main-link-icon si si-book-open"></i>
                <span class="nav-main-link-name">View Results</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-docs"></i>
                <span class="nav-main-link-name">View Courses</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/courses/current-level')}}">
                        <span class="nav-main-link-name">Show Current Session</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/courses/current-semester')}}">
                        <span class="nav-main-link-name">Show Current Semester</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/courses/level')}}">
                        <span class="nav-main-link-name">Show by Level</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/courses/semester')}}">
                        <span class="nav-main-link-name">Show by Semester</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/courses/registered')}}">
                        <span class="nav-main-link-name">Show Registered Courses</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/courses/outstanding')}}">
                        <span class="nav-main-link-name">Show Outstanding Courses</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-heading">Payments</li>
        <li class="nav-main-item">
            <a class="nav-main-link nav-main-link-submenu" data-toggle="submenu" aria-haspopup="true"
               aria-expanded="false" href="#">
                <i class="nav-main-link-icon si si-docs"></i>
                <span class="nav-main-link-name">Fees</span>
            </a>
            <ul class="nav-main-submenu">
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/fees/payment')}}">
                        <span class="nav-main-link-name">Make Payment</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/fees/invoices')}}">
                        <span class="nav-main-link-name">View Invoices</span>
                    </a>
                </li>
                <li class="nav-main-item">
                    <a class="nav-main-link" href="{{url('/undergraduate/fees/history')}}">
                        <span class="nav-main-link-name">Payment History</span>
                    </a>
                </li>
            </ul>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="javascript:void(0)">
                <i class="nav-main-link-icon fa fa-building"></i>
                <span class="nav-main-link-name">Hostel</span>
            </a>
        </li>
        <li class="nav-main-heading d-block d-md-none">Messages</li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('undergraduate/messages/inbox')}}">
                <i class="nav-main-link-icon si si-envelope"></i>
                <span class="nav-main-link-name">Inbox</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('undergraduate/messages/starred')}}">
                <i class="nav-main-link-icon si si-star"></i>
                <span class="nav-main-link-name">Starred</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('undergraduate/messages/sent')}}">
                <i class="nav-main-link-icon si si-share-alt"></i>
                <span class="nav-main-link-name">Sent</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('undergraduate/messages/draft')}}">
                <i class="nav-main-link-icon si si-folder-alt"></i>
                <span class="nav-main-link-name">Draft</span>
            </a>
        </li>
        <li class="nav-main-item d-block d-md-none">
            <a class="nav-main-link" href="{{url('undergraduate/messages/trash')}}">
                <i class="nav-main-link-icon si si-trash"></i>
                <span class="nav-main-link-name">Trash</span>
            </a>
        </li>

        <li class="nav-main-heading d-block d-md-none">Others</li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('/lms')}}">
                <i class="nav-main-link-icon fa fa-users"></i>
                <span class="nav-main-link-name">Learning Centre</span>
            </a>
        </li>
        <li class="nav-main-item">
            <a class="nav-main-link" href="{{url('/forums')}}">
                <i class="nav-main-link-icon fa fa-users"></i>
                <span class="nav-main-link-name">Forums</span>
            </a>
        </li>
        <li class="nav-main-item mt-3">
            <a class="btn btn-block btn-outline-primary" href="{{ url('/logout') }}"
               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i>
                <span class="nav-main-link-name">Logout</span>
            </a>
        </li>
    </ul>
</div>