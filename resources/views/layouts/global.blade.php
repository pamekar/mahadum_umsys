<?php
$public = '';
if (config('app.env') == 'production')
    $public = 'public';
$welcome = session('welcome');
$data = session('userData');
$notification = session('notification');
function time_elapsed($datetime, $full = false)
{
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full) $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

?>
<html lang="en">
<head>
    <title>@yield('title') | MAHADUM</title>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-sale=1, shrink-to-fit=no"
    >
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset($public.'images/global/_icon.ico')}}"
          type="image/x-icon"
    >
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/css1f05.css?family=Nunito+Sans:300,400,400i,600,700')}}">
    <link rel="stylesheet" id="css-main" href="{{asset($public.'/css/dashmix.min-1.1.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/global.css')}}">
    @yield('style')

</head>

<body class="mahadum">
@if(isset($welcome))
    <div id="page-loader" class="show bg-primary"></div>
@endif
<div id="page-container"
     class="sidebar-o enable-page-overlay side-scroll page-header-dark page-header-fixed">
    <aside id="side-overlay">
        @includeWhen(Auth::user()->user_type==='undergraduate','layouts.student.undergraduate.side-overlay')
        @includeWhen(Auth::user()->user_type==='staff' && Auth::user()->user_view === 'lecturer','layouts.staff.lecturer.side-overlay')
    </aside>
    <nav id="sidebar" aria-label="Main Navigation">
        @includeWhen(Auth::user()->user_type==='undergraduate','layouts.student.undergraduate.sidebar')
        @includeWhen(Auth::user()->user_type==='staff' && Auth::user()->user_view === 'lecturer','layouts.staff.lecturer.sidebar')
    </nav>
    <header id="page-header">
        <div class="content-header">
            <div>
                <button type="button" class="btn btn-dual mr-1" data-toggle="layout" data-action="sidebar_toggle">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="header_search_on">
                    <i class="fa fa-fw fa-search"></i> <span class="ml-1 d-none d-sm-inline-block">Search</span>
                </button>
            </div>
            <div>
                <div class="dropdown d-inline-block">
                    <button type="button" class="btn btn-dual" id="page-header-user-dropdown" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-user d-sm-none"></i>
                        <span class="d-none d-sm-inline-block">{{$data->user_meta->first_name}}</span>
                        <i class="fa fa-fw fa-angle-down ml-1 d-none d-sm-inline-block"></i>
                    </button>
                    <div class="dropdown-menu dropdown-menu-right p-0" aria-labelledby="page-header-user-dropdown">
                        <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                            User Options
                        </div>
                        <div class="p-2">
                            <a class="dropdown-item" href="{{url('/undergraduate/profile')}}">
                                <i class="far fa-fw fa-user mr-1"></i> Profile
                            </a>
                            <a class="dropdown-item d-flex align-items-center justify-content-between"
                               href="{{url('/undergraduate/messages')}}">
                                <span><i class="far fa-fw fa-envelope mr-1"></i> Inbox</span>
                                <span class="badge badge-primary">{{$data->msgCount}}</span>
                            </a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="javascript:void(0)" data-toggle="layout"
                               data-action="side_overlay_toggle">
                                <i class="far fa-fw fa-building mr-1"></i> Settings
                            </a>
                            <div role="separator" class="dropdown-divider"></div>
                            <a class="dropdown-item" href="{{ url('/logout') }}"
                               onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
                                <i class="far fa-fw fa-arrow-alt-circle-left mr-1"></i> Sign Out
                            </a>
                        </div>
                    </div>
                </div>

                <a href="{{url('/undergraduate/messages')}}" class="btn btn-dual">
                    <i class="fa fa-fw fa-envelope"></i>
                    <span class="badge badge-secondary badge-pill">{{$data->msgCount}}</span>
                </a>

                <div class="dropdown d-inline-block">
                    <button type="button" class="btn btn-dual" id="page-header-notifications-dropdown"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <i class="fa fa-fw fa-bell"></i>
                        <span class="badge badge-secondary badge-pill">{{count($data->notices)}}</span>
                    </button>
                    <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right p-0"
                         aria-labelledby="page-header-notifications-dropdown">
                        <div class="bg-primary-darker rounded-top font-w600 text-white text-center p-3">
                            Notifications
                        </div>
                        <ul class="nav-items my-2">
                            @foreach($data->notices as $notice)
                                <li>
                                    <a class="text-dark media py-2"
                                       href="{{url('/undergraduate/notices/view/'.$notice->id)}}">
                                        <div class="mx-3">
                                            <i class="fa fa-fw fa-check-circle text-success"></i>
                                        </div>
                                        <div class="media-body font-size-sm pr-2">
                                            <div class="font-w600">{{$notice->title}}</div>
                                            <div class="text-muted font-italic">{{time_elapsed($notice->created_at)}}</div>
                                        </div>
                                    </a>
                                </li>
                            @endforeach
                        </ul>
                        <div class="p-2 border-top">
                            <a class="btn btn-light btn-block text-center" href="{{url('/undergraduate/notices')}}">
                                <i class="fa fa-fw fa-eye mr-1"></i> View All
                            </a>
                        </div>
                    </div>
                </div>
                <button type="button" class="btn btn-dual" data-toggle="layout" data-action="side_overlay_toggle">
                    <i class="far fa-fw fa-list-alt"></i>
                </button>
            </div>
        </div>
        <div id="page-header-search" class="overlay-header bg-primary">
            <div class="content-header">
                <form class="w-100" action="{{url('/undergraduate/search')}}"
                      method="get">
                    <div class="input-group">
                        <div class="input-group-prepend">
                            <button type="button" class="btn btn-primary" data-toggle="layout"
                                    data-action="header_search_off">
                                <i class="fa fa-fw fa-times-circle"></i>
                            </button>
                        </div>
                        <input type="text" class="form-control border-0" placeholder="Search or hit ESC.."
                               id="page-header-search-input" value="{{$query or null}}" name="search">
                    </div>
                </form>
            </div>
        </div>
        <div id="page-header-loader" class="overlay-header bg-primary-darker">
            <div class="content-header">
                <div class="w-100 text-center">
                    <span class="text-white fa-2x">Sending</span>
                    <i class="fa fa-fw fa-2x fa-spinner fa-spin text-white"></i>
                </div>
            </div>
        </div>
    </header>
    <main id="main-container">
        @includeIf('layouts.partials.page-description')
        <div class="content">
            @yield('content')
        </div>
    </main>
    <footer id="page-footer" class="bg-body-light">
        <div class="content py-3">
            <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                    Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600"
                                                                               href="{{config('app.designer_url')}}"
                                                                               target="_blank">{{config('app.designer')}}</a>
                </div>
                <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
                    <a class="font-w600" href="{{url('')}}" target="_blank">{{config('app.name')}}</a> &copy; <span
                            data-toggle="year-copy">{{date('Y')}}</span>
                </div>
            </div>
        </div>
    </footer>
</div>
<form id="logout-form" action="{{ url('/logout') }}"
      method="POST" style="display: none;"
>{{ csrf_field() }}</form>
<script src="{{asset($public.'/js/dashmix.core.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/dashmix.app.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset($public.'/js/chart.bundle.min.js')}}"></script>
<script src="{{asset($public.'/js/be_pages_dashboard.min.js')}}"></script>
<script src="{{asset($public.'/js/bootstrap-notify.min.js')}}"></script>
@if(Auth::user()->user_type=='undergraduate')
    <script src="{{asset($public.'/js/student/undergraduate/global.js')}}"></script> @endif

<script>
    jQuery(function () {
        Dashmix.helpers('sparkline');
        @if(isset($notification))
        Dashmix.helpers('notify', {
            type: '{{$notification->type}}',
            icon: 'fa fa-info mr-1',
            message: '{{$notification->message}}'
        });
        @endif
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

</script>
@yield('script')
<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5b8a8927f31d0f771d845bee/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->
</body>
</html>