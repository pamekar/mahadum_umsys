@php
    $public = '';
    if (config('app.env') == 'production')
        $public = 'public';
    $notification = session('notification');
    $data = session('userData');
    $wards = $data->wards;

@endphp
<html lang="en">
<head>
    <title>@yield('title') - MAHADUM</title>
    <meta charset="utf-8">
    <meta name="viewport"
          content="width=device-width, initial-sale=1, shrink-to-fit=no"
    >
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="icon" href="{{asset($public.'images/global/_icon.ico')}}" type="image/x-icon">
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/font-awesome.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/css1f05.css?family=Nunito+Sans:300,400,400i,600,700')}}">
    <link rel="stylesheet" id="css-main" href="{{asset($public.'/css/dashmix.min-1.1.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/global.css')}}">
    @yield('style')

</head>
<body class="mahadum">
<div id="page-container" class="enable-page-overlay side-scroll main-content-boxed">
    @if(isset($data))
        <aside id="side-overlay">
            <div class="bg-image" style="background-image: url('/{{$public}}/jpg/bg_side_overlay_header.jpg');">
                <div class="bg-success-op">
                    <div class="content-header">
                        <a class="img-link mr-1" href="#">
                            <img class="img-avatar img-avatar48" src=""
                                 alt="">
                        </a>
                        <div class="ml-2">
                            <a class="text-white font-w600"
                               href="javascript:void(0)">{{Auth::user()->name}}</a>
                        </div>
                        <a class="ml-auto text-white" href="javascript:void(0)" data-toggle="layout"
                           data-action="side_overlay_close">
                            <i class="fa fa-times-circle"></i>
                        </a>
                    </div>
                </div>
            </div>
            <div class="content-side">
                <div class="block block-transparent pull-x pull-t">
                    <ul class="nav nav-tabs nav-tabs-block nav-justified" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active show" href="#so-people">
                                <i class="far fa-fw fa-user-circle"></i>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#so-profile">
                                <i class="fa fa-cog"></i>
                            </a>
                        </li>
                    </ul>
                    <div class="block-content tab-content overflow-hidden">
                        <div class="tab-pane pull-x fade fade-up active show" id="so-people" role="tabpanel">
                            <div class="block mb-0">
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Active Wards</span>
                                    <a href="javascript:void(0)" class="badge badge-pill badge-success float-right"
                                       data-html="true" data-animation="true" data-toggle="popover" data-placement="top"
                                       title=""
                                       data-content="This is a list of your wards. You can click on the links to view their schedule, performance, stats, and invoices. You can also pay their fees.<br><br>Goodluck!"
                                       data-original-title="Active Wards">?</a>
                                </div>
                                <div class="block-content">
                                    <ul class="nav-items">
                                        @foreach($wards['active'] as $ward)
                                            <li>
                                                <div class="media py-2">
                                                    <div class="mx-3 overlay-container">
                                                        <img class="img-avatar img-avatar48"
                                                             src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                                                        <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="font-w600"><a
                                                                    href="{{url("/guardian/view/$ward->reg_no")}}">{{$ward->first_name.' '.$ward->last_name}}</a>
                                                        </div>
                                                        <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                                                    </div>
                                                    <div class="btn-group-vertical btn-group-sm" role="group"
                                                         aria-label="Small Outline Primary">

                                                        <button class="btn btn-outline-danger" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Deactivate ward"
                                                                onclick="ward({{$ward->reg_no}},'blocked','active')"><i
                                                                    class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Pending Wards</span>
                                    <a href="javascript:void(0)" class="badge badge-pill badge-warning float-right"
                                       data-html="true" data-animation="true" data-toggle="popover" data-placement="top"
                                       title=""
                                       data-content="This shows a list of requests from student(s) to have you as their guardian. You can activate, block or just ignore the request.<br><br>Goodluck!"
                                       data-original-title="Pending Wards">?</a>
                                </div>
                                <div class="block-content">
                                    <ul class="nav-items">
                                        @foreach($wards['pending'] as $ward)
                                            <li>
                                                <div class="media py-2">
                                                    <div class="mx-3 overlay-container">
                                                        <img class="img-avatar img-avatar48"
                                                             src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                                                        <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-warning"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="font-w600"><a
                                                                    href="{{url("/guardian/view/$ward->reg_no")}}">{{$ward->first_name.' '.$ward->last_name}}</a>
                                                        </div>
                                                        <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                                                    </div>
                                                    <div class="btn-group-vertical btn-group-sm" role="group"
                                                         aria-label="Small Outline Primary">
                                                        <button class="btn btn-outline-success" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Activate Request"
                                                                onclick="ward({{$ward->reg_no}},'active','pending')"><i
                                                                    class="fa fa-check"></i></button>
                                                        <button class="btn btn-outline-danger" data-toggle="tooltip"
                                                                data-placement="bottom" title=""
                                                                data-original-title="Block Request"
                                                                onclick="ward({{$ward->reg_no}},'blocked','pending')"><i
                                                                    class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Blocked Students</span>
                                    <a href="javascript:void(0)" class="badge badge-pill badge-danger float-right"
                                       data-html="true" data-animation="true" data-toggle="popover" data-placement="top"
                                       title=""
                                       data-content="This is a list of students you've blocked from sending you requests. You can unblock or <b>permanently</b> block the student."
                                       data-original-title="Blocked Wards">?</a>
                                </div>
                                <div class="block-content">
                                    <ul class="nav-items">
                                        @foreach($wards['blocked'] as $ward)
                                            <li>
                                                <div class="media py-2">
                                                    <div class="mx-3 overlay-container">
                                                        <img class="img-avatar img-avatar48"
                                                             src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                                                        <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-danger"></span>
                                                    </div>
                                                    <div class="media-body">
                                                        <div class="font-w600"><a
                                                                    href="{{url("/guardian/view/$ward->reg_no")}}">{{$ward->first_name.' '.$ward->last_name}}</a>
                                                        </div>
                                                        <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                                                    </div>
                                                    <div class="btn-group-vertical btn-group-sm" role="group"
                                                         aria-label="Small Outline Primary">
                                                        <button class="btn btn-outline-success" data-toggle="tooltip"
                                                                data-placement="top" title=""
                                                                data-original-title="Unblock Ward"
                                                                onclick="ward({{$ward->reg_no}},'active','blocked')"><i
                                                                    class="fa fa-check"></i></button>
                                                        <button class="btn btn-outline-danger" data-toggle="tooltip"
                                                                data-placement="bottom" title=""
                                                                data-original-title="Block Permanently!"
                                                                onclick="ward({{$ward->reg_no}},'deleted','blocked')"><i
                                                                    class="fa fa-times"></i>
                                                        </button>
                                                    </div>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                                {{--<div class="block-content row justify-content-center border-top">
                                    <div class="col-9">
                                        <a class="btn btn-block btn-hero-primary" href="javascript:void(0)">
                                            <i class="fa fa-fw fa-plus mr-1"></i> Add People
                                        </a>
                                    </div>
                                </div>--}}
                            </div>
                        </div>
                        <div class="tab-pane pull-x fade fade-left" id="so-profile" role="tabpanel">

                            <div class="block mb-0">
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Personal</span>
                                </div>
                                <div class="block-content block-content-full">
                                    <div class="form-group">
                                        <label>Username</label>
                                        <input type="text" readonly class="form-control" id="staticEmail"
                                               value="{{Auth::user()->name}}" readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="so-profile-email">Email</label>
                                        <input type="email" class="form-control" id="so-profile-email"
                                               name="so-profile-email" value="{{Auth::user()->email}}" readonly>
                                    </div>
                                </div>
                                <div class="block-content block-content-sm block-content-full bg-body">
                                    <span class="text-uppercase font-size-sm font-w700">Password Update</span>
                                </div>
                                <form action="javascript:void(0)" method="post" id="password-form">
                                    {{method_field('patch')}}
                                    <div class="block-content block-content-full">
                                        <div class="form-group">
                                            <label for="so-profile-password">Current Password</label>
                                            <input type="password" class="form-control password"
                                                   id="so-profile-password"
                                                   name="current_password">
                                        </div>
                                        <div class="form-group">
                                            <label for="so-profile-new-password">New Password</label>
                                            <input type="password" class="form-control password" id="new-password"
                                                   name="new_password">
                                        </div>
                                        <div class="form-group">
                                            <label for="password-confirmation">Confirm New Password</label>
                                            <input type="password" class="form-control password"
                                                   id="so-profile-new-password-confirm"
                                                   name="new_password_confirmation">
                                        </div>
                                    </div>
                                    <div class="block-content row justify-content-center border-top">
                                        <div class="col-9">
                                            <button type="submit" class="btn btn-block btn-hero-primary"
                                                    onclick="changePassword()">
                                                <i class="fa fa-fw fa-save mr-1"></i> Update
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </aside>
    @endif
    <header id="page-header">
        <div class="content-header">
            <div>
                <a class="link-fx font-size-lg text-dual" href="{{url('/')}}">
                    <span class="font-w700 text-dual">{{config('app.abbr')}}</span> <span class="font-w300">UMS</span>
                </a>
            </div>
            <div class="d-flex align-items-center">
                <a href="{{ url('/logout') }}"
                   onclick="event.preventDefault(); document.getElementById('logout-form').submit();"
                   class="btn btn-sm btn-dual">
                    <i class="fa fa-fw fa-arrow-alt-circle-left mr-1"></i> <span class="hidden-sm-down">Logout</span>
                </a>
                <a href="{{ route('guardian.profile') }}" class="btn btn-sm btn-dual">
                    <i class="far fa-user-circle mr-1"></i> <span class="hidden-sm-down">Profile</span>
                </a>
                <button type="button" class="btn btn-sm btn-dual" data-toggle="layout"
                        data-action="side_overlay_toggle">
                    <i class="fa fa-fw fa-bars"></i>
                </button>
            </div>
        </div>
        <div id="page-header-loader" class="overlay-header bg-white">
            <div class="content-header">
                <div class="w-100 text-center">
                    <i class="fa fa-fw fa-2x fa-sun fa-spin"></i>
                </div>
            </div>
        </div>
    </header>
    <main id="main-container">
        <div class="bg-body-light border-top border-bottom">
            <div class="content content-full py-1">
                <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                    <h1 class="flex-sm-fill font-size-sm text-uppercase font-w700 mt-2 mb-0 mb-sm-2">
                        <i class="fa fa-angle-right fa-fw text-primary"></i> Welcome {{Auth::user()->name}}
                    </h1>
                    <nav class="flex-sm-00-auto ml-sm-3 font-size-sm" aria-label="breadcrumb">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item">Home</li>
                            <li class="breadcrumb-item active" aria-current="page">Dashboard</li>
                        </ol>
                    </nav>
                </div>
            </div>
        </div>
        @yield('content')
    </main>
    <footer id="page-footer" class="bg-body-light">
        <div class="content py-3">
            <div class="row font-size-sm">
                <div class="col-sm-6 order-sm-2 py-1 text-center text-sm-right">
                    Crafted with <i class="fa fa-heart text-danger"></i> by <a class="font-w600"
                                                                               href="{{config('app.designer_url')}}"
                                                                               target="_blank">{{config('app.designer')}}</a>
                </div>
                <div class="col-sm-6 order-sm-1 py-1 text-center text-sm-left">
                    <a class="font-w600" href="{{url('')}}" target="_blank">{{config('app.name')}}</a> &copy; <span
                            data-toggle="year-copy">{{date('Y')}}</span>
                </div>
            </div>
        </div>
    </footer>
</div>
<form id="logout-form" action="{{ url('/logout') }}"
      method="POST" style="display: none;"
>{{ csrf_field() }}</form>

<script src="{{asset($public.'/js/dashmix.core.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/dashmix.app.min-1.1.js')}}"></script>
<script src="{{asset($public.'/js/jquery.sparkline.min.js')}}"></script>
<script src="{{asset($public.'/js/chart.bundle.min.js')}}"></script>
<script src="{{asset($public.'/js/be_pages_dashboard.min.js')}}"></script>
<script src="{{asset($public.'/js/bootstrap-notify.min.js')}}"></script>
<script src="{{asset($public.'/js/guardian/global.js')}}"></script>

<script>
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    jQuery(function () {
        Dashmix.helpers('sparkline');
        @if(isset($notification))
        Dashmix.helpers('notify', {
            type: '{{$notification->type}}',
            icon: 'fa fa-info mr-1',
            message: '{{$notification->message}}'
        });
        @endif
    });
</script>

@yield('script')
</body>
</html>