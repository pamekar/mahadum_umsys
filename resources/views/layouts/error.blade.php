@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp <!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>@yield('title') - Mahadum</title>
    <meta name="description"
          content="{{config('app.name')}}">
    <meta name="author" content="pixelcave">
    <meta name="robots" content="noindex, nofollow">
    <meta property="og:title" content="{{config('app.name')}}">
    <meta property="og:site_name" content="{{config('app.name')}}">
    <meta property="og:description" content="{{config('app.name')}}">
    <meta property="og:type" content="website">
    <meta property="og:url" content="">
    <meta property="og:image" content="">
    <link rel="icon" href="{{asset($public.'images/global/_icon.ico')}}">
    <link rel="stylesheet" href="{{asset("$public/css/css1f05.css?family=Nunito+Sans:300,400,400i,600,700")}}">
    <link rel="stylesheet" id="css-main" href="{{asset("$public/css/dashmix.min-1.1.css")}}">
    <script>(function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o), m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'js/analytics.js', 'ga');
        ga('create', 'UA-16158021-6', 'auto');
        ga('send', 'pageview');</script>
</head>
<body>
<div id="page-container">
    <main id="main-container">
        <div class="bg-image" style="background-image: url('{{asset("$public/jpg/photo19%402x.jpg")}}');">
            <div class="hero bg-white-95">
                <div class="hero-inner">
                    <div class="content content-full">
                        <div class="px-3 py-5 text-center">
                            @yield('content')
                            <div class="invisible" data-toggle="appear" data-class="animated fadeInUp"
                                 data-timeout="600">
                                <a class="btn btn-hero-info" href="{{url()->previous()}}">
                                    <i class="fa fa-arrow-left mr-1"></i> Go Back
                                </a>
                                <a class="btn btn-hero-success" href="{{url('')}}">
                                    <i class="fa fa-home mr-1"></i> Go to Home
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
</div>
<script src="{{asset("$public/js/dashmix.core.min-1.1.js")}}"></script>
<script src="{{asset("$public/js/dashmix.app.min-1.1.js")}}"></script>
</body>
</html>