@php
    $public = '';
    if (config('app.env') == 'production')
        $public = 'public/';
@endphp
@if(Request::segment(1)=='' || Request::segment(1)=='home')
    <div class="bg-image" style="background-image: url('/{{$public}}jpg/bg_dashboard.jpg');">
        <div class="bg-white-90">
            <div class="content content-full">
                <div class="row">
                    <div class="col-md-6 d-md-flex align-items-md-center">
                        <div class="py-4 py-md-0 text-center text-md-left invisible" data-toggle="appear">
                            <h1 class="font-size-h2 mb-2">Dashboard</h1>
                            <h2 class="font-size-lg font-w400 text-muted mb-0">
                                Hello <span class="font-w600 text-primary">{{$data->user_meta->first_name}}</span>,
                                welcome to {{config('app.abbr')}} UMS
                            </h2>
                        </div>
                    </div>
                    @if(Auth::user()->user_type==='undergraduate')
                        <div class="col-md-6 d-md-flex align-items-md-center">
                            <div class="row w-100 text-center">
                                <div class="col-6 col-xl-4 offset-xl-4 invisible" data-toggle="appear"
                                     data-timeout="300">
                                    <p class="font-size-h3 font-w600 text-primary mb-0">
                                        {{$data->outstanding_courses}}
                                    </p>
                                    <p class="font-size-sm font-w700 text-uppercase mb-0 text-muted">
                                        <i class="far fa-newspaper text-muted mr-1"></i> Courses
                                    </p>
                                </div>
                                <div class="col-6 col-xl-4 invisible" data-toggle="appear" data-timeout="600">
                                    <p class="font-size-h3 font-w600 text-primary mb-0">
                                        &#8358; {{$data->outstanding_fees}}
                                    </p>
                                    <p class="font-size-sm font-w700 text-uppercase mb-0 text-muted">
                                        <i class="far fa-money-bill-alt text-muted mr-1"></i> Fees
                                    </p>
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@else
    <div class="bg-body-light">
        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">@yield('title')</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('')}}">Home</a></li>
                        @if(isset($breadcrumbs))
                            @foreach($breadcrumbs as $breadcrumb)
                                @if(!$loop->last)
                                    <li class="breadcrumb-item" aria-current="page">
                                        <a href="{{url($breadcrumb['url'])}}">{{$breadcrumb['title']}}</a>
                                    </li>
                                @elseif($loop->last)
                                    <li class="breadcrumb-item active" aria-current="page">
                                        <a href="{{url($breadcrumb['url'])}}">{{$breadcrumb['title']}}</a>
                                    </li>
                                @endif
                            @endforeach
                        @else
                            <li class="brea dcrumb-item active" aria-current="page">@yield('title')</li>
                        @endif
                    </ol>
                </nav>
            </div>
        </div>
    </div>
@endif