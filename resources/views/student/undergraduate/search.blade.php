@php
    $public='';
 if(config('app.env') == 'production')
 $public ='public';

    function highlightKeywords($text, $keyword){

        $wordsAry = explode(" ", $keyword);
        $wordsCount = count($wordsAry);

        for($i=0;$i<$wordsCount;$i++) {
            $highlighted_text = "<span class='font-w600'>$wordsAry[$i]</span>";
            $text = str_ireplace($wordsAry[$i], $highlighted_text, $text);
        }

		return $text;
	}
    function getMsgDate($date){
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }

    function trimText($text,$length=50){
        $trimmedText = "";
        $content=preg_replace('/\s+/', ' ',strip_tags($text));;

        $content = explode(" ", $content);
        $i = 0;
        while (($i < $length && count($content) > $length)
            || ($i < count($content)
                && count($content) < $length)) {
            $trimmedText .=  $content [$i] . " ";
            $i++;
        }
        return $trimmedText;
    }
@endphp
@extends('layouts.global')
@section('content')
    <div class="bg-white">
        <div class="content">
            <form class="push" action="{{url('/undergraduate/search')}}" method="get">
                <div class="input-group input-group-lg">
                    <input type="text" class="form-control form-control-alt" placeholder="Search.." name="search"
                           value="{{$query or null}}">
                    <div class="input-group-append">
	                    <span class="input-group-text border-0 bg-body">
	                        <i class="fa fa-fw fa-search"></i>
	                    </span>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="content">
        <div class="block block-rounded block-bordered">
            <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                <li class="nav-item">
                    <a class="nav-link @if(count($courses)>count($notices) && count($courses)>count($messages)) active @endif"
                       href="#search-courses">Courses <span
                                class="badge badge-primary badge-pill">{{count($courses)}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(count($notices)>count($courses) && count($notices)>count($messages)) active @endif"
                       href="#search-notices">Notices <span
                                class="badge badge-primary badge-pill">{{count($notices)}}</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link @if(count($messages)>count($notices) && count($messages)>count($courses)) active @endif"
                       href="#search-messages">Messages <span
                                class="badge badge-primary badge-pill">{{count($messages)}}</span></a>
                </li>
            </ul>
            <div class="block-content tab-content overflow-hidden">
                <div class="tab-pane fade @if(count($courses)>count($notices)&&count($courses)>count($messages)) show active @endif"
                     id="search-courses" role="tabpanel">
                    <div class="font-size-h3 font-w600 pt-2 pb-4 mb-4 text-center border-bottom">
                        <span class="text-primary font-w700">{{count($courses)}}</span> courses found for
                        <mark class="text-danger">{{$query}}</mark>
                    </div>
                    @if(sizeof($courses)>0)

                        <div class="row gutters-tiny push">
                            <table class="js-dataTable-full table-hover table-vcenter table-sm">

                                <thead>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                    <tr>
                                        <td>
                                            @include('student.undergraduate.partials.courseList')
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
                <div class="tab-pane fade @if(count($notices)>count($courses)&&count($notices)>count($messages)) show active @endif"
                     id="search-notices" role="tabpanel">
                    <div class="font-size-h3 font-w600 pt-2 pb-4 mb-4 text-center border-bottom">
                        <span class="text-primary font-w700">{{count($notices)}}</span> notices found for
                        <mark class="text-danger">{{$query}}</mark>
                    </div>
                    @if(sizeof($notices)>0)

                        <div class="row gutters-tiny push">
                            <table class="js-dataTable-full table-hover table-vcenter table-sm">

                                <thead>
                                <th></th>
                                </thead>
                                <tbody>
                                @foreach($notices as $notice)
                                    <tr>
                                        <td>
                                            @include('student.undergraduate.partials.noticeList')
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
                <div class="tab-pane fade @if(count($messages)>count($notices) && count($messages)>count($courses)) show active @endif"
                     id="search-messages" role="tabpanel">
                    <div class="font-size-h3 font-w600 pt-2 pb-4 mb-4 text-center border-bottom">
                        <span class="text-primary font-w700">{{count($messages)}}</span> messages found for
                        <mark class="text-danger">{{$query}}</mark>
                    </div>
                    @if(sizeof($messages)>0)

                        <div class="row gutters-tiny push">
                            <table class="js-dataTable-full table-hover table-vcenter table-sm">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($messages as $message)
                                    <tr class="@if(!in_array(Auth::user()->name,explode(';',$message->seen))) table-warning @endif">
                                        <td>@if(in_array(Auth::user()->name,explode(';',$message->starred)))
                                                <a href="javascript:void(0)" class="starred" onclick="starMessage(this)"
                                                   data-msgid="{{$message->message_id}}"><i
                                                            class="fa fa-star text-warning"></i></a>
                                            @else
                                                <a href="javascript:void(0)" class="starred" onclick="starMessage(this)"
                                                   data-msgid="{{$message->message_id}}"><i
                                                            class="si si-star text-warning"></i></a>
                                            @endif
                                        </td>
                                        <td class="d-none d-sm-table-cell font-w600"
                                            style="width: 140px;">@if($message->from==Auth::user()->name)
                                                <em>me</em> @else {!! highlightKeywords($message->name, $query) !!} @endif
                                        </td>
                                        <td>
                                            <a class="font-w600"
                                               href="{{url("undergraduate/messages/view/$message->message_id")}}">{!! highlightKeywords($message->subject, $query) !!}</a>
                                            <div class="text-muted ">{!! highlightKeywords(trimText($message->message,50),$query) !!}</div>
                                        </td>
                                        <td class="d-none d-xl-table-cell font-w600 font-size-sm text-muted"
                                            style="width: 120px;">
                                            {{ getMsgDate($message->created_at) }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
                <div class="tab-pane fade" id="search-people" role="tabpanel">
                    <div class="font-size-h3 font-w600 pt-2 pb-4 mb-4 text-center border-bottom">
                        <span class="text-primary font-w700">6</span> projects found for
                        <mark class="text-danger">HTML</mark>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset($public.'/js/jquery.datatables.min.js')}}"></script>
    <script src="{{asset($public.'/js/datatables.bootstrap4.min.js')}}"></script>
    <script src="{{asset($public.'/js/datatables.buttons.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.print.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.colvis.min.js')}}"></script>
    <script src="{{asset($public.'/js/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset($public.'/js/student/undergraduate/messages.js')}}"></script>
@endsection
