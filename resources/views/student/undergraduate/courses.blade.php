@php $public=''; if(config('app.env') == 'production') $public='public' @endphp
@extends('layouts.global')
@if(!isset($pageTitle))
    $pageTitle = 'Courses' @endif @section('title',$pageTitle)
@section('style')

    <style>
        #body-content {
            padding: 1.5rem 0.5rem;
        }

        @media ( min-width: 768px) {
            .bootstrap-vertical-nav {
                margin-top: 50px;
            }
        }

        .card-header {
            display: inline-block;
        }

        #header {
            display: inline-block;
        }

        .list-inline-item {
            padding-left: 0.5rem;
            padding-right: 0.5rem;
            border-right: 1px dotted;
            line-height: 0.7rem;
        }

        .list-inline-item:last-child {
            border: none;
        }

        .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active {
            color: #fff;
            background-color: #2c82b8;
            border-color: #2b80b5;
        }

        .nav-pills .nav-link.active:hover, .nav-pills .nav-link.active:active {
            color: #fff !important;
            background-color: #26709e;
            border-color: #2772a1;
        }

        .nav-pills .nav-link.active {
            color: #fff;
            background-color: #2c82b8;
            border-color: #2b80b5;
        }

        #side-bar {
            margin-bottom: 0.5rem;
        }
    </style>
@endsection('style') @section('content')
    <div class="container">

        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">{{$pageTitle}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}"
                                                       class="list-link"
                            >Home</a></li>
                        <li class="breadcrumb-item"><a
                                    href="{{url('undergraduate/course')}}" class="list-link"
                            >Courses</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$pageTitle or 'courses'}}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 block" id="body-content">
            <div class="block-content">
                <div class="block-content" id="results-data">
                    {!! $html !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        <!--
        //-->
        var domain = "<?php echo url(''); ?>";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        sidebarAdjust();
        $(window).resize(function () {
            sidebarAdjust();
        });

        function sidebarAdjust() {
            if ($(window).width() <= 768) {
                $('#side-bar').attr('class', 'nav nav-tabs flex-row-reverse justify-content-center');
            }
            else {
                $('#side-bar').attr('class', 'nav nav-pills flex-column card');
            }
        }

        // drg>> the ajax request to attend to either level or semester requests
        @if($type === 'courses')
        setOptionVal();

        function changeHTML() {

            var action = $('#specify').attr('name');
            var id = $('#specify').val();
            var data = {'id': id, 'action': action};

            $.post("{{url('/undergraduate/courses')}}", data, function (result) {
                let content = $('#body-content');
                content.fadeOut(500);
                content.html('');
                content.hide();
                content.html(result.html);
                content.fadeIn(500);
                setOptionVal();
            });

        }

        function setOptionVal() {
            var value = $('#option').val();
            $('#specify').val(value);
        }

        @elseif($type === 'results')
        function changeResult() {

            var action = $('#specify').attr('name');
            var id = $('#specify').val();
            var data = {'id': id, 'action': action};

            $.post("{{url('/undergraduate/courses')}}", data, function (result) {

                $('#area').fadeOut(500);
                $('#area').html('');
                $('#area').hide();
                $('#area').html(result.html);
                $('#area').fadeIn(500);
                var value = $('#specify').val();
                var value_1 = parseInt(value) + 1;
                $('#currentSession').html(value + '/' + value_1);
                $('#download').attr('href', domain + '/undergraduate/download/result/' + id + '/pdf');
            });

        }

                @elseif($type === 'register')
        var courses =<?php echo json_encode($courses);?>;
        var totalUnits = 0;
        var minUnits = 12;
        var maxUnits = 24;

        for (i = 0; i < courses.length; i++) {
            totalUnits += parseInt(courses[i].units);
        }

        showNumbers();

        $('.register').change(function () {
            showNumbers();
        });

        function showNumbers() {
            $('.total-units').text(totalUnits);
            $('.max-units').text(maxUnits);
            $('.min-units').text(minUnits);
            verifyForm();
        }

        function verifyForm() {
            var unitSum = 0;
            $('.courses').each(function () {
                if ($(this).find('.register').is(':checked')) {
                    unitSum += parseInt($(this).find('.units').text());
                    <?php
                    if ( isset ($edit) )
                    {
                    if ( $edit === true )
                    {
                    ?>
                    $(this).attr('class', 'courses table-success');
                    <?php
                    }
                    }
                    ?>
                }
                <?php
                    if ( isset ($edit) )
                    {
                    if ( $edit === true )
                    {
                    ?>
                else if ($(this).find('.register').not(':checked')) {
                    $(this).attr('class', 'courses table-warning');
                }

                <?php
                }
                }
                ?>

            });

            var availableUnits = maxUnits - unitSum;
            $('.available-units').text(availableUnits);
            $('.selected-units').text(unitSum);

            if (unitSum < minUnits) {
                $('.unit-monitor').attr('class', 'unit-monitor blue');
                $('#submit-form').attr('class', 'btn btn-info');
                $('#submit-form').attr({'class': 'btn btn-info disabled', 'disabled': 'ture'});
            }
            else if (availableUnits < 0) {
                alert('\tNumber of Permitted Units:\t' + maxUnits +
                    '\n\tNumber of Units Selected:\t' + unitSum +
                    '\n\nYou have exceeded the maximum number of units.' +
                    '\nHence, your request will not be processed');
                $('.unit-monitor').attr('class', 'unit-monitor red');
                $('#submit-form').attr({'class': 'btn btn-danger disabled', 'disabled': 'ture'});
                return false;
            }
            else {
                $('.unit-monitor').attr('class', 'unit-monitor green');
                $('#submit-form').attr('class', 'btn btn-success');
                $('#submit-form').removeAttr('disabled');
                return true;
            }

        }

        $('#registration-form').submit(function () {
            return verifyForm();
        });
        @endif
    </script>

@endsection
