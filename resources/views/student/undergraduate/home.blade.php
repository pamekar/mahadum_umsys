@php
    $public='';
    if(config('app.env') == 'production')
       $public ='public';
    $data=session('userData');
    function reformatDate($date){
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }

@endphp
@extends('layouts.global')
@section('title','Home') @section('style')
    <link rel='stylesheet' href="{{asset($public.'/css/fullcalendar.css')}}"/>
    <style>
        #body-content {
            padding: 1.5rem 0.5rem;
            margin: 0.5rem 0rem;
        }

        .calendar h2 {
            color: #2CAC5C !important;
            font-size: 1.375em;
        }

        .calendar button {
            height: auto;
            padding: 0.3rem !important;
            line-height: 1.15;
            margin: 0;
        }

        .calendar button span {
            margin: 0 !important;
            font-size: 90%
        }

        .calendar button .fc-icon {
            top: initial;
        }

        .fc-unthemed td.fc-today {
            color: #fff;
            background-color: #f0ad4e;
            border-color: #f0ad4e;
        }

        .fc-row .fc-content-skeleton td {
            background: none;
            border-color: transparent;
            border-bottom: 0;
        }

        .fc-day-number:hover {
            background-color: #F3F3F3;
            color: #2CAC5C !important;
        }

        .notice .list-inline {
            margin-bottom: 0.5rem;
            font-size: 75%;
        }

        #quick-links .list-group-item {
            padding: 0rem;
        }

        #user-details {
            display: block;
        }

        .side-bar #user-details > .navbar-text {
            display: block;
            margin: 0.03125rem 0;
            padding: 0;
            margin: 0.03125rem 0;
        }

        .side-bar #user-details img {
            width: 8.75rem;
            height: 9.0rem;
        }
    </style>
@endsection('style') @section('content')
    <div class="container width-full">
        <div class="row">
            <div class="col-md-7 col-xs-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Your Time Table</h3>
                    </div>
                    <div class="block-content block-content-full calendar" id="time-table"></div>
                </div>
            </div>
            <div class="col-md-5 col-xs-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Memo/Notices</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <div class="list-group">
                            @foreach ( $data->notices as $notice )
                                @php
                                    $info = "";
                                    $content = explode(" ", strip_tags($notice->content));
                                    for ($i = 0; $i < 20; $i++) {
                                        $info = $info . $content [$i] . " ";
                                    }
                                @endphp

                                <div class="list-group-item notice bghover">
                                    <h3 class="small mb-0">
                                        <a class="green"
                                           href="{{url('/undergraduate/notices/view/'.$notice->id)}}"
                                        >{{$notice->title}}</a>
                                    </h3>
                                    <ul class="list-inline text-muted">
                                        <li class="list-inline-item">
                                            Time: {{reformatDate($notice->created_at)}}
                                        </li>
                                        <li class="list-inline-item">
                                            Announcer: {{$notice->admin_title}}
                                        </li>
                                    </ul>
                                    <article class="small">{{ $info}}<a
                                                href="{{url('/undergraduate/notices/view/'.$notice->id)}}"
                                                class="list-link"
                                        > ... read more</a>
                                    </article>
                                </div>
                            @endforeach
                        </div>
                        <div class="" id="view-all">
                            <a href="{{url('/undergraduate/notices')}}"
                               class="width-full btn btn-warning"
                            >View all</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 col-xs-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">School's Academic Calendar</h3>
                    </div>
                    <div class="block-content block-content-full calendar" id="school-calendar"></div>
                </div>
            </div>
            <div class="col-md-4 col-xs-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Attendance Chart</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <canvas id="attendance-chart" height="150" width="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-6 col-xs-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Results Chart</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <canvas id="results-chart" height="80" width="100%"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-xs-12">
                <div class="block block-rounded block-bordered">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Fees Payment Chart</h3>
                    </div>
                    <div class="block-content block-content-full">
                        <canvas id="payments-chart" height="80" width="100%"></canvas>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection @section('script')
    <script type='text/javascript' src="{{asset($public.'/js/moment.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/fullcalendar.js')}}"></script>
    <script src="{{asset("$public/js/Chart.min.js")}}"></script>
    <script type="text/javascript">
        <!--

        //-->
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#time-table').fullCalendar({
            // put your options and callbacks here
            defaultView: 'listDay',
            aspectRatio: 1.2,
            header: {
                right: 'prev,next today',
                left: 'title',
            },
            fixedWeekCount: false,
            contentHeight: 'auto',
            navLinks: true,
            startParam: 'start',
            endParam: 'end',
            //
            events: {
                url: '{{url("home/gettimetable")}}',
                type: 'POST',
                data: {
                    type: 'timetable',
                },
                error: function () {
                    alert('there was an error while fetching events!');
                },
                color: '#2CAC5C',   // a non-ajax option
                textColor: '#f5f5f5' // a non-ajax option
            }

        });
        $('#school-calendar').fullCalendar({
            // put your options and callbacks here
            aspectRatio: 1.5,
            header: {
                right: 'prev,next today',
                center: 'title',
                left: 'month,listWeek'
            },
            fixedWeekCount: false,
            contentHeight: 'auto',
            navLinks: true,
            startParam: 'start',
            endParam: 'end',
            //
            events: {
                url: '{{url("home/getevents")}}',
                type: 'POST',
                data: {
                    type: 'calendar',
                },
                error: function () {
                    alert('there was an error while fetching events!');
                },
                color: '#2CAC5C',   // a non-ajax option
                textColor: '#f5f5f5' // a non-ajax option
            }

        });

        var color = {
            background1: [
                'rgba(75, 192, 192, 0.8)',
                'rgba(153, 102, 255, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',
                'rgba(255, 159, 64, 0.8)',
                'rgba(255, 99, 132, 0.8)',
            ],
            background2: [
                'rgba(75, 192, 192, 0.5)',
                'rgba(153, 102, 255, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(255, 159, 64, 0.5)',
                'rgba(255, 99, 132, 0.5)',
            ],
            background3: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
            ],
            border: [
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
            ],
            border1: [
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 99, 132, 1)',
            ]
        };

        var ctx = document.getElementById("attendance-chart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [2014, 2015, 2016, 2017, 2018],
                datasets: [
                    {
                        data: {{json_encode($attendance_present)}},
                        label: "Present",
                        backgroundColor: color.background2,
                        borderColor: color.border,
                        fill: false
                    }, {
                        data: {{json_encode($attendance_absent)}},
                        label: "Absent",
                        backgroundColor: color.background3,
                        borderColor: color.border,
                        fill: false
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Students Attendance Chart'
                }
            }
        });

        var ctx = document.getElementById("results-chart");
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Grade A", "Grade B", "Grade C", "Grade D", "Grade F"],
                datasets: [
                    {
                        label: "Students Gender Chart",
                        backgroundColor: color.border1,
                        data: {{json_encode($results)}}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: "Students Results Chart"
                }
            }
        });

        var ctx = document.getElementById("payments-chart");
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Paid fees', 'Unpaid Debts'],
                datasets: [
                    {
                        label: "Students year Chart",
                        backgroundColor: [
                            'rgba(75, 192, 192, 1)',
                            'rgba(255, 99, 132, 1)',
                        ],
                        data: {!! json_encode($payments) !!}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Students Fees Chart'
                }
            }
        });
    </script>

@endsection
