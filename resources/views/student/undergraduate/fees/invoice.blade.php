@php
    $title=isset($title)? "Fees - $title":'Fees';
        $public='';
        if(config('app.env') == 'production')
           $public ='public';
        $data=session('userData');
        function reformatDate($date){
            if (date('YMd',strtotime($date)) == date('YMd')){
                return (date('H:i',strtotime($date)));
            } elseif (date('Y',strtotime($date)) == date('Y')){
                return (date('M d ',strtotime($date)));
            } elseif (date('Y',strtotime($date)) !== date('Y')){
                return (date('M d, Y',strtotime($date)));
            }

            return date('M d, Y',strtotime($date));
        }

        $totalDue=0;
        $type=in_array($invoice->invoice_id,$payments)?'Cleared':'Outstanding';
@endphp
@extends('layouts.global')
@section('title',$title)
@section('content')
    <div class="block block-fx-shadow">
        <div class="block-header block-header-default">
            <h3 class="block-title">Invoice - {{ $invoice->title}} <span
                        class="badge @if($type=="Cleared") badge-primary @else badge-danger @endif">{{$type}}</span>
            </h3>
            <div class="block-options">
                <button type="button" class="btn-block-option" onclick="Dashmix.helpers('print');">
                    <i class="si si-printer mr-1"></i> Print Invoice
                </button>
                @if($type!=="Cleared")
                    <a href="{{url('undergraduate/fees/payment/'.($invoice->id+331))}}" class="btn-block-option">
                        <i class="si si-credit-card mr-1"></i> Pay Now
                    </a>
                @endif
            </div>
        </div>
        <div class="block-content">
            <div class="p-sm-4 p-xl-7">
                @include('student.undergraduate.partials.invoiceList')


                @if($type=="Cleared")
                    <p class="text-muted text-center my-5">
                        Thanks for paying on time
                    </p>
                @else

                    <div class="text-center">
                        <a href="{{url('undergraduate/fees/payment/?id='.($invoice->id+331))}}"
                           class="btn btn-primary btn-lg">
                            <i class="si si-credit-card mr-1"></i> Pay Now
                        </a>
                    </div>
                    <p class="text-muted text-center my-5">
                        Please pay on time
                    </p>
                @endif

            </div>
        </div>
    </div>
@endsection
@section('script')

@endsection