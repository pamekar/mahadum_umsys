@php
    $title=isset($title)? "Fees - $title":'Fees';
    $public='';
    if(config('app.env') == 'production')
       $public ='public';
    $data=session('userData');
    function reformatDate($date){
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }
    $total=0;
    $singleInvoice=isset($_REQUEST['id'])?$_REQUEST['id']-331:null;
@endphp
@extends('layouts.global')
@section('title',$title)
@section('content')
    <div class="container width-full">
        <div class="col-12">
            <div class="block block-rounded block-bordered">
                <div class="row">
                    <div class="col-md-7 col-xs-12">
                        <div class="block-content block-content-full">
                            <div class="col-12 d-md-flex align-items-md-center">
                                <div class="py-4 py-md-0 text-center text-md-left invisible" data-toggle="appear">
                                    <h1 class="font-size-h2 mb-2 text-primary">
                                        Hello {{$data->user_meta->first_name}}</h1>
                                    @if($data->outstanding_fees>0)
                                        <p class="font-size-lg font-w400 text-muted mb-0">You have unpaid invoices worth
                                            <span
                                                    class="font-w700 text-danger">&#8358;{{number_format($data->outstanding_fees)}}</span>.<br>Please
                                            make the necessary payments to avoid restricted access to university
                                            services, and
                                            to gain full access to this portal.</p>
                                    @else
                                        <p class="font-size-lg font-w400 text-muted mb-0">Congratulations! You have no
                                            outstanding fees to pay.</p>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12">
                        <div class="block-content block-content-full">
                            <div class="block-content block-content-full">
                                <canvas id="payments-chart" height="75" width="100%"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="block block-rounded block-bordered">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Unpaid Invoices</h3>
                    <div class="block-options">
                        <div class="block-options-item">
                            <button type="button" onclick="pay()" class="btn btn-sm btn-outline-primary">Pay now
                            </button>
                        </div>
                    </div>
                </div>
                <div class="block-content">
                    <div class="table-responsive">
                        <form>
                            <table class="js-table-checkable table table-hover table-vcenter">

                                <thead>
                                <tr>
                                    <th class="text-center" style="width: 70px;">
                                        <div class="custom-control custom-checkbox d-inline-block">
                                            <input type="checkbox" class="custom-control-input invoice-check" amount="0"
                                                   id="check-all"
                                                   name="check-all">
                                            <label class="custom-control-label" for="check-all"></label>
                                        </div>
                                    </th>
                                    <th>Invoice Title</th>
                                    <th>Session</th>
                                    <th>Issuer</th>
                                    <th class="text-right">Amount</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($invoices as $invoice)
                                    <tr>
                                        <td class="text-center">
                                            <div class="custom-control custom-checkbox d-inline-block">
                                                <input type="checkbox"
                                                       class="custom-control-input invoice-check invoice"
                                                       amount="{{$invoice->amount/100}}"
                                                       id="invoice_{{$loop->iteration}}"
                                                       name="invoice_{{$loop->iteration}}"
                                                       invoice="{{$invoice->invoice_id}}"
                                                       @if(isset($singleInvoice)) @if($invoice->id==$singleInvoice) checked="true"
                                                       @endif @else checked="true" @endif>
                                                <label class="custom-control-label"
                                                       for="invoice_{{$loop->iteration}}"></label>
                                            </div>
                                        </td>
                                        <td>{{$invoice->title}}</td>
                                        <td>{{$invoice->session}}</td>
                                        <td>{{$invoice->issuer}}</td>
                                        <td class="text-right">&#8358;<span
                                                    class="invoice-amount">{{number_format($invoice->amount/100)}}</span>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                                <tr>
                                    <td colspan="4" class="font-w700 text-uppercase text-right bg-body-light">Total
                                    </td>
                                    <td class="font-w700 text-right bg-body-light text-right">&#8358;<span
                                                id="total">0</span></td>
                                </tr>
                                <tr>
                                    <td colspan="5">
                                        <div></div>
                                    </td>
                                </tr>
                            </table>
                        </form>
                    </div>
                </div>
                <div class="block-header block-header-default block-header-rtl">
                    <div class="block-options">
                        <div class="block-options-item">
                            <button type="button" onclick="pay()" class="btn btn-lg btn-outline-primary">Pay now
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal" id="invoice-modal" tabindex="-1" role="dialog" aria-labelledby="modal-block-large" style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0 block-rounded">
                    <div class="block-content" id="invoice-lines">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script>
        jQuery(function () {
            Dashmix.helpers(['table-tools-checkable', 'table-tools-sections']);
        });
        calculateTotalAmount();

        $('.invoice-check').change(function () {
            calculateTotalAmount();
        });

        function calculateTotalAmount() {
            let amount = 0;
            $('#total').text(amount);
            $('.invoice:checkbox:checked').each(function () {
                amount += Number($(this).attr('amount'));
            });
            $('#total').text(amount);
        }

        function pay() {
            let invoices = [];

            $('.invoice:checkbox:checked').each(function () {
                invoices.push($(this).attr('invoice'));
            });
            let data = {invoices: invoices};
            console.log(invoices[2]);
            $.get("{{url('/undergraduate/fees/payment/invoice')}}", data, function (result) {
                let content = $('#invoice-lines');
                $('#invoice-modal').modal('show');
                content.fadeOut(100);
                content.html('');
                content.hide();
                content.html(result.html);
                content.fadeIn(800);
            });

        }

    </script>
    <script src="{{asset("$public/js/Chart.min.js")}}"></script>
    <script type="text/javascript">
        var ctx = document.getElementById("payments-chart");
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ['Paid fees', 'Unpaid Debts'],
                datasets: [
                    {
                        label: "Students year Chart",
                        backgroundColor: [
                            'rgba(75, 192, 192, 1)',
                            'rgba(255, 99, 132, 1)',
                        ],
                        data: {!! json_encode($fees) !!}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Students Fees Chart'
                }
            }
        });
    </script>
@endsection