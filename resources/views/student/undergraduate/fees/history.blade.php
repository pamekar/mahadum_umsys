@php
    $title=isset($title)? "Fees - $title":'Fees';
    $public='';
    if(config('app.env') == 'production')
       $public ='public';
    $data=session('userData');
    function reformatDate($date){
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }

@endphp
@extends('layouts.global')
@section('title',$title)
@section('content')
    <div class="container width-full">
        <div class="col-12">
            <div class="block block-rounded block-bordered">
                <div class="row">
                    <div class="col-md-7 col-xs-12">
                        <div class="block-content block-content-full">
                            <div class="col-12 d-md-flex align-items-md-center">
                                <div class="py-4 py-md-0 text-center text-md-left invisible" data-toggle="appear">
                                    <h1 class="font-size-h2 mb-2 text-primary">
                                        Hello {{$data->user_meta->first_name}}</h1>
                                    @if($data->outstanding_fees>0)
                                        <p class="font-size-lg font-w400 text-muted mb-0">You have unpaid invoices worth
                                            <span
                                                    class="font-w700 text-danger">&#8358;{{number_format($data->outstanding_fees)}}</span>.<br>Please
                                            make the necessary payments to avoid restricted access to university
                                            services, and
                                            to gain full access to this portal.</p>
                                        <div class="p-3 text-center"><a href="{{url('/undergraduate/fees/payment')}}" class="btn btn-hero-primary">Pay
                                                Now</a>
                                        </div>
                                    @else
                                        <p class="font-size-lg font-w400 text-muted mb-0">Congratulations! You have no
                                            outstanding fees to pay.</p>
                                    @endif
                                </div>

                            </div>
                        </div>
                    </div>
                    <div class="col-md-5 col-xs-12">
                        <div class="block-content block-content-full">
                            <div class="block-content block-content-full">
                                <canvas id="payments-chart" height="75" width="100%"></canvas>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="block block-rounded block-bordered">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Invoices</h3>
                </div>
                <div class="block-content block-content-full">
                    <div class="table-responsive">
                        <table class="table table-striped table-hover">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Invoice Title</th>
                                <th>Session</th>
                                <th>Issuer</th>
                                <th>Amount</th>
                                <th>Date</th>
                                <th class="text-center">?</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($payments as $payment)
                                <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$payment->title}}</td>
                                    <td>{{$payment->session}}</td>
                                    <td>{{$payment->issuer}}</td>
                                    <td>&#8358;{{number_format($payment->amount/100)}}</td>
                                    <td>{{reformatDate($payment->created_at)}}</td>
                                    <td class="text-center">
                                        <div class="btn-group btn-group-sm" role="group"
                                             aria-label="Small Outline Primary">
                                            <a href="{{url('undergraduate/fees/payment/print/'.($payment->id+331))}}"
                                               class="btn btn-outline-success"><i class="fa fa-print mr-md-2"></i> Print</a>
                                        </div>
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{asset("$public/js/Chart.min.js")}}"></script>
    <script type="text/javascript">
        var ctx = document.getElementById("payments-chart");
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Paid fees', 'Unpaid Debts'],
                datasets: [
                    {
                        label: "Students year Chart",
                        backgroundColor: [
                            'rgba(75, 192, 192, 1)',
                            'rgba(255, 99, 132, 1)',
                        ],
                        data: {!! json_encode($fees) !!}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Students Fees Chart'
                }
            }
        });
    </script>
@endsection