<div class="block-content" id="results-data">

    <div class="" id="" style="">
        <h3 class="card-header blue" id="" style="">Showing Courses for {{$level}} level, <span class="green">{{$semester}}</span></h3>

        <div class="list-group notice" id="" style="">
            @foreach($courses as $course)
                @include('student.undergraduate.partials.courseList')
            @endforeach
        </div>
    </div>

</div>