<div class="">
    <div class="card-header blue width-full">
        <h3 class="blue float-left" id="header" style="">Showing Courses for <span
                    class="green">Harmattan Semester</span></h3>

        <div class="form-group form-inline float-right"><input class="" id="option" value="1" placeholder="" required=""
                                                               type="hidden">
            <select class="form-control" id="specify" name="semester">
                <option value="1">First Semester</option>

                <option value="2">Second Semester</option>
            </select>

            <button class="btn cool-button form-control" id="specify-btn" onclick="changeHTML()">Show</button>
        </div>
    </div>

    <h4 class="card-header blue width-full">100 Level</h4>

    <div class="list-group notice">
        @foreach($first as $course)
            @include('student.undergraduate.partials.courseList')
        @endforeach
    </div>

    <div class="divider"></div>

    <h4 class="card-header blue width-full">200 Level</h4>

    <div class="list-group notice">
        @foreach($second as $course)
            @include('student.undergraduate.partials.courseList')
        @endforeach
    </div>

    <h4 class="card-header blue width-full">300 Level</h4>

    <div class="list-group notice">
        @foreach($third as $course)
            @include('student.undergraduate.partials.courseList')
        @endforeach
    </div>

    <h4 class="card-header blue width-full">400 Level</h4>

    <div class="list-group notice">
        @foreach($fourth as $course)
            @include('student.undergraduate.partials.courseList')
        @endforeach
    </div>

    <h4 class="card-header blue width-full">500 Level</h4>

    <div class="list-group notice">
        @foreach($final as $course)
            @include('student.undergraduate.partials.courseList')
        @endforeach
    </div>
</div>
