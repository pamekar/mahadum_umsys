<div class="block-content" id="results-data">
    <div>
        <h3 class="card-header blue width-full">Showing Courses for <span class="green">{{$level}}
                level</span></h3>

        <h4 class="card-header blue width-full">First Semester</h4>

        <div class="list-group notice">
            @foreach($firstSemester as $course)
                @include('student.undergraduate.partials.courseList')
            @endforeach
        </div>

        <div class="divider"></div>

        <h4 class="card-header blue width-full">Second Semester</h4>

        <div class="list-group notice">
            @foreach($firstSemester as $course)
                @include('student.undergraduate.partials.courseList')
            @endforeach
        </div>
    </div>
</div>