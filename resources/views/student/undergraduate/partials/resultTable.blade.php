<?php
function grade($result)
{
    $grade = "Not Set";
    switch ($result->result_grade) {
        case ('0') :
            $grade = "F";
            break;
        case ('1') :
            $grade = "E";
            break;
        case ('2') :
            $grade = "D";
            break;
        case ('3') :
            $grade = "C";
            break;
        case ('4') :
            $grade = "B";
            break;
        case ('5') :
            $grade = "A";
            break;
    }
    return $grade;
}

function score($result)
{
    $score = null;
    if (is_null($result->result_lab))
        $score = $result->result_test + $result->result_exam;
    else
        $score = $result->result_test + $result->result_lab + $result->result_exam;
    return $score;
}

?>
<h3 class="card-header blue width-full text-center" id="" style="">First Semester</h3>

<table class="table table-hover" id="">
    <thead class="thead-default" id="" style="">
    <tr class="" id="" style="">
        <th class="" id="" style="">Course Code</th>

        <th class="" id="" style="">Course Units</th>

        <th class="" id="" style="">Score</th>

        <th class="" id="" style="">Grade</th>

        <th class="" id="" style="">Remarks</th>
    </tr>
    </thead>

    <tbody>
    @foreach($firstSemester as $result)
        <tr class="" id="" style="">
            <td class="" id="" style="">{{$result->course_code}}</td>

            <td class="" id="" style="">{{$result->course_units}}</td>

            <td class="" id="" style="">{{score($result)}}</td>

            <td class="" id="" style="">{{grade($result)}}</td>

            <td class="" id="" style="">{{$result->remarks}}</td>
        </tr>
    @endforeach
    </tbody>
</table>

<h3 class="card-header blue width-full text-center" id="" style="">Second Semester</h3>

<table class="table table-hover" id="">
    <thead class="thead-default" id="" style="">
    <tr class="" id="" style="">
        <th class="" id="" style="">Course Code</th>

        <th class="" id="" style="">Course Units</th>

        <th class="" id="" style="">Score</th>

        <th class="" id="" style="">Grade</th>

        <th class="" id="" style="">Remarks</th>
    </tr>
    </thead>

    <tbody>
    @foreach($secondSemester as $result)
        <tr class="" id="" style="">
            <td class="" id="" style="">{{$result->course_code}}</td>

            <td class="" id="" style="">{{$result->course_units}}</td>

            <td class="" id="" style="">{{score($result)}}</td>

            <td class="" id="" style="">{{grade($result)}}</td>

            <td class="" id="" style="">{{$result->remarks}}</td>
        </tr>
    @endforeach
    </tbody>
</table>


<div class="" id="" style="">
    <h3 class="card-header red width-full text-center" id="" style="">Result Summary</h3>

    <table class="table" id="">
        <tbody>
        <tr class="" id="" style="">
            <td class="" id="" style="">First Semester GPA</td>

            <td class="" id="" style="">{{$firstGPA}}</td>

            <td class="" id="" style=""></td>
        </tr>

        <tr class="" id="" style="">
            <td class="" id="" style="">Second Semester GPA</td>

            <td class="" id="" style="">{{$secondGPA}}</td>

            <td class="" id="" style=""></td>
        </tr>

        <tr class="" id="" style="">
            <td class="" id="" style="">{{$session.'/'.($session+1)}} Session GPA</td>

            <td class="" id="" style="">{{$sessionGPA}}</td>

            <td class="" id="" style=""></td>
        </tr>
        </tbody>
    </table>
</div>
