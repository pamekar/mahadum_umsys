<div class="col-md-5 col-xl-3">
    <div class="js-inbox-nav d-none d-md-block">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Navigation</h3>
            </div>
            <div class="block-content">
                <div class="list-group push">
                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center @if($title=='inbox') active @endif"
                       href="{{url('undergraduate/messages/inbox')}}">
                        <span><i class="fa fa-fw fa-inbox mr-3"></i> Inbox</span>
                        <span class="badge badge-pill badge-secondary msgcount" data-type="inbox"
                              id="count-inbox">{{$count->inbox}}</span>
                    </a>


                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center @if($title=='starred') active @endif"
                       href="{{url('undergraduate/messages/starred')}}">
                        <span><i class="fa fa-fw fa-star mr-3"></i> Starred</span>
                        <span class="badge badge-pill badge-secondary msgcount" data-type="starred"
                              id="count-starred">{{$count->starred}}</span>
                    </a>


                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center @if($title=='sent') active @endif"
                       href="{{url('undergraduate/messages/sent')}}">
                        <span><i class="fa fa-fw fa-send mr-3"></i> Sent</span>
                        <span class="badge badge-pill badge-secondary msgcount" data-type="sent"
                              id="count-sent">{{$count->sent}}</span>
                    </a>


                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center @if($title=='draft') active @endif"
                       href="{{url('undergraduate/messages/draft')}}">
                        <span><i class="fa fa-fw fa-save mr-3"></i> Draft</span>
                        <span class="badge badge-pill badge-secondary msgcount" data-type="draft"
                              id="count-draft">{{$count->draft}}</span>
                    </a>


                    <a class="list-group-item list-group-item-action d-flex justify-content-between align-items-center @if($title=='trash') active @endif"
                       href="{{url('undergraduate/messages/trash')}}">
                        <span><i class="fa fa-fw fa-trash mr-3"></i> Trash</span>
                        <span class="badge badge-pill badge-secondary msgcount" data-type="trash"
                              id="count-trash">{{$count->trash}}</span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="block d-none d-md-block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Recent Contacts</h3>
        </div>
        <div class="block-content block-content-full">
            <ul class="nav-items">
                @foreach($contacts as $contact)
                    <li>
                        <a href="javascript:void(0)" class="media py-2">
                            <div class="mx-3 overlay-container">
                                <img class="img-avatar" src="{{asset("$public/$contact->avatar")}}"
                                     alt="{{$contact->name}}">
                                <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                            </div>
                            <div class="media-body">
                                <div class="font-w600">{{$contact->name}}</div>
                                <div class="font-size-sm text-muted">
                                    {{$contact->type}}
                                </div>
                            </div>
                        </a>

                @endforeach
            </ul>
        </div>
    </div>
</div>