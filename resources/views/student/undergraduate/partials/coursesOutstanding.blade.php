<div class="block-content" id="results-data">
    <div>
        @if ( count ( $courses ) > 0 )
            <h3 class="blue width-full card-header" id="header" style="">Showing <span
                        class="green">Outstanding</span>
                Courses</h3>
            @php($temp="")
            <div class="list-group notice" id="" style="">
                @foreach ( $courses as $course )
                    @if($temp!= $course->session)
                        <h4 class="card-header blue width-full">{{$course->session . '/' . ( $course->session + 1 )}}
                            Academic Session</h4>
                    @endif
                    @include('student.undergraduate.partials.courseList')
                @endforeach
            </div>
        @else

            <h3 class="card-header green width-full" id="" style="">Congratulations, you have no outstanding
                course.</h3>

        @endif
    </div>
</div>