

    <div>
        <div class="card-header blue width-full">
            <h3 class="blue float-left">Showing Courses for <span class="green">{{$level}}
                    level</span></h3>

            <div class="form-level form-inline float-right"><input class="" id="option" value="500" placeholder=""
                                                                   required="" type="hidden">
                <select class="form-control" id="specify" name="level">
                    <option @if($level==100) selected @endif value="100">100 level</option>
                    <option @if($level==200) selected @endif value="200">200 level</option>
                    <option @if($level==300) selected @endif value="300">300 level</option>
                    <option @if($level==400) selected @endif value="400">400 level</option>
                    <option @if($level==500) selected @endif value="500">500 level</option>
                </select>

                <button class="btn cool-button form-control" id="specify-btn" onclick="changeHTML()">Show</button>
            </div>
        </div>

        <h4 class="card-header blue width-full">First Semester</h4>

        <div class="list-group notice">
            @foreach($firstSemester as $course)
                @include('student.undergraduate.partials.courseList')
            @endforeach
        </div>

        <div class="divider"></div>

        <h4 class="card-header blue width-full">Second Semester</h4>

        <div class="list-group notice">
            @foreach($secondSemester as $course)
                @include('student.undergraduate.partials.courseList')
            @endforeach
        </div>
    </div>
