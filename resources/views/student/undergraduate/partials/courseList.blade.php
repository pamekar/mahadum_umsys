<?php
if (!empty ($course->status))
    $status = $course->status;
else
    $status = "Not Registered";
if (strlen($course->lecturer) > 2)
    $lecturer = $course->lecturer;
else
    $lecturer = "Not Set";
if ($course->semester == 1)
    $semester = "Harmattan";
elseif ($course->semester == 2)
    $semester = "Rain";
switch ($course->grade) {
    case ('0') :
        $grade = "F";
        break;
    case ('1') :
        $grade = "E";
        break;
    case ('2') :
        $grade = "D";
        break;
    case ('3') :
        $grade = "C";
        break;
    case ('4') :
        $grade = "B";
        break;
    case ('5') :
        $grade = "A";
        break;
    default :
        $grade = "Not Set";
        break;
}
?>
<div class="list-group-item bghover">
    <h5 class="green notice-heading">{{$course->course_title}}
        <small>({{$course->course_code}})</small>
    </h5>

    <ul class="list-inline text-muted small">
        <li class="list-inline-item">Units: {{$course->units}}</li>

        <li class="list-inline-item">Semester: {{$semester}}</li>

        <li class="list-inline-item">Lecturer: {{$lecturer}}</li>

        <li class="list-inline-item">Status: {{$status}}</li>

        <li class="list-inline-item">Grade: {{$grade}}</li>
    </ul>

    <p class="text-justify small">{{$course->course_description}}</p>
</div>