<div class="list-group-item bghover">
    <h5 class="notice-heading">
        <a class="heading-link"
           href="{{url('/undergraduate/notices/view/'.$notice->id)}}"
        ><?php echo $notice->title;?></a>
    </h5>
    <ul class="list-inline text-muted">
        <li class="list-inline-item">
            Date: <?php echo date('M j, Y', strtotime($notice->created_at));?>
        </li>
        <li class="list-inline-item">
            Announcer: <?php echo $notice->admin_title?>
        </li>
    </ul>
    <article>{{trimText($notice->content)}}<a
                href="{{url('notices/view/'.$notice->id)}}"
                class="list-link"
        > ... read more</a>
    </article>
</div>