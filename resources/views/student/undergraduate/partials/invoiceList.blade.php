@php
    $totalDue=0;
    $public='';
    if(config('app.env') == 'production')
       $public ='public';
    $data=isset($data)? $data:session('userData');
@endphp
<h1 class="font-size-h2 mb-2 text-primary text-center">
    @if(isset($invoice))
        Invoice for {{ $invoice->title}}
        ({{"$invoice->session/".($invoice->session+1)}})
    @else
        Auto-generated Invoice
    @endif
</h1>
<div class="row mb-5">
    <div class="col-5">
        <p class="h3">{{config('app.short-name')}}</p>
        <address>
            Ibom Plaza,<br>
            Abak Rd,<br>
            Akwa Ibom State, 520101<br>
            contact@mahadum.com
        </address>
    </div>
    <div class="col-2">
        <img src="{{asset("$public/".$data->user_meta->photo_location)}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{$data->user_meta->first_name.' '. $data->user_meta->last_name}}" width="100%">
    </div>
    <div class="col-5 text-right">
        <p class="h3">{{$data->user_meta->first_name.' '. $data->user_meta->last_name}}</p>
        <address>
            {{$data->user_meta->reg_no}}<br>
            {{$data->user_meta->dept}}<br>
            {{$data->user_meta->faculty}}<br>
            {{$data->user_meta->email}}
        </address>
    </div>
</div>
<div class="table-responsive push">
    <table class="table table-bordered">
        <thead class="bg-body">
        <tr>
            <th class="text-center">#</th>
            <th class="text-center">Item</th>
            <th class="text-center">Qnt</th>
            <th class="text-right">Unit (&#8358;)</th>
            <th class="text-right">Amount (&#8358;)</th>
        </tr>
        </thead>
        <tbody>
        @foreach($lines as $line)
            @php
                $total=$line->amount/100*$line->quantity;
                $totalDue+=$total;
            @endphp
            <tr>
                <td class="text-center">{{$loop->iteration}}</td>
                <td>
                    <p class="font-w600 mb-1">{{$line->item}}</p>
                    <div class="text-muted">{{$line->description}}</div>
                </td>
                <td class="text-center"><span
                            class="badge badge-pill @if(isset($type) && $type=="Cleared") badge-primary @elseif(isset($type) && $type!=="Cleared") badge-danger @else badge-info @endif">{{$line->quantity}}</span>
                </td>
                <td class="text-right">{{number_format($line->amount/100)}}</td>
                <td class="text-right">{{number_format($total)}}</td>
            </tr>
        @endforeach
        <tr>
            <td colspan="4" class="font-w700 text-uppercase text-right bg-body-light">Total</td>
            <td class="font-w700 text-right bg-body-light text-right">&#8358;{{$totalDue}}</td>
        </tr>
        @if(isset($payment))
            <tr>
                <td colspan="5" class="text-center">
                    <a class="img-link img-thumb" href="{{url('fees/initialize/payment')}}">
                        <img src="{{asset("$public/png/paystack.png")}}" class="img" width="50%" alt="Pay via Paystack">
                    </a>
                    <a class="btn btn-outline-info btn-lg" href="{{url('fees/initialize/payment')}}">Pay via PayStack</a>
                </td>
            </tr>
        @endif
        </tbody>
    </table>
    <p class="text-muted text-center my-5">
        You will be charged <span
                class="font-w600 text-info">&#8358;{{number_format(session('invoice')->charge/100,2)}}</span> for
        processing this transaction.
    </p>
</div>
