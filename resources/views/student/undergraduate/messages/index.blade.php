@php
    $public='';
    if(config('app.env') == 'production')
        $public ='public';
    function getMsgDate($date){
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }

    function trimText($text,$length=50){
        $trimmedText = "";
        $content=preg_replace('/\s+/', ' ',strip_tags($text));;

        $content = explode(" ", $content);
        $i = 0;
        while (($i < $length && count($content) > $length)
            || ($i < count($content)
                && count($content) < $length)) {
            $trimmedText .=  $content [$i] . " ";
            $i++;
        }
        return $trimmedText;
    }
@endphp
@extends('layouts.global')
@section('title',title_case($title).' Messages')
@section('style')
    {{--    <link rel="stylesheet" href="{{asset($public.'/css/mail.css')}}">--}}
@endsection('style')
@section('content')
    <div class="content">
        {{--<style scoped>
            @include('student.undergraduate.partials.messageCSS')
        </style>--}}
        <div class="row">
            @include('student.undergraduate.partials.messageSidebar')
            <div class="col-md-7 col-xl-9">
                <div class="block">
                    <div class="block-header block-header-default">
                        <div class="block-title">
                            <strong><i class="fa fa-fw fa-inbox mr-3"></i> {{title_case($title)}}</strong>
                        </div>
                        <div class="block-options">
                            <a href="{{url('undergraduate/messages/compose')}}"
                               class="btn btn-lg btn-rounded btn-outline-success success float-right"
                               data-toggle="tooltip" data-placement="top" title=""
                               data-original-title="New Message"><i class="si si-pencil"></i>
                            </a>
                        </div>
                    </div>
                    <div class="block-content">
                        @if(!in_array($title,['draft','trash']))
                            <div class="push">
                                <button type="submit" class="btn btn-rounded btn-alt-secondary float-right"
                                        data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Delete Messages"
                                        name="action" value="trash" form="message-list-form">
                                    <i class="fa fa-times text-danger "></i>
                                    <span class="d-none d-sm-inline"> Delete</span>
                                </button>
                                <button type="submit" class="btn btn-rounded btn-alt-secondary" name="action"
                                        data-toggle="tooltip" data-placement="top" title=""
                                        data-original-title="Star Messages"
                                        value="star" form="message-list-form">
                                    <i class="fa fa-star text-warning mx-1"></i>
                                    <span class="d-none d-sm-inline"> Star</span>
                                </button>
                            </div>
                        @endif
                        <form action="{{url('/undergraduate/messages/action/selected')}}" id="message-list-form"
                              method="post">
                            {{csrf_field()}}
                            <table class="js-dataTable-full table table-hover table-vcenter table-sm">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th></th>
                                    <th>Name</th>
                                    <th>Message</th>
                                    <th>Date</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($messages as $message)
                                    <tr class="@if(!in_array(Auth::user()->name,explode(';',$message->seen))) table-warning @endif">
                                        <td class="text-center" style="width: 40px;">
                                            <label class="css-control css-control-primary css-checkbox">
                                                <input class="css-control-input" type="checkbox"
                                                       name="{{$message->id+120321}}" value="{{$message->message_id}}">
                                                <span class="css-control-indicator"></span>
                                            </label>
                                        </td>
                                        <td>@if(in_array(Auth::user()->name,explode(';',$message->starred)))
                                                <a href="javascript:void(0)" class="starred" onclick="starMessage(this)"
                                                   data-msgid="{{$message->message_id}}"><i
                                                            class="fa fa-star text-warning"></i></a>
                                            @else
                                                <a href="javascript:void(0)" class="starred" onclick="starMessage(this)"
                                                   data-msgid="{{$message->message_id}}"><i
                                                            class="si si-star text-warning"></i></a>
                                            @endif
                                        </td>
                                        <td class="d-none d-sm-table-cell font-w600"
                                            style="width: 140px;">@if($message->from==Auth::user()->name)
                                                <em>me</em> @else {{$message->name}} @endif</td>
                                        <td>
                                            <a class="font-w600"
                                               href="{{url("undergraduate/messages/view/$message->message_id")}}">{{$message->subject}}</a>
                                            <div class="text-muted ">{{ trimText($message->message,12) }}</div>
                                        </td>
                                        <td class="d-none d-xl-table-cell font-w600 font-size-sm text-muted"
                                            style="width: 120px;">
                                            {{ getMsgDate($message->created_at) }}
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@section('script')
    <script src="{{asset($public.'/js/jquery.datatables.min.js')}}"></script>
    <script src="{{asset($public.'/js/datatables.bootstrap4.min.js')}}"></script>
    <script src="{{asset($public.'/js/datatables.buttons.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.print.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.colvis.min.js')}}"></script>
    <script src="{{asset($public.'/js/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset($public.'/js/student/undergraduate/messages.js')}}"></script>
@endsection
