@php
    $public='';
    if(config('app.env') == 'production')
        $public ='public';
@endphp
@extends('layouts.global')
@section('title',"Reply | $message->subject")
@section('style')
    <link rel="stylesheet" href="{{asset($public.'/css/summernote-bs4.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap-tagsinput.css')}}">
    <style>
        .bootstrap-tagsinput {
            display: block;
            width: 100%;
            padding: 0.5rem 0.75rem;
            font-size: 1rem;
            line-height: 1.25;
            color: #464a4c;
            background-color: #fff;
            background-image: none;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.15);
            border-radius: 0.25rem;
            -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s,
            -webkit-box-shadow ease-in-out 0.15s;
        }

        .bootstrap-tagsinput::-ms-expand {
            background-color: transparent;
            border: 0;
        }

        .bootstrap-tagsinput:focus {
            color: #464a4c;
            background-color: #fff;
            border-color: #5cb3fd;
            outline: none;
        }

        .bootstrap-tagsinput::-webkit-input-placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput::-moz-placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput:-ms-input-placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput::placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput:disabled, .bootstrap-tagsinput[readonly] {
            background-color: #eceeef;
            opacity: 1;
        }

        .bootstrap-tagsinput:disabled {
            cursor: not-allowed;
        }
    </style>
@endsection('style')
@section('content')
    <div class="content">
        <div class="row">
            @include('student.undergraduate.partials.messageSidebar')
            <div class="col-md-7 col-xl-9">
                <div class="block">
                    <div class="block-header block-header-default">
                        <div class="block-title">
                            <strong><i class="si si-pencil mr-3"></i> Compose</strong>
                        </div>
                    </div>
                    <div class="block-content">
                        <form action="{{url('/undergraduate/messages/send/')}}" method="post"
                              id="message-form">
                            {{csrf_field()}}
                            <div class="block-content block-content-full">
                                <div class="form-group row">
                                    <label class="col-form-label" for="to" style="width:100%;"><span style="float:left">To</span><a
                                                href="javascript:void(0)" class="small" id="showcc" onclick="showCC()"
                                                style="float:right">CC/BCC</a></label>
                                    <input class="form-control" id="to" name="to" data-role="tagsinput" type="text"
                                           placeholder="Enter recipient(s)">
                                </div>

                                <div class="form-group row to-cc d-none">
                                    <label class="col-form-label" for="cc">CC</label>
                                    <input class="form-control" id="cc" name="cc"
                                           placeholder="Enter recipient(s)" type="text">
                                </div>

                                <div class="form-group row to-cc d-none">
                                    <label class="col-form-label" for="bcc">BCC</label>
                                    <input class="form-control" id="bcc" name="bcc"
                                           placeholder="Enter recipient(s)" type="text">
                                </div>
                                <div class="form-group row">
                                    <label class="col-form-label" for="subject">Subject</label>
                                    <input class="form-control" id="subject" name="subject"
                                           placeholder="Enter subject" type="text">
                                </div>
                                <div class="form-group">
                                    <div class="form-control" style="min-height: 8em;">
                                        <textarea title="Message" name="message" id="message-field"></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Send Message</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset($public.'/js/summernote-bs4.min.js')}}"></script>
    <script src="{{asset($public.'/js/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset($public.'/js/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset($public.'/js/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset($public.'/js/student/undergraduate/messages.js')}}"></script>
    <script>

        $('#message-field').summernote({
            airMode: true,
            placeholder: 'Write message here. Highlight to edit etxt...'
        });
        $('#message-form').on('submit', function (e) {
            e.preventDefault();
            Dashmix.layout('header_loader_on');

            let form = e.target;
            let data = new FormData(form);

            $.ajax({
                url: form.action,
                method: form.method,
                contentType: false,
                data: data,
                processData: false,
                success: function (result) {
                    Dashmix.layout('header_loader_off');
                    Dashmix.helpers('notify', {
                        align: 'center',
                        type: result.type,
                        icon: 'fa fa-check mr-1',
                        message: result.message
                    });
                    window.location.replace("/undergraduate/messages/");
                },
                error: function () {
                    Dashmix.layout('header_loader_off');
                    Dashmix.helpers('notify', {
                        align: 'center',
                        type: 'danger',
                        icon: 'fa fa-times mr-1',
                        message: 'Oops! Something went wrong..'
                    });
                }
            });

            return false;
        });

        var nodes = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('id'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '{!! url("/undergraduate/messages/suggest/nodes") !!}' + '?keyword=%QUERY%',
                wildcard: '%QUERY%'
            }
        });
        nodes.initialize();

        tagsInput('#to');

        function tagsInput(element) {

            $(element).tagsinput({
                itemValue: 'id',
                itemText: 'name',
                maxChars: 10,
                trimValue: true,
                allowDuplicates: false,
                freeInput: false,
                focusClass: 'form-control',
                tagClass: function (item) {
                    if (item.display)
                        return 'badge badge-' + item.display;
                    else
                        return 'badge badge-default';

                },
                onTagExists: function (item, $tag) {
                    $tag.hide().fadeIn();
                },
                typeaheadjs: [
                    {
                        hint: false,
                        highlight: true
                    },
                    {
                        name: 'to',
                        itemValue: 'id',
                        displayKey: 'name',
                        source: nodes.ttAdapter(),
                        templates: {
                            empty: [
                                '<ul class="list-group"><li class="list-group-item">Nothing found.</li></ul>'
                            ],
                            header: [
                                '<ul class="list-group">'
                            ],
                            suggestion: function (data) {
                                return '<li class="list-group-item"><span class="media py-2">' +
                                    '<div class="mx-3 overlay-container"><img class="img-avatar" src="/' + data.avatar + '" alt="">' +
                                    '</div><div class="media-body"><div class="font-w600">' + data.name + '</div>' +
                                    '<div class="font-size-sm text-muted">' + data.user_type + '</div></div></span></li>'
                            }
                        }
                    }
                ]
            });
        }

        function showCC() {
            $('.to-cc').toggleClass('d-none');
            tagsInput('#cc');
            tagsInput('#bcc');
        }

    </script>

@endsection
