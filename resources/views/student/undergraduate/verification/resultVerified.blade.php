<div class="" id="" style="">
    <h1 class="text-center green my-3" id="" style="font-size: 3em;">
        Result Verified <i class="fa fa-check" id=""></i>
    </h1>
    <h2 class="card-header blue width-full text-center" id="" style="">
        Showing results for <span class="green" id="currentSession">2015/2016</span>
        Academic Session
    </h2>

    <div class="media mx-4 mt-3" id="" style="">
        <div class="media-body text-left" id="" style="">
            <dl class="dl-horizontal small green" id="">
                <dt class="" id="">Name</dt>
                <dd class="text-muted" id="">{{$userDetails->first_name . ' ' .
					$userDetails->last_name}}</dd>
                <dt class="" id="">Reg No.</dt>
                <dd class="text-muted" id="">{{$userDetails->reg_no or 'Not set'}}</dd>
                <dt class="" id="">School</dt>
                <dd class="text-muted" id="">{{$userDetails->faculty or 'Not set'}}</dd>
                <dt class="" id="">Department</dt>
                <dd class="text-muted" id="">{{$userDetails->dept or 'Not set'}}</dd>
                <dt class="" id="">Level</dt>
                <dd class="text-muted" id="">{{$userDetails->level or 'Not set'}}</dd>
                <dt class="" id="">Cummulative GPA</dt>
                <dd class="text-muted" id="">{{$userDetails->cgpa or 'Not set'}}</dd>
                <dt class="" id="">Previous Session GPA</dt>
                <dd class="text-muted" id="">{{$userDetails->psgpa or 'Not set'}}</dd>
                <dt class="" id="">Outstanding Courses</dt>
                <dd class="text-muted" id="">{{$userDetails->outstanding_courses or
					'Not set'}}</dd>
            </dl>
        </div>
        <img src="{{asset($userDetails->photo_location)}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{$userDetails->name or 'Not set'}}"
        >
    </div>

    <div class="" id="area" style="">
        <h3 class="card-header blue width-full text-center" id="" style="">First
            Semester</h3>

        <table class="table table-hover" id="">
            <thead class="thead-default" id="" style="">
            <tr class="" id="" style="">
                <th class="" id="" style="">Course Code</th>
                <th class="" id="" style="">Course Units</th>
                <th class="" id="" style="">Score</th>
                <th class="" id="" style="">Grade</th>
                <th class="" id="" style="">Remarks</th>
            </tr>
            </thead>

            <tbody>
            <?php
            foreach ( $firstSemesterResult as $result )
            {
            switch ($result->result_grade) {
                case ('0') :
                    $grade = "F";
                    break;
                case ('1') :
                    $grade = "E";
                    break;
                case ('2') :
                    $grade = "D";
                    break;
                case ('3') :
                    $grade = "C";
                    break;
                case ('4') :
                    $grade = "B";
                    break;
                case ('5') :
                    $grade = "A";
                    break;
                default :
                    $grade = "Not Set";
                    break;
            }
            if (is_null($result->result_lab))
                $score = $result->result_test + $result->result_exam;
            else
                $score = $result->result_test + $result->result_lab + $result->result_exam;
            ?>
            <tr class="@if($grade=='F') table-danger @endif" id="" style="">
                <td class="" id="" style=""><?php echo $result->course_code?></td>
                <td class="" id="" style=""><?php echo $result->course_units?></td>
                <td class="" id="" style=""><?php echo $score?></td>
                <td class="" id="" style=""><?php echo $grade?></td>
                <td class="" id="" style=""><?php echo $result->remarks?></td>
            </tr>
            <?php }?>
            </tbody>
        </table>
        <h3 class="card-header blue width-full text-center" id="" style="">Second
            Semester</h3>
        <table class="table table-hover" id="">
            <thead class="thead-default" id="" style="">
            <tr class="" id="" style="">
                <th class="" id="" style="">Course Code</th>
                <th class="" id="" style="">Course Units</th>
                <th class="" id="" style="">Score</th>
                <th class="" id="" style="">Grade</th>
                <th class="" id="" style="">Remarks</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ( $secondSemesterResult as $result )
            {
            switch ($result->result_grade) {
                case ('0') :
                    $grade = "F";
                    break;
                case ('1') :
                    $grade = "E";
                    break;
                case ('2') :
                    $grade = "D";
                    break;
                case ('3') :
                    $grade = "C";
                    break;
                case ('4') :
                    $grade = "B";
                    break;
                case ('5') :
                    $grade = "A";
                    break;
                default :
                    $grade = "Not Set";
                    break;
            }
            if (is_null($result->result_lab))
                $score = $result->result_test + $result->result_exam;
            else
                $score = $result->result_test + $result->result_lab + $result->result_exam;
            ?>
            <tr class="@if($grade=='F') table-danger @endif" id="" style="">
                <td class="" id="" style=""><?php echo $result->course_code?></td>
                <td class="" id="" style=""><?php echo $result->course_units?></td>
                <td class="" id="" style=""><?php echo $score?></td>
                <td class="" id="" style=""><?php echo $grade?></td>
                <td class="" id="" style=""><?php echo $result->remarks?></td>
            </tr>

            <?php }?>
            </tbody>
        </table>
        <div class="" id="" style="">
            <h3 class="card-header red width-full text-center" id="" style="">Result
                Summary</h3>
            <table class="table" id="">
                <tbody>
                <tr class="" id="" style="">
                    <td class="" id="" style="">First Semester GPA</td>
                    <td class="" id="" style="">{{$firstGPA}}</td>
                </tr>
                <tr class="" id="" style="">
                    <td class="" id="" style="">Second Semester GPA</td>
                    <td class="" id="" style="">{{$secondGPA}}</td>
                </tr>
                <tr class="" id="" style="">
                    <td class="" id="" style="">2015/2016 Session GPA</td>
                    <td class="" id="" style="">{{$sessionGPA}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>