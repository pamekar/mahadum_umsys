<?php
if ( $html == 'register' )
{
?>
<div class="card my-3">
    <h1 class="card-header green text-center">Course Registration</h1>
    <div class="card-block">
        <p>The following instructions should guide you through this process:</p>
        <ul>
            <li>You have a total of&nbsp<span class="total-units bold"></span>&nbspunits
                to register.
            </li>
            <li>The minimum number of units you can register is&nbsp<span
                        class="min-units bold"
                ></span>&nbspunits.
            </li>
            <li>The maximum number of units you can register is&nbsp<span
                        class="max-units bold"
                ></span>&nbspunits.
            </li>
            <li>Your registration would be rejected if the selected course units
                exceeds the maximum number of units you can register
            </li>
            <li>When done with the selection, click on the button "Register"
                while it is green.
            </li>
            <li>Goodluck.</li>
        </ul>
    </div>
</div>
<div>
    <form method="POST" action="/undergraduate/courses/register"
          id="registration-form"
    >
        {{ method_field('PUT') }} {{ csrf_field() }}
        <table class="table table-hover text-medium card">
            <thead class="thead-default">
            <tr>
                <th class="text-center">Course Code</th>
                <th class="text-center">Course Title</th>
                <th class="text-center">Units</th>
                <th class="text-center">Type</th>
                <th class="text-center">Status</th>
                <th class="text-center">Select</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ( $courses as $course )
            {
            ?>
            <tr class="courses">
                <td class="text-center course-code">{{$course->course_code}}</td>
                <td class="course-title">{{$course->course_title}}</td>
                <td class="text-center units" id="units-{{$i}}">{{$course ->
						units}}</td>
                <td class="course-type">{{$course->course_type}}</td>
                <td class="status">{{$course->remarks or "Not Registered"}}</td>
                <td class="text-center"><input type="checkbox"
                                               name="register-{{$i}}" class="form-control register"
                                               value="{{$course->id}}"
                    /></td>
            </tr>
            <?php
            $i++;
            }
            ?>
            </tbody>
        </table>
        <table class="table text-medium card">
            <thead>
            <tr class="blue">
                <th>Total Number of Units</th>
                <th class="total-units"></th>
            </tr>
            <tr class="blue">
                <th>Max. Number of Units</th>
                <th class="max-units"></th>
            </tr>
            <tr class="blue">
                <th>Min. Number of Units</th>
                <th class="min-units"></th>
            </tr>
            <tr class="unit-monitor">
                <th>Total Selected Units</th>
                <th class="selected-units"></th>
            </tr>
            <tr class="unit-monitor">
                <th>Available Units</th>
                <th class="available-units"></th>
            </tr>
            </thead>
        </table>
        <div class="card p-4">
            <input type="submit" id="submit-form" value="Register"
                   style="width: 80%; margin: auto;"
            />
        </div>
    </form>
</div>

<?php
} elseif ( $html == 'registered' )
{
if ($approved)
    $approvalStatus = "Your course registration has been approved by your department";
else
    $approvalStatus = "Your course registration is yet to be approved by your department";
?>
<div class="card my-3">
    <h1 class="card-header green text-center">Your courses have been
        registered</h1>
    <div class="card-block text-left">
        <p class="bold">Please note:</p>
        <ul>
            <li>{{$approvalStatus}}</li>

            <li>You registered a total of&nbsp<span class="total-units bold"></span>&nbspunits
            </li>
            <li>You can add more valid courses to your list of selected courses
                within two weeks from the date of your initial registration, or
                before the approval by your department.
            </li>
            <li>You should also download and print a copy of of your course
                registration, which is be presented to your department. Click the
                print button at the bottom of this document to proceed.
            </li>
            <li>Below is a list of your registered courses.</li>
        </ul>
    </div>
</div>
<div>
    <table class="table table-hover text-medium card">
        <thead class="thead-default">
        <tr>
            <th class="text-center">Course Code</th>
            <th class="text-center">Course Title</th>
            <th class="text-center">Units</th>
            <th class="text-center">Type</th>
            <th class="text-center">Status</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ( $courses as $course )
        {
        ?>
        <tr class="courses">
            <td class="text-center course-code">{{$course->course_code}}</td>
            <td class="course-title">{{$course->course_title}}</td>
            <td class="text-center units" id="units-{{$i}}">{{$course->units}}</td>
            <td class="course-type">{{$course->course_type}}</td>
            <td class="status">{{$course->remarks or "Not Registered"}}</td>
        </tr>
        <?php
        $i++;
        }
        ?>
        </tbody>
    </table>

    <div class="card p-3" id="" style="">
        <div class="" id="" style="">
            @if($userCanRegister)

                <div class="pull-left m-1" id="" style="">
                    <a class="btn btn-success" id="download"
                       href="{{url('/undergraduate/courses/register/edit')}}" target=""
                    >Edit <i class="fa fa-edit ml-1" id=""></i>
                    </a>
                </div>
            @endif
            @if(isset($isRegistered)&& $isRegistered)
                <div class="pull-right m-1" id="" style="">
                    <a class="btn btn-success" id="print"
                       href="{{url('/undergraduate/download/registration/pdf')}}"
                       target="_blank"
                    >Print <i class="fa fa-file-pdf-o ml-1" id=""></i>
                    </a>
                </div>
            @endif
        </div>
    </div>
</div>

<?php
} elseif ( $html == 'edit' )
{
?>
<div class="card my-3">
    <h1 class="card-header green text-center">Add/Remove Courses</h1>
    <div class="card-block">
        <p>The following instructions should guide you through this process:</p>
        <ul>
            <li>You have a total of&nbsp<span class="total-units bold"></span>&nbspunits
                to register.
            </li>
            <li>The minimum number of units you can register is&nbsp<span
                        class="min-units bold"
                ></span>&nbspunits.
            </li>
            <li>The maximum number of units you can register is&nbsp<span
                        class="max-units bold"
                ></span>&nbspunits.
            </li>
            <li>Your registration would be rejected if the selected course units
                exceeds the maximum number of units you can register
            </li>
            <li>When done with the selection, click on the button "Register"
                while it is green.
            </li>
            <li class="bold red">Note that you can edit your course registration
                only ONCE
            </li>
        </ul>
    </div>
</div>
<div>
    <form method="POST" action="/undergraduate/courses/register"
          id="registration-form"
    >
        {{ method_field('patch') }} {{ csrf_field() }}
        <table class="table table-hover text-medium card">
            <thead class="thead-default">
            <tr>
                <th class="text-center">Course Code</th>
                <th class="text-center">Course Title</th>
                <th class="text-center">Units</th>
                <th class="text-center">Type</th>
                <th class="text-center">Status</th>
                <th class="text-center">Select</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $i = 0;
            foreach ( $courses as $course )
            {
            ?>
            <tr class="courses">
                <td class="text-center course-code">{{$course->course_code}}</td>
                <td class="course-title">{{$course->course_title}}</td>
                <td class="text-center units" id="units-{{$i}}">{{$course ->
						units}}</td>
                <td class="course-type">{{$course->course_type}}</td>
                <td class="status">{{$course->status or "Not Registered"}}</td>
                <td class="text-center"><input type="checkbox"
                                               name="register-{{$i}}" class="form-control register"
                                               value="{{$course->id}}"
                                               @if($course->remarks === "Registered") checked @endif /></td>
            </tr><?php
            $i++;
            }
            ?>
            </tbody>
        </table>
        <table class="table text-medium card">
            <thead>
            <tr class="blue">
                <th>Total Number of Units</th>
                <th class="total-units"></th>
            </tr>
            <tr class="blue">
                <th>Max. Number of Units</th>
                <th class="max-units"></th>
            </tr>
            <tr class="blue">
                <th>Min. Number of Units</th>
                <th class="min-units"></th>
            </tr>
            <tr class="unit-monitor">
                <th>Total Selected Units</th>
                <th class="selected-units"></th>
            </tr>
            <tr class="unit-monitor">
                <th>Available Units</th>
                <th class="available-units"></th>
            </tr>
            </thead>
        </table>
        <div class="card p-4">
            <input type="submit" id="submit-form" value="Register"
                   style="width: 80%; margin: auto;"
            />
        </div>
    </form>
</div>

<?php
}
?>
