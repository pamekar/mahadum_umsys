<div class="" id="" style="">
    <h2 class="width-full text-center" id="" style="">2016/2017 Academic
        Session Registration</h2>

    <div class="" id="" style="">
        <table class="" id="">
            <tr class="" id="" style="">
                <td class="" id="" style=""></td>

                <td class="" id="" style="" rowspan="8" align="center"
                    valign="middle"
                ><img
                            src="/build/images/global/avatars/_avatar_female_3-66dfb3e095.png"
                            class="" id="" alt="" style="" width="90pt"
                            height=100pt
                    /></td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">Name</td>

                <td class="" id="" style="">Nkwopara Mary</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">Reg No.</td>

                <td class="" id="" style="">20121025005</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">School</td>

                <td class="" id="" style="">SOSC</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">Department</td>

                <td class="" id="" style="">PHY</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">Level</td>

                <td class="" id="" style="">500</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">Session</td>

                <td class="" id="" style="">2016</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="30%">Semester</td>

                <td class="" id="" style="">Rain Semester</td>
            </tr>
        </table>
    </div>

    <h3 class="card-header blue width-full text-center" id="" style="">
        Registered
        Courses</h3>

    <div class="" id="area" style="">
        <table class="table table-hover" id="" cellpadding="6" width="100%">
            <thead class="" id="" style="" cellpadding="6">
            <tr class="thead-default" id="" style="">
                <th class="" id="" style="" width="14%">Code</th>

                <th class="" id="" style="" width="50%">Course Title</th>

                <th class="" id="" style="" width="8%">Units</th>

                <th class="" id="" style="" width="14%">Type</th>

                <th class="" id="" style="" width="14%">Status</th>
            </tr>
            </thead>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY502</td>

                <td class="" id="" style="" width="50%">Advanced Mathematical
                    Meethods in Physics
                </td>

                <td class="" id="" style="" width="8%">3</td>

                <td class="" id="" style="" width="14%">Compulsory</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY504</td>

                <td class="" id="" style="" width="50%">Classical Mechanics</td>

                <td class="" id="" style="" width="8%">3</td>

                <td class="" id="" style="" width="14%">Compulsory</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY506</td>

                <td class="" id="" style="" width="50%">Introduction to Chaotic
                    Dynamics
                </td>

                <td class="" id="" style="" width="8%">3</td>

                <td class="" id="" style="" width="14%">Compulsory</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY508</td>

                <td class="" id="" style="" width="50%">Nuclear Physics</td>

                <td class="" id="" style="" width="8%">3</td>

                <td class="" id="" style="" width="14%">Compulsory</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY510</td>

                <td class="" id="" style="" width="50%">Extragalatic
                    Astrophysica
                    and Cosmology
                </td>

                <td class="" id="" style="" width="8%">3</td>

                <td class="" id="" style="" width="14%">Elective</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY512</td>

                <td class="" id="" style="" width="50%">Energy Conversion</td>

                <td class="" id="" style="" width="8%">2</td>

                <td class="" id="" style="" width="14%">Elective</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="" id="" style="">
                <td class="" id="" style="" width="14%">PHY514</td>

                <td class="" id="" style="" width="50%">Advanced Thermal and
                    Statistical Physics
                </td>

                <td class="" id="" style="" width="8%">2</td>

                <td class="" id="" style="" width="14%">Compulsory</td>

                <td class="" id="" style="" width="14%">Registered</td>
            </tr>

            <tr class="thead-default" id="" style="">
                <td class="" id="" style="" width="14%"></td>

                <th class="" id="" style="" width="50%">Total Units</th>

                <th class="" id="" style="" width="8%">19</th>
            </tr>
        </table>
    </div>
</div>