@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.global') @if(!isset($pageTitle))
    $pageTitle = 'Courses' @endif @section('title',$pageTitle)
@section('style') @endsection('style') @section('content')
    <div class="container">
	<span class="text-center mt-5 mb-5 width-full">{!!$html or 'Whoops!
		looks like something went wrong'!!}</span>
    </div>
@endsection @section('script')
    <script type="text/javascript">

        <!--
        //-->
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        {{$script or "" }}
    </script>

@endsection
