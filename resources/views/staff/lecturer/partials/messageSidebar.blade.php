<div class="col-md-5 col-xl-3">
    <div class="js-inbox-nav d-none d-md-block">
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Navigation</h3>
            </div>
            <div class="block-content">
                <ul class="nav nav-pills flex-column push">
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center justify-content-between @if($title=='inbox') active @endif"
                           href="{{url('staff/lecturer/messages/inbox')}}">
                            <span><i class="fa fa-fw fa-inbox mr-5"></i> Inbox</span>
                            <span class="badge badge-pill badge-secondary msgcount" data-type="inbox"
                                  id="count-inbox">{{$count->inbox}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center justify-content-between @if($title=='starred') active @endif"
                           href="{{url('staff/lecturer/messages/starred')}}">
                            <span><i class="fa fa-fw fa-star mr-5"></i> Starred</span>
                            <span class="badge badge-pill badge-secondary msgcount" data-type="starred"
                                  id="count-starred">{{$count->starred}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center justify-content-between @if($title=='sent') active @endif"
                           href="{{url('staff/lecturer/messages/sent')}}">
                            <span><i class="fa fa-fw fa-send mr-5"></i> Sent</span>
                            <span class="badge badge-pill badge-secondary msgcount" data-type="sent"
                                  id="count-sent">{{$count->sent}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center justify-content-between @if($title=='draft') active @endif"
                           href="{{url('staff/lecturer/messages/draft')}}">
                            <span><i class="fa fa-fw fa-save mr-5"></i> Draft</span>
                            <span class="badge badge-pill badge-secondary msgcount" data-type="draft"
                                  id="count-draft">{{$count->draft}}</span>
                        </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link d-flex align-items-center justify-content-between @if($title=='trash') active @endif"
                           href="{{url('staff/lecturer/messages/trash')}}">
                            <span><i class="fa fa-fw fa-trash mr-5"></i> Trash</span>
                            <span class="badge badge-pill badge-secondary msgcount" data-type="trash"
                                  id="count-trash">{{$count->trash}}</span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="block d-none d-md-block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Recent Contacts</h3>
        </div>
        <div class="block-content block-content-full">
            <ul class="nav-items">
                @foreach($contacts as $contact)
                    <li>
                        <a href="javascript:void(0)" class="media py-2">
                            <div class="mx-3 overlay-container">
                                <img class="img-avatar" src="{{asset("$public/$contact->avatar")}}"
                                     alt="{{$contact->name}}">
                                <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                            </div>
                            <div class="media-body">
                                <div class="font-w600">{{$contact->name}}</div>
                                <div class="font-size-sm text-muted">
                                    {{$contact->type}}
                                </div>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
</div>