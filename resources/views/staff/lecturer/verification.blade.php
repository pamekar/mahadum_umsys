@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp
@extends('layouts.global') @if(!isset($pageTitle))
    $pageTitle = 'Courses' @endif @section('title',$pageTitle)
@section('style')

    <style>
        #body-content {
            padding: 1.5rem 0.5rem;
        }

        @media ( min-width: 768px) {
            .bootstrap-vertical-nav {
                margin-top: 50px;
            }
        }

        .card-header {
            display: inline-block;
        }

        #header {
            display: inline-block;
        }

        .list-inline-item {
            padding-left: 0.5rem;
            padding-right: 0.5rem;
            border-right: 1px dotted;
            line-height: 0.7rem;
        }

        .list-inline-item:last-child {
            border: none;
        }

        /* .nav-list { */
        /* 	padding: 1rem; */
        /* } */

        /* .nav-list-item>ul { */
        /* 	border-top: 1px dashed rgba(0, 0, 0, 0.1); */
        /* 	display: block; */
        /* 	list-style: outside none none; */
        /* 	margin: 0.5em 0px 0px; */
        /* 	padding: 0.5em 0px 0px; */
        /* 	font-size: 14px; */
        /* } */

        /* .nav-list-item>.list-link { */
        /* 	padding: 0.1rem; */
        /* 	color: #636c72; */
        /* 	display: block; */
        /* } */
        .nav-pills .nav-item.show .nav-link, .nav-pills .nav-link.active {
            color: #fff;
            background-color: #2c82b8;
            border-color: #2b80b5;
        }

        .nav-pills .nav-link.active:hover, .nav-pills .nav-link.active:active {
            color: #fff !important;
            background-color: #26709e;
            border-color: #2772a1;
        }

        .nav-pills .nav-link.active {
            color: #fff;
            background-color: #2c82b8;
            border-color: #2b80b5;
        }

        .sidebar {
            border-right: 1px solid rgb(240, 242, 241);
            float: left;
            padding: 0 2rem;
        }

        .sidebar > ul {
            list-style: outside none none;
            padding: 0px;
            margin: 0px;
        }

        .sidebar > ul > li {
            font-size: 16px;
            font-weight: 400;
            padding: 0px 0px 10px;
            margin: 1em 0px 0px;
        }

        .sidebar > ul > li > ul {
            border-top: 1px dashed rgba(0, 0, 0, 0.1);
            display: block;
            list-style: outside none none;
            margin: 0.5em 0px 0px;
            padding: 0.5em 0px 0px;
            font-size: 0.875em;
        }

        .sidebar a {
            line-height: 1.5;
            display: block;
        }

        .sidebar a:hover {
            color: #0B9C41 !important;
        }

        #side-bar {
            margin-bottom: 0.5rem;
        }
    </style>
@endsection('style')
@section('content')
    <div class="container">

        <div class="content content-full">
            <div class="d-flex flex-column flex-sm-row justify-content-sm-between align-items-sm-center">
                <h1 class="flex-sm-fill font-size-h2 font-w400 mt-2 mb-0 mb-sm-2">{{$pageTitle}}</h1>
                <nav class="flex-sm-00-auto ml-sm-3" aria-label="breadcrumb">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="{{url('home')}}"
                                                       class="list-link"
                            >Home</a></li>
                        <li class="breadcrumb-item"><a
                                    href="{{url('undergraduate/course')}}" class="list-link"
                            >Verification</a></li>
                        <li class="breadcrumb-item active" aria-current="page">{{$pageTitle or 'courses'}}</li>
                    </ol>
                </nav>
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12 block" id="body-content">
            <div class="block-content">
                <div class="block-content" id="results-data">
                    {!! $html !!}
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type="text/javascript">
        <!--
        //-->
        var domain = "<?php echo url(''); ?>";
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

@endsection
