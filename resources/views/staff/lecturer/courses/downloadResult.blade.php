<div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
    <form id="downloadForm" method="post" action="{{url($formAction)}}">
        <legend class="text-center green">{{$pageTitle}}</legend>
        <div class="form-group">
            <label for="sessions" class="text-muted">Session</label> <select
                    id="sessions" name="session" class="form-control"
                    required
                    @if(!isset($resultSession)) autofocus @endif
            >
                <optgroup label="">
                    <?php
                    if (isset($sessions) && sizeof($sessions) > 0) {
                    $i = 0;
                    foreach ($sessions as $session) {
                    $i++;
                    ?>
                    <option value="{{$session->session}}"
                    <?php
                        if (isset($resultSession) && $resultSession == $session->session)
                            echo "selected";
                        elseif (!isset($resultSession) && $i == 1)
                            echo "selected";
                        ?>
                    >{{$session->session.'/'.($session->session+1).' session'}}</option>
                    <?php
                    }
                    }
                    ?>
                </optgroup>
            </select>
        </div>
        <div class="form-group">
            <label for="course-codes" class="text-muted">Course code</label> <select
                    id="course-codes" name="courseCode" class="form-control"
                    placeholder="Course Code" required
            >
                <optgroup label="">
                    <?php
                    if (isset($courses) && sizeof($courses) > 0) {
                    $i = 0;
                    foreach ($courses as $code) {
                    $i++;
                    $courseCode = explode('_', $code->system_id);
                    ?>
                    <option value="{{$code->system_id }}"
                    <?php
                        if (isset($resultCourse) && $resultCourse == $code->system_id)
                            echo "selected";
                        elseif (!isset($resultCourse) && $i == 1)
                            echo "selected";
                        ?>
                    >{{$courseCode [ 0 ] . ' (' . $courseCode [ 3 ] . ')'}}</option>
                    <?php
                    }
                    }
                    ?>
                </optgroup>
            </select>
        </div>
        <div class="form-group">
            {{ csrf_field() }}
            <table class="table table-responsive text-center">
                <tr>
                    <td>
                        <button type="submit" name="fileType" value="xlsx" id="xlsx"
                                class="btn btn-success mx-3"
                        >
                            <i class="fa fa-2x fa-file-excel-o"></i>
                        </button>
                    </td>
                    <td>
                        <button type="submit" name="fileType" value="pdf" id="pdf"
                                class="btn btn-danger mx-3"
                        >
                            <i class="fa fa-2x fa-file-pdf-o"></i>
                        </button>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
