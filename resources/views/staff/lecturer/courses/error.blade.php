<div class="p-3 my-3" id="" style="">
    <h1 class="text-center red" id="" style="font-size: 3em;">
        {{title_case($type)}} Error<i class="fa fa-remove" id=""></i>
    </h1>
    <ul class="list-group list-unstyled my-3 card" id="" style="">
        <li class="list-group-item-heading strong p-3 text-left" id=""><strong>Reasons
                for this could be: </strong></li>
        <li class="list-group-item p-3 text-left" id="">You do not have
            permission to {{$type}} this document,
        </li>
        <li class="list-group-item p-3 text-left" id="">You may have {{$type}}ed a damaged document,
        </li>
        <li class="list-group-item p-3 text-left" id="">You may have {{$type}}ed a wrong file format, or
        </li>
        <li class="list-group-item p-3 text-left" id="">An uninterpretable
            error has occurred with your {{$type}}.
        </li>
        <li class="p-3 text-center red" id="">Please contact administrator on
            this error message.
        </li>
    </ul>
</div>