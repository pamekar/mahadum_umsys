<div class="" id="" style="">
    <h3 class="card-header blue width-full" id="" style="">Showing your courses</h3>
    @if(isset($departmentalCourses))
        @if( sizeof($departmentalCourses) > 0)
            @php
                $deptCourse[100]['firstSemester']=array();
                $deptCourse[100]['secondSemester']=array();
                $deptCourse[200]['firstSemester']=array();
                $deptCourse[200]['secondSemester']=array();
                $deptCourse[300]['firstSemester']=array();
                $deptCourse[300]['secondSemester']=array();
                $deptCourse[400]['firstSemester']=array();
                $deptCourse[400]['secondSemester']=array();
                $deptCourse[500]['firstSemester']=array();
                $deptCourse[500]['secondSemester']=array();
                    foreach ( $departmentalCourses as $course )
                    {
                        switch ( $course->semester )
                        {
                            case ( 1 ) :
                                array_push ( $deptCourse[$course->level]['firstSemester'], $course );
                                break;
                            case ( 2 ) :
                                array_push ( $deptCourse[$course->level]['secondSemester'], $course );
                                break;
                            default :
                                // array_push ( $firstSemester, $course );
                                break;
                        }
                    }
            @endphp
            <div class="list-group notice" id="" style="">
                @for($i = 100; $i<=500; $i+=100)
                    <h4 class="card-header blue width-full" id="" style="">{{$i}} Level 1<sup>st</sup> Semester Courses
                    </h4>
                    @if(sizeof($deptCourse[$i]['firstSemester'])>0)
                        @foreach($deptCourse[$i]['firstSemester'] as $course)
                            <div class="list-group-item bghover" id="" style="">
                                <h5 class="green notice-heading" id="" style="">{{$course->course_title}}
                                    <small class="" id="">({{$course->course_code}})
                                    </small> @if(Auth::user()->dept == $course->course_source) <em>*</em> @endif
                                </h5>

                                <ul class="list-inline text-muted small" id="" style="">
                                    <li class="list-inline-item" id="">Department: {{$course->course_source}}</li>
                                    <li class="list-inline-item" id="">Course Type: {{$course->course_type}}</li>
                                    <li class="list-inline-item" id="">Level: {{$course->level}} level</li>
                                    <li class="list-inline-item" id="">Semester: Harmattan</li>
                                    <li class="list-inline-item" id="">Units: {{$course->units}}</li>
                                </ul>

                                <p class="text-justify small" id="">{{$course->course_description}}</p>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-warning">
                            <h3 class="alert-heading">No {{$i}} Level 1<sup>st</sup> Semester courses to display</h3>
                            <p>Sorry, you have no {{$i}} level 1<sup>st</sup> semester course to be displayed at the
                                moment.</p>
                        </div>
                    @endif
                    <div class="divider" id="" style=""></div>
                    <h4 class="card-header blue width-full" id="" style="">{{$i}} Level 2<sup>nd</sup> Semester Courses
                    </h4>
                    @if(sizeof($deptCourse[$i]['secondSemester'])>0)
                        @foreach($deptCourse[$i]['secondSemester'] as $course)
                            <div class="list-group-item bghover" id="" style="">
                                <h5 class="green notice-heading" id="" style="">{{$course->course_title}}
                                    <small class="" id="">({{$course->course_code}})
                                    </small> @if(Auth::user()->dept == $course->course_source) <em>*</em> @endif
                                </h5>

                                <ul class="list-inline text-muted small" id="" style="">
                                    <li class="list-inline-item" id="">Department: {{$course->course_source}}</li>
                                    <li class="list-inline-item" id="">Course Type: {{$course->course_type}}</li>
                                    <li class="list-inline-item" id="">Level: {{$course->level}} level</li>
                                    <li class="list-inline-item" id="">Semester: Harmattan</li>
                                    <li class="list-inline-item" id="">Units: {{$course->units}}</li>
                                </ul>

                                <p class="text-justify small" id="">{{$course->course_description}}</p>
                            </div>
                        @endforeach
                    @else
                        <div class="alert alert-warning">
                            <h3 class="alert-heading">No {{$i}} Level 2<sup>nd</sup> Semester courses to display</h3>
                            <p>Sorry, you have no {{$i}} level 2<sup>nd</sup> semester course to be displayed at the
                                moment.</p>
                        </div>
                    @endif
                    <div class="divider" id="" style=""></div>
                @endfor
            </div>
        @else
            <div class="alert alert-warning">
                <h3 class="alert-heading">No Current Semester courses to display</h3>
                <p>Sorry, you have no current semester course to be displayed at the moment.</p>
            </div>
        @endif
    @endif
    @if(isset($currentSemester))
        @if( sizeof($currentSemester) > 0)
            <h4 class="card-header blue width-full" id="" style="">Current Semester</h4>
            <div class="list-group notice" id="" style="">
                @foreach($currentSemester as $course)
                    @php
                        if($course->semester == 1)
                            $semester = 'Harmattan';
                        elseif($course->semester == 2)
                            $semester = 'Rain';
                    @endphp
                    <div class="list-group-item bghover" id="" style="">
                        <h5 class="green notice-heading" id="" style="">{{$course->course_title}}
                            <small class="" id="">({{$course->course_code}})
                            </small> @if(Auth::user()->dept == $course->course_source) <em>*</em> @endif
                        </h5>

                        <ul class="list-inline text-muted small" id="" style="">
                            <li class="list-inline-item" id="">Department: {{$course->course_source}}</li>
                            <li class="list-inline-item" id="">Course Type: {{$course->course_type}}</li>
                            <li class="list-inline-item" id="">Level: {{$course->level}} level</li>
                            <li class="list-inline-item" id="">Semester: Harmattan</li>
                            <li class="list-inline-item" id="">Units: {{$course->units}}</li>
                        </ul>

                        <p class="text-justify small" id="">{{$course->course_description}}</p>
                    </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-warning">
                <h3 class="alert-heading">No Current Semester courses to display</h3>
                <p>Sorry, you have no current semester course to be displayed at the moment.</p>
            </div>
        @endif
    @endif
    @if(isset($firstSemester))

        @if( sizeof($firstSemester) > 0)
            <h4 class="card-header blue width-full" id="" style="">1<sup>st</sup> Semester</h4>

            <div class="list-group notice" id="" style="">
                @foreach($firstSemester as $course)
                    <div class="list-group-item bghover" id="" style="">
                        <h5 class="green notice-heading" id="" style="">{{$course->title}}
                            <small class="" id="">({{$course->course_code}})
                            </small> @if(Auth::user()->dept == $course->course_source) <em>*</em> @endif
                        </h5>

                        <ul class="list-inline text-muted small" id="" style="">
                            <li class="list-inline-item" id="">Department: {{$course->course_source}}</li>
                            <li class="list-inline-item" id="">Course Type: {{$course->course_type}}</li>
                            <li class="list-inline-item" id="">Level: {{$course->level}} level</li>
                            <li class="list-inline-item" id="">Semester: Harmattan</li>
                            <li class="list-inline-item" id="">Units: {{$course->units}}</li>
                        </ul>

                        <p class="text-justify small" id="">{{$course->course_description}}</p>
                    </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-warning">
                <h3 class="alert-heading">No 1<sup>st</sup> Semester courses to display</h3>
                <p>Sorry, you have no 1<sup>st</sup> semester course to be displayed at the moment.</p>
            </div>
        @endif
    @endif
    @if(isset($secondSemester))
        @if( sizeof($secondSemester) > 0)
            <div class="divider" id="" style=""></div>

            <h4 class="card-header blue width-full" id="" style="">2<sup>nd</sup> Semester</h4>

            <div class="list-group notice" id="" style="">
                @foreach($secondSemester as $course)
                    <div class="list-group-item bghover" id="" style="">
                        <h5 class="green notice-heading" id="" style="">{{$course->title}}
                            <small class="" id="">({{$course->course_code}})
                            </small> @if(Auth::user()->dept == $course->course_source) <em>*</em> @endif
                        </h5>

                        <ul class="list-inline text-muted small" id="" style="">
                            <li class="list-inline-item" id="">Department: {{$course->course_source}}</li>
                            <li class="list-inline-item" id="">Course Type: {{$course->course_type}}</li>
                            <li class="list-inline-item" id="">Level: {{$course->level}} level</li>
                            <li class="list-inline-item" id="">Semester: Harmattan</li>
                            <li class="list-inline-item" id="">Units: {{$course->units}}</li>
                        </ul>
                        <p class="text-justify small" id="">{{$course->course_description}}</p>
                    </div>
                @endforeach
            </div>
        @else
            <div class="alert alert-warning">
                <h3 class="alert-heading">No 2<sup>nd</sup> Semester courses to display</h3>
                <p>Sorry, you have no 2<sup>nd</sup> semester course to be displayed at the moment.</p>
            </div>
        @endif
    @endif
</div>
