<div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
    <form action="{{url('staff/lecturer/courses/attendance')}}">
        {{ csrf_field() }}
        <legend class="text-center green">{{$pageTitle}}</legend>
        <div class="form-group">
            <label for="sessions" class="text-muted">Session</label> <select
                    id="sessions" name="session" class="form-control"
            >
                <optgroup label="">
                    <?php
                    if ( isset ($sessions) && sizeof($sessions) > 1 )
                    {
                    foreach ( $sessions as $session )
                    {
                    ?>
                    <option value="{{$session->session}}"
                    <?php if ($sessionOpt == $session->session) echo "selected";?>
                    >{{$session->session.'/'.($session->session+1).' session'}}</option>
                    <?php
                    }
                    }
                    ?>
                </optgroup>
            </select>
        </div>
        <div class="form-group">
            <label for="course-codes" class="text-muted">Course code</label> <select
                    id="course-codes" name="courseCode" class="form-control"
                    placeholder="Course Code"
            >
                <optgroup label="">
                    <?php
                    if ( isset ($courses) && sizeof($courses) > 1 )
                    {
                    foreach ( $courses as $course )
                    {
                    ?>
                    <option value="{{$course->course}}"
                    <?php if ($courseOpt == $course->course) echo "selected";?>
                    >{{$course->course.'/'.($course->course+1).' course'}}</option>
                    <?php
                    }
                    }
                    ?>
                </optgroup>
            </select>
        </div>
        <div class="form-group">
            {{ csrf_field() }} <input type="hidden" value="download"
                                      name="action"
            > <input type="submit" class="btn cool-button" value="Download">
        </div>
    </form>
</div>
