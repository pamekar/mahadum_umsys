<div class="block block-rounded">
    <div class="block-content">
        <div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
            <form id="viewForm" method="post" action="{{url($formAction)}}">
                <legend class="text-center green">{{$pageTitle}}</legend>
                <div class="form-group">
                    <label for="sessions" class="text-muted">Session</label> <select
                            id="sessions" name="session" class="form-control"
                            required
                            @if(!isset($resultSession)) autofocus @endif
                    >
                        <optgroup label="">
                            <?php
                            if (isset($sessions) && sizeof($sessions) > 0) {
                            $i = 0;
                            foreach ($sessions as $session) {
                            $i++;
                            ?>
                            <option value="{{$session->session}}"
                            <?php
                                if (isset($resultSession) && $resultSession == $session->session)
                                    echo "selected";
                                elseif (!isset($resultSession) && $i == 1)
                                    echo "selected";
                                ?>
                            >{{$session->session.'/'.($session->session+1).' session'}}</option>
                            <?php
                            }
                            }
                            ?>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group">
                    <label for="course-codes" class="text-muted">Course code</label> <select
                            id="course-codes" name="courseCode" class="form-control"
                            placeholder="Course Code" required
                    >
                        <optgroup label="">
                            <?php
                            if (isset($courses) && sizeof($courses) > 0) {
                            $i = 0;
                            foreach ($courses as $code) {
                            $i++;
                            $courseCode = explode('_', $code->system_id);
                            ?>
                            <option value="{{$code->system_id }}"
                            <?php
                                if (isset($resultCourse) && $resultCourse == $code->system_id)
                                    echo "selected";
                                elseif (!isset($resultCourse) && $i == 1)
                                    echo "selected";
                                ?>
                            >{{$courseCode [ 0 ] . ' (' . $courseCode [ 3 ] . ')'}}</option>
                            <?php
                            }
                            }
                            ?>
                        </optgroup>
                    </select>
                </div>
                <div class="form-group">
                    {{ csrf_field() }} <input type="submit" class="btn cool-button"
                                              value="View"
                    >
                </div>
            </form>
        </div>
    </div>
</div>
<?php
if (isset($results) && isset($courseDetails)) {
if (sizeof($results) > 0 && isset($courseDetails)) {
?>
<div class="block block-rounded">

    <div class="block-content">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <h1 class="text-center">
                Grade Report <i class="fa fa-list-alt ml-3"></i>
            </h1>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-7 col-sm-6 col-xs-12 pull-left">
                    <table class="table small table-responsive table-sm table-hover">
                        <thead>
                        <tr>
                            <th>School of Student</th>
                            <td>{{$courseDetails ->course_destination_school}}</td>
                        </tr>
                        <tr>
                            <th>Student Dept.</th>
                            <td>{{$courseDetails->course_destination_dept}}</td>
                        </tr>
                        <tr>
                            <th>Title of course</th>
                            <td>{{$courseDetails ->course_title}}</td>
                        </tr>
                        <tr>
                            <th>School Offering Course</th>
                            <td>{{Auth::user()->school}}</td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-md-4 col-md-offset-1 col-sm-6 col-xs-12 pull-left">
                    <table class="table small table-sm table-hover">
                        <thead>
                        <tr>
                            <th>Semester</th>
                            <td><?php
                                if ($courseDetails->semester == 1)
                                    echo "Harmattan";
                                elseif ($courseDetails->semester == 2)
                                    echo "Rain";
                                ?>
                            </td>
                        </tr>
                        <tr>
                            <th>Session</th>
                            <td>{{$resultSession."/".($resultSession+1)}}</td>
                        </tr>
                        <tr>
                            <th>Course Code</th>
                            <td>{{ $courseDetails ->course_code}}</td>

                        </tr>
                        <tr>
                            <th>Units</th>
                            <td>{{$courseDetails->units}}</td>
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
            <div class="col-md-12 col-sm-12 col-xs-12">
                <table class="table table-sm mt-3 table-hover">
                    <thead class="thead-default">
                    <tr>
                        <th>S/No</th>
                        <th>Names</th>
                        <th>Reg. No.</th>
                        <th>Dept.</th>
                        <th>Test</th>
                        <th>Lab</th>
                        <th>Exam</th>
                        <th>Total</th>
                        <th>Grade</th>
                        <th>Remark</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php
                    $i = 0;
                    $a = $b = $c = $d = $e = $f = 0;
                    foreach ($results as $result) {
                    $i++;
                    switch ($result->result_grade) {
                        case (null):
                            $grade = "F";
                            $f++;
                            break;
                        case ('0'):
                            $grade = "F";
                            $f++;
                            break;
                        case ('1'):
                            $grade = "E";
                            $e++;
                            break;
                        case ('2'):
                            $grade = "D";
                            $d++;
                            break;
                        case ('3'):
                            $grade = "C";
                            $c++;
                            break;
                        case ('4'):
                            $grade = "B";
                            $b++;
                            break;
                        case ('5'):
                            $grade = "A";
                            $a++;
                            break;
                        default:
                            $grade = "Not Set";
                            break;
                    }
                    if (is_null($result->result_lab))
                        $score = $result->result_test + $result->result_exam;
                    else
                        $score = $result->result_test + $result->result_lab + $result->result_exam;
                    ?>


                    <tr class="@if($grade=='F') table-danger @endif" id="" style="">
                        <td>{{$i}}</td>
                        <td>{{$result->name}}</td>
                        <td>{{$result->student_reg_no}}</td>
                        <td>{{$result->student_dept}}</td>
                        <td>{{$result->result_test}}</td>
                        <td>{{$result->result_lab}}</td>
                        <td>{{$result->result_exam}}</td>
                        <td>{{$score}}</td>
                        <td>{{$grade}}</td>
                        <td>{{$result->remarks}}</td>

                    </tr>
                    <?php }?>
                    </tbody>
                </table>

            </div>
            <div class="row">
                <div class="col-md-7 col-sm-6 col-xs-12 pull-left">
                    <table class="table mt-3 table-sm text-center small">
                        <thead>
                        <tr>
                            <th colspan="2"><h3 class="text-muted">Result Summary</h3></th>
                        </tr>
                        <tr>
                            <th>A</th>
                            <td>{{$a}}</td>
                        </tr>
                        <tr>
                            <th>B</th>
                            <td>{{$b}}</td>
                        </tr>
                        <tr>
                            <th>C</th>
                            <td>{{$c}}</td>
                        </tr>
                        <tr>
                            <th>D</th>
                            <td>{{$d}}</td>
                        </tr>
                        <tr>
                            <th>E</th>
                            <td>{{$e}}</td>
                        </tr>
                        <tr>
                            <th>F</th>
                            <td>{{$f}}</td>
                        </tr>
                        </thead>
                    </table>
                </div>
                <div class="col-md-3 col-md-offset-2 col-sm-6 col-xs-12">
                    <form id="downloadForm" method="post" action="{{url('/staff/lecturer/courses/results/download')}}">
                        {{ csrf_field() }}
                        <input type="hidden" name="session" value="{{$resultSession}}">
                        <input type="hidden" name="courseCode" value="{{$resultCourse}}">

                        <table class="table mt-3 table-sm text-center">
                            <tr>
                                <th colspan="1"><h3 class="text-muted text-center">Download</h3></th>
                            </tr>
                            <tr>
                                <td class="p-3">
                                    <button type="submit" name="fileType" value="xlsx" id="xlsx"
                                            class="btn btn-success mx-3"
                                    >
                                        <i class="fa fa-2x fa-file-excel-o"></i>
                                    </button>
                                </td>
                            </tr>
                            <tr>
                                <td class="p-3">
                                    <button type="submit" name="fileType" value="pdf" id="pdf"
                                            class="btn btn-danger mx-3"
                                    >
                                        <i class="fa fa-2x fa-file-pdf-o"></i>
                                    </button>
                                </td>
                            </tr>
                        </table>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<?php
} else {
?>
<div class="block block-rounded">
    <div class="block-content">
        <div class="p-3" id="" style="">
            <h1 class="text-center text-danger" id="" style="font-size: 3em;">
                No records found <i class="fa fa-user-times ml-3" id=""></i>
            </h1>
        </div>
    </div>
</div>

<?php
}
}
?>