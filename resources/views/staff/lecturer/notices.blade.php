@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.global')
@section('title','Notices') @section('style')
    <?php
    $start_from = ($page - 1) * $limit;
    $showing_from = $start_from + 1;
    $showing_to = $start_from + $limit;
    $total_results = $result_num; // total number of results returned from the search query
    $total_pages = ceil($total_results / $limit);
    if ($showing_to >= $total_results)
        $showing_to = $total_results;
    ?>
    <style>
        .notice .list-inline {
            margin-bottom: 0.5rem;
            font-size: 90%;
        }

        #paginate {
            display: inline-block;
        }

        #paginate i {
            font-size: 1.25rem;
            font-weight: 600;
        }

        .page-item.active .page-link {
            z-index: 2;
            color: #fff !important;
            background-color: #2c82b8 !important;
            border-color: #2c82b8 !important;
        }

        .page-item.disabled .page-link {
            color: #0275d8;
            pointer-events: none;
            cursor: not-allowed;
            background-color: #fff;
            border-color: #ddd;
        }

        .page-link {
            color: #2c82b8 !important;
        }

        .page-link:focus, .page-link:hover {
            color: #3191cc !important;
            text-decoration: none;
            background-color: #eceeef !important;
            border-color: #ddd;
        }

        #results-showing {
            padding: 1.0rem 1.5rem;
        }

        #body-content {
            padding: 1.5rem 0.5rem;
            margin: 0.5rem 0rem;
        }
    </style>

@section('content')
    <div class="container">
        <div class="row">
            <div id="navigation" class="col-sm-12 col-xs-12 no-margin no-padding">
                <ul class="breadcrumb width-full">
                    <li class="breadcrumb-item"><a href="{{url('home')}}"
                                                   class="list-link"
                        >Home</a></li>
                    <li class="breadcrumb-item active">Notices</li>
                </ul>
            </div>
            <div class="col-md-12 col-xs-12 card" id="body-content">
                <h3 class="card-header blue">Memo/Notices</h3>
                <div id="results-showing">
				<span class="pull-left blue">Showing <strong><?php echo $showing_from?></strong>
					to <strong><?php echo $showing_to?></strong> of <strong><?php echo $total_results?></strong>
					Announcements
				</span>
                    <form id="set-limit" action="{{ url('/notices') }}"
                          class="pull-right" method="POST"
                    >
                        {{ csrf_field() }}
                        <ul class="list-inline">
                            <li class=" list-inline-item"><select class="form-control"
                                                                  name="limit" id="user-category"
                                >
                                    <option value="5" <?php if ($limit == 5) echo "selected";?>>5
                                        per page
                                    </option>
                                    <option value="10" <?php if ($limit == 10) echo "selected";?>>10
                                        per page
                                    </option>
                                    <option value="20" <?php if ($limit == 20) echo "selected";?>>20
                                        per page
                                    </option>
                                    <option value="30" <?php if ($limit == 30) echo "selected";?>>30
                                        per page
                                    </option>
                                    <option value="50" <?php if ($limit == 50) echo "selected";?>>50
                                        per page
                                    </option>
                                    <option value="75" <?php if ($limit == 75) echo "selected";?>>75
                                        per page
                                    </option>
                                    <option value="100" <?php if ($limit == 100) echo "selected";?>>100
                                        per page
                                    </option>
                                </select></li>
                            <li class=" list-inline-item">
                                <button type="submit"
                                        class=" btn btn-info
								form-control cool-button"
                                        form="set-limit"
                                >
                                    <i class="fa fa-repeat"></i>
                                </button>
                            </li>
                        </ul>

                    </form>
                </div>
                <div class="row">
                    <div class="col-md-12 col-xs-12 ">
                        <div class="wrapper">
                            <div class="list-group">
                                <?php
                                foreach ( $notices as $notice )
                                {
                                $info = "";
                                $html_elements = array(
                                    "<p>",
                                    "</p>",
                                    "<ul>",
                                    "</ul>",
                                    "<ol>",
                                    "</ol>",
                                    "<li>",
                                    "</li>",
                                    "<h1>",
                                    "</h1>",
                                    "<h2>",
                                    "</h2>",
                                    "<h3>",
                                    "</h3>",
                                    "<br>"
                                );
                                $content = $notice->content;
                                $content = str_replace($html_elements, "", $content);
                                $content = explode(" ", $content);
                                $i = 0;
                                while (($i < 50 && count($content) > 50)
                                    || ($i < count($content)
                                        && count($content) < 50)) {
                                    $info = $info . $content [$i] . " ";
                                    $i++;
                                }
                                ?>

                                <div class="list-group-item notice bghover">
                                    <h5 class="notice-heading">
                                        <a class="heading-link"
                                           href="{{url('notices/view/'.$notice->id)}}"
                                        ><?php echo $notice->title;?></a>
                                    </h5>
                                    <ul class="list-inline text-muted">
                                        <li class="list-inline-item">
                                            Date: <?php echo date('M j, Y', strtotime($notice->created_at));?>
                                        </li>
                                        <li class="list-inline-item">
                                            Announcer: <?php echo $notice->admin_title?>
                                        </li>
                                    </ul>
                                    <article><?php echo $info;?><a
                                                href="{{url('notices/view/'.$notice->id)}}"
                                                class="list-link"
                                        > ... read more</a>
                                    </article>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="card-footer text-center">
                                <div id="paginate">
                                    <ul class="pagination">
                                        <?php
                                        if ( $page == 1 )
                                        {
                                        ?>
                                        <li class="page-item"><span class="disabled page-link"
                                                                    data-toggle="tooltip" data-placement="top"
                                                                    title="Previous"
                                            ><i class="fa fa-arrow-left"></i></span></li>
                                        <?php
                                        } else
                                        {
                                        $previous = $page - 1;
                                        ?>
                                        <li class="page-item"><a href="{{url('notices/'.$previous)}}"
                                                                 class="page-link" data-toggle="tooltip"
                                                                 data-placement="top"
                                                                 title="Previous"
                                            ><i class="fa fa-arrow-left"></i></a></li>
                                        <?php
                                        }
                                        for ( $i = 1; $i <= $total_pages; $i++  )
                                        {
                                        if ( $page == $i )
                                        {
                                        ?>
                                        <li class="active page-item"><span class="disabled page-link">{{$i}}</span></li>
                                        <?php
                                        } else
                                        {
                                        ?>
                                        <li class="page-item"><a href="{{url('notices/'.$i)}}"
                                                                 class="page-link"
                                            >{{$i}}</a></li>
                                        <?php
                                        }
                                        }
                                        if ( $page == $total_pages )
                                        {
                                        ?>
                                        <li class="page-item"><span class="disabled page-link"><i
                                                        class="fa fa-arrow-right" data-toggle="tooltip"
                                                        data-placement="top" title="Next"
                                                ></i></span></li>
                                        <?php
                                        } else
                                        {
                                        $next = $page + 1;
                                        ?>
                                        <li class="page-item"><a href="{{url('notices/'.$next)}}"
                                                                 class="page-link" data-toggle="tooltip"
                                                                 data-placement="top"
                                                                 title="Next"
                                            ><i class="fa fa-arrow-right"></i></a></li>
                                        <?php }?>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection @section('script')
    <script type="text/javascript">
        <!--

        //-->
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $(function () {
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>

@endsection
