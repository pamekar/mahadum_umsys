@php
    $public='';
    if(config('app.env') == 'production')
        $public ='public';

@endphp
@extends('layouts.global')
@section('title', title_case($title).' | Messages')
@section('style')
@endsection('style')
@section('content')
    <div class="content">
        <div class="row">
            @include('student.undergraduate.partials.messageSidebar')
            <div class="col-md-7 col-xl-9">
                <div class="block">
                    <div class="block-header block-header-default">
                        <div class="block-title">
                            <strong>{{title_case($title)}}</strong>
                        </div>
                        <div class="block-options">
                            <a href="{{url("staff/lecturer/messages/forward/$message->message_id")}}"
                               data-toggle="tooltip" data-placement="top" title=""
                               data-original-title="Forward"
                               class="btn btn-rounded btn-outline-success success float-right mr-3">
                                <i class="si si-action-redo"></i>
                            </a>
                            <a href="{{url("staff/lecturer/messages/reply_all/$message->message_id")}}"
                               data-toggle="tooltip" data-placement="top" title=""
                               data-original-title="Reply All"
                               class="btn btn-rounded btn-outline-success success float-right mr-3">
                                <i class="fa fa-reply-all"></i>
                            </a>
                            <a href="{{url("staff/lecturer/messages/reply/$message->message_id")}}"
                               data-toggle="tooltip"
                               data-placement="top" title=""
                               data-original-title="Reply"
                               class="btn btn-rounded btn-outline-success success float-right mr-3">
                                <i class="fa fa-reply"></i>
                            </a>
                            <a href="{{url('staff/lecturer/messages/compose')}}"
                               class="btn btn-rounded btn-outline-success success float-right mr-3"
                               data-toggle="tooltip" data-placement="top" title=""
                               data-original-title="New Message"><i class="si si-pencil"></i>
                            </a>

                        </div>
                    </div>
                    <div class="block-content">
                        <div class="float-right">
                            <button type="button" class="btn btn-rounded btn-alt-secondary btn-lg" data-toggle="tooltip"
                                    data-placement="top" title="" data-original-title="Star Message"
                                    onclick="starMessage(this)" data-msgid="{{$message->message_id}}">
                                @if(in_array(Auth::user()->name,explode(';',$message->starred)))
                                    <i class="fa fa-star text-warning mx-1"></i>
                                @else
                                    <i class="si si-star text-warning mx-1"></i>
                                @endif
                            </button>
                            <button type="button" class="btn btn-rounded btn-alt-secondary btn-lg" data-toggle="tooltip"
                                    data-placement="top" title=""
                                    data-original-title="@if(in_array(Auth::user()->name,explode(';',$message->trashed))) Move to inbox @else Delete Message @endif"
                                    onclick="trashMessage(this)" data-msgid="{{$message->message_id}}">
                                @if(in_array(Auth::user()->name,explode(';',$message->trashed)))
                                    <i class="fa fa-arrow-alt-circle-right text-info"></i>
                                @else
                                    <i class="fa fa-trash-alt text-danger"></i>
                                @endif
                            </button>
                        </div>
                        <div class="media py-2">
                            <div class="mx-3 overlay-container">
                                <img class="img-avatar" src="{{asset("$public/$message->avatar")}}" alt="">
                                <span class="overlay-item item item-tiny item-circle border border-2x border-white bg-success"></span>
                            </div>
                            <div class="media-body">
                                <div class="font-size-sm text-muted">From</div>
                                <div class="font-w600">@if($message->from==Auth::user()->name)
                                        <em>me</em> @else {{$message->name}} @endif</div>
                                <div class="font-size-sm text-muted">
                                    {{date('M d,Y H:i:s',strtotime($message->created_at))}}
                                </div>
                                <button class="btn btn-link" data-toggle="popover" data-trigger="click"
                                        data-placement="bottom"
                                        title="" data-html="true"
                                        data-content="<table class='table table-sm font-size-sm'><tr><td class='text-muted'>subject</td><td class='font-w600'>{{$message->subject}}</td></tr><tr><td class='text-muted'>from:</td><td class='font-w600'>{{"$message->name"}}</td></tr><tr><td class='text-muted'>to</td><td class='font-w600'>{{$to or null}}</td></tr><tr><td class='text-muted'>cc</td><td class='font-w600'>{{$cc or null}}</td></tr><tr><td class='text-muted'>bcc</td><td class='font-w600'>{{$bcc or null}}</td></tr><tr><td class='text-muted'>date</td><td class='font-w600'>{{date('M d,Y',strtotime($message->created_at))}}</td></tr></table>"
                                        data-original-title="">View Details <i
                                            class="fa fa-caret-down"></i>
                                </button>
                            </div>
                        </div>
                        <div class="block block-bordered block-rounded">
                            <div class="block-content">
                                {!! $message->message !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset($public.'/js/jquery.datatables.min.js')}}"></script>
    <script src="{{asset($public.'/js/datatables.bootstrap4.min.js')}}"></script>
    <script src="{{asset($public.'/js/datatables.buttons.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.print.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.flash.min.js')}}"></script>
    <script src="{{asset($public.'/js/buttons.colvis.min.js')}}"></script>
    <script src="{{asset($public.'/js/be_tables_datatables.min.js')}}"></script>
    <script src="{{asset($public.'/js/staff/lecturer/messages.js')}}"></script>
@endsection
