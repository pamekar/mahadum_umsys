@php
    $public='';
    if(config('app.env') == 'production')
        $public ='public';
@endphp
@extends('layouts.global')
@section('title', "Compose Message")
@section('style')
    <link rel="stylesheet" href="{{asset($public.'/css/summernote-bs4.css')}}">
    <link rel="stylesheet" href="{{asset($public.'/css/bootstrap-tagsinput.css')}}">
    <style>
        .bootstrap-tagsinput {
            display: block;
            width: 100%;
            padding: 0.25rem 0.5rem;
            font-size: 1rem;
            line-height: 1.2;
            color: #464a4c;
            background-color: #fff;
            background-image: none;
            -webkit-background-clip: padding-box;
            background-clip: padding-box;
            border: 1px solid rgba(0, 0, 0, 0.15);
            border-radius: 0.2rem;
            -webkit-transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, -webkit-box-shadow ease-in-out 0.15s;
            -o-transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s;
            transition: border-color ease-in-out 0.15s, box-shadow ease-in-out 0.15s,
            -webkit-box-shadow ease-in-out 0.15s;
        }

        .bootstrap-tagsinput::-ms-expand {
            background-color: transparent;
            border: 0;
        }

        .bootstrap-tagsinput:focus {
            color: #464a4c;
            background-color: #fff;
            border-color: #5cb3fd;
            outline: none;
        }

        .bootstrap-tagsinput::-webkit-input-placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput::-moz-placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput:-ms-input-placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput::placeholder {
            color: #636c72;
            opacity: 1;
        }

        .bootstrap-tagsinput:disabled, .bootstrap-tagsinput[readonly] {
            background-color: #eceeef;
            opacity: 1;
        }

        .bootstrap-tagsinput:disabled {
            cursor: not-allowed;
        }

        .bootstrap-tagsinput .twitter-typeahead {
            display: inline !important;
        }


    </style>
@endsection('style')
@section('content')
    <div class="content">
        {{--<style scoped>
            @include('student.undergraduate.partials.messageCSS')
        </style>--}}
        <div class="row">
            @include('student.undergraduate.partials.messageSidebar')
            <div class="col-md-7 col-xl-9">
                <div class="block">
                    <div class="block-header block-header-default">
                        <div class="block-title">
                            <strong><i class="si si-pencil mr-3"></i> Compose</strong>
                        </div>
                    </div>
                    <div class="block-content">
                        <form action="{{url('/staff/lecturer/messages/send/')}}" method="post"
                              id="message-form">
                            {{csrf_field()}} @if(isset($message_id)) <input type="hidden" name="message_id"
                                                                            value="{{$message_id}}">@endif
                            <div class="block-content block-content-full">
                                <table class="w-100">
                                    <tr>
                                        <td colspan="2">

                                            <div class="btn-group">
                                                <button type="submit" class="btn btn-sm btn-primary send-message">
                                                    <i class="fa fa-paper-plane"></i> Send Message
                                                </button>
                                                <button type="button" class="btn btn-sm btn-default save-message">
                                                    <i class="fa fa-save"></i> Save Message
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger discard-message">
                                                    <i class="fa fa-trash-alt"></i> Discard
                                                </button>
                                            </div>
                                            <a
                                                    href="javascript:void(0)" class="small" id="showcc"
                                                    onclick="showCC()"
                                                    style="float:right">CC/BCC</a></td>
                                    </tr>
                                    <tr>
                                        <td><label class="col-form-label" for="to" style="width:100%;"><span
                                                        style="float:left">To</span></label></td>
                                        <td class="w-100"><input class="form-control" id="to" name="to"
                                                                 data-role="tagsinput"
                                                                 type="text" required
                                                                 placeholder="Enter recipient(s)" autofocus tabindex="1">
                                    </tr>

                                    <tr>
                                        <td><label class="col-form-label" for="cc">CC</label></td>
                                        <td class="w-100"><input class="form-control" id="cc" name="cc"
                                                                 placeholder="Enter recipient(s)" type="text"></td
                                    </tr>

                                    <tr>
                                        <td><label class="col-form-label" for="bcc">BCC</label></td>
                                        <td class="w-100"><input class="form-control" id="bcc" name="bcc"
                                                                 placeholder="Enter recipient(s)" type="text"></td
                                    </tr>
                                    <tr>
                                        <td><label class="col-form-label" for="subject">Subject</label></td>
                                        <td class="w-100"><input class="form-control" id="subject" name="subject"
                                                                 placeholder="Enter subject" type="text" required tabindex="2"
                                                                 @if(isset($subject)) value="{{$subject}}" @endif>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="form-control" style="min-height: 8em;">
                                        <textarea title="Message" name="message" id="message-field" tabindex="3">
                                            @if(isset($message))
                                                {!! $message !!}
                                            @endif
                                        </textarea>

                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <div class="btn-group" style="float:right;">
                                                <div class="btn-group">
                                                    <button type="submit" class="btn btn-sm btn-primary send-message">
                                                        <i class="fa fa-paper-plane"></i> Send Message
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-default save-message">
                                                        <i class="fa fa-save"></i> Save Message
                                                    </button>
                                                    <button type="button" class="btn btn-sm btn-danger discard-message">
                                                        <i class="fa fa-trash-alt"></i> Discard
                                                    </button>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script src="{{asset($public.'/js/summernote-bs4.min.js')}}"></script>
    <script src="{{asset($public.'/js/typeahead.bundle.min.js')}}"></script>
    <script src="{{asset($public.'/js/bootstrap-notify.min.js')}}"></script>
    <script src="{{asset($public.'/js/bootstrap-tagsinput.min.js')}}"></script>
    <script src="{{asset($public.'/js/staff/lecturer/messages.js')}}"></script>
    <script>
        // drg >> initialize text-editor
        $('#message-field').summernote({
            airMode: true,
            placeholder: 'Write message here. Highlight to edit text...',
            height: 300,
        });
        // drg >> change default font for text-editor
        $('.note-editable').css({'font-family': 'arial, sans-serif', 'font-size': '90%', 'line-height': '1'});

        // drg >> submit form via ajax
        $('#message-form').on('submit', function (e) {
            submitMessage(e, 'submit');
        });
        $('.save-message').on('click', function (e) {
            submitMessage(e, 'save');
        });
        $('.discard-message').on('click', function (e) {
            var discard = confirm("Press a button!");
            if (discard) {
                submitMessage(e, 'discard');
            }

        });

        // drg >> function submits message
        function submitMessage(e, action) {
            e.preventDefault();
            Dashmix.layout('header_loader_on');

            var form = document.getElementById('message-form');
            var data = new FormData(form);
            var url = '';
            switch (action) {
                case 'submit':
                    url = form.action;
                    break;
                case 'save':
                    url = '/staff/lecturer/messages/save';
                    break;
                case 'discard':
                    url = '/staff/lecturer/messages/discard';
                    break;
            }
            $.ajax({
                url: url,
                method: 'POST',
                contentType: false,
                data: data,
                processData: false,
                success: function (result) {
                    Dashmix.layout('header_loader_off');
                    Dashmix.helpers('notify', {
                        align: 'center',
                        type: result.type,
                        icon: 'fa fa-check mr-1',
                        message: result.message
                    });
                    window.location.replace("/staff/lecturer/messages/");
                },
                error: function () {
                    Dashmix.layout('header_loader_off');
                    Dashmix.helpers('notify', {
                        align: 'center',
                        type: 'danger',
                        icon: 'fa fa-times mr-1',
                        message: 'Oops! Something went wrong..'
                    });
                }
            });

            return false;
        }

        var nodes = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('id'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            remote: {
                url: '{!! url("/staff/lecturer/messages/suggest/nodes") !!}' + '?keyword=%QUERY%',
                wildcard: '%QUERY%'
            }
        });
        nodes.initialize();

        tagsInput('#to');
                @if(isset($to))
        let to = {!! json_encode($to) !!};
        for (i = 0; i < to.length; i++) {
            $('#to').tagsinput('add', {id: to[i].id, name: to[i].name});
        }
        @endif
        @if(isset($cc) || isset($bcc))
        showCC();
        let cc = {!! json_encode($cc) !!};
        for (i = 0; i < cc.length; i++) {
            $('#cc').tagsinput('add', {id: cc[i].id, name: cc[i].name});
        }
        let bcc = {!! json_encode($cc) !!};
        for (i = 0; i < bcc.length; i++) {
            $('#bcc').tagsinput('add', {id: bcc[i].id, name: bcc[i].name});
        }

        @endif
        function tagsInput(element) {

            $(element).tagsinput({
                itemValue: 'id',
                itemText: 'name',
                maxChars: 10,
                trimValue: true,
                allowDuplicates: false,
                freeInput: false,
                focusClass: 'form-control',
                tagClass: function (item) {
                    if (item.display)
                        return 'badge badge-' + item.display;
                    else
                        return 'badge badge-default';

                },
                onTagExists: function (item, $tag) {
                    $tag.hide().fadeIn();
                },
                typeaheadjs: [
                    {
                        hint: false,
                        highlight: true
                    },
                    {
                        name: 'to',
                        itemValue: 'id',
                        displayKey: 'name',
                        source: nodes.ttAdapter(),
                        templates: {
                            empty: [
                                '<ul class="list-group"><li class="list-group-item">Nothing found.</li></ul>'
                            ],
                            header: [
                                '<ul class="list-group">'
                            ],
                            suggestion: function (data) {
                                return '<li class="list-group-item"><span class="media py-2">' +
                                    '<div class="mx-3 overlay-container"><img class="img-avatar" src="/' + data.avatar + '" alt="">' +
                                    '</div><div class="media-body"><div class="font-w600">' + data.name + '</div>' +
                                    '<div class="font-size-sm text-muted">' + data.user_type + '</div></div></span></li>'
                            }
                        }
                    }
                ]
            });
        }

        function showCC() {
            $('.to-cc').toggleClass('d-none');
            tagsInput('#cc');
            tagsInput('#bcc');
        }

    </script>

@endsection
