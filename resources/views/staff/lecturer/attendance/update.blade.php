@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp
@extends('layouts.global')

@section('content')
    <div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
        <form action="{{url('/staff/lecturer/attendance/create')}}" method="post" id="verification-form">
            {{ csrf_field() }}
            <legend class="text-center green">Create Attendance</legend>

            <div class="form-group">
                <label for="dept" class="text-muted">Signing Method</label>
                <select name="method" id="method" class="form-control" readonly>
                    <option selected value="mass">Mass Verification</option>
                    <option selected value="">U-Code Verification</option>
                </select>
            </div>
            <div class="form-group">
                <label for="course" class="text-muted">Course Code</label>
                <select name="cid" id="course" class="form-control" required>
                    <option selected disabled>Select Course</option>
                    @foreach($courses as $course)
                        <option value="{{$course->id+3327}}">{{"$course->course_code (from- $course->course_source, for- $course->course_destination_dept)"}}</option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <input type="submit" class="btn cool-button" value="Verify">
            </div>
        </form>
    </div>
@endsection