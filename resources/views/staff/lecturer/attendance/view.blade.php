@php
    $public='';
        if(config('app.env') == 'production')
            $public ='public';

    function processDate($date){
        if($date==null){
            return null;
        }
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }

    $title=explode('_',$details->system_id);
@endphp
@extends('layouts.global')
@section('title','View Attendance')
@section('content')
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Attendance Details</h3>
        </div>
        <div class="block-content">
            <table class="table">
                <tbody>
                <tr>
                    <th>Title</th>
                    <td>{{"$title[0] (from- $title[1], for- $title[3])"}}</td>
                </tr>
                <tr>
                    <th>Session</th>
                    <td>{{$details->session}}</td>
                </tr>
                <tr>
                    <th>Count</th>
                    <td>{{$details->count}}</td>
                </tr>
                <tr>
                    <th>Status</th>
                    <td><span class="badge badge-pill badge-lg @if($details->status=='open') badge-success @elseif($details->status=='closed') badge-warning @endif" style="font-size: 1em; font-weight: 600;">{{$details->status}}</span></td>
                </tr>
                <tr>
                    <th>Opened on</th>
                    <td>{{processDate($details->created_at)}}</td>
                </tr>
                <tr>
                    <th>Closed on</th>
                    <td>{{processDate($details->closed_at)}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="block block-rounded block-bordered">
        <div class="block-header block-header-default">
            <h3 class="block-title">Attendance List</h3>
        </div>
        <div class="block-content">
            <table class="js-table-checkable table table-v-center table-hover">
                <thead class="thead-light">
                <tr>
                    @if($details->status=="open")
                        <th class="text-center" style="width: 70px;">
                            <div class="custom-control custom-checkbox d-inline-block">
                                <input type="checkbox" class="custom-control-input" id="check-all" name="check-all">
                                <label class="custom-control-label" for="check-all"></label>
                            </div>
                        </th>
                    @endif
                    <th>Signed By</th>
                    <th>Student</th>
                    <th>Reg. No.</th>
                    <th>Remark</th>
                    <th>Signed At</th>
                </tr>
                </thead>
                <tbody>
                @foreach($attendances as $attendance)
                    <tr>
                        @if($details->status=="open")

                            <td class="text-center">
                                <div class="custom-control custom-checkbox d-inline-block">
                                    <input type="checkbox" class="custom-control-input" id="row_{{$loop->index}}"
                                           name="{{"$attendance->attendance_id:$attendance->id"}}">
                                    <label class="custom-control-label" for="row_{{$loop->index}}"></label>
                                </div>
                            </td>
                        @endif
                        <td>{{$attendance->signed_by}}</td>
                        <td class="font-w600">{{$attendance->student_name}}</td>
                        <td>{{$attendance->signed_user}}</td>
                            <td class="text-center"><span class="badge @if($attendance->remark=='present') badge-success @elseif($attendance->remark=='absent') badge-danger @endif">{{$attendance->remark}}</span></td>
                        <td>{{processDate($attendance->signed_at)}}</td>

                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>
@endsection
@section('script')
    <script>jQuery(function () {
            Dashmix.helpers(['table-tools-checkable', 'table-tools-sections']);
        });
        var ctx = document.getElementById("myChart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ["Red", "Blue", "Yellow", "Green", "Purple", "Orange"],
                datasets: [{
                    label: '# of Votes',
                    data: [12, 19, 3, 5, 2, 3],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero:true
                        }
                    }]
                }
            }
        });
    </script>
@endsection

