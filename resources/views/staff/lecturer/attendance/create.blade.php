@php
    $public='';
        if(config('app.env') == 'production')
            $public ='public';

    function processDate($date){
        if($date==null){
            return null;
        }
        if (date('YMd',strtotime($date)) == date('YMd')){
            return (date('H:i',strtotime($date)));
        } elseif (date('Y',strtotime($date)) == date('Y')){
            return (date('M d ',strtotime($date)));
        } elseif (date('Y',strtotime($date)) !== date('Y')){
            return (date('M d, Y',strtotime($date)));
        }

        return date('M d, Y',strtotime($date));
    }
@endphp
@extends('layouts.global')
@section('title','Create Attendance')
@section('content')
    <div class="block block-rounded block-bordered">
        <div class="block-content">
            <div class="p-3 col-md-6 col-sm-10 col-xs-12 align-middle   " style="margin: auto">
                <form action="{{url('/staff/lecturer/attendance/create')}}" method="post" id="verification-form">
                    {{ csrf_field() }}
                    <legend class="text-center green">Create Attendance</legend>
                    <div class="form-group">
                        <label for="course" class="text-muted">Select Course</label>
                        <select name="id" id="course" class="form-control" required>
                            <option selected disabled>Select Course</option>
                            @foreach($courses as $course)
                                @php
                                    $course_array=explode('_',$course);
                                @endphp
                                <option value="{{$course}}">{{"$course_array[0] (from- $course_array[1], for- $course_array[3])"}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group text-center">
                        <input type="submit" class="btn cool-button" value="Create">
                    </div>
                </form>
            </div>
        </div>
    </div>
    @if(isset($attendances))
        <div class="block block-rounded block-bordered">
            <div class="block-content">
                <table class="table">
                    <thead>
                    <tr>
                        <th>Title</th>
                        <th>Session</th>
                        <th>Count</th>
                        <th>Opened</th>
                        <th class="text-center">Status</th>
                        <th class="text-center">Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($attendances as $attendance)
                        @php
                            $title=explode('_',$attendance->system_id);
                        @endphp
                        <tr>
                            <td class="font-w600"><a href="{{url("/staff/lecturer/attendance/view/$attendance->attendance_id")}}">{{"$title[0] (from- $title[1], for- $title[3])"}}</a></td>
                            <td>{{"$attendance->session/".($attendance->session+1)}}</td>
                            <td>{{$attendance->count}}</td>
                            <td>{{processDate($attendance->created_at)}}</td>
                            <td class="text-center"><span class="badge @if($attendance->status=='open') badge-success @elseif($attendance->status=='closed') badge-warning @endif">{{$attendance->status}}</span></td>
                            <td class="text-center">
                                <div class="btn-group-sm" role="group" aria-label="Horizontal Outline Success">
                                    <a href="{{url("/staff/lecturer/attendance/update/$attendance->attendance_id")}}" class="btn btn-outline-success">Update</a>
                                    <a href="{{url("/staff/lecturer/attendance/view/$attendance->attendance_id")}}" class="btn btn-outline-info">View</a>
                                </div>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    @endif
@endsection