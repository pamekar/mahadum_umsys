<div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
    <form action="{{$formAction}}" method="post" id="verification-form">
        {{ csrf_field() }}
        <legend class="text-center green">{{$pageTitle}} with Matric No.</legend>
        @if($type=='exam')

            <div class="form-group">
                <label for="dept" class="text-muted">Department</label>
                <select name="dept" id="dept" class="form-control" readonly>
                    <option selected value="dept">{{Auth::user()->school.' - '.Auth::user()->dept}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="course" class="text-muted">Course Code</label>
                <select name="cid" id="course" class="form-control" required>
                    <option selected disabled>Select Course</option>
                    @foreach($courses as $course)
                        <option value="{{$course->id+3327}}">{{"$course->course_code (from- $course->course_source, for- $course->course_destination_dept)"}}</option>
                    @endforeach
                </select>
            </div>


        @elseif ( $type != 'student' )

            <div class="form-group">
                <label for="sessions" class="text-muted">Session</label>
                <select id="sessions" name="session" class="form-control" required>
                    <optgroup label="">
                        <?php
                        if ( !is_numeric($sessions) )
                        {
                        $i = 0;
                        foreach ( $sessions as $session )
                        {
                        $i++;
                        ?>
                        <option value="{{$session->session}}"
                        <?php if ($i == 1) echo "selected";?>
                        >{{$session->session.'/'.($session->session+1).' session'}}</option>
                        <?php
                        }
                        } elseif(isset($sessions))
                        {
                        ?>
                        <option value="{{$sessions}}">{{$sessions.'/'.($sessions+1).'
						session'}}</option>
                        <?php
                        }
                        ?>
                    </optgroup>
                </select>
            </div>
        @endif

        <div class="form-group">
            <label for="matricno" class="text-muted">Matric No.</label> <input
                    id="matricno" class="form-control" name="user" type="text"
                    placeholder="Enter here" required
            />
        </div>
        <div class="form-group">
            <input type="submit" class="btn cool-button" value="Verify">
        </div>
    </form>
</div>