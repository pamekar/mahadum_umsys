@php
    $public = '';
    if (config('app.env') == 'production')
    $public = 'public';
@endphp

<h1 class="text-center green" id="" style="font-size: 3em;">
    Registration Verified <i class="fa fa-check" id=""></i>
</h1>
<h2 class="card-header blue width-full text-center" id="" style="">
    Showing registered courses for <span class="green" id="currentSession">{{$session}}/{{$session+1}}</span>
    Academic Session
</h2>
<div class="media p-3 py-4" id="" style="">
    <div class="media-body text-left" id="" style="">
        <dl class="dl-horizontal small green" id="">
            <dt class="" id="">Name</dt>
            <dd class="text-muted" id="">{{$userDetails->first_name . ' ' .
				$userDetails->last_name}}</dd>
            <dt class="" id="">Reg No.</dt>
            <dd class="text-muted" id="">{{$userDetails->reg_no or 'Not set'}}</dd>
            <dt class="" id="">School</dt>
            <dd class="text-muted" id="">{{$userDetails->faculty or 'Not set'}}</dd>
            <dt class="" id="">Department</dt>
            <dd class="text-muted" id="">{{$userDetails->dept or 'Not set'}}</dd>
            <dt class="" id="">Level</dt>
            <dd class="text-muted" id="">{{$level or 'Not set'}}</dd>
        </dl>
    </div>
    <img src="{{asset("$public/$userDetails->photo_location")}}"
         class="img img-thumbnail d-flex align-self-center" id=""
         alt="{{$userDetails->name or 'Not set'}}"
    >
</div>
<div>
    <table class="table table-hover text-medium card">
        <thead class="thead-default">
        <tr>
            <th class="text-center">Course Code</th>
            <th class="text-center">Course Title</th>
            <th class="text-center">Units</th>
            <th class="text-center">Type</th>
            <th class="text-center">Status</th>
        </tr>
        </thead>
        <tbody>
        <?php
        $i = 0;
        foreach ( $courses as $course )
        {
        ?>
        <tr class="courses">
            <td class="text-center course-code">{{$course->course_code}}</td>
            <td class="course-title">{{$course->course_title}}</td>
            <td class="text-center units" id="units-{{$i}}">{{$course->units}}</td>
            <td class="course-type">{{$course->course_type}}</td>
            <td class="status">{{$course->remarks or "Not Registered"}}</td>
        </tr>
        <?php
        $i++;
        }
        ?>
        </tbody>
    </table>
</div>
