<?php
$public = '';
if (config('app.env') == 'production')
    $public = 'public';
switch ( $grantAccess )
{
case ('ca_hod') :
?>
<div class="" id="" style="">
    <h1 class="text-center green" id="" style="font-size: 3em;">
        Result Verified <i class="fa fa-check" id=""></i>
    </h1>

    <h2 class="card-header blue width-full text-center" id="" style="">
        Showing results for <span class="green" id="currentSession">{{$session}}/{{$session+1}}</span>
        Academic Session
    </h2>

    <div class="media mx-4 mt-3" id="" style="">
        <div class="media-body text-left" id="" style="">
            <dl class="dl-horizontal small green" id="">
                <dt class="" id="">Name</dt>
                <dd class="text-muted" id="">{{$userDetails->first_name . ' ' .
					$userDetails->last_name}}</dd>
                <dt class="" id="">Reg No.</dt>
                <dd class="text-muted" id="">{{$userDetails->reg_no or 'Not set'}}</dd>
                <dt class="" id="">School</dt>
                <dd class="text-muted" id="">{{$userDetails->faculty or 'Not set'}}</dd>
                <dt class="" id="">Department</dt>
                <dd class="text-muted" id="">{{$userDetails->dept or 'Not set'}}</dd>
                <dt class="" id="">Level</dt>
                <dd class="text-muted" id="">{{$level or 'Not set'}}</dd>
                <dt class="" id="">Cummulative GPA</dt>
                <dd class="text-muted" id="">{{$cgpa or 'Not set'}}</dd>
                <dt class="" id="">Previous Session GPA</dt>
                <dd class="text-muted" id="">{{$psgpa or 'Not set'}}</dd>
                <dt class="" id="">Outstanding Courses</dt>
                <dd class="text-muted" id="">{{$outstanding_courses or 'Not set'}}</dd>
            </dl>
        </div>
        <img src="{{asset("$public/$userDetails->photo_location")}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{$userDetails->name or 'Not set'}}"
        >
    </div>

    <div class="" id="area" style="">
        <h3 class="card-header blue width-full text-center" id="" style="">First
            Semester</h3>

        <table class="table table-hover" id="">
            <thead class="thead-default" id="" style="">
            <tr class="" id="" style="">
                <th class="" id="" style="">Course Code</th>
                <th class="" id="" style="">Course Units</th>
                <th class="" id="" style="">Score</th>
                <th class="" id="" style="">Grade</th>
                <th class="" id="" style="">Remarks</th>
            </tr>
            </thead>

            <tbody>
            <?php
            foreach ( $firstSemesterResult as $result )
            {
            switch ($result->result_grade) {
                case ('0') :
                    $grade = "F";
                    break;
                case ('1') :
                    $grade = "E";
                    break;
                case ('2') :
                    $grade = "D";
                    break;
                case ('3') :
                    $grade = "C";
                    break;
                case ('4') :
                    $grade = "B";
                    break;
                case ('5') :
                    $grade = "A";
                    break;
                default :
                    $grade = "Not Set";
                    break;
            }
            if (is_null($result->result_lab))
                $score = $result->result_test + $result->result_exam; else
                $score = $result->result_test + $result->result_lab + $result->result_exam;
            ?>
            <tr class="@if($grade=='F') table-danger @endif" id="" style="">
                <td class="" id="" style=""><?php echo $result->course_code?></td>
                <td class="" id="" style=""><?php echo $result->course_units?></td>
                <td class="" id="" style=""><?php echo $score?></td>
                <td class="" id="" style=""><?php echo $grade?></td>
                <td class="" id="" style=""><?php echo $result->remarks?></td>
            </tr>
            <?php }?>
            </tbody>
        </table>
        <h3 class="card-header blue width-full text-center" id="" style="">Second
            Semester</h3>
        <table class="table table-hover" id="">
            <thead class="thead-default" id="" style="">
            <tr class="" id="" style="">
                <th class="" id="" style="">Course Code</th>
                <th class="" id="" style="">Course Units</th>
                <th class="" id="" style="">Score</th>
                <th class="" id="" style="">Grade</th>
                <th class="" id="" style="">Remarks</th>
            </tr>
            </thead>
            <tbody>
            <?php
            foreach ( $secondSemesterResult as $result )
            {
            switch ($result->result_grade) {
                case ('0') :
                    $grade = "F";
                    break;
                case ('1') :
                    $grade = "E";
                    break;
                case ('2') :
                    $grade = "D";
                    break;
                case ('3') :
                    $grade = "C";
                    break;
                case ('4') :
                    $grade = "B";
                    break;
                case ('5') :
                    $grade = "A";
                    break;
                default :
                    $grade = "Not Set";
                    break;
            }
            if (is_null($result->result_lab))
                $score = $result->result_test + $result->result_exam; else
                $score = $result->result_test + $result->result_lab + $result->result_exam;
            ?>
            <tr class="@if($grade=='F') table-danger @endif" id="" style="">
                <td class="" id="" style=""><?php echo $result->course_code?></td>
                <td class="" id="" style=""><?php echo $result->course_units?></td>
                <td class="" id="" style=""><?php echo $score?></td>
                <td class="" id="" style=""><?php echo $grade?></td>
                <td class="" id="" style=""><?php echo $result->remarks?></td>
            </tr>

            <?php }?>
            </tbody>
        </table>
        <div class="" id="" style="">
            <h3 class="card-header red width-full text-center" id="" style="">Result
                Summary</h3>
            <table class="table" id="">
                <tbody>
                <tr class="" id="" style="">
                    <td class="" id="" style="">First Semester GPA</td>
                    <td class="" id="" style="">{{$firstGPA}}</td>
                </tr>
                <tr class="" id="" style="">
                    <td class="" id="" style="">Second Semester GPA</td>
                    <td class="" id="" style="">{{$secondGPA}}</td>
                </tr>
                <tr class="" id="" style="">
                    <td class="" id="" style="">2015/2016 Session GPA</td>
                    <td class="" id="" style="">{{$sessionGPA}}</td>
                </tr>
                </tbody>
            </table>
        </div>
    </div>
</div>
<?php
break;
case ('courselecturer') :
?>
<div class="" id="" style="">
    <h1 class="text-center green" id="" style="font-size: 3em;">
        Result Verified <i class="fa fa-check" id=""></i>
    </h1>

    <h2 class="card-header blue width-full text-center" id="" style="">
        Showing results for <span class="green" id="currentSession">{{$session}}/{{$session+1}}</span>
        Academic Session
    </h2>

    <div class="media mx-4 mt-3" id="" style="">
        <div class="media-body text-left" id="" style="">
            <dl class="dl-horizontal small green" id="">
                <dt class="" id="">Name</dt>
                <dd class="text-muted" id="">{{$userDetails->first_name . ' ' .
					$userDetails->last_name}}</dd>
                <dt class="" id="">Reg No.</dt>
                <dd class="text-muted" id="">{{$userDetails->reg_no or 'Not set'}}</dd>
                <dt class="" id="">School</dt>
                <dd class="text-muted" id="">{{$userDetails->faculty or 'Not set'}}</dd>
                <dt class="" id="">Department</dt>
                <dd class="text-muted" id="">{{$userDetails->dept or 'Not set'}}</dd>
                <dt class="" id="">Level</dt>
                <dd class="text-muted" id="">{{$level or 'Not set'}}</dd>
            </dl>
        </div>
        <img src="{{asset($userDetails->photo_location)}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{$userDetails->name or 'Not set'}}"
        >
    </div>

    <div class="" id="area" style="">
        <h3 class="card-header blue width-full text-center" id="" style="">Course
            Result</h3>
        <table class="table table-hover" id="">
            <thead class="thead-default" id="" style="">
            <tr class="" id="" style="">
                <th class="" id="" style="">Course Code</th>
                <th class="" id="" style="">Course Units</th>
                <th class="" id="" style="">Score</th>
                <th class="" id="" style="">Grade</th>
                <th class="" id="" style="">Remarks</th>
            </tr>
            </thead>

            <tbody>
            <?php
            foreach ( $courseResults as $result )
            {
            switch ($result->result_grade) {
                case ('0') :
                    $grade = "F";
                    break;
                case ('1') :
                    $grade = "E";
                    break;
                case ('2') :
                    $grade = "D";
                    break;
                case ('3') :
                    $grade = "C";
                    break;
                case ('4') :
                    $grade = "B";
                    break;
                case ('5') :
                    $grade = "A";
                    break;
                default :
                    $grade = "Not Set";
                    break;
            }
            if (is_null($result->result_lab))
                $score = $result->result_test + $result->result_exam; else
                $score = $result->result_test + $result->result_lab + $result->result_exam;
            ?>
            <tr class="@if($grade=='F') table-danger @endif" id="" style="">
                <td class="" id="" style=""><?php echo $result->course_code?></td>
                <td class="" id="" style=""><?php echo $result->course_units?></td>
                <td class="" id="" style=""><?php echo $score?></td>
                <td class="" id="" style=""><?php echo $grade?></td>
                <td class="" id="" style=""><?php echo $result->remarks?></td>
            </tr>
            <?php }?>
            </tbody>
        </table>
    </div>
</div>
<?php
break;
default :
?>

<div class="p-3 my-3" id="" style="">
    <h1 class="text-center red" id="" style="font-size: 3em;">
        Verification Failed <i class="fa fa-remove" id=""></i>
    </h1>
    <ul class="list-group list-unstyled my-3 card" id="" style="">
        <li class="list-group-item-heading strong p-3 text-left" id=""><strong>Reasons
                for this could be: </strong></li>
        <li class="list-group-item p-3 text-left" id="">You do not have
            permission to verify this user,
        </li>
        <li class="list-group-item p-3 text-left" id="">The given input is not
            valid for the given user,
        </li>
        <li class="list-group-item p-3 text-left" id="">The document to be
            verified is no more valid, or
        </li>
        <li class="list-group-item p-3 text-left" id="">An uninterpretable
            error has occured with your verification.
        </li>
        <li class="p-3 text-center red" id="">Please contact administrator on
            this error message.
        </li>
    </ul>
</div>
<?php
break;
}
?>