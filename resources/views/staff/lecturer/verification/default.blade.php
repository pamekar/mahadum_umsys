<div class="px-3 py-2 text-justify">
    <h2 class="green">Verification Module for Lecturers</h2>
    <p>The verification module for lecturers enables lecturers verify
        students academic status. With this verification module, lecturer can
        verify a students result, course registration, school fees, and
        details.</p>
    <p>A unique code is attached to every student, and valid student's
        documents. A student, student's document can be verified using these
        unique codes. In a case where the unique code is not available the
        lecturer can verify the student with information such as registration
        number, session and department of the student.</p>
    <p>You are presented with a list of possible verifications which you
        can carry out. The sub-lists are for you to select methods you prefer
        to use for student verification.</p>
    <blockquote class="red px-4">
        <span class="bold">Note:</span>
        <ul>
            <li>The are restrictions present for a students verification.
                Therefore each lecturer is granted permission to verify only a
                limited set of students.
            </li>

            <li>On a set of related students, the view one lecturer might get on
                a particular verification might be different from the view a
                lecturer would recieve for the same verification, while some
                lecturers might not be able to carry out some verifications on the
                same set of students. This is because different lecturers have
                different academic based relationship.
            </li>
            <li>The restricted access is based on the lecturers school,
                department, academic based relationship with students.
            </li>
            <li>Hence, the HOD, course advisers, and other lecturers have varying
                verification restrictions.
            </li>

        </ul>
    </blockquote>
</div>