<?php
if ( sizeof($students) == 0 )
{
?>

<table class="table">
    <tbody>
    <tr>
        <td>
            <h1 class="text-center red">No records found @if(!empty($query))for
                "{{$query}}"@endif</h1>

        <td>

    </tr>
    </tbody>
</table>

<?php
}
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th>S/N</th>
        <th>Name</th>
        <th>Reg. No.</th>
        <th>Dept.</th>
        <th>Code</th>
    </tr>
    </thead>
    <tbody>
    <?php
    $i = 1;
    foreach ( $students as $student )
    {
    $documentID = "";
    switch ($type) {
        case ('result') :
            $documentID = md5($student->reg_no . 'undergraduate_result' . $session);
            break;
        case ('course') :
            $documentID = md5($student->reg_no . 'undergraduate_course_registration' . $session);
            break;
        case ('student') :
            $documentID = md5($student->reg_no);
            break;
    }
    $index = $student->id % 37;
    $code = substr(sha1($documentID), $index, 4);
    $url = isset($mobile) ? "javascript:router('verifyLink', '/v/c/$action/$code/$student->reg_no/$session')"
        : url("/v/c/$action/$code/$student->reg_no/$session");
    ?>
    <tr>
        <td style="color: rgba(0, 0, 0, 0.5);">{{$i}}</td>
        <td><a style="color: rgb(41, 43, 44);" href="{{$url}}">{{$student->name}}</a></td>
        <td><a style="color: rgb(41, 43, 44);" href="{{$url}}">{{$student->reg_no}}</a></td>
        <td style=" color: rgba(0, 0, 0, 0.5);">{{$student->dept}}</td>
        <td style="color: rgba(0, 0, 0, 0.5);">{{$code}}</td>
    </tr>
    <?php
    $i++;
    }
    ?>
    </tbody>
</table>