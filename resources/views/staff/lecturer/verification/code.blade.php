<div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
    <form action="{{$formAction}}" method="post" id="verification-form">
        {{ csrf_field() }}
        <legend class="text-center green">{{$pageTitle}} by code</legend>
        @if($type=='exam')
            <div class="form-group">
                <label for="dept" class="text-muted">Department</label>
                <select name="dept" id="dept" class="form-control" readonly>
                    <option selected value="dept">{{Auth::user()->school.' - '.Auth::user()->dept}}</option>
                </select>
            </div>
            <div class="form-group">
                <label for="course" class="text-muted">Course Code</label>
                <select name="cid" id="course" class="form-control" required>
                    <option selected disabled>Select Course</option>
                    @foreach($courses as $course)
                        <option value="{{$course->id+3327}}">{{"$course->course_code (from- $course->course_source, for- $course->course_destination_dept)"}}</option>
                    @endforeach
                </select>
            </div>
        @endif
        <div class="form-group">
            <label for="code" class="text-muted">Unique Code</label> <input
                    id="code" class="form-control" name="code" type="text"
                    placeholder="Enter here" required
            />
        </div>
        <div class="form-group">
            <input type="submit" class="btn cool-button" value="Verify">
        </div>
    </form>
</div>