<?php
$public = '';
if (config('app.env') == 'production')
    $public = 'public';
switch ( $grantAccess )
{
case ('ca_hod') :
?>
<div class="" id="" style="">
    <h1 class="text-center green" id="" style="font-size: 3em;">
        Student Verified <i class="fa fa-check" id=""></i>
    </h1>

    <div class="media mx-4 mt-3" id="" style="">
        <div class="media-body text-left" id="" style="">
            <dl class="dl-horizontal small green" id="">
                <dt class="" id="">Name</dt>
                <dd class="text-muted" id="">{{$userDetails->first_name . ' ' .
					$userDetails->last_name}}</dd>
                <dt class="" id="">Reg No.</dt>
                <dd class="text-muted" id="">{{$userDetails->reg_no or 'Not set'}}</dd>
                <dt class="" id="">School</dt>
                <dd class="text-muted" id="">{{$userDetails->faculty or 'Not set'}}</dd>
                <dt class="" id="">Department</dt>
                <dd class="text-muted" id="">{{$userDetails->dept or 'Not set'}}</dd>
                <dt class="" id="">Level</dt>
                <dd class="text-muted" id="">{{$level or 'Not set'}}</dd>
                <dt class="" id="">Cummulative GPA</dt>
                <dd class="text-muted" id="">{{$cgpa or 'Not set'}}</dd>
                <dt class="" id="">Previous Session GPA</dt>
                <dd class="text-muted" id="">{{$psgpa or 'Not set'}}</dd>
                <dt class="" id="">Outstanding Courses</dt>
                <dd class="text-muted" id="">{{$outstanding_courses or 'Not set'}}</dd>
            </dl>
        </div>
        <img src="{{asset("$public/$userDetails->photo_location")}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{$userDetails->first_name or 'Not set'}}"
        >
    </div>
</div>
<?php
break;
case ('lecturer') :
?>
<div class="" id="" style="">
    <h1 class="text-center green" id="" style="font-size: 3em;">
        Student Verified <i class="fa fa-check" id=""></i>
    </h1>

    <div class="media mx-4 mt-3" id="" style="">
        <div class="media-body text-left" id="" style="">
            <dl class="dl-horizontal small green" id="">
                <dt class="" id="">Name</dt>
                <dd class="text-muted" id="">{{$userDetails->first_name . ' ' .
					$userDetails->last_name}}</dd>
                <dt class="" id="">Reg No.</dt>
                <dd class="text-muted" id="">{{$userDetails->reg_no or 'Not set'}}</dd>
                <dt class="" id="">School</dt>
                <dd class="text-muted" id="">{{$userDetails->faculty or 'Not set'}}</dd>
                <dt class="" id="">Department</dt>
                <dd class="text-muted" id="">{{$userDetails->dept or 'Not set'}}</dd>
                <dt class="" id="">Level</dt>
                <dd class="text-muted" id="">{{$level or 'Not set'}}</dd>
            </dl>
        </div>
        <img src="{{asset($userDetails->photo_location)}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{$userDetails->first_name or 'Not set'}}"
        >
    </div>
</div>
<?php
break;
default :
?>

<div class="p-3 my-3" id="" style="">
    <h1 class="text-center red" id="" style="font-size: 3em;">
        Verification Failed <i class="fa fa-remove" id=""></i>
    </h1>
    <ul class="list-group list-unstyled my-3 card" id="" style="">
        <li class="list-group-item-heading strong p-3 text-left" id=""><strong>Reasons
                for this could be: </strong></li>
        <li class="list-group-item p-3 text-left" id="">You do not have
            permission to verify this user,
        </li>
        <li class="list-group-item p-3 text-left" id="">The given input is not
            valid for the given user,
        </li>
        <li class="list-group-item p-3 text-left" id="">The document to be
            verified is no more valid, or
        </li>
        <li class="list-group-item p-3 text-left" id="">An uninterpretable
            error has occurred with your verification.
        </li>
        <li class="p-3 text-center red" id="">Please contact administrator on
            this error message.
        </li>
    </ul>
</div>
<?php
break;
}
?>