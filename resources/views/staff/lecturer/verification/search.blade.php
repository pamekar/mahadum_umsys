<div class="width-full">
    <div class="p-3 col-md-6 col-sm-10 col-xs-12" style="margin: auto">
        <form action="{{$formAction}}" method="post" id="verification-form">
            {{ csrf_field() }}
            <legend class="text-center green">Search {{$pageTitle}}</legend>

            @if($type=='exam')

                <div class="form-group">
                    <label for="dept" class="text-muted">Department</label>
                    <select name="dept" id="dept" class="form-control" readonly>
                        <option selected value="dept">{{Auth::user()->school.' - '.Auth::user()->dept}}</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="course" class="text-muted">Course Code</label>
                    <select name="cid" id="course" class="form-control" required>
                        @if(empty($courseid))
                            <option selected disabled>Select Course</option> @endif
                        @foreach($courses as $course)
                            <option @if($course->id+3327 == $courseid) selected
                                    @endif value="{{$course->id+3327}}">{{"$course->course_code (from- $course->course_source, for- $course->course_destination_dept)"}}</option>
                        @endforeach
                    </select>
                </div>


            @elseif ( $type != 'student' )
                <div class="form-group">
                    <label for="sessions" class="text-muted">Session</label> <select
                            id="sessions" name="session" class="form-control"
                    >
                        <optgroup label="">

                            @if ((is_object($sessions)|| is_array($sessions)) && sizeof($sessions) > 0 )
                                @php $i = 0; @endphp
                                @foreach ( $sessions as $session )

                                    @php $i++; @endphp

                                    <option value="{{$session->session}}"
                                            @if($i == 1) selected @endif
                                    >{{$session->session.'/'.($session->session+1).' session'}}</option>

                                @endforeach
                            @else
                                <option value="{{$sessions}}">{{$sessions.'/'.($sessions+1).'
							session'}}</option>
                            @endif
                        </optgroup>
                    </select>
                </div>
            @endif
            <div class="form-group">
                <label for="parameters" class="text-muted">Parameters</label> <input
                        id="parameters" class="form-control" name="query" type="text"
                        placeholder="Enter search parameters"
                        value="@if(!empty($query)){{$query}}@endif"
                />
            </div>
            <div class="form-group">
                <input type="submit" class="btn cool-button" value="Search">
            </div>
        </form>
    </div>
</div>
<div class="width-full">{!!$html!!}</div>