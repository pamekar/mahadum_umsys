@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';
    $data=session('userData');
@endphp
@extends('layouts.global')
@section('title','Home')
@section('style')
    <link rel='stylesheet' href="{{asset($public.'/css/fullcalendar.css')}}"/>
    <style>
        #body-content {
            padding: 1.5rem 0.5rem;
            margin: 0.5rem 0rem;
        }

        .calendar h2 {
            color: #2CAC5C !important;
            font-size: 1.375em;
        }

        .calendar button {
            height: auto;
            padding: 0.3rem !important;
            line-height: 1.15;
            margin: 0;
        }

        .calendar button span {
            margin: 0 !important;
            font-size: 90%
        }

        .calendar button .fc-icon {
            top: initial;
        }

        .fc-unthemed td.fc-today {
            color: #fff;
            background-color: #f0ad4e;
            border-color: #f0ad4e;
        }

        .fc-row .fc-content-skeleton td {
            background: none;
            border-color: transparent;
            border-bottom: 0;
        }

        .fc-day-number:hover {
            background-color: #F3F3F3;
            color: #2CAC5C !important;
        }

        .notice .list-inline {
            margin-bottom: 0.5rem;
            font-size: 75%;
        }

        #quick-links .list-group-item {
            padding: 0rem;
        }

        #user-details {
            display: block;
        }

        .side-bar #user-details > .navbar-text {
            display: block;
            margin: 0.03125rem 0;
            padding: 0;
            margin: 0.03125rem 0;
        }

        .side-bar #user-details img {
            width: 8.75rem;
            height: 9.0rem;
        }
    </style>
@endsection('style')
@section('content')
    <div class="container width-full">
        <div class="row">
            <div class="col-md-12 col-xs-12 card" id="body-content">
                <div class="col-xs-12 table">
                    <div class="col-md-7 col-xs-12 partition">
                        <div class=partition-heading>
                            <strong>Your Time Table</strong>
                        </div>
                        <div class="partition-body calendar" id="time-table"></div>
                    </div>
                    <div class="col-md-5 col-xs-12 partition">
                        <div class=partition-heading>
                            <strong>Memo/Notices</strong>
                        </div>
                        <div class="partition-body">
                            <div class="list-group">
                                <?php
                                foreach ( $data->notices as $notice )
                                {
                                $info = "";
                                $html_elements = array(
                                    "<p>",
                                    "</p>",
                                    "<ul>",
                                    "</ul>",
                                    "<ol>",
                                    "</ol>",
                                    "<li>",
                                    "</li>",
                                    "<h1>",
                                    "</h1>",
                                    "<h2>",
                                    "</h2>",
                                    "<h3>",
                                    "</h3>",
                                    "<br>"
                                );
                                $content = $notice->content;
                                $content = str_replace($html_elements, "", $content);
                                $content = explode(" ", $content);
                                for ($i = 0; $i < 20; $i++) {
                                    $info = $info . $content [$i] . " ";
                                }
                                ?>

                                <div class="list-group-item notice bghover">
                                    <h3 class="small mb-0">
                                        <a class="green"
                                           href="{{url('/notices/view/'.$notice->id)}}"><?php echo $notice->title;?></a>
                                    </h3>
                                    <ul class="list-inline text-muted">
                                        <li class="list-inline-item">
                                            Date: <?php echo date('M j, Y', strtotime($notice->created_at));?>
                                        </li>
                                        <li class="list-inline-item">
                                            Announcer: <?php echo $notice->admin_title?>
                                        </li>
                                    </ul>
                                    <article class="small"><?php echo $info;?><a
                                                href="{{url('notices/view/'.$notice->id)}}"
                                                class="list-link"
                                        > ... read more</a>
                                    </article>
                                </div>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="" id="view-all">
                                <a href="{{url('notices')}}" class="width-full btn btn-warning">View
                                    all</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 table">
                    <div class="col-md-9 col-xs-12 partition">
                        <div class="partition-heading">
                            <strong>School's Academic Calendar</strong>
                        </div>
                        <div class="partition-body calendar" id="school-calendar"></div>
                    </div>
                    <div class="col-md-3 col-xs-12 partition">
                        <div class="partition-heading">
                            <strong>Quick Links</strong>
                        </div>
                        <div class="partition-body">
                            <ul class="list-group" id="quick-links">
                                <li class="list-group-item"><a
                                            href="{{url('undergraduate/courses/results')}}"
                                            class="list-link"
                                    >Check your result</a></li>
                                <li class="list-group-item"><a href="#" class="list-link">Register
                                        your courses</a></li>
                                <li class="list-group-item"><a href="#" class="list-link">View
                                        class attendance</a></li>
                                <li class="list-group-item"><a href="{{url('notices')}}"
                                                               class="list-link"
                                    >Memo and Notices</a></li>
                                <li class="list-group-item"><a href="#" class="list-link">View
                                        Department</a></li>
                                <li class="list-group-item"><a href="#" class="list-link">View
                                        School</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection @section('script')
    <script type='text/javascript' src="{{asset($public.'/js/moment.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/fullcalendar.js')}}"></script>
    <script type="text/javascript">
        <!--

        //-->
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#time-table').fullCalendar({
            // put your options and callbacks here
            defaultView: 'listDay',
            aspectRatio: 1.2,
            header: {
                right: 'prev,next today',
                left: 'title',
            },
            fixedWeekCount: false,
            contentHeight: 'auto',
            navLinks: true,
            startParam: 'start',
            endParam: 'end',
            //
            events: {
                url: '{{url("home/gettimetable")}}',
                type: 'POST',
                data: {
                    type: 'timetable',
                },
                error: function () {
                    alert('there was an error while fetching events!');
                },
                color: '#2CAC5C',   // a non-ajax option
                textColor: '#f5f5f5' // a non-ajax option
            }

        });
        $('#school-calendar').fullCalendar({
            // put your options and callbacks here
            aspectRatio: 1.5,
            header: {
                right: 'prev,next today',
                center: 'title',
                left: 'month,listWeek'
            },
            fixedWeekCount: false,
            contentHeight: 'auto',
            navLinks: true,
            startParam: 'start',
            endParam: 'end',
            //
            events: {
                url: '{{url("home/getevents")}}',
                type: 'POST',
                data: {
                    type: 'calendar',
                },
                error: function () {
                    alert('there was an error while fetching events!');
                },
                color: '#2CAC5C',   // a non-ajax option
                textColor: '#f5f5f5' // a non-ajax option
            }

        });
    </script>

@endsection
