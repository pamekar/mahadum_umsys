@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.global')
@section('title','Notices') @section('style')
    <style>
        .notice .list-inline {
            margin-bottom: 0.5rem;
            font-size: 90%;
        }

        .notice article {
            padding: 1rem;
        }

        #body-content {
            padding: 1.5rem 0.5rem;
            margin: 0.5rem 0rem;
        }
    </style>
@endsection('style') @section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-xs-12 no-margin no-padding">
                <ul class="breadcrumb width-full">
                    <li class="breadcrumb-item"><a href="{{url('home')}}"
                                                   class="list-link"
                        >Home</a></li>
                    <li class="breadcrumb-item"><a href="{{url('notices')}}"
                                                   class="list-link"
                        >Notices</a></li>
                    <li class="breadcrumb-item active">{{$notice->title}}</li>
                </ul>
            </div>
            <div class="col-sm-12 col-xs-12 card" id="body-content">
                <div class="row">
                    <div class="col-sm-12 col-xs-12 notice">
                        <div class="card-header">
                            <h3 class="green notice-heading">{{$notice->title}}</h3>
                            <ul class="list-inline text-muted">
                                <li class="list-inline-item">
                                    Date: <?php echo date('M j, Y', strtotime($notice->created_at));?>
                                </li>
                                <li class="list-inline-item">Announcer: {{$notice ->
								admin_title}}</li>
                            </ul>
                        </div>
                        <article>{!!$notice->content!!}</article>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection @section('script')
    <script type="text/javascript">
        <!--

        //-->
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

@endsection
