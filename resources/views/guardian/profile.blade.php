@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';
        $background = ["$public/jpg/photo15.jpg", "$public/jpg/photo4.jpg", "$public/jpg/photo1.jpg", "$public/jpg/photo3.jpg"];

    $viewedWard=[];
    $data = session('userData');
    $wards = $data->wards;
@endphp
@extends('layouts.guardian.global')
@section('style')
    <link rel='stylesheet' href="{{asset($public.'/css/fullcalendar.css')}}"/>
    <link rel='stylesheet' href="{{asset($public.'/css/dropzone.min.css')}}"/>
@endsection
@section('title','Profile')
@section('content')
    <div class="content">

        <div class="block block-themed bg-image"
             style="background-image: url('{{$background[array_rand($background)]}}');">
            <div class="block-header bg-black-50">
                <h3 class="block-title">Wards</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                </div>
            </div>
            <div class="block-content bg-white">
                <div class="block block-rounded block-bordered">
                    <ul class="nav nav-tabs nav-tabs-block" data-toggle="tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link @if(!sizeof($wards['pending'])>0) active @endif" href="#active-wards">Active</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link @if(sizeof($wards['pending'])>0) active @endif" href="#pending-wards">Pending</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#blocked-students">Blocked</a>
                        </li>
                    </ul>
                    <div class="block-content tab-content overflow-hidden" id="wards">
                        @include('guardian.partials.wards')
                    </div>
                </div>
            </div>
        </div>
        <div class="block block-themed bg-image"
             style="background-image: url('{{$background[array_rand($background)]}}');">
            <div class="block-header bg-black-50">
                <h3 class="block-title">Profile Details</h3>
                <div class="block-options">
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                    <button type="button" class="btn-block-option" data-toggle="block-option"
                            data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                </div>
            </div>
            <div class="block-content bg-white">
                <form action="{{route('guardian.settings.profile')}}" method="post" id="profile-form">
                    {{csrf_field()}}
                    <div class="form-group">
                        <label for="username">Username</label>
                        <input class="form-control" type="text" id="username"
                               value="{{Auth::user()->name}}" readonly>
                    </div>
                    <div class="form-group">
                        <label for="first_name">First Name</label>
                        <input class="form-control" type="text" id="first_name"
                               name="first_name" placeholder="What's your first name?" required
                               value="{{$data->user_meta->first_name}}">
                    </div>
                    <div class="form-group">
                        <label for="last_name">Last Name</label>
                        <input class="form-control" type="text" id="last_name"
                               name="last_name" placeholder="What's your last name?" required
                               value="{{$data->user_meta->last_name}}">
                    </div>
                    <div class="form-group">
                        <label class="d-block">Gender</label>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" id="gender1" name="gender" required
                                   @if($data->user_meta->gender=='male') checked @endif
                                   value="male"
                                   type="radio">
                            <label class="form-check-label" for="gender1">Male</label>
                        </div>
                        <div class="form-check form-check-inline">
                            <input class="form-check-input" id="gender2" name="gender" required
                                   @if($data->user_meta->gender=='female') checked @endif
                                   value="female"
                                   type="radio">
                            <label class="form-check-label" for="gender2">Female</label>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <input class="form-control" type="text" id="address"
                               name="address" placeholder="Where do you live?"
                               value="{{$data->user_meta->address}}" required>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone Number</label>
                        <input class="form-control" id="phone" name="phone_no"
                               value="{{$data->user_meta->phone_no}}"
                               placeholder="+234 0999 999 9999" type="text" required>
                    </div>

                </form>

            </div>
            <div class="block-content block-content-full block-content-sm bg-body-light text-right">
                <button type="submit" form="profile-form" class="btn btn-success">
                    <i class="fa fa-check"></i> Submit
                </button>
            </div>
        </div>

    </div>


@endsection
@section('script')
    <script type='text/javascript' src="{{asset($public.'/js/jquery.bootstrap.wizard.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/jquery.validate.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'js/additional-methods.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/be_forms_wizard.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/jquery.maskedinput.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/dropzone.js')}}"></script>
@endsection