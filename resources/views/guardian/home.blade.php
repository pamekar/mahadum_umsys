@php
    $public='';
    if(config('app.env') == 'production')
         $public ='public';
$data = session('userData');
    $wards = $data->wards;
@endphp
@extends('layouts.guardian.global')
@section('title','Home')
@section('style')
    <link rel='stylesheet' href="{{asset($public.'/css/fullcalendar.css')}}"/>
@endsection('style')
@section('content')
    <div class="content">
        <a class="block block-bordered text-center d-sm-none" href="javascript:void(0)" data-toggle="class-toggle"
           data-target=".js-classic-nav" data-class="d-none d-sm-block">
            <div class="block-content block-content-full text-center">
                <div class="font-w600 text-uppercase">
                    <i class="fa fa-bars mr-1"></i> Navigation
                </div>
            </div>
        </a>
        <div class="row">
            @php
                $background = ["$public/jpg/photo15.jpg", "$public/jpg/photo4.jpg", "$public/jpg/photo1.jpg", "$public/jpg/photo3.jpg"];
            @endphp
            @foreach($wards['active'] as $ward)
                <div class="col-md-6 col-xl-3 animated fadeIn" data-toggle="appear">
                    <div class="block block-rounded text-center">
                        <div class="block-content block-content-full bg-image"
                             style="background-image: url('{{$background[array_rand($background)]}}');">
                            <img class="img-avatar img-avatar-thumb" src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                        </div>
                        <div class="block-content block-content-full block-content-sm bg-body-light">
                            <div class="font-w600">{{$ward->first_name.' '.$ward->last_name}}</div>
                            <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                        </div>
                        <div class="block-content block-content-full">
                            <a class="btn btn-sm btn-light" href="{{url("/guardian/view/$ward->reg_no")}}">
                                <i class="fa fa-user-circle text-muted mr-1"></i> View Ward
                            </a>
                        </div>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="row">
            <div class="block block-rounded block-bordered">
                <div class="block-header block-header-default">
                    <h3 class="block-title">School Calendar
                        <small>Dates and Events</small>
                    </h3>
                    <div class="block-options">
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                                data-action="close">
                            <i class="si si-close"></i>
                        </button>
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                                data-action="content_toggle"><i class="si si-arrow-up"></i></button>
                        <button type="button" class="btn-block-option" data-toggle="block-option"
                                data-action="fullscreen_toggle"><i class="si si-size-fullscreen"></i></button>
                    </div>
                </div>
                <div class="block-content">
                    <div class="calendar" id="school-calendar"></div>
                </div>
            </div>
        </div>
    </div>
@endsection @section('script')
    <script type='text/javascript' src="{{asset($public.'/js/moment.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/fullcalendar.js')}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#school-calendar').fullCalendar({
            // put your options and callbacks here
            aspectRatio: 1.5,
            header: {
                right: 'prev,next today',
                center: 'title',
                left: 'month,listWeek'
            },
            fixedWeekCount: false,
            contentHeight: 'auto',
            navLinks: true,
            startParam: 'start',
            endParam: 'end',
            //
            events: {
                url: '{{url("home/getevents")}}',
                type: 'POST',
                data: {
                    type: 'calendar',
                },
                error: function () {
                    alert('there was an error while fetching events!');
                },
                color: '#2CAC5C',   // a non-ajax option
                textColor: '#f5f5f5' // a non-ajax option
            }

        });
    </script>

@endsection
