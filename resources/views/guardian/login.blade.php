{{-- $drg>> Stored in resources/views/welcome.blade.php --}}

@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.login_template') @section('title', 'Login')
@section('navigation') @parent @endsection @section('login-form')
    @parent
    <div class="login-form-wrapper">

        <form method="POST" action="/login" id="user-category-form">
            {{ csrf_field() }}
            <div class="form-group">
                <label for="id-no" class="text-muted">
                    <small>Enter the registration
                        number of your ward
                    </small>
                </label> <input type="text" id="reg-no"
                                name="reg-no" class="form-control" placeholder="Student Reg. No."
                                required
                >
            </div>
            <div class="form-group">
                <label for="pswd" class="text-muted">
                    <small>Enter your password here</small>
                </label>
                <input type="password" id="pswd" name="password" class="form-control"
                       placeholder="Your Password"
                >
            </div>
            <button type="submit" class="btn btn-primary pull-right">Submit</button>

        </form>


    </div>
@endsection
