@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';
        $background = ["$public/jpg/photo15.jpg", "$public/jpg/photo4.jpg", "$public/jpg/photo1.jpg", "$public/jpg/photo3.jpg"];

    $viewedWard=[];
@endphp
@extends('layouts.guardian.global')
@section('style')
    <link rel='stylesheet' href="{{asset($public.'/css/fullcalendar.css')}}"/>
    <link rel='stylesheet' href="{{asset($public.'/css/dropzone.min.css')}}"/>
@endsection
@section('title','Profile')
@section('content')
    <div class="content">
        <div class="row">
            <div class="col-md-12">
                <div class="js-wizard-simple block block block-rounded block-bordered">
                    <ul class="nav nav-tabs nav-tabs-block nav-justified" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#step1" data-toggle="tab">1. Personal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#step2" data-toggle="tab">2. Details</a>
                        </li>
                        {{--<li class="nav-item">
                            <a class="nav-link" href="#step3" data-toggle="tab">3. Photo</a>
                        </li>--}}
                    </ul>

                    <div class="block-content block-content-sm">
                        <div class="progress" data-wizard="progress" style="height: 8px;">
                            <div class="progress-bar progress-bar-striped progress-bar-animated bg-primary"
                                 role="progressbar" style="width: 30%;" aria-valuenow="30" aria-valuemin="0"
                                 aria-valuemax="100"></div>
                        </div>
                    </div>
                    <form action="{{route('guardian.settings.profile')}}" method="post" id="profile-form">
                        {{csrf_field()}}
                        <div class="block-content block-content-full tab-content" style="min-height: 300px;">

                            <div class="tab-pane active" id="step1" role="tabpanel">
                                <div class="form-group">
                                    <label for="username">Username</label>
                                    <input class="form-control" type="text" id="username"
                                           value="{{Auth::user()->name}}" readonly>
                                </div>
                                <div class="form-group">
                                    <label for="first_name">First Name</label>
                                    <input class="form-control" type="text" id="first_name"
                                           name="first_name" placeholder="What's your first name?" required>
                                </div>
                                <div class="form-group">
                                    <label for="last_name">Last Name</label>
                                    <input class="form-control" type="text" id="last_name"
                                           name="last_name" placeholder="What's your last name?" required>
                                </div>
                            </div>
                            <div class="tab-pane" id="step2" role="tabpanel">
                                <div class="form-group">
                                    <label class="d-block">Gender</label>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" id="gender1" name="gender" required value="male"
                                               type="radio">
                                        <label class="form-check-label" for="gender1">Male</label>
                                    </div>
                                    <div class="form-check form-check-inline">
                                        <input class="form-check-input" id="gender2" name="gender" required
                                               value="female"
                                               type="radio">
                                        <label class="form-check-label" for="gender2">Female</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="address">Address</label>
                                    <input class="form-control" type="text" id="address"
                                           name="address" placeholder="Where do you live?" required>
                                </div>
                                <div class="form-group">
                                    <label for="phone">Phone Number</label>
                                    <input class="form-control" id="phone" name="phone_no"
                                           placeholder="+234 0999 999 9999" type="text" required>
                                </div>
                            </div>

                            {{--<div class="tab-pane" id="step3" role="tabpanel">
                                <div class="form-group">
                                    <div class="block-content block-content-full">
                                        <h2
                                                class="content-heading">Asynchronous File Uploads</h2>
                                        <div class="row">
                                            <div class="col-lg-4">
                                                <p class="text-muted">
                                                    Drag and drop photo for your file uploads
                                                </p>
                                                <p class="text-muted">
                                                    Your photo will be resized to 400x400, so upload a photo with
                                                    corresponding dimensions
                                                </p>
                                            </div>
                                            <div class="col-lg-8 col-xl-5">
                                                <div class="dropzone" id="photo"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>--}}

                        </div>
                    </form>
                    <div class="block-content block-content-sm block-content-full bg-body-light rounded-bottom">
                        <div class="row">
                            <div class="col-6">
                                <button type="button" class="btn btn-secondary" data-wizard="prev">
                                    <i class="fa fa-angle-left mr-1"></i> Previous
                                </button>
                            </div>
                            <div class="col-6 text-right">
                                <button type="button" class="btn btn-secondary" data-wizard="next">
                                    Next <i class="fa fa-angle-right ml-1"></i>
                                </button>
                                <button type="submit" class="btn btn-primary d-none" data-wizard="finish"
                                        form="profile-form" id="submit-button">
                                    <i class="fa fa-check mr-1"></i> Submit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
    <script type='text/javascript' src="{{asset($public.'/js/jquery.bootstrap.wizard.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/jquery.validate.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'js/additional-methods.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/be_forms_wizard.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/jquery.maskedinput.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/dropzone.js')}}"></script>
    <script>
        $('#phone').mask("+234 0999 999 9999");
        Dropzone.autoDiscover=false;
        var myDropzone = new Dropzone("div#photo", {
            url: "{{route('guardian.settings.photo')}}",
            paramName: 'photo',
            maxFiles: 1,
            acceptedFiles: 'image/*',
            resizeWidth: 400,
            resizeHeight: 400,
            processing: function (file) {
                //$('#submit-button').attr('disabled', '');
            },
            success: function (file, done) {
           //     $('#submit-button').removeAttr('disabled');
            }
        });
    </script>
@endsection