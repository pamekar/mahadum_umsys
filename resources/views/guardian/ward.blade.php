@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';
        $background = ["$public/jpg/photo15.jpg", "$public/jpg/photo4.jpg", "$public/jpg/photo1.jpg", "$public/jpg/photo3.jpg"];


@endphp
@extends('layouts.guardian.global')
@section('style')
    <link rel='stylesheet' href="{{asset($public.'/css/fullcalendar.css')}}"/>
@endsection('style') @section('content')
    <div class="content">
        <a class="block block-bordered text-center d-sm-none" href="javascript:void(0)" data-toggle="class-toggle"
           data-target=".js-classic-nav" data-class="d-none d-sm-block">
            <div class="block-content block-content-full text-center">
                <div class="font-w600 text-uppercase">
                    <i class="fa fa-bars mr-1"></i> Navigation
                </div>
            </div>
        </a>
        <div class="row no-gutters flex-md-10-auto">
            <div class="@if(sizeof($wards['active'])>1) col-md-4 @endif">
                <div class="content">
                    <div class="d-md-none push">
                        <button type="button" class="btn btn-block btn-hero-primary" data-toggle="class-toggle"
                                data-target="#side-content" data-class="d-none">
                            Active Wards
                        </button>
                    </div>
                    <div id="side-content" class="d-none d-md-block push">
                        @foreach($wards['active'] as $ward)
                            @if($ward->reg_no!==$user)
                                <div class="col-12 animated fadeIn" data-toggle="appear">
                                    <div class="block block-rounded text-center">
                                        <div class="block-content block-content-full bg-image"
                                             style="background-image: url('{{$background[array_rand($background)]}}');">
                                            <img class="img-avatar img-avatar-thumb"
                                                 src="{{asset($public.'/'.$ward->photo_location)}}"
                                                 alt="">
                                        </div>
                                        <div class="block-content block-content-full block-content-sm bg-body-light">
                                            <div class="font-w600">{{$ward->first_name.' '.$ward->last_name}}</div>
                                            <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                                        </div>
                                        <div class="block-content block-content-full">
                                            <a class="btn btn-sm btn-light"
                                               href="{{url("/guardian/view/$ward->reg_no")}}">
                                                <i class="fa fa-user-circle text-muted mr-1"></i> View Ward
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @elseif($ward->reg_no===$user)
                                @php
                                    $viewedWard=$ward;
                                @endphp
                            @endif
                        @endforeach
                    </div>
                </div>
            </div>

            <div class="@if(sizeof($wards['active'])>1) col-md-8 @else col-12 @endif  bg-body-dark">
                @if(isset($viewedWard))
                    @php
                        $guardian_access= explode(';',$viewedWard->guardian_access)
                    @endphp

                    @if(sizeof($guardian_access)<4)
                        <div class="col-12">
                            <div class="alert alert-warning d-flex mr-auto align-items-center" role="alert">
                                <div class="flex-00-auto">
                                    <i class="fa fa-fw fa-info-circle"></i>
                                </div>
                                <div class="flex-fill ml-3">
                                    <p class="mb-0 font-w600">Some features might not be accessible, because of the
                                        current permissions given.</p>
                                </div>
                            </div>
                        </div>
                    @endif
                    <div class="content content-full">

                        <div class="bg-image"
                             style="background-image: url('{{$background[array_rand($background)]}}');">
                            <div class="bg-black-25">
                                <div class="content content-full">
                                    <div class="py-5 text-center">
                                        <a class="img-link" href="#">
                                            <img class="img-avatar img-avatar96 img-avatar-thumb"
                                                 src="{{asset($public.'/'.$viewedWard->photo_location)}}"
                                                 alt="{{"$viewedWard->first_name $viewedWard->last_name"}}">
                                        </a>
                                        <h1 class="font-w700 my-2 text-white">{{"$viewedWard->first_name $viewedWard->last_name"}}</h1>
                                        <h2 class="h4 font-w700 text-white-75">
                                            {{$viewedWard->reg_no}} <a class="text-primary-lighter"
                                                                       href="javascript:void(0)">{{(setting('admin.current_session')-$viewedWard->year_entry+1)*100 }}
                                                Level</a>
                                        </h2>


                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="block block-bordered">
                            <ul class="nav nav-tabs nav-tabs-alt nav-tabs-block" data-toggle="tabs" role="tablist">
                                @if(in_array('schedules',$guardian_access))
                                    <li class="nav-item">
                                        <a class="nav-link active show" href="#timetable">Time Table</a>
                                    </li>
                                @endif
                                @if(in_array('results',$guardian_access))
                                    <li class="nav-item">
                                        <a class="nav-link" href="#results">Results</a>
                                    </li>
                                @endif
                                @if(in_array('statistics',$guardian_access))
                                    <li class="nav-item">
                                        <a class="nav-link" href="#statistics">Statistics</a>
                                    </li>
                                @endif
                                @if(in_array('fees',$guardian_access))
                                    <li class="nav-item">
                                        <a class="nav-link" href="#fees">Fees</a>
                                    </li>
                                @endif
                            </ul>
                            <div class="block-content block-content-full tab-content overflow-hidden">

                                @if(in_array('schedules',$guardian_access))
                                    <div class="tab-pane fade fade-left active show" id="timetable" role="tabpanel">
                                        <div class="calendar" id="time-table"></div>
                                    </div>
                                @endif
                                @if(in_array('results',$guardian_access))
                                    <div class="tab-pane fade fade-left" id="results" role="tabpanel">
                                        {!! $result !!}
                                    </div>
                                @endif
                                @if(in_array('statistics',$guardian_access))
                                    <div class="tab-pane fade fade-left" id="statistics" role="tabpanel">
                                        <div class="row">
                                            <div class="col-md-12 col-xs-12">
                                                <div class="block block-rounded block-bordered">
                                                    <div class="block-header block-header-default">
                                                        <h3 class="block-title">Attendance Chart</h3>
                                                    </div>
                                                    <div class="block-content block-content-full">
                                                        <canvas id="attendance-chart" height="70" width="100%"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="block block-rounded block-bordered">
                                                    <div class="block-header block-header-default">
                                                        <h3 class="block-title">Results Chart</h3>
                                                    </div>
                                                    <div class="block-content block-content-full">
                                                        <canvas id="results-chart" height="80" width="100%"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 col-xs-12">
                                                <div class="block block-rounded block-bordered">
                                                    <div class="block-header block-header-default">
                                                        <h3 class="block-title">Fees Payment Chart</h3>
                                                    </div>
                                                    <div class="block-content block-content-full">
                                                        <canvas id="payments-chart" height="80" width="100%"></canvas>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                @if(in_array('fees',$guardian_access))
                                    <div class="tab-pane fade fade-left" id="fees" role="tabpanel">
                                        <div class="block block-rounded block-bordered">
                                            <div class="block-header block-header-default">
                                                <h3 class="block-title">Unpaid Invoices</h3>
                                                <div class="block-options">
                                                    <div class="block-options-item">
                                                        <button type="button" onclick="pay()"
                                                                class="btn btn-sm btn-outline-primary">Pay now
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="block-content">
                                                <div class="table-responsive">
                                                    <form>
                                                        <table class="js-table-checkable table table-hover table-vcenter">

                                                            <thead>
                                                            <tr>
                                                                <th class="text-center" style="width: 70px;">
                                                                    <div class="custom-control custom-checkbox d-inline-block">
                                                                        <input type="checkbox"
                                                                               class="custom-control-input invoice-check"
                                                                               amount="0"
                                                                               id="check-all"
                                                                               name="check-all">
                                                                        <label class="custom-control-label"
                                                                               for="check-all"></label>
                                                                    </div>
                                                                </th>
                                                                <th>Invoice Title</th>
                                                                <th>Session</th>
                                                                <th>Issuer</th>
                                                                <th class="text-right">Amount</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody>
                                                            @foreach($invoices as $invoice)
                                                                <tr>
                                                                    <td class="text-center">
                                                                        <div class="custom-control custom-checkbox d-inline-block">
                                                                            <input type="checkbox"
                                                                                   class="custom-control-input invoice-check invoice"
                                                                                   amount="{{$invoice->amount/100}}"
                                                                                   id="invoice_{{$loop->iteration}}"
                                                                                   name="invoice_{{$loop->iteration}}"
                                                                                   invoice="{{$invoice->invoice_id}}"
                                                                                   @if(isset($singleInvoice)) @if($invoice->id==$singleInvoice) checked="true"
                                                                                   @endif @else checked="true" @endif>
                                                                            <label class="custom-control-label"
                                                                                   for="invoice_{{$loop->iteration}}"></label>
                                                                        </div>
                                                                    </td>
                                                                    <td>{{$invoice->title}}</td>
                                                                    <td>{{$invoice->session}}</td>
                                                                    <td>{{$invoice->issuer}}</td>
                                                                    <td class="text-right">&#8358;<span
                                                                                class="invoice-amount">{{number_format($invoice->amount/100)}}</span>
                                                                    </td>
                                                                </tr>
                                                            @endforeach
                                                            </tbody>
                                                            <tr>
                                                                <td colspan="4"
                                                                    class="font-w700 text-uppercase text-right bg-body-light">
                                                                    Total
                                                                </td>
                                                                <td class="font-w700 text-right bg-body-light text-right">
                                                                    &#8358;<span
                                                                            id="total">0</span></td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="5">
                                                                    <div></div>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </form>
                                                </div>
                                            </div>
                                            <div class="block-header block-header-default block-header-rtl">
                                                <div class="block-options">
                                                    <div class="block-options-item">
                                                        <button type="button" onclick="pay()"
                                                                class="btn btn-lg btn-outline-primary">Pay now
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>
                @else
                    <div class="col-12">
                        <div class="alert alert-danger d-flex align-items-center justify-content-between" role="alert">
                            <div class="flex-fill mr-3">
                                <p class="mb-0 font-w700 font-size-16-">Oops! We cant get info on your ward ({{$user}}), looks like some settings were
                                    changed!</p>
                            </div>
                            <div class="flex-00-auto">
                                <i class="fa fa-fw fa-times-circle"></i>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
    </div>
    <div class="modal" id="invoice-modal" tabindex="-1" role="dialog" aria-labelledby="modal-block-large"
         style="display: none;" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="block block-themed block-transparent mb-0 block-rounded">
                    <div class="block-content" id="invoice-lines">

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@php
    $wardName=isset($viewedWard->first_name)?$viewedWard->first_name: 'Not Found';
@endphp
@section('title',"View Ward - $wardName")
@section('script')
    <script type='text/javascript' src="{{asset($public.'/js/moment.min.js')}}"></script>
    <script type='text/javascript' src="{{asset($public.'/js/fullcalendar.js')}}"></script>
    <script src="{{asset("$public/js/Chart.min.js")}}"></script>
    <script type="text/javascript">
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $('#time-table').fullCalendar({
            // put your options and callbacks here
            defaultView: 'listDay',
            aspectRatio: 1.2,
            header: {
                right: 'prev,next today',
                left: 'title',
            },
            fixedWeekCount: false,
            contentHeight: 'auto',
            navLinks: true,
            startParam: 'start',
            endParam: 'end',
            //
            events: {
                url: '{{url("guardian/view/$user/gettimetable")}}',
                type: 'POST',
                data: {
                    type: 'timetable',
                },
                error: function () {
                    alert('there was an error while fetching events!');
                },
                color: '#2CAC5C',   // a non-ajax option
                textColor: '#f5f5f5' // a non-ajax option
            }
        });

        var color = {
            background1: [
                'rgba(75, 192, 192, 0.8)',
                'rgba(153, 102, 255, 0.8)',
                'rgba(54, 162, 235, 0.8)',
                'rgba(255, 206, 86, 0.8)',
                'rgba(255, 159, 64, 0.8)',
                'rgba(255, 99, 132, 0.8)',
            ],
            background2: [
                'rgba(75, 192, 192, 0.5)',
                'rgba(153, 102, 255, 0.5)',
                'rgba(54, 162, 235, 0.5)',
                'rgba(255, 206, 86, 0.5)',
                'rgba(255, 159, 64, 0.5)',
                'rgba(255, 99, 132, 0.5)',
            ],
            background3: [
                'rgba(75, 192, 192, 0.2)',
                'rgba(153, 102, 255, 0.2)',
                'rgba(54, 162, 235, 0.2)',
                'rgba(255, 206, 86, 0.2)',
                'rgba(255, 159, 64, 0.2)',
                'rgba(255, 99, 132, 0.2)',
            ],
            border: [
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 206, 86, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
            ],
            border1: [
                'rgba(75, 192, 192, 1)',
                'rgba(153, 102, 255, 1)',
                'rgba(54, 162, 235, 1)',
                'rgba(255, 159, 64, 1)',
                'rgba(255, 99, 132, 1)',
            ]
        };

        var ctx = document.getElementById("attendance-chart");
        var myChart = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [2014, 2015, 2016, 2017, 2018],
                datasets: [
                    {
                        data: {{json_encode($attendance_present)}},
                        label: "Present",
                        backgroundColor: color.background2,
                        borderColor: color.border,
                        fill: false
                    }, {
                        data: {{json_encode($attendance_absent)}},
                        label: "Absent",
                        backgroundColor: color.background3,
                        borderColor: color.border,
                        fill: false
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: 'Students Attendance Chart'
                }
            }
        });

        var ctx = document.getElementById("results-chart");
        var myChart = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: ["Grade A", "Grade B", "Grade C", "Grade D", "Grade F"],
                datasets: [
                    {
                        label: "Students Gender Chart",
                        backgroundColor: color.border1,
                        data: {{json_encode($results)}}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: "Students Results Chart"
                }
            }
        });

        var ctx = document.getElementById("payments-chart");
        var myChart = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: ['Paid fees', 'Unpaid Debts'],
                datasets: [
                    {
                        label: "Students year Chart",
                        backgroundColor: [
                            'rgba(75, 192, 192, 1)',
                            'rgba(255, 99, 132, 1)',
                        ],
                        data: {!! json_encode($payments) !!}
                    }
                ]
            },
            options: {
                title: {
                    display: true,
                    text: ' Chart'
                }
            }
        });

    </script>
    <script>
        jQuery(function () {
            Dashmix.helpers(['table-tools-checkable', 'table-tools-sections']);
        });
        calculateTotalAmount();

        $('.invoice-check').change(function () {
            calculateTotalAmount();
        });

        function calculateTotalAmount() {
            let amount = 0;
            $('#total').text(amount);
            $('.invoice:checkbox:checked').each(function () {
                amount += Number($(this).attr('amount'));
            });
            $('#total').text(amount);
        }

        function pay() {
            let invoices = [];

            $('.invoice:checkbox:checked').each(function () {
                invoices.push($(this).attr('invoice'));
            });
            let data = {invoices: invoices};
            console.log(invoices[2]);
            $.get("{{url("/guardian/fees/payment/invoice/$user")}}", data, function (result) {
                let content = $('#invoice-lines');
                $('#invoice-modal').modal('show');
                content.fadeOut(100);
                content.html('');
                content.hide();
                content.html(result.html);
                content.fadeIn(800);
            });

        }

    </script>

@endsection
