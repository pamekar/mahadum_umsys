@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp
<div class="" id="" style="">
    <h2 class="card-header blue width-full text-center" id="" style="">Showing results for <span class="green"
                                                                                                 id="currentSession">{{$session.'/'.($session+1)}}</span>
        Academic Session</h2>
    <div class="media mx-4 mt-3" id="" style="">
        <div class="media-body text-left" id="" style="">
            <dl class="dl-horizontal small green" id="">
                <dt class="" id="">Name</dt>
                <dd class="text-muted" id="">{{"$userDetails->first_name $userDetails->last_name"}}</dd>
                <dt class="" id="">Reg No.</dt>
                <dd class="text-muted" id="">{{$userDetails->reg_no}}</dd>
                <dt class="" id="">School</dt>
                <dd class="text-muted" id="">{{$userDetails->faculty}}</dd>
                <dt class="" id="">Department</dt>
                <dd class="text-muted" id="">{{$userDetails->dept}}</dd>
                <dt class="" id="">Level</dt>
                <dd class="text-muted" id="">{{$userDetails->level}}</dd>
                <dt class="" id="">Cummulative GPA</dt>
                <dd class="text-muted" id="">{{$userDetails->cgpa}}</dd>
                <dt class="" id="">Previous Session GPA</dt>
                <dd class="text-muted" id="">{{$userDetails->pgpa}}</dd>
                <dt class="" id="">Outstanding Courses</dt>
                <dd class="text-muted" id="">
                    <a class="" id="" href="{{url('/undergraduate/courses/outstanding')}}"
                       target="">{{$userDetails->outstanding}}</a>
                </dd>

            </dl>
        </div>

        <img src="{{asset("$public/$userDetails->photo_location")}}"
             class="img img-thumbnail d-flex align-self-center" id=""
             alt="{{"$userDetails->first_name $userDetails->last_name"}}">
    </div>

    {{--<div class="card" id="" style="">
        <div class="list-inline p-3 " id="" style="">
            <div class="list-inline pl-3 pull-right" id="" style=""><input class="" id="option" value="2016"
                                                                          placeholder="" required="" type="hidden">
                <select class="form-control" id="specify" name="session">
                    <option value="{{$session}}">{{$session.'/'.($session+1)}}</option>
                </select>

                <button class="btn cool-button form-control" id="specify-btn" onclick="changeResult()">Show</button>
            </div>

            <div class="pr-3 pull-right" id="" style="border-right: 1px dotted;">
                <a class="btn btn-success form-control pull-right" id="download"
                   href="{{url("/undergraduate/download/result/$session/pdf")}}" target="_blank">Print
                    <i class="fa fa-file-pdf-o ml-2" id=""></i>
                </a>
            </div>
        </div>
    </div>--}}

    <div class="" id="area" style="">
        @include('student.undergraduate.partials.resultTable')
    </div>
</div>