@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';

    $background = ["$public/jpg/photo15.jpg", "$public/jpg/photo4.jpg", "$public/jpg/photo1.jpg", "$public/jpg/photo3.jpg"];

@endphp

<div class="tab-pane fade @if(!sizeof($wards['pending'])>0) show active @endif" id="active-wards" role="tabpanel">
    <h4 class=" font-w400">Active Wards</h4>
<div class="row">
    @foreach($wards['active'] as $ward)
        <div class="col-md-6 col-xl-3 animated fadeIn" data-toggle="appear">
            <div class="block block-rounded text-center">
                <div class="block-content block-content-full bg-image"
                     style="background-image: url('{{$background[array_rand($background)]}}');">
                    <img class="img-avatar img-avatar-thumb"
                         src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light">
                    <div class="font-w600">{{$ward->first_name.' '.$ward->last_name}}</div>
                    <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                </div>
                <div class="block-content block-content-full">
                    <a class="btn btn-sm btn-outline-info"
                       href="{{url("/guardian/view/$ward->reg_no")}}">View
                    </a>
                    <button type="button" onclick="ward({{$ward->reg_no}},'blocked','active')"
                            class="btn btn-sm btn-outline-danger">Block
                    </button>

                </div>
            </div>
        </div>
    @endforeach
</div>
</div>
<div class="tab-pane fade @if(sizeof($wards['pending'])>0) show active @endif" id="pending-wards" role="tabpanel">
    <h4 class="font-w400">Pending Wards</h4>
    <div class="row">
        @foreach($wards['pending'] as $ward)
            <div class="col-md-6 col-xl-3 animated fadeIn" data-toggle="appear">
                <div class="block block-rounded text-center">
                    <div class="block-content block-content-full bg-image"
                         style="background-image: url('{{$background[array_rand($background)]}}');">
                        <img class="img-avatar img-avatar-thumb"
                             src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light">
                        <div class="font-w600">{{$ward->first_name.' '.$ward->last_name}}</div>
                        <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                    </div>
                    <div class="block-content block-content-full">
                        <button type="button" onclick="ward({{$ward->reg_no}},'active','pending')"
                                class="btn btn-sm btn-outline-success">Accept
                        </button>
                        <button type="button" onclick="ward({{$ward->reg_no}},'blocked','pending')"
                                class="btn btn-sm btn-outline-danger">Reject
                        </button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
<div class="tab-pane fade" id="blocked-students" role="tabpanel">
    <h4 class="font-w400">Blocked Students</h4>
    <div class="row">
        @foreach($wards['blocked'] as $ward)
            <div class="col-md-6 col-xl-3 animated fadeIn" data-toggle="appear">
                <div class="block block-rounded text-center">
                    <div class="block-content block-content-full bg-image"
                         style="background-image: url('{{$background[array_rand($background)]}}');">
                        <img class="img-avatar img-avatar-thumb"
                             src="{{asset($public.'/'.$ward->photo_location)}}" alt="">
                    </div>
                    <div class="block-content block-content-full block-content-sm bg-body-light">
                        <div class="font-w600">{{$ward->first_name.' '.$ward->last_name}}</div>
                        <div class="font-size-sm text-muted">{{$ward->reg_no}}</div>
                    </div>
                    <div class="block-content block-content-full">
                        <button type="button" onclick="ward({{$ward->reg_no}},'active','blocked')"
                                class="btn btn-sm btn-outline-success">Un-block
                        </button>
                        <button type="button" onclick="ward({{$ward->reg_no}},'deleted','blocked')"
                                class="btn btn-sm btn-outline-danger">Delete
                        </button>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
</div>
