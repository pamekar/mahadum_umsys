@extends('voyager::master')

@section('page_title','Create Invoice')

@section('css')
    <style>
        .panel .mce-panel {
            border-left-color: #fff;
            border-right-color: #fff;
        }

        .panel .mce-toolbar,
        .panel .mce-statusbar {
            padding-left: 20px;
        }

        .panel .mce-edit-area,
        .panel .mce-edit-area iframe,
        .panel .mce-edit-area iframe html {
            padding: 0 10px;
            min-height: 350px;
        }

        .mce-content-body {
            color: #555;
            font-size: 14px;
        }

        .panel.is-fullscreen .mce-statusbar {
            position: absolute;
            bottom: 0;
            width: 100%;
            z-index: 200000;
        }

        .panel.is-fullscreen .mce-tinymce {
            height:100%;
        }

        .panel.is-fullscreen .mce-edit-area,
        .panel.is-fullscreen .mce-edit-area iframe,
        .panel.is-fullscreen .mce-edit-area iframe html {
            height: 100%;
            position: absolute;
            width: 99%;
            overflow-y: scroll;
            overflow-x: hidden;
            min-height: 100%;
        }
    </style>
@stop

@section('page_header')
    <h1 class="page-title">
        <i class=""></i>
        Create Invoice
    </h1>
    @include('voyager::multilingual.language-selector')
@stop

@section('content')

    <div class="page-content container-fluid">
    <form class="form-edit-add" role="form" action="http://mahadum.dev:8000/admin/posts" method="POST"
          enctype="multipart/form-data">
        <!-- PUT Method if we are editing -->
        <input name="_token" value="QzNqhqrNA7esLdFLHFNxZVnS7i7xR6VZXfjiQ62h" type="hidden">

        <div class="row">
            <div class="col-md-4">
                <!-- ### DETAILS ### -->
                <div class="panel panel panel-bordered panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title"><i class="icon wb-clipboard"></i> Post Details</h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                               aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="slug">Invoice Title</label>
                            <input class="form-control" id="slug" name="slug" placeholder="slug" {=""
                                   data-slug-origin="title" data-slug-forceupdate="true}" value="" type="text">
                        </div>
                        <div class="form-group">
                            <label for="status">Issuer</label>
                            <select class="form-control" name="status">
                                <option value="PUBLISHED">University</option>
                                <option value="DRAFT">Hostel</option>
                                <option value="PENDING">Library</option>
                                <option value="PENDING">Faculty</option>
                                <option value="DRAFT">Department</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="status">Type</label>
                            <select class="form-control" name="status">
                                <option value="PUBLISHED">School Invoice</option>
                                <option value="PENDING">Department Invoice</option>
                                <option value="PENDING">Level Invoice</option>
                                <option value="DRAFT">Student Invoice</option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="featured">Target</label>
                            <input name="featured" type="checkbox">
                        </div>
                        <div class="form-group">
                            <label for="category_id">Session</label>
                            <select class="form-control" name="category_id">
                                <option value="2">2013</option>
                                <option value="2">2014</option>
                                <option value="2">2015</option>
                                <option value="2">2016</option>
                                <option value="2">2017</option>
                                <option value="2">2018</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- ### IMAGE ### -->
            </div>
            <div class="col-md-8">
                <!-- ### TITLE ### -->
                <div class="panel">

                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="voyager-character"></i> Invoice Lines
                            <span class="panel-desc"> List out the items for your invoice</span>
                        </h3>
                        <div class="panel-actions">
                            <a class="panel-action voyager-angle-down" data-toggle="panel-collapse"
                               aria-hidden="true"></a>
                        </div>
                    </div>
                    <div class="panel-body">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Item</th>
                                <th>Price</th>
                                <th>Units</th>

                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td>1</td>
                                <td><input class="form-control" name="item" placeholder="Enter Item title"></td>
                                <td><input class="form-control" name="price" placeholder="Enter Item Price"></td>
                                <td><input class="form-control" name="units" placeholder="Enter no. of Units"></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>

            </div>

        </div>

        <button type="submit" class="btn btn-primary pull-right">
            <i class="icon wb-plus-circle"></i> Create New Invoice
        </button>
    </form>
</div>
@stop

@section('javascript')
    <script>

    </script>
@stop
