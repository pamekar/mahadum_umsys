@component('mail::message')
# Welcome <strong>{{Auth::user()->name}}</strong>,

Welcome to project MAHADUM. You are receiving this message because you registered on MAHADUM.

You can now view the school calendar, or keep track your ward's academic performance.

Your ward will need to add you by your username <em>({{Auth::user()->name}})</em>
before you can have access to view his/her academic performance.

@component('mail::button', ['url' => url('/login')])
Login to MAHADUM
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
