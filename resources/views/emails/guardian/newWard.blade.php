@component('mail::message')
# Good day {{$guardian}}

You are receiving this email, because {{$ward->first_name}} has requested to add you as Guardian. You can accept this request or block this student in your profile page.

Click the link below to continue.

@component('mail::button', ['url' => route('guardian.profile')])
Profile
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent
