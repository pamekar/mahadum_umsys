@php
    $public='';    
    if(config('app.env') == 'production')    
    $public ='public';
@endphp
@extends('layouts.global')
@section('title','Notices') @section('style')
    <link rel='stylesheet' href="{{url('css/fullcalendar.css')}}"/>
    <style>
        #body-content {
            padding: 1.5rem 0.5rem;
            margin: 0.5rem 0rem;
        }
    </style>
@endsection('style') @section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-9 col-xs-12 card" id="body-content">
                <div class="col-md-12 col-xs-12 partition">
                    <div class=partition-heading>
                        <strong>Memo/Notices</strong>
                    </div>
                    <div class="partition-body">
                        <div class="list-group">
                            <?php
                            foreach ( $notices as $notice )
                            {

                            ?>

                            <div class="list-group-item notice bghover">
                                <h4>
                                    <a class="green"
                                       href="{{url('notices/'.$notice->id)}}"><?php echo $notice->title;?></a>
                                </h4>
                                <ul class="list-inline text-muted">
                                    <li class="list-inline-item">
                                        Date: <?php echo date('M j, Y', strtotime($notice->created_at));?>
                                    </li>
                                    <li class="list-inline-item">
                                        Announcer: <?php echo $notice->admin_title?>
                                    </li>
                                </ul>
                                <article>{{trimText($notice->content,80)}}<a href="#" class="list-link"> ...
                                        read more</a>
                                </article>
                            </div>
                            <?php
                            }
                            ?>
                        </div>
                        <div class="" id="view-all">
                            <a href="{{url('notices')}}" class="width-full btn btn-warning">View
                                all</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection @section('script')
    <script type="text/javascript">
        <!--

        //-->
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

    </script>

@endsection
