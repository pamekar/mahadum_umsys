@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.loginTemplate') @section('title', 'Login')
@section('content')
    <div class="mb-3 text-center">
        <a class="link-fx font-w700 font-size-h1" href="{{url('')}}">
            <span class="text-dark">{{config('app.abbr')}}</span><span class="text-primary">UMS</span>
        </a>
        <p class="text-uppercase font-w700 font-size-sm text-muted">{{ $category or 'User' }} Login Form</p>
    </div>
    <div class="row no-gutters justify-content-center">
        <div class="col-sm-8 col-xl-6">
            <form method="POST" action="{{url('/login')}}" id="user-category-form">
                {{ csrf_field() }}
                <fieldset>
                    <div class="form-group">
                        <input id="reg-no" type="text"
                               class="@if($errors->has('typeError') || $errors->has('notFound') ) is-invalid @endif form-control form-control-lg form-control-alt"
                               name="name"
                               value="{{ old('name') }}" required placeholder="Username"
                               autofocus> @if($errors->has('typeError'))
                            <small
                                    class="help-block text-danger"
                            >{{ $errors->first('typeError') . $category}} category. <a
                                        href="{{url('')}}"
                                >Change Category</a>
                            </small> @endif @if($errors->has('notFound'))
                            <small
                                    class="help-block text-danger"
                            >{{ $errors->first('notFound') }} </small> @endif
                    </div>
                    <div class="form-group">
                        <input id="password" type="password"
                               class="form-control form-control-lg form-control-alt @if($errors->has('name') ) is-invalid @endif "
                               name="password" required placeholder="Password">
                        @if ($errors->has('name'))
                            <small class="help-block text-danger">
                                {{$errors->first('name') }}
                            </small>
                        @endif
                    </div>
                    <div class="form-group">
                        @if ($category!='Staff')
                            <label>
                                <input type="checkbox" id="remember" name="remember">
                                <small>Remember Me</small>
                            </label>
                        @endif
                        <input type="hidden" name="category" value="{{$category or 'default'}}" required>
                    </div>

                    <div class="form-group">
                        <button type="submit" class="btn btn-block btn-hero-lg btn-hero-primary">
                            <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                        </button>

                        <p class="mt-3 mb-0 d-lg-flex justify-content-lg-between">
                            <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1"
                               href="{{ url('/password/reset') }}">
                                <i class="fa fa-exclamation-triangle text-muted mr-1"></i> Recover password
                            </a>
                            @if ($category=='Guardian')
                                <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1"
                                   href="{{ url('/register') }}">
                                    <i class="fa fa-question-circle text-muted mr-1"></i> New User?
                                </a>
                            @endif
                        </p>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
@endsection
