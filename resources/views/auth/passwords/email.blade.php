@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';
@endphp
@extends('layouts.loginTemplate')
@section('title', 'Reset Password')

@section('content')
    <div class="mb-3 text-center">
        <a class="link-fx font-w700 font-size-h1" href="{{url('')}}">
            <span class="text-dark">{{config('app.abbr')}}</span><span class="text-primary">UMS</span>
        </a>
        <p class="text-uppercase font-w700 font-size-sm text-muted">Password Reset Form</p>
    </div>
    <div class="row no-gutters justify-content-center">
        <div class="col-sm-8 col-xl-6">
            <form class="js-validation-reminder" role="form" method="POST" action="{{ url('/password/email') }}">
                {{ csrf_field() }}
                <div class="form-group py-3">
                    <input type="text"
                           class="form-control @if($errors->has('email')) is-invalid @endif form-control-lg form-control-alt"
                           id="email" placeholder="E-mail Address" name="email" value="{{ old('email') }}" required>
                    @if($errors->has('email'))
                        <small class="help-block text-danger">
                            {{ $errors->first('email') }}
                        </small>
                    @endif
                </div>
                <div class="form-group text-center">
                    <button type="submit" class="btn btn-block btn-hero-lg btn-hero-primary">
                        <i class="fa fa-fw fa-reply mr-1"></i> Send Link
                    </button>
                    <p class="mt-3 mb-0 d-lg-flex justify-content-lg-between">
                        <a class="btn btn-sm btn-light d-block d-lg-inline-block mb-1" href="{{url('login')}}">
                            <i class="fa fa-sign-in-alt text-muted mr-1"></i> Sign In
                        </a>

                    </p>
                </div>
            </form>
        </div>
    </div>
@endsection
