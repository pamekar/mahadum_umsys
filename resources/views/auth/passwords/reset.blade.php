@php
    $public='';
    if(config('app.env') == 'production')
        $public ='public';
@endphp
@extends('layouts.loginTemplate')
 @section('title', 'Reset Password')
@section('content')
    <div class="mb-3 text-center">
        <a class="link-fx font-w700 font-size-h1" href="{{url('')}}">
            <span class="text-dark">{{config('app.abbr')}}</span><span class="text-primary">UMS</span>
        </a>
        <p class="text-uppercase font-w700 font-size-sm text-muted">Password Reset Form</p>
    </div>
    <div class="row no-gutters justify-content-center">
        <div class="col-sm-8 col-xl-6">
            <form class="js-validation-reminder" method="POST" action="{{ url('/password/reset') }}">
                {{ csrf_field() }}
                <input type="hidden" name="token" value="{{ $token }}">
                <div class="form-group{{ $errors->has('email') ? ' is-invalid' : '' }}">
                    <input id="email" type="email" class="form-control" name="email" placeholder="E-mail Address"
                           value="{{ $email or old('email') }}" required autofocus>
                    @if($errors->has('email'))
                        <small class="help-block text-danger">
                            {{ $errors->first('email') }}
                        </small>
                    @endif
                </div>

                <div class="form-group{{ $errors->has('password') ? ' is-invalid' : '' }}">
                    <input id="password" type="password" class="form-control" placeholder="New Password"
                           name="password" required>
                    @if ($errors->has('password'))
                        <small class="help-block text-danger">
                            {{ $errors->first('password') }}
                        </small>
                    @endif

                </div>

                <div class="form-group{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
                    <input id="password-confirm" placeholder="Confirm Password"
                           type="password" class="form-control" name="password_confirmation"
                           required>
                    @if ($errors->has('password_confirmation'))
                        <small class="help-block text-danger">
                            {{ $errors->first('password_confirmation') }}
                        </small>
                    @endif
                </div>

                <div class="form-group text-center">
                    <button type="submit" class="btn btn-block btn-hero-lg btn-hero-primary">
                        <i class="fa fa-fw fa-refresh mr-1"></i> Reset Password
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
