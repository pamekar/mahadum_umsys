@php    $public='';    if(config('app.env') == 'production')    $public ='public'; @endphp @extends('layouts.loginTemplate') @section('title', 'Login')
@section('content')
    <div class="mb-3 text-center">
        <a class="link-fx font-w700 font-size-h1" href="{{url('/')}}">
            <span class="text-dark">{{config('app.abbr')}}</span><span class="text-primary">UMS</span>
        </a>
        <p class="text-uppercase font-w700 font-size-sm text-muted">{{ $category or 'User' }} Login Form</p>
    </div>
    <div class="row no-gutters justify-content-center">
        <div class="col-sm-8 col-xl-6">
            <form class="js-validation-signin" method="POST" action="{{url('/user')}}" id="user-category-form">
                {{ csrf_field() }}
                <div class="form-group">
                    <select class="form-control form-control-lg form-control-alt" name="user-category"
                            id="user-category">
                        <option value="undergraduate">Undergraduate</option>
                        <option value="postgraduate">Postgraduate</option>
                        <option value="guardian">Parent/Guardian</option>
                        <option value="staff">Staff</option>
                    </select>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-hero-lg btn-hero-primary">
                        <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Submit
                    </button>
                </div>
            </form>
        </div>
    </div>
@endsection
