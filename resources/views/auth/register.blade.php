@php
    $public='';
    if(config('app.env') == 'production')
    $public ='public';
@endphp
@extends('layouts.loginTemplate')
@section('title', 'Guardian Registration')
@section('content')
    <div class="mb-3 text-center">
        <a class="link-fx font-w700 font-size-h1" href="{{url('')}}">
            <span class="text-dark">{{config('app.abbr')}}</span><span class="text-primary">UMS</span>
        </a>
        <p class="text-uppercase font-w700 font-size-sm text-muted">Guardian Registration Form</p>
    </div>
    <div class="row no-gutters justify-content-center">
        <div class="col-sm-8 col-xl-6">
            <form method="POST" action="{{ route('register') }}">
                {{csrf_field()}}
                <div class="form-group">
                    <input id="name" type="text"
                           class="form-control form-control-lg form-control-alt{{ $errors->has('name') ? ' is-invalid' : '' }}"
                           placeholder="Username"
                           name="name" value="{{ old('name') }}" required autofocus>
                    @if ($errors->has('name'))
                        <span class="text-danger">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                    @endif
                </div>

                <div class="form-group">
                    <input id="email" type="email"
                           class="form-control form-control-lg form-control-alt{{ $errors->has('email') ? ' is-invalid' : '' }}"
                           name="email" placeholder="E-mail"
                           value="{{ old('email') }}" required>

                    @if ($errors->has('email'))
                        <span class="text-danger">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                    @endif

                </div>

                <div class="form-group">
                    <input id="password" type="password"
                           class="form-control form-control-lg form-control-alt{{ $errors->has('password') ? ' is-invalid' : '' }}"
                           name="password" placeholder="Password" required>

                    @if ($errors->has('password'))
                        <span class="text-danger">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                    @endif

                </div>

                <div class="form-group">
                    <input id="password-confirm" type="password" class="form-control form-control-lg form-control-alt"
                           name="password_confirmation" placeholder="Confirm Password" required>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-block btn-hero-lg btn-hero-primary">
                        <i class="fa fa-fw fa-sign-in-alt mr-1"></i> Sign In
                    </button>
                </div>

            </form>
        </div>
    </div>
@endsection
