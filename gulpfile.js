const asset = require('laravel-asset');

require('laravel-asset-vue-2');

var gulp = require('gulp');
var uncss = require('gulp-uncss');

/*
 |--------------------------------------------------------------------------
 | asset Asset Management
 |--------------------------------------------------------------------------
 |
 | asset provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

asset(mix => {
    mix.sass('app.scss')
       .webpack('app.js');
});

asset(function(mix) {
    mix.version(['css','js','images','fonts']).browserSync({
        proxy: 'www.mahadum.com'
    });
});
/*
asset(function(mix) {
    mix.copy('out', 'resources/assets/css');
	mix.copy('public/js', 'resources/assets/js');
	mix.styles([
        'bootstrap.css',
        'global.css'
    ], 'public/css/_site.css');
	mix.version(['css','js','images','fonts']).browserSync({
        proxy: 'www.mahadum.com'
    });
});*/


// drg >> gulp task to remove unused css rules
/*gulp.task('default', function() {
     gulp.src(['public/css/global.css','public/css/bootstrap.css',])
         .pipe(uncss({
             html: ['resources/assets/html/tcpdf.html']
         }))
         .pipe(gulp.dest('out'));
 });*/