<?php
/*
 * |--------------------------------------------------------------------------
 * | Web Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register web routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | contains the "web" middleware group. Now create something great!
 * |
 */

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

Auth::routes();

Route::group([
    'namespace' => 'Auth'
],
    function () {
        Route::group([
            'prefix' => 'login'
        ],
            function () {
                Route::get('', 'LoginController@showLoginCategoryForm')
                    ->name('login');
                Route::get('{category}', 'LoginController@showLoginForm');
                Route::post('', 'LoginController@login')
                    ->middleware('checkUser');;
            });
        Route::group([
            'prefix' => 'user'
        ],
            function () {
                Route::get('', 'LoginController@showLoginCategoryForm');
                Route::post('', 'LoginController@redirectToCategory');
            });
        // Change Password Routes...
        Route::get('change_password',
            'ChangePasswordController@showChangePasswordForm')
            ->name('auth.change_password');
        Route::patch('change_password',
            'ChangePasswordController@changePassword')
            ->name('auth.change_password');

    });
// drg>> Forum Routes
// ****************************************************
// ********Forum Routes *******************************
// ********Start        *******************************
// ****************************************************

/**
 * Helpers.
 */

// Route helper.
$route = function ($accessor, $default = '') {
    return config('chatter.routes.' . $accessor, $default);
};

// Middleware helper.
$middleware = function ($accessor, $default = []) {
    return config('chatter.middleware.' . $accessor, $default);
};

// Authentication middleware helper.
$authMiddleware = function ($accessor) use ($middleware) {
    return array_unique(
        array_merge((array)$middleware($accessor), ['auth'])
    );
};

/*
 * Chatter routes.
 */
Route::group([
    'as'         => 'chatter.',
    'prefix'     => $route('home'),
    'middleware' => $middleware('global', 'web'),
    'namespace'  => 'Chatter',
], function () use ($route, $middleware, $authMiddleware) {

    // Home view.
    Route::get('/', [
        'as'         => 'home',
        'uses'       => 'ChatterController@index',
        'middleware' => $middleware('home'),
    ]);

    // Single category view.
    Route::get($route('category') . '/{slug}', [
        'as'         => 'category.show',
        'uses'       => 'ChatterController@index',
        'middleware' => $middleware('category.show'),
    ]);

    /*
     * Auth routes.
     */

    // Login view.
    Route::get('login', [
        'as'   => 'login',
        'uses' => 'ChatterController@login',
    ]);

    // Register view.
    Route::get('register', [
        'as'   => 'register',
        'uses' => 'ChatterController@register',
    ]);

    /*
     * Discussion routes.
     */
    Route::group([
        'as'     => 'discussion.',
        'prefix' => $route('discussion'),
    ], function () use ($middleware, $authMiddleware) {

        // All discussions view.
        Route::get('/', [
            'as'         => 'index',
            'uses'       => 'ChatterDiscussionController@index',
            'middleware' => $middleware('discussion.index'),
        ]);

        // Create discussion view.
        Route::get('create', [
            'as'         => 'create',
            'uses'       => 'ChatterDiscussionController@create',
            'middleware' => $authMiddleware('discussion.create'),
        ]);

        // Store discussion action.
        Route::post('/', [
            'as'         => 'store',
            'uses'       => 'ChatterDiscussionController@store',
            'middleware' => $authMiddleware('discussion.store'),
        ]);

        // Single discussion view.
        Route::get('{category}/{slug}', [
            'as'         => 'showInCategory',
            'uses'       => 'ChatterDiscussionController@show',
            'middleware' => $middleware('discussion.show'),
        ]);

        // Add user notification to discussion
        Route::post('{category}/{slug}/email', [
            'as'   => 'email',
            'uses' => 'ChatterDiscussionController@toggleEmailNotification',
        ]);

        /*
         * Specific discussion routes.
         */
        Route::group([
            'prefix' => '{discussion}',
        ], function () use ($middleware, $authMiddleware) {

            // Single discussion view.
            Route::get('/', [
                'as'         => 'show',
                'uses'       => 'ChatterDiscussionController@show',
                'middleware' => $middleware('discussion.show'),
            ]);

            // Edit discussion view.
            Route::get('edit', [
                'as'         => 'edit',
                'uses'       => 'ChatterDiscussionController@edit',
                'middleware' => $authMiddleware('discussion.edit'),
            ]);

            // Update discussion action.
            Route::match(['PUT', 'PATCH'], '/', [
                'as'         => 'update',
                'uses'       => 'ChatterDiscussionController@update',
                'middleware' => $authMiddleware('discussion.update'),
            ]);

            // Destroy discussion action.
            Route::delete('/', [
                'as'         => 'destroy',
                'uses'       => 'ChatterDiscussionController@destroy',
                'middleware' => $authMiddleware('discussion.destroy'),
            ]);
        });
    });

    /*
     * Post routes.
     */
    Route::group([
        'as'     => 'posts.',
        'prefix' => $route('post', 'posts'),
    ], function () use ($middleware, $authMiddleware) {

        // All posts view.
        Route::get('/', [
            'as'         => 'index',
            'uses'       => 'ChatterPostController@index',
            'middleware' => $middleware('post.index'),
        ]);

        // Create post view.
        Route::get('create', [
            'as'         => 'create',
            'uses'       => 'ChatterPostController@create',
            'middleware' => $authMiddleware('post.create'),
        ]);

        // Store post action.
        Route::post('/', [
            'as'         => 'store',
            'uses'       => 'ChatterPostController@store',
            'middleware' => $authMiddleware('post.store'),
        ]);

        /*
         * Specific post routes.
         */
        Route::group([
            'prefix' => '{post}',
        ], function () use ($middleware, $authMiddleware) {

            // Single post view.
            Route::get('/', [
                'as'         => 'show',
                'uses'       => 'ChatterPostController@show',
                'middleware' => $middleware('post.show'),
            ]);

            // Edit post view.
            Route::get('edit', [
                'as'         => 'edit',
                'uses'       => 'ChatterPostController@edit',
                'middleware' => $authMiddleware('post.edit'),
            ]);

            // Update post action.
            Route::match(['PUT', 'PATCH'], '/', [
                'as'         => 'update',
                'uses'       => 'ChatterPostController@update',
                'middleware' => $authMiddleware('post.update'),
            ]);

            // Destroy post action.
            Route::delete('/', [
                'as'         => 'destroy',
                'uses'       => 'ChatterPostController@destroy',
                'middleware' => $authMiddleware('post.destroy'),
            ]);
        });
    });
});

/*
 * Atom routes
 */
Route::get($route('home') . '.atom', [
    'as'         => 'chatter.atom',
    'uses'       => 'Chatter\ChatterAtomController@index',
    'middleware' => $middleware('home'),
]);

// ****************************************************
// ********End          *******************************
// ********Forum Routes *******************************
// ****************************************************

Route::group([
    'middleware' => 'auth',
],
    function () {
        Route::get('', 'HomeController@index');
        Route::group([
            'prefix' => 'home',
        ],
            function () {
                Route::get('', 'HomeController@index');
                // drg>> These are routes for JSON requests
                Route::post('getevents', 'HomeController@getEvents');
                Route::post('gettimetable', 'HomeController@getTimeTable');
            });
        Route::group(
            [
                'prefix'     => 'undergraduate',
                'namespace'  => 'student\undergraduate',
                'as'         => 'undergraduate.',
                'middleware' => [
                    'isUndergraduate'
                ]
            ],
            function () {
                Route::group(['prefix' => 'settings', 'as' => 'settings.'],
                    function () {
                        Route::post('guardian',
                            'SettingsController@changeGuardian')
                            ->name('change.guardian');
                        Route::post('guardian/update',
                            'SettingsController@updateGuardian')
                            ->name('update.guardian');

                    });
                Route::get('/search', 'SearchController@index');

                // drg>> routes for undergraduate courses
                Route::group([
                    'prefix' => 'courses'
                ],
                    function () {
                        // drg>> route group to serve notice requests
                        Route::get('', 'CourseController@index');
                        Route::get('register/{action?}',
                            'CourseController@getRegistrationPage');
                        Route::match([
                            'post',
                            'put'
                        ], 'register', 'CourseController@registerCourse');
                        Route::patch('register',
                            'CourseController@updateCourse');
                        Route::get('{page}', 'CourseController@getPage');
                        Route::get('results/{session}',
                            'CourseController@getResults');
                        Route::post('', 'CourseController@getJSON');
                    });
                Route::group([
                    'prefix' => 'fees'
                ],
                    function () {
                        Route::get('/history', 'FeesController@viewHistory');
                        Route::get('/invoice/{id}',
                            'FeesController@viewInvoice');
                        Route::get('/invoices', 'FeesController@viewInvoices');
                        Route::get('/payment', 'FeesController@viewPayment');
                        Route::get('/payment/invoice',
                            'FeesController@paymentInvoice');

                    });
                Route::group([
                    'prefix' => 'messages'
                ],
                    function () {
                        Route::post('/send', 'MessageController@sendMessage');
                        Route::post('/save', 'MessageController@saveMessage');
                        Route::post('/discard',
                            'MessageController@discardMessage');
                        Route::get('/reply/{id}',
                            'MessageController@viewReplyMessage');
                        Route::get('/reply_all/{id}',
                            'MessageController@viewReplyAllMessage');
                        Route::get('/forward/{id}',
                            'MessageController@viewForwardMessage');
                        Route::get('suggest/nodes',
                            'MessageController@suggestNodes');
                        Route::match(['get', 'post'], '/action/{type}',
                            'MessageController@action');
                        Route::get('/{type?}', 'MessageController@index');
                        Route::get('/view/{id}',
                            'MessageController@viewMessage');
                    });
                Route::group([
                    'prefix' => 'notices'
                ],
                    function () {
                        // drg>> route group to serve notice requests
                        Route::get('', 'NoticesController@index');
                        Route::post('',
                            'NoticesController@index'); // drg>> 'post' for form requests only
                        Route::get('{page}', 'NoticesController@index');
                        Route::get('view/{id}', 'NoticesController@display');
                    });

                Route::group([
                    'prefix' => 'verification'
                ],
                    function () {
                        Route::get('', 'VerifyController@index');
                        Route::get('{action}/code',
                            'VerifyController@getCodePage');
                        Route::get('{action}/code/verify',
                            'VerifyController@getRequest');
                        Route::post('{action}/code/verify',
                            'VerifyController@getRequest');
                    });
                Route::get('download/result/{session}/{format}',
                    'DownloadController@result');
                Route::get('download/registration/{format}',
                    'DownloadController@registration');
            });
// drg >> route group for guardian
        Route::group(
            [
                'prefix'     => 'guardian',
                'as'         => 'guardian.',
                'namespace'  => 'guardian',
                'middleware' => 'isGuardian'
            ],
            function () {
                // drg>> routes for urls with prefix "notices"
                Route::group([
                    'prefix' => 'settings',
                    'as'     => 'settings.'
                ],
                    function () {
                        Route::post('profile', 'SettingsController@profile')
                            ->name('profile');
                        Route::post('ward/{ward}/{action}',
                            'SettingsController@ward');
                        Route::post('photo', 'SettingsController@profile')
                            ->name('photo');
                    });
                Route::get('profile', 'GuardianController@viewProfile')
                    ->name('profile');
                Route::group([
                    'prefix' => 'view'
                ],
                    function () {
                        Route::get('/{id}', 'GuardianController@viewWard');
                        Route::post('/{id}/gettimetable',
                            'GuardianController@getTimeTable');
                    });
                Route::group(['prefix' => 'fees'], function () {
                    Route::get('/payment/invoice/{id}',
                        'GuardianController@paymentInvoice');
                });
                Route::get('approve/{action}/{ward}',
                    'GuardianController@approve')->name('approval');
            });
// drg >> route group for staff
        Route::group(
            [
                'prefix'     => 'staff',
                'namespace'  => 'staff',
                'middleware' => [
                    'isStaff'
                ]
            ],
            function () {
                // drg>> routes for urls with prefix "notices"
                Route::group(
                    [
                        'prefix'     => 'lecturer',
                        'namespace'  => 'lecturer',
                        'middleware' => 'isLecturer'
                    ],
                    function () {
                        Route::group([
                            'prefix' => '/attendance'
                        ],
                            function () {
                                Route::get('/{action}',
                                    'AttendanceController@getPage');
                                Route::get('/view/{id}',
                                    'AttendanceController@viewAttendance');

                                Route::post('create',
                                    'AttendanceController@createAttendance');
                            });
                        Route::group([
                            'prefix' => 'messages'
                        ],
                            function () {
                                Route::post('/send',
                                    'MessageController@sendMessage');
                                Route::post('/save',
                                    'MessageController@saveMessage');
                                Route::post('/discard',
                                    'MessageController@discardMessage');
                                Route::get('/reply/{id}',
                                    'MessageController@viewReplyMessage');
                                Route::get('/reply_all/{id}',
                                    'MessageController@viewReplyAllMessage');
                                Route::get('/forward/{id}',
                                    'MessageController@viewForwardMessage');
                                Route::get('suggest/nodes',
                                    'MessageController@suggestNodes');
                                Route::match(['get', 'post'], '/action/{type}',
                                    'MessageController@action');
                                Route::get('/{type?}',
                                    'MessageController@index');
                                Route::get('/view/{id}',
                                    'MessageController@viewMessage');
                            });

                        Route::group([
                            'prefix' => 'notices'
                        ],
                            function () {
                                // drg>> route group to serve notice requests
                                Route::get('', 'NoticesController@index');
                                Route::post('',
                                    'NoticesController@index'); // drg>> 'post' for form requests only
                                Route::get('{page}', 'NoticesController@index');
                                Route::get('view/{id}',
                                    'NoticesController@display');
                            });
                        // drg>> routes for undergraduate courses
                        Route::group([
                            'prefix' => 'courses'
                        ],
                            function () {
                                Route::get('', 'CourseController@index');
                                Route::get('{category}/{action}',
                                    'CourseController@index');
                                Route::post('{category}/{action}',
                                    'CourseController@request');
                            });
                        Route::group([
                            'prefix' => 'verification'
                        ],
                            function () {
                                Route::get('', 'VerifyController@index');
                                Route::get('{action}/code',
                                    'VerifyController@getCodePage');
                                Route::get('{action}/matricno',
                                    'VerifyController@getMatricnoPage');
                                Route::get('{action}/search',
                                    'VerifyController@getSearchPage');
                                Route::post('{action}/code/verify',
                                    'VerifyController@getRequest');
                                Route::post('{action}/matricno/verify',
                                    'VerifyController@getRequest');
                                Route::post('{action}/search/verify',
                                    'VerifyController@getSearchPage');
                                // drg >> this route is placed last in the list of
                                // >> verification routes because it redirects links that require verification
                                Route::get('{action}/search/verify',
                                    'VerifyController@getRequest');
                                Route::get('{action}/matricno/verify',
                                    'VerifyController@getRequest');
                                Route::get('{action}/code/verify',
                                    'VerifyController@getRequest');
                            });
                    });
            });

        Route::get('/v/c/{action}/{code}/{user}/{session?}',
            'VerifyController@index');

        /*Route::get('debug', function () {
            $recipients = \App\Message::offset(6000)->take(1500)->get();
            echo 'Get ready..';
            echo '<br>Set..';
            foreach ($recipients as $recipient) {
                $received = "$recipient->to;$recipient->cc;$recipient->bcc";
                $received = explode(';', $received);
                $received = array_flip($received);
                $number = mt_rand(1, count($received));
                /*array_push($received, [
                ]);
                $array = array_rand($received, 1);

                $seen = is_array($array) ? implode(';', $array) : $array;

                $message = \App\Message::find($recipient->id);
                $message->trashed = $seen;
                $message->save();
                echo $recipient->id . '<br>';
            }
            echo '<br>Done..';
        });*/
        Route::group(['prefix' => 'fees'], function () {
            Route::get('/initialize/payment',
                'PaystackController@initializePayment');// drg >> Initiates payment via Paystack
            Route::get('/verify/payment',
                'PaystackController@verifyPayment');
        });
    });
Route::get('/mailable/', function () {
    \Illuminate\Support\Facades\Mail::to(Auth::user()->email)
        ->send(new \App\Mail\Guardian\WelcomeGuardian());
    return new \App\Mail\Guardian\WelcomeGuardian();
});


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
    Route::get('/chart/results', 'Admin\ChartController@results');
});

Route::get('debug', function (Request $request) {
    echo $request->url() . '--)' . $request->path() . '--)'
        . route('guardian.settings.profile');
});
Route::get('/debug/guardiansapprovedpendingandblockedwards',
    function () {
        $guardians = \App\Users_guardian::select('id', 'guardian_id')->get();
        $approved = '';
        $pending = '';
        $blocked = '';
        foreach ($guardians as $guardian) {
            $wards = \App\Users_undergraduate::where('guardian',
                $guardian->guardian_id)->pluck('reg_no');
            for ($i = 0; $i < count($wards); $i++) {
                switch ($i) {
                    case 0:
                        $pending = $wards[$i];
                        break;
                    case 1:
                        $blocked = $wards[$i];
                        break;
                    default:
                        $approved .= ";$wards[$i]";
                        break;

                }
            }
            $update = \App\Users_guardian::find($guardian->id);
            $update->pending = $pending;
            $update->active = $approved;
            $update->blocked = $blocked;
            $update->save();
            $approved = '';
        }
    });