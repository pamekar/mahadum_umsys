<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
 * | API Routes
 * |--------------------------------------------------------------------------
 * |
 * | Here is where you can register API routes for your application. These
 * | routes are loaded by the RouteServiceProvider within a group which
 * | is assigned the "api" middleware group. Enjoy building your API!
 * |
 */
Route::group(['prefix' => '/mums', 'namespace' => 'API'], function () {
    Route::post('/signin', 'AuthController@signIn')->middleware('checkUser');
    Route::get('/refreshToken', 'AuthController@refreshToken');
    Route::group(['middleware' => 'jwt.auth'], function () {
        Route::post('/home', 'HomeController@index');
        Route::post('/v/c/{action}/{code}/{user}/{session?}',
            'Staff\Lecturer\VerifyController@index');
        Route::group([
            'prefix' => 'staff/lecturer/verification',
            'namespace' => 'Staff\Lecturer'
        ],
            function () {
                Route::post('', 'VerifyController@index');

                // drg >> routes provide verification forms
                Route::post('{action}/code',
                    'VerifyController@getCodePage');
                Route::post('{action}/matricno',
                    'VerifyController@getMatricnoPage');
                Route::post('{action}/search',
                    'VerifyController@getSearchPage');
                // drg >> routes to verify form requests
                Route::post('{action}/code/verify',
                    'VerifyController@getRequest');
                Route::post('{action}/matricno/verify',
                    'VerifyController@getRequest');
                Route::post('{action}/search/verify',
                    'VerifyController@getSearchPage');
                // drg >> this route is placed last in the list of
                // >> verification routes because it redirects links that require verification
                Route::get('{action}/search/verify',
                    'VerifyController@getRequest');
                Route::get('{action}/matricno/verify',
                    'VerifyController@getRequest');
                Route::get('{action}/code/verify',
                    'VerifyController@getRequest');
            });
    });
});

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');
